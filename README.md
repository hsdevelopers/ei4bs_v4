# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* EI for BootStrap is the HealthSpace development environment for the XPages application based on BootStrap
* Version 4

### How do I get set up? ###

This code depends upon 

* Domino Designer 9.0.1 FP8
* OpenNF XPages Extension Library - http://extlib.openntf.org/ or part of Domino Designer install. Minimun version V9.0.1.v00_16+2-160119-2239
* OpenNTF Domino API - http://www.openntf.org/main.nsf/project.xsp?r=project/OpenNTF%20Domino%20API Minimun version 3.2.1.201703140846
* OPenNTF Swiper - https://www.openntf.org/main.nsf/project.xsp?r=project/Swiper minimum version: 2.0.1.201707212238

To use it:

* Clone this repository using SourceTree
* Create a personal development branch using the GIT Flow feature of SourceTree
* Push your development branch back to BitBucket
* In DDE Package Explorer create a New project with the ODP (On Disk Project) folder as the source
* Create a new database from the ODP using the Team Development menu in the DDE
* Create and Complete Features using GIT Flow
* Develop and test your features locally
* Push changes back into your Development branch adn push those to BitBucket
* Notify Newbs when changes are ready to be merged

### Contribution guidelines ###

* Test your code
* Develop in a branch and see Newbs to promote

### Who do I talk to? ###

* Repo owner is Newbs