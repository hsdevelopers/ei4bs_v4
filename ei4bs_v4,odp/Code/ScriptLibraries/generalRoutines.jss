
function eiApplicationInit() {
	// we are putting these variables just once for each application
	// they are changing application(database) to application
	var dbarLoc = 'generalRoutines.eiApplicationInit';
	var localDebug = ("Yes".equals(hs_util.getAppSettingString("debug_AppInit")));
	
	// var testjunk = hs_util.getAppSettingString("EvenMoreJunk");
	
	try	{
		var mrcVarName = "myRunningContext";
		var mrcValue = "";
		localDebug = ("Yes".equals(hs_util.getAppSettingString("debug_xpinc")));
		if (!sessionScope.containsKey(mrcVarName)) {
			mrcValue= sessionScope.get(mrcVarName);
			if(localDebug) print ('************************************************************' + 
					'**************************************************************************');
			mrcValue = context.getUrlParameter("xspRunningContext");
			if(("".equals(mrcValue) )) mrcValue = "Browser";
			sessionScope.put(mrcVarName, mrcValue )
			//if("Yes".equals(hs_util.getAppSettingString("debug_xpinc")) && "Notes".equals(mrcValue)) {
			if (localDebug) print(dbarLoc + " : myRunningContext:[" + mrcValue + "]");
			//}
		}
	} catch(e) {
		dBar.error(e.toString(), dbarLoc);
		print(dbarLoc + ' ERROR: ' +  e.toString())
	}
	localDebug = ("Yes".equals(hs_util.getAppSettingString("debug_AppInit")));
	try	{
		if(localDebug) print ('************************************************************' + 
				'**************************************************************************');
		if (typeof dBar == "undefined") {
			if(localDebug) print (dbarLoc + ' - setting up debugtoolbar...'  + typeof dBar);
			importPackage(eu.linqed.debugtoolbar)
			dBar.init(true, 'black');
		} else {
			if(localDebug) print (dbarLoc + ' - debugtoolbar seems to be OK. ' + typeof dBar);
		}
	} catch(e) {
		dBar.error(e.toString(), dbarLoc);
		print(dbarLoc + ' ERROR: ' +  e.toString())
	}
	
	try {
		// xxbuildProfileNames();
	} catch(e) {
		dBar.error(e.toString(), dbarLoc);
		print(dbarLoc + ' ERROR: ' +  e.toString())
	}
	
	try {
		// hs_ProfileMap.init();
	} catch(e) {
		dBar.error(e.toString(), dbarLoc);
		print(dbarLoc + ' ERROR: ' +  e.toString())
	}

	try {
		getAppModules();
	} catch(e) {
		dBar.error(e.toString(), dbarLoc);
		print(dbarLoc + ' ERROR: ' +  e.toString())
	}
	
	try {
	//	var dbDesign = hs_profiles.get("eiroot", "MasterSettings", "DBDesign");
	//	var dbScope = hs_profiles.get("eiroot", "MasterSettings", "DBScope");
	//	dBar.debug("dbDesign is:[" + dbDesign + "] and dbScope is [" + dbScope + "]", dbarLoc)
	} catch(e) {
		dBar.error(e.toString(), dbarLoc);
		print(dbarLoc + ' ERROR from hs_profiles: ' +  e.toString())
	}
	
	try {
		var asMobiScrollKey = "HS_useMobiscroll";
		if(!(applicationScope.containsKey(asMobiScrollKey))) {
			applicationScope.put(asMobiScrollKey, "no");
			if(localDebug) print (dbarLoc + ' - set ' + asMobiScrollKey + ' to no');
		}
	} catch(e) {
		dBar.error(e.toString(), dbarLoc);
	}
	
	if(localDebug) print (dbarLoc + ' - done with routine.');
}

function getDb(key:string) {
	var dbarLoc = database.getFilePath() + '.' + 'generalRoutines.getDb';
	var localDebug = false;
	var retdb:NotesDatabase = null;
	
	try	{
		//if (typeof dBar != "undefined") {
		//	dBar.init(true, 'black');
		//}
		if (localDebug) print(dbarLoc + ' - looking for database with key: "' + key + '"')
		retdb = ("".equals(key)) ? hs_Database.getdb() : hs_Database.getdb(key);
		if(retdb == null) {
			if (localDebug) print(dbarLoc + ' - database with key: "' + key + '" returned null!');
		} else {
			if (localDebug) print(dbarLoc + ' - database with key: "' + key + '" returned ' + typeof retdb);
		}
	} catch(e) {
		dBar.error(e.toString(), dbarLoc);
		print(dbarLoc + ' ERROR: ' +  e.toString());
	} finally {
		
	}
	return retdb;
}

// NO LONGER USED
function xxbuildProfileNames() {
	var dbarLoc = database.getFilePath() + '.generalRoutines.xxbuildProfileNames';
	var localDebug = true;
	var nc:NotesNoteCollection = null;
	var pdoc:NotesDocument = null;
	var eiDb:NotesDatabase = null;
	
	try {
		var pnames = applicationScope.get('profileNames');
		if (pnames == null) {
			var noteID:String = "";
			var pname:String = "";
			var pwork = null;
			var pout = null;
			var out = [];
			var out2 = [];
			
			if(localDebug) dBar.debug('Begin processing profiles.', dbarLoc);
			if(localDebug) print (dbarLoc + ' - Begin processing profiles.');
			
			eiDb = getDb("");
			if(eiDb == null) throw ("Unable to access linked database.");
			
			nc = eiDb.createNoteCollection(false)// this creates an empty collection of noteIDs
			nc.setSelectProfiles(true);                 // 'we only need profile documents
			nc.buildCollection();		 				// 'execute the building of the notecollection
			if(nc.getCount() == 0) {
				out.push('**THERE ARE NO PROFILES IN DATABASE "' + eiDb.getTitle() + '"');
				dBar.warn('**THERE ARE NO PROFILES IN DATABASE "' + eiDb.getTitle() + '"', dbarLoc)
			} else {
				noteID = nc.getFirstNoteID(); //get the first noteid
				while (!"".equals(noteID)) {
					pdoc = eiDb.getDocumentByID(noteID);
					pname = pdoc.getItemValueString('$Name');
					pwork = pname.split("_");
					pname = pwork[1];
					if("".equals(pwork[2]) && pdoc.getItems().size() > 4){
						// out.push(pname);
						pout = {
								name : pname.substr(3),
								seq  : pname.substr(0,3),
								form : pdoc.getItemValueString('Form'),
								numItems : pdoc.getItems().size(),
								noteID : pdoc.getNoteID()
							};
							
						pname = pname.substr(3);
						//pname += ' seq:"' + pname.substr(0,3) + '",';
						//pname += ' form:"' + pdoc.getItemValueString('Form') +'",'
						//pname += ' numItems:' + pdoc.getItems().size() + ',';
						//pname += ' noteID:"' + pdoc.getNoteID() + '"';
						
						if(localDebug) dBar.debug('processing profile: ' + pname, dbarLoc);
						if(localDebug) print (dbarLoc + ' - processing profile: ' + pname);
						
						out.push(pname);
						out2.push(pout);
					}
					noteID = nc.getNextNoteID(noteID);
				}
			}
			out.sort(function (a,b){
				return (a>b) ? 1 : -1;	
			});
			out = @Unique(out);
			out2.sort(function (a,b){
				if (a.name > b.name) return 1;
				if (b.name > a.name) return -1;
				if (a.seq > b.seq) return 1;
				if (b.seq > a.seq) return -1;
				if (a.noteID > b.noteID) return 1;
				if (b.noteID > a.noteID) return -1;
				return 1;
			});
			applicationScope.put('profileNames', out);
			// applicationScope.profileData = out2;
			// dBar.debug('there are [' + (out.length) + '] profiles...', 'generalRoutines.eiApplicationInit')
			if(localDebug) print (dbarLoc + ' - there are [' + (out.length) + '] profiles...');
			
			// profile.loadGlobalSettingToScope();
			// profile.loadProfilesToScope();
			
		} else {
			// if(localDebug) print (dbarLoc + ' - applicationScope.profileNames already defined.');
		}
	} catch(e) {
		dBar.error(e.toString(), dbarLoc);
		print(dbarLoc + ' ERROR: ' +  e.toString())
	} finally {
		try {
			if (nc != null) nc.recycle();
			if (pdoc != null) pdoc.recycle();
			if (eiDb != null) eidb.recycle();
		} catch(e) {
			
		}
		if(localDebug) print (dbarLoc + ' - done with routine.');
	}
}

function getAppModules() {
	var dbarLoc = 'generalRoutines.getAppModules';
	//var localDebug = hs_util.isDebugServer();
	var localDebug = false;
	var nc:NotesNoteCollection = null;
	var pdoc:NotesDocument = null;
	var eiDb:NotesDatabase = null;
	
	var altData = hs_appmodules.getModules();
	try {
		var mdata = applicationScope.get('moduleData');
		if (mdata == null) {
			applicationScope.put("moduleData", altData)
		}
	} catch (e) {
		dBar.error(e.toString(), dbarLoc);
		print(dbarLoc + ' ERROR: ' +  e.toString())
	}
}

function addFormActions(formName, component) {

	var localDebug = false;
	var dbarLoc = "generalRoutines.addFormActions";

	if (localDebug) dBar.debug("Starting", dbarLoc);
	
	var url:XSPUrl = context.getUrl();
	var turl = "";
	var oneui = getComponent("utilLayout1");
	var uiConfig = oneui.getConfiguration();
	
	//EDIT Action
	try {
		var editNode:com.ibm.xsp.extlib.tree.complex.ComplexLeafTreeNode = new com.ibm.xsp.extlib.tree.complex.ComplexLeafTreeNode();
		editNode.setLabel("Edit");
		
		if(!currentDocument.isEditable()) {
			//turl = url.getAddress() + "/";
			//turl += @ReplaceSubstring(url.getQueryString(), ["action=openDocument"], ["action=editDocument"]);
			//editNode.setHref(turl);
			editNode.setOnClick("hsDoAction('edit');");
			if (localDebug) dBar.debug("Adding editNode", dbarLoc);
			uiConfig.addPlaceBarAction(editNode);
		} else {
			if (localDebug) dBar.debug("Adding editNode as NoOp", dbarLoc);
			editNode.setOnClick("return;");
			uiConfig.addPlaceBarAction(editNode);
		}
	} catch(e) {
		dBar.error(e.toString(), dbarLoc);
	}
	
	// SAVE Action
	try {
		var saveNode:com.ibm.xsp.extlib.tree.complex.ComplexLeafTreeNode = new com.ibm.xsp.extlib.tree.complex.ComplexLeafTreeNode();
		saveNode.setLabel("Save");
		
		if(currentDocument.isEditable()) {
			//turl = "javascript:doDocumentSave();"
			//saveNode.setHref(turl);
			saveNode.setOnClick("hsDoAction('save');");
			if (localDebug) dBar.debug("Adding saveNode", dbarLoc);
			uiConfig.addPlaceBarAction(saveNode);
		} else {
			if (localDebug) dBar.debug("Adding saveNode as NoOp", dbarLoc);
			//saveNode.setHref("#");
			saveNode.setOnClick("return;");
			uiConfig.addPlaceBarAction(saveNode);
		}
		
	} catch(e) {
		dBar.error(e.toString(), dbarLoc);
	}
	
	// CANCEL action
	try {
		var cancelNode:com.ibm.xsp.extlib.tree.complex.ComplexLeafTreeNode = new com.ibm.xsp.extlib.tree.complex.ComplexLeafTreeNode();
		cancelNode.setLabel("Cancel");
		cancelNode.setOnClick("hsDoAction('cancel');");
		if (localDebug) dBar.debug("Adding cancelNode", dbarLoc);
		uiConfig.addPlaceBarAction(cancelNode);
	} catch(e) {
		dBar.error(e.toString(), dbarLoc);
	}
	
	// NEW action(s)
	try {
		// localDebug = hs_util.isDebugServer();
		if(localDebug) dBar.debug("Creating newForms array for form:[" + formName + "]", dbarLoc);
		var newForms = [];
		if("Recreational Water".equalsIgnoreCase(formName)){
			newForms.push({form:"PoolFacility",label:"Pool Facility"})
			newForms.push({form:"PoolReport",label:"Pool Report"})
		} else if("PoolFacility".equalsIgnoreCase(formName)){
			newForms.push({form:"Inspection",label:"Inspection"});
			newForms.push({form:"SafetyPlan",label:"Safety Plan"});
			newForms.push({form:"PoolFacility",label:"Pool"});
			newForms.push({form:"EngineeringReview",label:"Engineering Review"});
		} else{
			newForms.push({form:"None",label:"None"})
		}
		
		if(localDebug) dBar.debug("for form:[" + formName + "] adding [" + newForms.length + "] elements to New Menu.", dbarLoc);
		//if(localDebug) openLogBean.addEvent("for form:[" + formName + "] adding [" + newForms.length + "] elements to New Menu.", component, 7);
		
		var newNode:com.ibm.xsp.extlib.tree.complex.ComplexContainerTreeNode = new com.ibm.xsp.extlib.tree.complex.ComplexContainerTreeNode();
		newNode.setLabel("New");
		
		var scopeKey = "HS_ViewParms";
		var viewParms = sessionScope.get(scopeKey);
		var baseUrl = @Left(url.getAddress(), ".nsf") + 
			".nsf/showDocument.xsp" +
			"?dbkey=" + viewParms.dbkey;
		var childNode:com.ibm.xsp.extlib.tree.complex.ComplexLeafTreeNode = null;
		for(var i=0; i<newForms.length; i++) {
			// if(localDebug) dBar.debug("processing " + i + " label:[" + newForms[i].label + "] form:[" + newForms[i].form + "]", dbarLoc);
			childNode = new com.ibm.xsp.extlib.tree.complex.ComplexLeafTreeNode();
			childNode.setComponent(oneui);
			childNode.setLabel(newForms[i].label);
	    
	    	turl = baseUrl + "&form=" + escape(newForms[i].form);
	    	childNode.setHref(turl);
	    	//childNode.setImage(docAppProfile.getItemValueString("appLogoThumb"));
	    	newNode.addChild(childNode);

	    	childNode = null;
		}
		uiConfig.addPlaceBarAction(newNode);
	} catch(e) {
		dBar.error(e.toString(), dbarLoc);
		//openLogBean.addError(e, component, 2);
	}
}

function buildDirectPath(resourceName) {
	var dbarLoc = "genreralRoutines.buildDirectPath";
	var localDebug = false;
	var src = resourceName;
	var path = '';
	try {
		path = context.getUrl().getAddress();
		//if (localDebug) dBar.debug("Base path is: " + path, dbarLoc);
		
		if(path.match(".nsf") != null) {
			path = @Left(path, ".nsf") + ".nsf/" + src;
		} else if(path.match(".NSF") != null) {
			path = @Left(path, ".NSF") + ".NSF/" + src;
		} else {
			path = @Left(path.toLowerCase(), ".nsf") + ".nsf/" + src;
		}
		if (localDebug) dBar.debug("New path is: " + path, dbarLoc);
		return path;
	} catch(e) {
		dBar.error("ERROR: " + e.toString(), dbarLoc);
	}
	return path;
}

function getVideoDirectPath (fext:string) {
	var dbarLoc = "genreralRoutines.getVideoDirectPath";
	var localDebug = false;
	
	var viewName = "HS_Files";
	var docKey = "videobackground.";
	var result = buildDirectPath("christmas_snow." + fext);;
	
	var nview:NotesView = null;
	var nventry:NotesViewEntry = null;
	var vcols:java.util.Vector = null;
	var src = "";
	try {
		nview = database.getView(viewName);
		if (nview == null) throw(new java.lang.Exception("Could not access view [" + viewName + "] in database [" + database.getFilePath() + "]"));
		nventry = nview.getEntryByKey(docKey + fext, true);
		if (nventry == null) throw(new java.lang.Exception("Could not access access doc [" + docKey + fext + "] in view [" + viewName + "] in database [" + database.getFilePath() + "]"));
		vcols = nventry.getColumnValues();
		if(localDebug) dBar.debug("Got columns: 0:[" + vcols[0] + "] 2:[" + vcols[2] + "]", dbarLoc);
		src = viewName + "/" + vcols[0] + "/$FILE/" + vcols[2];
		result = buildDirectPath(src);
		if(localDebug) dBar.debug("Computed result is: [" + result + "]", dbarLoc);
	} catch(e) {
		dBar.error("ERROR: " + e.toString(), dbarLoc);
	} finally {
		if(nventry != null) nventry.recycle();
		if (nview != null) nview.recycle()
		return result;
	}
}
function getFileData() {
	var dbarLoc = "genreralRoutines.loadFileData";
	var localDebug = false;
	
	var viewName = "HS_Files";
	var scopeKey = "HS_Files";
	var result = applicationScope.get(scopeKey);
	if(result != null) return result;
	
	var nview:NotesView = null;
	var vec:NotesViewEntryCollection = null;
	
	result = [];
	var nfile = {};
	var sdate:Date = new Date();
	var inactive = "";
	try {
		nview = database.getView(viewName);
		if (nview == null) throw(new java.lang.Exception("Could not access view [" + 
							viewName + "] in database [" + database.getFilePath() + "]"));
		vec = nview.getAllEntries();
		for (nventry in vec) {
			vcols = nventry.getColumnValues();
			inactive = (vcols.size() < 11) ? "" : vcols[10];
			if(!"Yes".equals(inactive)) {
				src = viewName + "/" + vcols[0] + "/$FILE/" + vcols[2];
				nfile = {
					"name" : vcols[0],
					"desc" : vcols[1],
					"file" : vcols[2],
					"purpose" : vcols[3],
					"timeout" : vcols[4],
					"sizes" : vcols[5],
					"src" : src,
					"module" : (vcols.size() < 7) ? nventry.getDocument().getItemValueString("fileModule") : vcols[6],
					"imgwidth" : (vcols.size() < 8) ? "" : vcols[7],
					"actionlabel" : (vcols.size() < 9) ? "" : vcols[8],
					"targetxpage" : (vcols.size() < 10) ? "" : vcols[9],
					"ovactionlabel" : (vcols.size() < 13) ? "" : vcols[11],
					"ovtargetxpage" : (vcols.size() < 14) ? "" : vcols[12]
				}
				if(localDebug) dBar.debug("Got columns: 0:[" + vcols[0] + "]" + 
						" 1:[" + vcols[1] + "]" + " 2:[" + vcols[2] + "]" + 
						" 3:[" + vcols[3] + "]" + " 4:[" + vcols[4] + "]" + 
						" 5:[" + vcols[5] + "]" + " src:[" + src + "]", dbarLoc);
				
				if(!"hpcarousel".equals(nfile.purpose)) {
					nfile.actionlabel = "";
					nfile.targetxpage = "";
					nfile.ovactionlabel = "";
					nfile.ovtargetxpage = "";
				} else {
					if("".equals(nfile.actionlabel)) nfile.actionlabel = "View facilities";
					if("".equals(nfile.targetxpage)) nfile.targetxpage = "module_facilities.xsp";
					if("".equals(nfile.ovactionlabel)) nfile.ovactionlabel = nfile.actionlabel;
					if("".equals(nfile.ovtargetxpage)) nfile.ovtargetxpage = nfile.targetxpage;
				}
				
				result.push(nfile);
			}
		}
		applicationScope.put(scopeKey, result);
		if(localDebug){
			var msecs = new Date().getTime() - sdate.getTime();
			dBar.debug("Added [" + (result.length+1) + 
						"] elements to [" + scopeKey + "]" +
						"  in [" + msecs + "] msecs.", dbarLoc);
		}
	} catch(e) {
		dBar.error("ERROR: " + e.toString(), dbarLoc);
	} finally {
		//if(vec != null) nventry.recycle();
		//if (nview != null) nview.recycle()
		return result;
	}
}

function getSplashTimeout() {
	var dbarLoc = "generalRoutines.getSplashTimeout";
	var localDebug = false;

	//var dflt = "/HealthSpace-a.png";
	var rid = context.getUrlParameter("replid");
	var did = context.getUrlParameter("docid");
	
	if(!("".equals(rid)) && !("".equals(did))) {
		return 5;	// fast timeout for redirectpage.
	}
	var dflt = 4000;
	var sdata = getFileData();
	if (sdata == null) return dflt;
	if (sdata.length == 0) return dflt;
	for (var i=0; i<sdata.length; i++) {
		if("Splash".equalsIgnoreCase(sdata[i].purpose)) {
			var result = parseInt(sdata[i].timeout) * 1000;
			if(localDebug) dBar.debug("Found splash timeout: [" + result + "]", dbarLoc);
			//if(@UserName().indexOf("Henry Newberry") > -1) result += 60000;
			return result;
		}
	}
}

function getSplashNextPage () {
	var dbarLoc = "generalRoutines.getSplashNextPage";
	var localDebug = false;

	var rid = context.getUrlParameter("replid");
	var did = context.getUrlParameter("docid");
	var turl = "";
	if(!("".equals(rid)) && !("".equals(did))) {
		turl = context.getUrl().getPath();
		turl += ("".equals(context.getUrlParameter("platform"))) ? "" : "?platform=" + context.getUrlParameter("platform");
	} else {
		var replkey = "urlReplid";
		var dockey = "urlDocid";
		if(sessionScope.containsKey(replkey)) {
			rid = sessionScope.get(replkey);
			did = sessionScope.get(dockey);
			turl = @Left(context.getUrl().getPath(), ".nsf");
			turl += ".nsf/frmfacility.xsp";	
		} else {
			turl = @Left(context.getUrl().getPath(), ".nsf");
			turl += ".nsf/home.xsp"
		}
	}
	if (localDebug) dBar.debug("returning;[" + turl + "]", dbarLoc);
	return turl;
}

function getSplashUrl() {
	var dbarLoc = "generalRoutines.getSplashUrl";
	var localDebug = false;

	//var dflt = "/HealthSpace-a.png";
	var dflt = "HS_LogoSmalle.jpg";
	var sdata = getFileData();
	if (sdata == null) return dflt;
	if (sdata.length == 0) return dflt;
	var splashes = [];
	for (var i=0; i<sdata.length; i++) {
		if("Splash".equalsIgnoreCase(sdata[i].purpose)) {
			splashes.push(sdata[i]);
			//var result = buildDirectPath(sdata[i].src);
			//if(localDebug) dBar.debug("Found splash value: [" + result + "]", dbarLoc);
			//return result;
		}
	}
	if(splashes.length == 0) {
		if(localDebug) dBar.debug("did not find splash returning [" + dflt + "]", dbarLoc); 
		return dflt;
	};
	if(localDebug) dBar.debug("found [" + splashes.length + "] splashes", dbarLoc);
	var width = "";
	var result = "";
	try {
		var splnum = 0;
		if(deviceBean.isIpad()) splnum = 1;
		if(deviceBean.isAndroid()) splnum = 2;
		if(deviceBean.isIphone()) splnum = 3;
		if(splnum >= splashes.length) splnum = 0;
		result = buildDirectPath(splashes[splnum].src);
		if(localDebug) dBar.debug("Found splash [" + splnum + "] value: [" + result + "]", dbarLoc);
		return result;
	} catch(e) {
		dBar.error(e.toString(), dbarLoc);
		result = buildDirectPath(splashes[0].src);
		if(localDebug) dBar.debug("Found splash [" + splnum + "] value: [" + result + "]", dbarLoc);
		return result;
	}
}

function getImgFromFileUrl(imgkey) {
	var dbarLoc = "generalRoutines.getImgFromFileUrl";
	var localDebug = false;

	var sdata = getFileData();
	if (sdata == null) return "";
	if (sdata.length == 0) return "";
	for (var i=0; i<sdata.length; i++) {
		if(imgkey.equalsIgnoreCase(sdata[i].purpose)) {
			var result = buildDirectPath(sdata[i].src);
			if(localDebug) dBar.debug("Found " + imgkey  + " value: [" + result + "]", dbarLoc);
			return result;
		}
	}
	if(localDebug) dBar.debug("did not find splash returning []", dbarLoc); 
	return "";
}

function getAppleIcon(sizes) {
	var dbarLoc = "generalRoutines.getAppleIcon";
	var localDebug = false;

	//var dflt = "/HealthSpace-a.png";
	var dflt = "icon.png";
	var sdata = getFileData();
	if (sdata == null) return dflt;
	if (sdata.length == 0) return dflt;
	for (var i=0; i<sdata.length; i++) {
		if("appleIcon".equalsIgnoreCase(sdata[i].purpose) 
				&& sizes.equals(sdata[i].sizes)) {
			var result = buildDirectPath(sdata[i].src);
			if(localDebug) dBar.debug("Found apple icon for [" + sizes + "] value: [" + result + "]", dbarLoc);
			return result;
		}
	}
	if(localDebug) dBar.debug("did not find splash returning [" + dflt + "]", dbarLoc); 
	return dflt;
	
}
// this function verifies that a view can be accessed and returns a messaeg if it cannot be accessed.
function checkViewAccessOld(dbkey, vname) {
	var dbarLoc = "genreralRoutines.checkViewAccess";
	var localDebug = false;
	
	var ssName = "";
	var ssvalue = "";
	var wdb:NotesDatabase = null;
	var nview:NotesView = null;
	var msg = "";
	
	try {
		if("".equals(dbkey)) return "";
		if("".equals(vname)) return "";
		
		ssName = "HS_CVA_" + dbkey.replace(" ", "") + "_" + vname.replace(" ", "");
		ssName = ssName.replace("\\", "");
		ssvalue = sessionScope.get(ssName);
		if (ssvalue != null) return ssvalue;
		
		wdb = getDb(dbkey);
		if(wdb == null ) {
			msg = 'Unable to access the database with key: "' + dbkey + '".';
			dBar.error(msg, dbarLoc);
		} else if (!wdb.isOpen()) {
			msg = 'Unable to access the database with key: "' + dbkey + '".';
			dBar.error(msg, dbarLoc);
		} else {
			nview = wdb.getView(vname);
			if(nview == null) {
				msg = 'Unable to access the view "' + vname + '" in database "' + wdb.getTitle() + '"';
				dBar.error(msg, dbarLoc);
			}
		}
		
	} catch (e) {
		msg = 'ERROR accessing view "' + module.vname + '" in db with key: "' + module.dbkey + '". ' + e.toString();
		dBar.error(msg, dbarLoc);
	} finally {
		if(nview != null) nview.recycle();
		if(wdb != null) wdb.recycle();
	}
	sessionScope.put(ssName, msg);
	return msg;
}

//this function verifies that a view can be accessed and returns a messaeg if it cannot be accessed.
function checkViewAccess (dbkey, vname) {
	var dbarLoc = "genreralRoutines.checkViewAccess";
	var localDebug = false;
	
	var ssKey = "HS_CheckViewAccess";
	var ssData = sessionScope.get(ssKey);
	if(ssData == null) {
		ssData = {
			firstkey : dbkey,
			firstview : vname
		}
	}
	var ssName = "";
	var ssvalue = "";
	var wdb:NotesDatabase = null;
	var nview:NotesView = null;
	var msg = "";
	
	try {
		if("".equals(dbkey)) return "";
		if("".equals(vname)) return "";
		
		//ssName = "HS_CVA_" + dbkey.replace(" ", "") + "_" + vname.replace(" ", "");
		ssName = "" + dbkey.replace(" ", "") + "_" + vname.replace(" ", "");
		ssName = ssName.replace("\\", "");
		if(ssData[ssName]) {
			//dBar.debug("Found member for [" + ssName + "]: [" + typeof ssData[ssName] + "]", dbarLoc);
			return ssData[ssName];
		}
		wdb = getDb(dbkey);
		if(wdb==null) {
			msg = 'Unable to access the database with key: "' + dbkey + '".';
			dBar.error(msg, dbarLoc);
		} else if (!wdb.isOpen()) {
			msg = 'Unable to access the database with key: "' + dbkey + '".';
			dBar.error(msg, dbarLoc);
		} else {
			nview = wdb.getView(vname);
			if(nview == null) {
				msg = 'Unable to access the view "' + vname + '" in database "' + wdb.getTitle() + '"';
				dBar.error(msg, dbarLoc);
			}
		}
		
	} catch (e) {
		msg = 'ERROR accessing view "' + vname + '" in db with key: "' + dbkey + '". ' + e.toString();
		dBar.error(msg, dbarLoc);
		print (msg);
	} finally {
		if(nview != null) nview.recycle();
		if(wdb != null) wdb.recycle();
	}
	if (!"".equals(msg)) {
		try {
			msg += " path:[" + hs_Database.getLastPath() + "]";
			msg += " replId:[" + hs_Database.getLastReplId() + "]";
			msg += " server:[" + hs_Database.getLastServer() + "]";
		} catch (e) {
			// ignore...
		}
	}
	ssData[ssName] = msg;
	sessionScope.put(ssKey, ssData);
	return msg;
}

function clearCheckViewAccess() {
	var dbarLoc = "genreralRoutines.clearCheckViewAccess";
	var localDebug = false;
	try {
		var ssKey = "HS_CheckViewAccess";
		sessionScope.remove(ssKey);
		ssKey = HS_database.getHsdb_scopeKey();
		sessionScope.remove(ssKey);
	} catch(e) {
		dBar.error("ERROR: " + e.toString(), dbarLoc);
	}
}

// From notesIn9 #110 by Peter Persnell
function isMobile() {
	var uagent:string = context.getUserAgent().getUserAgent();
	var matchStrings = ['iphone','iPhone','android','Android', 'ipad', 'iPad'];
	// Added this to support Thinkpad Tablet like things...
	matchStrings.push("(compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0; Touch; MALCJS)");
	
	var rslt = false;
	for(var i=0; i<matchStrings.length; i++) {
		if (uagent.match(matchStrings[i]) != null) rslt = true;
	}
	return rslt;
}

function isMobilePhone() {
	var uagent:string = context.getUserAgent().getUserAgent();
	var matchStrings = ['iphone','iPhone','android','Android'];
	var rslt = false;
	for(var i=0; i<matchStrings.length; i++) {
		if (uagent.match(matchStrings[i]) != null) rslt = true;
	}
	return rslt;
}

function isMobilePage() {
	if(context.getUrl().getPath().indexOf("/m_") > 0) {
		return true;
	} else {
		return false;
	}
}

function saveUserAgentString() {
	var localDebug = hs_util.isDebugServer();
	var dbarLoc = "generalRoutines.saveUserAgentString";
	
	try {
		//if(!localDebug) return; // Only do this on Henry1 :-)
		var uagent:string = context.getUserAgent().getUserAgent();
		var srcIpAddr = facesContext.getExternalContext().getRequest().getRemoteAddr();
		if(localDebug) print(dbarLoc + " processing:[" + uagent + "] from:[" + srcIpAddr + "]");
		
		var nview:NotesView = database.getView("UserAgentStrings");
		if (nview == null) {
			if(localDebug) print(dbarLoc + " view UserAgentStrings not found.");
			return;
		}
		var uadoc:NotesDocument = nview.getDocumentByKey(uagent, true);
		if(uadoc != null) {
			if(localDebug) print(dbarLoc + " already have a doc for that one.")
			return;
		}
		
		uadoc = database.createDocument();
		if(uadoc == null) {
			if(localDebug) print(dbarLoc + " database.createDocument() failed.")
			return;
		}
		uadoc.replaceItemValue("Form", "UserAgentString");
		uadoc.replaceItemValue("UserAgentString", uagent);
		uadoc.replaceItemValue("RemoteAddr", srcIpAddr);
		uadoc.computeWithForm(false, false);
		uadoc.save(true, false);
		if(localDebug) print(dbarLoc + " doc saved.");
		return;
	} catch (e) {
		if(localDebug) print("generalRoutines.saveUserAgentString ERROR: " +e.toString())
	}
}

function getDirectUrl(tdoc:NotesXspDocument) {
	//var localDebug = (hs_util.isDebugServer() || hs_util.isDebugger());
	var localDebug = false;
	var dbarLoc = "generalRoutines.getDirectUrl";
	
	try {
		var replid = tdoc.getDocument().getParentDatabase().getReplicaID();
		var docid =  tdoc.getDocument().getUniversalID();
		var url = @Left(context.getUrl().getPath(), ".nsf") + ".nsf/m_home.xsp";
		url += "?open&replid=" + replid;
		url += "&docid=" + docid;
		if(localDebug)dBar.debug(url, dbarLoc);
		return url;
	} catch (e) {
		if(localDebug) print(dbarLoc + " ERROR: " + e.toString())
		return "ERROR: " + e.toString();
	}
}

function checkUrlModule() {
	// DEBUGGING uses PRINTS because the dBar output is not yet ready when this is called.
	//var localDebug = false;
	var localDebug = "Yes".equals(hs_util.getAppSettingString("debug_View"));
	var dbarLoc = "generalRoutines.xsp.checkUrlModule";
	var sskey = "active_module";
	var askey = "moduleData";
	var urlkey = "module";
	var oname:string = "";
	if(sessionScope.containsKey(sskey)) {
		if("".equals(context.getUrlParameter(urlkey))) {
			if (localDebug) print("No module on the URL...", dbarLoc);
			return;
		}
		var amod = sessionScope.get(sskey);
		oname = amod.oname;
		if(context.getUrlParameter(urlkey).equals(oname.replace(" ", ""))) return;
		//print("in:[" + dbarLoc + "]:" + ' sessionScope." + sskey + " i OK:[' + sessionScope.containsKey(sskey) + "]");
		sessionScope.remove(sskey)
	}
	try {
		eiApplicationInit();
		var tmod = ("".equals(context.getUrlParameter(urlkey))) ? "Food" : context.getUrlParameter(urlkey);
		if(localDebug) print(dbarLoc + ": attempt to use module [" + tmod + "] url parm:[" + context.getUrlParameter(urlkey) + "]");
		
		var mdata = applicationScope.get(askey);
		if(mdata == null) {
			getAppModules();
			mdata = applicationScope.get(askey);
		}
		if(mdata == null) {
			print (dbarLoc + ": applicationScope." + askey + " is returning null...");
			return;
		}

		for (var i=0; i<mdata.length; i++){
			if(!("Anonymous".equals(@UserName())) || (mdata[i].aaccess)) {
				if(!("-Blank-".equalsIgnoreCase(mdata[i].label))) {
					oname = mdata[i].oname;
					oname = oname.replace(" ", "");
					if(localDebug) print(dbarLoc + ": checking module:[" + oname + "]");
					if(tmod.equals(oname) || tmod.equals(mdata[i].oname)) { 
						sessionScope.put(sskey, mdata[i]);
						if (localDebug) print(dbarLoc + ": found the module as item #[" + i + "] mdata[i].oname:[" + mdata[i].oname + "]");
						return;
					}
				}
			}	
		}
		if(localDebug) print("in: " + dbarLoc + ": did not find the module using item #[0]");
		sessionScope.put(sskey, mdata[0]);
		return;
	} catch (e) {
		print("in: " + dbarLoc + " got error: " + e.toString());
	}
}

function addBreadCrumb(page, bctype) {
	var localDebug = ("pay invoice".equals(page));
	// var localDebug = false;
	var dbarLoc = "generalRoutines.ssjs.addBreadCrumb";
	
	if(!bctype) bctype = page.toLowerCase();
	
	var bcformat = hs_util.getAppSettingString("BreadCrumbFormat");
	var home = "Home";
	if ("".equals(bcformat)) bcformat="default";
	if ("camel".equals(bcformat)) { 
		page = hs_util.toTitleCase(page);
		home - "home";
	}
	if ("lcase".equals(bcformat)) {page = page.toLowerCase();}
	if(localDebug) dBar.debug("**********************************************************************************************************************************", dbarLoc);
	if(localDebug) dBar.debug("called from page:[" + context.getUrl().getPath() + "] page:[" + page + "] bctype:[" + bctype + "]", dbarLoc);
	
	var sskey = "HS_BreadCrumbs";
	var breadcrumbs = sessionScope.get(sskey);
	if(breadcrumbs == null) {
		breadcrumbs = [];
		if(!"Home".equals(bctype)) {
			breadcrumbs.push({
				page : home,
				bctype : "Home",
				url : @Left(context.getUrl().toString(), ".nsf") + ".nsf/home.xsp"
			});
		}
		breadcrumbs.push({
			page : page,
			bctype : bctype,
			url : context.getUrl().toString()
		});
		if(localDebug)dBar.debug("sessionScope." + sskey + " is null added:[" + breadcrumbs.toString() + "]" , dbarLoc);
	} else {
		var newbc = [];
		for(var i=0; i<breadcrumbs.length; i++) {
			if(bctype.equals(breadcrumbs[i].bctype)) {
				newbc.push({
					page: page,
					bctype : bctype,
					url : context.getUrl().toString()
				});
				if(localDebug)dBar.debug("adding breadcrumb [" + page + "][" + bctype + "] in position:[" + i + "] was:[" + breadcrumbs.size() + "]", dbarLoc);
				i = 10000;
			} else {
				newbc.push(breadcrumbs[i]);
				if(localDebug)dBar.debug("copying over breadcrumb: [" + breadcrumbs[i].page + "][" + breadcrumbs[i].bctype + "] in position [" + i + "]", dbarLoc);
			}
		}
		if (i<10000) {
			newbc.push({
				page: page,
				bctype : bctype,
				url : context.getUrl().toString()
			});
			if(localDebug)dBar.debug("adding breadcrumb [" + page + "][" + bctype + "] to the end. [" + newbc.length + "]", dbarLoc);
		}
		
		breadcrumbs = newbc;
	}
	sessionScope.put(sskey, breadcrumbs);
}

function getAppSettingsValue(fname) {
	var fvalues = getAppSettingsValues(fname);
	if(fvalues == null) return "";
	if(fvalues.size() < 1) return "";
	return fvalues.get(0);
}

function getAppSettingsValues(fname) {
	return hs_util.getAppSettingsValues(fname);
}

function getActiveDbKey(){
	var rkey = "eiRoot";
	var localDebug = "Yes".equals(hs_util.getAppSettingString("debug_View"));
	var dbarLoc = "generalRoutines.getActiveDbKey"
	
	var vskey = "activeDbKey";
	if(viewScope.containsKey(vskey)) return viewScope.get(vskey);
	
	var scopeKey = "active_module";
	var amod = sessionScope.get(scopeKey);
	if (amod == null) {
		var mdata = applicationScope.get("moduleData"); 
		amod = mdata[0];
	} 
	rkey = amod.dbkey;
	if(localDebug) dBar.debug("Returning dbkey:[" + rkey + "]", dbarLoc);
	return rkey;
}
function getActiveViewName() {
	//var localDebug = false;
	var localDebug = "Yes".equals(hs_util.getAppSettingString("debug_View"));
	var dbarLoc = "generalRoutines.getActiveViewName"
	var vskey = "activeViewName";
	var vskey2 = "useLetterPager";
	if(viewScope.containsKey(vskey)) return viewScope.get(vskey);
	
	//return "AllFacilitiesFlat";
	var scopeKey = "active_module";
	var amod = sessionScope.get(scopeKey);
	var vname = "";
	if (amod == null) {
		var mdata = applicationScope.get("moduleData"); 
		amod = mdata[0];
	} 
	vname = amod.v4rname;
	if(hs_util.isEditor()) vname = amod.vname;
	if(!amod.v4rname) vname = amod.vname;
	if("".equals(amod.v4rname)) vname = amod.vname;
	
	if(localDebug) dBar.debug("view name is: [" + vname + "]", dbarLoc);
	viewScope.put(vskey, vname);
	viewScope.put(vskey2, "Y"); // Set this as default.
	return vname;
}