/*
InspectionCs.js - Copyright 2013-2014 HealthSpace, Inc
This code is NOT public domain 
 */

if (!hs_inspectionTables)
	var hs_inspectionTables = null;

if (!hs_inspectionPreload)
	var hs_inspectionPreload = {
		// steps : ["clear", "sections", "violations", "observations",
		// "ajaxload"],
		// steps : ["clear", "ajaxload"],
		// steps : [ "clear", "doajax.sections", "doajax.violations",
		// "doajax.observations" ],
		// steps : [ "doajax.sections", "doajax.violations",
		// "doajax.observations" ],
		steps : [ "postCurrent", "doajax.sections", "doajax.violations", "doajax.observations" ],
		doedit : false,
		// steps : ["clear", "sections", "violations", "observations"],
		msecs : [],
		// steps : ["sections", "violations", "observations"],
		cstep : "",
		id : "",
		id2 : "",
		isDbServer : "false",
		vmodule : "",
		sgetDt : {},

		doFirst : function(doeditstr) {
			this.sdate = null;
			this.putMessage("Started");
			if (this.isDbServer)
				this.putMessage("violation module:[" + this.vmodule + "]");
			if (this.vmodule == "") {
				this.putMessage("Unable to load violation data because module is not defined!");
				return;
			}
			this.doedit = (doeditstr) ? true : false;
			this.doStep(this.steps[0]);
		},

		getBaseUrl : function() {
			var turl = location.pathname;
			var idx = turl.toLowerCase().indexOf(".nsf");
			if (idx > -1) {
				turl = turl.substring(0, idx) + ".nsf/"
			}
			// hs_utils.debug("[hs_inspectionPreload.getBaseUrl]: url:" + turl);
		return turl;
	},

	doStep : function(step) {
		try {
			this.putMessage("Step: " + step);
			this.cstep = step;
			if ((step == "ajaxload") || (step.indexOf("ajax.") > 0)) {
				var turl = this.getBaseUrl() + "m_inspectionDataAjax.xsp?open&vmodule=" + escape(this.vmodule);
				turl += (step.indexOf("ajax.") > 0) ? "&listname=" + step.split(".")[1] : "";
				turl += "&unique=" + (new Date().getTime());
				hs_utils.debug("AJAX URL: " + turl, "hs_inspectionPreload.doStep");
				// The "xhrGet" method executing an HTTP GET
		dojo.xhrGet( {
			// The URL to request
			url : turl,
			// Extend the timeout to an unreasonable time...
			timeout : (75 * 1000),
			// The method that handles the request's
			// successful result
			// Handle the result as JSON data
			handleAs : "json",
			// Handle the response any way you'd like!
			load : function(jsondata) {
				try {
					var step = hs_inspectionPreload.cstep;
					if (step.indexOf("ajax.") > 0) {
						if (hs_inspectionTables == null) {
							hs_inspectionTables = jsondata;
						} else {
							for ( var t in jsondata) {
								hs_inspectionTables[t] = jsondata[t];
							}
						}
					} else {
						hs_inspectionTables = jsondata;
					}
					if (hs_inspectionTables.error != null) {
						hs_inspectionPreload.putMessage("ERROR: " + hs_inspectionTables.error);
					} else {
						if (hs_inspectionPreload.isDbServer == "true")
							hs_inspectionPreload.putMessage(step + " - complete" + " in [" + hs_inspectionTables.xmsecs + "] msecs");

					}
				} catch (e) {
					hs_inspectionPreload.putMessage("ERROR on loading hs_inspectionTables\n" + e);
				}
				hs_inspectionPreload.doNext();
			},
			error : function(type, data, evt) {
				if (hs_inspectionPreload.id2 != "")
					dojo.style(dojo.byId(hs_inspectionPreload.id2), "display", "none");
				hs_inspectionPreload.putMessage("An error occurred on ajaxload. " + type);
				hs_inspectionCSCode.waitPanelHide();
			}
		});
	} else if (this.cstep == "postCurrent") {
		var localparams = {
			"$$xspsubmitvalue" : this.cstep
		};
		console.log("doStep doing postCurrent to this.id:[" + this.id + "]");
		XSP.partialRefreshPost(this.id, {
			params : localparams,
			onComplete : function() {
				// alert("Refresh 1 inspectionPreLoadPanel
			// Completed");
			// dojo.style(dojo.byId('#{id:preloaderPanel1}'),
			// "display", "none");
			setTimeout( function() {
				hs_inspectionPreload.doNext();
			}, 1);
		},
		onError : function() {
			console.error("ERROR step failed:[" + hs_inspectionPreload.cstep + "]");
			alert("ERROR step failed:[" + hs_inspectionPreload.cstep + "]");
			hs_inspectionCSCode.waitPanelHide();
		}
		});
	} else {
		// Extend the timeout to a long time..
		var localparams = {
			"$$xspsubmitvalue" : this.cstep
		};
		if ("followUpClear" == this.cstep) {
			localparams.srcunid = hs_inspectionCSCode.followUpUnid;
		}
		XSP.submitLatency = 75 * 1000;
		XSP.partialRefreshGet(this.id, {
			params : localparams,
			onComplete : function() {
				// alert("Refresh 1 inspectionPreLoadPanel
			// Completed");
			// dojo.style(dojo.byId('#{id:preloaderPanel1}'),
			// "display", "none");
			hs_inspectionPreload.doNext();
		},
		onError : function() {
			alert("Refresh 1 inspectionPreLoadPanel step[" + hs_inspectionPreload.cstep + "] ERROR");
			hs_inspectionCSCode.waitPanelHide();
		}
		});
	}
} catch (e) {
	alert("ERROR with partialRefreshes " + this.cstep + "\n" + e.toString());
}
},

doNext : function() {
var nstep = "";
for ( var i = 0; i < this.steps.length; i++) {
	if (this.steps[i] == this.cstep) {
		nstep = (i + 1 == this.steps.length) ? "" : this.steps[i + 1];
	}
}
if (nstep == "" && this.doedit) {
	nstep = "loaddoc";
	this.doedit = false;
}
if (nstep == "") {
	if (this.id2 != "")
		dojo.style(dojo.byId(this.id2), "display", "none");
	if (this.isDbServer == "true") {
		this.putMessage("Sections:" + hs_inspectionTables.sectionDataSize);
		this.putMessage("Violations:" + hs_inspectionTables.violationDataSize);
		this.putMessage("Observations:" + hs_inspectionTables.observationDataSize);
		this.putMessage("holding on debug server.");
		hs_inspectionCSCode.waitPanelHide();
	} else {
		setTimeout( function() {
			hs_inspectionCSCode.switchToViolEditTabFinalStep();
		}, 50);
	}
} else {
	this.doStep(nstep);
}
},

msgs : [],
mid : "",
sdate : null,

putMessage : function(msg) {
if (this.sdate == null) {
	this.sdate = new Date();
	this.msgs = [];
}
var msecs = new Date().getTime() - this.sdate.getTime();
var omsg = "[" + msecs + "]: " + msg;
// omsg += " [" + this.vmodule + "]";
		hs_utils.debug(omsg);
		this.msgs[this.msgs.length] = omsg;
		dojo.byId(this.mid).innerHTML = this.msgs.join("<br />");
	}
	};

if (!hs_inspectionCSCode) {
	var hs_inspectionCSCode = {
		inspShowViolationOverview : "",
		editOrNew : "",
		editValues : {},
		followUpUnid : "",
		getActionTaken : "",
		showInOut : "",
		sections : {},
		isFromInspLoad : false,

		id_cbs : [],
		id_iobp : "",
		id_status : "",
		id_critical : "",
		id_observe : "",
		id_nextObsButtons : [],
		id_nextObsPanel : "",
		id_corrAct : "",
		id_corrSet : "",
		id_corrSetPanel : "",
		id_corrByDate : "",
		id_legalText : "",
		id_sectionDetail : "",
		id_topEditPanel : "",
		id_topLoadPanel : "",
		id_violLoaderDataUpdate : "",
		id_violDisplay : "",
		id_violLoadMsgPanel : "",
		id_inspActionRead : "",

		ids : {
			actionLinks : {},
			ayr : "",
			certifiedmanager : "",
			chokingposter : "",
			comments : "",
			criticalpanel : "",
			eho : "",
			enforcement : "",
			facname : "",
			factype : "",
			foodsafe : "",
			fureq : "",
			inspdatestr : "",
			licensedisplayed : "",
			messages : "",
			messagesPanel : "",
			nidatestr : "",
			observedseating : "",
			parentunid : "",
			riskrating : "",
			savecf1 : "",
			smoking : "",
			statuspanel : "",
			type : "",
			violmodule : "",
			waitimage : "",
			waitpanel : ""
		},
		idbtns : {
			save : "",
			savenew : "",
			clear : "",
			cancel : "",
			violCorrectedSetOffButton : "",
			violCorrectedSetOnButton : ""
		},
		insp : {
			ayr : "",
			certifiedmanager : "",
			chokingposter : "",
			eho : "",
			enforcement : "",
			facname : "",
			factype : "",
			fureq : "",
			inspdatestr : "",
			licensedisplayed : "",
			nidatestr : "",
			observedseating : "",
			parentunid : "",
			riskrating : "",
			smoking : "",
			type : "",
			violmodule : [ "" ]
		},

		// Settings from the MasterSetting Profile Document
		ms_codeStartDate : "",
		ms_dbDesign : "",
		ms_useCodes : "",

		plcats : [],
		plgroups : [],
		plsections : [],
		plviolations : [],
		plobserve : [],
		plcorrect : [],
		pllegal : [],

		saveSuccess : false,

		useInOut : false,
		
		//for error messages adn debugging
		username : '',
		facilityname : '',
		
		vgroup : "",
		vcategory : "",
		vsnum : "",
		vsdesc : "",
		vinout : "",
		vstatus : "",
		vcode : "",
		vdescription : "",
		vcritical : "",
		vobserve : "",
		vcorrect : "",
		vcorrectby : "",
		vcorrectedset : "",
		vhzrdrating : "",
		vlegaltext : "",
		vrepeat : "",

		checkSelect : function(id) {
			var dbugLoc = "[hs_inspectionCSCode.checkSelect] ";
			var localDebug = false;
			//var localDebug = (id == this.id_cbs[3]);

			// var cbwrk = dojo.byId(id);
			// var value = (cbwrk == null) ? "" : cbwrk.value;
			// var value = dojo.byId(id).value;
			var value = x$(id).val();
			if (localDebug) console.debug(dbugLoc + "id:[" + id + "]\n" + "typeof value:[" + typeof value + "]\n" + "value:[" + value + "]");

			if (value == "-select-")
				return "";
			if (value.indexOf("-select") > -1)
				return "";
			if (value.toLowerCase() == "not loaded")
				return "";
			if (value.toLowerCase() == "not loaded yet")
				return "";
			if (value.toLowerCase() == "not yet loaded")
				return "";
			return value;
		},

		updateValues : function() {
			var dbugLoc = "(hs_inspectionCSCode.updateValues): ";
			var localDebug = false;

			this.vgroup = this.checkSelect(this.id_cbs[0]);
			this.vcategory = this.checkSelect(this.id_cbs[1]);
			this.vsnum = this.checkSelect(this.id_cbs[2]);
			this.vcode = this.checkSelect(this.id_cbs[3]);
			if (localDebug)
				console.log(dbugLoc + "vgroup:[" + this.vgroup + "] " + "vcategory:[" + this.vcategory + "] " + "vsnum:[" + this.vsnum
						+ "] " + "vcode:[" + this.vcode + "] ");
			// TODO - Need to determine how this.vgroup changes after this point in the routine.
			
			this.vdescription = this.getViolDesc();
			if (localDebug)
				console.log(dbugLoc + "getViolDesc() OK vgroup:[" + this.vgroup + "]");
			this.vcorrect = x$(this.id_corrAct).val();
			this.vcorrectby = x$(this.id_corrByDate).val();
			if (localDebug)
				console.log(dbugLoc + "vcorrect:[" + this.vcorrect + "] vcorrectby:[" + this.vcorrectby + "] vgroup:[" + this.vgroup + "]");
			if (this.vcorrectby != "") {
				if (localDebug)
					console.log(dbugLoc + " vcorrectby is:[" + this.vcorrectby + "] vgroup:[" + this.vgroup + "]");
				// this.vcorrect += " Correct By: " +
				// this.formatDate(this.vcorrectby);
				this.vcorrect += "  Correct By: " + this.vcorrectby + ".";
				x$(this.id_corrAct).val(this.vcorrect);
				this.vcorrectby = "";
				x$(this.id_corrByDate).val("");
			}
			this.vobserve = x$(this.id_observe).val();
			this.vcorrectedset = this.getCorrectedSet();

			// this.vlegaltext = dojo.byId(this.id_legalText).value;
			if (this.vlegaltext == "") {
				this.vlegaltext = x$(this.id_legalText).html();
			}
			if (this.vcode != "") {
				if (this.vsnum == "") {
					if (localDebug)
						console.log(dbugLoc + " calling: updateGroupCatSectionFromCode vgroup:[" + this.vgroup + "]");
					this.isFromInspLoad = true;
					this.updateGroupCatSectionFromCode(this.vcode);
					this.isFromInspLoad = false;
				}
				if (this.vstatus != "Out") {
					this.vstatus = "Out";
					this.vinout = "Out";
					this.showStatus();
				}
			}
			if (localDebug) console.log(dbugLoc + " vgroup:[" + this.vgroup + "]");
		},

		formatDate : function(idt) {
			var wdt = new Date(idt);
			var odt = "";
			try {
				var wdtspl = idt.split("-");
				var months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
				var month = months[parseInt(wdtspl[1]) - 1];
				odt = wdtspl[2] + "-" + month + "-" + wdtspl[0];
			} catch (ex) {
				odt = wdt.toLocaleDateString();
			}
			return odt;
		},

		getCorrectedSet : function() {
			var csfld = dojo.byId(this.id_corrSet);
			if (csfld != null) {
				// this.vcorrectedset = (csfld.checked) ? "Corrected" : "";
				this.vcorrectedset = csfld.value;
				// hs_utils.debug("correctedSet is:[" + this.vcorrectedset +
				// "]",
				// "hs_inspectionCSCode.getCorrectedSet");
				// csfld.checked:[" + csfld.checked + "]",
				// "hs_inspectionCSCode.getCorrectedSet");
			}
			return this.vcorrectedset;
		},

		getFullId : function(id) {
			var wrk;
			for ( var i = 0; i < this.id_cbs.length; i++) {
				wrk = this.id_cbs[i].split(":");
				if (wrk[wrk.length - 1] == id)
					return this.id_cbs[i];
			}
			return "not:found:" + id;
		},

		getPlGroups : function() {
			var wlist = [ "-select group-|-select-" ];
			this.plgroups = wlist;
			if (hs_inspectionTables == null)
				return wlist;
			if (hs_inspectionTables.sectionData == null)
				return wlist;
			var sdata = hs_inspectionTables.sectionData;
			var grp = "";
			for ( var idx = 0; idx < sdata.length; idx++) {
				grp = unescape(sdata[idx].group);
				// grp += "|" + unescape(sdata[idx].group);
				if (!this.inList(grp, wlist)) {
					wlist[wlist.length] = grp;
				}
			}
			if (wlist.length == 2)
				wlist = [ wlist[1] ];
			this.plgroups = wlist;
			return wlist;
		},

		getPlCats : function(group) {
			if (!group)
				group = this.vgroup;
			var wlist = [ "-select category-|-select-" ];
			this.plcats = wlist;
			if (hs_inspectionTables == null)
				return wlist;
			if (hs_inspectionTables.sectionData == null)
				return wlist;
			if (group == "")
				return wlist;
			var sdata = hs_inspectionTables.sectionData;
			var cat = "";
			for ( var idx = 0; idx < sdata.length; idx++) {
				if (group == unescape(sdata[idx].group)) {
					cat = unescape(sdata[idx].category);
					if (!this.inList(cat, wlist)) {
						wlist[wlist.length] = cat;
					}
				}
			}
			if (wlist.length == 2)
				wlist = [ wlist[1] ];
			this.plcats = wlist;
			return wlist;
		},

		getPlSections : function(group, cat) {
			var msgContext = "hs_inspaectionCSCode.getPlSections: ";
			if (!group)
				group = this.vgroup;
			if (!cat)
				cat = this.vcategory;
			var wlist = [ "-select section-|-select-" ];
			this.plsections = wlist;
			if (hs_inspectionTables == null)
				return wlist;
			if (hs_inspectionTables.sectionData == null)
				return wlist;
			if (group == "")
				return wlist;
			if (cat == "")
				return wlist;
			var sdata = hs_inspectionTables.sectionData;
			var section = "";
			var addsection = false;
			for ( var idx = 0; idx < sdata.length; idx++) {
				if (group == unescape(sdata[idx].group) && cat == unescape(sdata[idx].category)) {
					section = unescape(sdata[idx].number) + " " + unescape(sdata[idx].description);
					section += "|" + unescape(sdata[idx].number);
					if (!this.inList(section, wlist)) {
						addsection = true;
						try {
							if (this.useInOut && !this.isFromInspLoad) {
								for ( var sidx = 0; sidx < this.sections.length; sidx++) {
									if (this.sections[sidx].number == unescape(sdata[idx].number)) {
										addsection = false;
										console.log(msgContext + "not including section:[" + section + "]")
									}
								}
							}
						} catch (e) {
							console.error(msgContext + e.toString());
						}
						if (addsection)
							wlist[wlist.length] = section;
					}
				}
			}
			if (wlist.length == 1) {
				wlist = [ "all sections in category are processed!|-select-" ];
			} else if (wlist.length == 2)
				wlist = [ wlist[1] ];
			this.plsections = wlist;
			return wlist;
		},

		getPlViolations : function(wgroup, wcat, wsnum) {
			var localDebug = false;
			var dbugLoc = "[hs_inspectionsCSCode.getPlViolations] ";
			
			if (!wgroup)
				wgroup = this.vgroup;
			if (!wcat)
				wcat = this.vcat;
			if (!wsnum)
				wsnum = this.vsnum;

			var donull = (wsnum == "") ? true : false;

			//var wlist = [ "-select violation-|-select-" ];
			var wlist = [];
			if (hs_inspectionTables == null)
				return wlist;
			if (hs_inspectionTables.sectionData == null)
				return wlist;
			var vdata = hs_inspectionTables.violationData;
			var violation = "";
			for ( var idx = 0; idx < vdata.length; idx++) {
				if (donull || wsnum == unescape(vdata[idx].number)) {
					violation = unescape(vdata[idx].code) + " : " + unescape(vdata[idx].description);
					violation += "|" + unescape(vdata[idx].code);
					if (!this.inList(violation, wlist)) {
						wlist[wlist.length] = violation;
					}
				}
			}
			if (wlist.length == 0 && wsnum != "")
				return [ "No violations in this section|No violation code" ];

			if(localDebug) console.debug(dbugLoc + "for group:[" + wgroup + "] cat:[" + wcat + "] snum:[" + wsnum + "] wlist.length:[" + wlist.length + "] wlist[0]:[" + wlist[0] + "]");
			if (wlist.length == 1)
				return [ wlist[0] ];
			// Sort it
			wlist.sort( function(a, b) {
				if (a > b) return 1;
				return -1;
			});
			var olist = [ "-select violation-|-select-" ];
			for(var idx2 = 0; idx2<wlist.length; idx2++) {
				olist.push(wlist[idx2]);
			}
			if(localDebug) console.debug(dbugLoc + "after sort olist.length:[" + olist.length + "] olist[0]:[" + olist[0] + "]");
			
			this.plviolations = olist;
			return olist;
		},

		getVsdesc : function() {
			if (this.vsnum == "")
				return "";

			var sdata = this.getVsdata();
			if(sdata != null) return unescape(sdata.description);
			/********
			var sdata = hs_inspectionTables.sectionData;
			var wgrp = "";
			var wcat = "";
			var wsnum = "";
			for ( var i = 0; i < sdata.length; i++) {
				wgrp = unescape(sdata[i].group);
				wcat = unescape(sdata[i].category);
				wsnum = unescape(sdata[i].number);
				if (wgrp == this.vgroup && wcat == this.vcategory && wsnum == this.vsnum) {
					return unescape(sdata[i].description);
				}
			}
			************/
			return "could not find description for " + this.vsnum;
		},
		
		getVsdata : function() {
			var dbugLoc = "(hs_inspectionCSCode.getVsdata): ";
			var localDebug = false;
			if (this.vsnum == "")
				return null;

			var sdata = hs_inspectionTables.sectionData;
			var wgrp = "";
			var wcat = "";
			var wsnum = "";
			for ( var i = 0; i < sdata.length; i++) {
				wgrp = unescape(sdata[i].group);
				wcat = unescape(sdata[i].category);
				wsnum = unescape(sdata[i].number);
				if (wgrp == this.vgroup && wcat == this.vcategory && wsnum == this.vsnum) {
					if(localDebug) console.log(dbugLoc + "found the section data for [" + wgrp + "." + wcat + "." + wsnum + "]");
					return sdata[i];
				}
			}
			console.log(dbugLoc +  "could not find description for:[" + this.vgroup + "." + this.vcategory + "." + this.vsnum + "]");
			return null;
		},

		getViolDesc : function() {
			var dbugLoc = "(hs_inspectionCSCode.getViolDesc): ";
			var localDebug = false;

			try {
				var wrkstr = "";
				if (this.vcode == "") {
					this.vcritical = "";
					this.showCritical();
					this.vdescription = "";
					if (localDebug)
						hs_utils.debug(dbugLoc + "1. for vcode:[" + this.vcode + "]" + " vcritical:[" + this.vcritical + "]");
					return wrkstr;
				}
			} catch (e) {
				console.error(dbugLoc + "1: " + e.toString());
			}
			var vdata = hs_inspectionTables.violationData;
			var wcode = "";
			try {
				for ( var i = 0; i < vdata.length; i++) {
					wcode = unescape(vdata[i].code);
					if (wcode == this.vcode) {
						this.vcritical = unescape(vdata[i].critical);
						this.showCritical();
						this.vdescription = unescape(vdata[i].description);
						if (localDebug)
							hs_utils.debug(dbugLoc + "2. for vcode:[" + this.vcode + "]" + " vcritical:[" + this.vcritical + "]");
						return this.vdescription;
					}
				}
			} catch (e) {
				console.error(dbugLoc + "2: " + e.toString());
			}
			try {
				if (localDebug)
					hs_utils.debug(dbugLoc + "3. for vcode:[" + this.vcode + "]" + " vcritical:[" + this.vcritical + "]");
				this.showCritical();
				return "could not find description for " + this.vcode;
			} catch (e) {
				console.error(dbugLoc + "3: " + e.toString());
			}
		},

		inList : function(val, list) {
			for ( var i = 0; i < list.length; i++) {
				if (list[i] == val)
					return true;
			}
			return false;
		},

		replacePicklist : function(plid, plist) {
			return this.replacePicklistDojo(plid, plist);
		},

		replacePicklistDojo : function(plid, plist) {
			var dbugLoc = "[hs_inspectionCSCode.replacePicklistDojo] ";
			var localDebug = false;
			// var localDebug = (plid == this.id_cbs[3]);

			if(localDebug) console.debug(dbugLoc + "plid:[" + plid + " plist.length:[" + plist.length + "] plist[0]:[" + plist[0] + "]")
			var pl = dojo.byId(plid);
			if (pl != null) {
				pl.options.length = 0;

				var newOpt = null;
				var plentry = [];
				for ( var idx = 0; idx < plist.length; idx++) {
					if (plist[idx].indexOf("|") > 0) {
						plentry = plist[idx].split("|");
					} else {
						plentry = [ plist[idx], plist[idx] ];
					}
					newOpt = document.createElement('option');
					newOpt.text = plentry[0];
					newOpt.value = plentry[1];
					newOpt.selected = (idx == 0);
					pl.add(newOpt, null);
				}
				// dojo.style(pl, "font-size", "medium");
				// dojo.style(pl, "width", this.getPlWidth(200));
			}
		},

		getPlWidth : function(labelSize) {
			var vs = dojo.window.getBox();
			if (!labelSize)
				labelSize = 200;
			var rslt = "" + (vs.w - labelSize) + "px";
			return rslt
		},

		setCBwidthsForInsp : function() {
			// this.setCBWidths( [ this.ids.eho, this.ids.type,
			// this.ids.enforcement ]);
			// this.setCBFonts( [ "eho", "type", "enforcement" ]);
			// this.setCBFonts( [ "foodsafe", "certifiedmanager",
			// "chokingposter", "smoking", "observedseating",
			// "licensedisplayed",
			// "nidatestr", "inspdatestr" ]);
		},

		setCBWidths : function(wrkids) {
			// var cb = null;
			// for ( var i = 0; i < wrkids.length; i++) {
			// cb = dojo.byId(wrkids[i]);
			// if (cb != null) {
			// dojo.style(cb, "width", this.getPlWidth(240));
			// }
			// }
		},

		setCBFonts : function(wrkids) {
			// var cb = null;
			// for ( var i = 0; i < wrkids.length; i++) {
			// cb = dojo.byId(this.ids[wrkids[i]]);
			// if (cb != null) {
			// dojo.style(cb, "font-size", "medium");
			// } else {
			// console.warn("[hs_inspectionCSCode.setCBFonts]: failed to find
			// control for id: [" + wrkids[i] + "]:["
			// + this.ids[wrkids[i]] + "]");
			// }
			// }
		},

		intializeViolation : function() {
			var dbugLoc = "(hs_inspectionCSCode.intializeViolation): ";
			var localDebug = false;

			if ("" == this.id_cbs[0]) {
				hs_utils.debug(dbugLoc + "client side IDs not found for combo boxes", dbugLoc);
				return;
			}
			var grpPl = dojo.byId(this.id_cbs[0]);
			try {
				if (grpPl != null) {
					var plist = this.getPlGroups();
					this.replacePicklist(this.id_cbs[0], plist);
					if(localDebug) console.debug(dbugLoc + "groups pl replaced. new length:[" + plist.length + "] vgroup:[" + this.vgroup + "]");
					if(localDebug) console.debug(dbugLoc + "plist is: [" + plist + "]");
					grpPl.selectedIndex = 0;
					if (plist.length == 1) {
						this.updateValues();
						plist = this.getPlCats();
						this.replacePicklist(this.id_cbs[1], plist);
						if(localDebug) console.debug(dbugLoc + "cats pl replaced. new length:[" + plist.length + "]")
						if (plist.length == 1) {
							plist = this.getPlSections();
							this.replacePicklist(this.id_cbs[2], plist);
							if(localDebug) console.debug(dbugLoc + "sections pl replaced. new length:[" + plist.length + "]")
							this.vsnum = "";
						} else {
							this.replacePicklist(this.id_cbs[2], [ "Not yet loaded" ]);
							if(localDebug) console.debug(dbugLoc + "sections pl not loaded. vgroup:[" + this.vgroup + "]")
							this.vcategory = "";
							this.vsnum = "";
						}
					} else {
						this.replacePicklist(this.id_cbs[1], [ "Not yet loaded" ]);
						this.replacePicklist(this.id_cbs[2], [ "Not yet loaded" ]);
						if(localDebug) console.debug(dbugLoc + "cats and sections pl not loaded. vgroup:[" + this.vgroup + "]")
						this.vcategory = "";
						this.vsnum = "";
					}
					plist = this.getPlViolations();
					this.replacePicklist(this.id_cbs[3], plist);
					if (localDebug) console.debug(dbugLoc + "violations pl replaced. new length:[" + plist.length + "] vgroup:[" + this.vgroup + "]")
					x$(this.id_violDisplay).html("");
					// Added 12 Feb 2014 to complete clearing screen
					this.setInOut("");
					if (localDebug) console.debug(dbugLoc + "setInOut OK. vgroup:[" + this.vgroup + "]")
					//x$(this.id_iobp).parent().hide();
					this.showIObp(false);
					this.vstatus = "";
					this.showStatus();
					if (localDebug) console.debug(dbugLoc + "showStatus OK. vgroup:[" + this.vgroup + "]")

					this.updateValues();
					if (localDebug) console.debug(dbugLoc + "updateValues OK. vgroup:[" + this.vgroup + "]")
					this.updateObservations();
					if (localDebug) console.debug(dbugLoc + "updateObservations OK. vgroup:[" + this.vgroup + "]")
					this.editOrNew = "new";
					this.editValues = {};

					this.setVcorrectedSet("");
					if (localDebug)
						console.debug(dbugLoc + "setVcorrectedSet OK. vgroup:[" + this.vgroup + "]")
					try {
						hs_violationSaver.showMessage("");
					} catch (e1) {
						console.log(dbugLoc + "clearing and Hiding violationMsgCF1 failed: " + e1.toString())
					}
					if (!this.useInOut) {
						// var frio =
						// dojo.byId(this.id_iobp).parentNode.parentNode;
						// with bootstrap we only have to go up one parent.
						//x$(this.id_iobp).parent().hide();
						// with bootstrap we do not have to go up to parents.
						//x$(this.id_status).hide();
						this.showIObp(false);
					}
				} else {
					hs_utils.debug(dbugLoc + " could not access combo box with id:[" + this.id_cbs[0] + "[ [hs_inspectionCSCode.id_cbs]")
				}
			} catch (e) {
				console.error(dbugLoc + e.toString());
			}
		},

		clearViolation : function() {
			var dbugLoc = "[hs_inspectionCSCode.clearViolation] ";
			var localDebug = false;
			try {
				this.vcategory = "";
				this.vcode = "";
				this.vcorrect = "";
				this.vcorrectby = "";
				this.vcorrectedset = "";
				this.vcritical = "";
				this.vdescription = "";
				this.vgroup = "";
				this.vhzrdrating = "";
				this.vinout = "";
				this.vlegaltext = "";
				this.vobserve = "";
				this.vrepeat = "";
				this.vsdesc = "";
				this.vsnum = "";
				this.vstatus = "";

				x$(this.id_violDisplay).html("");
				var plg = this.getPlGroups();
				this.replacePicklist(this.id_cbs[0], plg);
				if (plg.length == 0) {
					this.vgroup = plg[0];
				}
			} catch (e) {
				console.error(dbugLoc + "ERROR: " + e);
			}

		},

		intializeEditViolation : function() {
			var dbugLoc = "[hs_inspectionCSCode.intializeEditViolation]: ";
			var localDebug = false;
			try {
				this.clearViolation();
				if (localDebug)
					hs_utils.debug(
							"new vcode:["
									+ this.editValues.vcode
									+ "] "
									+
									// "new vsnum:[" + this.editValues.vsnum +
									// "] " +
									// "old vcode:[" + this.vcode + "] " +
									"old vcorrectedset:[" + this.vcorrectedset + "] " + "new vcorrectedset:["
									+ this.editValues.vcorrectedset + "]", dbugLoc);
				this.setVcode(this.editValues.vcode, true);
			} catch (e) {
				console.error(dbugLoc + "1: ERROR: " + e.toString());
			}
			try {
				this.setInOut(this.editValues.vinout);
				if (this.useInOut)
					//x$(this.id_iobp).show();
					this.showIObp(true);
			} catch (e) {
				console.error(dbugLoc + "2a: ERROR: " + e.toString());
			}
			try {
				this.setVcode(this.editValues.vcode, true); // yes it is called
															// twice
			} catch (e) {
				console.error(dbugLoc + "2b: ERROR: " + e.toString());
			}
			try {
				this.setVobserve(this.editValues.vobserve);
				this.setVcorrect(this.editValues.vcorrect);
				this.setVcorrectBy("");
				this.setVcorrectedSet(this.editValues.vcorrectedset);
				// this.setVlegalText(this.editValues.vlegaltxt);
				// relookup legal text
				this.setVlegalText(this.lookupLegalText());

				hs_violationSaver.showMessage("");
				this.showStatus(this.editValues.vstatus);
			} catch (e) {
				console.error(dbugLoc + "3: ERROR: " + e.toString());
			}
			try {
				if (!this.useInOut) {
					// with bootstrap we only have to go up one parent.
					//x$(this.id_iobp).parent().hide();
					// with bootstrap we do not have to go up to parents.
					//x$(this.id_status).hide();
					this.showIObp(false);
				}
				var btns = [ {
					id : this.idbtns.save,
					style : "inline-block"
				}, {
					id : this.idbtns.savenew,
					style : "none"
				}, {
					id : this.idbtns.clear,
					style : "none"
				}, {
					id : this.idbtns.cancel,
					style : "inline-block"
				} ];
				for ( var b = 0; b < btns.length; b++) {
					try {
						dojo.style(dojo.byId(btns[b].id), "display", btns[b].style);
					} catch (ex) {
						// console.error(dbugLoc + "ERROR on btns[" + b +
						// "].id:[" + btns[b].id + "]: " + e.toString());
					}
				}
			} catch (e) {
				console.error(dbugLoc + "4: ERROR: " + e.toString());
			}
		},

		changeSelection : function(sid, valueText, btn, inst) {
			var dbugLoc = "[hs_inspectionCSCode.changeSelection] ";
			var localDebug = false;

			try {
				valueText = this.checkSelect(this.getFullId(sid));
				btn = "setDojo";
				if (localDebug)
					console.log(dbugLoc + "sid:[" + sid + "]\n" + "full id:[" + this.getFullId(sid) + "]\n" + "valueText:[" + valueText
							+ "]\n" + "btn:[" + btn + "]");
				if (btn == 'cancel')
					return;
				var nid = "";
				this.updateValues();
				var spl = dojo.byId(this.getFullId(sid));
				if (spl != null) {
					var ids = this.id_cbs;
					if (sid == "violGroupCB1") {
						this.setVgroup(valueText);
					} else if (sid == "violCategoryCB1") {
						this.setVcategory(valueText);
					} else if (sid == "violSectionCB1") {
						this.setVsnum(valueText);
					} else if (sid == "violViolationCB1") {
						this.setVcode(valueText, true);
					} else {
						try {
							hs_utils.debug("for:[" + sid + "]\nsidbase: [" + this.getFullId(sid) + "]\n value:[" + valueText
									+ "doing nothing yet");
						} catch (e) {
						}
					}
					this.updateValues();

					// var iopanel = dojo.byId(this.id_iobp);
					if (this.vsnum == "") {
						// dojo.style(iopanel, "display", "none");
						//x$(this.id_iobp).parent().hide()
						this.showIObp(false);
					} else {
						if (this.useInOut) this.showIObp(true);
							// dojo.style(iopanel, "display", "block");
							//x$(this.id_iobp).parent().show();
							//x$(this.id_iobp).show();
					}

				} else {
					try {
						hs_utils.debug("could not access cbox with id:[" + sid + "]\n[" + this.getFullId(sid) + "]");
					} catch (e) {
					}
				}
			} catch (e) {
				hs_utils.errorHandler();
				// console.error(dbugLoc + e.toString());
			}
		},

		showIObp : function(toggle) {
			var dbugLoc = "[hs_inspectionCSCode.showIObp] ";
			var localDebug = false;
			if(toggle) {
				var sdata = this.getVsdata();
				if(sdata == null) {
					x$(this.id_iobp).parent().hide();
					x$(this.id_iobp).hide();
					return;
				}
				x$(this.id_iobp).parent().show();
				x$(this.id_iobp).show();
				if(localDebug) console.debug(dbugLoc + "showing buttons. - now need to hide buttons:[" + unescape(sdata.buttons) + "]");
				var btns = unescape(sdata.buttons);
				//if(btns == "") return;
				var wd = ["In~inAction", "out~outAction", "N/A~naAction", "N/O~noAction"];
				for (var wdi = 0; wdi < wd.length; wdi++) {
					var wdb = wd[wdi].split("~");
					var wdid = this.ids.actionLinks[wdb[1]];
					if(btns.indexOf(wdb[0]) > -1) {
						if(localDebug) console.debug(dbugLoc + "hiding button:[" + wdb[0] + "[ id:[" + wdid + "]");
						x$(wdid).hide();
					} else {
						if(localDebug) console.debug(dbugLoc + "showing button:[" + wdb[0] + "[ id:[" + wdid + "]");
						x$(wdid).show();
					}
				}
			} else {
				x$(this.id_iobp).parent().hide();
				x$(this.id_iobp).hide();
				if(localDebug) console.debug(dbugLoc + "hiding buttons.")
			}
		},
		
		setVgroup : function(valueText) {
			var dbugLoc = "[hs_inspectionCSCode.setVgroup] ";
			var localDebug = false;
			try {
				this.vgroup = valueText;
				var ids = this.id_cbs;
				var wcb = dojo.byId(ids[0]);
				if (wcb.options[0].value == "Not Loaded") {
					this.replacePicklist(ids[0], this.getPlGroups());
				}
				var sidx = wcb.selectedIndex;
				if (valueText != wcb.options[sidx].value) {
					for ( var i = 0; i < wcb.options.length; i++) {
						if (valueText == wcb.options[i].value) {
							wcb.selectedIndex = i;
							wcb.value = valueText;
						}
					}
				}

				this.replacePicklist(ids[1], this.getPlCats());
				this.replacePicklist(ids[2], this.getPlSections());
				this.replacePicklist(ids[3], this.getPlViolations());
				this.vcode = "";
				this.updateObservations();
				x$(this.id_violDisplay).html("");
			} catch (e) {
				console.error(dbugLoc + "ERROR: " + e);
			}
		},

		setVcategory : function(valueText) {
			var dbugLoc = "[hs_inspectionCSCode.setVcategory] ";
			var localDebug = false;

			this.vcategory = valueText;
			var ids = this.id_cbs;
			var wcb = dojo.byId(ids[1]);
			var sidx = wcb.selectedIndex;
			if (valueText != wcb.options[sidx].value) {
				for ( var i = 0; i < wcb.options.length; i++) {
					if (valueText == wcb.options[i].value) {
						wcb.selectedIndex = i;
						if (localDebug)
							hs_utils.debug(dbugLoc + "for [" + valueText + "] updated selected index from [" + sidx + "] to [" + i + "]");
						wcb.value = valueText;
					}
				}
			}

			var pls = this.getPlSections();
			this.replacePicklist(ids[2], pls);
			if (valueText != "" && pls.length == 1) {
				var onlyVsnum = pls[0].split("|")[1];
				if (localDebug)
					hs_utils.debug(dbugLoc + "for [" + valueText + "] plSections has [" + pls.length + "] elements - onlyVsnum:["
							+ onlyVsnum + "]");
				if (onlyVsnum != "-select-")
					this.setVsnum(onlyVsnum);
			} else {
				this.setVsnum("");
				// this.replacePicklist(ids[3], this.getPlViolations());
				// this.setVcode("");
			}
			this.updateObservations();
		},

		setVsnum : function(valueText) {
			var dbugLoc = "[hs_inspectionCSCode.setVsnum] ";
			var localDebug = false;

			this.vsnum = valueText;
			var ids = this.id_cbs;
			var wcb = dojo.byId(ids[2]);
			var sidx = wcb.selectedIndex;
			if (valueText != wcb.options[sidx].value) {
				for ( var i = 0; i < wcb.options.length; i++) {
					if (valueText == wcb.options[i].value) {
						wcb.selectedIndex = i;
						if (localDebug)
							hs_utils.debug(dbugLoc + "for [" + valueText + "] updated selected index from [" + sidx + "] to [" + i + "]");
						wcb.value = valueText;
					}
				}
			}

			this.vsdesc = this.getVsdesc();
			var plv = this.getPlViolations();
			this.replacePicklist(ids[3], plv);
			if (plv.length == 1) {
				if (localDebug)
					hs_utils.debug(dbugLoc + "for [" + valueText + "] plViolations has [" + plv.length + "] elements");
				this.setVcode(plv[0].split("|")[1]);
				// this.vdescription = this.getViolDesc();
			} else {
				this.vcode = "";
				x$(this.id_violDisplay).html("");
			}
			this.updateObservations();
		},

		setVcode : function(valueText, fromEditLoad) {
			var dbugLoc = "[hs_inspectionCSCode.setVcode] ";
			var localDebug = false;

			try {
				this.vcode = valueText;
				if (localDebug)
					console.log(dbugLoc + "updating code: " + valueText + " " + "this.vsnum:[" + this.vsnum + "] fromEditLoad:["
							+ fromEditLoad + "]");
				this.isFromInspLoad = fromEditLoad;
				var ids = this.id_cbs;
				var wcb = dojo.byId(ids[3]);
				if (wcb != null) {
					var sidx = wcb.selectedIndex;
					if (valueText != wcb.options[sidx].value) {
						for ( var i = 0; i < wcb.options.length; i++) {
							if (valueText == wcb.options[i].value) {
								wcb.selectedIndex = i;
								if (localDebug)
									hs_utils.debug(dbugLoc + "for [" + valueText + "] updated selected index from [" + sidx + "] to [" + i
											+ "]");
								wcb.value = valueText;
							}
						}
					}
					if (this.vsnum == "") {
						if (this.updateGroupCatSectionFromCode(valueText)) {

						} else {

						}
						wcb.value = valueText;
					}
					this.updateObservations();
				}
				if (localDebug)
					console.log(dbugLoc + "-completed run...");
				this.isFromInspLoad = false;
			} catch (e) {
				console.error(dbugLoc + e.toString());
			}
		},

		setVobserve : function(observe) {
			this.vobserve = observe;
			x$(this.id_observe).val(observe);
		},
		setVcorrect : function(correct) {
			this.vcorrect = correct;
			x$(this.id_corrAct).val(correct);
		},

		setVcorrectBy : function(correctBy) {
			this.vcorrectby = correctBy;
			x$(this.id_corrByDate).val(correctBy);
		},

		setVcorrectedSet : function(correctedSet, fromUI) {
			this.vcorrectedset = correctedSet;
			x$(this.id_corrSet).val(correctedSet);
			if ("Corrected" == correctedSet) {
				x$(this.idbtns.violCorrectedSetOnButton).removeClass("btn-default").addClass("btn-success");
				x$(this.idbtns.violCorrectedSetOffButton).removeClass("btn-danger").addClass("btn-default");
				if (fromUI && ("prompt" == this.getActionTaken)) {
					console.log("prompting for action taken.");
					// x$('#actionTakenModal').modal({show:true})
					x$(hs_actionTaken.modalid).modal( {
						show : true,
						keyboard : true
					});
				}
			} else {
				x$(this.idbtns.violCorrectedSetOnButton).removeClass("btn-success").addClass("btn-default");
				x$(this.idbtns.violCorrectedSetOffButton).removeClass("btn-default").addClass("btn-danger");
			}
		},

		setVlegalText : function(legaltext) {
			var dbarLoc = "hs-inspectionCSCode.setVlegalText";
			var localDebug = false;
			if (localDebug) {
				if (legaltext.indexOf("<br>") > -1) {
					hs_utils.debug("Legal text has line breaks WITHOUT slashes", dbarLoc);
				} else if (legaltext.indexOf("<br/>") > -1) {
					hs_utils.debug("Legal text has line breaks WITH slashes", dbarLoc);
				} else {
				}
			}
			this.vlegaltext = legaltext;
			x$(this.id_legalText).html(legaltext);
			// var lspan = dojo.byId(this.id_legalText);
			// lspan.innerHTML = legaltext;

			x$(this.id_violDisplay).html(this.vcode + ": " + this.getViolDesc());
			// lspan = dojo.byId(this.id_violDisplay);
			// lspan.innerHTML = this.vcode + ": " + this.getViolDesc();
			// dojo.style(lspan.parentNode, "padding-left", "15px");
		},

		lookupLegalText : function() {
			var odata = hs_inspectionTables.observationData;
			var ltL = [];

			for ( var i = 0; i < odata.length; i++) {
				if (this.vcode == unescape(odata[i].code)) {
					ltL[ltL.length] = unescape(odata[i].legalText);
				}
			}
			if (ltL.length == 0) {
				ltL[0] = "Unable to find legal text for code:[" + this.vcode + "]";
			}
			return ltL[0];
		},
		updateGroupCatSectionFromCode : function(code) {
			var dbarLoc = "(hs-inspectionCSCode.updateGroupCatSectionFromCode): ";
			var localDebug = false;
			var wsnum = "";
			var wsnum2 = "";
			var vdata = hs_inspectionTables.violationData;
			var sdata = hs_inspectionTables.sectionData;
			var wcode = "";
			var newgrp = "";
			var newcat = "";
			var newdesc = "";
			var newsnum = "";
			try {
				for ( var i = 0; i < vdata.length; i++) {
					wcode = unescape(vdata[i].code);

					if (wcode == code) {
						wsnum = unescape(vdata[i].number);
						newcat = unescape(vdata[i].category);
						newdesc = unescape(vdata[i].description);
						if (localDebug && wcode.indexOf("FL ") > -1) {
							hs_utils.debug("Found code:[" + code + "] with:[" + wcode + "] wsnum:[" + wsnum + "] category:[" + newcat
									+ "] desc:[" + newdesc + "i:[" + i + "]");
						}
						i = vdata.length;
					}
				}

				if (wsnum != "") {
					for ( var si = 0; si < sdata.length; si++) {
						wsnum2 = unescape(sdata[si].number);
						if (wsnum == wsnum2) {
							newgrp = unescape(sdata[si].group);
							newcat = unescape(sdata[si].category);
							newsnum = unescape(sdata[si].number);
							if (localDebug)
								hs_utils.debug(dbarLoc + "for code:[" + code + "] " + "newgrp:[" + newgrp + "] " + "newcat:[" + newcat
										+ "] " + "newsnum:[" + newsnum + "]");
							this.setVgroup(newgrp);
							this.setVcategory(newcat);
							this.setVsnum(newsnum);
							this.vcode = code;
							return true;
						}
					}
					hs_utils.debug(dbarLoc + "for code:[" + code + "] wsnum:[" + wsnum + "] section record not found!");
				} else if (newdesc != "") {
					// this.setVcategory(newcat);
					this.vcode = code;
					this.vdescription = this.getViolDesc();
					if (localDebug)
						hs_utils.debug(dbarLoc + "for code:[" + code + "] " + " updated description to:[" + this.vdescription + "] not:["
								+ newdesc + "]");
					return true;
				} else {
					hs_utils.debug(dbarLoc + "for code:[" + code + "] " + "violation record not found!");
				}
			} catch (e) {
				console.error(dbugLoc + e.toString());
			}
			return false;
		},

		getBaseId : function(id) {
			var idsplit = id.split(":");
			return idsplit[idsplit.length - 1];
		},

		updateObservations : function() {
			var dbugLoc = "(hs-inspectionCSCode.updateObservations): ";
			var localDebug = false;

			var odata = hs_inspectionTables.observationData;
			var opl = [];
			var cpl = [];
			var ltL = [];

			try {
				this.vdescription = this.getViolDesc();
			} catch (e) {
				console.error(dbugLoc + "1: " + e.toString());
			}

			var obspanel = dojo.byId(this.id_nextObsPanel);
			try {
				if (this.vcode == "") {
					this.plobserve = opl;
					this.plcorrect = cpl;
					this.pllegal = ltL;
					x$(this.id_observe).val("");
					x$(this.id_corrAct).val("");
					x$(this.id_corrByDate).val("");
					x$(this.id_legalText).html("");
					// with bootstrap we only have to go up one parent.
					x$(this.id_nextObsPanel).parent().hide();
					// dojo.style(obspanel.parentNode, "display", "none");
					x$(this.id_observe).hide();
					x$(this.id_observe).prev().hide();

					x$(this.id_corrAct).hide();
					x$(this.id_corrAct).prev().hide();

					x$(this.id_corrByDate).hide();
					x$(this.id_corrByDate).prev().hide();

					x$(this.id_corrSetPanel).hide();

					x$(this.id_legalText).hide();

					x$(this.ids.statuspanel).hide();
					x$(this.ids.criticalpanel).hide()

					return;
				}
			} catch (e) {
				console.error(dbugLoc + "2: " + e.toString());
			}
			try {
				for ( var i = 0; i < odata.length; i++) {
					if (this.vcode == unescape(odata[i].code)) {
						opl[opl.length] = unescape(odata[i].observation);
						cpl[cpl.length] = unescape(odata[i].correction);
						ltL[ltL.length] = unescape(odata[i].legalText);
					}
				}
			} catch (e) {
				console.error(dbugLoc + "3: " + e.toString());
			}
			try {
				if (opl.length == 0) {
					opl[0] = "Unable to find observation data for code:[" + this.vcode + "]";
					cpl[0] = "Unable to find corrective action data for code:[" + this.vcode + "]";
					ltL[0] = "Unable to find legal text for code:[" + this.vcode + "]";
				}
			} catch (e) {
				console.error(dbugLoc + "4: " + e.toString());
			}
			try {
				this.plobserve = opl;
				this.plcorrect = cpl;
				this.pllegal = ltL;
				this.setVobserve(opl[0]);
				this.setVcorrect(cpl[0]);
				this.setVlegalText(ltL[0]);
			} catch (e) {
				console.error(dbugLoc + "5: " + e.toString());
			}
			try {
				if (localDebug)
					console.log(dbugLoc + "opl.length:[" + opl.length + "]");
				if (opl.length == 1) {
					// with bootstrap we only have to go up one parent.
					x$(this.id_nextObsPanel).parent().hide();
				} else {
					// with bootstrap we only have to go up one parent.
					// x$(this.id_nextObsPanel).parent().show();
					x$(this.id_nextObsPanel).parent().css('display', 'block');
					for ( var nidx = 0; nidx < this.id_nextObsButtons.length; nidx++) {
						if (opl.length <= nidx) {
							this.toggleNextObservation("none", nidx + 1);
						} else {
							this.toggleNextObservation("inline", nidx + 1);
							if (localDebug)
								console.log(dbugLoc + "turning on obs button:[" + nidx + "]");
						}
					}
					this.nextObservation(this.id_nextObsButtons[0]);
				}
				x$(this.id_observe).show();
				x$(this.id_observe).prev().show();

				x$(this.id_corrAct).show();
				x$(this.id_corrAct).prev().show();

				x$(this.id_corrByDate).show();
				x$(this.id_corrByDate).prev().show();

				x$(this.id_corrSetPanel).show();
				x$(this.id_legalText).show();

				// x$(this.ids.statuspanel).show();
				x$(this.ids.criticalpanel).show();
			} catch (e) {
				console.error(dbugLoc + "6: " + e.toString());
			}
		},

		toggleNextObservation : function(whatToDo, bnum) {
			x$(this.id_nextObsButtons[bnum - 1]).css('display', whatToDo);
			// var button = dojo.byId(this.id_nextObsButtons[bnum - 1]);
			// if (this.useInOut) {
			// dojo.style(button, "display", whatToDo);
			// } else {
			// dojo.style(button, "display", "none");
			// }
		},

		nextObservation : function(id) {
			var idparts = id.split(":");
			var onum = parseInt(idparts[idparts.length - 2]);
			// try {
			// hs_utils.debug("nextObservation - id: [" + id + "] onum:[" + onum
			// + "]"
			// + " this.plobserve[onum-1]:[" + this.plobserve[onum] + "]");
			// } catch (e) {}
			dojo.byId(this.id_observe).value = this.plobserve[onum];
			dojo.byId(this.id_corrAct).value = this.plcorrect[onum];
			dojo.byId(this.id_corrByDate).value = "";
			var lspan = dojo.byId(this.id_legalText);
			lspan.innerHTML = this.pllegal[onum];
			dojo.style(lspan.parentNode, "padding-left", "15px");

			for ( var i = 0; i < this.id_nextObsButtons.length; i++) {
				x$(this.id_nextObsButtons[i]).removeClass('btn-success').removeClass('btn-primary').addClass('btn-default');
			}
			x$(this.id_nextObsButtons[onum]).removeClass('btn-default').addClass('btn-success');
		},

		setInOut : function(value) {
			var dbugLoc = "[hs_inspectionCSCode.setInOut] ";
			var localDebug = false;

			if (value != this.vinout) {// already set
				this.vinout = value;
				this.vstatus = value;
				try {
					this.replacePicklist(this.id_cbs[3], this.getPlViolations());
				} catch (e) {
					console.error(dbugLoc + "1: " + e.toString());
				}
				this.vcode = "";
				x$(this.id_violDisplay).html("");
				try {
					this.updateObservations();
				} catch (e) {
					console.error(dbugLoc + "2: " + e.toString());
				}
			} else {
				try {
					if (localDebug)
						hs_utils.debug(dbugLoc + "status already is:[" + value + "]");
				} catch (e) {
				}
			}
			this.showStatus();
		},

		showCritical : function() {
			var vc = this.vcritical.toLowerCase();

			// done Have to determine which set of values to show.
			var result = "";
			if (this.ms_useCodes == "Yes" && this.isAfterDate() && (this.ms_dbDesign != "Florida")) {
				result = (vc == "") ? "" : (vc == "no") ? "core"
						: (vc == "yes") ? "<span style='font-weight:bold;color:red'>Priority</span>"
								: (vc == "potential") ? "Priority foundation" : "Unkown value: [" + this.vcritical + "]";
			} else {
				result = (vc == "") ? "" : (vc == "no") ? "not critical"
						: (vc == "yes") ? "<span style='font-weight:bold;color:red'>Critical violation</span>" : "Unkown critical value: ["
								+ this.vcritical + "]" + " ms_useCodes:[" + this.ms_useCodes + "]" + " isAfterDate():["
								+ this.isAfterDate() + "]" + " ms_dbDesign:[" + this.ms_dbDesign + "]";
			}
			// dojo.byId(this.id_critical).innerHTML = result;
			x$(this.id_critical).html(result);
		},

		isAfterDate : function() {
			var dbugLoc = "(hs_inspectionCSCode.isAfterDate): ";
			var localDebug = false;
			// IsAfterDate := @If( @IsTime( CodeStartDate );
			// @If( InspectionDate >= CodeStartDate; @True; @False ); @False );
			if (this.ms_codeStartDate == "")
				return false;
			var csdt = null;
			var idt = null;
			var result = false;
			try {
				csdt = new Date(this.ms_codeStartDate);
				idt = new Date(this.insp.inspdatestr);
				result = (idt > csdt);
				if (localDebug)
					hs_utils.debug(dbugLoc + "codeStrartDate:[" + csdt.toDateString() + "] inspectionDate:[" + idt.toDateString()
							+ "] result:[" + result + "]")
				return result;
			} catch (ex) {
				return false;
			}

		},

		showStatus : function() {
			// dojo.byId(this.id_status).innerHTML = this.vstatus;
			x$(this.id_status).html(this.vstatus);
		},

		getSaveValues : function(sjob) {
			var dbugLoc = "(hs_inspectionCSCode.getSaveValues): ";
			var localDebug = false;

			var result = {
				saveJob : sjob,
				editOrNew : this.editOrNew,
				editIdx : (this.editOrNew == "edit") ? this.editValues.index : "",
				error : ""
			};
			this.updateValues();
			try {
				for ( var t in this) {
					if (t.charAt(0) == "v") {
						result["sj_" + t] = this[t];
						if (localDebug && t == "vcorrectedset") {
							hs_utils.debug(t + " : [" + this[t] + "]", dbugLoc)
						}
					}
				}
			} catch (e) {
				result.error = e;
			}
			return result;
		},

		signAfterSave : false,
		submitAfterSave : false,
		signURL : "",

		saveInspection : function(saveparms) {
			var localDebug = false;
			var dbarLoc = "[hs_inspectionCSCode.saveInspection] ";
			this.signAfterSave = false;
			this.submitAfterSave = false;
			if (saveparms) {
				if (saveparms.signit) {
					this.signAfterSave = true;
					if (localDebug)
						hs_utils.debug(dbarLoc + "saving and signing");
				}
				if (saveparms.submit) {
					this.submitAfterSave = true;
				}
				if (localDebug)
					hs_utils.debug(dbarLoc + "saving. submitAfterSave:[" + this.submitAfterSave + "]");
			}
			// var pldiv = dojo.byId( hs_inspectionPreload.id2);
			// var pos = dojo.style(pldiv, "position");
			// var top = dojo.style(pldiv, "top");
			// var left = dojo.style(pldiv, "left");
			// var width = dojo.style(pldiv, "width");
			// var height = dojo.style(pldiv, "height");
			// var zidx = dojo.style(pldiv, "z-index");
			// if(localDebug) hs_utils.debug("activating preload div:[" +
			// hs_inspectionPreload.id2 + "] position:[" + pos + "]" +
			// " top:[" + top + "] left:[" + left + "] width:[" + width + "]
			// height:[" + height + "] z-index:[" + zidx + "]");
			// dojo.style(pldiv, "display", "block");
			hs_inspectionCSCode.waitPanelShow();
			setTimeout( function() {
				hs_inspectionCSCode.saveInspectionStep2()
			}, 2);
		},

		saveInspectionStep2 : function() {
			var localDebug = false;
			var dbarLoc = "[hs_inspectionCSCode.saveInspectionStep2] ";

			var parms = {};
			parms.inspectionSaving = true;
			// parms.inspType = dojo.byId(this.ids.type).value;
			// parms.inspEho = dojo.byId(this.ids.eho).value;
			// var ifureq = dojo.byId(this.ids.fureq);
			// parms.inspFureq = (ifureq.selected) ? "Yes" : "No";
			// parms.inspNiDateStr = dojo.byId(this.ids.nidatestr).value;
			// parms.inspComments = dojo.byId(this.ids.comments).value;

			parms.signAfterSave = this.signAfterSave;
			parms.submitAfterSave = this.submitAfterSave;
			parms.valmode = 0; // Is this right

			XSP
					.partialRefreshPost(
							this.ids.savecf1,
							{
								params : parms,
								onComplete : function() {
									var localDebug = false;
									var dbarLoc = "[hs_inspectionCSCode.saveInspectionStep2.partialRefresh.onComplete] ";
									var msgdiv = dojo.byId(hs_inspectionCSCode.ids.messages);
									// with bootstrap we only have to go up one
									// parent.
									// dojo.style(msgdiv.parentNode, "display",
									// "block");
									x$(hs_inspectionCSCode.ids.messagesPanel).show();
									if (localDebug)
										console.log(dbarLoc + "showing messages panel:[" + hs_inspectionCSCode.ids.messagesPanel + "]")
									window.scrollTo(0, 0);
									try {
										var sdiv = dojo.byId(hs_inspectionCSCode.ids.savecf1);
										var smsgs = sdiv.innerHTML;
										if (localDebug)
											hs_utils.debug(smsgs, dbarLoc);
										var sok = (smsgs.indexOf("Inspection save failed.") > -1) ? false : (smsgs
												.indexOf("Inspection validation failed.") > -1) ? false : true;
										// $(msgdiv).addClass('panel-success').removeClass('panel-danger');
										msgdiv.innerHTML = smsgs;
										hs_inspectionCSCode.waitPanelHide();
										if (!sok) {
											if (localDebug)
												console.debug(dbarLoc + "Issues:" + smsgs);
										} else if (!hs_inspectionCSCode.submitAfterSave) {
											setTimeout( function() {
												x$(hs_inspectionCSCode.ids.messages).html("");
												x$(hs_inspectionCSCode.ids.messagesPanel).hide();
												console.log("submitAfterSave.setTimeoutFunc]: hiding messages panel:["
														+ hs_inspectionCSCode.ids.messagesPanel + "]")
											}, 4000);
										}
									} catch (e) {
										console.error(dbarLoc + e.toString());
										var emsg = x$(hs_inspectionCSCode.ids.messages).html();
										emsg += "<br/>ERROR: " + dbarLoc + e.toString();
										x$(hs_inspectionCSCode.ids.messages).html(emsg);
										x$(hs_inspectionCSCode.ids.messagesPanel).show();
										console.log("showing messages panel:[" + hs_inspectionCSCode.ids.messagesPanel + "]")

										hs_inspectionCSCode.waitPanelHide();
									}
									window.scrollTo(0, 0);
								},
								onError : 'hs_inspectionCSCode.saveInspectionErrorHandler( arguments[0], arguments[1], ["hs_inspectionCSCode.saveInspectionStep2"] )'
							})
		},

		saveComplete : function(submitAfterSave) {
			var localDebug = false;
			var dbarLoc = "[hs_inspectionCSCode.saveComplete] ";
			// var msgdiv = dojo.byId(this.ids.messages);
			// with bootstrap we only have to go up one parent.
			// dojo.style(msgdiv.parentNode, "display", "block");
			x$(this.ids.messagesPanel).show();
			if (localDebug)
				console.log(dbarLoc + "showing messages panel:[" + hs_inspectionCSCode.ids.messagesPanel + "]")
			window.scrollTo(0, 0);
			try {
				var smsgs = x$(hs_inspectionCSCode.ids.savecf1).html();
				if (localDebug)
					hs_utils.debug(smsgs, dbarLoc);
				var sok = (smsgs.indexOf("Inspection save failed.") > -1) ? false
						: (smsgs.indexOf("Inspection validation failed.") > -1) ? false : true;
				// $(msgdiv).addClass('panel-success').removeClass('panel-danger');
				x$(this.ids.messages).html(smsgs);
				hs_inspectionCSCode.waitPanelHide();
				if (!sok) {
					if (localDebug)
						console.debug(dbarLoc + "Issues:" + smsgs);
				} else if (!submitAfterSave) {
					setTimeout( function() {
						x$(hs_inspectionCSCode.ids.messages).html("");
						x$(hs_inspectionCSCode.ids.messagesPanel).hide();
						console.log("submitAfterSave.setTimeoutFunc]: hiding messages panel:[" + hs_inspectionCSCode.ids.messagesPanel
								+ "]")
					}, 4000);
				}
			} catch (e) {
				console.error(dbarLoc + e.toString());
				var emsg = x$(hs_inspectionCSCode.ids.messages).html();
				emsg += "<br/>ERROR: " + dbarLoc + e.toString();
				x$(hs_inspectionCSCode.ids.messages).html(emsg);
				x$(hs_inspectionCSCode.ids.messagesPanel).show();
				console.log("showing messages panel:[" + hs_inspectionCSCode.ids.messagesPanel + "]")

				hs_inspectionCSCode.waitPanelHide();
			}
			window.scrollTo(0, 0);
		},

		saveInspectionErrorHtml : '',

		saveInspectionErrorHandler : function() {
			var dbarLoc = "[hs_inspectionCSCode.saveInspectionErrorHandler] ";
			hs_utils.debug("Error on post to [" + hs_inspectionCSCode.ids.savecf1 + "]", dbarLoc);
			
			window.scrollTo(0, 0);
			
			try {
				hs_errorHandler.msgdivid = hs_inspectionCSCode.ids.messages;
				hs_errorHandler.otherData = 'Notes user: ' + hs_inspectionCSCode.username
				hs_errorHandler.otherData += '<br/>Facility name: ' + hs_inspectionCSCode.facilityname;
				hs_errorHandler.otherData += '<br/>Timeout minutes: ' + hs_inspectionCSCode.timeout.elapsedMinutes;
				hs_errorHandler.otherData += '<br/>Autosave triggered: ' + hs_inspectionCSCode.timeout.autosaveTriggered;
				hs_errorHandler.error(arguments[0], arguments[1], 'save of inspection!');
			} catch(e) {
				console.error(dbarLoc + "Error during hs_errorHandler.error: " + e.toString());
			}
			x$(hs_inspectionCSCode.ids.messagesPanel).show();
			
			/*********
			var msgdiv = dojo.byId(hs_inspectionCSCode.ids.messages);
			// with bootstrap we only have to go up one parent.
			x$(hs_inspectionCSCode.ids.messagesPanel).show();
			console.log(dbarLoc + "showing messages panel:[" + hs_inspectionCSCode.ids.messagesPanel + "]")
			if (hs_inspectionPreload.id2 != "")
				x$(hs_inspectionPreload.id2).hide();
			this.saveInspectionErrorHtml = arguments[0].responseText;
			// msgdiv.innerHTML = "<h3 style='color:red;'>" + "Error on post to
			// [" + hs_inspectionCSCode.ids.savecf1 + "]</h3><br/>" + txt
			var msghtml = "<h3 class='text-danger'>" + "Error occured during save of inspection!</h3>";
			if (this.saveInspectionErrorHtml.indexOf("This XPage must be called") > -1) {
				msghtml += ""
			}
			msghtml += "<a href='javascript:hs_inspectionCSCode.saveInspectionErrorShow()'>show error detail</a>";

			msgdiv.innerHTML = msghtml;
			hs_utils.errorHandler(arguments[0], arguments[1], arguments[2]);
			************/
		},

		saveInspectionErrorShow : function() {
			// var dbarLoc = "[hs_inspectionCSCode.saveInspectionErrorShow] ";
			// hs_utils.debug("posting out the response error.", dbarLoc);
			document.write(hs_inspectionCSCode.saveInspectionErrorHtml);
			document.close();
		},

		tempFldNextId : "",
		tempFldPositioner : function(callerid, dfltNextId) {
			var localDebug = false;
			var dbugLoc = "[hs_inspectionCSCode.tempFldPositioner] ";
			var cidspl = null;
			var widx = -1;
			var newid = "";
			var newfld = null;
			try {
				cidspl = callerid.split(":");
				widx = parseInt(cidspl[cidspl.length - 2]);
				cidspl[cidspl.length - 2] = widx + 1
				newid = cidspl.join(":");
				newfld = dojo.byId(newid);
				if (newfld != null) {
					if (localDebug)
						hs_utils.debug(dbugLoc + " got widx:[" + widx + "] newid:[" + newid + "] field exisits!");
					this.tempFldNextId = newid;
					setTimeout( function() {
						var tfld = dojo.byId(hs_inspectionCSCode.tempFldNextId);
						if (tfld != null)
							tfld.focus();
						hs_inspectionCSCode.tempFldNextId = "";
					}, 2);
				} else {
					if (localDebug)
						hs_utils.debug(dbugLoc + " got widx:[" + widx + "] newid:[" + newid + "] no field!");
					this.tempFldNextId = dfltNextId;
					setTimeout( function() {
						var tfld = dojo.byId(hs_inspectionCSCode.tempFldNextId);
						if (tfld != null)
							tfld.focus();
						hs_inspectionCSCode.tempFldNextId = "";
					}, 2);
				}
			} catch (e) {
				console.error(dbugLoc + " ERROR: " + e.toString());
			}
			// if (localDebug) hs_utils.debug(dbugLoc + " got fname:[" + fname +
			// "] widx:[" + widx + "]");
		},

		waitPanelHide : function() {
			// var waitPanel = dojo.byId(this.ids.waitpanel);
			// dojo.style(waitPanel, "display", "none");
			x$(this.ids.waitpanel).modal('hide');
			this.timeout.startTimeout();
		},

		waitPanelShow : function() {
			// var waitPanel = dojo.byId(this.ids.waitpanel);
			// dojo.style(waitPanel, "display", "block");
			x$(this.ids.waitpanel).modal();
			this.timeout.clearTimeout();
			setTimeout( function() {
				hs_splashPositioner.setpos(hs_inspectionCSCode.ids.waitimage);
			}, 2);

		},

		switchMode : "",
		switchVnum : "",

		switchToViolEditTab : function(mode, vnum) {
			// Do not go to again if already selected
			if (hs_inspDivs.toShow == "violedit")
				return;

			this.switchMode = mode;
			this.switchVnum = vnum;

			hs_inspectionCSCode.waitPanelShow();
			hs_inspectionCSCode.editOrNew = mode;
			if (hs_inspectionTables != null) {
				hs_inspDivs.show("violedit", function() {
					x$(hs_inspectionCSCode.id_topEditPanel).show();
					x$(hs_inspectionCSCode.id_topLoadPanel).hide();
					hs_inspectionCSCode.switchToViolEditTabFinalStep();
				});
			} else {
				x$(hs_inspectionCSCode.id_topEditPanel).toggle();
				x$(hs_inspectionCSCode.id_topLoadPanel).toggle();
				console.debug("loading the hs_inspectionTables");
				hs_inspDivs.show("violedit", function() {
					hs_inspectionPreload.mid = hs_inspectionCSCode.id_violLoadMsgPanel;
					hs_inspectionPreload.putMessage("Loading the violation tables...");

					// console.debug("calling hs_inspectionPreload.doFirst.");
						hs_inspectionPreload.doFirst(hs_inspectionCSCode.editOrNew == "edit");
					});
			}
		},

		switchToViolEditTabStep2 : function() {
			hs_inspDivs.show("violedit", function() {
				hs_inspectionPreload.mid = hs_inspectionCSCode.id_violLoadMsgPanel;
				hs_inspectionPreload.putMessage("Loading the violation tables...");

				hs_inspectionPreload.doFirst(hs_inspectionCSCode.editOrNew == "edit");
			});
		},
		switchToViolEditTabFinalStep : function() {
			XSP.partialRefreshPost(hs_inspectionCSCode.id_violLoaderDataUpdate, {
				onComplete : function() {
					try {
						hs_inspectionCSCode.editOrNew = hs_inspectionCSCode.switchMode;
						if ("new" == hs_inspectionCSCode.switchMode) {
							hs_inspectionCSCode.intializeViolation();
						} else {
							hs_inspectionCSCode.intializeEditViolation();
						}
					} catch (e) {
						console.error("Error during switchToViolEditFinalStep.onComplete:" + e.toString());
						console.debug("hs_inspectionCSCode.switchMode:[" + hs_inspectionCSCode.switchMode + "]");
					}
					setTimeout( function() {
						x$(hs_inspectionCSCode.id_topEditPanel).show();
						x$(hs_inspectionCSCode.id_topLoadPanel).hide();
						// x$(hs_inspDivs.btnids.violedit).addClass("active");
							hs_inspectionCSCode.waitPanelHide();
						}, 10)
				},
				onError : function() {
					console.error("Error refreshing: [" + hs_inspectionCSCode.id_violLoaderDataUpdate + "]");
				}
			});
		},

		markStatusOnCompleteStatus : "",
		
		markStatusOnComplete : function(status) {
			this.markStatusOnCompleteStatus = status;
			XSP.partialRefreshPost(hs_inspectionCSCode.id_sectionDetail, {
				onComplete : function() {
					setTimeout( function() {
						var dbugLoc = "(hs_inspectionCSCode.markStatusOnComplete.RefreshOnComplete): ";
						var localDebug = false;
						
						var msg = 'Section ' + hs_inspectionCSCode.vsnum + " " + hs_inspectionCSCode.vsdesc + ' marked as <strong>'
								+ hs_inspectionCSCode.markStatusOnCompleteStatus + '</strong>.';
						if (localDebug) console.log(dbugLoc + msg)
						hs_violationSaver.showMessage(msg, 'success');
						hs_inspectionCSCode.setVsnum('');
						hs_inspectionCSCode.isFromInspLoad = false;
						var pls = hs_inspectionCSCode.getPlSections();
						hs_inspectionCSCode.replacePicklist(hs_inspectionCSCode.id_cbs[2], pls);
						if (pls.length == 1) {
							var onlyVsnum = pls[0].split("|")[1];
							if (localDebug)
								console.debug(dbugLoc + "plSections has [" + pls.length + "] elements - onlyVsnum:["
										+ onlyVsnum + "]");
							if (onlyVsnum != "-select-") {
								hs_inspectionCSCode.setVsnum(onlyVsnum);
								hs_inspectionCSCode.showIObp(true);
							} else {
								hs_inspectionCSCode.showIObp(false);
							}
						} else {
							hs_inspectionCSCode.setVsnum("");
							hs_inspectionCSCode.showIObp(false);
							// this.replacePicklist(ids[3], this.getPlViolations());
							// this.setVcode("");
						}
						
						
						setTimeout( function() {
							hs_violationSaver.showMessage("");
						}, 5000);
					}, 2);
				},
				onError : function() {
					var msg = "error in refreshing:[" + hs_inspectionCSCode.id_sectionDetail + "]";
					console.error(msg);
					// alert(msg);
					hs_violationSaver.showMessage(msg, 'danger');
				}
			});
		},

		timeout : {
			savebtnid : "",
			mins : 0,
			elapsedMinutes : 0,
			autosaveTriggered : false,
			startDt : new Date(),
			toval : 30000,
			to : null,

			checkTimeout : function() {
				var localDebug = false;
				var dbugLoc = "[hs_inspectionCSCode.timeout.checkTimeOut]: ";

				var cdt = new Date();
				var emins = (cdt.getTime() - this.startDt.getTime()) / 1000 / 60;
				this.elapsedMinutes = emins;
				if (localDebug)
					console.log(dbugLoc + "checking timeout [" + this.mins + "]" + " since [" + this.startDt.toLocaleTimeString() + "]"
							+ " now [" + cdt.toTimeString() + "]" + " emins:[" + emins + "]");

				if (emins > this.mins) {
					this.startDt = new Date();
					console.log("firing the save: id:[" + this.savebtnid + "]");
					this.autosaveTriggered = true;
					this.to = setTimeout( function() {
						// x$(hs_inspectionCSCode.timeout.savebtnid).trigger(
						// "click" );
							document.getElementById(hs_inspectionCSCode.timeout.savebtnid).click();
						}, 10);
					return;
				}
				this.autosaveTriggered = false;
				if (this.mins > 0) {
					this.to = setTimeout( function() {
						hs_inspectionCSCode.timeout.checkTimeout();
					}, this.toval);
				}
			},

			clearTimeout : function() {
				if (this.to != null) {
					window.clearTimeout(this.to);
				}
				this.to = null;
			},

			startTimeout : function() {
				var localDebug = false;
				var dbugLoc = "[hs_inspectionCSCode.timeout.startTimeOut]: ";

				if (this.mins == 0) {
					console.log(dbugLoc + "no editing timeout set")
					return;
				}
				this.startDt = new Date();
				this.to = setTimeout( function() {
					hs_inspectionCSCode.timeout.checkTimeout()
				}, this.toval);
				console.log(dbugLoc + "editing timeout set to:[" + this.mins + "] minutes.")
			}
		}
	};
}

if (!hs_violationSaver) {
	var hs_violationSaver = {
		action : "",
		params : {},
		violationMsgCf1Id : "",
		violationSaveId : "",
		violationMessagesId : "",
		
		saveParms : {
			v2 : false,
			saveinsp : false,
			submitinsp : false
		},

		save : function(wtd, sparms) {
			var dbugLoc = "[hs_violationSaver.saveAndClose]: ";
			hs_inspectionCSCode.saveSuccess = false;

			this.action = wtd;
			this.saveParms = (sparms) ? sparms : {
				v2 : false,
				saveinsp : false,
				submitinsp : false
			};
			if (!this.validate())
				return;

			if (this.saveParms.v2) {
				hs_inspectionCSCode.waitPanelShow();
				setTimeout( function() {
					hs_violationSaver.postSaveToXSP();
				}, 5);
			} else {
				console.error(dbugloc + " v1 save no longer supported");
				return;
			}
		},

		postSaveToXSP : function() {
			var dbugLoc = "[hs_violationSaver.postSaveToXSP]: ";
			XSP.partialRefreshPost(this.violationSaveId, {
				params : this.params,
				onComplete : function() {
					hs_violationSaver.saveOnComplete();
				},
				onError : 'hs_violationSaver.saveOnError( arguments[0], arguments[1], ["Violation Save/postSaveToXSP"] )'
			})
		},

		saveOnComplete : function() {
			var dbugLoc = "[hs_violationSaver.saveOnComplete]: ";
			var localDebug = false;
			if (localDebug)
				console.debug(dbugLoc + "hs_inspectionCSCode.saveSuccess:[" + hs_inspectionCSCode.saveSuccess + "]");
			if (hs_inspectionCSCode.saveSuccess) {
				if (!hs_violationSaver.saveParms.v2) {
					hs_inspectionCSCode.waitPanelHide();
				}
				if (hs_violationSaver.action == "saveAndClose") {
					if (localDebug)
						console.debug(dbugLoc + "completed post to " + hs_violationSaver.violationSaveId
								+ " - moving to frmCreateInspection");
					if (hs_violationSaver.saveParms.v2) {
						setTimeout( function() {
							hs_violationSaver.afterSaveUpdate();
						}, 20);
					} else {
						location = "#frmCreateInspection";
					}
				} else {
					if (localDebug)
						console.debug(dbugLoc + "completed post to " + hs_violationSaver.violationSaveId
								+ " - clearing the violation.");
					try {
						hs_inspectionCSCode.intializeViolation();
						if (hs_violationSaver.saveParms.v2) {
							hs_inspectionCSCode.waitPanelHide();
						} else {
							hs_inspectionCSCode.waitPanelHide();
						}
					} catch (e) {
						//console.error(dbugLoc + "ERROR: " + e);
						msg = dbugLoc + "Violation Save failed. Please contact support! [" + hs_inspectionCSCode.saveSuccess + "] ID:2033<br/>" + e.toString();
						console.debug(msg);
						hs_inspectionCSCode.waitPanelHide();
						hs_violationSaver.showMessage(msg, danger);
					}
				}
			} else {
				msg = dbugLoc + "Violation Save failed. Please contact support! [" + hs_inspectionCSCode.saveSuccess + "] ID:2036";
				console.debug(msg);
				hs_inspectionCSCode.waitPanelHide();
				hs_violationSaver.showMessage(msg, danger);
			}
		},
		
		saveOnError : function (arg0, arg1, arg2) {
			hs_errorHandler.msgdivid = this.violationMessagesId;
			hs_errorHandler.otherData = "Refreshing id: " + this.violationSaveId;
			hs_inspectionCSCode.waitPanelHide();
			hs_errorHandler.error(arg0, arg1, arg2, {noTextClass : true});
		},
		
		afterSaveUpdate : function() {
			XSP.partialRefreshPost(hs_inspDivs.ids.violations, {
				onComplete : function() {
					setTimeout( function() {
						hs_violationSaver.afterSaveUpdateLink();
					}, 1);
				},
				onError : function () {
					//'hs_utils.errorHandler( arguments[0], arguments[1], ["hs_violationSaveer.afterSaveUpdate"] )';
					hs_errorHandler.msgdivid = hs_violationSaver.violationMessagesId;
					hs_errorHandler.error(arguments[0], arguments[1], "Violation Save/afterSaveUpdate");
				}
			})
		},

		afterSaveUpdateLink : function() {
			XSP.partialRefreshPost(hs_inspDivs.btnids.violations, {
				onComplete : function() {
					setTimeout( function() {
						hs_violationSaver.afterSaveShowViolations();
					}, 1);
				},
				onError : 'hs_utils.errorHandler( arguments[0], arguments[1], ["hs_violationSaveer.afterSaveUpdate"] )'
			})
		},

		afterSaveShowViolations : function() {
			hs_inspDivs.show("violations", function() {
				if (hs_violationSaver.saveParms.saveinsp) {
					hs_violationSaver.afterViolSaveDoInspectionSave();
				} else {
					hs_inspectionCSCode.waitPanelHide()
				}
			});
		},

		afterViolSaveDoInspectionSave : function() {
			console.debug("now doing the save of the inspection.");
			hs_inspDivs.show("main", function() {
				console.debug("calling hs_inspectionCSCode.saveInspection;");
				hs_inspectionCSCode.saveInspection( {
					submit : hs_violationSaver.saveParms.submitinsp
				});
			});
		},

		validate : function() {
			var dbugLoc = "[hs_violationSaver.validate]: ";
			var localDebug = false;
			
			try {
				this.params = hs_inspectionCSCode.getSaveValues(this.action);
				hs_violationSaver.showMessage("");
				try {
					if (localDebug)
						hs_utils.debug(dbugLoc + "vcode:[" + this.params.sj_vcode + "] status:[" + this.params.sj_vstatus + "]");
				} catch (e) {
				}

				if (this.params.sj_vcode == "" && (this.params.sj_vstatus == "" || this.params.sj_vstatus == "Out")) {
					if (localDebug)
						console.error(dbugLoc + "ERROR: violation code and status have not been selected.");
					this.showMessage("Please select violation code and status before saving the violation.");
					return false;
				}
				return true;
			} catch (e) {
				console.error(dbugLoc + "ERROR: " + e.toString());
				this.showMessage("An error occured in hs_violationSaver.validate:<br/>" + e.toString());
				return false;
			}
		},
		
		showMessage : function (msg, txtClass) {
			x$(this.violationMsgCf1Id).html(msg);
			if(msg == "") {
				x$(this.violationMsgCf1Id).parent().hide();
			} else {
				x$(this.violationMsgCf1Id).parent().removeClass('bg-danger').removeClass('bg-success');
				var nclass = (txtClass) ? 'bg-' + txtClass : 'bg-danger';
				x$(this.violationMsgCf1Id).parent().addClass(nclass).show();
			}
		}
	}
}

if (!hs_splashPositioner) {
	var hs_splashPositioner = {
		localDebug : false,
		imageDivId : "",

		setpos : function(inpImageDivId) {
			if (inpImageDivId)
				this.imageDivId = inpImageDivId;
			if (this.imageDivId == "") {
				console.error("hs_splashPositioner.setpos called without proper imageDivId.[" + inpImageDivId + "]");
				return;
			}
			var splashImage = dojo.byId(this.imageDivId);
			var recallFunction = false;
			var si = {
				h : splashImage.clientHeight,
				w : splashImage.clientWidth
			};
			if (si.h == 0) {
				si.h = 128;
				recallFunction = true;
			}
			if (si.w == 0) {
				si.w = 128;
				recallFunction = true;
			}
			try {
				// dojo.require("dojo.window");
		var vs = dojo.window.getBox();
		var toppx = (vs.h % 2 > 0) ? vs.h - 1 : vs.h;
		toppx = (((toppx / 2) - (si.h / 2)) < 0) ? "0px" : "" + ((toppx / 2) - (si.h / 2)) + "px";
		var leftpx = (vs.w % 2 > 0) ? vs.w - 1 : vs.w;
		leftpx = (((leftpx / 2) - (si.w / 2)) < 0) ? "0px" : "" + ((leftpx / 2) - (si.w / 2)) + "px";

		if (this.localDebug)
			hs_utils.debug("[hs_splashPositioner.setpos:] " + "window.h:[" + vs.h + "] " + "window.w:[" + vs.w + "] " + "toppx:[" + toppx
					+ "] leftpx:[" + leftpx + "] " + "si.h:[" + si.h + "] si.w:[" + si.w + "]");

		dojo.style(splashImage, "position", "absolute");
		dojo.style(splashImage, "top", toppx);
		dojo.style(splashImage, "left", leftpx);
		if (si.h > vs.h)
			dojo.style(splashImage, "height", "" + vs.h + "px");
		if (si.w > vs.w)
			dojo.style(splashImage, "width", "" + vs.w + "px");

		if (this.localDebug) {
			vs = dojo.window.getBox();
			hs_utils.debug("[hs_splashPositioner.setpos:] after reset: " + "window.h:[" + vs.h + "] " + "window.w:[" + vs.w + "] ");
		}
		if (recallFunction) {
			if (this.localDebug)
				console.debug("[hs_splashPositioner.setpos:]  is being recalled.");
			setTimeout( function() {
				hs_splashPositioner.setpos()
			}, 100);
		}
	} catch (e) {
		console.error("[hs_splashPositioner.setpos:] error:" + e.toString());
	}
}

	}
}

if (!hs_certMgrFuncs) {
	var hs_certMgrFuncs = {
		dateFormat : "",
		init : function() {
			var localDebug = false;
			var dbugLoc = "[hs_certMgrFuncs.init]: ";

			var wnode = hs_utils.getFirstNode('managerEBNew');
			var nodeList = dojo.query("[id$='managerEBx']");

			wnode = hs_utils.getFirstNode('managerAddressEBNew', true);
			nodeList = dojo.query("[id$='managerAddressEBx']");

			wnode = hs_utils.getFirstNode('managerCertificateIdEBNew');
			nodeList = dojo.query("[id$='managerCertificateIdEBx']");

			// var dtformat =
			// "#{javascript:getAppSettingsValue('inspDateFormat_cs');}";
		if (this.dateFormat == "")
			this.dateFormat = "d-M-yyyy";

		var datePickerOptions = {
			format : this.dateFormat,
			autoclose : true,
			clearBtn : true,
			orientation : "top left"
		};

		wnode = hs_utils.getFirstNode('managerCertificateExpEBNew');
		if (wnode != null) {
			jQuery(wnode).datepicker(datePickerOptions).on("show", function(){document.activeElement.blur();});
		} else {
			console.error(dbugLoc + "could not access node for managerCertificateExpEBNew");
		}

		nodeList = dojo.query("[id$='managerCertificateExpEBx']");
		if (localDebug)
			hs_utils.debug(dbugLoc + "for managerCertificateExpEBNew processing [" + nodeList.length + "] nodes.");
		dojo.forEach(nodeList, function(node) {
			jQuery(node).datepicker(datePickerOptions).on("show", function(){document.activeElement.blur();});
		});
	},

	newAddCF1id : "",
	tableId : "",

	addrow : function() {
		var localDebug = false;
		var dbugLoc = "[hs_certMgrFuncs.add]: ";

		var wnode = hs_utils.getFirstNode('managerEBNew');
		var mgr = (wnode == null) ? "" : wnode.value;

		wnode = hs_utils.getFirstNode('managerAddressEBNew', true);
		;
		var maddr = (wnode == null) ? "" : wnode.value;

		wnode = hs_utils.getFirstNode('managerCertificateIdEBNew');
		;
		var certId = (wnode == null) ? "" : wnode.value;

		wnode = hs_utils.getFirstNode('managerCertificateExpEBNew');
		;
		var certExp = (wnode == null) ? "" : wnode.value;

		var parms = {
			"manager" : mgr,
			"managerAddress" : maddr,
			"managerCertificateId" : certId,
			managerCertificateExp : certExp
		}
		wnode = hs_utils.getFirstNode('managerNewAddCF1');
		this.newAddCF1id = (wnode == null) ? "" : wnode.id;
		XSP.partialRefreshPost(this.newAddCF1id, {
			params : parms,
			onComplete : function() {
				hs_utils.debug("post succeeded to " + hs_certMgrFuncs.newAddCF1id);
				setTimeout( function() {
					var wnode = hs_utils.getFirstNode('inspCertifiedManagerEntryTable');
					hs_certMgrFuncs.tableId = (wnode == null) ? "" : wnode.id;
					XSP.partialRefreshPost(hs_certMgrFuncs.tableId, {
						onComplete : function() {
							var wnode = hs_utils.getFirstNode('managerEBNew');
							if (wnode != null)
								wnode.value = '';
							wnode = hs_utils.getFirstNode('managerAddressEBNew', true);
							if (wnode != null)
								wnode.value = '';
							wnode = hs_utils.getFirstNode('managerCertificateIdEBNew');
							if (wnode != null)
								wnode.value = '';
							wnode = hs_utils.getFirstNode('managerCertificateExpEBNew');
							if (wnode != null)
								wnode.value = '';
							setTimeout( function() {
								hs_certMgrFuncs.init()
							}, 20);
						},
						onError : function() {
							console.error("error on get to " + hs_certMgrFuncs.tableId)
						}
					});
				}, 20);
			},
			onError : function() {
				console.error("error on post to " + hs_certMgrFuncs.newAddCF1id)
			}
		})
	}
	};
};

if (!hs_utils) {
	var hs_utils = {
		dbDesign : "",
		isDebugger : false,
		isDebugServer : false,
		logHtmlErrorPageEventId : "",

		debug : function(msg, source) {
			if (!source)
				source = arguments.callee.caller.name;
			if (this.isDebugger || this.isDebugServer)
				console.debug("[" + source + "]: " + msg);
		},

		errorHtml : "",
		errorRoutine : "",

		// call this as:
		// onError : 'hs_utils.errorHandler( arguments[0], arguments[1],
		// ["<where you are calling from>"] )'
		errorHandler : function() {
			this.errorRoutine = (arguments[2]) ? arguments[2][0] : "GenericCall";
			//hs_errorHandler(arguments[0], arguments[1], this.errorHtml);
			
			this.errorHtml = arguments[0].responseText;
			if (this.errorHtml == "")
				this.errorHtml = "<b>NO RESPONSE MESSAGE RECEIVED.</b>";

			console.error("Error caught from XSP call to: " + this.errorRoutine);
			setTimeout( function() {
				var id = hs_utils.logHtmlErrorPageEventId;
				var ehtml = escape(hs_utils.errorHtml);
				var elen = ehtml.length;
				var parms = {
					desc : "XSP Error handler for: " + hs_utils.errorRoutine,
					elen : elen,
					hlen : hs_utils.errorHtml.length
				};
				var maxlen = 15000;
				if (ehtml.length < maxlen) {
					parms.html = ehtml;
				} else {
					var eparts = [];
					var epiece = "";
					var idx = 0;
					while (ehtml != "" && idx < 30) {
						idx += 1;
						epiece = (ehtml.length <= maxlen) ? ehtml : ehtml.substring(0, maxlen);
						eparts.push(epiece);
						ehtml = (ehtml.length <= maxlen) ? "" : ehtml.substring(maxlen, ehtml.length);
						console.debug("elen:[" + elen + "] part:[" + eparts.length + "] remaining:[" + ehtml.length + "]");
						parms["html" + eparts.length] = epiece;
					}
					// parms.html = eparts;
				}
				XSP
						.executeOnServer(
								id,
								false,
								{
									params : parms,
									onError : 'hs_utils.errorHandlerAgain( arguments[0], arguments[1], ["hs_inspectionCSCode.saveInspectionErrorHandler"] )',

									onComplete : function() {
										console.debug("error logged.");
									}
								});
			}, 10);
		},

		errorHandlerAgain : function() {
			console.error("Error caught from XSP call to:[" + hs_utils.logHtmlErrorPageEventId + "]");
		},

		getFirstNode : function(id1, silent) {
			var dbugLoc = "[hs_utils.getFirstNode]: ";
			var nodeList = dojo.query("[id$='" + id1 + "']");
			if (nodeList.length == 1)
				return nodeList[0]
			if (!silent)
				console.error(dbugLoc + 'dojo.query("[id$=\'' + id1 + '\']") returned[' + nodeList.length + '] elements.')
			if (nodeList.length > 1)
				return nodeList[0]
			return null;
		},

		xspRefreshErrorHandler : function() {
			// just do some debugging
		// of ioArgs object
		var arg = arguments[1];
		var txt = "";
		for ( var p in arg) {
			if ("arg" == p) {
				for ( var p1 in arg[p]) {
					console.error("xspRefreshErrorHandler arg[p][p1]:[" + p1 + "]\n" + arg[p][p1] + "\n" + typeof arg[p][p1]);
				}
			} else {
				console.error("xspRefreshErrorHandler arguments[1].p:[" + p + "]\n" + arg[p] + "\n" + typeof arg[p]);
			}
			// txt += p + " -> " + arg[p] + " [" + typeof( arg[p] ) + "]\n";
		// txt += p + "\n";
	}
	// alert( "Error\n" + txt + "\n" + arguments[0].responseText);
}
	}
};

if (!hs_inspDivs) {
	var hs_inspDivs = {
		btnids : {
			debug : "",
			images : "",
			main : "",
			temperatures : "",
			timerpt : "",
			violations : "",
			violedit : ""
		},
		ids : {
			debug : "",
			images : "",
			main : "",
			temperatures : "",
			timerpt : "",
			violations : "",
			violedit : ""
		},
		localDebug : false,
		toShow : "",
		toShowNext : null,

		show : function(toshow, nxtfunc) {
			var dbugLoc = "[hs_inspDiv.show]: ";
			for ( var dname in this.ids) {
				if (dname == toshow) {
					this.toShow = toshow;
					this.toShowNext = (nxtfunc) ? nxtfunc : null;
					setTimeout( function() {
						if (hs_inspDivs.localDebug)
							console.debug(dbugLoc + "showing div:[" + hs_inspDivs.toShow + "] - id:[" + hs_inspDivs.ids[hs_inspDivs.toShow]
									+ "]");
						var toshowDiv = dojo.byId(hs_inspDivs.ids[hs_inspDivs.toShow]);
						dojo.style(toshowDiv, "display", "block");
						if (hs_inspDivs.btnids[hs_inspDivs.toShow] != "") {
							dojo.addClass(dojo.byId(hs_inspDivs.btnids[hs_inspDivs.toShow]), "active");
						}
						// var pdiv =
						// toshowDiv.parentNode.parentNode.parentNode;
							// dojo.style(pdiv, "top", "0px");
							if (hs_inspDivs.toShowNext != null) {
								try {
									hs_inspDivs.toShowNext();
									if (hs_inspDivs.localDebug)
										console.debug("ran next function");
								} catch (e) {
									console.error("hs_inspDivs.show: Error on next function: " + e.toString());
								}
							}
							setTimeout( function() {
								window.scrollTo(0, 0)
							}, 10);
						}, 2);
				} else if (this.ids[dname] != "") {
					dojo.style(dojo.byId(this.ids[dname]), "display", "none");
					if (this.btnids[dname] != "") {
						// dojo.attr(dojo.byId(this.btnids[dname]), "class",
						// "mblTabBarButton");
						dojo.removeClass(dojo.byId(this.btnids[dname]), "active");
					}
				}
			}
		},

		showWithRefresh : function(toshow, nxtfunc) {
			var dbugLoc = "[hs_inspDiv.showWithRefresh]: ";
			if (this.toShow == toshow) {
				if (hs_inspDivs.localDebug)
					console.debug(dbugLoc + "Already showing this tab:[" + this.toShow + "]");
				return;
			}
			for ( var dname in this.ids) {
				if (dname == toshow) {
					this.toShow = toshow;
					this.toShowNext = (nxtfunc) ? nxtfunc : null;
					hs_inspectionCSCode.waitPanelShow();
					setTimeout( function() {
						if (hs_inspDivs.localDebug)
							console.debug(dbugLoc + "refreshing div:[" + hs_inspDivs.toShow + "] - id:["
									+ hs_inspDivs.ids[hs_inspDivs.toShow] + "]");
						XSP.partialRefreshPost(hs_inspDivs.ids[hs_inspDivs.toShow], {
							onComplete : function() {
								hs_inspDivs.showWithRefreshStep2();
							},
							onError : 'hs_utils.errorHandler( arguments[0], arguments[1], ["hs_inspDiv.showWithRefresh"] )'
						});
					}, 2);
				} else if (this.ids[dname] != "") {
					// dojo.style(dojo.byId(this.ids[dname]), "display",
					// "none");
					x$(this.ids[dname]).hide();
					x$(this.btnids[dname]).removeClass("active");
				}
			}
		},

		showWithRefreshStep2 : function() {
			var dbugLoc = "[hs_inspDiv.showWithRefreshStep2]: ";
			if (hs_inspDivs.localDebug)
				console.debug(dbugLoc + "showing div:[" + hs_inspDivs.toShow + "]");
			x$(hs_inspDivs.ids[hs_inspDivs.toShow]).show();
			if (hs_inspDivs.btnids[hs_inspDivs.toShow] != "") {
				if (hs_inspDivs.localDebug)
					console.debug(dbugLoc + "showing button div:[" + hs_inspDivs.toShow + "] - id:["
							+ hs_inspDivs.btnids[hs_inspDivs.toShow] + "]");
				x$(hs_inspDivs.btnids[hs_inspDivs.toShow]).addClass("active");
			}
			if (hs_inspDivs.toShowNext != null) {
				try {
					hs_inspDivs.toShowNext();
					if (hs_inspDivs.localDebug)
						console.debug("ran next function");
				} catch (e) {
					console.error("hs_inspDivs.show: Error on next function: " + e.toString());
				}
			}
			setTimeout( function() {
				window.scrollTo(0, 0);
				if (hs_inspDivs.localDebug)
					console.debug(dbugLoc + "scrolling to the top and hiding the waitPanel...");
				hs_inspectionCSCode.waitPanelHide();
			}, 10);
		}
	}
}

if (!hs_errorHandler) {
	var hs_errorHandler = {
		saveErrorHtml : '',
		saveErrorSource : '',
		saveErrorException : '',
		saveStackTrace : '',
		saveStyle : '',
		msgdivid : '',
		msghtml : '',
		otherData : '',
		
		error : function(arg0, arg1, arg2, options) {
			var dbarLoc = "[hs_errorHandler.error] ";
			
			console.log(dbarLoc + "Error from:[" + arg2 + "]");
			if(!options) options = {
				noTextClass : false,
				others : ""
			}
			//this.saveErrorHtml = arguments[0].responseText;
			this.saveErrorHtml = arg0.responseText;
			this.msghtml = "<h4";
			this.msghtml += (options.noTextClass) ? "" : "class='text-danger'"
				this.msghtml += "><strong>" + "Error occured during " + arg2 + "</strong></h4>";
			
			try {
				this.msghtml += "<a id='errorHandlerExpandLink' class='btn btn-xs btn-primary' href='javascript:hs_errorHandler.expandError()'>show error detail</a>";
				this.msghtml += '<div id="errorHandlerCollpsedDiv" style="display:none;">'
				this.saveErrorSource = this.getErrorSource(this.saveErrorHtml);
				this.msghtml += this.saveErrorSource;
				
				this.msghtml += '<br/>';
				
				this.saveErrorException = this.getErrorException(this.saveErrorHtml);
				this.msghtml += this.saveErrorException;
				if(this.otherData != '') {
					this.msghtml += '<h2 style="font-size: 14pt; margin-bottom: 2px; margin-top: 12px;">Other data</h2>'; 
					this.msghtml += '<p style="margin-bottom: 2px; margin-top: 2px;">' + this.otherData  + '</p>'
				}
				this.msghtml += "<div id='errorHandlerButtons' class='btn-group' style='margin-top:15px;'>";
				this.msghtml += "<a class='btn btn-xs btn-primary' href='javascript:hs_errorHandler.showSavedError()'>show full error detail</a>";
				this.msghtml += "<a class='btn btn-xs btn-warning' href='javascript:hs_errorHandler.sendToSupport()'>send error to support</a>";
				this.msghtml += "</div>";
				this.msghtml += "</div>";
				
				this.saveStackTrace = this.getStackTrace(this.saveErrorHtml);
				this.saveStyle = this.getStyle(this.saveErrorHtml);
				
			} catch (e) {
				this.msghtml += '<h5 class="text-danger">ERROR on extract:' + e.toString() + '</h5>';
			}
			console.log(dbarLoc + "writing error to div:[" + this.msgdivid + "]")
			x$(this.msgdivid).html(this.msghtml).show();
			//window.scrollTo(0, 0);
		},
		
		expandError : function () {
			$("#errorHandlerExpandLink").hide();
			$("#errorHandlerCollpsedDiv").show();
		},
		
		getErrorSource : function (html) {
			var p1 = html.indexOf("<h2>Error source");
			var p2 = html.indexOf("<h2>Exception");
			return this.cleanUpHtml(html.substring(p1, p2));
		},
		
		getErrorException : function (html) {
			var p2 = html.indexOf("<h2>Exception");
			var p3 = html.indexOf('<p style="margin-left : 2em;">');
			return this.cleanUpHtml(html.substring(p2,p3));
		},
		
		getStackTrace : function (html) {
			// <pre id="stacktrace" style="display:none">
			x$(this.msgdivid).html(html);
			var strace = x$(this.msgdivid).find('#stacktrace').css('display', 'block').html();
			return strace;
		},
		
		getStyle : function (html) {
			x$(this.msgdivid).html(html);
			var sstyle = x$(this.msgdivid).find('style').html();
			return sstyle;
		},
		
		cleanUpHtml : function (html) {
			x$(this.msgdivid).html(html);
			x$(this.msgdivid).find('.row').each(function(index) {
				$(this).removeClass('row').css('display', 'block');
			});
			x$(this.msgdivid).find('br').each(function(index){
				$(this).remove();
			});
			x$(this.msgdivid).find('h2').css('font-size', '14pt').css('margin-bottom', '2px').css('margin-top', '2px');
			
			return x$(this.msgdivid).html();
		},
		
		sendToSupport: function () {
			console.log('sending error to support');
			$('#errorHandlerButtons').html('<h5 class="text-warning">sending error to support... Please wait</h5>');
			var resturl = "sendErrorToSupport.xsp/sendError1"
			//resturl += "?_t=" + new Date().getTime();
			
			var edata = {
				sourceUrl : escape(location.toString()),
				errorException : escape(this.saveErrorException),
				errorHtml : escape(this.saveErrorHtml),
				errorSource : escape(this.saveErrorSource),
				otherData : escape(this.otherData),
				stackTrace : escape(this.saveStackTrace),
				style : escape(this.saveStyle)
			};
			var data = {
					sourceUrl : location.toString(),
					errorException : this.saveErrorException,
					errorHtml : this.saveErrorHtml,
					errorSource : this.saveErrorSource,
					otherData : this.otherData,
					stackTrace : this.saveStackTrace,
					style : this.saveStyle
				};
				
			$.post(resturl, data, function(data, status, xhr) {
				console.log("send status: " + status);
				//console.log("data: " + datastr);
				//var data = {}
				//eval("data = " + datastr);
				console.log("service status: " + data.status);
				var html = '<h5 class="text-success">' + data.status + '</h5>';
				html += '<a href="home.xsp" class="btn btn-primary btn-sm">Re-open DB</a>'
				$('#errorHandlerButtons').html(html);
			});
		},
		
		showSavedError : function () {
			document.write(this.saveErrorHtml);
			document.close();
		}
	};
}