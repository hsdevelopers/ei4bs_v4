/********
 * dbLinks library - Code for building and managing the database links.
 */

function get_HS_DatabaseLinks() {
	var localDebug = false;
	var dbarLoc = database.getFilePath() + '.dbLinks.get_HS_DatabaseLinks';
	var startDate:Date = new Date();
	scopeKey = "HS_DatabaseLinksData";
	scopeKey2 = "HS_DatabaseUser";
	var dbData:java.util.Vector = sessionScope.get(scopeKey);
	var dbUser = sessionScope.get(scopeKey2);
	if (dbUser == null) {
		dbUser = @UserName();
		//dbUser = @Name("[CANONICALIZE]", "John Doe/Vinton/Ohio/HealthSpace");
		dbData = null;
	}
	if("Anonymous".equalsIgnoreCase(@UserName())) {
		if(localDebug) dBar.debug("detected anonymous user. clearing lists...", dbarLoc);
		sessionScope.remove(scopeKey);
		sessionScope.remove(scopeKey2);
		return new java.util.Vector();
	}
	if(!dbUser.equals(@UserName())) {
		if(localDebug) dBar.debug("detected changed user user. clearing lists...", dbarLoc);
		sessionScope.remove(scopeKey);
		sessionScope.remove(scopeKey2);
		dbUser = @UserName();
		dbData = null;
	}
	var dbUserOu1 = @Name("[OU1]", dbUser);
	var dbUserOu2 = @Name("[OU2]", dbUser);
	
	if(dbData == null) {
		dbData = hs_Database.buildLinksData();
		
	} else {
		if(localDebug) {
			var endDate:Date = new Date();
			var msecs:long = endDate.getTime() - startDate.getTime();
			dBar.debug("retuirned from scope [" + dbData.size() + "] Vector in [" +
					msecs + "] milli seconds.", dbarLoc);
		}
	}
	
	return dbData;
}

function get_HS_FileData() {
	var localDebug = false;
	var dbarLoc = database.getFilePath() + '.dbLinks.get_HS_FileData';
	scopeKey = "HS_FileData";
	var fData = applicationScope.get(scopeKey);
	if("Anonymous".equalsIgnoreCase(@UserName())) {
		applicationScope.remove(scopeKey);
		return new java.util.Vector();
	}
	if(fData == null) {
		fData = new java.util.Vector();
				
		var fView = database.getView("HS_Files");
		var fEntries:NotesViewEntryCollection = null;
		var fEntry:NotesViewEntry = null;
		var nEntry:NotesViewEntry = null;
		var url:XSPUrl = context.getUrl();
		var turl = "";
		if(fView == null) {
			// if(localDebug) dBar.debug("HS_Files view is null", dbarLoc)
			return fData;
		} else {
			fEntries = fView.getAllEntries();
			if(fEntries.getCount() == 0) {
				// if(localDebug) dBar.debug("There are no file docs.", dbarLoc);
				return fData;
			} else {
				// if(localDebug) dBar.debug("Adding [" + fEntries.getCount() + "] file docs.", dbarLoc);
				fEntry = fEntries.getFirstEntry();
				while (fEntry != null) {
					var cvals = fEntry.getColumnValues();
					turl = @Left(url.getAddress(), ".nsf") + 
							".nsf/HS_Files" +
							"/" + fEntry.getUniversalID() +
							"/$File/" + cvals.get(2)
					fData.push({
						fname : cvals.get(0),
						desc  : cvals.get(1),
						aname : cvals.get(2),
						url   : turl,
						unid  : fEntry.getUniversalID()
					});
					nEntry = fEntries.getNextEntry(fEntry);
					fEntry.recycle();
					fEntry = nEntry;				
				}
			}
		}
		applicationScope.put(scopeKey, fData);
		
		if(nEntry != null) nEntry.recycle();
		if(fEntry != null) fEntry.recycle();
		if(fEntries != null) fEntries.recycle();
		if(fView != null) fView.recycle();
	}
	return fData;
}