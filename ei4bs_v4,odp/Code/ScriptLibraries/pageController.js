// Page controller for HS style forms

if(!pageController) {
	var pageController = {
			addrFields : {},
			
			buttons : {},
			
			sections : {},
	
			switches : {},
			
			checkboxes : {},
			
			checkboxes2 : {},
			
			// dtformat : "#{javascript:getAppSettingsValue('inspDateFormat_cs');}",
			
			datePickerOptions : {
			    format : "",
			    autoclose : true,
			    clearBtn : true,
			    todayBtn : "linked",
			    todayHighlight : true,
			    orientation : "top left"
			},
			
			clockPickerOptions : {
			    placement: 'bottom',
			    align: 'left',
			    donetext: 'Done',
			    autoclose : true,
			    'default' : "now",
			    afterShow : function () {
					//console.log("time picker afterShow - bluring the active element to close the keyboard")
					document.activeElement.blur();
				}
			},
			
			showButtonsAndSections : function () {
				$.each( this.sections, function( key, value ) {
					var selectorBody = "div[id*='" + value.id + "Body']";
					var selectorMinus = "span[id*='" + value.id + "MinusPanel']";
					var selectorPlus = "span[id*='" + value.id + "PlusPanel']";
					
					if (value.status) {
						//console.log("Showing id:[" + value + "Body] for key:[" + key + "] selector: " + selectorBody + selectorMinus + selectorPlus);
						$(selectorBody).show();
						$(selectorMinus).show();
						$(selectorPlus).hide();
					} else {
						//console.log("Hiding id:[" + value + "Body] for key:[" + key + "] selector: " + selectorBody + selectorMinus + selectorPlus);
						$(selectorBody).hide();
						$(selectorMinus).hide();
						$(selectorPlus).show();
					}
				});
				
				$.each( this.buttons, function( key, value ) {
					// console.log("for:[" + key + "] did:[" + value.id + "] status:[" + value.status + "]");
					if(value.status) {
						x$(value.id).show();
					} else {
						x$(value.id).hide();
					}
				});
				
				//if(this.dtformat == "") this.dtformat = "d-M-yyyy";
				if (this.datePickerOptions.format == "") this.datePickerOptions.format = "d-M-yyyy";
				//if (this.datePickerOptions.format == "") this.datePickerOptions.format = "M d, yyyy";
				
				//'#{id:form-row-dateEB1}'
				//var selectorDates = "input[id*='form-row-dateEB1']";
				var selectorDates = "input[id*='form-row-dateEB']";
				var dateControls = $(selectorDates)
				//console.log("for selector: " + selectorDates);
				//console.log("got[" + dateControls.length + "] fields.")
				$.each(dateControls, function(key, value){
					//console.log("got date field:[" + key + "] id:[" + value.id + "]");
					$(value).datepicker(pageController.datePickerOptions).on("show", function(){document.activeElement.blur();});
				});
				
				try{
					$('.clockpicker').clockpicker(pageController.clockPickerOptions);
				} catch(e) {
					// fails if there are no clock pickers on the page.
				}
				this.resetSwitches();
				
				$.each(this.checkboxes2, function(key, value){
					var values = x$(value.ebid).val();
					// console.log("for checkboxes2:[" + key + "] ebid:[" + value.ebid + "] rptid:[" + value.rptid + "] values:[" + values + "]");
					// toggleCBvalue2 : function (dbind, value, btnid)
					if(values != "") {
						$.each(values.split(";"), function(vkey, val) {
							// console.log("turning on vkey:[" + vkey + "] val:[" + val + "]");
							x$(value.rptid).find("*").each( function (bkey, bval) {
								if(val == bval.value) {
									//console.log("FOUND BUTTON!:[" + bval.id + "]");
									x$(bval.id).removeClass('btn-default').addClass('btn-primary');
								} 
							});
						});
					}
				});
			},
			
			toggleSection : function (sid) {
				$.each( this.sections, function( key, value ) {
					if(sid == value.id) {
						value.status = !(value.status);
						pageController.sections[key] = value; 
						var selector = "div[id*='" + value.id + "']";
						$(selector).toggle();
						selector = "span[id*='" + value.id + "']";
						$(selector).toggle();
						//console.log("key:[" + key + "] value.id:[" + value.id + "] sid:[" + sid + "] status:[" + value.status + "] selector: " + selector );
					}
				});
				//setTimeout(function(){ pageController.showButtonsAndSections();}, 1);
			},
			
			collapseAll : function() {
				$.each( this.sections, function( key, value ) {
					// console.log("key:[" + key + "] id:[" + value.id + "] status:[" + value.status + "]");
					if(value.status) {
						pageController.toggleSection(key);
					}
				})
			},
			
			expandAll : function() {
				$.each( this.sections, function( key, value ) {
					// console.log("key:[" + key + "] id:[" + value.id + "] status:[" + value.status + "]");
					if(!value.status) {
						pageController.toggleSection(key);
					}
				})
			},
			
			addButton : function(bname, bid, state) {
				if(!(this.buttons[bname])) {
					this.buttons[bname] = { id : bid, status : state};
					//console.log('button:"' + bid + '" loaded with bid:[' + bid + '] state:[' + state + ']');
				}
			},
			
			addSection : function(sid,state) {
				if(!(this.sections[sid])) {
					this.sections[sid] = { id : sid, status : !(state)};
					//console.log('section"' + sid + '" loaded with state:[' + state + ']');
				}
			},
			
			addSwitch2 : function (wid, bdata, dbind){
				if(!(this.switches[wid])) {
					this.switches[wid] = {
							'ebid' : wid,
							'offclass' : "btn-default",
							'onclass' : "btn-primary",
							'dbind' : dbind
					}
					for (var i=0; i<bdata.length; i++) {
						this.switches[wid]["bid"+(i+1)] = bdata[i].bid;
						this.switches[wid]["text"+(i+1)] = bdata[i].text;
						this.switches[wid]["val"+(i+1)] = bdata[i].val;
					}
					// console.log("switch loaded: wid:[" + wid + "] dbind:[" + dbind + "] using addSwitch2");
				}
			},
			
			resetSwitches : function() {
				// Set the switches back to the way they should be...
				$.each( this.switches, function(key, value){
					var fvalue = x$(value.ebid).val();
					//console.log("processing switch: wid:[" + value.ebid + "]  fvalue:[" + fvalue + "]");
					if(fvalue == value.val1) {
						x$(value.bid1).removeClass(value.offclass).addClass(value.onclass);
						x$(value.bid2).removeClass(value.onclass).addClass(value.offclass);
						if (value.bid3) x$(value.bid3).removeClass(value.onclass).addClass(value.offclass);
						// console.log("processing switch: wid:[" + value.ebid + "]  fvalue:[" + fvalue + "] set #1 as on.");
					}
					else if(fvalue == value.val2) {
						x$(value.bid2).removeClass(value.offclass).addClass(value.onclass);
						x$(value.bid1).removeClass(value.onclass).addClass(value.offclass);
						if (value.bid3) x$(value.bid3).removeClass(value.onclass).addClass(value.offclass);
						// console.log("processing switch: wid:[" + value.ebid + "]  fvalue:[" + fvalue + "] set #2 as on.");
					}
					else if(fvalue == value.val3) {
						x$(value.bid3).removeClass(value.offclass).addClass(value.onclass);
						x$(value.bid1).removeClass(value.onclass).addClass(value.offclass);
						x$(value.bid2).removeClass(value.onclass).addClass(value.offclass);
						// console.log("processing switch: wid:[" + value.ebid + "]  fvalue:[" + fvalue + "] set #3 as on.");
					} else {
						// console.log("processing switch: wid:[" + value.ebid + "]  fvalue:[" + fvalue + "] did not find a value match! ");
					}
				});
			},
			
			addSwitch : function (wid, bid1, bid2, st1, st2, sv1, sv2, dbind) {
				if(!(this.switches[wid])) {
					this.switches[wid] = {
							'ebid' : wid,
							'bid1' : bid1,
							'bid2' : bid2,
							'text1' : st1,
							'text2' : st2,
							'val1' : sv1,
							'val2' : sv2,
							'offclass' : "btn-default",
							'onclass' : "btn-primary",
							'dbind' : dbind
					}
					// console.log("switch loaded: wid:[" + wid + "] dbind:[" + dbind + "]");
				} 
			},
			
			addCheckbox : function (ebid, dbind, label, rptid, rfid) {
				if(!this.checkboxes[dbind]) {
					this.checkboxes[dbind] = {
							'dbind' : dbind,
							'ebid' : ebid,
							'label' : label,
							'rptid' : rptid
					}
					this.checkboxes[dbind].rfid = (rfid) ? rfid : ""; 
				}		
			},
			
			toggleCBvalue : function (dbind, value) {
				if (this.checkboxes[dbind]) {
					var ebid = this.checkboxes[dbind].ebid;
					var cvalue = x$(ebid).val();
					var nvalue = "";
					var rfid = this.checkboxes[dbind].rfid;
					
					if (cvalue.indexOf(value) > -1) {
						var cvlist = cvalue.split(";");
						var nlist = [];
						for(var i=0; i<cvlist.length; i++) {
							if(cvlist[i] != value) nlist.push(cvlist[i]);
						}
						nvalue = nlist.join(";");
					} else {
						nvalue = cvalue;
						nvalue += (nvalue == "") ? "" : ";";
						nvalue += value;
					}
					// console.log("toggling value:[" + value + "] in dbind:[" + dbind + "] ebid:[" + ebid + "] nvalue is:[" + nvalue + "] rfid:[" + rfid + "]");
					x$(ebid).val(nvalue)
					if (rfid != ""){
						XSP.partialRefreshPost( rfid, {
							"onComplete" : function() {
								setTimeout(function(){ pageController.showButtonsAndSections() },2);
							}
						});
					}
				} else {
					console.error("Unable to find checkbox for dbind:[" + dbind + "] to toggle!  pageController.toggleCBvalue;");
				}
			},
			
			addCheckbox2 : function (ebid, dbind, label, rptid, oebid) {
				if(!this.checkboxes2[dbind]) {
					this.checkboxes2[dbind] = {
							'dbind' : dbind,
							'ebid' : ebid,
							'label' : label,
							'rptid' : rptid,
							'oebid' : oebid
					}
				}		
			},
			
			toggleCBvalue2 : function (dbind, value, btnid) {
				if (this.checkboxes2[dbind]) {
					var ebid = this.checkboxes2[dbind].ebid;
					var cvalue = x$(ebid).val();
					var nvalue = "";
					var nclass = "";
					
					if (cvalue.indexOf(value) > -1) {
						var cvlist = cvalue.split(";");
						var nlist = [];
						for(var i=0; i<cvlist.length; i++) {
							if(cvlist[i] != value) nlist.push(cvlist[i]);
						}
						nvalue = nlist.join(";");
						nclass = 'btn-default';
					} else {
						nvalue = cvalue;
						nvalue += (nvalue == "") ? "" : ";";
						nvalue += value;
						nclass = 'btn-primary';
					}
					// console.log("btnid:[" + btnid + "]  nclass:[" + nclass + "]");
					x$(btnid).removeClass('btn-default').removeClass('btn-primary').addClass(nclass);
					
					// console.log("toggling value:[" + value + "] in dbind:[" + dbind + "] ebid:[" + ebid + "] nvalue is:[" + nvalue + "] - pageController.toggleCBvalue2");
					x$(ebid).val(nvalue);
					XSP.partialRefreshPost( this.checkboxes2[dbind].rptid, {
						"onComplete" : function() {
							setTimeout(function(){ pageController.showButtonsAndSections() },2);
						}
					});
				} else {
					console.error("Unable to find checkbox for dbind:[" + dbind + "] to toggle!  pageController.toggleCBvalue2;");
				}
			},
			
			showCBOther2 : function(dbind) {
				if (this.checkboxes2[dbind]) {
					var rptid = this.checkboxes2[dbind].rptid;
					
					// console.log("showing ebox in pageController.showCBOther2 for:[" + rptid + "]");
					//x$(oebid).removeClass("hide").addClass("show");
					//x$(rptid).find("input").parent().removeClass("hide").addClass("show");
					x$(rptid).find("input").parent().removeClass("hide");
					x$(rptid).find("input").focus();
				} else {
					console.error("Unable to find checkbox for dbind:[" + dbind + "] to toggle!  pageController.showCBOther2;");
				}
			}, 
			
			addCBOther2 : function(dbind) {
				if (this.checkboxes2[dbind]) {
					var rptid = this.checkboxes2[dbind].rptid;
					var ovalue = x$(rptid).find("input").val();
					// console.log("adding value:[" + ovalue + "] in pageController.addCBOther2 for:[" + rptid + "]");
					if(ovalue == "") {
						//x$(rptid).find("input").parent().removeClass("show").addClass("hide");
						x$(rptid).find("input").parent().addClass("hide");
						return;
					}
					
					var ebid = this.checkboxes2[dbind].ebid;
					var cvalue = x$(ebid).val();
					var nvalue = "";
					var nclass = "";
					if(cvalue.indexOf(ovalue) > -1) {
						
					} else {
						nvalue = cvalue;
						nvalue += (nvalue == "") ? "" : ";";
						nvalue += ovalue;
						x$(ebid).val(nvalue);
					}
					
					x$(rptid).find("input").parent().addClass("hide");
					x$(rptid).find("input").val("");
					
					XSP.partialRefreshPost( rptid, {
						"onComplete" : function() {
							setTimeout(function(){ pageController.showButtonsAndSections() },2);
						}
					});
				} else {
					console.error("Unable to find checkbox for dbind:[" + dbind + "] to toggle!  pageController.showCBOther2;");
				}
			},
			
			addrFieldAdd : function(fldid, pfldid, rid, bid, did, snid, stid, ssid, modalid) {
				if(!this.addrFields[fldid]) {
					this.addrFields[fldid] = {
							'currid' : fldid,
							'pfldid' : pfldid,
							'rid' : rid,
							'bid' : bid,
							'did' : did,
							'snid' : snid,
							'stid' : stid,
							'ssid' : ssid,
							'currpartsid' : pfldid,
							'resultid' : rid,
					        'bnumid' : bid,
					        'dirid' : did,
					        'snameid' : snid,
					        'stypeid' : stid,
					        'suffixid' : ssid,
					        'modalid' : modalid
					}
				}		
				
			},
			
			addrFieldFocus : function(fldid) {
				if (this.addrFields[fldid]) {
					var wrkfld = this.addrFields[fldid];
					x$(wrkfld.currid).blur();
					setTimeout(function (){
						pageController.addrFieldLoadAndShow( fldid );
					}, 1);
				} else {
					console.error("Unable to find address field for:[" + fldid + "] in pageController.addrFieldFocus");
				}
			},
			
			addrFieldLoadAndShow : function(fldid) {
				if (this.addrFields[fldid]) {
					var wrkfld = this.addrFields[fldid];
					var parts = x$(wrkfld.currpartsid).val();
					var bnum = "";
					var dir =  "";
					var sname = "";
					var stype = "";
					var suffix = "";
					if(parts != "") {
						// bn:'123', dir:'', sname:'Main', stype:'St.', suffix:''
						parts = parts.split(",");
						bnum = parts[0].substring(4, (parts[0].length-1));
						dir = parts[1].substring(6, (parts[1].length-1));
						sname = parts[2].substring(8, (parts[2].length-1));
						stype = parts[3].substring(8, (parts[3].length-1));
						suffix = parts[4].substring(9, (parts[4].length-1));
						
						console.log("part0:[" + parts[0] + "] bnum:[" + bnum + "]\npart1:[" + parts[1] + "] dir:[" + dir + "]\nspart2:[" + parts[2] + "] sname:[" + sname + "]"); 
						console.log("part3:[" + parts[3] + "] stype:[" + stype+ "]\npart4:[" + parts[4] + "] suffix:[" + suffix + "]");
					}
					x$(wrkfld.bnumid).val(bnum);
					x$(wrkfld.dirid).val(dir);
					x$(wrkfld.snameid).val(sname);
					x$(wrkfld.stypeid).val(stype);
					x$(wrkfld.suffixid).val(suffix);
					
					//wrkfld.recompute();
					pageController.addrFieldRecompute(fldid);
					
					x$( wrkfld.modalid ).modal('show');
				} else {
					console.error("Unable to find address field for:[" + fldid + "] in pageController.addrFieldLoadAndShow");
				}
			},
			
			addrFieldRecompute : function(fldid) {
				if (this.addrFields[fldid]) {
					var wrkfld = this.addrFields[fldid];
					console.log( "recomputing target is:[" + wrkfld.resultid + "]");
					x$( wrkfld.resultid ).val("recomputing target is:[" + wrkfld.resultid + "]");
					var bnum = x$(wrkfld.bnumid).val();
					var dir =  x$(wrkfld.dirid).val();
					var sname = x$(wrkfld.snameid).val();
					var stype = x$(wrkfld.stypeid).val();
					var suffix = x$(wrkfld.suffixid).val();
					wrkfld.result = "";
					wrkfld.result += (bnum == "") ? "" : bnum + " ";
					wrkfld.result += (dir == null)  ? "" : (dir == " ") ? "" : dir + " ";
					wrkfld.result += (sname == "") ? "" : sname;
					wrkfld.result += (stype == null) ? "" : (stype == " ") ? "" : " " + stype;
					wrkfld.result += (suffix == null) ? "" : (suffix == " ") ? "" : " " + suffix;
					
					x$( wrkfld.resultid ).val( wrkfld.result );
					console.log("new result:[" + wrkfld.result + "]");
					
					wrkfld.parts = "bn:'" + ((bnum == "") ? "" : bnum) + "'";
					wrkfld.parts += ", dir:'" + ((dir == null)  ? "" : (dir == "") ? "" : dir) + "'";
					wrkfld.parts += ", sname:'" + sname + "'";
					wrkfld.parts += ", stype:'" + ((stype == null) ? "" : (stype == " ") ? "" : stype) + "'";
					wrkfld.parts += ", suffix:'" + ((suffix == null) ? "" : (suffix == " ") ? "" : suffix) + "'";
					
					x$(wrkfld.resultid).val(wrkfld.result);
					console.log("new parts:[" + wrkfld.parts + "]");
					
				} else {
					console.error("Unable to find address field for:[" + fldid + "] in pageController.addrFieldRecompute");
				}
			},
			
			addrFieldUpdateAndClose : function(fldid) {
				if (this.addrFields[fldid]) {
					var wrkfld = this.addrFields[fldid];
					this.addrFieldRecompute(fldid);
					x$(wrkfld.currid).val(wrkfld.result);
					x$(wrkfld.currpartsid).val(wrkfld.parts);
					
					x$( wrkfld.modalid ).modal('hide');
					
				} else {
					console.error("Unable to find address field for:[" + fldid + "] in pageController.addrFieldUpdateAndClose");
				}
			}
			
			
	}
	$(document).ready(function() {
		setTimeout(function(){pageController.showButtonsAndSections();}, 2);
	})
}