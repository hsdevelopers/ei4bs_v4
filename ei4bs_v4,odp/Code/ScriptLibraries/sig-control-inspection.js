$(document).ready(function(){
	var _sig1 = $(".fldSignature1").val() || [];
	$("#sigBlock1").signaturePad({drawOnly:true,output:".fldSignature1",lineTop:70}).regenerate(_sig1);
	var _sig2 = $(".fldSignature2").val() || [];
	$("#sigBlock2").signaturePad({drawOnly:true,output:".fldSignature2",lineTop:70}).regenerate(_sig2);
});