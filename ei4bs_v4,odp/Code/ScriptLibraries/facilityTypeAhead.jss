var facilityTypeAhead = function (searchValue:string) {
	// update the following line to point to your real searchDb
	//var directory:NotesDatabase = session.getDatabase("", "test/spnames.nsf");
	var returnList = "<ul>";
	//var dbpath = "Clients\\Michigan\\Genesee\\Genesee_County_EHS_Testing.nsf";
	var searchDb:NotesDatabase = getDb("eiRoot");
	if(searchDb == null) {
		returnList += "<li>Unable to access database</li>";
		returnList += "<li>eiRoot</li>";
		returnList += "</ul>";
		return returnList;
	}
	if(!searchDb.isOpen()) {
		returnList += "<li>Unable to OPEN database</li>";
		returnList += "<li>" + dbpath + "</li>";
		returnList += "</ul>";
		return returnList;
	}
	var vname = "($Users)";
	var allUsers:NotesView = searchDb.getView(vname);
	if (allUsers == null) {
		returnList += "<li>Unable to access view</li>";
		returnList += "<li>" + vname + "</li>";
		returnList += "</ul>";
		return returnList;
	}
	var matches = {};
	var includeForm = {
		Person: true,
		Group: true
	}
	var matchingEntries:NotesViewEntryCollection = allUsers.getAllEntriesByKey(searchValue, false);
	var entry:NotesViewEntry = matchingEntries.getFirstEntry();
	var resultCount:int = 0;
	while (entry != null) {
		var matchDoc:NotesDocument = entry.getDocument();
		var matchType:string = matchDoc.getItemValueString("Form");
		if (includeForm[matchType]) { // ignore if not person or group
			var fullName:string = matchDoc.getItemValue("FullName").elementAt(0);
			if (!(matches[fullName])) { // skip if already stored
				resultCount++;
				var matchName:NotesName = session.createName(fullName);
				matches[fullName] = {
					cn: matchName.getCommon(),
					photo: matchDoc.getItemValueString("photoUrl"),
					job: matchDoc.getItemValueString("jobTitle"),
					email: matchDoc.getItemValueString("internetAddress")
				};
			}			
		}
		if (resultCount > 9) {
			entry = null; // limit the results to first 10 found
		} else {
			entry = matchingEntries.getNextEntry(entry);
		}
	}
	for (var matchName in matches) {
		var match = matches[matchName];
		var matchDetails:string = [
			"<li><table><tr><td><img class=\"avatar\" src=\"",
			match.photo,
			"\"/></td><td valign=\"top\"><p><strong>",
			match.cn,
			"</strong></p><p><span class=\"informal\">",
			match.job,
			"<br/>",
			match.email,
			"</span></p></td></tr></table></li>"
		].join("");
		returnList += matchDetails;
	}
	returnList += "</ul>";
	return returnList;
}