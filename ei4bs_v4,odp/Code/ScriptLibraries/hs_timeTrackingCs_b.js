if (!hs_timeTracking)
	var hs_timeTracking = {
		ids : {
			timeIn : "",
			timeOut : "",
			tmins : "",
			thours : "",
			tspent : "",
			messages : "",
			messagesFr1 : "",
			saveCF1 : "",
			newCf1 : "",
			newMessages : "",
			eho : "",
			timetype : "",
			timeTrackingViewPanel : ""
		},
		saveStatus : "",
		saveMessages : [],
		saveParms : {
			saveinsp : false, 
			submitinsp : false
		},
		
		timeIn : "",
		timeOut : "",
		tmins : "",
		thours : "",
		tspent : "",

		recalc : function() {
			var nval = dojo.byId(this.ids.timeIn).value;
			var msg = "";
			if (nval != this.timeIn) {
				this.timeIn = nval;
				this.tspent = this.recalc1();
				msg = "timeIn updated to:[" + nval + "] ts:[" + this.tspent
						+ "]";
			}
			nval = dojo.byId(this.ids.timeOut).value;
			if (nval != this.timeOut) {
				this.timeOut = nval;
				this.tspent = this.recalc1();
				msg = "timeOut updated to:[" + nval + "] ts:[" + this.tspent
						+ "]";
			}
			nval = dojo.byId(this.ids.tmins).value;
			if (nval != this.tmins) {
				this.tmins = nval;
				this.tspent = this.recalc2();
				msg = "tmins updated to:[" + nval + "] ts:[" + this.tspent
						+ "]";
			}
			nval = dojo.byId(this.ids.thours).value;
			if (nval != this.thours) {
				this.thours = nval;
				this.tspent = this.recalc2();
				msg = "thours updated to:[" + nval + "] ts:[" + this.tspent
						+ "]";
			}
			if (isNaN(this.tspent))
				this.tspent = 0;
			dojo.byId(this.ids.tspent).innerHTML = this.tspent;
			console.debug(msg);
		},

		recalc1 : function() {
			this.tmins = "0";
			this.thours = "0";
			dojo.byId(this.ids.tmins).value = "0";
			dojo.byId(this.ids.thours).value = "0";

			if ((this.timeIn == "") || (this.timeOut == ""))
				return 0;
			var wdt = new Date();
			var indt = new Date(wdt.toLocaleDateString() + " " + this.timeIn);
			var outdt = new Date(wdt.toLocaleDateString() + " " + this.timeOut);
			console.debug("indt:[" + indt.toLocaleTimeString() + "] outdt:["
					+ outdt.toLocaleTimeString() + "]");
			var msecs = outdt.getTime() - indt.getTime();
			console.debug("msces:[" + msecs + "]");
			if (msecs < 0)
				return 0;
			// return (msecs / 1000) / 3600;
			return Math.round((msecs / 10) / 3600) / 100;
			return "WIP"
		},

		recalc2 : function() {
			this.timeIn = "";
			this.timeOut = "";
			dojo.byId(this.ids.timeIn).value = "";
			dojo.byId(this.ids.timeOut).value = "";
			/*
			 * hr := @If( @IsNumber(Hours); (Hours * 60); 0 ); Min := @If(
			 * @IsNumber(Min); Min; 0); Time := Hr + Min;
			 */
			var hr = parseFloat(this.thours);
			if (isNaN(hr)) {
				hr = 0;
				this.thours = "0";
				dojo.byId(this.ids.thours).value = "0";
			}
			var min = parseFloat(this.tmins);
			if (isNaN(min)) {
				min = 0;
				this.tmins = "0";
				dojo.byId(this.ids.tmins).value = "0";
			}

			var ts = Math.round(((hr * 60) + min) / 60 * 100) / 100;
			return ts;
		},

		save : function(sparms) {
			// console.debug("in hs_timeTracking.save()")
			this.saveStatus = "posting";
			this.saveMessages = [];
			var msgs = dojo.byId(this.ids.messages);
			this.saveParms = (sparms) ? sparms : {
				saveinsp : false, 
				submitinsp : false
			}
			msgs.innerHTML = "";
			dojo.style(msgs.parentNode.parentNode, "display", "none");
			hs_inspectionCSCode.waitPanelShow();
			
			XSP.partialRefreshPost(this.ids.saveCF1, {
				params : {
					saveMode : "save",
					timeTrackingSave : "true",
					tspent : this.tspent
				},
				onComplete : function() {
					var status = hs_timeTracking.saveStatus;
					console.log("time tracking Save Status:[" + status + "]");
					if (status.toLowerCase() == "success") {
						var v2 = (hs_timeTracking.launchParms == null) ? false : hs_timeTracking.launchParms.v2; 
						if(!v2) {
							setTimeout( function() {
								hs_inspectionCSCode.waitPanelHide();
								location = "#frmCreateInspection";},
							200);
						} else {
							//var mainid = hs_inspDivs.ids.main;
							console.debug("[hs_timeTracking.save]: Refreshing [" + hs_timeTracking.ids.timeTrackingViewPanel + "]");
							XSP.partialRefreshGet(hs_timeTracking.ids.timeTrackingViewPanel, {
								onComplete : function () {
									console.debug("switching to main tab...");
									hs_inspDivs.show("main", function () {
										if(hs_timeTracking.saveParms.saveinsp) {
											setTimeout(function () {
												hs_inspectionCSCode.saveInspection({submit:hs_timeTracking.saveParms.submitinsp});
											},2);
										} else {
											hs_inspectionCSCode.waitPanelHide();
										}
									});
								},
								onError : "hs_timeTracking.errHandler(arguments[0], arguments[1])"
							})
						}
					} else {
						var msgs = dojo.byId(hs_timeTracking.ids.messages);
						var smsgs = hs_timeTracking.saveMessages;
						for ( var i = 0; i < smsgs.length; i++) {
							smsgs[i] = unescape(smsgs[i]);
						}
						msgs.innerHTML = smsgs.join("<br/>") + "<br/>status: "
								+ status;
						dojo.style(msgs.parentNode.parentNode, "display",
								"block");
						hs_inspectionCSCode.waitPanelHide();
					}
				},
				onError : function() {
					console.error("Error during timeTrackingSave.");
					var msgs = dojo.byId(hs_timeTracking.ids.messages);
					msgs.innerHTML = "Error during timeTrackingSave.";
					dojo.style(msgs.parentNode.parentNode, "display", "block");
					hs_inspectionCSCode.waitPanelHide();
				}
			});
		},

		saveAndNew : function() {
			alert("WIP!");
		},

		launchParms : null, 
		
		launchNew : function(parms) {
			hs_inspectionCSCode.waitPanelShow();
			
			this.thours = "";
			this.timeIn = "";
			this.timeOut = "";
			this.tmins = "";
			this.tspent = "";
			
			this.launchParms = (parms) ? parms : null;
			setTimeout("hs_timeTracking.launchNewStep2()", 200);
		},

		launchNewStep2 : function() {
			XSP.partialRefreshPost(this.ids.newCf1, {
				params : {
					newTimeTracking : true
				},
				onComplete : function() {
					setTimeout(function(){hs_timeTracking.launchNewStep3();},1);
					
				},
				onError : "hs_timeTracking.errHandler(arguments[0], arguments[1])"
			})
		},
		
		launchNewStep3 : function() {
			var dbugloc = "[hs_timeTracking.launchNewStep3]: ";
			//var sdiv = dojo.byId(hs_timeTracking.ids.newCf1);
			//var smsgs = sdiv.innerHTML;
			var smsgs = x$(hs_timeTracking.ids.newCf1).html();
			console.debug(dbugloc + hs_timeTracking.ids.newCf1 + " is:[" + smsgs + "]");
			var sok = (smsgs.indexOf("failed") > -1) ? false : true;
//			var msgdiv = dojo.byId(hs_timeTracking.ids.newMessages);
//			dojo.style(msgdiv.parentNode.parentNode, "display", "block");
			x$(hs_timeTracking.ids.messagesFr1).show();
			if (!sok) {
				//msgdiv.innerHTML = smsgs;
				x$(hs_timeTracking.ids.newMessages).html(smsgs);
				hs_inspectionCSCode.waitPanelHide();
			} else {
				//msgdiv.innerHTML = "Redirecting to frmTimeTracking..."
				x$(hs_timeTracking.ids.newMessages).html("");
				setTimeout( function() {
					var v2 = (hs_timeTracking.launchParms == null) ? false : hs_timeTracking.launchParms.v2; 
					if(!v2) {
						console.debug("v1 is not supported with Bootstrap");
						hs_inspectionCSCode.waitPanelHide();
					} else {
						var ttid = hs_inspDivs.ids.timerpt;
						console.debug("[hs_timeTracking.launchNewStep3]: Refreshing [" + ttid + "]");
						XSP.partialRefreshGet(ttid, {
							onComplete : function () {
								console.debug("switching to timerpt tab...");
								hs_inspDivs.show("timerpt", function () {
									hs_inspectionCSCode.waitPanelHide();
								});
							},
							onError : "hs_timeTracking.errHandler(arguments[0], arguments[1])"
						});
					}
					
				}, 2);
			}
		},
		
		errHandler : function() {
			// just do some debugging
			// of ioArgs object
			hs_inspectionCSCode.waitPanelHide();
			var arg = arguments[1];
			var txt = arg.error.message;
			var rtxt = arguments[0].responseText
		// for( p in arg ){
		// txt += p;
		// //txt += " -> " + arg[p];
		// txt += " [" + typeof( arg[p] ) + "]<br/>";
		// }
		// alert( txt );
		console.debug("Error on post to [" + this.ids.newCf1 + "]");
		var msgdiv = dojo.byId(this.ids.newMessages);
		msgdiv.innerHTML = "<h1 style='color:red;'>"
				+ "ERROR opening Time Tracking Page</h1>" + txt;
		dojo.style(msgdiv.parentNode.parentNode, "display", "block");
		dojo.style(dojo.byId(hs_inspectionCSCode.ids.waitpanel), "display",
				"none");
		var newWindow = window.open("", "",
				"height=600,width=600,alwaysRaised=yes");
		newWindow.document.write(rtxt);
		newWindow.document.close();
	},
	
	setWidthsAndAlignment : function() {
		hs_inspectionCSCode.setCBWidths([this.ids.eho, this.ids.timetype]);
		
		var localDebug = false;
		var dbugLoc = "[hs_timeTracking.setWidthsAndAlignment]: ";
		var wrkids = [this.ids.thours, this.ids.tmins];
		var wrkfld = null;
		for(var i=0; i<wrkids.length; i++) {
			wrkfld = dojo.byId(wrkids[i]);
			if(wrkfld != null) {
				if(localDebug) console.debug(dbugLoc + "working with field id:[" + wrkids[i] + "]");
				wrkfld.value = "";
				// THIS WOULD NOT WORK???
				// dojo.style(wrkfld, "text-align", "right");
				// dojo.style(wrkfld, "font-size", "medium");
			} else {
				console.error(dbugLoc + "could not access field with ID:[" + wrkids[i] + "]" )
			}
		}
		dojo.byId(this.ids.tmins).focus();
	}
}