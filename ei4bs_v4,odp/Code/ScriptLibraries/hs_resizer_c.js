
// objectto manage resizing scrollable panes in a single pass of the Single App Page

var hs_resizer = {
	original : null,
	adjustedh : -1,
	adjhw : null,
	resize : function(calledfrom) {
		var localDebug = false;
		var dbugLoc = "hs_resizer.resize";
		if (this.original == null) this.original = dojo.window.getBox();
		var vs = this.original;
		var vs2 = dojo.window.getBox();
		//var ah = this.adjustedh;
		var adjhw = this.adjhw;
		var i = 0;
		var h2 = 0;
		if (localDebug) console.debug("*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#");
		if (localDebug) hs_utils.debug("before window.getBox.h:[" + vs2.h + "] w:[" + vs2.w + "]" +
				" original:h:[" + vs.h + "] w:[" + vs.w + "]", dbugLoc);
		if (localDebug) hs_utils.debug("called from:[" + calledfrom + "]", dbugLoc);
		var nodeList = dojo.query("[id$='hsScrollableArea']");
		dojo.forEach(nodeList, function(node) {
			try{
			if(adjhw == null) {
				if(calledfrom == "hs_resizer.onResize()") {
					if(node.clientHeight > 1) {
						var rsd = hs_resizer.resizeDelta;
						if (localDebug) hs_utils.debug("resizing:[" + node.id + "] " + 
								"from node.h:[" + node.clientHeight + "] node.w:[" + node.clientWidth + "] " +
								"resizeDelta.h:[" + rsd.h + "] w:[" + rsd.w + "]", dbugLoc);
						var th = 0; 
						var tw = 0;
						
						if(rsd.h == 0 && rsd.w != 0) {
							th = node.clientHeight - 29;
							dojo.style(node, "height", "" + th + "px");
						} else if(rsd.h != 0 && rsd.w == -17) {
							th = node.clientHeight - 29;
							dojo.style(node, "height", "" + th + "px");
						}
						
						if (localDebug) hs_utils.debug("resizing step 2:[" + 
								"from node.h:[" + node.clientHeight + "] node.w:[" + node.clientWidth + "] " +
								"resizeDelta.h:[" + rsd.h + "] w:[" + rsd.w + "]", dbugLoc);
							
						th = node.clientHeight + rsd.h;
						dojo.style(node, "height", "" + th + "px");
						
						//tw = (node.clientWidth + rsd.w) ;
						//dojo.style(node, "width", "" + tw + "px");
						
						if(rsd.h == 0 && rsd.w != 0) {
							vs2 = dojo.window.getBox();
							tw = vs2.w;
							dojo.style(node, "width", "" + tw + "px");
						} else if(rsd.h != 0 && rsd.w == 0) {
							vs2 = dojo.window.getBox();
							tw = vs2.w;
							dojo.style(node, "width", "" + tw + "px");
						}
						
						i = th;
						hs_resizer.adjhw= {
								h : node.clientHeight,
								w : node.clientWidth
						}
					}
					vs2 = dojo.window.getBox();
				} else {
					var presize = {h:node.clientHeight, w:node.clientWidth};
					dijit.byNode(node).resize();
					if (localDebug) hs_utils.debug("resizing:[" + node.id + "] " + 
						"from node.h:[" + node.clientHeight + "] node.w:[" + node.clientWidth + "] " +
						"presize.h:[" + presize.h + "] presize.w:[" + presize.w + "]", dbugLoc);
					if(node.clientHeight > 1) {
		//				var pnode = node.parentNode;
		//				if (localDebug) hs_utils.debug("parentNode id:[" + pnode.id + "]  " + 
		//						"h:[" + pnode.clientHeight + "] w:[" + pnode.clientWidth + "]" + 
		//						" # of children:[" + pnode.childNodes.length + "]");
		//				if (localDebug) dojo.forEach(pnode.childNodes, function(cnode) {
		//					if(cnode.id) hs_utils.debug("childNode id:[" + cnode.id + "]  " + 
		//							"h:[" + cnode.clientHeight + "] w:[" + cnode.clientWidth + "]");
		//				});
						vs2 = dojo.window.getBox();
						
						i = 0;
						while (((vs.h < vs2.h) || (vs2.w < vs.w)) && (i < 50)) {
							i = i + 1;
							h2 = node.clientHeight - 1;
							dojo.style(node, "height", "" + h2 + "px");
							vs2 = dojo.window.getBox();
						}
						//hs_resizer.adjustedh = node.clientHeight;
						hs_resizer.adjhw= {
								h : node.clientHeight,
								w : node.clientWidth
						}
						if (localDebug) hs_utils.debug(node.id + " adjusted to: " +
								"node:h:[" + node.clientHeight + "] w:[" + node.clientWidth + "]" +
								"window:h:[" + vs2.h + "] w:[" + vs2.w + "] in [" + i + "] steps." +
								" hs_resizer.adjhw.h:[" + hs_resizer.adjhw.h + "]" +
								" hs_resizer.adjhw.w:[" + hs_resizer.adjhw.w + "]", dbugLoc);
					}
				}
			} else {
				if(node.clientHeight > 1) {
					dojo.style(node, "height", "" + adjhw.h + "px");
					dojo.style(node, "width", "" + adjhw.w + "px");
					if (localDebug) hs_utils.debug(node.id + " adjusted to: " +
						"node:h:[" + node.clientHeight + "] w:[" + node.clientWidth + "]" +
						" using adjhw in hs_resizer object.", dbugLoc);
				} else if(node.id == calledfrom) {
					dojo.style(node, "height", "" + adjhw.h + "px");
					dojo.style(node, "width", "" + adjhw.w + "px");
					if (localDebug) hs_utils.debug(node.id + " adjusted to: " +
						"node:h:[" + node.clientHeight + "] w:[" + node.clientWidth + "]" +
						" using adjhw in hs_resizer object because it was calledfrom node..", dbugLoc);
				} else {
					// if (localDebug) hs_utils.debug("skipping [" + node.id + "]");
				}
			}
			} catch(e) {
				console.error(dbugLoc + e.toString());
			}
		})
		if(localDebug) {
			vs2 = dojo.window.getBox();
			hs_utils.debug("after: window.getBox.h:[" + vs2.h + "] w:[" + vs2.w + "]", dbugLoc);
		}
		
	},
	resizeTo : null,
	resizeDelta : null,
	resizeOrig : null,
	
	onResize : function () {
		var localDebug = false;
		var dbugLoc = "[hs_resizer.onResize]: ";
		
		var vs = dojo.window.getBox();
		var vso = this.original;
		if(vso == null) {
			vso = this.resizeOrig;
		} else {
			this.resizeOrig = {h:vso.h, w:vso.w};
		}
		
		if(this.resizeTo != null) clearTimeout(this.resizeTo); // cancel the resize timeout
		this.resizeDelta = {h : (vs.h - vso.h), w: (vs.w - vso.w) };
		this.adjhw = null;
		this.original = null;
		if (localDebug) hs_utils.debug("onResize triggered - " + 
				"new h:[" + vs.h + "] w:[" + vs.w + "] " + 
				"original h:[" + vso.h + "] w:[" + vso.w + "] " +
				"delta h:[" + this.resizeDelta.h + "] w:[" + this.resizeDelta.w + "]", dbugLoc);
		this.resizeTo = setTimeout(function(){hs_resizer.resize("hs_resizer.onResize()")}, 500);
	},
	onOrientationChange : function() {
		console.debug("onOrientationChange triggered");
	}
}

if(!hs_executeOnServer) {
	var hs_executeOnServer = function () {
	
	    // must supply event handler id or we're outta here....
	    if (!arguments[0]) return false;
	
	    // the ID of the event handler we want to execute
	    var functionName = arguments[0];
	
	    // OPTIONAL - The Client Side ID that you want to partial refresh after executing the event handler
	    var refreshId = (arguments[1]) ? arguments[1] : "@none";
	    var form = (arguments[1]) ? XSP.findForm(arguments[1]) : dojo.query('form')[0];
	
	    // OPTIONAL - Options object contianing onStart, onComplete and onError functions for the call to the
	    // handler and subsequent partial refresh
	    var options = (arguments[2]) ? arguments[2] : {};
	
	    // Set the ID in $$xspsubmitid of the event handler to execute
	    dojo.query('[name="$$xspsubmitid"]')[0].value = form.id + ':' + functionName;
	    XSP._partialRefresh("post", form, refreshId, options);
	}
}

//from Keith Stickland via NotesIn9 Episode 121.
//function to resize all scrollable panes in a single pass of the Single App Page
var hs_resize = function() {
	var dbugLoc = "hs_resize";
	var localDebug = false;
	var nodeList = dojo.query("[id$='scrollableContent']");
	if (localDebug) hs_utils.debug("resizing [" + nodeList.length + "] nodes.", dbugLoc);
	dojo.forEach(nodeList, function(node) {
		dijit.byNode(node).resize();
		if (localDebug) {
			try {
				localDebug = false;
				hs_utils.debug("resized [" + node.id + "]");
				//hs_utils.debug("resized [" + node.id + "] to h:[" + output.h + "] w:[" + output.w + "] " + 
				//			"t:[" + output.t + "] l:[" + output.l + "] b:[" + output.b + "] t:[" + output.r + "] ");
			} catch (e){
				console.error(dbugLoc + "ERROR: " + e.toString());
			}
		}
	});
};

