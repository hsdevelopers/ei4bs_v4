// Methods for inspection xpages:
function inspMarkSectionStatus(section, status) {
	var localDebug = true;
	var dbarLoc = "inspectionRoutines.inspMarkSectionStatus";
	try {
		//var violSectionCB1:com.ibm.xsp.component.xp.XspSelectOneMenu = getComponent("violSectionCB1");
		//var section = violSectionCB1.getValue();
		if (localDebug) dBar.debug("In section is:[" + section + "] status:[" + status + "]", dbarLoc);
		var sdata = inspection.getPlSectionsData();
		if (localDebug) dBar.debug("PlSectionsData has:[" + sdata.length + "] elements", dbarLoc);
		var wdata = null;
		for(var sidx = 0; sidx<sdata.length; sidx++) {
			if(sdata[sidx].number.equals(section)) {
				wdata = sdata[sidx]
				var newsect = new com.healthspace.general.InspectionSection(wdata.group, wdata.category, 
										unescape(wdata.description), unescape(wdata.module), wdata.number, status);
				inspection.getSections().add(newsect);
				if (localDebug) dBar.debug("New section created:[" + section + "]" + 
								" grp:[" + wdata.group + "] cat:[" + wdata.category + "]" + 
								" desc:[" + unescape(wdata.description) + "]" +
								" status:[" + status + "]", dbarLoc);
				return; 
			}
		}	
		throw(new Exception("Could not find matching section for [" + section + "]"))
	} catch (e) {
		dBar.error(e.toString(), dbarLoc);
	}
}

function inspShowViolationOverview(doc1:NotesXspDocument, fldName:string) {
	/*
	 * Original HIDE-WHEN code:
	 * UseCodes := @GetProfileField( "MasterSettings"; "FoodCode2009Districts" );
	 * CodeStartDate := @GetProfileField( "MasterSettings"; "FoodCode2009Date" );
	 * IsAfterDate := @If( @IsTime( CodeStartDate ); 
	 * 				@If( InspectionDate >= CodeStartDate; @True; @False ); 
	 * 				@False );
	 * 
	 * ( (Form = "FoodReport" | Form = "AgricultureReport") & 
	 * UseCodes = "Yes" & IsAfterDate ) | 
	 * ViolationOverview = "False" | DBDesign = "Florida"
	 */
	var dbarLoc = "inspectionRoutines.inspShowViolationOverview";
	var localDebug = false;
	var scopeData = {};
	var rslt = false;
	var scopeKey = "HS_inspShowViolationOverview";
	//var startTime = new Date().getTime();
	try {
		//if (localDebug) dBar.debug("Processing fldName:[" + fldName + "]", dbarLoc);
		
		scopeData = viewScope.get(scopeKey);
		if (scopeData == null) {
			//rslt = ("true".equals(scopeData));
			//if(localDebug) dBar.debug("returning [" + scopeData + "][" + rslt + "]" + 
			//		" from scope", dbarLoc);
			//return rslt;
			
			scopeData = {
				codeStartDate : [],
				dbDesign : "",
				form : "",
				inspectionDate : null,
				isAfterDate : false,
				useCodes : "",
				violationOverview : ""
			}
			var wrkDate:NotesDateTime = getMasterSettingsDateItem("FoodCode2009Date");
			wrkDate =(wrkDate == null) ? null : 
				(wrkDate.size() == 0) ? null : wrkDate[0];
			scopeData.codeStartDate = wrkDate.toJavaDate();
			if(localDebug) dBar.debug("codeStartDate:[" + scopeData.codeStartDate + "] " + 
					"type:[" + typeof scopeData.codeStartDate + "]", dbarLoc);
			
			scopeData.dbDesign = doc1.getItemValue("DBDesign").get(0);
			if(localDebug) dBar.debug("dbDesign:[" + scopeData.dbDesign + "] " + 
					"type:[" + typeof scopeData.dbDesign + "]", dbarLoc);
			
			scopeData.form = doc1.getItemValue("Form").get(0);
			if(localDebug) dBar.debug("form:[" + scopeData.form + "] " + 
					"type:[" + typeof scopeData.form + "]", dbarLoc);
			
			wrkDate = doc1.getItemValue("InspectionDate").get(0);
			scopeData.inspectionDate = wrkDate.toJavaDate();  
			if(localDebug) dBar.debug("inspectionDate:[" + scopeData.inspectionDate + "] " + 
					"type:[" + typeof scopeData.inspectionDate + "]", dbarLoc);
			
			scopeData.isAfterDate = (scopeData.codeStartDate == null) ? false : 
				(!("lotus.domino.local.DateTime".equalsIgnoreCase(typeof scopeData.codeStartDate))) ? false :
				(scopeData.inspectionDate >= scopeData.codeStartDate);
			if(localDebug) dBar.debug("isAfterDate:[" + scopeData.isAfterDate + "] " + 
					"type:[" + typeof scopeData.isAfterDate + "]", dbarLoc);
			
			scopeData.useCodes = getMasterSettingsItem("FoodCode2009Districts");
			scopeData.useCodes =(scopeData.useCodes != null) ? scopeData.useCodes[0] : "";
			if(localDebug) dBar.debug("useCodes:[" + scopeData.useCodes + "] " + 
					"type:[" + typeof scopeData.useCodes + "]", dbarLoc);
			
			var tempv = doc1.getItemValue("ViolationOverview");
			scopeData.violationOverview = (tempv.size() > 0) ? tempv.get(0) : "";
			if(localDebug) dBar.debug("violationOverview:[" + scopeData.violationOverview + "] " + 
					"type:[" + typeof scopeData.violationOverview + "]", dbarLoc);
			
			viewScope.put(scopeKey, scopeData);
		}
		
		rslt = ("FoodReport".equals(scopeData.form) || "AgricultureReport".equals(scopeData.form) );
		rslt = (rslt && "Yes".equals(scopeData.useCodes) && scopeData.isAfterDate);
		rslt = !(rslt || "False".equals(scopeData.violationOverview) || "Florida".equals(scopeData.dbDesign));
		
		if(localDebug) dBar.debug("returning [" + rslt + "]" + 
				" from computed version", dbarLoc);
		
		return rslt;
	} catch (e) {
		dBar.error("ERROR: " + e.toString(), dbarLoc);
		//importPackage("com.healthspace.general");
		//if(HS_Util.isDebugServer()) print(dbarLoc + ": ERROR:" + e.toString());
		return true;
	}
}

function buildViolationDisplay(doc:NotesXspDocument, critSet) {
	var dbarLoc = "inspectionRoutines.buildViolationDisplay";
	var localDebug = false;
	var rslt = [];
	var config = [];
	var fldPrefix = ("corrected".equalsIgnoreCase(critSet)) ? "ViolCorrected" : "Viol";
	config.push({fld: fldPrefix+ "Code", tag: "code"});
	config.push({fld: fldPrefix+ "CorrectedSet", tag: "corrected"});
	config.push({fld: fldPrefix+ "CorrectiveActions", tag: "action"});
	config.push({fld: fldPrefix+ "CriticalSet", tag: "criticalSet"});
	config.push({fld: fldPrefix+ "Description", tag: "description"});
	config.push({fld: fldPrefix+ "HzrdRating", tag: "hazardRating"});
	config.push({fld: fldPrefix+ "LegalTxt", tag: "legal"});
	config.push({fld: fldPrefix+ "Observations", tag: "observation"});
	config.push({fld: fldPrefix+ "Repeat", tag: "repeat"});
	config.push({fld: fldPrefix+ "Section", tag: "section"});
	config.push({fld: fldPrefix+ "Status", tag: "status"});
	var viol = {}
	var idx = 0;
	try {
		for(idx=0; idx<config.length; idx++) {
			viol[config[idx].tag] = "";
		}
		
		var data=[];
		var wval = null;
		var maxval = 0;
		for(idx=0; idx<config.length; idx++) {
			wval = doc.getItemValue(config[idx].fld);
			data.push(wval);
			maxval = (wval.length > maxval) ? wval.length : maxval;
		}
		if (localDebug) dBar.debug("got data for [" + maxval + "] violations. critSet:[" + critSet + "]", dbarLoc);
		
		for(var vidx=0; vidx < maxval; vidx++) {
			viol= {};
			for(idx=0; idx<config.length; idx++) {
				wval = (vidx >= data[idx].length) ? "" : data[idx][vidx];
				wval = (" ".equals(wval)) ? "" : wval;
				//if (localDebug) dBar.debug("for violation:[" + vidx + "] " + 
				//		config[idx].tag + " is:[" + wval + "]", dbarLoc);
				viol[config[idx].tag] = wval;
			}
			// Are we only looking up specific violations types? 
			if (!critSet) {
				viol.legal = "lookup legal again.";
				rslt.push(viol);
			} else if ("corrected".equalsIgnoreCase(critSet)) {
				viol.legal = "lookup legal again.";
				rslt.push(viol);
			} else if (critSet.equalsIgnoreCase(viol.criticalSet)){
				viol.legal = "lookup legal again.";
				rslt.push(viol);
			// Added this to include as non critical violations with blank values.
			} else if ("non-critical".equalsIgnoreCase(critSet) && "".equals(viol.criticalSet)) {
				viol.legal = "lookup legal again.";
				rslt.push(viol);
			}
		}
		viewScope.put("AA_Debug_Violations", rslt);
		
		if(!critSet) return rslt;
		lookupLegalAgain(rslt, doc.getItemValueString("ViolationModule"));
		return rslt;
	} catch (e) {
		dBar.error(e.toString(), dbarLoc);
		for(idx=0; idx<config.length; idx++) {
			viol[config[idx].tag] = "";
		}
		viol.section="ERROR";
		viol.code="error";
		viol.description = e.toString();
		rslt.push(viol);
	}
	return rslt;
}

function lookupLegalAgain(viols, vmodule) {
	var dbarLoc = "inspectionRoutines.lookupLegalAgain";
	var localDebug = false;
	var startTime = new Date().getTime();
	var cdt = null;
	
	try {
		var tdb:NotesDatabase = getDb("eiRoot");
		var oView:NotesView = tdb.getView("XPagesObservationsByModule");
		oView.setAutoUpdate(false);
		var okeys = [vmodule, ""];
		var vec:NotesViewEntryCollection = oView.getAllEntriesByKey(vmodule, true)
		var nve:NotesViewEntry = null;
		var tnve:NotesViewEntry = null;  
		for (var i=0; i<viols.length; i++) {
			//okeys[1] = viols[i].code;
			//nve = oView.getEntryByKey(okeys, true);
			nve = null;
			tnve = vec.getFirstEntry();
			while (tnve != null) {
				if(viols[i].code.equals(tnve.getColumnValues().get(1))) {
					nve = tnve;
					tnve = null;
				} else {
					tnve = vec.getNextEntry(tnve);
				}
			}
			
			cdt =  new Date().getTime() - startTime;
			if(nve == null) {
				if (localDebug) dBar.warn("for code:[" + viols[i].code + "] no entry found. [" + cdt + "]", dbarLoc);
			} else {
				viols[i].legal = nve.getColumnValues().get(4);
				if (localDebug) dBar.debug("for code:[" + viols[i].code + "[ new legalText is: [" + @Left(viols[i].legal, "<br/>") + "] [" + cdt + "]", dbarLoc);
			}
		}
	} catch(e) {
		dBar.error(e.toString(), dbarLoc);
	}
}

function xbuildTemperatureDisplay(doc:NotesXspDocument, ttype:String) {
	var dbarLoc = "inspectionRoutines.xbuildTemperatureDisplay";
	var localDebug = hs_util.isDebugServer();
	var rslt = [];
	
	var ndoc:NotesDocument = doc.getDocument(true);
	if ("equipment".equals(ttype)) {
		if (ndoc.hasItem("EquipDescrip")) {
			var edescs = ndoc.getItemValue("EquipDescrip");
			var etemps = ndoc.getItemValue("EquipTemp");
			if(edescs.size() == 0 ) {
				rslt.push("No equipment temperatures recorded");
			} else {
				while (etemps.size() < edescs.size()) {
					etemps.push("n/r");
				}
				for(var i=0; i<edescs.size(); i++) {
					// "Description: " + @Subset(@Subset(EquipDescrip; i); - 1) + "      ";
					// sEquipTemp := @If(@Subset(@Subset(EquipTemp ; i); - 1) = " "; ""; 
					//							"Temperature: " + @Subset(@Subset(EquipTemp ; i); - 1) + "ºF");
					rslt.push("Description: " + edescs.get(i) + "      " + 
							"Temperature: " + etemps.get(i) + "ºF");
				}
			}
		} else {
			rslt.push("No equipment temperatures recorded");
		}
	} else if("food".equals(ttype)) {
		if (ndoc.hasItem("FoodDescrip")) {
			var fdescs = ndoc.getItemValue("FoodDescrip");
			var ftemps = ndoc.getItemValue("FoodTemp");
			var fstates = ndoc.getItemValue("FoodState");
			if(fdescs.size() == 0 ) {
				rslt.push("No food temperatures recorded");
			} else {
				while (ftemps.size() < fdescs.size()) {
					ftemps.push("");
				}
				while (fstates.size() < fdescs.size()) {
					fstates.push("");
				}
				
				for(var i=0; i<fdescs.size(); i++) {
					var nrslt = "Description: " + fdescs.get(i) + "      ";
					nrslt += ("".equals(ftemps.get(i))) ? "" : "Temperature: " + ftemps.get(i) + "ºF   ";
					nrslt += ("".equals(@Trim(fstates.get(i)))) ? "" : "State of food: " + fstates.get(i);
					rslt.push(nrslt);
				}
			}
		} else {
			rslt.push("No ware washing temperatures recorded");
		}
	} else if("washing".equals(ttype)) {
		if (ndoc.hasItem("MachineName")) {
			var mdescs = ndoc.getItemValue("MachineName");
			/*****
			var ftemps = ndoc.getItemValue("FoodTemp");
			var fstates = ndoc.getItemValue("FoodState");
			if(fdescs.size() == 0 ) {
				rslt.push("No ware washing temperatures recorded");
			} else {
				while (ftemps.size() < fdescs.size()) {
					ftemps.push("");
				}
				while (fstates.size() < fdescs.size()) {
					fstates.push("");
				}
				
				for(var i=0; i<edescs.size(); i++) {
					var nrslt = "Description: " + fdescs.get(i) + "      ";
					nrslt += ("".equals(ftemps.get(i))) ? "" : "Temperature: " + ftemps.get(i) + "ºF   ";
					nrslt += ("".equals(fstates.get(i))) ? "" : "State of food: " + fstate.get(i);
					rslt.push(nrslt);
				}
			}*******/
			rslt.push("Ware washing temperatures WIP");
		} else {
			rslt.push("No ware washing temperatures recorded");
		}	
	}
	return rslt;
}
function updateInspectionViewDescription(inspDoc:NotesXspDocument) {
	/*
	 * Months := "Jan":"Feb":"Mar":"Apr":"May":"Jun":"Jul":"Aug":"Sep":"Oct":"Nov":"Dec";
	 * 
	 * idate := @If(@IsTime(InspectionDate);
	 * 			" on " + @Text( @Day(InspectionDate)) + "-" + @Subset( @Subset( Months; @Month(InspectionDate));-1)  + "-" + @Text( @Year(InspectionDate)); 
	 * 			"");
	 * 
	 * strReleaseDate := @If(@IsTime(ReleaseDate);
	 * 			" on " + @Text(@Day(ReleaseDate)) + "-" + @Subset(@Subset(Months;@Month(ReleaseDate));-1)  + "-" + @Text(@Year(ReleaseDate)); 
	 * 			"");
	 * 
	 * strStatus := @If(Enforcement="Release";
	 * 			", License Status: " + Enforcement + strReleaseDate; 
	 * 			Enforcement!=""; ", License Status: " + Enforcement;
	 * 			"");
	 * 
	 * strType := @If(@IsMember(Type;"Re-inspection":"Follow-up");
	 * 			"Re-inspection";
	 * 			@IsMember(Type;"FU");
	 * 				"Follow up";
	 * 			Type);
	 * 
	 * "Inspection " + strType + idate + ", by " + EHO + strStatus + strRelease
	 */
	var dbarLoc = "inspectionRoutines.sjs.updateInspectionViewDescription";
	var localDebug = false;

	var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	var itype:string = inspDoc.getItemValueString("Type");
	var strType = (@IsMember(itype, ["Re-inspection","Follow-up"])) ? "Re-inspection" : 
					@IsMember(itype, ["FU"]) ? "Follow up" : itype;
	
	var idt= (!(inspDoc.hasItem("InspectionDate"))) ? "" : inspDoc.getItemValue("InspectionDate")[0];
	var strIDate = "";
	var jdt:Date = null;
	if((typeof idt).indexOf("DateTime") > 0) {
		jdt = idt.toJavaDate();
		strIDate += " on " + jdt.getDate();
		strIDate += "-" + months[jdt.getMonth()];
		strIDate += "-" + jdt.getFullYear();
	}
	
	var strEho = ", by " + inspDoc.getItemValueString("EHO");
	
	var strStatus = "";
	var enforcement = (!(inspDoc.hasItem("Enforcement"))) ? "" :  inspDoc.getItemValueString("Enforcement");
	if("Release".equals(enforcement)){
		strStatus += ", License Status: " + enforcement;
		
		var rdt = (!(inspDoc.hasItem("Release"))) ? "" : inspDoc.getItemValue("Release")[0];
		if((typeof rdt).indexOf("DateTime") > 0) {
			jdt = rdt.toJavaDate()
			strStatus += " on " + jdt.getDate + "-" + months[jdt.getMonth()]  + "-" + jdt.getFullYear(); 
		}
	} else if(!("".equals(enforcement))){
		strStatus = ", License Status: " + enforcement
	}
	var vdesc = "Inspection " + strType + strIDate + strEho + strStatus
	return vdesc;
}

function updateInspectionDate(inspDoc:NotesXspDocument) {
	var dt:NotesDateTime = session.createDateTime(@Today());
	dt.setAnyTime();
	inspDoc.replaceItemValue("InspectionDate", dt);
	return dt;
}

function inspectionPreloadClear(vskey) {
	var localDebug = false;
	var dbarLoc = "inspectionRoutines.ssjs.inspectionPreloadClear";
	var hsiSsKey = "HS_InspectionViewData";
	
	var vsdata = viewScope.get(vsKey);
	var wtdci = "";
	var insertCsjs = "";
	if(vsdata == null) {
		wtdci = "pending";
	} else if("clear".equals(vsdata.wtd)){
		wtdci = "processing"
	} else {
		wtdci = vsdata.clear;
	}
	if("processing".equals(wtdci)) {
		try {
			var sdate:Date = new Date();
			var msecs = 0;
			vsdata.clear = wtdci;
			viewScope.put(vsKey, vsdata);
			
			if (localDebug) dBar.debug("Start processing", dbarLoc);
			inspection.clearInspection();
			wtdci = "Cleared Inspection";
				msecs = new Date().getTime() - sdate.getTime();
				wtdci += " [" + msecs + "]";			
			vsdata.clear = wtdci;
			if (localDebug) dBar.debug(wtdci, dbarLoc);
			vsdata.insertCsjs = "";
			viewScope.put(vsKey, vsdata);
			
			var hsinsp = sessionScope.get(hsiSsKey);
			if (hsinsp != null) {
				inspection.setParentUnid(hsinsp.facUnid);
				msecs = new Date().getTime() - sdate.getTime();
				insertCsjs += "hs_inspectionPreload.msecs.push('after setParentUnid:" + msecs + "');"
				
				if (localDebug) dBar.debug("Calling getViolationModule.", dbarLoc);
				var vmodule = inspection.getViolationModule();
				msecs = new Date().getTime() - sdate.getTime();
				if (localDebug) dBar.debug("vmodule came back as:[" + 
											@Implode(vmodule, "][") + 
											"] in [" + msecs + "] msecs", dbarLoc);
				insertCsjs += "hs_inspectionPreload.msecs.push('after getViolationModule:" + msecs + "');"
				
				wtdci = "loaded vmodule '" + vmodule[0] + "'";
					msecs = new Date().getTime() - sdate.getTime();
					wtdci += " [" + msecs + "]";
				insertCsjs += "hs_inspectionPreload.vmodule='" + vmodule[0] + "';";
				
				insertCsjs += inspectionPreloadNewInspdata(hsinsp);
				vsdata.clear = wtdci;
				if (localDebug) dBar.debug(wtdci, dbarLoc);
				vsdata.insertCsjs = insertCsjs;
				vsdata.vmodule = vmodule[0];
				viewScope.put(vsKey, vsdata);
			}
		} catch(e) {
			dBar.error(e.toString(), dbarLoc);
			if(hs_util.isDebugServer())e.printStackTrace();
			wtdci = "error";
			vsdata.clear = wtdci;
			viewScope.put(vsKey, vsdata);
		}
	} else {
		if (localDebug) dBar.debug("not doing anything now: [" + wtdci + "]", dbarLoc);
	}
	return wtdci;
}

function inspectionFollowupClear( vskey ) {
	var localDebug = false;
	var dbarLoc = "inspectionRoutines.ssjs.inspectionFollowupClear";
	var hsiSsKey = "HS_InspectionViewData";
	
	var vsdata = viewScope.get(vsKey);
	var wtdci = "";
	var insertCsjs = "";
	if(vsdata == null) {
		wtdci = "pending";
	} else if("followUpClear".equals(vsdata.wtd)){
		wtdci = "processing"
	} else {
		wtdci = vsdata.fuclear;
	}
	if("processing".equals(wtdci)) {
		try {
			var sdate:Date = new Date();
			var msecs = 0;
			vsdata.fuclear = wtdci;
			viewScope.put(vsKey, vsdata);
			
			if (localDebug) dBar.debug("Start processing", dbarLoc);
			inspection.clearInspection();
			wtdci = "Cleared Inspection";
			if(localDebug) {
				msecs = new Date().getTime() - sdate.getTime();
				wtdci += " [" + msecs + "]";
				dBar.debug(wtdci, dbarLoc);
			}
			vsdata.fuclear = wtdci;
			vsdata.insertCsjs = "";
			viewScope.put(vsKey, vsdata);
			
			var srcunid = param.get("srcunid");
			if(localDebug) dBar.debug("srcunid:[" + srcunid + "]");
			vsdata.srcunid = srcunid;
			viewScope.put(vsKey, vsdata);
			
			var hsinsp = sessionScope.get(hsiSsKey);
			if (hsinsp != null) {
				inspection.setParentUnid(hsinsp.facUnid);
				msecs = new Date().getTime() - sdate.getTime();
				insertCsjs += "hs_inspectionPreload.msecs.push('after setParentUnid:" + msecs + "');"
				
				var vmodule = inspection.getViolationModule();
				msecs = new Date().getTime() - sdate.getTime();
				if (localDebug) dBar.debug("vmodule came back as:[" + 
											@Implode(vmodule, "][") + 
											"] in [" + msecs + "] msecs", dbarLoc);
				insertCsjs += "hs_inspectionPreload.msecs.push('after getViolationModule:" + msecs + "');"
				
				wtdci = "loaded vmodule '" + vmodule[0] + "'";
					msecs = new Date().getTime() - sdate.getTime();
					wtdci += " [" + msecs + "]";
				insertCsjs += "hs_inspectionPreload.vmodule='" + vmodule[0] + "';";
				
				insertCsjs += inspectionPreloadFollowupInspdata(hsinsp, srcunid);
				vsdata.clear = wtdci;
				vsdata.insertCsjs = insertCsjs;
				vsdata.vmodule = vmodule[0];
				viewScope.put(vsKey, vsdata);
			} else {
				dBar.error("sessionScope." + hsiSsKey + " not found!", dbarLoc);
			}
			
			//if(localDebug) dBar.debug("not doing the processing yet.", dbarLoc);
			//wtdci = "not processing yet";
			//vsdata.fuclear = wtdci;
			//viewScope.put(vsKey, vsdata);
		} catch(e) {
			dBar.error(e.toString(), dbarLoc);
			wtdci = "error";
			vsdata.fuclear = wtdci;
			viewScope.put(vsKey, vsdata);
		}
	}
	
	return wtdci;
}

function inspectionPreloadNewInspdata (hsinsp) {
	var localDebug = false;
	var dbarLoc = "inspectionRoutines.ssjs.inspectionPreloadNewInspdata";
	var insertCsjs = "";
	
	/*******
	 * ayr : "",
	 * eho : "",
	 * facname : "",
	 * factype : "",
	 * fureq : "",
	 * inspdatestr : "",
	 * nidatestr : "",
	 * parentunid : "",
	 * riskrating : "",
	 * type : "",
	 * violmodule : ""
	 * 
	************/
	try {
		insertCsjs += "hs_inspectionCSCode.insp.ayr=unescape('" + escape(inspection.getAllYearRound()) + "');";
		insertCsjs += "hs_inspectionCSCode.insp.eho=unescape('" + escape(inspection.getEho()) + "');";
		insertCsjs += "hs_inspectionCSCode.insp.facname=unescape('" + escape(inspection.getFacilityName()) + "');";
		insertCsjs += "hs_inspectionCSCode.insp.parentunid=unescape('" + escape(inspection.getParentUnid()) + "');";
	} catch (e) {
		dBar.error(e.toString(), dbarLoc);
	}
	try {
		var idt:Date = inspection.getInspectionDate();
		var idtfmt = hs_util.formatDate(idt, "yyyy-MM-dd");
		insertCsjs += "hs_inspectionCSCode.insp.inspdatestr=unescape('" + escape(idtfmt) + "');";
	} catch (e) {
		dBar.error(e.toString(), dbarLoc);
		insertCsjs += "hs_inspectionCSCode.insp.inspdatestr=unescape('" + e.toString() +  "');";
	}
	return insertCsjs;
}

function inspectionPreloadFollowupInspdata(hsinsp) {
	var localDebug = false;
	var dbarLoc = "inspectionRoutines.ssjs.inspectionPreloadFollowupInspdata";
	var insertCsjs = "";
	
	try {
		insertCsjs += "hs_inspectionCSCode.insp.ayr=unescape('" + escape(inspection.getAllYearRound()) + "');";
		insertCsjs += "hs_inspectionCSCode.insp.eho=unescape('" + escape(inspection.getEho()) + "');";
		insertCsjs += "hs_inspectionCSCode.insp.facname=unescape('" + escape(inspection.getFacilityName()) + "');";
		insertCsjs += "hs_inspectionCSCode.insp.parentunid=unescape('" + escape(inspection.getParentUnid()) + "');";
	} catch (e) {
		dBar.error(e.toString(), dbarLoc);
	}
	try {
		var idt:Date = inspection.getInspectionDate();
		var idtfmt = hs_util.formatDate(idt, "yyyy-MM-dd");
		insertCsjs += "hs_inspectionCSCode.insp.inspdatestr=unescape('" + escape(idtfmt) + "');";
	} catch (e) {
		dBar.error(e.toString(), dbarLoc);
		insertCsjs += "hs_inspectionCSCode.insp.inspdatestr=unescape('" + e.toString() +  "');";
	}
	try {
		insertCsjs += inspection.loadDataForFollowup(srcunid);	
	} catch (e) {
		dBar.error(e.toString(), dbarLoc);
		insertCsjs += "console.error('[" + dbarLoc + ".loadDataForFollowup]:" + e.toString() +  "');";
	}
	
	return insertCsjs;
}

function xinspectionPreloadSections (vsKey) {
	var localDebug = false;
	var dbarLoc = "inspectionRoutines.ssjs.inspectionPreloadSections";
	var hsiSsKey = "HS_InspectionViewData";
	
	var vsdata = viewScope.get(vsKey);
	var wtdsect = "";
	if(vsdata == null) {
		wtdsect = "pending";
	} else if("sections".equals(vsdata.wtd)){
		wtdsect = "processing"
	} else {
		wtdsect = vsdata.section;
	}
	if("processing".equals(wtdsect)) {
		try {
			var sdate:Date = new Date();
			var msecs = 0;
			vsdata.section = wtdsect;
			viewScope.put(vsKey, vsdata);
			
			if (localDebug) dBar.debug("Start processing", dbarLoc);
			
			var plist = inspection.getPlSectionsData();
			wtdsect = "loaded";
			msecs = new Date().getTime() - sdate.getTime();
			if(localDebug) {
				dBar.debug("sections data came back with: [" + plist.length + 
							"] elements in [" + msecs + "] msecs", dbarLoc);
			}
			wtdsect += " [" + msecs + "] msecs"
			vsdata.section = wtdsect;
			viewScope.put(vsKey, vsdata);
		} catch(e) {
			dBar.error(e.toString(), dbarLoc);
			wtdsect = "error";
			vsdata.section = wtdsect;
			viewScope.put(vsKey, vsdata);
		}
	} else {
		if (localDebug) dBar.debug("not doing anything now: [" + wtdsect + "]", dbarLoc);
	}
	return wtdsect;
}

function xinspectionPreloadViolations (vsKey) {
	var localDebug = false;
	var dbarLoc = "inspectionRoutines.ssjs.inspectionPreloadViolations";

	var vsdata = viewScope.get(vsKey);
	var wtdviol = "";
	if(vsdata == null) {
		wtdviol = "pending";
	} else if("violations".equals(vsdata.wtd)){
		wtdviol = "processing"
	} else {
		wtdviol = vsdata.violations;
	}
	if("processing".equals(wtdviol)) {
		try {
			var sdate:Date = new Date();
			var msecs = 0;
			vsdata.violations = wtdviol;
			viewScope.put(vsKey, vsdata);
			
			var vmodule = inspection.getViolationModule();
			if (localDebug) dBar.debug("vmodule came back as: " + vmodule, dbarLoc);
				
			var plist = inspection.getPlViolationsData();
			wtdviol = "loaded";
			msecs = new Date().getTime() - sdate.getTime();
			if(localDebug) {
				dBar.debug("violationss data came back with: [" + plist.length + 
							"] elements in [" + msecs + "] msecs", dbarLoc);
			}
			wtdviol += " [" + msecs + "] msecs"
			vsdata.violations = wtdviol;
			viewScope.put(vsKey, vsdata);
		} catch(e) {
			dBar.error(e.toString(), dbarLoc);
			wtdviol = "error";
			vsdata.violations = wtdviol;
			viewScope.put(vsKey, vsdata);
		}
	}
	return wtdviol;
}

function xinspectionPreloadObservations (vsKey) {
	var localDebug = false;
	var dbarLoc = "inspectionRoutines.ssjs.inspectionPreloadObservations";

	var vsdata = viewScope.get(vsKey);
	var wtdobsever = "";
	if(vsdata == null) {
		wtdobsever = "pending";
	} else if("observations".equals(vsdata.wtd)){
		wtdobsever = "processing"
	} else {
		wtdobsever = vsdata.observations;
	}
	if("processing".equals(wtdobsever)) {
		try {
			var sdate:Date = new Date();
			var msecs = 0;
			vsdata.observations = wtdobsever;
			viewScope.put(vsKey, vsdata);
			
			var vmodule = inspection.getViolationModule();
			//if (localDebug) dBar.debug("vmodule came back as: " + vmodule, dbarLoc);
				
			var plist = inspection.getPlObservationData();
			wtdobsever = "loaded";
			msecs = new Date().getTime() - sdate.getTime();
			if(localDebug) {
				dBar.debug("observations data came back with: [" + plist.length + 
							"] elements in [" + msecs + "] msecs", dbarLoc);
			}
			wtdobsever += " [" + msecs + "] msecs";
			vsdata.observations = wtdobsever;
			viewScope.put(vsKey, vsdata);
		} catch(e) {
			dBar.error(e.toString(), dbarLoc);
			wtdobsever = "error";
			vsdata.observations = wtdobsever;
			viewScope.put(vsKey, vsdata);
		}
	}
	return wtdobsever;
}

function inspectionPreloadDocument(vsKeys) {
	var localDebug = false;
	var dbarLoc = "inspectionRoutines.ssjs.inspectionPreloadDocument";

	var vsdata = viewScope.get(vsKeys[0]);
	var wtdloaddoc = "";
	if(vsdata == null) {
		wtdloaddoc = "pending";
	} else if("loaddoc".equals(vsdata.wtd)){
		wtdloaddoc = "processing"
	} else if(vsdata.loaddoc == undefined) {
		wtdloaddoc = "pending";
	} else {
		wtdloaddoc = vsdata.loaddoc;
	}
	if("processing".equals(wtdloaddoc)) {
		try {
			var unid = viewScope.get(vsKeys[1]);
			if(localDebug) dBar.debug("Loading doc with unid:[" + unid + "]", dbarLoc);
			if(inspection.load(businessObjects, unid)) {
				wtdloaddoc = "load success";
			} else {
				wtdloaddoc = "load failed";
			}
			vsdata.loaddoc = wtdloaddoc;
			viewScope.put(vsKey, vsdata);
			if(localDebug) dBar.debug(wtdloaddoc, dbarLoc);
			
		} catch (e) {
			dBar.error(e.toString(), dbarLoc);
			wtdloaddoc = "error";
			vsdata.loaddoc = wtdloaddoc;
			viewScope.put(vsKey, vsdata);
		}
	}
	return wtdloaddoc;
}

/*******************
function inspectionSaveCF1Old() {
	var localDebug = false;
	var dbarLoc = "inspectionRoutines.ssjs..inspectionSaveCF1";

	var msg = "Inspection Save - inspType:[" + inspection.getInspType() + "]";
	if (localDebug) dBar.debug(msg, dbarLoc);
	inspection.resetSaveMessages();
	var outOfSync = false;
	if (param.containsKey("inspType") && inspaection.getInspType() != param.get("inspType")) {
		outOfSync = true;
		msg = "Inspection object out of sync with UI objects.";
		if (localDebug) dBar.debug(msg, dbarLoc);
		inspection.getSaveMessages().add(msg);
		msg = "i.inspType:[" + inspection.getInspType() + "] " +
				"p.inspType:[" + param.get("inspType") + "]";
		if (localDebug) dBar.debug(msg, dbarLoc);
		inspection.getSaveMessages().add(msg);
		inspection.setInspType(param.get("inspType"));
	}
	if(param.containsKey("inspEho") && inspection.getEho() != param.get("inspEho")) {
		if(!outOfSync) {
			outOfSync = true;
			msg = "Inspection object out of sync with UI objects.";
			if (localDebug) dBar.debug(msg, dbarLoc);
			inspection.getSaveMessages().add(msg);
		}
		msg = "i.eho:[" + inspection.getEho() + "] p.inspEho:[" + param.get("inspEho") + "]"
		if (localDebug) dBar.debug(msg, dbarLoc);
		inspection.getSaveMessages().add(msg);
		inspection.setEho(param.get("inspEho"));
	}
	if(param.containsKey("inspFureq") && inspection.getFollowupInspectionRequired() != param.get("inspFureq")) {
		if(!outOfSync) {
			outOfSync = true;
			msg = "Inspection object out of sync with UI objects.";
			if (localDebug) dBar.debug(msg, dbarLoc);
			inspection.getSaveMessages().add(msg);
		}
		msg = "i.followupInspectionRequired:[" + inspection.getFollowupInspectionRequired() + "] " +
				"p.inspFureq:[" + param.get("inspFureq") + "]";
		if (localDebug) dBar.debug(msg, dbarLoc);
		inspection.getSaveMessages().add(msg);
		inspection.setFollowupInspectionRequired(param.get("inspFureq"));
	}
	if(param.containsKey("inspNiDateStr") && inspection.getNextInspectionDateStr() != param.get("inspNiDateStr")) {
		if(!outOfSync) {
			outOfSync = true;
			msg = "Inspection object out of sync with UI objects.";
			if (localDebug) dBar.debug(msg, dbarLoc);
			inspection.getSaveMessages().add(msg);
		}
		msg = "i.nextInspectionDateStr:[" + inspection.getNextInspectionDateStr() + "] " +
				"p.inspNiDateStr:[" + param.get("inspNiDateStr") + "]";
		if (localDebug) dBar.debug(msg, dbarLoc);
		inspection.getSaveMessages().add(msg);
		inspection.setNextInspectionDateStr(param.get("inspNiDateStr"));
	}
	
	if(param.containsKey("inspComments") && inspection.getComments() != param.get("inspComments")) {
		if(!outOfSync) {
			outOfSync = true;
			msg = "Inspection object out of sync with UI objects.";
			if (localDebug) dBar.debug(msg, dbarLoc);
			inspection.getSaveMessages().add(msg);
		}
		msg = "i.comments:[" + inspection.getComments() + "] " +
				"p.inspComments:[" + param.get("inspComments") + "]";
		if (localDebug) dBar.debug(msg, dbarLoc);
		inspection.getSaveMessages().add(msg);
		inspection.setComments(param.get("inspComments"));
		// Use this to force an error for error handling testing.
		inspection.setBadComments(param.get("inspComments"));
	}
	
	var signAfterSave = ("true".equals(param.get("signAfterSave")));
	if (localDebug) dBar.debug("signAfterSave:[" + signAfterSave + "] typeof:[" + typeof signAfterSave + "]", dbarLoc);

	var submitAfterSave = ("true".equals(param.get("submitAfterSave")));
	if (localDebug) dBar.debug("submitAfterSave:[" + submitAfterSave + "] typeof:[" + typeof submitAfterSave + "]", dbarLoc);
	
	try {
		if(inspection.validate()) {
			msg = "Inspection validated.";
			if (localDebug) dBar.debug(msg, dbarLoc);
		
			if(inspection.save(businessObjects)) {
				msg = (@Implode(inspection.saveMessages, ",").indexOf("no changes in the document") > -1) ? "" : "Inspection saved";
				if (localDebug) dBar.debug(msg, dbarLoc);
				if(signAfterSave) {
					var surl = getSigningUrl();
					if(localDebug) dBar.debug("SURL:[" + surl + "]", dbarLoc);
					msg += "<script>hs_inspectionCSCode.signURL='" + surl + "';";
					msg += "setTimeout(function(){var turl=hs_inspectionCSCode.signURL;";
					if(localDebug) msg += "console.debug('signing url is:[' + turl + ']');"
					msg += "window.open(turl,'SignInspection');";
					msg += "setTimeout(function(){location='#frmFacility';}, 500);";
					msg += "}, 500);";
					msg += "</script>";
				} else if(submitAfterSave) {
					var facunid = inspection.getParentUnid();
					inspection.clearInspection();
					msg = "Inspection saved and cleared.";
					if (localDebug) dBar.debug(msg, dbarLoc);
					var turl = @Left(context.getUrl().toString(), ".nsf/") + ".nsf/";
					turl += "formFacility.xsp?id=" + facunid
					if(localDebug) dBar.debug("turl:[" + turl + "]", dbarLoc);
					//msg += "<script>setTimeout(function(){location='#frmFacility';}, 20);" +
					//			"</script>";
					//msg += "<script>setTimeout(function(){alert('SUBMIT NEEDS WORK IN SSJS CODE');}, 2);</script>";
					msg += "<script>setTimeout(function(){location='" + turl + "';}, 1000)</script>";
				} else {
					msg += "<script>setTimeout(function(){" + 
							"dojo.byId(hs_inspectionCSCode.ids.savecf1),innerHTML=" + 
							"'Insection saved.';}, 5);" + 
							"</script>";
				}
				msg += "<br/>" + @Implode(inspection.saveMessages, "<br/>");
			} else {
				msg = "Inspection save failed.";
				if (localDebug) dBar.debug(msg, dbarLoc);
				msg += "<br/>" + @Implode(inspection.saveMessages, "<br/>");
			}
		} else {
			msg = "Inspection validation failed.";
			if (localDebug) dBar.debug(msg, dbarLoc);
			msg += "<br/>" + @Implode(inspection.validationMessages, "<br/>");
		}
	} catch(e) {
		msg = "Inspection save failed.";
		if (localDebug) dBar.debug(msg, dbarLoc);
		dBar.error(e.toString(), dbarLoc);
		msg += "<br/>" + e.toString();
		msg += "<br/>" + @Implode(inspection.validationMessages, "<br/>");
		print(dbarLoc + " - ERROR: " + e.toString());
	}
	return msg;
}
******************/

function inspectionSaveBS(signAfterSave, submitAfterSave, isrpId, is4bscf1Id) {
	var localDebug = true;
	var dbarLoc = "inspectionRoutines.ssjs..inspectionSaveBS";

	var msg = "Inspection Save - inspType:[" + inspection.getInspType() + "]";
	if (localDebug) dBar.debug(msg, dbarLoc);
	inspection.resetSaveMessages();
		
	//var signAfterSave = ("true".equals(param.get("signAfterSave")));
	if (localDebug) dBar.debug("signAfterSave:[" + signAfterSave + "] typeof:[" + typeof signAfterSave + "]", dbarLoc);

	//var submitAfterSave = ("true".equals(param.get("submitAfterSave")));
	if (localDebug) dBar.debug("submitAfterSave:[" + submitAfterSave + "] typeof:[" + typeof submitAfterSave + "]", dbarLoc);
	
	try {
		if(inspection.validate()) {
			msg = "Inspection validated.";
			if (localDebug) dBar.debug(msg, dbarLoc);
		
			if(inspection.save(businessObjects)) {
				msg = (@Implode(inspection.saveMessages, ",").indexOf("no changes in the document") > -1) ? "" : "Inspection saved";
				if (localDebug) dBar.debug(msg, dbarLoc);
				/*** 
				 if(signAfterSave) {
					var surl = getSigningUrl();
					if(localDebug) dBar.debug("SURL:[" + surl + "]", dbarLoc);
					msg += "<script>hs_inspectionCSCode.signURL='" + surl + "';";
					msg += "setTimeout(function(){var turl=hs_inspectionCSCode.signURL;";
					if(localDebug) msg += "console.debug('signing url is:[' + turl + ']');"
					msg += "window.open(turl,'SignInspection');";
					msg += "setTimeout(function(){location='#frmFacility';}, 500);";
					msg += "}, 500);";
					msg += "</script>";
				} else ***/
				if(submitAfterSave) {
					var facunid = inspection.getParentUnid();
					inspection.clearInspection();
					msg = "Inspection saved and cleared.";
					if (localDebug) dBar.debug(msg, dbarLoc);
					var turl = @Left(context.getUrl().toString(), ".nsf/") + ".nsf/";
					turl += "formFacility.xsp?id=" + facunid
					if(localDebug) dBar.debug("turl:[" + turl + "]", dbarLoc);
					//msg += "<script>setTimeout(function(){location='#frmFacility';}, 20);" +
					//			"</script>";
					//msg += "<script>setTimeout(function(){alert('SUBMIT NEEDS WORK IN SSJS CODE');}, 2);</script>";
					msg += "<script>setTimeout(function(){location='" + turl + "';}, 1000)</script>";
				} else {
					
				}
				msg += "<br/>" + @Implode(inspection.saveMessages, "<br/>");
			} else {
				msg = "Inspection save failed.";
				if (localDebug) dBar.debug(msg, dbarLoc);
				msg += "<br/>" + @Implode(inspection.saveMessages, "<br/>");
			}
		} else {
			msg = "Inspection validation failed.";
			if (localDebug) dBar.debug(msg, dbarLoc);
			msg += "<br/>" + @Implode(inspection.validationMessages, "<br/>");
		}
	} catch(e) {
		msg = "Inspection save failed.";
		if (localDebug) dBar.debug(msg, dbarLoc);
		dBar.error(e.toString(), dbarLoc);
		msg += "<br/>" + e.toString();
		msg += "<br/>" + @Implode(inspection.validationMessages, "<br/>");
		print(dbarLoc + " - ERROR: " + e.toString());
	}
	msg += "<script>hs_inspectionCSCode.ids.savecf1='" + is4bscf1Id + "';</script>";
	viewScope.ei4bsSave = msg;
	return msg;
}

function violationSaveCF1 () {
	var localDebug = false;
	var dbarLoc = "inspectionRoutines.ssjs.violationSaveCF1";

	var dt = new Date().toLocaleString();
	if (!param.containsKey("saveJob")) return dt + " - no job.";

	var sj = param.get("saveJob");
	var msgs = dt + "<br/>sj:[" + sj + "]<br/>";

	var emsg = param.get("error");
	if(!"".equals(emsg)) {
		msgs += "ERROR:[" + emsg + "]<br/>";
		return msgs;
	}
	
	var ks:java.util.Set = param.keySet();
	var ksa = ks.toArray();

	var editOrNew = "";
	var editIdx = -1;
	for (var i = 0; i<ksa.length; i++) {
		if(ksa[i].indexOf("sj_") > -1) {
			var ksl = @Right(ksa[i], "sj_");
			//if (localDebug) msgs += ksl + ":[" + param.get(ksa[i]) +"]<br/>";
			if(localDebug) dBar.debug("ksl:[" + ksl + "] value:["+ ksa[i] + "]", dbarLoc);
			try {
				if("vcategory" == ksl) inspection.setWrkCat(param.get(ksa[i]));
				if("vcode" == ksl) inspection.setWrkViolation(param.get(ksa[i]));
				if("vcorrect" == ksl) inspection.setWrkCorrectiveAction(param.get(ksa[i]));
				if("vcritical" == ksl)  {
					//if(localDebug) dBar.debug("calling inspection.setWrkCritical " + 
					//			"with type:[" + typeof ksa[i] + "]", dbarLoc);
					inspection.setWrkCritical(param.get(ksa[i]));
					msgs += "<br/>WrkCritical set to [" + param.get(ksa[i]) + "]"
				}
				if("vdescription" == ksl) inspection.setWrkDescription(param.get(ksa[i]));
				if("vgroup" == ksl) {
					inspection.setWrkGrp(param.get(ksa[i]));
					//if(localDebug) dBar.debug("called setWrkGroup with:[" + param.get(ksa[i]) + "]", dbarLoc);
				}
				if("vhzrdrating" == ksl) inspection.setWrkHzrdRating(param.get(ksa[i]));
				if("vinout" == ksl) inspection.setWrkInOut(param.get(ksa[i]));
				if("vlegaltext" == ksl) inspection.setWrkLegalText(param.get(ksa[i]));
				if("vobserve" == ksl) inspection.setWrkObservation(param.get(ksa[i]));
				if("vrepeat" == ksl) inspection.setWrkRepeat(param.get(ksa[i]));
				if("vsnum" == ksl) inspection.setWrkSection(param.get(ksa[i]));
				if("vstatus" == ksl) inspection.setWrkStatus(param.get(ksa[i]));
				
				if("vcorrectedset" == ksl){
					inspection.setWrkCorrectedSet(param.get(ksa[i]));
					if(localDebug) dBar.debug("vcorrectedset set to:[" + param.get(ksa[i]) + "]", dbarLoc);
				}
						
				if("vcorrectbydate" == ksl) inspection.setWrkCorrectByDate(param.get(ksa[i]));
				
			} catch (e) {
				msgs += "ERROR Saving [" + ksl + "] " + e.toString() + "<br/>"
			}
		} else if(ksa[i].indexOf("editOrNew") > -1) {
			editOrNew = param.get(ksa[i]);
			if (localDebug) dBar.debug("editOrNew is:[" + editOrNew + "]", dbarLoc);
		} else if(ksa[i].indexOf("editIdx") > -1) {
			editIdx = param.get(ksa[i]);
			if (localDebug) dBar.debug("editIdx is:[" + editIdx + "]", dbarLoc);
		} else {
			//if (localDebug) dBar.debug("Unknown parameter:[" + ksa[i] + "] - [" + param.get(ksa[i]) + "]", dbarLoc);
		}
	}

	if(inspection.validateViolation()) {
		if (localDebug) msgs += "violation passed validation<br/>";
		
		if(editOrNew == "new") {
			if (!inspection.saveNewViolation()) {
				msgs += "violation saved FAILED!<br/>";
			} else {
				msgs += "violation saved<br/>";
				msgs += "<script>hs_inspectionCSCode.saveSuccess=true;</script>"
				inspection.clearViolation();
				if (localDebug) msgs += "violation cleared<br/>";
			}
		} else {
			if (!inspection.saveEditedViolation(parseInt(editIdx))) {
				msgs += "violation update FAILED!<br/>";
			} else {
				msgs += "violation saved<br/>";
				msgs += "<script>hs_inspectionCSCode.saveSuccess=true;</script>"
				inspection.clearViolation();
				if (localDebug) msgs += "violation cleared<br/>";
			}
		}
		//facesContext.gtExternalContext().redirect("#frmCreateInspection");
	} else {
		msgs += "inspection failed validation<br/>";
		msgs += inspection.getWrkMessages().split("<br/>");
		if(localDebug) {
			dBar.debug("Violation validation failed.", dbarLoc);
			dBar.debug(inspection.getWrkMessages(), dbarLoc);
		}
	}

	return msgs;
}

function getPrintUrl(inspDoc, altUnid) {
	var bangstr =  "";
	if (inspDoc == null){
		bangstr	= hs_Database.getdbBang('eiRoot');
	} else {
		bangstr	= @Name("[CN]", inspDoc.getParentDatabase().getServer()) + "!!" + inspDoc.getParentDatabase().getFilePath();
	}
	var turl = "PrintInspection.xsp?databaseName=" + escape(bangstr);
	turl += "&documentId=" + ((altUnid) ? altUnid : inspDoc.getDocument().getUniversalID());
	turl += "&action=openDocument";
	return turl;
}

function getSigningUrl() {
	//var bangstr = @Name("[CN]", inspDoc.getParentDatabase().getServer()) + "!!" + inspDoc.getParentDatabase().getFilePath();
	var bangstr = hs_Database.getdbBang("eiRoot");
	var turl = "/" + session.evaluate("@WebDbName").get(0) + "/"; 
	turl += "PrintInspection.xsp?databaseName=" + escape(bangstr);
	turl += "&documentId=" + inspection.getUnid();
	turl += "&action=editDocument";
	//turl += "#signature"
	return turl;
}

function getSigningUrlV2(inspDoc, altUnid) {
	var bangstr = @Name("[CN]", inspDoc.getParentDatabase().getServer()) + "!!" + inspDoc.getParentDatabase().getFilePath();
	var turl = "PrintInspection.xsp?databaseName=" + escape(bangstr);
	turl += "&documentId=" + ((altUnid) ? altUnid : inspDoc.getDocument().getUniversalID());
	turl += "&action=editDocument";
	return turl;
}

function getDBDesign() {
	//@GetProfileField("MasterSettings"; "DBDesign");
	try {
		return getMasterSettingsItem("DBDesign").get(0);
	} catch (e){
		return "";
	}
}
function getMasterSettingsItem(fname) {
	var dbarLoc = "inspectionRoutines.getMasterSettingsItem";
	var localDebug = ("xxx".equals(fname));
	//@GetProfileField("MasterSettings"; "DBDesign");
	try {
		var wrk = hs_profiles.get("eiRoot", "MasterSettings", fname);
		if(wrk == null) {
			if (localDebug) dBar.debug(fname + " lookup returned a null value.", dbarLoc);
			return "";
		}
		if (localDebug) {
			if("string".equals(typeof wrk.get(0))) {
				dBar.debug(fname + " lookup returned a " + typeof wrk.get(0) + " [" + wrk.get(0) + "]", dbarLoc);
			} else {
				dBar.debug(fname + " lookup returned a " + typeof wrk.get(0), dbarLoc);
			}
		}
		return wrk.get(0);
	} catch (e){
		if (localDebug) dBar.debug(fname + " got an error: " + e.toString(), dbarLoc);
		return "";
	}
}

function getMasterSettingsDateItem(fname) {
	var dbarLoc = "inspectionRoutines.getMasterSettingsDateItem";
	var localDebug = ("xxx".equals(fname));
	//@GetProfileField("MasterSettings"; "DBDesign");
	try {
		var wrkdt = hs_profiles.get("eiRoot", "MasterSettings", fname);
		if(wrkdt == null) {
			if (localDebug) dBar.debug(fname + " lookup returned a null value.", dbarLoc);
			return "";
		}
		var wdt = wrkdt.get(0);
		if (localDebug) dBar.debug(fname + " lookup returned a " + typeof wdt, dbarLoc);
		return hs_util.formatDate(wdt.toJavaDate(), "yyyy-MM-dd");
	} catch(e) {
		if (localDebug) dBar.debug(fname + " got an error: " + e.toString(), dbarLoc);
		return "";
	}
}

function useNewViolTypes(inspectionDate, establishmentType) {
	var dbarLoc = "inspectionRoutines.useNewViolTypes";
	var localDebug = false;
/*
	UseCodes := @GetProfileField( "MasterSettings"; "FoodCode2009Districts" );
	CodeStartDate := @GetProfileField( "MasterSettings"; "FoodCode2009Date" );
	IsAfterDate := @If( @IsTime( CodeStartDate ); @If( InspectionDate >= CodeStartDate; @True; @False ); @False );
	
	@If( EstablishmentType = "Foodservice" & UseCodes = "Yes" & IsAfterDate;
		@True;
		@False )
*/
	var useCodes = getMasterSettingsItem("FoodCode2009Districts");
	var codeStartDate:NotesDateTime = getMasterSettingsItem("FoodCode2009Date");
	if("".equals(establishmentType)) establishmentType = "Foodservice";
	var isAfterDate = ((typeof codeStartDate).indexOf("DateTime") == -1) ? false :
				(inspectionDate.toJavaDate().getTime() >= codeStartDate.toJavaDate().getTime()) ? true :
				false;
	if(localDebug) dBar.debug("useCodes:[" + useCodes + "]" + 
		" codeStartDate:[" + codeStartDate + "] " +  
		" inspectionDate:[" + inspectionDate + "] " +  
		" establishmentType:[" + establishmentType + "] " +
		" isAfterDate:[" + isAfterDate + "]", dbarLoc);
	return ("Foodservice".equals(establishmentType) 
				&& "Yes".equals(useCodes) 
				&& isAfterDate ); 
}
