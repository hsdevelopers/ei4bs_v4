// canvasSignature.js : Author Henry Newberry - Feb, 2014
// Based on canvasOutputCapture by Declan Lynch - 2011
//		www.openntf.org/internal/home.nsf/project.xsp?action=openDocument&name=Signature%20Capture%20Control
// 		released under the Apache License, Version 2.0
// Declan's work was based on the work of Richard Garside - www.nogginbox.co.uk [2010]
// 		Released under the MIT License

if(!cb_canvases) {
	var cb_canvases = []
}

// Parameters are:
// 	cid - id of the canvas area -eg: "#{id:canvasArea}"
// 	dsFldContent - dataSource field content - eg: '#{compositeData.dataSource[compositeData.dataField]}'
//	inpIsEditable - indicates if this is read only or editable - eg: #{javascript:compositeData.dataSource.isEditable()}
//	outputFldId - id of the output field: - eg: "#{id:canvasOutput}" 
//
//	Sample deployment: 
function cb_canvasClass (cid, dsFldContent, inpIsEditable, outputFldId, idebug) {
	this.canvasId = cid;
	this.dataSourceFieldContent = dsFldContent;
	this.isEditable = inpIsEditable;
	this.outputFieldId = outputFldId;
	this.debug = (!idebug) ? false : true;
	
	if(this.debug) {
		console.debug("canvasSignature.cb_canvasClass - cid:[" + cid + "]");
		console.debug("canvasSignature.cb_canvasClass - dsFldContent:[" + dsFldContent + "]");
		console.debug("canvasSignature.cb_canvasClass - inpIsEditable:[" + inpIsEditable + "]");
		console.debug("canvasSignature.cb_canvasClass - outputFldId:[" + outputFldId + "]");
	}
	
	this.cb_canvas = null;
	this.cb_ctx = null;
	this.cb_lastPoints = null;
	this.cb_easing = 0.4;
	this.output = [];
	this.outputField = null;
	
	this.init = function (e) {
		this.cb_canvas = document.getElementById(this.canvasId);
		this.cb_lastPoints = Array();

		if (this.cb_canvas.getContext) {
			this.cb_ctx = this.cb_canvas.getContext('2d');
			this.cb_ctx.lineWidth = 2;
			this.cb_ctx.strokeStyle = "rgb(0, 0, 0)";
			this.cb_ctx.beginPath();
			
			// Draw in the existing JSON String
			if (this.dataSourceFieldContent != '' ){
				this.output = dojo.fromJson(this.dataSourceFieldContent);
				for(var i in this.output) {
					if (typeof this.output[i] === 'object') {
						this.cb_ctx.beginPath()
						this.cb_ctx.moveTo(this.output[i].sX, this.output[i].sY)
						this.cb_ctx.lineTo(this.output[i].eX, this.output[i].eY)
						this.cb_ctx.stroke()
						this.cb_ctx.closePath()
					}
				}
			}
			
			// Enable drawing mode if the datasource is editable
			if (this.isEditable){
				this.cb_canvas.onmousedown = this.startDraw;
				this.cb_canvas.onmouseup = this.stopDraw;
				this.cb_canvas.ontouchstart = this.startDraw;
				this.cb_canvas.ontouchstop = this.stopDraw;
				this.cb_canvas.ontouchmove = this.drawMouse;
			}
		}
	}
	
	this.startDraw = function (e) {
		var cidx = -1;
		var wrkcanvas = null;
		for(var i=0; i< cb_canvases.length; i++) {
			if(cb_canvases[i].canvasId == this.id) {
				cidx = i;
				wrkcanvas = cb_canvases[i]; 
			}
		}
		if (wrkcanvas.debug) console.debug("canvasSignature.js.startDraw: this.id:[" + this.id + "] cidx:[" + cidx + "]");
		if (e.touches) {
			// Touch event
			for (var i = 1; i <= e.touches.length; i++) {
				wrkcanvas.cb_lastPoints[i] = wrkcanvas.getCoords(e.touches[i - 1]); // Get info for finger #1
			}
		}
		else {
			// Mouse event
			wrkcanvas.cb_lastPoints[0] = wrkcanvas.getCoords(e);
			wrkcanvas.cb_canvas.onmousemove = wrkcanvas.drawMouse;
		}
		
		return false;
	}
	
	this.stopDraw = function (e) {
		var cidx = -1;
		var wrkcanvas = null;
		for(var i=0; i< cb_canvases.length; i++) {
			if(cb_canvases[i].canvasId == this.id) {
				cidx = i;
				wrkcanvas = cb_canvases[i]; 
			}
		}
		if (wrkcanvas.debug) console.debug("canvasSignature.js.stopDraw: this.id:[" + this.id + "] cidx:[" + cidx + "]");
		
		e.preventDefault();
		wrkcanvas.cb_canvas.onmousemove = null;
	}

	this.drawMouse = function (e) {
		var cidx = -1;
		var wrkcanvas = null;
		for(var i=0; i< cb_canvases.length; i++) {
			if(cb_canvases[i].canvasId == this.id) {
				cidx = i;
				wrkcanvas = cb_canvases[i]; 
			}
		}
		if (wrkcanvas.debug) console.debug("canvasSignature.js.drawMouse: this.id:[" + this.id + "] cidx:[" + cidx + "]");
		
		if (e.touches) {
			// Touch Enabled
			for (var i = 1; i <= e.touches.length; i++) {
				var p = wrkcanvas.getCoords(e.touches[i - 1]); // Get info for finger i
				wrkcanvas.cb_lastPoints[i] = wrkcanvas.drawLine(wrkcanvas.cb_lastPoints[i].x, wrkcanvas.cb_lastPoints[i].y, p.x, p.y);
			}
		}
		else {
			// Not touch enabled
			var p = wrkcanvas.getCoords(e);
			wrkcanvas.cb_lastPoints[0] = wrkcanvas.drawLine(wrkcanvas.cb_lastPoints[0].x, wrkcanvas.cb_lastPoints[0].y, p.x, p.y);
		}
		wrkcanvas.cb_ctx.stroke();
		wrkcanvas.cb_ctx.closePath();
		wrkcanvas.cb_ctx.beginPath();

		return false;
	}

	// Draw a line on the canvas from (s)tart to (e)nd
	this.drawLine = function (sX, sY, eX, eY) {
		this.cb_ctx.moveTo(sX, sY);
		this.cb_ctx.lineTo(eX, eY);
		
		this.output.push({
	      'sX': sX
	      ,'sY': sY
	      ,'eX': eX
	      ,'eY': eY
	    });
	    
		this.outputField = document.getElementById(this.outputFieldId);
		this.outputField.value = dojo.toJson(this.output);
	    
		return { x: eX, y: eY };
	}

	// Get the coordinates for a mouse or touch event
	this.getCoords = function(e) {
		if (e.offsetX) {
			return { x: e.offsetX, y: e.offsetY };
		}
		else if (e.layerX) {
			return { x: e.layerX, y: e.layerY };
		}
		else {
			return { x: e.pageX - this.offsetLeft, y: e.pageY - this.offsetTop };
		}
	}
}
