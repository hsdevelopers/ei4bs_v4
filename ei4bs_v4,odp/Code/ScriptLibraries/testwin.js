// store original outer dimensions as page loads
var originalLocationbar = window.locationbar.visible;
var originalMenubar = window.menubar.visible;
var originalPersonalbar = window.personalbar.visible;
var originalScrollbars = window.scrollbars.visible;
var originalStatusbar = window.statusbar.visible;
var originalToolbar = window.toolbar.visible;
// generic function to set inner dimensions
function toggleBar(bar) {
	var nsec = netscape.security; 
	nsec.PrivilegeManager.enablePrivilege("UniversalBrowserWrite");
	bar.visible = !bar.visible;
	nsec.PrivilegeManager.revertPrivilege("UniversalBrowserWrite");
}
// restore settings
function restore() {
	netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserWrite");
	window.locationbar.visible = originalLocationbar;
	window.menubar.visible = originalMenubar;
	window.personalbar.visible = originalPersonalbar;
	window.scrollbars.visible = originalScrollbars;
	window.statusbar.visible = originalStatusbar;
	window.toolbar.visible = originalToolbar;
	netscape.security.PrivilegeManager.revertPrivilege("UniversalBrowserWrite");
}