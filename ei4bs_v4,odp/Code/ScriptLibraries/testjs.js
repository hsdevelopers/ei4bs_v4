// invoke by calling: HS_ViolationsRefresher.startRefresh(id, t/f, {"$$xspsubmitvalue": status});
if(!HS_ViolationsRefresher) {
	var HS_ViolationsRefresher = {
		// Ids of fields/areas to refresh in order to be refreshed
		ids : ["#{id:violGroupCB1}",
		       "#{id:violCategoryCB1}",
		       "#{id:violSectionCB1}",
		       "#{id:violInOutButtonsPanel}",
		       "#{id:violStatusCF1}",
		       "#{id:violViolationCB1}",
		       "#{id:violCriticalCF1}",
		       "#{id:violObservationTA1}",
		       "#{id:violCorrectiveActionTA1}",
		       "#{id:violCorrectByDateCF1}"],
		 
		 currentId : "",
		 
		 startRefresh : function (id, post, params) {
		 	var wrkid = "";
			 for (var i=0; i<(this.ids.length-1); i++) {
				 if (this.ids[i].indexOf(id) > 0) {
					 wrkid = this.ids[i+1]
				 }
			 }
			this.currentId = wrkid;
		 	alert("startRefresh - id is:" + id + "\nwrkid:" + wrkid);
		 	if (wrkid == "") return;
		 	if(post) {
		 		XSP.partialRefreshPost(wrkid, {
		 			'params' : params,
		 			onComplete: function () {
		 				HS_ViolationsRefresher.nextRefresh();
	      			},
	      			onError : function () {
	      				alert("HS_ViolationsRefresher.startRefreshPost ERROR\n" + 
	      						"ID: " + HS_ViolationsRefresher.currentId);
	      			}
		 		});
		 	} else {
		 		XSP.partialRefreshGet(wrkid, {
		 			'params' : params,
		 			onComplete: function () {
		 				HS_ViolationsRefresher.nextRefresh();
	      			},
	      			onError : function () {
	      				alert("HS_ViolationsRefresher.startRefreshGet ERROR\n" + 
	      						"ID: " + HS_ViolationsRefresher.currentId);
	      			}
		 		});
		 	}
		 },
		 
		 nextRefresh : function() {
			 var nid = "";
			 for (var i=0; i<(this.ids.length-1); i++) {
				 if (this.currentId == this.ids[i]) {
					 nid = this.ids[i+1]
				 }
			 }
			 if (nid == "") return;
			 this.currentId = nid;
			 XSP.partialRefreshGet(nid, {
		 			onComplete: function () {
		 				setTimeout("HS_ViolationsRefresher.nextRefresh()", 5);
	      			},
	      			onError : function () {
	      				alert("HS_ViolationsRefresher.nextRefreshGet ERROR\n" + 
	      						"ID: " + HS_ViolationsRefresher.currentId);
	      			}
		 		});
		 },
		 
		 fixIds : function () {
		 	if(this.ids[0] == "") this.ids[0] = "#{id:violGroupCB1}";
		 	if(this.ids[1] == "") this.ids[1] = "#{id:violCategoryCB1}";
		 	if(this.ids[2] == "") this.ids[2] = "#{id:violSectionCB1}";
		 	if(this.ids[3] == "") this.ids[3] = "#{id:violInOutButtonsPanel}";
		 	if(this.ids[4] == "") this.ids[4] = "#{id:violStatusCF1}";
		 	if(this.ids[5] == "") this.ids[5] = "#{id:violViolationCB1}";
		 	if(this.ids[6] == "") this.ids[6] = "#{id:violCriticalCF1}";
		 	if(this.ids[7] == "") this.ids[7] = "#{id:violObservationTA1}";
			if(this.ids[8] == "") this.ids[8] = "#{id:violCorrectiveActionTA1}";
			if(this.ids[9] == "") this.ids[9] = "#{id:violCorrectByDateCF1}";
		 }
		 
	}
}


setTimeout(function() {
	hs_executeOnServer("#{id:splashPageEndEvent}", '@none', {
    	onStart: function() { alert('starting!'); },
    	onComplete: function() { alert('complete!'); },
    	onError: function() { alert('DANGER WILL ROBINSON!'); } 
    });
}, 2000); 



var hs_executeOnServer = function () {

    // must supply event handler id or we're outta here....
    if (!arguments[0])
        return false;

    // the ID of the event handler we want to execute
    var functionName = arguments[0];

    // OPTIONAL - The Client Side ID that you want to partial refresh after executing the event handler
    var refreshId = (arguments[1]) ? arguments[1] : "@none";
    var form = (arguments[1]) ? XSP.findForm(arguments[1]) : dojo.query('form')[0];

    // OPTIONAL - Options object contianing onStart, onComplete and onError functions for the call to the
    // handler and subsequent partial refresh
    var options = (arguments[2]) ? arguments[2] : {};

    // Set the ID in $$xspsubmitid of the event handler to execute
    dojo.query('[name="$$xspsubmitid"]')[0].value = form.id + ':' + functionName;
    XSP._partialRefresh("post", form, refreshId, options);

}