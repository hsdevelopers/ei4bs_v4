if (!hs_certMgrFuncs) {
	var hs_certMgrFuncs = {
		init : function() {
			var localDebug = false;
			var dbugLoc = "[hs_certMgrFuncs.init]: ";
			var ids = [ "#{id:managerEBNew}", "#{id:managerAddressEBNew}",
					"#{id:managerCertificateIdEBNew}",
					"#{id:managerCertificateExpEBNew}" ];
			var eb1 = null;
			for ( var i = 0; i < ids.length; i++) {
				eb1 = dojo.byId(ids[i]);
				if (eb1 == null) {
					if (localDebug)
						console.debug(dbugLoc + "SKIPPING setting style for: "
								+ ids[i]);
				} else {
					if (localDebug)
						console.debug(dbugLoc + "setting style for: " + ids[i]);
					dojo.style(eb1, "border", "thin solid #AFAFAF");
					dojo.style(eb1, "font-size", "medium");
					if (ids[i].indexOf("managerEBNew") > -1) {
						dojo.style(eb1, "width", "200px");
					}
					if (ids[i].indexOf("managerAddressEBNew") > -1) {
						dojo.style(eb1, "width", "200px");
					}
					if (ids[i].indexOf("managerCertificateIdEBNew") > -1) {
						dojo.style(eb1, "width", "150px");
					}
					if (ids[i].indexOf("managerCertificateExpEBNew") > -1) {
						dojo.style(eb1, "width", "150px");
					}
				}
			}

			var nodeList = dojo.query("[id$='managerEBx']");
			if (localDebug)
				console.debug(dbugLoc + "for managerEBx processing ["
						+ nodeList.length + "] nodes.");
			dojo.forEach(nodeList, function(node) {
				dojo.style(node, "border", "thin solid #AFAFAF");
				dojo.style(node, "width", "200px");
				dojo.style(node, "text-align", "left");
				dojo.style(node, "font-size", "medium");
			});

			nodeList = dojo.query("[id$='managerAddressEBx']");
			if (localDebug)
				console.debug(dbugLoc + "for managerAddressEBx processing ["
						+ nodeList.length + "] nodes.");
			dojo.forEach(nodeList, function(node) {
				dojo.style(node, "border", "thin solid #AFAFAF");
				dojo.style(node, "width", "200px");
				dojo.style(node, "font-size", "medium");
			});

			nodeList = dojo.query("[id$='managerCertificateIdEBx']");
			if (localDebug)
				console.debug(dbugLoc
						+ "for managerCertificateIdEBx processing ["
						+ nodeList.length + "] nodes.");
			dojo.forEach(nodeList, function(node) {
				dojo.style(node, "border", "thin solid #AFAFAF");
				dojo.style(node, "width", "150px");
				dojo.style(node, "font-size", "medium");
			});

			nodeList = dojo.query("[id$='managerCertificateExpEBx']");
			if (localDebug)
				console.debug(dbugLoc
						+ "for managerCertificateExpEBx processing ["
						+ nodeList.length + "] nodes.");
			dojo.forEach(nodeList, function(node) {
				dojo.style(node, "border", "thin solid #AFAFAF");
				dojo.style(node, "width", "150px");
				dojo.style(node, "font-size", "medium");
			});
		},

		addrow : function() {
			var localDebug = false;
			var dbugLoc = "[hs_certMgrFuncs.add]: ";
			var maddrid = "#{id:managerAddressEBNew}";
			if (localDebug)
				console.debug("maddrid:[" + maddrid + "]");
			var mgr = dojo.byId("#{id:managerEBNew}").value;
			var maddr = (maddrid == "") ? "" : dojo.byId(maddrid).value;
			var certId = dojo.byId("#{id:managerCertificateIdEBNew}").value;
			var certExp = dojo.byId("#{id:managerCertificateExpEBNew}").value;
			var parms = {
				"manager" : mgr,
				"managerAddress" : maddr,
				"managerCertificateId" : certId,
				managerCertificateExp : certExp
			}
			XSP
					.partialRefreshPost(
							"#{id:managerNewAddCF1}",
							{
								params : parms,
								onComplete : function() {
									console
											.debug("post succeeded to #{id:managerNewAddCF1}");
									setTimeout(
											function() {
												XSP
														.partialRefreshGet(
																"#{id:inspCertifiedManagerEntryTable}",
																{
																	onComplete : function() {
																		var maddr1 = dojo
																				.byId("#{id:managerAddressEBNew}");
																		dojo
																				.byId("#{id:managerEBNew}").value = "";
																		if (maddr1 != null)
																			maddr1.value = "";
																		dojo
																				.byId("#{id:managerCertificateIdEBNew}").value = "";
																		dojo
																				.byId("#{id:managerCertificateExpEBNew}").value = "";

																	},
																	onError : function() {
																		console
																				.error("error on post to #{id:inspCertifiedManagerEntryTable}")
																	}
																});
											}, 20);
								},
								onError : function() {
									console
											.error("error on post to #{id:managerNewAddCF1}")
								}
							})
		}
	}
}

setTimeout( function() {
	hs_certMgrFuncs.init()
}, 200);
