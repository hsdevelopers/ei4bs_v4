
if(!hsbs_utils) {
	var hsbs_utils = {
		isDebugger : false,
		isDebugServer : false,
		logHtmlErrorPageEventId : "",

		debug : function(msg, source) {
			if (!source)
				source = arguments.callee.caller.name;
			if (this.isDebugger || this.isDebugServer)
				console.debug("[" + source + "]: " + msg);
		},
		
		waitPanelId : "",
		waitImageId : "",
		
		waitPanelHide : function() {
			x$(this.waitPanelId).modal('hide');
		},

		waitPanelShow : function() {
			x$(this.waitPanelId).modal();
		}
	};
}

if (!hsbs_splashPositioner) {
	var hsbs_splashPositioner = {
		imageDivId : "",
		splUrl : "",
		
		resizeTO : null,
		
		setpos : function(inpImageDivId, inpSplUrl) {
			hsbs_utils.isDebugServer = true;
			var dbugLoc = "hs_splashPositioner.setpos";
			if (inpImageDivId) this.imageDivId = inpImageDivId;
			if (this.imageDivId == "") {
				console.error("hsbs_splashPositioner.setpos called without proper imageDivId.");
				return;
			}
			if(inpSplUrl) this.splUrl = inpSplUrl;
			if(this.splUrl == "") {
				console.error("hsbs_splashPositioner.setpos called without proper splash URL.");
				return;
			}
			// var splashImage = dojo.byId(this.imageDivId);
			var splashDiv = x$(this.imageDivId);
			
			// background-image:url(" + bgimage + ");" + 
			// background-repeat:no-repeat;" + 
			// background-size: 100% 100%;" +

			$(splashDiv).css("background-image", "url(" + this.splUrl + ")");
			$(splashDiv).css("background-repeat", "no-repeat");
			$(splashDiv).css("background-size","100% 100%");
			
			var recallFunction = false;
			var si = {
				h : $(splashDiv).height(),
				w : $(splashDiv).width()
			};
			hsbs_utils.debug("starting si.h:[" + si.h + "] si.w:[" + si.w + "]", dbugLoc);
			if (si.h == 0) {
				$(splashDiv).height(400);
				si.h = $(splashDiv).height()
				//recallFunction = true;
			}
			if (si.w == 0) {
				$(splashDiv).width(400);
				si.w = $(splashDiv).width();
				//recallFunction = true;
			}
			hsbs_utils.debug("step one si.h:[" + si.h + "] si.w:[" + si.w + "]", dbugLoc);
			
			var vs = {
				h : $(window).height(),
				w : $(window).width()
			};
			hsbs_utils.debug("window is vs.h:[" + vs.h + "] vs.w:[" + vs.w + "]", dbugLoc);
			
			var ps = {
				h : $(splashDiv).prev().height(),
				w : $(splashDiv).prev().width()
			}
			hsbs_utils.debug("previousSibling is ps.h:[" + ps.h + "] ps.w:[" + ps.w + "]", dbugLoc);
			
			$(splashDiv).width(vs.w - 30);
			si.w = $(splashDiv).width();
			
			$(splashDiv).height((vs.h - ps.h) - 30);
			si.h = $(splashDiv).height();
			
			hsbs_utils.debug("final si.h:[" + si.h + "] si.w:[" + si.w + "]", dbugLoc);
			
			if (recallFunction) {
				hsbs_utils.debug("is being recalled.", dbugLoc);
				setTimeout( function() {
					//hsbs_splashPositioner.setpos()
				}, 100);
			} else {
				// Listen for orientation changes
				window.addEventListener("orientationchange", function() {
					console.debug(window.orientation);
					if(hsbs_splashPositioner.resizeTO != null) {
						window.clearTimeout(hsbs_splashPositioner.resizeTO);
						hsbs_splashPositioner.resizeTO = null;
					} 
					hsbs_splashPositioner.resizeTO = setTimeout(function() {
						hsbs_splashPositioner.setpos();
					}, 100);
				}, false);
				
				// Listen for resize changes
				window.addEventListener("resize", function() {
					// Get screen size (inner/outerWidth, inner/outerHeight)
					if(hsbs_splashPositioner.resizeTO != null) {
						window.clearTimeout(hsbs_splashPositioner.resizeTO);
						hsbs_splashPositioner.resizeTO = null;
					} 
					hsbs_splashPositioner.resizeTO = setTimeout(function() {
						hsbs_splashPositioner.setpos();
					}, 300);
				}, false);
			}
		}

	}
}