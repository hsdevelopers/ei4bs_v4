package com.healthspace.test;

import org.openntf.domino.Database;
import org.openntf.domino.Session;
import org.openntf.domino.View;
import org.openntf.domino.exceptions.OpenNTFNotesException;

import com.healthspace.general.HS_Util;

public class ErrorTester {

	public ErrorTester() {

	}

	public void test1() {
		Throwable cause = null;
		cause = new Throwable("here is the cause");

		// throw new RuntimeException("testing");
		throw new OpenNTFNotesException("test exception by newbs", cause);
	}

	public int test2() {
		Session s = HS_Util.getSession();
		Database db = null;
		View v = null;
		db = s.getDatabase("", "nosuchfile.nsf");
		v = db.getView("nosuchniew");
		return v.getAllDocuments().getCount();
	}

}
