package com.healthspace.test;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Map;

import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.healthspace.general.HS_Util;
import com.ibm.xsp.component.UIViewRootEx2;

public class InspectionTest implements Serializable {
	private static final long serialVersionUID = 1L;

	private String uniqueViewId;
	private String comments;

	@SuppressWarnings("unchecked")
	public InspectionTest() {
		final boolean localDebug = true;
		final String dbarLoc = "inspectionbean.constructor";
		try {
			UIViewRoot vr = FacesContext.getCurrentInstance().getViewRoot();
			UIViewRootEx2 vr2 = (UIViewRootEx2) vr;
			this.uniqueViewId = vr2.getUniqueViewId();

			if (localDebug) {
				// Throwable t = new Throwable();
				// t.printStackTrace();
				HS_Util.debug("Constructor executed  - getUniqueViewId:[" + uniqueViewId + "]", "debug", dbarLoc);
				System.out.println(dbarLoc + " Constructor executed  - getUniqueViewId:[" + uniqueViewId + "]");
				ExternalContext extContext = FacesContext.getCurrentInstance().getExternalContext();
				Map rhvm = extContext.getRequestHeaderValuesMap();
				Enumeration ref = (Enumeration) rhvm.get("Referer");
				String referer = "dunno yet";
				if (ref instanceof Enumeration) {
					Object val = (ref).nextElement();
					if (val instanceof String) {
						referer = (String) val;
					} else if (val == null) {
						referer = "seems to be null";
					} else {
						referer = "is not a string: " + val.getClass().getName();
					}
				} else {
					// report what it is instead of an Enumeration
					referer = "ref is not an Enumeration it is a:" + ref.getClass().getName();
				}
				HS_Util.debug("referer:[" + referer + "]", "debug", dbarLoc);
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			e.printStackTrace();
		}
	}

	public String getUniqueViewId() {
		return uniqueViewId;
	}

	public String getComments() {
		return comments;
	}

	public void setUniqueViewId(String uniqueViewId) {
		this.uniqueViewId = uniqueViewId;
	}

	public void setComments(String comments) {
		final boolean localDebug = true;
		final String dbarLoc = "inspectionbean.setComments";
		if (localDebug) HS_Util.debug("Comment: [" + comments + "] uniqueViewId:[" + this.uniqueViewId + "]", "debug", dbarLoc);
		this.comments = comments;
	}

}
