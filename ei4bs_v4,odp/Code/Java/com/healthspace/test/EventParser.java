package com.healthspace.test;

import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.Map.Entry;

import org.openntf.domino.Database;
import org.openntf.domino.DateTime;
import org.openntf.domino.Session;
import org.openntf.domino.View;
import org.openntf.domino.ViewEntry;
import org.openntf.domino.ViewNavigator;

import com.healthspace.general.HS_Util;
import com.healthspace.general.HS_database;
import com.healthspace.tools.JSFUtil;
import com.ibm.domino.services.util.JsonWriter;

//import com.ibm.domino.xsp.module.nsf.NotesContext;
//import com.ibm.xsp.extlib.util.ExtLibUtil;

public class EventParser {
	private Long unixStart;
	private Long unixEnd;

	public EventParser() {
		final boolean localDebug = false;
		final String msgContext = "EventParser.constructor";
		try {
			String startParam = JSFUtil.getParamValue("start");
			String endParam = JSFUtil.getParamValue("end");
			if (startParam != null && !startParam.isEmpty()) {
				setUnixStart(Long.parseLong(startParam));
			}
			if (endParam != null && !endParam.isEmpty()) {
				setUnixEnd(Long.parseLong(endParam));
			}
			if (localDebug) HS_Util.debug("startParam:[" + startParam + "] " + "unixStart:[" + unixStart + "] " + "endParam:[" + endParam + "] " + "unixEnd:[" + unixEnd + "] ", "debug", msgContext);
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if (HS_Util.isDebugServer()) e.printStackTrace();
		}
	}

	public String writeJson() {
		final boolean localDebug = false;
		final String msgContext = "EventParser.writeJson";
		StringWriter sw = new StringWriter();
		JsonWriter jw = new JsonWriter(sw, false);
		try {
			if (localDebug) HS_Util.debug("starting", "debug", msgContext);
			jw.startArray();
			if (localDebug) HS_Util.debug("calling writeEntries", "debug", msgContext);
			writeEntries(jw);
			if (localDebug) HS_Util.debug("ending", "debug", msgContext);
			jw.endArray();
			if (localDebug) HS_Util.debug("done", "debug", msgContext);
		} catch (IOException e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if (HS_Util.isDebugServer()) e.printStackTrace();
		}
		return sw.toString();
	}

	private void writeEntries(JsonWriter jw) {
		final boolean localDebug = HS_Util.isDebugServer();
		final String msgContext = "EventParser.writeEntries";
		try {
			List<Map<String, String>> entryList = getEntryList();
			if (localDebug) HS_Util.debug("got [" + entryList.size() + "] entries.", "debug", msgContext);
			for (Map<String, String> valMap : entryList) {
				jw.startArrayItem();
				jw.startObject();
				for (Entry<String, String> valEntry : valMap.entrySet()) {
					jw.startProperty(valEntry.getKey());
					jw.outStringLiteral(valEntry.getValue());
					jw.endProperty();
				}
				jw.endObject();
				jw.endArrayItem();
			}
		} catch (IOException e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if (HS_Util.isDebugServer()) e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private List<Map<String, String>> getEntryList() {
		final boolean localDebug = HS_Util.isDebugServer();
		final String msgContext = "EventParser.getEntryList";
		List<Map<String, String>> entryList = new ArrayList<Map<String, String>>();
		try {
			// Session sess = NotesContext.getCurrent().getCurrentSession();
			Session sess = HS_Util.getSession();
			HS_database hsdb = new HS_database();
			Database eidb = hsdb.getdb("eiRoot");
			// View entryView = ExtLibUtil.getCurrentDatabase().getView("Events");
			View entryView = eidb.getView("XpagesCalendar");
			entryView.setAutoUpdate(false);
			ViewNavigator viewNav = entryView.createViewNav();
			Date startDate = null;
			DateTime startDateTime = sess.createDateTime("Today");
			if (getUnixStart() != null) {
				startDate = new Date(getUnixStart() * 1000);
				startDateTime = sess.createDateTime(startDate);
			}
			Date endDate = null;
			DateTime endDateTime = sess.createDateTime("Today");
			if (getUnixEnd() != null) {
				endDate = new Date(getUnixEnd() * 1000);
				endDateTime = sess.createDateTime(endDate);
				endDateTime.adjustDay(1);
			}
			if (localDebug) HS_Util.debug("startDateTime:[" + startDateTime.getDateOnly() + "] endDateTime:[" + endDateTime.getDateOnly() + "]", "debug", msgContext);
			ViewEntry startEnt = getStartEntry(entryView, startDateTime);
			if (startEnt == null) {
				startEnt = viewNav.getFirst();
				startDateTime = sess.createDateTime(((DateTime) startEnt.getColumnValues().get(0)).toJavaDate());
				ViewEntry endEnt = viewNav.getLast();
				endDateTime = sess.createDateTime(((DateTime) endEnt.getColumnValues().get(0)).toJavaDate());
			}
			ViewEntry viewEnt = startEnt;
			DateTime entStartDateTime = null;
			DateTime entEndDateTime = null;
			String title = "";
			while (viewEnt != null) {
				Vector colValues = viewEnt.getColumnValues();
				title = colValues.get(4).toString();
				try {
					entStartDateTime = sess.createDateTime(((DateTime) colValues.get(0)).toJavaDate());
					entEndDateTime = sess.createDateTime(((DateTime) colValues.get(1)).toJavaDate());
				} catch (Exception e) {
					HS_Util.debug("col 1:[" + colValues.get(1).toString() + "]", "warn", msgContext);
					HS_Util.debug("col 0:[" + colValues.get(0).toString() + "]", "warn", msgContext);
					HS_Util.debug("On entry:[" + title + "] got error:" + e.toString(), "warn", msgContext);
					entStartDateTime = sess.createDateTime("Today");
					entEndDateTime = sess.createDateTime("Today");
					entStartDateTime.adjustYear(50); // put it out of time...
					entEndDateTime.adjustYear(50); // put it out of time...
				}
				if (entStartDateTime.toJavaDate().getTime() < endDateTime.toJavaDate().getTime()) {
					Map<String, String> valueMap = new HashMap<String, String>();
					if (!colValues.get(2).toString().equals("Some Random Event")) {
						valueMap.put("color", "red");
					}
					valueMap.put("start", getFormattedDate(entStartDateTime.toJavaDate()));
					valueMap.put("end", getFormattedDate(entEndDateTime.toJavaDate()));
					valueMap.put("title", title);

					valueMap.put("color", colValues.get(2).toString());
					valueMap.put("ehs", colValues.get(3).toString());
					valueMap.put("facility", colValues.get(4).toString());
					valueMap.put("reinspection", colValues.get(5).toString());
					valueMap.put("lastinspection", colValues.get(6).toString());
					valueMap.put("unid", colValues.get(7).toString());
					valueMap.put("url", JSFUtil.getDbUrl() + "/formFacility.xsp?documentId=" + colValues.get(7).toString());
					entryList.add(valueMap);

					viewEnt = viewNav.getNext(viewEnt);
				} else {
					viewEnt = null;
				}
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if (HS_Util.isDebugServer()) e.printStackTrace();
		}
		return entryList;
	}

	private String getFormattedDate(Date formatDate) {
		String returnDate = "";
		if (formatDate != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			returnDate = formatter.format(formatDate);
		}
		return returnDate;
	}

	@SuppressWarnings("deprecation")
	private ViewEntry getStartEntry(View entryView, DateTime startDateTime) {
		ViewEntry ent = entryView.getEntryByKey(startDateTime);
		if (ent == null) {
			while (ent == null) {
				startDateTime.adjustDay(1);
				ent = entryView.getEntryByKey(startDateTime);
			}
		}
		return ent;
	}

	public Long getUnixStart() {
		return unixStart;
	}

	public void setUnixStart(Long unixStart) {
		this.unixStart = unixStart;
	}

	public Long getUnixEnd() {
		return unixEnd;
	}

	public void setUnixEnd(Long unixEnd) {
		this.unixEnd = unixEnd;
	}
}
