package com.healthspace.ei.model;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

import javax.faces.component.UIComponent;

import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.View;

import com.healthspace.general.HS_Util;
import com.healthspace.general.HS_database;

public class PermitAppModel implements Serializable {
	private static final long serialVersionUID = 1L;

	private String documentId;
	private String unid;
	private String form;
	private String lastError;

	private boolean valid;

	private String locationUnid;
	private String locationID;
	private String locationName;
	// private String locationAddress;
	private AddressWithParts physicalAddress;
	private String physicalCity;
	private String physicalProvince;
	private String physicalPostalCode;
	private String physicalMunicipality;
	private String physicalCountry;
	private String locationContact;
	private String locationContactLast;
	private String locationContactEmail;
	private String locationContactPhoneInput;
	private String locationContactPhone;
	private String locationContactPhoneArea;
	private String locationContactPhoneExt;
	private String locationContactMobilePhoneInput;
	private String locationContactMobilePhone;
	private String locationContactMobilePhoneArea;

	private String ownerUnid;
	private String ownerID;
	private String ownerName;
	private String ownerAddress;
	private String ownerCity;
	private String ownerProvince;
	private String ownerPostalCode;
	private String ownerCountry;

	private String ownerContact;
	private String ownerContactLast;
	private String ownerPhone;
	private String ownerPhoneArea;
	private String ownerPhoneExt;
	private String ownerPhoneInput;
	private String ownerPhoneCell;
	private String ownerPhoneCellArea;
	private String ownerPhoneCellInput;

	private String ownerEmail;

	private String facilityUnid;
	private String facilityID;
	private String facilityName;
	private String facilityType;

	private String applicantName;
	private String applicantEmail;
	private String applicantPhone;
	private String applicationType;
	private String applicationComments;
	private transient UIComponent applicationCommentsInput;

	private String weigherCommodities;
	private String weigherUSCitizen;
	private String weigherOver18;
	private String weigherAppType;
	private String weigherCurrentLicenseNum;

	private Vector<Reference> references;

	// private ArrayList<String> cbtest;

	private String status;

	public PermitAppModel() {
		this.unid = "";
		this.locationUnid = "";
		this.facilityUnid = "";
		this.valid = false;
		this.lastError = "";
		this.setReferences(new Vector<Reference>());
		// dev stuff
		locationID = "";
	}

	public void clearLocaction() {
		locationUnid = "";
		locationID = "";
		locationName = "";
		// locationAddress = "";
		physicalAddress = new AddressWithParts();
		physicalCity = "";
		physicalProvince = "";
		physicalPostalCode = "";
		physicalCountry = "";
		physicalMunicipality = "";
		locationContact = "";
		locationContactLast = "";
		locationContactPhone = "";
		locationContactPhoneArea = "";
		locationContactPhoneExt = "";
		locationContactMobilePhone = "";
		locationContactMobilePhoneArea = "";
		locationContactEmail = "";

		locationContactPhoneInput = "";
		locationContactMobilePhoneInput = "";
	}

	public void clearOwner() {
		ownerUnid = "";
		ownerID = "";
		ownerName = "";
		ownerAddress = "";
		ownerCity = "";
		ownerProvince = "";
		ownerPostalCode = "";
		ownerCountry = "";

		ownerContact = "";
		ownerContactLast = "";
		ownerPhone = "";
		ownerPhoneArea = "";
		ownerPhoneExt = "";
		ownerPhoneInput = "";

		ownerPhoneCell = "";
		ownerPhoneCellArea = "";
		ownerPhoneCellInput = "";
		ownerEmail = "";
	}

	public boolean copyLocation2Owner() {
		final boolean localDebug = true;
		final String msgContext = this.getClass().getName() + ".copyLocation2Owner";
		boolean result = true;
		// String[] skipThese = new String[] { "locationunid" };

		Field[] fields = this.getClass().getDeclaredFields();
		for (Field field : fields) {
			if ( field.getName().toLowerCase().startsWith("location") ) {
				String sfname = field.getName();
				String tfname = "owner" + sfname.substring(8);
				// if ( localDebug ) HS_Util.debug("copying " + sfname + " ==> " + tfname, "debug", msgContext);
				Object obj;
				try {
					obj = field.get(this);
					if ( obj == null ) obj = "";
					for (Field tfield : fields) {
						if ( tfname.equals(tfield.getName()) ) {
							if ( localDebug ) HS_Util.debug("copying " + sfname + " ==> " + tfname, "debug", msgContext);
							tfield.set(this, obj);
						}
					}
				} catch (IllegalArgumentException e) {
					HS_Util.debug(e.toString(), "error", msgContext);
					if ( HS_Util.isDebugServer() ) e.printStackTrace();
					result = false;
				} catch (IllegalAccessException e) {
					HS_Util.debug(e.toString(), "error", msgContext);
					if ( HS_Util.isDebugServer() ) e.printStackTrace();
					result = false;
				}
			}
		}
		return result;
	}

	public boolean findLocation(String locationID2) {
		final boolean localDebug = true;
		final String msgContext = this.getClass().getSimpleName() + ".findLocation";
		HS_database hsdb = new HS_database();
		Database eidb = hsdb.getdb("eiRoot");
		View docidView = eidb.getView("(DocumentIDLookup)");
		docidView.refresh();
		Document locdoc = docidView.getFirstDocumentByKey(locationID2);
		if ( locdoc == null ) {
			docidView.refresh();
			locdoc = docidView.getFirstDocumentByKey(locationID2);
		}
		if ( locdoc == null ) {
			if ( localDebug ) HS_Util.debug("locdoc is null in view:[" + docidView.getName() + "] with key:[" + locationID2 + "]", "debug", msgContext);
			return false;
		}
		while (!"Premises".equals(locdoc.getFormName())) {
			if ( localDebug )
				HS_Util.debug("locdoc is not a Premises form key:[" + locationID2 + "] docid:[" + locdoc.getItemValueString("DocumentID") + "] form:["
						+ locdoc.getFormName() + "]", "debug", msgContext);

			// Perhaps they entered the key for a facility - Go up a level and see if that is right...
			if ( locdoc.getParentDocument() == null ) {
				if ( localDebug ) HS_Util.debug("locdoc has no more parents.", "debug", msgContext);
				return false;
			}
			locdoc = (Document) locdoc.getParentDocument();
		}
		locationUnid = locdoc.getUniversalID();
		locationID = locdoc.getItemValueString("DocumentID");
		locationName = locdoc.getItemValueString("Name");
		// locationAddress = locdoc.getItemValueString("PhysicalAddress");
		this.getPhysicalAddress().load(locdoc, "Physical");

		physicalCity = locdoc.getItemValueString("PhysicalCity");
		physicalProvince = locdoc.getItemValueString("PhysicalProvince");
		physicalPostalCode = locdoc.getItemValueString("PhysicalPostalCode");
		physicalCountry = locdoc.getItemValueString("PhysicalCountry");
		physicalMunicipality = locdoc.getItemValueString("PhysicalMunicipality");

		locationContact = locdoc.getItemValueString("BuildingContact");
		locationContactLast = locdoc.getItemValueString("ContactLast");

		if ( localDebug ) HS_Util.debug("locationContact:[" + locationContact + "] locationContactLast:[" + locationContactLast + "]", "debug", msgContext);

		locationContactPhone = locdoc.getItemValueString("BuildingContactPhone");
		locationContactPhoneArea = locdoc.getItemValueString("BuildingContactPhoneArea");
		locationContactPhoneExt = locdoc.getItemValueString("BuildingContactPhoneExt");

		locationContactPhoneInput = parsePhoneLoader(getLocationContactPhoneArea(), getLocationContactPhone(), getLocationContactPhoneExt());

		locationContactMobilePhone = locdoc.getItemValueString("BuildingContactMobile");
		locationContactMobilePhoneArea = locdoc.getItemValueString("BuildingContactMobileArea");

		locationContactMobilePhoneInput = parsePhoneLoader(getLocationContactMobilePhoneArea(), getLocationContactMobilePhone(), "");
		locationContactEmail = locdoc.getItemValueString("BuildingEmail");

		return true;
	}

	public boolean findOwner(String ownerID2) {
		try {
			HS_database hsdb = new HS_database();
			Database eidb = hsdb.getdb("eiRoot");
			View docidView = eidb.getView("(DocumentIDLookup)");
			Document ownerdoc = docidView.getFirstDocumentByKey(ownerID2);
			if ( ownerdoc == null ) return false;
			ownerUnid = ownerdoc.getUniversalID();
			ownerID = ownerdoc.getItemValueString("DocumentID");
			ownerName = ownerdoc.getItemValueString("Name");
			ownerAddress = ownerdoc.getItemValueString("PhysicalAddress");
			ownerCity = ownerdoc.getItemValueString("PhysicalCity");
			ownerProvince = ownerdoc.getItemValueString("PhysicalProvince");
			ownerPostalCode = ownerdoc.getItemValueString("PhysicalPostalCode");
			ownerCountry = ownerdoc.getItemValueString("PhysicalCountry");

			ownerContact = ownerdoc.getItemValueString("ContactFirst");
			ownerContactLast = ownerdoc.getItemValueString("ContactLast");

			ownerPhone = ownerdoc.getItemValueString("PhoneDayNumber");
			ownerPhoneArea = ownerdoc.getItemValueString("PhoneDayArea");
			ownerPhoneExt = ownerdoc.getItemValueString("PhoneDayExt");
			ownerPhoneInput = parsePhoneLoader(getOwnerPhoneArea(), getOwnerPhone(), getOwnerPhoneExt());

			ownerPhoneCell = ownerdoc.getItemValueString("PhoneDayCellNumber");
			ownerPhoneCellArea = ownerdoc.getItemValueString("PhoneDayCellArea");
			ownerPhoneCellInput = parsePhoneLoader(getOwnerPhoneCellArea(), getOwnerPhoneCell(), "");

			ownerEmail = ownerdoc.getItemValueString("EMail");

			return true;
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", this.getClass().getName() + ".findOwner");

		}
		return false;
	}

	private String fixnull(String wrkstr) {
		return (wrkstr == null) ? "" : wrkstr;
	}

	public String getApplicantEmail() {
		return applicantEmail;
	}

	public String getApplicantName() {
		return applicantName;
	}

	public String getApplicantPhone() {
		return applicantPhone;
	}

	public String getApplicationComments() {
		if ( applicationComments == null ) applicationComments = "";
		return applicationComments;
	}

	public UIComponent getApplicationCommentsInput() {
		return applicationCommentsInput;
	}

	public String getDocumentId() {
		return documentId;
	}

	public String getFacilityID() {
		return facilityID;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public String getFacilityType() {
		return facilityType;
	}

	public String getFacilityUnid() {
		return facilityUnid;
	}

	public String getForm() {
		return form;
	}

	public String getLastError() {
		return lastError;
	}

	public String getPhysicalCity() {
		return physicalCity;
	}

	public String getLocationContact() {
		return fixnull(locationContact);
	}

	public String getLocationContactEmail() {
		return fixnull(locationContactEmail);
	}

	public String getLocationContactLast() {
		return fixnull(locationContactLast);
	}

	public String getLocationContactMobilePhone() {
		return fixnull(locationContactMobilePhone);
	}

	public String getLocationContactMobilePhoneArea() {
		return fixnull(locationContactMobilePhoneArea);
	}

	public String getLocationContactMobilePhoneInput() {
		return fixnull(locationContactMobilePhoneInput);
	}

	public String getLocationContactPhone() {
		return fixnull(locationContactPhone);
	}

	public String getLocationContactPhoneArea() {
		return fixnull(locationContactPhoneArea);
	}

	public String getLocationContactPhoneExt() {
		return fixnull(locationContactPhoneExt);
	}

	public String getLocationContactPhoneInput() {
		return fixnull(locationContactPhoneInput);
	}

	public String getPhysicalCountry() {
		return fixnull(physicalCountry);
	}

	public String getLocationID() {
		return fixnull(locationID);
	}

	public String getPhysicalMunicipality() {
		return fixnull(physicalMunicipality);
	}

	public String getLocationName() {
		return locationName;
	}

	public String getPhysicalPostalCode() {
		return physicalPostalCode;
	}

	public String getPhysicalProvince() {
		return physicalProvince;
	}

	public String getLocationUnid() {
		if ( locationUnid == null ) locationUnid = "";
		return locationUnid;
	}

	public String getOwnerAddress() {
		return fixnull(ownerAddress);
	}

	public String getOwnerCity() {
		return fixnull(ownerCity);
	}

	public String getOwnerContact() {
		return fixnull(ownerContact);
	}

	public String getOwnerContactLast() {
		return fixnull(ownerContactLast);
	}

	public String getOwnerCountry() {
		return fixnull(ownerCountry);
	}

	public String getOwnerEmail() {
		return fixnull(ownerEmail);
	}

	public String getOwnerID() {
		return fixnull(ownerID);
	}

	public String getOwnerName() {
		return fixnull(ownerName);
	}

	public String getOwnerPhone() {
		return fixnull(ownerPhone);
	}

	public String getOwnerPhoneArea() {
		return fixnull(ownerPhoneArea);
	}

	public String getOwnerPhoneExt() {
		return fixnull(ownerPhoneExt);
	}

	public String getOwnerPhoneInput() {
		return fixnull(ownerPhoneInput);
	}

	public String getOwnerPostalCode() {
		return fixnull(ownerPostalCode);
	}

	public String getOwnerProvince() {
		return fixnull(ownerProvince);
	}

	public String getOwnerUnid() {
		return fixnull(ownerUnid);
	}

	public String getStatus() {
		if ( status == null ) status = "Draft";
		if ( "".equals(status) ) status = "Draft";
		return status;
	}

	public String getUnid() {
		return unid;
	}

	public boolean isValid() {
		return valid;
	}

	public boolean loadPermitApp() {
		final String msgContext = this.getClass().getName() + ".loadPermitApp";
		final boolean localDebug = ("Yes".equals(HS_Util.getAppSettingString("debug_permitApp")));
		// final boolean localDebug = false;
		boolean result = false;

		HS_database hsdb = new HS_database();
		Database eidb = hsdb.getdbAsSigner("eiRoot");
		Document appdoc = null;
		if ( "".equals(this.unid) ) return result; // cannot load it if we do not have it....
		try {
			appdoc = eidb.getDocumentByUNID(this.unid);
			if ( appdoc == null ) throw (new Exception("Unable to access document with UNID:[" + this.unid + "]"));

			if ( "Cancelled".equals(appdoc.getItemValueString("Status")) ) {
				status = "Cancelled";
				return false;
			}
			String[] skipThese = new String[] { "xxx" };
			// Use reflection process to retreive as much as we can...
			Field[] fields = this.getClass().getDeclaredFields();
			for (Field field : fields) {
				if ( appdoc.hasItem(field.getName()) && !HS_Util.isMember(field.getName().toLowerCase(), skipThese) ) {
					try {
						// HS_Util.debug("Filed:[" + field.getName() + "] is a genericType:[" + field.getGenericType().toString() + "] and a class named:[" +
						// field.getClass().getName() + "]", "debug", msgContext);

						if ( field.getGenericType().toString().toLowerCase().contains("arraylist") ) {
							// HS_Util.debug("Field:[" + field.getName() + "] is a genericType:[" + field.getGenericType().toString() + "]", "debug",
							// msgContext);
							Vector<Object> wobj1 = appdoc.getItemValue(field.getName());
							ArrayList<Object> wal2 = new ArrayList<Object>();
							for (Object wobj : wobj1)
								wal2.add(wobj);
							field.set(this, wal2);
						} else if ( field.getGenericType().toString().toLowerCase().contains("addresswithparts") ) {
							HS_Util.debug("Field:[" + field.getName() + "] is a genericType:[" + field.getGenericType().toString() + "]", "debug", msgContext);
							AddressWithParts waddr = null;
							if ( "physicalAddress".equals(field.getName()) ) {
								waddr = this.getPhysicalAddress();
							} else {
								throw (new Exception("not prepared to handle this address field:[" + field.getName()));
							}
							// waddr = (AddressWithParts) field.get(null);
							waddr.load(appdoc, "Physical");
							HS_Util.debug("Loaded address:[" + waddr.getAddress() + "] from:[" + field.getName() + "]", "debug", msgContext);
						} else {
							Vector<Object> obj = appdoc.getItemValue(field.getName());
							if ( obj.size() == 0 ) obj.add("");
							field.set(this, obj.get(0));
						}
					} catch (Exception e1) {
						// if ( localDebug ) HS_Util.debug("Failed to load content into field:[" + field.getName() + "]", "debug", msgContext);
						HS_Util.debug(
								"Failed to load content into field:[" + field.getName() + "] a:[" + field.getGenericType().toString().toLowerCase() + "]",
								"error", msgContext);
						e1.printStackTrace();
					}
				}
			}

			if ( "".equals(getLocationContactPhoneInput()) ) {
				locationContactPhoneInput = parsePhoneLoader(getLocationContactPhoneArea(), getLocationContactPhone(), getLocationContactPhoneExt());
			}
			if ( "".equals(getLocationContactMobilePhoneInput()) ) {
				locationContactMobilePhoneInput = parsePhoneLoader(getLocationContactMobilePhoneArea(), getLocationContactMobilePhone(), "");
			}
			if ( "".equals(getOwnerPhoneInput()) ) {
				ownerPhoneInput = parsePhoneLoader(getOwnerPhoneArea(), getOwnerPhone(), getOwnerPhoneExt());
			}
			if ( "".equals(getOwnerPhoneCellInput()) ) {
				ownerPhoneCellInput = parsePhoneLoader(getOwnerPhoneCellArea(), getOwnerPhoneCell(), "");
			}
			if ( appdoc.hasItem("RefNames") ) loadReferences(appdoc);

			if ( localDebug )
				HS_Util.debug("Document with UNID:[" + this.unid + "] loaded: locationName:[" + locationName + "] owner:[" + ownerName + "]", "debug",
						msgContext);
			result = true;
		} catch (Exception e) {
			HS_Util.debug("ERROR: " + e.toString(), "error", msgContext);
			result = false;
		}
		return result;
	}

	private String parsePhoneLoader(String area, String phone, String ext) {
		String out = ("".equals(area)) ? "" : "(" + area + ") ";
		out += ("".endsWith(phone)) ? "" : phone;
		out += ("".equals(ext)) ? "" : (ext.startsWith("x")) ? " " + ext : " x" + ext;
		return out;
	}

	private Vector<String> parsePhoneSaver(String iph) {
		final boolean localDebug = false;
		final String msgContext = this.getClass().getSimpleName() + ".parsePhoneSaver";

		Vector<String> out = new Vector<String>();
		out.add("");
		out.add("");
		out.add("");

		if ( localDebug ) HS_Util.debug("Started with:[" + iph + "]", "debug", msgContext);

		String wph = iph.replaceAll("[()]", "");
		wph = wph.replaceAll(" ", "");
		wph = wph.replaceAll("-", "");
		wph = wph.replaceAll("[.]", "");

		if ( localDebug ) HS_Util.debug("After replaces 1:[" + wph + "]", "debug", msgContext);
		if ( wph.startsWith("+") ) wph = wph.substring(1);
		if ( wph.startsWith("1") ) wph = wph.substring(1);
		if ( localDebug ) HS_Util.debug("After replaces 2:[" + wph + "] length:" + wph.length() + ".", "debug", msgContext);
		if ( wph.length() > 10 ) {
			out.set(2, wph.substring(10));
			if ( out.get(2).startsWith("ext") ) out.set(2, out.get(2).substring(3));
			if ( out.get(2).startsWith("x") ) out.set(2, out.get(2).substring(1));
			wph = wph.substring(0, 10);
			if ( localDebug )
				HS_Util.debug("Ext extracted as::[" + out.get(2) + "] left over is:[" + wph + "] length:" + wph.length() + ".", "debug", msgContext);
		}
		if ( wph.length() == 10 ) {
			out.set(0, wph.substring(0, 3));
			out.set(1, wph.substring(3, 6) + "-" + wph.substring(6));
			if ( localDebug ) HS_Util.debug("Area extracted as:[" + out.get(0) + "] phone extracted as:[" + out.get(1) + "]", "debug", msgContext);
		} else {
			out.set(1, iph);
			if ( localDebug ) HS_Util.debug("Could not figure out the parsing - phone extracted as:[" + out.get(1) + "]", "debug", msgContext);
		}
		return out;
	}

	public boolean save() throws Exception {
		final String msgContext = this.getClass().getName() + ".save";
		// final boolean localDebug = true;
		final boolean localDebug = ("Yes".equals(HS_Util.getAppSettingString("debug_permitApp")));

		HS_database hsdb = new HS_database();
		Database eidb = hsdb.getdbAsSigner("eiRoot");
		Document appdoc = null;
		if ( "".equals(this.unid) ) {
			appdoc = eidb.createDocument();
			appdoc.replaceItemValue("Form", "PermitApplication");
			documentId = "PMAPP-" + (HS_Util.getSession().evaluate("@Unique").get(0));
			appdoc.replaceItemValue("DocumentID", documentId);

		} else {
			// if ( localDebug ) HS_Util.debug("Not yet accepting updates to applications. Booo...", "warn", msgContext);
			// throw (new Exception("Not yet accepting updates to applications. Booo..."));
			appdoc = eidb.getDocumentByUNID(this.unid);
			if ( appdoc == null ) throw (new Exception("Unable to access application document with unid: [" + this.unid + "]"));
			if ( appdoc.hasItem("DeletedBy") && !("".equals(appdoc.getItemValueString("DeletedBy"))) )
				throw (new Exception("Permit application submission time has expired."));
		}
		if ( !appdoc.hasItem("AppSourceServer") ) appdoc.replaceItemValue("AppSourceServer", eidb.getServer());
		if ( !appdoc.hasItem("AppSourceFilepath") ) appdoc.replaceItemValue("AppSourceFilepath", eidb.getFilePath());
		if ( !appdoc.hasItem("AppSourceTitle") ) appdoc.replaceItemValue("AppSourceTitle", eidb.getTitle());
		if ( !appdoc.hasItem("AppSourceReplid") ) appdoc.replaceItemValue("AppSourceReplid", eidb.getReplicaID());

		if ( this.references.size() > 0 ) saveReferences(appdoc);

		// appdoc.replaceItemValue("cbtest", cbtest);

		String[] skipThese = new String[] { "serialversionuid", "unid", "form", "lastError", "valid", "applicationcommentsinput", "references", "xxx" };

		// PARSE AND HANDLE THE PHONE NUMBERS HERE.
		Vector<String> wph = parsePhoneSaver(getLocationContactPhoneInput());
		setLocationContactPhoneArea(wph.get(0));
		setLocationContactPhone(wph.get(1));
		setLocationContactPhoneExt(wph.get(2));

		wph = parsePhoneSaver(getLocationContactMobilePhoneInput());
		setLocationContactMobilePhoneArea(wph.get(0));
		setLocationContactMobilePhone(wph.get(1));

		wph = parsePhoneSaver(getOwnerPhoneInput());
		setOwnerPhoneArea(wph.get(0));
		setOwnerPhone(wph.get(1));
		setOwnerPhoneExt(wph.get(2));

		wph = parsePhoneSaver(getOwnerPhoneCellInput());
		setOwnerPhoneCellArea(wph.get(0));
		setOwnerPhoneCell(wph.get(1));

		Field[] fields = this.getClass().getDeclaredFields();
		for (Field field : fields) {
			if ( !HS_Util.isMember(field.getName().toLowerCase(), skipThese) && !field.getName().toLowerCase().endsWith("input") ) {
				Object obj = field.get(this);
				if ( obj == null ) obj = "";
				if ( obj instanceof AddressWithParts ) {
					((AddressWithParts) obj).save(appdoc, field.getName());
				} else {
					appdoc.replaceItemValue(field.getName(), obj);
				}
			}
		}

		if ( appdoc.save() ) {
			this.unid = appdoc.getUniversalID();
			if ( localDebug ) HS_Util.debug("save successful to:[" + hsdb.getdbBang("eiRoot") + "]", "debug", msgContext);
			return true;
		} else {
			if ( localDebug ) HS_Util.debug("save failed to:[" + hsdb.getdbBang("eiRoot") + "]", "warn", msgContext);
			return false;
		}
	}

	private void saveReferences(Document appdoc) {
		Vector<String> rnames = new Vector<String>();
		Vector<String> raddr = new Vector<String>();
		Vector<String> rpostal = new Vector<String>();
		Vector<String> rphone = new Vector<String>();
		Vector<String> remail = new Vector<String>();

		Reference ref = null;
		Iterator<Reference> itr = references.iterator();
		while (itr.hasNext()) {
			ref = itr.next();
			rnames.add(HS_Util.cvrtToSpace(ref.getRefName()));
			raddr.add(HS_Util.cvrtToSpace(ref.getRefAddress()));
			rpostal.add(HS_Util.cvrtToSpace(ref.getRefPostalCode()));
			rphone.add(HS_Util.cvrtToSpace(ref.getRefPhone()));
			remail.add(HS_Util.cvrtToSpace(ref.getRefEmail()));
		}
		appdoc.replaceItemValue("RefNames", rnames);
		appdoc.replaceItemValue("RefAddresses", raddr);
		appdoc.replaceItemValue("RefPostalCodes", rpostal);
		appdoc.replaceItemValue("RefPhones", rphone);
		appdoc.replaceItemValue("RefEmails", remail);
	}

	private void loadReferences(Document appdoc) {
		final boolean localDebug = false;
		final String msgContext = this.getClass().getName() + ".loadReferences";

		Vector<Object> rnames = appdoc.getItemValue("RefNames");
		int numrefs = rnames.size();

		if ( localDebug ) HS_Util.debug("Loading [" + numrefs + "] references.", "debug", msgContext);

		Vector<Object> raddr = appdoc.getItemValue("RefAddresses");
		Vector<Object> rpostal = appdoc.getItemValue("RefPostalCodes");
		Vector<Object> rphone = appdoc.getItemValue("RefPhones");
		Vector<Object> remail = appdoc.getItemValue("RefEmails");

		this.references = new Vector<Reference>();
		Reference ref = null;
		for (int i = 0; i < rnames.size(); i++) {
			ref = new Reference();
			ref.setRefName((String) rnames.get(i));
			ref.setRefAddress((String) raddr.get(i));
			ref.setRefPostalCode((String) rpostal.get(i));
			ref.setRefPhone((String) rphone.get(i));
			ref.setRefEmail((String) remail.get(i));
			this.references.add(ref);
		}
		if ( localDebug ) HS_Util.debug("Loaded [" + this.references.size() + "] references.", "debug", msgContext);
	}

	public void setApplicantEmail(String applicantEmail) {
		this.applicantEmail = applicantEmail;
	}

	public void setApplicantName(String applicantName) {
		this.applicantName = applicantName;
	}

	public void setApplicantPhone(String applicantPhone) {
		this.applicantPhone = applicantPhone;
	}

	public void setApplicationComments(String applicationComments) {
		this.applicationComments = applicationComments;
	}

	public void setApplicationCommentsInput(UIComponent applicationCommentsInput) {
		this.applicationCommentsInput = applicationCommentsInput;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public void setFacilityID(String facilityID) {
		this.facilityID = facilityID;
	}

	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}

	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	public void setFacilityUnid(String facilityUnid) {
		this.facilityUnid = facilityUnid;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public void setLastError(String lastError) {
		this.lastError = lastError;
	}

	public AddressWithParts getPhysicalAddress() {
		if ( physicalAddress == null ) {
			physicalAddress = new AddressWithParts();
			physicalAddress.setFldPrefix("Physical");
		}
		return physicalAddress;
	}

	public void setPhysicalAddress(AddressWithParts physicalAddress) {
		this.physicalAddress = physicalAddress;
	}

	public void setPhysicalCity(String physicalCity) {
		this.physicalCity = physicalCity;
	}

	public void setLocationContact(String locationContact) {
		this.locationContact = locationContact;
	}

	public void setLocationContactEmail(String locationContactEmail) {
		this.locationContactEmail = locationContactEmail;
	}

	public void setLocationContactLast(String locationContactLast) {
		this.locationContactLast = locationContactLast;
	}

	public void setLocationContactMobilePhone(String locationContactMobilePhone) {
		this.locationContactMobilePhone = locationContactMobilePhone;
	}

	public void setLocationContactMobilePhoneArea(String locationContactMobilePhoneArea) {
		this.locationContactMobilePhoneArea = locationContactMobilePhoneArea;
	}

	public void setLocationContactMobilePhoneInput(String locationContactMobilePhoneInput) {
		this.locationContactMobilePhoneInput = locationContactMobilePhoneInput;
	}

	public void setLocationContactPhone(String locationContactPhone) {
		this.locationContactPhone = locationContactPhone;
	}

	public void setLocationContactPhoneArea(String locationContactPhoneArea) {
		this.locationContactPhoneArea = locationContactPhoneArea;
	}

	public void setLocationContactPhoneExt(String locationContactPhoneExt) {
		this.locationContactPhoneExt = locationContactPhoneExt;
	}

	public void setLocationContactPhoneInput(String locationContactPhoneInput) {
		this.locationContactPhoneInput = locationContactPhoneInput;
	}

	public void setPhysicalCountry(String physicalCountry) {
		this.physicalCountry = physicalCountry;
	}

	public void setLocationID(String locationID) {
		this.locationID = locationID;
	}

	public void setPhysicalMunicipality(String physicalMunicipality) {
		this.physicalMunicipality = physicalMunicipality;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public void setPhysicalPostalCode(String physicalPostalCode) {
		this.physicalPostalCode = physicalPostalCode;
	}

	public void setPhysicalProvince(String physicalProvince) {
		this.physicalProvince = physicalProvince;
	}

	public void setLocationUnid(String locationUnid) {
		this.locationUnid = locationUnid;
	}

	public void setOwnerAddress(String ownerAddress) {
		this.ownerAddress = ownerAddress;
	}

	public void setOwnerCity(String ownerCity) {
		this.ownerCity = ownerCity;
	}

	public void setOwnerContact(String ownerContact) {
		this.ownerContact = ownerContact;
	}

	public void setOwnerContactLast(String ownerContactLast) {
		this.ownerContactLast = ownerContactLast;
	}

	public void setOwnerCountry(String ownerCountry) {
		this.ownerCountry = ownerCountry;
	}

	public void setOwnerEmail(String ownerEmail) {
		this.ownerEmail = ownerEmail;
	}

	public void setOwnerID(String ownerID) {
		this.ownerID = ownerID;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public void setOwnerPhone(String ownerPhone) {
		this.ownerPhone = ownerPhone;
	}

	public void setOwnerPhoneArea(String ownerPhoneArea) {
		this.ownerPhoneArea = ownerPhoneArea;
	}

	public void setOwnerPhoneExt(String ownerPhoneExt) {
		this.ownerPhoneExt = ownerPhoneExt;
	}

	public void setOwnerPhoneInput(String ownerPhoneInput) {
		this.ownerPhoneInput = ownerPhoneInput;
	}

	public void setOwnerPostalCode(String ownerPostalCode) {
		this.ownerPostalCode = ownerPostalCode;
	}

	public void setOwnerProvince(String ownerProvince) {
		this.ownerProvince = ownerProvince;
	}

	public void setOwnerUnid(String ownerUnid) {
		this.ownerUnid = ownerUnid;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setUnid(String unid) {
		this.unid = unid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public boolean validate(String mode, ArrayList<String> messages) {
		final String msgContext = this.getClass().getName() + ".savePermit";
		// final boolean localDebug = true;
		final boolean localDebug = ("Yes".equals(HS_Util.getAppSettingString("debug_permitApp")));

		if ( localDebug ) HS_Util.debug("Validating with mode:[" + mode + "]", "debug", msgContext);

		this.valid = true; // Start with it OK....

		if ( "".equals(mode) ) return false;
		// Things we allways have to have...
		if ( "".equals(applicantName) ) {
			messages.add("Please provide a name in the applicant infomation secition");
			this.valid = false;
		}
		if ( "".equals(applicantEmail) ) {
			messages.add("Please provide an email address in the applicant infomation secition");
			this.valid = false;
		}

		if ( "save".equals(mode) ) {

		} else if ( "submit".equals(mode) ) {
			messages.add("Submit validation not complete.");
			this.valid = false;
		}
		if ( localDebug ) HS_Util.debug("Done validating with mode:[" + mode + "] result:[" + this.valid + "]", "debug", msgContext);
		return this.valid;
	}

	public String getOwnerPhoneCell() {
		return fixnull(ownerPhoneCell);
	}

	public void setOwnerPhoneCell(String ownerPhoneCell) {
		this.ownerPhoneCell = ownerPhoneCell;
	}

	public String getOwnerPhoneCellArea() {
		return fixnull(ownerPhoneCellArea);
	}

	public void setOwnerPhoneCellArea(String ownerPhoneCellArea) {
		this.ownerPhoneCellArea = ownerPhoneCellArea;
	}

	public String getOwnerPhoneCellInput() {
		return fixnull(ownerPhoneCellInput);
	}

	public void setOwnerPhoneCellInput(String ownerPhoneCellInput) {
		this.ownerPhoneCellInput = ownerPhoneCellInput;
	}

	public String getApplicationType() {
		if ( "".equals(applicationType) ) applicationType = "-select-";
		return applicationType;
	}

	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	public String getWeigherCommodities() {
		return weigherCommodities;
	}

	public void setWeigherCommodities(String weigherCommodities) {
		this.weigherCommodities = weigherCommodities;
	}

	public String getWeigherUSCitizen() {
		return weigherUSCitizen;
	}

	public void setWeigherUSCitizen(String weigherUSCitizen) {
		this.weigherUSCitizen = weigherUSCitizen;
	}

	public String getWeigherOver18() {
		return weigherOver18;
	}

	public void setWeigherOver18(String weigherOver18) {
		this.weigherOver18 = weigherOver18;
	}

	public String getWeigherAppType() {
		return weigherAppType;
	}

	public void setWeigherAppType(String weigherAppType) {
		this.weigherAppType = weigherAppType;
	}

	public String getWeigherCurrentLicenseNum() {
		return weigherCurrentLicenseNum;
	}

	public void setWeigherCurrentLicenseNum(String weigherCurrentLicenseNum) {
		this.weigherCurrentLicenseNum = weigherCurrentLicenseNum;
	}

	public void setReferences(Vector<Reference> references) {
		this.references = references;
	}

	public Vector<Reference> getReferences() {
		return references;
	}

}
