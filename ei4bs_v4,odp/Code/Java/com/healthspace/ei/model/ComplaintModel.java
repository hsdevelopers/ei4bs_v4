package com.healthspace.ei.model;

import java.lang.reflect.Field;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import javax.faces.component.UIComponent;

import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.Session;
import org.openntf.domino.View;

import com.healthspace.general.HS_Util;
import com.healthspace.general.HS_database;

public class ComplaintModel {

	private static final long		serialVersionUID	= 1L;

	private String					documentID;
	private String					form;
	private String					lastError;
	private Date					dateCreated;
	private String					dbDesign;
	private String					singleMulti;
	private boolean					anonymous;
	private String					anonymousInput;

	// General details
	private String					critical;
	private String[]				typeMulti;
	private String					typeSingle;
	private String					type;
	private String					dateStr;
	private Date					date;
	private String					timeStr;
	private Time					time;
	private Date					dateStart;
	private Time					timeStart;
	private Date					dateClosed;
	private String					receivedBy;
	private String					referredTo;
	private String					confirmed;
	private String					complaintId;
	private String					interfaceId;

	private Date					viewDate;
	private String					viewDescription;

	// Complainant Information
	private String					complainantName;
	// private int complainantAge;
	private String					verified;

	private String					areaCode;
	private String					phoneNumber;
	private String					areaCode_1;
	private String					phoneNumber_1;
	private String					complainantEveningPhone;
	private String					complainantPhone;
	private AddressWithParts		complainantAddress;
	private String					complainantEmail;
	private String					address;					// This item is badly named...
	private String					complainantCity;
	private String					complainantProvince;
	private String					complainantPostalCode;
	private String					complainantCountry;
	private String					resultsRequested;

	// Facility Details
	private String					name;
	private AddressWithParts		physicalAddress;
	// Computed values that should NOT be saved..
	private String[]				pl_complaintTypes;
	private String					parentRef;
	private String					unid;
	private boolean					valid;

	// Animal Welfare Details
	private String					animalType;
	// private String[] animalType;
	private String					countyIncident;
	private String					abuseDateStr;
	private Date					abuseDate;
	private String					animalOwner;
	// private String animalAddress;
	private AddressWithParts		animalAddress;
	private String					animalPostalCode;
	private String					animalCity;
	private String					animalProvince;
	private String					animalDirections;
	private String					previousIncidentDateStr;
	private Date					previousIncidentDate;
	private String					feed;
	private String					water;

	// Other details
	private String					details;
	private transient UIComponent	detailsInput;

	public ComplaintModel() {
		this.init();
	}

	public void addMockData() {
		String msgContext = this.getClass().getName() + ".addMockData";
		try {
			this.type = "Animal Welfare";
			this.anonymous = true;
			this.anonymousInput = "Yes";

			this.complainantName = "Joe Complainsalot";
			// this.complainantAddress = "123 Main St. SW";
			this.complainantAddress = new AddressWithParts();

			this.complainantAddress.setBuilding("123");
			this.complainantAddress.setStreetName("Main");
			this.complainantAddress.setStreetSuffix("SW");
			this.complainantAddress.setStreetType("St");
			this.complainantAddress.setAddress("123 Main St SW");

			this.complainantCity = "Small town";
			this.complainantProvince = "TN";
			this.complainantPostalCode = "55555";
			this.complainantPhone = "513-555-1234";
			this.complainantEveningPhone = "513-555-4321";
			this.complainantEmail = "test@tester.com";

			this.countyIncident = "Cheatham";
			this.abuseDate = new Date();
			this.abuseDateStr = HS_Util.formatDate(this.abuseDate, "dd-MMM-yyyy");
			this.animalType = "Pig;Sheep;Goat";
			this.animalOwner = "Bad Dude Jones";

			this.animalAddress = new AddressWithParts();
			this.animalAddress.setBuilding("444");
			this.animalAddress.setDirection("N");
			this.animalAddress.setStreetName("Central");
			this.animalAddress.setStreetSuffix("");
			this.animalAddress.setStreetType("Ave");
			this.animalAddress.setAddress("444 N Central Ave");

			this.animalCity = "Smallerton";
			this.animalProvince = "TN";
			this.animalPostalCode = "54545";

			this.setAnimalDirections("From the 7-11 on HWY 64 go east 1.4 miles then turn right on Jessy Farm Rd and go south 2.4 miles");

			// this.previousIncidentDate = new Date("01/01/2017");
			// this.previousIncidentDateStr = HS_Util.formatDate(this.previousIncidentDate, "dd-MMM-yyyy");

			this.feed = "No";
			this.water = "Yes";

			this.details = "Here is a summary of the issues in this here complaint.";
			this.details += " Bacon ipsum dolor amet ham hock sirloin burgdoggen pig, meatball corned beef filet mignon turkey picanha ham.";
			this.details += " Pancetta pig chuck corned beef bacon. Tongue boudin burgdoggen, short loin chicken leberkas landjaeger doner chuck.";
			this.details += " Beef ribs tri-tip meatloaf, t-bone turkey ball tip brisket meatball.";
			this.details += " Short loin tongue cupim chicken pork chop landjaeger turkey pork loin pig spare ribs frankfurter t-bone chuck capicola.";
			this.details += " Turkey pig shank, boudin pork loin cupim alcatra ribeye pastrami pork pork chop sausage prosciutto.";
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			e.printStackTrace();
		}
	}

	public Date getAbuseDate() {
		return abuseDate;
	}

	public String getAbuseDateStr() {
		return abuseDateStr;
	}

	public String getAddress() {
		return address;
	}

	public String getAnimalCity() {
		return animalCity;
	}

	public String getAnimalDirections() {
		return animalDirections;
	}

	public String getAnimalOwner() {
		return animalOwner;
	}

	public String getAnimalPostalCode() {
		return animalPostalCode;
	}

	public String getAnimalProvince() {
		return animalProvince;
	}

	public String getAnimalType() {
		if ( animalType == null ) animalType = "";
		return animalType;
	}

	public String getAnonymousInput() {
		return anonymousInput;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public String getAreaCode_1() {
		return areaCode_1;
	}

	public AddressWithParts getComplainantAddress() {
		if ( complainantAddress == null ) complainantAddress = new AddressWithParts();
		return complainantAddress;
	}

	public String getComplainantCity() {
		return complainantCity;
	}

	public String getComplainantCountry() {
		return complainantCountry;
	}

	public String getComplainantEmail() {
		return complainantEmail;
	}

	public String getComplainantEveningPhone() {
		return complainantEveningPhone;
	}

	public String getComplainantName() {
		if ( complainantName == null ) complainantName = "";
		return complainantName;
	}

	public String getComplainantPhone() {
		return complainantPhone;
	}

	public String getComplainantPostalCode() {
		return complainantPostalCode;
	}

	public String getComplainantProvince() {
		return complainantProvince;
	}

	public String getComplaintId() {
		return complaintId;
	}

	public String getConfirmed() {
		return confirmed;
	}

	public String getCountyIncident() {
		return countyIncident;
	}

	public String getCritical() {
		return critical;
	}

	public Date getDate() {
		return date;
	}

	public Date getDateClosed() {
		return dateClosed;
	}

	public Date getDateCreated() {
		if ( dateCreated == null ) dateCreated = new Date();
		return dateCreated;
	}

	public Date getDateStart() {
		return dateStart;
	}

	public String getDateStr() {
		return dateStr;
	}

	public String getDbDesign() {
		return dbDesign;
	}

	public String getDetails() {
		return details;
	}

	public UIComponent getDetailsInput() {
		return detailsInput;
	}

	public String getDocumentID() {
		return documentID;
	}

	public String getFeed() {
		return feed;
	}

	public String getForm() {
		if ( "".equals(form) ) form = "ComplaintIntake";
		return form;
	}

	public String getInterfaceId() {
		return interfaceId;
	}

	public String getLastError() {
		return lastError;
	}

	public String getName() {
		return name;
	}

	public String getParentRef() {
		return parentRef;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getPhoneNumber_1() {
		return phoneNumber_1;
	}

	public AddressWithParts getPhysicalAddress() {
		if ( physicalAddress == null ) physicalAddress = new AddressWithParts();
		return physicalAddress;
	}

	public String[] getPl_complaintTypes() {
		return pl_complaintTypes;
	}

	public Date getPreviousIncidentDate() {
		return previousIncidentDate;
	}

	public String getPreviousIncidentDateStr() {
		return previousIncidentDateStr;
	}

	public String getReceivedBy() {
		return receivedBy;
	}

	public String getReferredTo() {
		return referredTo;
	}

	public String getResultsRequested() {
		return resultsRequested;
	}

	public String getSingleMulti() {
		return singleMulti;
	}

	public Time getTime() {
		return time;
	}

	public Time getTimeStart() {
		return timeStart;
	}

	public String getTimeStr() {
		return timeStr;
	}

	public String getType() {
		return type;
	}

	public String[] getTypeMulti() {
		return typeMulti;
	}

	public String getTypeSingle() {
		return typeSingle;
	}

	public String getUnid() {
		return unid;
	}

	public String getVerified() {
		return verified;
	}

	public Date getViewDate() {
		return viewDate;
	}

	public String getViewDescription() {
		return viewDescription;
	}

	public String getWater() {
		return water;
	}

	public void init() {
		this.unid = "";
		this.valid = false;
		this.lastError = "";
		this.singleMulti = HS_Util.getGlobalSettingsString("UseCompTypeMulti");
		String[] plist = HS_Util.getGlobalSettingsArray("ComplaintTypes");
		this.pl_complaintTypes = plist;
		this.documentID = "";
	}

	public boolean isAnonymous() {
		return anonymous;
	}

	public boolean isValid() {
		return valid;
	}

	public boolean load(String key) {
		// this key is the unique key of the document. UNID would be
		// faster/easier.. I just kinda hate using them and seeing them in URLS
		// Session session = Factory.getSession();
		// Database db = session.getCurrentDatabase();
		HS_database hsdb = new HS_database();
		Database eirootDb = hsdb.getdb("eiroot");

		View view = eirootDb.getView("What View Should I Use??");
		Document doc = view.getFirstDocumentByKey(key);

		if ( null == doc ) {
			// document not found. DANGER
			this.lastError = "document not found for key:[" + key + "]";
			HS_Util.debug(this.lastError, "warn", HS_Util.debugCalledFrom());
			this.valid = false;
		} else {
			this.loadValues(doc);
			// this.setLoadedForm(new HS_auditForm(doc));
		}

		return this.valid;
	}

	public boolean loadNew() {
		final String msgContext = this.getClass().getName() + ".loadValues";
		final boolean localDebug = ("Yes".equals(HS_Util.getAppSettingString("debug_complaints")));
		try {
			Session sess = HS_Util.getSession();
			this.unid = "";
			this.documentID = (String) sess.evaluate("@Unique").get(0);
			this.complaintId = this.documentID;
			this.form = "ComplaintIntake";
			this.dateCreated = new Date();
			this.date = new Date();
			this.time = new Time(System.currentTimeMillis());
			this.dateStr = HS_Util.formatDate(this.date, "dd-MMM-yyyy");
			this.timeStr = HS_Util.formatDate(this.date, "hh:mm");
			this.dbDesign = HS_Util.getMasterSettingsString("DBDesign");
			this.critical = "";
			this.confirmed = "No";
			this.receivedBy = "WWW Input";

			this.valid = false;
			if ( localDebug ) HS_Util.debug("ComplaintIntake setup with id: " + this.documentID, "debug", msgContext);
			return true;
		} catch (Exception e) {
			this.lastError = e.toString();
			HS_Util.debug(this.lastError, "error", HS_Util.debugCalledFrom() + ".loadNew");
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			return false;
		}
	}

	private void loadValues(Document doc) {
		final String msgContext = this.getClass().getName() + ".loadValues";
		// final boolean localDebug =
		// ("Yes".equals(HS_Util.getAppSettingString("debug_complaints")));

		try {
			this.unid = doc.getUniversalID();
			this.documentID = HS_Util.loadValueString(doc, "documentID");
			this.form = doc.getFormName();
			this.parentRef = doc.getParentDocumentUNID();

			String[] skipThese = new String[] { "form", "documentid", "pl_complainttypes", "xxx" };
			// Use reflection process to retreive as much as we can...
			Field[] fields = this.getClass().getDeclaredFields();
			for (Field field : fields) {
				if ( doc.hasItem(field.getName()) && !HS_Util.isMember(field.getName().toLowerCase(), skipThese) ) {
					try {

						if ( field.getGenericType().toString().toLowerCase().contains("arraylist") ) {
							Vector<Object> wobj1 = doc.getItemValue(field.getName());
							ArrayList<Object> wal2 = new ArrayList<Object>();
							for (Object wobj : wobj1)
								wal2.add(wobj);
							field.set(this, wal2);
						} else if ( field.getGenericType().toString().toLowerCase().contains("addresswithparts") ) {
							HS_Util.debug("Field:[" + field.getName() + "] is a genericType:[" + field.getGenericType().toString() + "]", "debug", msgContext);
							AddressWithParts waddr = null;
							if ( "physicalAddress".equals(field.getName()) ) {
								waddr = this.getPhysicalAddress();
							} else {
								throw (new Exception("not prepared to handle this address field:[" + field.getName()));
							}
							waddr.load(doc, "Physical");
							HS_Util.debug("Loaded address:[" + waddr.getAddress() + "] from:[" + field.getName() + "]", "debug", msgContext);
						} else {
							Vector<Object> obj = doc.getItemValue(field.getName());
							if ( obj.size() == 0 ) obj.add("");
							field.set(this, obj.get(0));
						}
					} catch (Exception e1) {
						HS_Util.debug("Failed to load content into field:[" + field.getName() + "] a:[" + field.getGenericType().toString().toLowerCase() + "]", "error", msgContext);
						e1.printStackTrace();
					}
				}
			}
		} catch (Exception e) {
			this.lastError = e.toString();
			HS_Util.debug(this.lastError, "error", HS_Util.debugCalledFrom() + ".loadValues");
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			this.valid = false;

		}
	}

	public boolean save() throws Exception {
		final String msgContext = this.getClass().getName() + ".save";
		// final boolean localDebug = true;
		final boolean localDebug = ("Yes".equals(HS_Util.getAppSettingString("debug_complaints")));

		HS_database hsdb = new HS_database();
		Database eidb = hsdb.getdbAsSigner("eiRoot");
		Document doc = null;
		if ( "".equals(this.unid) ) {
			doc = eidb.createDocument();
			doc.replaceItemValue("Form", this.getForm());
			doc.replaceItemValue("DocumentID", this.documentID);
		} else {
			doc = eidb.getDocumentByUNID(this.unid);
			if ( doc == null ) throw (new Exception("Unable to access application document with unid: [" + this.unid + "]"));
			if ( doc.hasItem("DeletedBy") && !("".equals(doc.getItemValueString("DeletedBy"))) ) throw (new Exception("Document has been deleted.."));
		}
		if ( !doc.hasItem("CompSourceServer") ) doc.replaceItemValue("CompSourceServer", eidb.getServer());
		if ( !doc.hasItem("CompSourceFilepath") ) doc.replaceItemValue("CompSourceFilepath", eidb.getFilePath());
		if ( !doc.hasItem("CompSourceTitle") ) doc.replaceItemValue("CompSourceTitle", eidb.getTitle());
		if ( !doc.hasItem("CompSourceReplid") ) doc.replaceItemValue("CompSourceReplid", eidb.getReplicaID());

		String[] skipThese = new String[] { "serialversionuid", "unid", "form", "lastError", "valid", "pl_complainttypes", "xxx" };
		// String[] mapThese = new String[] { "complainantaddress", "complainantphone", "complainanteveningphone" };
		// String[] toThese = new String[] { "address", "phonenumber", "phonenumber1"};

		this.address = this.complainantAddress.getAddress();

		// PARSE AND HANDLE THE PHONE NUMBERS HERE.
		Vector<String> wph = HS_Util.parsePhoneSaver(this.getComplainantPhone());
		this.setAreaCode(wph.get(0));
		this.setPhoneNumber(wph.get(1));

		wph = HS_Util.parsePhoneSaver(this.getComplainantEveningPhone());
		this.setAreaCode_1(wph.get(0));
		this.setPhoneNumber_1(wph.get(1));
		//
		// wph = parsePhoneSaver(getOwnerPhoneInput());
		// setOwnerPhoneArea(wph.get(0));
		// setOwnerPhone(wph.get(1));
		// setOwnerPhoneExt(wph.get(2));
		//
		// wph = parsePhoneSaver(getOwnerPhoneCellInput());
		// setOwnerPhoneCellArea(wph.get(0));
		// setOwnerPhoneCell(wph.get(1));

		Field[] fields = this.getClass().getDeclaredFields();
		// First process all input dates to make sure they are converted. and update teh mapped fiedls
		for (Field wfield : fields) {
			if ( wfield.getName().toLowerCase().endsWith("datestr") ) {
				String fname = wfield.getName();
				String fvalstr = (String) wfield.get(this);
				if ( !"".equals(fvalstr) ) {
					Date fvaldate = HS_Util.cvrtStringToDate(fvalstr);
					String dfname = fname.substring(0, fname.length() - 3);
					// if ( localDebug ) HS_Util.debug("Attempting update of Date field " + dfname + ":[" + fvaldate + "] from " + fname + ":[" + fvalstr + "]", msgContext);
					try {
						for (Field dfield : fields) {
							if ( dfname.equals(dfield.getName()) ) {
								dfield.set(this, fvaldate);
							}
						}
					} catch (Exception dfe) {
						HS_Util.debug(dfe.toString(), "error", msgContext);
					}
				}
			}
		}

		for (Field field : fields) {
			if ( !HS_Util.isMember(field.getName().toLowerCase(), skipThese) && !field.getName().toLowerCase().endsWith("input") ) {
				Object obj = field.get(this);
				if ( obj == null ) obj = "";
				if ( obj instanceof AddressWithParts ) {
					((AddressWithParts) obj).save(doc, field.getName());
				} else if ( field.getName().toLowerCase().endsWith("datestr") ) {
					// not needed to save this item for now
				} else {
					doc.replaceItemValue(field.getName(), obj);
				}
			}
		}

		if ( doc.save() ) {
			this.unid = doc.getUniversalID();
			if ( localDebug ) HS_Util.debug("save successful to:[" + hsdb.getdbBang("eiRoot") + "]", "debug", msgContext);
			return true;
		} else {
			if ( localDebug ) HS_Util.debug("save failed to:[" + hsdb.getdbBang("eiRoot") + "]", "warn", msgContext);
			return false;
		}
	}

	public void setAbuseDate(Date abuseDate) {
		this.abuseDate = abuseDate;
	}

	public void setAbuseDateStr(String abuseDateStr) {
		this.abuseDateStr = abuseDateStr;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setAnimalCity(String animalCity) {
		this.animalCity = animalCity;
	}

	public void setAnimalDirections(String animalDirections) {
		this.animalDirections = animalDirections;
	}

	public void setAnimalOwner(String animalOwner) {
		this.animalOwner = animalOwner;
	}

	public void setAnimalPostalCode(String animalPostalCode) {
		this.animalPostalCode = animalPostalCode;
	}

	public void setAnimalProvince(String animalProvince) {
		this.animalProvince = animalProvince;
	}

	public void setAnimalType(String animalType) {
		this.animalType = animalType;
	}

	public void setAnonymous(boolean anonymous) {
		this.anonymous = anonymous;
	}

	public void setAnonymousInput(String anonymousInput) {
		this.anonymousInput = anonymousInput;
		// did it change??
		HS_Util.debug("new value:[" + anonymousInput + "] anonymous:[" + this.anonymous + "]", "debug", "ComplaintModel.setAnonymousInput");
		if ( "Yes".equalsIgnoreCase(anonymousInput) && this.anonymous ) return;
		if ( "".equalsIgnoreCase(anonymousInput) && !this.anonymous ) return;

		if ( "Yes".equalsIgnoreCase(anonymousInput) ) {
			this.setAnonymous(true);
			this.setComplainantName("Anonymous");
			this.setComplainantEmail("");
			this.setComplainantEveningPhone("");
			this.setComplainantPhone("");
		} else {
			this.setAnonymous(false);
			this.setComplainantName("");
			this.setComplainantEmail("");
			this.setComplainantEveningPhone("");
			this.setComplainantPhone("");
		}
		HS_Util.debug("VALUE CHANGED!:[" + anonymousInput + "] anonymous:[" + this.anonymous + "]", "debug", "ComplaintModel.setAnonymousInput");
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public void setAreaCode_1(String areaCode_1) {
		this.areaCode_1 = areaCode_1;
	}

	public void setComplainantAddress(AddressWithParts complainantAddress) {
		this.complainantAddress = complainantAddress;
	}

	public void setComplainantCity(String complainantCity) {
		this.complainantCity = complainantCity;
	}

	public void setComplainantCountry(String complainantCountry) {
		this.complainantCountry = complainantCountry;
	}

	public void setComplainantEmail(String complainantEmail) {
		this.complainantEmail = complainantEmail;
	}

	public void setComplainantEveningPhone(String complainantEveningPhone) {
		this.complainantEveningPhone = complainantEveningPhone;
	}

	public void setComplainantName(String complainantName) {
		this.complainantName = complainantName;
	}

	public void setComplainantPhone(String complainantPhone) {
		this.complainantPhone = complainantPhone;
	}

	public void setComplainantPostalCode(String complainantPostalCode) {
		this.complainantPostalCode = complainantPostalCode;
	}

	public void setComplainantProvince(String complainantProvince) {
		this.complainantProvince = complainantProvince;
	}

	public void setComplaintId(String complaintId) {
		this.complaintId = complaintId;
	}

	public void setConfirmed(String confirmed) {
		this.confirmed = confirmed;
	}

	public void setCountyIncident(String countyIncident) {
		this.countyIncident = countyIncident;
	}

	public void setCritical(String critical) {
		this.critical = critical;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setDateClosed(Date dateClosed) {
		this.dateClosed = dateClosed;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}

	public void setDbDesign(String dbDesign) {
		this.dbDesign = dbDesign;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public void setDetailsInput(UIComponent detailsInput) {
		this.detailsInput = detailsInput;
	}

	public void setDocumentID(String documentID) {
		this.documentID = documentID;
	}

	public void setFeed(String feed) {
		this.feed = feed;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public void setInterfaceId(String interfaceId) {
		this.interfaceId = interfaceId;
	}

	public void setLastError(String lastError) {
		this.lastError = lastError;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setParentRef(String parentRef) {
		this.parentRef = parentRef;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setPhoneNumber_1(String phoneNumber_1) {
		this.phoneNumber_1 = phoneNumber_1;
	}

	public void setPhysicalAddress(AddressWithParts physicalAddress) {
		this.physicalAddress = physicalAddress;
	}

	public void setPl_complaintTypes(String[] pl_complaintTypes) {
		this.pl_complaintTypes = pl_complaintTypes;
	}

	public void setPreviousIncidentDate(Date previousIncidentDate) {
		this.previousIncidentDate = previousIncidentDate;
	}

	public void setPreviousIncidentDateStr(String previousIncidentDateStr) {
		this.previousIncidentDateStr = previousIncidentDateStr;
	}

	public void setReceivedBy(String receivedBy) {
		this.receivedBy = receivedBy;
	}

	public void setReferredTo(String referredTo) {
		this.referredTo = referredTo;
	}

	public void setResultsRequested(String resultsRequested) {
		this.resultsRequested = resultsRequested;
	}

	public void setSingleMulti(String singleMulti) {
		this.singleMulti = singleMulti;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public void setTimeStart(Time timeStart) {
		this.timeStart = timeStart;
	}

	public void setTimeStr(String timeStr) {
		this.timeStr = timeStr;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setTypeMulti(String[] typeMulti) {
		this.typeMulti = typeMulti;
	}

	public void setTypeSingle(String typeSingle) {
		this.typeSingle = typeSingle;
	}

	public void setUnid(String unid) {
		this.unid = unid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public void setVerified(String verified) {
		this.verified = verified;
	}

	public void setViewDate(Date viewDate) {
		this.viewDate = viewDate;
	}

	public void setViewDescription(String viewDescription) {
		this.viewDescription = viewDescription;
	}

	public void setWater(String water) {
		this.water = water;
	}

	// public String getAnimalAddress() {
	// return animalAddress;
	// }
	//
	// public void setAnimalAddress(String animalAddress) {
	// this.animalAddress = animalAddress;
	// }

	public AddressWithParts getAnimalAddress() {
		if ( animalAddress == null ) animalAddress = new AddressWithParts();
		return animalAddress;
	}

	public void setAnimalAddress(AddressWithParts animalAddress) {
		this.animalAddress = animalAddress;
	}

}
