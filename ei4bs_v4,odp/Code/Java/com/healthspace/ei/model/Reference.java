package com.healthspace.ei.model;

import java.io.Serializable;

public class Reference implements Serializable {
	private static final long serialVersionUID = 1L;

	private String refName;
	private String refAddress;
	private String refPostalCode;
	private String refPhone;
	private String refEmail;

	public Reference() {
		this.refName = "";
		this.refAddress = "";
		this.refPostalCode = "";
		this.refPhone = "";
		this.refEmail = "";

	}

	public String getRefName() {
		return refName;
	}

	public void setRefName(String refName) {
		this.refName = refName;
	}

	public String getRefAddress() {
		return refAddress;
	}

	public void setRefAddress(String refAddress) {
		this.refAddress = refAddress;
	}

	public String getRefPostalCode() {
		return refPostalCode;
	}

	public void setRefPostalCode(String refPostalCode) {
		this.refPostalCode = refPostalCode;
	}

	public String getRefPhone() {
		return refPhone;
	}

	public void setRefPhone(String refPhone) {
		this.refPhone = refPhone;
	}

	public String getRefEmail() {
		return refEmail;
	}

	public void setRefEmail(String refEmail) {
		this.refEmail = refEmail;
	}

}
