package com.healthspace.ei.model;

import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.View;

import com.healthspace.general.HS_Util;
import com.healthspace.general.HS_database;

public class BaseModel {

	private static final long serialVersionUID = 1L;

	private String documentID;
	private String form;
	private String lastError;

	private String parentRef;
	private String unid;
	private boolean valid;

	public BaseModel() {
		this.unid = "";
		this.valid = false;
		this.lastError = "";

	}

	public String getDocumentID() {
		return documentID;
	}

	public String getForm() {
		return form;
	}

	public String getLastError() {
		return lastError;
	}

	public String getParentRef() {
		return parentRef;
	}

	public String getUnid() {
		return unid;
	}

	public boolean isValid() {
		return valid;
	}

	public boolean load(String key) {
		// this key is the unique key of the document. UNID would be
		// faster/easier.. I just kinda hate using them and seeing them in URLS
		// Session session = Factory.getSession();
		// Database db = session.getCurrentDatabase();
		HS_database hsdb = new HS_database();
		Database eirootDb = hsdb.getdb("eiroot");

		View view = eirootDb.getView("What View Should I Use??");
		Document doc = view.getFirstDocumentByKey(key);

		if ( null == doc ) {
			// document not found. DANGER
			this.lastError = "document not found for key:[" + key + "]";
			HS_Util.debug(this.lastError, "warn", HS_Util.debugCalledFrom());
			this.valid = false;
		} else {
			this.loadValues(doc);
			// this.setLoadedForm(new HS_auditForm(doc));
		}

		return this.valid;
	}

	private void loadValues(Document doc) {
		try {
			this.unid = doc.getUniversalID();
			this.documentID = HS_Util.loadValueString(doc, "documentID");
			this.form = doc.getFormName();
			this.parentRef = doc.getParentDocumentUNID();

		} catch (Exception e) {
			this.lastError = e.toString();
			HS_Util.debug(this.lastError, "error", HS_Util.debugCalledFrom() + ".loadValues");
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			this.valid = false;

		}
	}

	public void setDocumentID(String documentID) {
		this.documentID = documentID;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public void setLastError(String lastError) {
		this.lastError = lastError;
	}

	public void setParentRef(String parentRef) {
		this.parentRef = parentRef;
	}

	public void setUnid(String unid) {
		this.unid = unid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}
}
