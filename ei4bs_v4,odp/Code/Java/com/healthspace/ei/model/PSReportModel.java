package com.healthspace.ei.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import org.openntf.domino.Database;
import org.openntf.domino.DateTime;
import org.openntf.domino.Document;
import org.openntf.domino.Session;
import org.openntf.domino.View;
import org.openntf.domino.utils.Factory;

import com.healthspace.general.HS_Util;
import com.healthspace.general.HS_auditForm;
import com.healthspace.general.HS_database;

public class PSReportModel implements Serializable {

	private static final long serialVersionUID = 1L;

	// private Date dateAnalyzed;
	private Date dateCollected;
	private Date dateCreated;
	private Date dateDelivery;
	private String dateDeliveryStr;
	private Date dateReceived;
	private String dateReceivedStr;
	private Date dateReported;
	private String dateReportedStr;
	private Date viewDate;

	private String documentID;

	private String facilityMailingContact;
	private String facilityMailingAddress;
	private String facilityMailingCity;
	private String facilityMailingProvince;
	private String facilityMailingPostalCode;
	private String facilityMunicipality;

	private String facilityName;
	private String facilityType;
	private String collectedBy;
	private String businessType;

	private String form;

	private String prod;
	private String grade;
	private String brand;
	private String blending;
	private String tnc;
	private String number;
	private String oxy;
	private String akicn;
	private String gallons;
	private String delivery;
	private String ppg;

	private String rom;
	private String mon;
	private String aki;
	private String ci;
	private String cn;
	private String s;
	private String visual;

	private String id;
	private String inspectionType;
	private String labComments;
	private String labSampleNo;
	private String sample;
	private String samplerComments;
	private String siteAddress;
	private String siteName;
	private String source;
	private String supplier;
	private String tncid;
	private String sealsIntact;
	private String viewDescription;

	private String parentRef;

	private Vector<PSReportDetail> rptDetail;

	private String unid;
	private boolean valid;
	private String lastError;

	private HS_auditForm loadedForm;

	public HS_auditForm getLoadedForm() {
		return loadedForm;
	}

	public void setLoadedForm(HS_auditForm loadedForm) {
		this.loadedForm = loadedForm;
	}

	public PSReportModel() {
		this.unid = "";
		this.valid = false;
		this.lastError = "";

	}

	public void create() {
		Session session = Factory.getSession(); // this will be slightly
		// different if not using the
		// OpenNTF Domino API
		this.setDocumentID(session.getUnique());
	}

	public boolean load(String key) {

		// this key is the unique key of the document. UNID would be
		// faster/easier.. I just kinda hate using them and seeing them in URLS
		// Session session = Factory.getSession();
		// Database db = session.getCurrentDatabase();
		HS_database hsdb = new HS_database();
		Database eirootDb = hsdb.getdb("eiroot");

		View view = eirootDb.getView("LookupPetroleumSampleReports");
		Document doc = view.getFirstDocumentByKey(key); // This is deprecated
		// because the API
		// prefers to use
		// getFirstDocumentByKey

		if ( null == doc ) {
			// document not found. DANGER
			this.lastError = "document not found for key:[" + key + "]";
			HS_Util.debug(this.lastError, "warn", HS_Util.debugCalledFrom());
			this.valid = false;
		} else {
			this.loadValues(doc);
			this.setLoadedForm(new HS_auditForm(doc));
		}

		return this.valid;

	}

	private void loadValues(Document doc) {
		try {
			this.unid = doc.getUniversalID();
			this.documentID = HS_Util.loadValueString(doc, "documentID");

			// this.dateAnalyzed = HS_Util.loadValueDate(doc, "DateAnalyzed");
			this.dateCollected = HS_Util.loadValueDate(doc, "DateCollected");
			this.dateCreated = HS_Util.loadValueDate(doc, "DateCreated");
			this.dateDelivery = (doc.hasItem("Delivery")) ? HS_Util.loadValueDate(doc, "Delivery") : HS_Util.loadValueDate(doc, "DateDelivery");
			this.dateDeliveryStr = (this.dateDelivery == null) ? "" : HS_Util.formatDate(this.dateDelivery, this.getStandardDateMask());
			this.dateReceived = HS_Util.loadValueDate(doc, "DateReceived");
			this.dateReceivedStr = (this.dateReceived == null) ? "" : HS_Util.formatDate(this.dateReceived, this.getStandardDateMask());
			this.dateReported = HS_Util.loadValueDate(doc, "DateReported");
			this.dateReportedStr = (this.dateReported == null) ? "" : HS_Util.formatDate(this.dateReported, this.getStandardDateMask());
			this.viewDate = HS_Util.loadValueDate(doc, "ViewDate");

			this.facilityName = HS_Util.loadValueString(doc, "FacilityName");
			this.facilityMailingAddress = HS_Util.loadValueString(doc, "SiteAddress");
			this.facilityMailingCity = HS_Util.loadValueString(doc, "SiteCity");
			this.facilityMailingContact = HS_Util.loadValueString(doc, "FacilityMailingContact");
			this.facilityMailingPostalCode = HS_Util.loadValueString(doc, "SiteZip");
			this.facilityMunicipality = HS_Util.loadValueString(doc, "SiteCounty");
			if ( "".equals(this.facilityMunicipality) ) {
				this.facilityMunicipality = this.getFacilityMunicipalityFromFacility(doc);
			}
			this.facilityMailingProvince = HS_Util.loadValueString(doc, "FacilityMailingProvince");

			this.businessType = HS_Util.loadValueString(doc, "BusinessType");

			this.facilityType = HS_Util.loadValueString(doc, "FacilityType");
			this.collectedBy = HS_Util.loadValueString(doc, "CollectedBy");

			this.sample = HS_Util.loadValueString(doc, "Sample");
			this.prod = HS_Util.loadValueString(doc, "Prod");
			this.grade = HS_Util.loadValueString(doc, "Grade");
			this.brand = HS_Util.loadValueString(doc, "Brand");
			this.blending = HS_Util.loadValueString(doc, "Blending");
			this.tnc = HS_Util.loadValueString(doc, "TNC");
			this.number = HS_Util.loadValueString(doc, "Number");
			this.oxy = HS_Util.loadValueString(doc, "Oxy");
			this.akicn = HS_Util.loadValueString(doc, "AKICN");
			this.gallons = HS_Util.loadValueString(doc, "Gallons");
			this.delivery = HS_Util.loadValueString(doc, "Delivery");
			this.ppg = HS_Util.loadValueString(doc, "PPG");

			this.rom = HS_Util.loadValueString(doc, "ROM");
			this.mon = HS_Util.loadValueString(doc, "MON");
			this.aki = HS_Util.loadValueString(doc, "AKI");
			this.ci = HS_Util.loadValueString(doc, "CI");
			this.cn = HS_Util.loadValueString(doc, "CN");
			this.s = HS_Util.loadValueString(doc, "S");
			this.visual = HS_Util.loadValueString(doc, "Visual");

			this.id = HS_Util.loadValueString(doc, "ID");
			this.inspectionType = HS_Util.loadValueString(doc, "InspectionType");
			this.labComments = HS_Util.loadValueString(doc, "LabComments");
			this.labSampleNo = HS_Util.loadValueString(doc, "LabSampleNo");
			this.samplerComments = HS_Util.loadValueString(doc, "SamplerComments");
			this.siteAddress = HS_Util.loadValueString(doc, "SiteAddress");
			this.siteName = HS_Util.loadValueString(doc, "SiteName");
			this.source = HS_Util.loadValueString(doc, "Source");
			this.supplier = HS_Util.loadValueString(doc, "Supplier");

			this.tncid = HS_Util.loadValueString(doc, "TNCID");
			this.sealsIntact = HS_Util.loadValueString(doc, "SealsIntact");
			this.viewDescription = HS_Util.loadValueString(doc, "ViewDescription");

			this.form = doc.getFormName();
			this.parentRef = doc.getParentDocumentUNID();

			this.valid = this.loadDetail(doc);
		} catch (Exception e) {
			this.lastError = e.toString();
			HS_Util.debug(this.lastError, "error", HS_Util.debugCalledFrom() + ".loadValues");
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			this.valid = false;

		}
	}

	@SuppressWarnings("unchecked")
	private boolean loadDetail(Document doc) {
		final boolean localDebug = HS_Util.isDebugServer();
		final String dbarLoc = HS_Util.debugCalledFrom() + ".loadDetails";

		boolean fresult = false;

		this.rptDetail = new Vector<PSReportDetail>();

		try {
			// ArrayList<String> analytes = new
			// ArrayList<String>((Vector<String>) (Vector<?>)
			// (doc.getItemValue("Analytes")));
			String fname = (doc.hasItem("Test")) ? "Test" : "Analytes";
			ArrayList<String> test = new ArrayList<String>((Vector<String>) (Vector<?>) (doc.getItemValue(fname)));
			int vsize = test.size();

			fname = "Units";
			ArrayList<String> units = loadDetailItem(doc, fname, vsize);

			fname = (doc.hasItem("Method")) ? "Method" : "ASTMethod";
			ArrayList<String> method = loadDetailItem(doc, fname, vsize);

			fname = "Prefix";
			ArrayList<String> prefix = loadDetailItem(doc, fname, vsize);

			fname = (doc.hasItem("Result")) ? "Result" : "Results";
			ArrayList<String> result = loadDetailItem(doc, fname, vsize);

			fname = (doc.hasItem("Min")) ? "Min" : "MinResult";
			ArrayList<String> minResult = loadDetailItem(doc, fname, vsize);

			fname = (doc.hasItem("Max")) ? "Max" : "MaxResult";
			ArrayList<String> maxResult = loadDetailItem(doc, fname, vsize);

			ArrayList<String> compliance = loadDetailItem(doc, "Compliance", vsize);

			if ( localDebug ) HS_Util.debug("currViolCode has [" + vsize + "] elements.", "debug", dbarLoc);

			for (int i = 0; i < vsize; i++) {
				PSReportDetail psrd = new PSReportDetail();
				psrd.setTest(test.get(i));
				psrd.setUnits(units.get(i));
				psrd.setMethod(method.get(i));
				psrd.setPrefix(prefix.get(i));
				psrd.setResult(result.get(i));
				psrd.setMin(minResult.get(i));
				psrd.setMax(maxResult.get(i));
				psrd.setCompliance(compliance.get(i));

				this.rptDetail.add(psrd);
			}
			if ( localDebug ) HS_Util.debug("loaded [" + this.rptDetail.size() + "] elements.", "debug", dbarLoc);
			fresult = true;
		} catch (Exception e) {
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			HS_Util.debug(e.toString(), "error", HS_Util.debugCalledFrom() + ".loadDetail");
			fresult = false;
		}
		return fresult;
	}

	@SuppressWarnings("unchecked")
	private static ArrayList<String> loadDetailItem(Document doc, String iname, int vsize) {
		final boolean localDebug = false;
		final String dbarLoc = HS_Util.debugCalledFrom() + ".loadDetailItem";
		Vector<String> result = null;
		int actr = 0;
		try {
			result = (Vector<String>) (Vector<?>) (doc.getItemValue(iname));
			while (result.size() < vsize) {
				result.add("");
				actr++;
			}
			if ( localDebug && actr > 0 ) HS_Util.debug("added [" + actr + "] elements to [" + iname + "]", "debug", dbarLoc);
		} catch (Exception e) {
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			HS_Util.debug(e.toString(), "error", dbarLoc);
		}
		return new ArrayList<String>(result);
	}

	public boolean save() throws Exception {
		// this key is the unique key of the document. UNID would be
		// faster/easier.. I just kinda hate using them and seeing them in URLS
		// Session session = Factory.getSession();
		// Database db = session.getCurrentDatabase();
		String dbarLoc = "PSReportModel.save";
		String msg = "";
		HS_database hsdb = new HS_database();
		Database eirootDb = hsdb.getdb("eiroot");

		// View view = eirootDb.getView("LookupPetroleumSampleReports");
		Document doc = eirootDb.getDocumentByUNID(this.unid);
		if ( null == doc ) {
			this.lastError = "document not found for unid:[" + unid + "]";
			throw new Exception("PSReportModel.save error: " + this.lastError);
		}

		if ( this.getLoadedForm() == null ) {
			msg = "LoadedForm is null on document update save!";
			HS_Util.debug(msg, "error", dbarLoc);
			this.lastError = msg + " " + dbarLoc;
			return false;
		} else {
			if ( this.getLoadedForm().checkChanges(doc) ) {
				msg = "Changes detected since document loaded!";
				HS_Util.debug(msg, "error", dbarLoc);
				this.lastError = msg + " " + dbarLoc;
				Vector<String> cfields = this.getLoadedForm().getChangedFields();
				msg += "\nChanges detected to fields:" + cfields.toString();
				this.lastError = msg;
				return false;
			}
		}

		boolean changesDetected = false;
		if ( this.saveValues(doc) ) {
			try {
				changesDetected = loadedForm.checkChanges(doc);
			} catch (Exception e) {
				msg = "Exception during checkChanges: " + e.toString();
				HS_Util.debug(msg, "error", dbarLoc);
				this.lastError = msg + " " + dbarLoc;
				if ( HS_Util.isDebugServer() ) e.printStackTrace();
				return false;
			}
			if ( !changesDetected ) {
				this.lastError = "No changes detected.";
				return false;
			} else if ( doc.save() ) {
				String auditEvent = "Document Saved - From web";
				this.loadedForm.writeAuditEvent(doc, auditEvent, this.loadedForm.getChangedFields());
				return true;
			} else {
				this.lastError = "Document save failed.";
				return false;
			}
		} else
			return false;
	}

	private boolean saveValues(Document doc) {
		try {
			int fsize = this.rptDetail.size();
			// ArrayList<String> test = new ArrayList<String>(fsize);
			// ArrayList<String> units = new ArrayList<String>(fsize);
			// ArrayList<String> method = new ArrayList<String>(fsize);
			ArrayList<String> prefix = new ArrayList<String>(fsize);
			ArrayList<String> result = new ArrayList<String>(fsize);
			ArrayList<String> minResult = new ArrayList<String>(fsize);
			ArrayList<String> maxResult = new ArrayList<String>(fsize);
			ArrayList<String> compliance = new ArrayList<String>(fsize);
			String comply = "";
			String rslt = "";
			String compSaveValue = " ";
			for (int idx = 0; idx < fsize; idx++) {
				prefix.add(this.rptDetail.get(idx).getPrefix());
				rslt = this.rptDetail.get(idx).getResult();
				result.add(("Select".equals(rslt)) ? " " : rslt);
				minResult.add(this.rptDetail.get(idx).getMin());
				maxResult.add(this.rptDetail.get(idx).getMax());
				comply = this.rptDetail.get(idx).getCompliance();
				compSaveValue = ("Select".equalsIgnoreCase(comply)) ? " " : comply;
				// HS_Util.debug("for idx:[" + idx + "] comply:[" + comply +
				// "] save value:[" + compSaveValue + "]", "debug",
				// "PSReportModel.saveValues");
				compliance.add(compSaveValue);
			}

			doc.replaceItemValue("Prefix", prefix);
			doc.replaceItemValue("Result", result);
			doc.replaceItemValue("Results", result);
			doc.replaceItemValue("Min", minResult);
			doc.replaceItemValue("MinResult", minResult);
			doc.replaceItemValue("Max", maxResult);
			doc.replaceItemValue("MaxResult", maxResult);
			doc.replaceItemValue("Min", minResult);
			doc.replaceItemValue("Compliance", compliance);

			// this.dateAnalyzed = new Date();
			// DateTime ndt = HS_Util.getSession().createDateTime(this.dateAnalyzed);
			// ndt.setAnyTime();
			// updateItem(doc, "DateAnalyzed", ndt);

			if ( !doc.hasItem("SiteCounty") ) doc.replaceItemValue("SiteCounty", this.facilityMunicipality);
			DateTime wdt = null;
			String fldName = "";

			fldName = (doc.hasItem("Delivery")) ? "Delivery" : "DateDelivery";
			if ( "".equals(this.dateDeliveryStr) ) {
				doc.replaceItemValue(fldName, "");
			} else {
				try {
					wdt = HS_Util.getSession().createDateTime(this.dateDeliveryStr);
					wdt.setAnyTime();
					updateItem(doc, fldName, wdt);
				} catch (Exception e1) {
				}
			}

			fldName = (doc.hasItem("Received")) ? "Received" : "DateReceived";
			if ( "".equals(this.dateReceivedStr) ) {
				doc.replaceItemValue(fldName, "");
			} else {
				try {
					wdt = HS_Util.getSession().createDateTime(this.dateReceivedStr);
					wdt.setAnyTime();
					updateItem(doc, fldName, wdt);
				} catch (Exception e1) {
				}
			}

			fldName = (doc.hasItem("Reported")) ? "Reported" : "DateReported";
			if ( "".equals(this.dateReportedStr) ) {
				doc.replaceItemValue("DateReported", "");
			} else {
				try {
					wdt = HS_Util.getSession().createDateTime(this.dateReportedStr);
					wdt.setAnyTime();
					updateItem(doc, fldName, wdt);
				} catch (Exception e1) {
				}
			}

			// TODO Anything else to update? Dates, etc.
			doc.replaceItemValue("SealsIntact", this.sealsIntact);

			return true;
		} catch (Exception e) {
			this.lastError = e.toString();
			HS_Util.debug(this.lastError, "error", "PSReportModel.saveValues");
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}
		return false;
	}

	private boolean updateItem(Document doc, String itemName, DateTime idate) {
		final boolean localDebug = false;
		final String dbarLoc = "PSReportModel.updateItem - date";
		boolean result = false;
		try {
			if ( idate == null ) {
				doc.replaceItemValue(itemName, "");
				result = true;
			} else {
				if ( idate.isAnyTime() ) {
					if ( localDebug ) HS_Util.debug("Time zone is: [" + idate.getTimeZone() + "] idate is:[" + idate.toString() + "]", "debug", dbarLoc);
				}
				doc.replaceItemValue(itemName, idate);
				if ( localDebug ) HS_Util.debug("item:[" + itemName + "] updated OK to:[" + idate.toString() + "]", "debug", dbarLoc);
				result = true;
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString() + " on item:[" + itemName + "]", "error", dbarLoc);
		}

		return result;
	}

	// public Date getDateAnalyzed() {
	// return dateAnalyzed;
	// }

	public Date getDateCollected() {
		return dateCollected;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public Date getDateDelivery() {
		if ( dateDelivery == null ) {
			dateDelivery = new Date();
		}
		return dateDelivery;
	}

	public Date getDateReceived() {
		return dateReceived;
	}

	public Date getDateReported() {
		return dateReported;
	}

	public Date getViewDate() {
		return viewDate;
	}

	public String getDocumentID() {
		return documentID;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public String getForm() {
		return form;
	}

	public String getGallons() {
		return gallons;
	}

	public String getGrade() {
		return grade;
	}

	public String getId() {
		return id;
	}

	public String getInspectionType() {
		return inspectionType;
	}

	public String getLabComments() {
		return labComments;
	}

	public String getLabSampleNo() {
		return labSampleNo;
	}

	public String getPpg() {
		return ppg;
	}

	public String getProd() {
		return prod;
	}

	public String getSample() {
		return sample;
	}

	public String getSamplerComments() {
		return samplerComments;
	}

	public String getSiteAddress() {
		return siteAddress;
	}

	public String getSiteName() {
		return siteName;
	}

	public String getSource() {
		return source;
	}

	public String getSupplier() {
		return supplier;
	}

	public String getTnc() {
		return tnc;
	}

	public String getTncid() {
		return tncid;
	}

	public String getViewDescription() {
		return viewDescription;
	}

	public String getParentRef() {
		return parentRef;
	}

	public Vector<PSReportDetail> getRptDetail() {
		return rptDetail;
	}

	public String getUnid() {
		return unid;
	}

	public boolean isValid() {
		return valid;
	}

	public String getLastError() {
		return lastError;
	}

	// public void setDateAnalyzed(Date dateAnalyzed) {
	// this.dateAnalyzed = dateAnalyzed;
	// }

	public void setDateCollected(Date dateCollected) {
		this.dateCollected = dateCollected;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public void setDateDelivery(Date dateDelivery) {
		this.dateDelivery = dateDelivery;
	}

	public void setDateReceived(Date dateReceived) {
		this.dateReceived = dateReceived;
	}

	public void setDateReported(Date dateReported) {
		this.dateReported = dateReported;
	}

	public void setViewDate(Date viewDate) {
		this.viewDate = viewDate;
	}

	public void setDocumentID(String documentID) {
		this.documentID = documentID;
	}

	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public void setGallons(String gallons) {
		this.gallons = gallons;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setInspectionType(String inspectionType) {
		this.inspectionType = inspectionType;
	}

	public void setLabComments(String labComments) {
		this.labComments = labComments;
	}

	public void setLabSampleNo(String labSampleNo) {
		this.labSampleNo = labSampleNo;
	}

	public void setPpg(String ppg) {
		this.ppg = ppg;
	}

	public void setProd(String prod) {
		this.prod = prod;
	}

	public void setSample(String sample) {
		this.sample = sample;
	}

	public void setSamplerComments(String samplerComments) {
		this.samplerComments = samplerComments;
	}

	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public void setTnc(String tnc) {
		this.tnc = tnc;
	}

	public void setTncid(String tncid) {
		this.tncid = tncid;
	}

	public void setViewDescription(String viewDescription) {
		this.viewDescription = viewDescription;
	}

	public void setParentRef(String parentRef) {
		this.parentRef = parentRef;
	}

	public void setRptDetail(Vector<PSReportDetail> rptDetail) {
		this.rptDetail = rptDetail;
	}

	public String getFacilityMailingContact() {
		return facilityMailingContact;
	}

	public String getFacilityMailingAddress() {
		return facilityMailingAddress;
	}

	public String getFacilityMailingCity() {
		return facilityMailingCity;
	}

	public String getFacilityMailingProvince() {
		return facilityMailingProvince;
	}

	public String getFacilityMailingPostalCode() {
		return facilityMailingPostalCode;
	}

	public String getFacilityMunicipality() {
		return facilityMunicipality;
	}

	private String getFacilityMunicipalityFromFacility(Document doc) {
		String result = "";
		try {
			Database eidb = doc.getParentDatabase();
			Document fdoc = eidb.getDocumentByUNID(doc.getParentDocumentUNID());
			result = fdoc.getItemValueString("PhysicalMunicipality");
			if ( "".equals(result) ) result = "not found";
		} catch (Exception e) {
			result = e.toString();
		}
		return result;
	}

	public void setFacilityMunicipality(String facilityMunicipality) {
		this.facilityMunicipality = facilityMunicipality;
	}

	public void setFacilityMailingContact(String facilityMailingContact) {
		this.facilityMailingContact = facilityMailingContact;
	}

	public void setFacilityMailingAddress(String facilityMailingAddress) {
		this.facilityMailingAddress = facilityMailingAddress;
	}

	public void setFacilityMailingCity(String facilityMailingCity) {
		this.facilityMailingCity = facilityMailingCity;
	}

	public void setFacilityMailingProvince(String facilityMailingProvince) {
		this.facilityMailingProvince = facilityMailingProvince;
	}

	public void setFacilityMailingPostalCode(String facilityMailingPostalCode) {
		this.facilityMailingPostalCode = facilityMailingPostalCode;
	}

	public void setUnid(String unid) {
		this.unid = unid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public void setLastError(String lastError) {
		this.lastError = lastError;
	}

	public String getFacilityType() {
		return facilityType;
	}

	public String getCollectedBy() {
		return collectedBy;
	}

	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	public void setCollectedBy(String collectedBy) {
		this.collectedBy = collectedBy;
	}

	public String getBrand() {
		return brand;
	}

	public String getBlending() {
		return blending;
	}

	public String getNumber() {
		return number;
	}

	public String getOxy() {
		return oxy;
	}

	public String getAkicn() {
		return akicn;
	}

	public String getDelivery() {
		return delivery;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public void setBlending(String blending) {
		this.blending = blending;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public void setOxy(String oxy) {
		this.oxy = oxy;
	}

	public void setAkicn(String akicn) {
		this.akicn = akicn;
	}

	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}

	public String getRom() {
		return rom;
	}

	public String getMon() {
		return mon;
	}

	public String getAki() {
		return aki;
	}

	public String getCi() {
		return ci;
	}

	public String getCn() {
		return cn;
	}

	public String getS() {
		return s;
	}

	public String getVisual() {
		return visual;
	}

	public void setRom(String rom) {
		this.rom = rom;
	}

	public void setMon(String mon) {
		this.mon = mon;
	}

	public void setAki(String aki) {
		this.aki = aki;
	}

	public void setCi(String ci) {
		this.ci = ci;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public void setS(String s) {
		this.s = s;
	}

	public void setVisual(String visual) {
		this.visual = visual;
	}

	private String standardDateMask = "";

	public String getDateDeliveryStr() {
		return dateDeliveryStr;
	}

	public void setDateDeliveryStr(String dateDeliveryStr) {
		this.dateDeliveryStr = dateDeliveryStr;
		if ( !"".equals(dateDeliveryStr) ) {
			DateTime wdt = HS_Util.getSession().createDateTime(dateDeliveryStr);
			this.dateDelivery = wdt.toJavaDate();
		}
	}

	public String getDateReceivedStr() {
		return dateReceivedStr;
	}

	public void setDateReceivedStr(String dateReceivedStr) {
		this.dateReceivedStr = dateReceivedStr;
		if ( !"".equals(dateReceivedStr) ) {
			DateTime wdt = HS_Util.getSession().createDateTime(dateReceivedStr);
			this.dateReceived = wdt.toJavaDate();
		}
	}

	public String getDateReportedStr() {
		return dateReportedStr;
	}

	public void setDateReportedStr(String dateReportedStr) {
		this.dateReportedStr = dateReportedStr;
		if ( !"".equals(dateReportedStr) ) {
			DateTime wdt = HS_Util.getSession().createDateTime(dateReportedStr);
			this.dateReported = wdt.toJavaDate();
		}
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public String getSealsIntact() {
		if ( sealsIntact == null ) sealsIntact = "";
		return sealsIntact;
	}

	public void setSealsIntact(String sealsIntact) {
		this.sealsIntact = sealsIntact;
	}

	public String getStandardDateMask() {
		if ( "".equals(standardDateMask) ) {
			Vector<String> idf = HS_Util.getAppSettingsValues("inspDateFormat");
			standardDateMask = (idf == null) ? "d-MMM-yyyy" : (idf.size() == 0) ? "d-MMM-yyyy" : idf.get(0);
		}
		return standardDateMask;
	}

	public void setStandardDateMask(String standardDateMask) {
		this.standardDateMask = standardDateMask;
	}
}
