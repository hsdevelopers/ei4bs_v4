package com.healthspace.ei.model;

import java.io.Serializable;

public class PSReportDetail implements Serializable {

	private static final long serialVersionUID = 1L;

	// private String analytes;
	// private String astMethod;

	private String test;
	private String units;
	private String method;
	private String prefix;
	private String result;
	// private String results;
	private String min;
	// private String minResult;
	private String max;
	// private String maxResult;
	private String compliance;

	public PSReportDetail() {

	}

	public String getCompliance() {
		if ("".equals(compliance)) compliance = "Select";
		return compliance;
	}

	public String getMax() {
		return max;
	}

	public String getMethod() {
		return method;
	}

	public String getMin() {
		return min;
	}

	public String getResult() {
		if ("".equals(result)) {
			if (this.test.contains("Workmanship")) result = "Select";
		}
		return result;
	}

	public String getTest() {
		return test;
	}

	public String getUnits() {
		return units;
	}

	public void setCompliance(String compliance) {
		this.compliance = compliance;
	}

	public void setMax(String max) {
		this.max = max;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public void setMin(String min) {
		this.min = min;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public void setTest(String test) {
		this.test = test;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	public String getPrefix() {
		if ("".equals(prefix)) prefix = "=";
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

}
