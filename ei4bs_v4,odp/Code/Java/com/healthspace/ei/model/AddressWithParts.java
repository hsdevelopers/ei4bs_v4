package com.healthspace.ei.model;

import org.openntf.domino.Document;

import com.healthspace.general.HS_Util;

public class AddressWithParts {

	private String	address;
	private String	addressParts;
	private String	building;
	private String	direction;
	private String	streetName;
	private String	streetType;
	private String	streetSuffix;

	private String	fldPrefix;

	public AddressWithParts() {
		address = "";
		addressParts = "";
		building = "";
		direction = "";
		streetName = "";
		streetType = "";
		streetSuffix = "";
	}

	public String getAddress() {
		return address;
	}

	public String getAddressParts() {
		if ( "".equals(addressParts) ) {
			addressParts = "";
			addressParts += "bn:'" + building + "'";
			addressParts += ", dir:'" + direction + "'";
			addressParts += ", sname:'" + streetName + "'";
			addressParts += ", stype:'" + streetType + "'";
			addressParts += ", suffix:'" + streetSuffix + "'";
		}
		return addressParts;
	}

	public String getBuilding() {
		return building;
	}

	public String getDirection() {
		return direction;
	}

	public String getStreetName() {
		return streetName;
	}

	public String getStreetSuffix() {
		return streetSuffix;
	}

	public String getStreetType() {
		return streetType;
	}

	public void load(Document doc, String newFldPrefix) {
		final String msgContext = this.getClass().getName() + ".load";
		// final boolean localDebug = false;
		final boolean localDebug = ("Yes".equals(HS_Util.getAppSettingString("debug_permitApp")));

		if ( "".equals(fldPrefix) ) fldPrefix = newFldPrefix;

		// address = doc.getItemValueString(fldPrefix + "Address");
		building = doc.getItemValueString(fldPrefix + "Building");
		direction = doc.getItemValueString(fldPrefix + "Direction");
		streetName = doc.getItemValueString(fldPrefix + "StreetName");
		streetSuffix = doc.getItemValueString(fldPrefix + "StreetSuffix");
		streetType = doc.getItemValueString(fldPrefix + "StreetType");
		address = this.recompute();
		if ( localDebug )
			HS_Util.debug("address:[" + address + "] building:[" + building + "] direction:[" + direction + "] streetName:[" + streetName + "] suffix:[" + streetSuffix + "] type:[" + streetType
									+ "] fldPrefix:[" + fldPrefix + "]", "debug", msgContext);
		// x:[" + x + "]
	}

	private String recompute() {
		String result = "";
		result += ("".equals(building)) ? "" : building + " ";
		result += ("".equals(direction)) ? "" : direction + " ";
		result += ("".equals(streetName)) ? "" : streetName;
		result += ("".equals(streetType)) ? "" : " " + streetType;
		result += ("".equals(streetType)) ? "" : " " + streetSuffix;

		return result;
	}

	public void save(Document doc, String fldName) {
		final String msgContext = this.getClass().getName() + ".save";
		// final boolean localDebug = false;
		final boolean localDebug = ("Yes".equals(HS_Util.getAppSettingString("debug_permitApp")));

		if ( !"".equals(fldName) ) fldPrefix = fldName.substring(0, fldName.lastIndexOf("Address"));
		if ( "".equals(fldPrefix) ) fldPrefix = "Physical";

		this.parse();

		doc.replaceItemValue(fldPrefix + "Building", building);
		doc.replaceItemValue(fldPrefix + "Direction", direction);
		doc.replaceItemValue(fldPrefix + "StreetName", streetName);
		doc.replaceItemValue(fldPrefix + "StreetType", streetType);
		doc.replaceItemValue(fldPrefix + "StreetSuffix", streetSuffix);
		doc.replaceItemValue(fldPrefix + "Address", address);
		if ( localDebug )
			HS_Util.debug("parts:[" + addressParts + "] building:[" + building + "] direction:[" + direction + "] streetName:[" + streetName + "] suffix:[" + streetSuffix + "] type:[" + streetType
									+ "] fldPrefix:[" + fldPrefix + "]", "debug", msgContext);
	}

	public void parse() {
		String[] parts = getAddressParts().split(", ");
		for (String part : parts) {
			String[] ps = part.split(":");
			String wval = ps[1].substring(1, (ps[1].lastIndexOf("'")));
			if ( "bn".equals(ps[0]) ) building = wval;
			if ( "dir".equals(ps[0]) ) direction = wval;
			if ( "sname".equals(ps[0]) ) streetName = wval;
			if ( "stype".equals(ps[0]) ) streetType = wval;
			if ( "suffix".equals(ps[0]) ) streetSuffix = wval;
		}
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setAddressParts(String addressParts) {
		this.addressParts = addressParts;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public void setStreetSuffix(String streetSuffix) {
		this.streetSuffix = streetSuffix;
	}

	public void setStreetType(String streetType) {
		this.streetType = streetType;
	}

	public String getFldPrefix() {
		return fldPrefix;
	}

	public void setFldPrefix(String fldPrefix) {
		this.fldPrefix = fldPrefix;
	}

}
