package com.healthspace.ei.model;

import java.util.Date;
import java.util.Vector;

import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.View;
import org.openntf.domino.ViewNavigator;

import com.healthspace.general.HS_Util;
import com.healthspace.general.HS_database;
import com.healthspace.tools.JSFUtil;
import com.ibm.xsp.designer.context.ServletXSPContext;
import com.ibm.xsp.designer.context.XSPUrl;

public class PayInvoiceModel {

	private static final long serialVersionUID = 1L;

	// doc specific data
	private double amount;
	private String businessAddress;
	private String businessCity;

	private String businessContact;
	private String businessEmail;
	private String businessName;

	private String businessNumber;
	private String businessPhone;
	private String businessPostalCode;
	private String businessProvince;
	private Date date;
	private String documentID;
	private Date endDate;
	private String form;
	private String invoiceNumber;
	private String lastError;
	private String parentRef;
	private String unid;
	private boolean valid;
	private String viewDescription;

	private Vector<Double> li_amounts;
	private Vector<String> li_addresses;
	private Vector<String> li_facilities;
	private Vector<String> li_fees;
	private Vector<String> li_type;
	private Vector<String> lineItems;

	private String returnUrl = "";
	private String cancelUrl = "";
	private String postUrl = "";
	private String postBackUrl = "";
	private String merchantCode = "";
	private String settleCode = "";
	private String paymentMethod = "";

	private String paymentDocumentID;

	public PayInvoiceModel() {
		this.unid = "";
		this.valid = false;
		this.lastError = "";
	}

	public double getAmount() {
		return amount;
	}

	private String getBaseUrl() {
		// HS_Util.debug("JSFUtil.getDbUrl(): " + JSFUtil.getDbUrl(), "debug", this.getClass().getName() + ".getBaseUrl");
		// String result = HS_Util.getSession().getCurrentDatabase().getHttpURL();
		// result = result.substring(0, result.length() - "?OpenDatabase".length());
		// return JSFUtil.getDbUrl();
		ServletXSPContext thisContext = (ServletXSPContext) JSFUtil.getVariableValue("context");
		XSPUrl thisUrl = thisContext.getUrl();
		String protocol = (thisUrl.toString().contains("https:")) ? "https" : "http";
		String permaLink = protocol + "://" + thisUrl.getHost() + thisUrl.getPath().substring(0, thisUrl.getPath().lastIndexOf("/"));
		return permaLink;
	}

	public String getBusinessAddress() {
		return businessAddress;
	}

	public String getBusinessCity() {
		return businessCity;
	}

	public String getBusinessContact() {
		return businessContact;
	}

	public String getBusinessEmail() {
		return businessEmail;
	}

	public String getBusinessName() {
		return businessName;
	}

	public String getBusinessNumber() {
		return businessNumber;
	}

	public String getBusinessPhone() {
		return businessPhone;
	}

	public String getBusinessPostalCode() {
		return businessPostalCode;
	}

	public String getBusinessProvince() {
		return businessProvince;
	}

	public String getCancelUrl() {
		if ( "".equals(cancelUrl) ) {
			cancelUrl = getBaseUrl();
			cancelUrl += "/" + HS_Util.getAppSettingString("AppsAndPmts_CancelPage");
		}
		return cancelUrl;
	}

	public Date getDate() {
		return date;
	}

	public String getDocumentID() {
		return documentID;
	}

	public Date getEndDate() {
		return endDate;
	}

	public String getForm() {
		return form;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public String getLastError() {
		return lastError;
	}

	public Vector<String> getLi_addresses() {
		return li_addresses;
	}

	public Vector<Double> getLi_amounts() {
		return li_amounts;
	}

	public Vector<String> getLi_facilities() {
		return li_facilities;
	}

	public Vector<String> getLi_fees() {
		return li_fees;
	}

	public Vector<String> getLi_type() {
		return li_type;
	}

	public Vector<String> getLineItems() {
		return lineItems;
	}

	public String getMerchantCode() {
		if ( "".equals(merchantCode) ) {
			merchantCode = HS_Util.getKeywordValues("AppsAndPmts_MerchantCode").get(0);
		}
		return merchantCode;
	}

	public String getParentRef() {
		return parentRef;
	}

	public String getPaymentDocumentID() {
		if ( (paymentDocumentID == null) || ("".equals(paymentDocumentID)) ) {
			paymentDocumentID = HS_Util.getAppSettingString("AppsAndPmts_Vendor");
			paymentDocumentID += "-" + HS_Util.getSession().evaluate("@Unique").get(0);
		}
		return paymentDocumentID;
	}

	public String getPaymentMethod() {
		if ( "".equals(paymentMethod) ) {
			paymentMethod = HS_Util.getAppSettingString("AppsAndPmts_Method");
		}
		return paymentMethod;
	}

	public String getPostBackUrl() {
		if ( "".equals(postBackUrl) ) {
			postBackUrl = getBaseUrl();
			postBackUrl += "/" + HS_Util.getAppSettingString("AppsAndPmts_PostbackPage");
		}
		return postBackUrl;
	}

	public String getPostUrl() {
		if ( "".equals(postUrl) ) {
			postUrl = HS_Util.getAppSettingString("AppsAndPmts_PostURL");
		}
		return postUrl;
	}

	public String getReturnUrl() {
		if ( "".equals(returnUrl) ) {
			returnUrl = getBaseUrl();
			returnUrl += "/" + HS_Util.getAppSettingString("AppsAndPmts_ReturnPage");
		}
		return returnUrl;
	}

	public String getSettleCode() {
		if ( "".equals(settleCode) ) {
			settleCode = HS_Util.getKeywordValues("AppsAndPmts_SettleCode").get(0);
		}
		return settleCode;
	}

	public String getUnid() {
		return unid;
	}

	public String getViewDescription() {
		return viewDescription;
	}

	public boolean isValid() {
		return valid;
	}

	public boolean load(String key) {
		// this key is the unique key of the document. UNID would be
		// faster/easier.. I just kinda hate using them and seeing them in URLS
		// Session session = Factory.getSession();
		// Database db = session.getCurrentDatabase();
		final String viewName = "Invoices";
		final String cfgViewName = "DocumentIDLookup";
		try {
			HS_database hsdb = new HS_database();
			Database eirootDb = hsdb.getdb("eiroot");
			Database cfgCtrDB = null;

			View view = eirootDb.getView(viewName);
			if ( view == null ) throw (new Exception("Could not access the view \"" + viewName + "\""));
			Document doc = view.getFirstDocumentByKey(key);

			if ( doc == null ) {
				// OK, so look for it in the Config Center....
				if ( "ignore".equals(HS_Util.getAppSettingString("ConfigCenter")) ) {
					HS_Util.debug("Config center is being ignored.", "warn", "PayInvoceModel.load");
					// this.lastError = "Config center is being ignored.";
					// this.valid = false;
					// return false;
				} else {
					cfgCtrDB = hsdb.getdb("configCenter");
					if ( cfgCtrDB == null ) throw (new Exception("Could not access Config Center database: [" + hsdb.getdbBang("configCenter") + "]"));
					view = cfgCtrDB.getView(cfgViewName);
					if ( view == null ) throw (new Exception("Could not access the view \"" + cfgViewName + "\" in \"" + cfgCtrDB.getTitle() + "\"."));
					doc = view.getFirstDocumentByKey(key);
				}
			}

			if ( null == doc ) {
				// document not found. DANGER
				this.lastError = "document not found for key:[" + key + "]";
				HS_Util.debug(this.lastError, "warn", HS_Util.debugCalledFrom());
				this.valid = false;
			} else {
				this.loadValues(doc);
				// this.setLoadedForm(new HS_auditForm(doc));
			}

		} catch (Exception e) {
			this.lastError = "ERROR: " + e.toString();
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			this.valid = false;
		}
		return this.valid;

	}

	private void loadValues(Document doc) {
		try {
			this.unid = doc.getUniversalID();
			this.documentID = HS_Util.loadValueString(doc, "documentID");
			this.form = doc.getFormName();
			this.parentRef = doc.getParentDocumentUNID();

			this.amount = HS_Util.loadValueDouble(doc, "amount");
			this.viewDescription = HS_Util.loadValueString(doc, "ViewDescription");

			this.lineItems = new Vector<String>();
			String listr = "";
			if ( "CertificateRenewal".equals(this.form) ) {
				this.date = HS_Util.loadValueDate(doc, "FeeDate");
				this.invoiceNumber = HS_Util.loadValueString(doc, "DocumentID");

				this.businessContact = HS_Util.loadValueString(doc, "FeeBy");
				this.businessEmail = HS_Util.loadValueString(doc, "EMail");
				this.businessPhone = HS_Util.loadValueString(doc, "BusinessPhone");

				this.businessNumber = HS_Util.loadValueString(doc, "ParentUNID");
				this.businessName = HS_Util.loadValueString(doc, "Name");
				String dba = HS_Util.loadValueString(doc, "DoingBusinessAs");
				if ( !"".equals(dba) ) this.businessName += " (dba: " + dba + ")";

				this.businessAddress = HS_Util.loadValueString(doc, "MailingAddress");
				this.businessCity = HS_Util.loadValueString(doc, "MailingCity");
				this.businessPostalCode = HS_Util.loadValueString(doc, "MailingPostalCode");
				this.businessProvince = HS_Util.loadValueString(doc, "MailingProvince");

				this.li_facilities = new Vector<String>();
				this.li_facilities.add(this.businessName);
				this.li_fees = new Vector<String>();
				this.li_fees.add("");
				this.li_type = new Vector<String>();
				this.li_type.add(HS_Util.loadValueString(doc, "Type"));
				this.li_amounts = new Vector<Double>();
				this.li_amounts.add(HS_Util.loadValueDouble(doc, "amount"));
				this.li_addresses = new Vector<String>();
				this.li_addresses.add("");

				listr = "1*Professional certification renewal " + this.invoiceNumber;
				listr += "*" + HS_Util.formatDate(this.date, "d-MMM-yyyy");
				// String bcontact = (!"".equals(doc.getItemValueString("BillingContact"))) ? doc.getItemValueString("BillingContact") : (!"".equals(doc
				// .getItemValueString("FacilityName"))) ? doc.getItemValueString("FacilityName") : "(no name)";
				// listr += " for " + bcontact + ", " + HS_Util.getSession().evaluate("@Text(Amount; \"C,2\")", doc).get(0);
				listr += "*" + this.amount;
				this.lineItems.add(listr);
			} else {
				this.date = HS_Util.loadValueDate(doc, "Date");
				this.invoiceNumber = HS_Util.loadValueString(doc, "InvoiceNumber");

				this.businessContact = HS_Util.loadValueString(doc, "BusinessContact");
				this.businessEmail = HS_Util.loadValueString(doc, "BusinessEmail");
				this.businessPhone = HS_Util.loadValueString(doc, "BusinessPhone");

				this.businessNumber = HS_Util.loadValueString(doc, "BusinessNumber");
				this.businessName = HS_Util.loadValueString(doc, "BusinessName");
				this.businessAddress = HS_Util.loadValueString(doc, "BusinessAddress");
				this.businessCity = HS_Util.loadValueString(doc, "BusinessCity");
				this.businessPostalCode = HS_Util.loadValueString(doc, "BusinessPostalCode");
				this.businessProvince = HS_Util.loadValueString(doc, "BusinessProvince");

				this.endDate = HS_Util.loadValueDate(doc, "EndDate");

				this.li_facilities = HS_Util.loadValueStringArray(doc, "facilities");
				this.li_fees = HS_Util.loadValueStringArray(doc, "Fees");
				this.li_type = HS_Util.loadValueStringArray(doc, "type");
				this.li_amounts = HS_Util.loadValueDoubleArray(doc, "Amounts");
				this.li_addresses = HS_Util.loadValueStringArray(doc, "Addresses");

				if ( "CombinedFacilitiesRenewal".equals(this.form) ) {
					// [1*Multi Facility Renewal Notice QUAL201796830*1-Aug-2016 for $350.00*350.0]
					listr = "1*Multi Facility Renewal Notice " + this.invoiceNumber;
					listr += "*" + HS_Util.formatDate(this.date, "d-MMM-yyyy");
					// listr += " for " + HS_Util.getSession().evaluate("@Text(Amount; \"C\")", doc).get(0);
					listr += "*" + this.amount;
					this.lineItems.add(listr);
				} else if ( "Invoice".equals(this.form) ) {
					// [1*Invoice XXXXXXX*1-Aug-2016 for $300.00*300.0]
					listr = "1*Invoice " + this.invoiceNumber;
					listr += "*" + HS_Util.formatDate(this.date, "d-MMM-yyyy");
					// String bcontact = (!"".equals(doc.getItemValueString("BillingContact"))) ? doc.getItemValueString("BillingContact") : (!"".equals(doc
					// .getItemValueString("FacilityName"))) ? doc.getItemValueString("FacilityName") : "(no name)";
					// listr += " for " + bcontact + ", " + HS_Util.getSession().evaluate("@Text(Amount; \"C,2\")", doc).get(0);
					listr += "*" + this.amount;
					this.lineItems.add(listr);
				} else if ( "Renewal".equals(this.form) ) {
					// [1*Renewal Notice 201700004*for DOLLAR GENERAL #16394, 01-Jul-2016 for $50.00*50.0
					listr = "1*Renewal Notice " + this.invoiceNumber;
					// listr += "*" + HS_Util.formatDate(this.date, "d-MMM-yyyy");
					// String bcontact = (!"".equals(doc.getItemValueString("BillingContact"))) ? doc.getItemValueString("BillingContact") : (!"".equals(doc
					// .getItemValueString("FacilityName"))) ? doc.getItemValueString("FacilityName") : "(no name)";
					// listr += " for " + bcontact + ", " + HS_Util.getSession().evaluate("@Text(Amount; \"C,2\")", doc).get(0);
					listr += "*" + this.amount;
					this.lineItems.add(listr);
				} else {
					listr = "1*Other Notice " + this.invoiceNumber;
					listr += "*" + HS_Util.formatDate(this.date, "d-MMM-yyyy");
					// String bcontact = (!"".equals(doc.getItemValueString("BillingContact"))) ? doc.getItemValueString("BillingContact") : (!"".equals(doc
					// .getItemValueString("FacilityName"))) ? doc.getItemValueString("FacilityName") : "(no name)";
					// listr += " for " + bcontact + ", " + HS_Util.getSession().evaluate("@Text(Amount; \"C,2\")", doc).get(0);
					listr += "*" + this.amount;
					this.lineItems.add(listr);
				}

			}
			// " value="[1*[Line Item User Part 1]*[Line Item User Part 2]*[Line Item User Part 3]~[Amount]]"/>

			// for (int i = 0; i < this.li_facilities.size(); i++) {
			// listr = "" + (i + 1) + ".*";
			// listr += this.li_facilities.get(i) + "*";
			// listr += this.li_addresses.get(i) + "*";
			// listr += "" + this.li_amounts.get(i);
			// this.lineItems.add(listr);
			// }
			this.valid = true;
		} catch (Exception e) {
			this.lastError = e.toString();
			HS_Util.debug(this.lastError, "error", HS_Util.debugCalledFrom() + ".loadValues");
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			this.valid = false;
		}
	}

	public String saveTransaction() {
		final boolean localDebug = HS_Util.isDebugServer();
		final String msgContext = this.getClass().getName() + ".saveTransaction";
		final String pmtsDbKey = "payments";
		String url = "";
		String unid = "";

		try {
			if ( localDebug ) HS_Util.debug("Posting with key:[" + this.invoiceNumber + "]", "debug", msgContext);
			HS_database hsdb = new HS_database();
			// Database eirootDb = hsdb.getdb("eiroot");
			Database paymentDB = hsdb.getdbAsSigner(pmtsDbKey);
			if ( paymentDB == null ) throw (new Exception("Unable to access " + pmtsDbKey + " database! [" + hsdb.getdbBang(pmtsDbKey) + "]"));

			if ( localDebug )
				HS_Util.debug("Creating the transaction document in  [" + hsdb.getdbBang(pmtsDbKey) + "] with access level:["
						+ paymentDB.getCurrentAccessLevel() + "]", "debug", msgContext);
			String wrkDate = HS_Util.formatDate(new Date(), "yyyy-MM-dd") + "T00:00:00";
			String procMode = HS_Util.getAppSettingString("AppsAndPmts_ProcMode") + "-" + HS_Util.getAppSettingString("AppsAndPmts_Vendor");
			Document paydoc = paymentDB.createDocument();
			paydoc.replaceItemValue("Form", "Transaction");
			paydoc.replaceItemValue("ProcMode", procMode);
			paydoc.replaceItemValue("BatchDate", wrkDate);
			paydoc.replaceItemValue("DateReceived", wrkDate);
			// - Do we need a sequential number here??
			paydoc.replaceItemValue("TransactionNum", this.getTransactionNum(paymentDB, wrkDate, procMode));

			paydoc.replaceItemValue("pmt_invoice", this.invoiceNumber);
			paydoc.replaceItemValue("pmt_LastName", this.businessName);
			paydoc.replaceItemValue("TransactionID", "Pending");
			paydoc.replaceItemValue("EHSStatus", "Pending");
			paydoc.replaceItemValue("TransactionStatus", "Pending");
			paydoc.replaceItemValue("BatchStatus", "Pending");
			paydoc.replaceItemValue("TransactionTotal", "" + this.amount); // Convert it to a string??
			paydoc.replaceItemValue("Error", "");

			paydoc.replaceItemValue("PaymentBatchID", HS_Util.getAppSettingString("AppsAndPmts_Vendor") + "-" + HS_Util.formatDate(new Date(), "yyyy-MM-dd"));
			paydoc.replaceItemValue("BatchActionReason", "Web Site Payment Posting");
			paydoc.replaceItemValue("BatchDepositTotal", "" + this.amount);
			paydoc.replaceItemValue("BatchTransactionTotal", "" + this.amount);
			paydoc.replaceItemValue("PaymentOrigin", "Website to " + HS_Util.getAppSettingString("AppsAndPmts_Vendor"));

			paydoc.replaceItemValue("InvoiceType", this.form);
			// paydoc.replaceItemValue("BillingName", this.businessName);
			// paydoc.replaceItemValue("BillingAddress", this.businessAddress);
			// paydoc.replaceItemValue("BillingCity", this.businessCity);
			// paydoc.replaceItemValue("BillingState", this.businessProvince);
			// paydoc.replaceItemValue("BillingPostalCode", this.businessPostalCode);
			// paydoc.replaceItemValue("BillingEmail", this.businessEmail);
			// paydoc.replaceItemValue("BillingPhone", this.businessPhone);

			// Returning blanks for all these items per Blake Thomas - Newbs 18Aug2016
			paydoc.replaceItemValue("BillingName", "");
			paydoc.replaceItemValue("BillingAddress", "");
			paydoc.replaceItemValue("BillingCity", "");
			paydoc.replaceItemValue("BillingState", "");
			paydoc.replaceItemValue("BillingPostalCode", "");
			paydoc.replaceItemValue("BillingEmail", "");
			paydoc.replaceItemValue("BillingPhone", "");

			paydoc.replaceItemValue("UserPart1", this.businessName);
			paydoc.replaceItemValue("UserPart2", this.businessNumber);

			paydoc.replaceItemValue("BusinessNumber", this.businessNumber);
			paydoc.replaceItemValue("PostURL", this.getPostUrl());

			paydoc.replaceItemValue("LineItems", this.lineItems);

			String docid = this.getPaymentDocumentID();
			paydoc.replaceItemValue("DocumentID", docid);

			if ( localDebug ) HS_Util.debug("Saving the ransaction document.", "debug", msgContext);
			if ( paydoc.save() ) {
				unid = paydoc.getUniversalID();
				if ( localDebug ) HS_Util.debug("transaction document saved! unid:[" + unid + "]", "debug", msgContext);
				url = this.getBaseUrl() + "/FISPaymentPostingForm?ReadForm";
				// url += "&unid=" + unid;
				url += "&documentid=" + docid;
				url += "&server=" + paymentDB.getServer();
				url += "&path=" + paymentDB.getFilePath();

				// if ( url.contains("henry1.healthspace.com") || url.contains("hsdevtest01.healthspace.com") ) url += "&test=true";
				if ( "Yes".equals(HS_Util.getAppSettingString("debug_payInvoice")) ) url += "&test=true";

				return url;
			} else {
				throw (new Exception("Unable to save the transaction document in " + pmtsDbKey + " database. [" + hsdb.getdbBang(pmtsDbKey) + "]"));
			}
			// this.lastError = msgContext + " - work in process";
			// return url;
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			this.lastError = "ERROR! " + e.toString();
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			return "";
		}
	}

	private Object getTransactionNum(Database paydb, String wrkDate, String procMode) {
		String msgContext = this.getClass().getName() + ".getTransactionNum";
		String result = "101";
		final String vname = "Code.Transactions.Lookup.For.Number";
		final String cat = procMode + "-" + wrkDate.substring(0, 10);
		View txview = paydb.getView(vname);
		if ( txview == null ) {
			HS_Util.debug("Unable to access view: \"" + vname + "\"", "warn", msgContext);
			return result;
		}
		ViewNavigator vnav = txview.createViewNavFromCategory(cat);
		if ( vnav == null ) {
			HS_Util.debug("Unable to view nav in view  \"" + vname + "\" with cat:[" + cat + "]", "warn", msgContext);
			return result;
		}
		// HS_Util.debug("in view  \"" + vname + "\" with cat:[" + cat + "] count:[" + vnav.getCount() + "]", "debug", msgContext);

		result = "" + (101 + vnav.getCount());
		return result;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setBusinessAddress(String businessAddress) {
		this.businessAddress = businessAddress;
	}

	public void setBusinessCity(String businessCity) {
		this.businessCity = businessCity;
	}

	public void setBusinessContact(String businessContact) {
		this.businessContact = businessContact;
	}

	public void setBusinessEmail(String businessEmail) {
		this.businessEmail = businessEmail;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public void setBusinessNumber(String businessNumber) {
		this.businessNumber = businessNumber;
	}

	public void setBusinessPhone(String businessPhone) {
		this.businessPhone = businessPhone;
	}

	public void setBusinessPostalCode(String businessPostalCode) {
		this.businessPostalCode = businessPostalCode;
	}

	public void setBusinessProvince(String businessProvince) {
		this.businessProvince = businessProvince;
	}

	public void setCancelUrl(String cancelUrl) {
		this.cancelUrl = cancelUrl;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setDocumentID(String documentID) {
		this.documentID = documentID;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public void setLastError(String lastError) {
		this.lastError = lastError;
	}

	public void setLi_addresses(Vector<String> li_addresses) {
		this.li_addresses = li_addresses;
	}

	public void setLi_amounts(Vector<Double> li_amounts) {
		this.li_amounts = li_amounts;
	}

	public void setLi_facilities(Vector<String> li_facilities) {
		this.li_facilities = li_facilities;
	}

	public void setLi_fees(Vector<String> li_fees) {
		this.li_fees = li_fees;
	}

	public void setLi_type(Vector<String> li_type) {
		this.li_type = li_type;
	}

	public void setLineItems(Vector<String> lineItems) {
		this.lineItems = lineItems;
	}

	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	public void setParentRef(String parentRef) {
		this.parentRef = parentRef;
	}

	public void setPaymentDocumentID(String paymentDocumentID) {
		this.paymentDocumentID = paymentDocumentID;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public void setPostBackUrl(String postBackUrl) {
		this.postBackUrl = postBackUrl;
	}

	public void setPostUrl(String postUrl) {
		this.postUrl = postUrl;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	public void setSettleCode(String settleCode) {
		this.settleCode = settleCode;
	}

	public void setUnid(String unid) {
		this.unid = unid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public void setViewDescription(String viewDescription) {
		this.viewDescription = viewDescription;
	}

}
