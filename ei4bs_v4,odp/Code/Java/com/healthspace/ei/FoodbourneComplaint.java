package com.healthspace.ei;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import org.openntf.domino.Database;
import org.openntf.domino.DateTime;
import org.openntf.domino.Document;
import org.openntf.domino.Name;
import org.openntf.domino.Session;
import org.openntf.domino.View;
import org.openntf.domino.utils.Factory;

import com.healthspace.general.HS_Util;
import com.healthspace.general.HS_database;
import com.healthspace.tools.JSFUtil;
import com.ibm.jscript.InterpretException;
import com.ibm.jscript.std.ObjectObject;

/**
 * @author Henry Newberry
 * 
 */
public class FoodbourneComplaint implements Serializable {

	private static final long serialVersionUID = 1L;

	private String additionalInformation;
	private String address;

	// private String complainantName;
	private String complaintName;
	private String complainantAddress;
	private String complainantCity;
	private String complainantProvince;
	private String complainantPostalCode;
	private String complainantCountry;
	private String complainantPhone;
	private String complainantEveningPhone;
	private String complainantEMail;
	private double complainantAge;
	private String complainantGender;

	private String hcProviderSeen;
	private Date hcProviderDate;
	private String hcProviderName;
	private String hcProviderPhone;
	private String hcProviderStoolSample;
	private String hcProviderDiagnosis;
	private String hcProviderDiagnosisDetails;

	private Date date;
	private Date time;
	private Date dateClosed;
	private Date dateCreated;
	private Date dateReportReceived;
	private Date dateStart;
	private String documentID;
	private String form;
	private String name;
	private String physicalAddress;
	private String physicalCity;
	private String physicalProvince;
	private String physicalPostalCode;
	private String physicalCountry;

	private String phone;
	private String receivedBy;
	private String referredTo;
	private String source;
	private String status;
	private Date suspectedDate;
	private String suspectedFood;
	private Date suspectedTime;

	private String symptoms;
	private Date symptomsDate;
	private Date symptomsDateStopped;
	private String symptomsFirst;
	private String symptonsRemain;
	private String symptomsWorst;

	private Date symptomsTime;
	private Date timeStart;
	private String viewDate;
	private String viewDescription;
	private String viewHide;

	private String unid;
	private boolean valid;
	private String lastError;

	// These handle the input for date and time values.
	private String dateStr;
	private String timeStr;
	private String dateCreatedStr;
	private String dateReportReceivedStr;
	private String dateStartStr;

	private String hcProviderDateStr;

	private String suspectedDateStr;
	private String suspectedTimeStr;
	private String symptomsDateStr;
	private String symptomsDateStoppedStr;
	private String symptomsTimeStr;

	public FoodbourneComplaint() {
		this.unid = "";
		this.valid = false;
		this.lastError = "";
	}

	public void create() {

		Session session = Factory.getSession(); // this will be slightly different if not using the OpenNTF Domino API
		this.setDocumentID(session.getUnique());
		Name uname = session.createName(session.getEffectiveUserName());
		this.setReceivedBy("");

		this.setSource(uname.getCommon());
		// this.setDate(new Date());
		Date wdt = new Date();
		String mask = HS_Util.getAppSettingString("inspDateFormat");
		this.setDateCreated(wdt);
		this.setDateCreatedStr(HS_Util.formatDate(wdt, mask));
		this.setDateReportReceived(wdt);
		this.setDateReportReceivedStr(HS_Util.formatDate(wdt, mask));
		this.setStatus("New");
		this.setName_default();

		this.valid = true;
	}

	public String getAdditionalInformation() {
		return additionalInformation;
	}

	public String getAddress() {
		return address;
	}

	public String getComplainantAddress() {
		return complainantAddress;
	}

	public double getComplainantAge() {
		return complainantAge;
	}

	public String getComplainantCity() {
		return complainantCity;
	}

	public String getComplainantCountry() {
		return complainantCountry;
	}

	public String getComplainantEMail() {
		return complainantEMail;
	}

	public String getComplainantEveningPhone() {
		return complainantEveningPhone;
	}

	public String getComplainantGender() {
		return complainantGender;
	}

	public String getComplainantPhone() {
		return complainantPhone;
	}

	public String getComplainantPostalCode() {
		return complainantPostalCode;
	}

	public String getComplainantProvince() {
		return complainantProvince;
	}

	public String getComplaintName() {
		return complaintName;
	}

	public Date getDate() {
		if (!"".equals(this.dateStr)) {
			try {
				date = HS_Util.cvrtStringToDate(this.dateStr);
			} catch (Exception e) {
				HS_Util.debug("dateStr is:[" + this.dateStr + "] ERROR: " + e.toString(), "error", "FoodbourneComplaint.java.getDate");
			}
		}
		return date;
	}

	public Date getDateClosed() {
		return dateClosed;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public String getDateCreatedStr() {
		return dateCreatedStr;
	}

	public Date getDateReportReceived() {
		return dateReportReceived;
	}

	public String getDateReportReceivedStr() {
		return dateReportReceivedStr;
	}

	public Date getDateStart() {
		return dateStart;
	}

	public String getDateStartStr() {
		return dateStartStr;
	}

	public String getDateStr() {
		return dateStr;
	}

	public String getDocumentID() {
		return documentID;
	}

	public String getForm() {
		if (this.form == null || "".equals(this.form)) this.form = "FoodborneComplaint";
		return form;
	}

	public Date getHcProviderDate() {
		return hcProviderDate;
	}

	public String getHcProviderDateStr() {
		return hcProviderDateStr;
	}

	public String getHcProviderDiagnosis() {
		return hcProviderDiagnosis;
	}

	public String getHcProviderDiagnosisDetails() {
		return hcProviderDiagnosisDetails;
	}

	public String getHcProviderName() {
		return hcProviderName;
	}

	public String getHcProviderPhone() {
		return hcProviderPhone;
	}

	public String getHcProviderSeen() {
		return hcProviderSeen;
	}

	public String getHcProviderStoolSample() {
		return hcProviderStoolSample;
	}

	public String getLastError() {
		return lastError;
	}

	public String getName() {
		return name;
	}

	public String getPhone() {
		return phone;
	}

	public String getPhysicalAddress() {
		return physicalAddress;
	}

	public String getPhysicalCity() {
		return physicalCity;
	}

	public String getPhysicalCountry() {
		return physicalCountry;
	}

	public String getPhysicalPostalCode() {
		return physicalPostalCode;
	}

	public String getPhysicalProvince() {
		return physicalProvince;
	}

	public String getReceivedBy() {
		return receivedBy;
	}

	public String getReferredTo() {
		return referredTo;
	}

	public String getSource() {
		return source;
	}

	public String getStatus() {
		return status;
	}

	public Date getSuspectedDate() {
		return suspectedDate;
	}

	public String getSuspectedDateStr() {
		return suspectedDateStr;
	}

	public String getSuspectedFood() {
		return suspectedFood;
	}

	public Date getSuspectedTime() {
		return suspectedTime;
	}

	public String getSuspectedTimeStr() {
		return suspectedTimeStr;
	}

	public String getSymptoms() {
		return symptoms;
	}

	public Date getSymptomsDate() {
		return symptomsDate;
	}

	public Date getSymptomsDateStopped() {
		return symptomsDateStopped;
	}

	public String getSymptomsDateStoppedStr() {
		return symptomsDateStoppedStr;
	}

	public String getSymptomsDateStr() {
		return symptomsDateStr;
	}

	public String getSymptomsFirst() {
		return symptomsFirst;
	}

	public Date getSymptomsTime() {
		return symptomsTime;
	}

	public String getSymptomsTimeStr() {
		return symptomsTimeStr;
	}

	public String getSymptomsWorst() {
		return symptomsWorst;
	}

	public String getSymptonsRemain() {
		return symptonsRemain;
	}

	public Date getTime() {
		return time;
	}

	public Date getTimeStart() {
		return timeStart;
	}

	public String getTimeStr() {
		return timeStr;
	}

	public String getUnid() {
		return unid;
	}

	public String getViewDate() {
		return viewDate;
	}

	public String getViewDescription() {
		return viewDescription;
	}

	public String getViewHide() {
		return viewHide;
	}

	public boolean isValid() {
		return valid;
	}

	public boolean load(String key) {

		// this key is the unique key of the document. UNID would be faster/easier.. I just kinda hate using them and seeing them in URLS
		// Session session = Factory.getSession();
		// Database db = session.getCurrentDatabase();
		HS_database hsdb = new HS_database();
		Database eirootDb = hsdb.getdb("eiroot");

		View view = eirootDb.getView("(DocumentIDLookup)");
		Document doc = view.getFirstDocumentByKey(key); // This is deprecated because the API prefers to use getFirstDocumentByKey

		if (null == doc) {
			// document not found. DANGER
			this.lastError = "document not found for key:[" + key + "] DANGER";
			HS_Util.debug(this.lastError, "warn", HS_Util.debugCalledFrom());
			this.valid = false;
		} else {
			this.loadValues(doc);
		}

		return this.valid;

	}

	private void loadValues(Document doc) {
		try {
			this.unid = doc.getUniversalID();
			this.documentID = HS_Util.loadValueString(doc, "documentID");
			this.additionalInformation = HS_Util.loadValueString(doc, "additionalInformation");
			this.address = HS_Util.loadValueString(doc, "address");

			this.complaintName = HS_Util.loadValueString(doc, "complaintName");
			this.complainantAddress = HS_Util.loadValueString(doc, "complainantAddress");
			this.complainantCity = HS_Util.loadValueString(doc, "complainantCity");
			this.complainantProvince = HS_Util.loadValueString(doc, "complainantProvince");
			this.complainantPostalCode = HS_Util.loadValueString(doc, "complainantPostalCode");
			this.complainantCountry = HS_Util.loadValueString(doc, "complainantCountry");
			this.complainantPhone = HS_Util.loadValueString(doc, "complainantPhone");
			this.complainantEveningPhone = HS_Util.loadValueString(doc, "complainantEveningPhone");
			this.complainantEMail = HS_Util.loadValueString(doc, "complainantEMail");
			this.complainantAge = HS_Util.loadValueDouble(doc, "complainantAge");
			this.complainantGender = HS_Util.loadValueString(doc, "complainantGender");

			this.hcProviderDate = HS_Util.loadValueDate(doc, "hcProviderDate");
			this.hcProviderDiagnosis = HS_Util.loadValueString(doc, "hcProviderDiagnosis");
			this.hcProviderDiagnosisDetails = HS_Util.loadValueString(doc, "hcProviderDiagnosisDetails");
			this.hcProviderName = HS_Util.loadValueString(doc, "hcProviderName");
			this.hcProviderPhone = HS_Util.loadValueString(doc, "hcProviderPhone");
			this.hcProviderSeen = HS_Util.loadValueString(doc, "hcProviderSeen");
			this.hcProviderStoolSample = HS_Util.loadValueString(doc, "hcProviderStoolSample");

			this.form = HS_Util.loadValueString(doc, "form");
			this.name = HS_Util.loadValueString(doc, "name");
			this.physicalAddress = HS_Util.loadValueString(doc, "PhysicalAddress");
			this.physicalCity = HS_Util.loadValueString(doc, "PhysicalCity");
			this.physicalProvince = HS_Util.loadValueString(doc, "PhysicalProvince");
			this.physicalPostalCode = HS_Util.loadValueString(doc, "PhysicalPostalCode");
			this.physicalCountry = HS_Util.loadValueString(doc, "PhysicalCountry");

			this.phone = HS_Util.loadValueString(doc, "phone");
			this.receivedBy = HS_Util.loadValueString(doc, "receivedBy");
			this.referredTo = HS_Util.loadValueString(doc, "referedTo");
			this.source = HS_Util.loadValueString(doc, "source");
			this.status = HS_Util.loadValueString(doc, "status");
			this.suspectedFood = HS_Util.loadValueString(doc, "suspectedFood");
			this.symptoms = HS_Util.loadValueString(doc, "symptoms");
			this.viewDate = HS_Util.loadValueString(doc, "viewDate");
			this.viewDescription = HS_Util.loadValueString(doc, "viewDescription");
			this.viewHide = HS_Util.loadValueString(doc, "viewHide");

			this.date = HS_Util.loadValueDate(doc, "date");
			this.dateClosed = HS_Util.loadValueDate(doc, "dateClosed");
			this.dateCreated = HS_Util.loadValueDate(doc, "dateCreated");
			this.dateReportReceived = HS_Util.loadValueDate(doc, "dateReportReceived");
			this.dateStart = HS_Util.loadValueDate(doc, "dateStart");
			this.suspectedDate = HS_Util.loadValueDate(doc, "suspectedDate");
			this.suspectedTime = HS_Util.loadValueDate(doc, "suspectedTime");
			this.time = HS_Util.loadValueDate(doc, "time");
			this.timeStart = HS_Util.loadValueDate(doc, "timeStart");

			this.valid = true;
		} catch (Exception e) {
			this.lastError = e.toString();
			HS_Util.debug(this.lastError, "error", HS_Util.debugCalledFrom() + ".loadValues");
		}
	}

	public boolean save() {
		boolean localDebug = true;
		String msgContext = "FoodbourneComplaint.save";

		boolean result = false;
		try {
			// Database eidb = (new HS_database()).getdb("eiRoot");
			Database eidb = (new HS_database()).getdbAsSigner("eiRoot");
			if (localDebug) HS_Util.debug("saving to " + eidb.getServer() + "!!" + eidb.getFilePath() + " - access level: " + eidb.getCurrentAccessLevel(), "debug", msgContext);
			Document fbidoc = null;
			if ("".equals(this.unid)) {
				fbidoc = eidb.createDocument();
			} else {
				fbidoc = eidb.getDocumentByUNID(this.unid);
			}
			if (fbidoc != null) {
				result = this.saveValues(fbidoc);
				HS_Util.debug("Document saveValues result:[" + result + "]", "debug", HS_Util.debugCalledFrom());

				if (result) result = fbidoc.save();

				if (result) this.unid = fbidoc.getUniversalID();

				HS_Util.debug("Document save result:[" + result + "] unid:[" + this.unid + "]", "debug", HS_Util.debugCalledFrom());
			}
		} catch (Exception e) {
			this.lastError = e.toString();
			HS_Util.debug(this.lastError, "error", HS_Util.debugCalledFrom());
			result = false;
		}

		return result;
	}

	private boolean saveValues(Document doc) {
		boolean result = false;
		try {
			result = true;
			result = result && this.updateItem(doc, "DocumentId", this.getDocumentID());
			result = result && this.updateItem(doc, "AdditionalInformation", this.getAdditionalInformation());
			result = result && this.updateItem(doc, "Address", this.getAddress());

			result = result && this.updateItem(doc, "ComplaintName", this.getComplaintName());
			result = result && this.updateItem(doc, "ComplainantAddress", this.getComplainantAddress());
			result = result && this.updateItem(doc, "ComplainantCity", this.getComplainantCity());
			result = result && this.updateItem(doc, "ComplainantProvince", this.getComplainantProvince());
			result = result && this.updateItem(doc, "ComplainantPostalCode", this.getComplainantPostalCode());
			result = result && this.updateItem(doc, "ComplainantCountry", this.getComplainantCountry());
			result = result && this.updateItem(doc, "ComplainantPhone", this.getComplainantPhone());
			result = result && this.updateItem(doc, "ComplainantEveningPhone", this.getComplainantEveningPhone());
			result = result && this.updateItem(doc, "ComplainantEMail", this.getComplainantEMail());
			result = result && this.updateItem(doc, "ComplainantAge", this.getComplainantAge());
			result = result && this.updateItem(doc, "ComplainantGender", this.getComplainantGender());

			result = result && this.updateItem(doc, "HCProviderSeen", this.getHcProviderSeen());
			result = result && this.updateItem(doc, "HcProviderDate", this.getHcProviderDate());
			result = result && this.updateItem(doc, "HCHcProviderName", this.getHcProviderName());
			result = result && this.updateItem(doc, "HCProviderPhone", this.getHcProviderPhone());
			result = result && this.updateItem(doc, "HcProviderStoolSample", this.getHcProviderStoolSample());
			result = result && this.updateItem(doc, "HcProviderDiagnosis", this.getHcProviderDiagnosis());
			result = result && this.updateItem(doc, "HcProviderDiagnosisDetails", this.getHcProviderDiagnosisDetails());

			result = result && this.updateItem(doc, "Name", this.getName());
			result = result && this.updateItem(doc, "PhysicalAddress", this.getPhysicalAddress());
			result = result && this.updateItem(doc, "PhysicalCity", this.getPhysicalCity());
			result = result && this.updateItem(doc, "PhysicalProvince", this.getPhysicalProvince());
			result = result && this.updateItem(doc, "PhysicalPostalCode", this.getPhysicalPostalCode());
			result = result && this.updateItem(doc, "PhysicalCountry", this.getPhysicalCountry());

			result = result && this.updateItem(doc, "Form", this.getForm());

			result = result && this.updateItem(doc, "Phone", this.getPhone());
			result = result && this.updateItem(doc, "ReceivedBy", this.getReceivedBy());
			result = result && this.updateItem(doc, "ReferredTo", this.getReferredTo());
			result = result && this.updateItem(doc, "Source", this.getSource());
			result = result && this.updateItem(doc, "Status", this.getStatus());
			result = result && this.updateItem(doc, "SuspectedFood", this.getSuspectedFood());
			result = result && this.updateItem(doc, "Symptoms", this.getSymptoms());
			result = result && this.updateItem(doc, "ViewDate", this.getViewDate());
			result = result && this.updateItem(doc, "ViewDescription", this.getViewDescription());
			result = result && this.updateItem(doc, "ViewHide", this.getViewHide());

			result = result && this.updateItem(doc, "Date", this.getDate());
			result = result && this.updateItem(doc, "DateClosed", this.getDateClosed());
			result = result && this.updateItem(doc, "DateCreated", this.getDateCreated());
			result = result && this.updateItem(doc, "DateReportReceived", this.getDateReportReceived());
			result = result && this.updateItem(doc, "DateStarte", this.getDateStart());
			result = result && this.updateItem(doc, "SuspectedDate", this.getSuspectedDate());
			result = result && this.updateItem(doc, "SuspectedTime", this.getSuspectedTime());
			result = result && this.updateItem(doc, "Time", this.getTime());
			result = result && this.updateItem(doc, "TimeStart", this.getTimeStart());

			// result = result && this.updateItem(doc, "x", this.x);
			// result = result && this.updateItem(doc, "x", this.x);
			// result = result && this.updateItem(doc, "x", this.x);
			// result = result && this.updateItem(doc, "x", this.x);

		} catch (Exception e) {
			this.lastError = e.toString();
			HS_Util.debug(this.lastError, "error", HS_Util.debugCalledFrom());
			result = false;
		}

		return result;

	}

	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setComplainantAddress(String complainantAddress) {
		this.complainantAddress = complainantAddress;
	}

	public void setComplainantAge(double complainantAge) {
		this.complainantAge = complainantAge;
	}

	public void setComplainantCity(String complainantCity) {
		this.complainantCity = complainantCity;
	}

	public void setComplainantCountry(String complainantCountry) {
		this.complainantCountry = complainantCountry;
	}

	public void setComplainantEMail(String complainantEMail) {
		this.complainantEMail = complainantEMail;
	}

	public void setComplainantEveningPhone(String complainantEveningPhone) {
		this.complainantEveningPhone = complainantEveningPhone;
	}

	public void setComplainantGender(String complainantGender) {
		this.complainantGender = complainantGender;
	}

	public void setComplainantPhone(String complainantPhone) {
		this.complainantPhone = complainantPhone;
	}

	public void setComplainantPostalCode(String complainantPostalCode) {
		this.complainantPostalCode = complainantPostalCode;
	}

	public void setComplainantProvince(String complainantProvince) {
		this.complainantProvince = complainantProvince;
	}

	public void setComplaintName(String complaintName) {
		this.complaintName = complaintName;
		// if (!("".equals(complaintName))) {
		HS_Util.debug("complaintNames set to:[" + complaintName + "]", "debug", "FoodbourneComplaint.java.setComplaintName");
		// }
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setDateClosed(Date dateClosed) {
		this.dateClosed = dateClosed;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public void setDateCreatedStr(String dateCreatedStr) {
		this.dateCreatedStr = dateCreatedStr;
		this.dateCreated = this.updateDateFromString(dateCreatedStr, "dateCreatedStr");
	}

	public void setDateReportReceived(Date dateReportReceived) {
		this.dateReportReceived = dateReportReceived;
	}

	public void setDateReportReceivedStr(String dateReportReceivedStr) {
		this.dateReportReceivedStr = dateReportReceivedStr;
		this.dateReportReceived = this.updateDateFromString(dateReportReceivedStr, "dateReportReceivedStr");
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public void setDateStartStr(String dateStartStr) {
		this.dateStartStr = dateStartStr;
		this.dateStart = this.updateDateFromString(dateStartStr, "dateStartStr");
	}

	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
		this.date = this.updateDateFromString(dateStr, "dateStr");
	}

	public void setDocumentID(String documentID) {
		this.documentID = documentID;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public void setHcProviderDate(Date hcProviderDate) {
		this.hcProviderDate = hcProviderDate;
	}

	public void setHcProviderDateStr(String hcProviderDateStr) {
		this.hcProviderDateStr = hcProviderDateStr;
		this.hcProviderDate = this.updateDateFromString(hcProviderDateStr, "hcProviderDateStr");
	}

	public void setHcProviderDiagnosis(String hcProviderDiagnosis) {
		this.hcProviderDiagnosis = hcProviderDiagnosis;
	}

	public void setHcProviderDiagnosisDetails(String hcProviderDiagnosisDetails) {
		this.hcProviderDiagnosisDetails = hcProviderDiagnosisDetails;
	}

	public void setHcProviderName(String hcProviderName) {
		this.hcProviderName = hcProviderName;
	}

	public void setHcProviderPhone(String hcProviderPhone) {
		this.hcProviderPhone = hcProviderPhone;
	}

	public void setHcProviderSeen(String hcProviderSeen) {
		this.hcProviderSeen = hcProviderSeen;
	}

	public void setHcProviderStoolSample(String hcProviderStoolSample) {
		this.hcProviderStoolSample = hcProviderStoolSample;
	}

	public void setLastError(String lastError) {
		this.lastError = lastError;
	}

	public void setName(String name) {
		this.name = name;
	}

	private void setName_default() {
		boolean localDebug = false;
		String msgContext = "FoodbourneComplaint.setName_default";
		Map<String, Object> sscope = JSFUtil.getSessionScope();
		String facUnid = "";
		Database eidb = null;
		Document facDoc = null;

		String referer = JSFUtil.getReferer();
		if (localDebug) HS_Util.debug("referer:[" + referer + "]", "debug", msgContext);

		if ("".equals(referer)) return;
		if (!(referer.contains("formFacility.xsp"))) return;

		// String referer = //ExtLibUtil.getExternalContext();
		// String rtype = (HttpServletRequest) (JSFUtil.getServletContext().getRequest());
		// if (localDebug) HS_Util.debug("request is an : [" + rtype + "]", "debug", msgContext);

		if (sscope.containsKey("HS_InspectionViewData")) {
			ObjectObject ivd = (ObjectObject) sscope.get("HS_InspectionViewData");
			if (ivd.hasProperty("facName")) {
				try {
					this.name = ivd.get("facName").stringValue();
					facUnid = ivd.get("facUnid").stringValue();
					if (localDebug) HS_Util.debug("SScope has HS_InspectionViewData - facName: [" + this.name + "] FacUnid:[" + facUnid + "]", "debug", msgContext);
					if ("".equals(facUnid)) return;
					eidb = (new HS_database()).getdb("eiRoot");
					if (!eidb.isOpen()) {
						if (localDebug) HS_Util.debug("Could not access eiRoot database.", "warn", msgContext);
						return;
					}
					facDoc = eidb.getDocumentByUNID(facUnid);
					if (facDoc == null) {
						if (localDebug) HS_Util.debug("Could not get the facility document", "warn", msgContext);
						return;
					}
					this.physicalAddress = facDoc.getItemValueString("physicalAddress");
					this.physicalCity = facDoc.getItemValueString("physicalCity");
					this.physicalProvince = facDoc.getItemValueString("physicalProvince");
					this.physicalPostalCode = facDoc.getItemValueString("physicalPostalCode");
					this.physicalCountry = facDoc.getItemValueString("physicalCountry");
				} catch (InterpretException e) {
					HS_Util.debug("ERROR: " + e.toString(), "Error", msgContext);
				}
			}
		} else {
			if (localDebug) HS_Util.debug("SScope does not have HS_InspectionViewData", "debug", msgContext);
		}
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setPhysicalAddress(String physicalAddress) {
		this.physicalAddress = physicalAddress;
	}

	public void setPhysicalCity(String physicalCity) {
		this.physicalCity = physicalCity;
	}

	public void setPhysicalCountry(String physicalCountry) {
		this.physicalCountry = physicalCountry;
	}

	public void setPhysicalPostalCode(String physicalPostalCode) {
		this.physicalPostalCode = physicalPostalCode;
	}

	public void setPhysicalProvince(String physicalProvince) {
		this.physicalProvince = physicalProvince;
	}

	public void setReceivedBy(String receivedBy) {
		this.receivedBy = receivedBy;
	}

	public void setReferredTo(String referredTo) {
		this.referredTo = referredTo;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setSuspectedDate(Date suspectedDate) {
		this.suspectedDate = suspectedDate;
	}

	public void setSuspectedDateStr(String suspectedDateStr) {
		this.suspectedDateStr = suspectedDateStr;
		this.suspectedDate = this.updateDateFromString(suspectedDateStr, "SuspectedDateStr");
	}

	public void setSuspectedFood(String suspectedFood) {
		this.suspectedFood = suspectedFood;
	}

	public void setSuspectedTime(Date suspectedTime) {
		this.suspectedTime = suspectedTime;
	}

	public void setSuspectedTimeStr(String suspectedTimeStr) {
		this.suspectedTimeStr = suspectedTimeStr;
		this.suspectedTime = this.updateTimeFromString(suspectedTimeStr, "suspectedTimeStr");
	}

	public void setSymptoms(String symptoms) {
		this.symptoms = symptoms;
	}

	public void setSymptomsDate(Date symptomsDate) {
		this.symptomsDate = symptomsDate;
	}

	public void setSymptomsDateStopped(Date symptomsDateStopped) {
		this.symptomsDateStopped = symptomsDateStopped;
	}

	public void setSymptomsDateStoppedStr(String symptomsDateStoppedStr) {
		this.symptomsDateStoppedStr = symptomsDateStoppedStr;
	}

	public void setSymptomsDateStr(String symptomsDateStr) {
		this.symptomsDateStr = symptomsDateStr;
		this.symptomsDate = this.updateDateFromString(symptomsDateStr, "symptomsDateStr");
	}

	public void setSymptomsFirst(String symptomsFirst) {
		this.symptomsFirst = symptomsFirst;
	}

	public void setSymptomsTime(Date symptomsTime) {
		this.symptomsTime = symptomsTime;
	}

	public void setSymptomsTimeStr(String symptomsTimeStr) {
		this.symptomsTimeStr = symptomsTimeStr;
		this.symptomsTime = this.updateTimeFromString(symptomsTimeStr, "symptomsTimeStr");
	}

	public void setSymptomsWorst(String symptomsWorst) {
		this.symptomsWorst = symptomsWorst;
	}

	public void setSymptonsRemain(String symptonsRemain) {
		this.symptonsRemain = symptonsRemain;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public void setTimeStart(Date timeStart) {
		this.timeStart = timeStart;
	}

	public void setTimeStr(String timeStr) {
		this.timeStr = timeStr;
		this.time = this.updateTimeFromString(timeStr, "timeStr");
	}

	public void setUnid(String unid) {
		this.unid = unid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public void setViewDate(String viewDate) {
		this.viewDate = viewDate;
	}

	public void setViewDescription(String viewDescription) {
		this.viewDescription = viewDescription;
	}

	public void setViewHide(String viewHide) {
		this.viewHide = viewHide;
	}

	private Date updateDateFromString(String source, String fname) {
		final boolean localDebug = true;
		final String msgContext = "FoodbourneComplaint.java.updateDateFromString";
		Date result = null;

		try {
			result = ("".equals(source)) ? null : HS_Util.cvrtStringToDate(source);
			if (result != null) {
				if (localDebug) HS_Util.debug("for field: [" + fname + "] dateStr:[" + source + "] date is:[" + result.toString() + "]", "debug", msgContext);
			} else {
				if (localDebug) HS_Util.debug("for field: [" + fname + "] dateStr:[" + source + "] date is:[null]", "debug", msgContext);
			}
		} catch (Exception e) {
			HS_Util.debug("for field: [" + fname + "] for dateStr:[" + dateStr + "] ERROR: " + e.toString(), "error", "FoodbourneComplaint.java.setDateStr");
		}

		return result;
	}

	@SuppressWarnings("unused")
	private boolean updateItem(Document idoc, String itemName, DateTime idate) {
		final boolean localDebug = false;
		final String dbarLoc = "FoodbourneComplaint.updateItem - date";
		boolean result = false;
		try {
			if (idate == null) {
				idoc.replaceItemValue(itemName, "");
				result = true;
			} else {
				if (idate.isAnyTime()) {
					if (localDebug) HS_Util.debug("Time zone is: [" + idate.getTimeZone() + "] idate is:[" + idate.toString() + "]", "debug", dbarLoc);
					// idate.setLocalTime(0, 0, 0, 0);
				}
				idoc.replaceItemValue(itemName, idate);
				if (localDebug) HS_Util.debug("item:[" + itemName + "] updated OK to:[" + idate.toString() + "]", "debug", dbarLoc);
				result = true;
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString() + " on item:[" + itemName + "]", "error", dbarLoc);
		}

		return result;
	}

	private boolean updateItem(Document idoc, String itemName, Object value) {
		final boolean localDebug = false;
		final String dbarLoc = "FoodbourneComplaint.updateItem";
		boolean result = false;
		try {
			if (value == null) value = "";
			idoc.replaceItemValue(itemName, value);
			if (localDebug) HS_Util.debug("item:[" + itemName + "] updated OK", "debug", dbarLoc);
			result = true;
		} catch (Exception e) {
			HS_Util.debug(e.toString() + " on item:[" + itemName + "]", "error", dbarLoc);
		}

		return result;
	}

	private Date updateTimeFromString(String source, String fname) {
		final boolean localDebug = true;
		final String msgContext = "FoodbourneComplaint.java.updateDateFromString";
		Date result = null;

		if (localDebug) HS_Util.debug("for field: [" + fname + "] timeStr:[" + source + "] NOT SETTING TIME YET...", "debug", msgContext);

		// TODO Finish this method

		return result;
	}

}
