package com.healthspace.ei.controller;

import java.util.ArrayList;

import com.healthspace.ei.model.PayInvoiceModel;
import com.healthspace.general.HS_Util;

import controller.base;

public class PayInvoiceControl extends com.healthspace.ei.controller.base {

	private static final long serialVersionUID = 1L;

	private boolean readOnly;
	private boolean valid;
	private boolean loaded;
	private boolean newdoc;
	private boolean saved;
	private ArrayList<String> messages;
	private boolean hasMessages;
	private boolean hasSubmitErrors;

	private String invoiceKey;

	private PayInvoiceModel currentInvoice;

	public PayInvoiceControl() {
		invoiceKey = "";
		messages = new ArrayList<String>();
		currentInvoice = new PayInvoiceModel();
	}

	public PayInvoiceModel getCurrentInvoice() {
		return currentInvoice;
	}

	public String getInvoiceKey() {
		return invoiceKey;
	}

	public ArrayList<String> getMessages() {
		return messages;
	}

	public void init() {
		invoiceKey = "";
		messages = new ArrayList<String>();
		currentInvoice = new PayInvoiceModel();
		if ( !base.getParam("invoice").isEmpty() ) {
			invoiceKey = base.getParam("invoice");
			this.loadPayInvoice();
		}

	}

	public boolean isHasMessages() {
		// return hasMessages;
		if ( this.messages.size() == 0 ) {
			hasMessages = false;
		} else if ( "".equals(this.messages.get(0)) ) {
			hasMessages = false;
		} else
			hasMessages = true;
		return hasMessages;
	}

	public boolean isHasSubmitErrors() {
		return hasSubmitErrors;
	}

	public boolean isLoaded() {
		return loaded;
	}

	public boolean isNewdoc() {
		return newdoc;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public boolean isSaved() {
		return saved;
	}

	public boolean isValid() {
		return valid;
	}

	public boolean loadPayInvoice() {
		boolean result = true;
		messages = new ArrayList<String>();
		hasSubmitErrors = false;
		try {

			final boolean localDebug = HS_Util.isDebugServer();
			final String msgContext = HS_Util.debugCalledFrom() + ".loadPayInvoice";

			if ( localDebug ) HS_Util.debug("Searching for invoice with key:[" + invoiceKey + "]", "debug", msgContext);

			this.loaded = false;

			if ( "".equals(invoiceKey) ) {
				messages.add("Please enter a invoice number before clicking search...");
				result = false;
			} else {
				// currentReport = new PSReportModel();
				result = currentInvoice.load(invoiceKey);
				if ( result ) {
					this.loaded = true;
					this.readOnly = true;
				} else {
					messages.add("Failed to load the invoice");
					if ( !"".equals(currentInvoice.getLastError()) ) {
						messages.add(currentInvoice.getLastError());
					}
				}
			}
			return result;
		} catch (Exception e) {
			messages.add("ERROR! " + e.toString());
			hasSubmitErrors = true;
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			return false;
		}
	}

	// Saves the transaction document and Returns the URL to be used to redirect the user to the payment processor.
	public String postPayment() {
		final boolean localDebug = HS_Util.isDebugServer();
		final String msgContext = this.getClass().getName() + ".postPayment";
		String url = "";
		// String unid = "";
		messages = new ArrayList<String>();
		hasSubmitErrors = false;
		try {
			if ( localDebug ) HS_Util.debug("Posting with key:[" + invoiceKey + "]", "debug", msgContext);
			url = currentInvoice.saveTransaction();
			if ( "".equals(url) ) {
				if ( !"".equals(currentInvoice.getLastError()) ) throw (new Exception(currentInvoice.getLastError()));
				return "";
			}
			// messages.add("URL is not yet built! UNID:[" + unid + "]");
			return url;
		} catch (Exception e) {
			messages.add("ERROR! " + e.toString());
			hasSubmitErrors = true;
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			return "";
		}
	}

	public void setCurrentInvoice(PayInvoiceModel currentInvoice) {
		this.currentInvoice = currentInvoice;
	}

	public void setHasMessages(boolean hasMessages) {
		this.hasMessages = hasMessages;
	}

	public void setHasSubmitErrors(boolean hasSubmitErrors) {
		this.hasSubmitErrors = hasSubmitErrors;
	}

	public void setInvoiceKey(String invoiceKey) {
		this.invoiceKey = invoiceKey;
	}

	public void setLoaded(boolean loaded) {
		this.loaded = loaded;
	}

	public void setMessages(ArrayList<String> messages) {
		this.messages = messages;
	}

	public void setNewdoc(boolean newdoc) {
		this.newdoc = newdoc;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public void setSaved(boolean saved) {
		this.saved = saved;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}
}
