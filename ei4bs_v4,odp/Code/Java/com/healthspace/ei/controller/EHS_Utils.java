package com.healthspace.ei.controller;

import java.util.Date;
import java.util.Vector;

import org.openntf.domino.Document;

import com.healthspace.general.HS_Util;
import com.healthspace.general.Inspection;

public class EHS_Utils {

	public static Date loadValueDate(Inspection insp, Document idoc, String iname) {
		return HS_Util.loadValueDate(idoc, iname);
	}

	public static double loadValueDouble(Inspection insp, Document idoc, String iname) {
		return HS_Util.loadValueDouble(idoc, iname);
	}

	public static Vector<Double> loadValueDoubleArray(Inspection insp, Document idoc, String iname) {
		return HS_Util.loadValueDoubleArray(idoc, iname);
	}

	public static int loadValueInt(Inspection insp, Document idoc, String iname) {
		return HS_Util.loadValueInt(idoc, iname);
	}

	public static String loadValueString(Inspection insp, Document idoc, String iname) {
		return HS_Util.loadValueString(idoc, iname);
	}

	public static Vector<String> loadValueStringArray(Inspection insp, Document idoc, String iname) {
		return HS_Util.loadValueStringArray(idoc, iname);
	}

}
