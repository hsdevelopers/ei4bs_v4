package com.healthspace.ei.controller;

import java.util.ArrayList;
import java.util.Date;

import com.healthspace.ei.model.PSReportModel;
import com.healthspace.general.HS_Util;

import controller.base;

public class PSReportControl extends com.healthspace.ei.controller.base {

	private static final long serialVersionUID = 1L;

	private boolean readOnly;
	private boolean valid;
	private boolean loaded;
	private boolean newdoc;
	private boolean saved;
	private ArrayList<String> messages;
	private boolean hasMessages;
	private boolean hasSubmitErrors;

	private String sampleKey;

	private PSReportModel currentReport;

	public PSReportControl() {
		sampleKey = "";
		messages = new ArrayList<String>();
		currentReport = new PSReportModel();
	}

	public void init() {
		sampleKey = "";
		messages = new ArrayList<String>();
		currentReport = new PSReportModel();
		if ( !base.getParam("sampleKey").isEmpty() ) {
			sampleKey = base.getParam("sampleKey");
			this.loadPSReport();
		}

	}

	public boolean loadPSReport() {
		return loadPSReport(false);
	}

	public boolean loadPSReport(boolean autoedit) {
		boolean result = false;
		messages = new ArrayList<String>();

		final boolean localDebug = HS_Util.isDebugServer();
		final String msgContext = HS_Util.debugCalledFrom() + ".loadPSReport";

		if ( localDebug ) HS_Util.debug("Searching for sample with key:[" + sampleKey + "]", "debug", msgContext);

		this.loaded = false;

		if ( "".equals(sampleKey) ) {
			messages.add("Please enter a sample key before clicking search...");
			result = false;
		} else {
			// currentReport = new PSReportModel();
			result = currentReport.load(sampleKey);
			if ( result ) {
				this.loaded = true;
				this.readOnly = (autoedit) ? false : true;

			} else {
				messages.add("Failed to load the sample report");
				if ( !"".equals(currentReport.getLastError()) ) {
					messages.add(currentReport.getLastError());
				}
			}
		}
		return result;
	}

	public boolean switchToEdit() {
		if ( this.readOnly ) {
			this.setReadOnly(false);
			this.currentReport.setLastError("");
			this.messages = new ArrayList<String>();
		}
		return false;
	}

	public boolean submit() {
		boolean result = false;

		hasSubmitErrors = false;

		messages = new ArrayList<String>();
		// messages.add("Submit not yet implemented.");
		// hasSubmitErrors = true;

		if ( !loaded ) {
			messages.add("No report is loaded so you cannot submit at this time..");
			hasSubmitErrors = true;
			return result;
		}
		if ( readOnly ) {
			messages.add("Report is not in edit mode so you cannot submit at this time..");
			hasSubmitErrors = true;
			return result;
		}

		try {
			// messages.add("unid is: [" + currentReport.getUnid() + "]");
			if ( !this.validate() ) {
				result = false;
				hasSubmitErrors = true;
				messages.add("Validation failure!");
			} else if ( currentReport.save() ) {
				messages.add("report saved.");
				readOnly = true;
				result = true;
			} else {
				messages.add(currentReport.getLastError());
				hasSubmitErrors = true;
			}
			return result;
		} catch (Exception e) {
			messages.add("report save failed with error: " + e.toString());
			hasSubmitErrors = true;
			return false;
		}
	}

	private boolean validate() {
		boolean result = true;

		// Date field rules - Rules to incorporate:
		// 1. no date fields can accept future values
		// 2. Date received cannot be before delivery date.
		// 3. Date reported cannot be before Date received.
		Date today = new Date();
		Date wdt = null;

		// 1. no date fields can accept future values
		if ( !"".equals(currentReport.getDateDeliveryStr()) ) {
			wdt = currentReport.getDateDelivery();
			if ( wdt.getTime() > today.getTime() ) {
				result = false;
				messages.add("Delivery date may not be in the future.");
			}
		}
		if ( !"".equals(currentReport.getDateReceivedStr()) ) {
			wdt = currentReport.getDateReceived();
			if ( wdt.getTime() > today.getTime() ) {
				result = false;
				messages.add("Received date may not be in the future.");
			}
		}
		if ( !"".equals(currentReport.getDateReportedStr()) ) {
			wdt = currentReport.getDateReported();
			if ( wdt.getTime() > today.getTime() ) {
				result = false;
				messages.add("Reported date may not be in the future.");
			}
		}

		Date wdt1 = null;
		Date wdt2 = null;

		// 2. Date received cannot be before delivery date.
		if ( !"".equals(currentReport.getDateDeliveryStr()) && !"".equals(currentReport.getDateReceivedStr()) ) {
			wdt1 = currentReport.getDateReceived();
			wdt2 = currentReport.getDateDelivery();
			if ( wdt1.getTime() < wdt2.getTime() ) {
				result = false;
				messages.add("Date received cannot be before delivery date.");
			}
		}

		// 3. Date reported cannot be before date received.
		if ( !"".equals(currentReport.getDateReportedStr()) && !"".equals(currentReport.getDateReceivedStr()) ) {
			wdt1 = currentReport.getDateReported();
			wdt2 = currentReport.getDateReceived();
			if ( wdt1.getTime() < wdt2.getTime() ) {
				result = false;
				messages.add("Date reported cannot be before date received.");
			}
		}
		return result;
	}

	public PSReportModel getCurrentReport() {
		return currentReport;
	}

	public ArrayList<String> getMessages() {
		return messages;
	}

	public String getSampleKey() {
		return sampleKey;
	}

	public boolean isHasMessages() {
		if ( this.messages.size() == 0 ) {
			hasMessages = false;
		} else if ( "".equals(this.messages.get(0)) ) {
			hasMessages = false;
		} else
			hasMessages = true;
		return hasMessages;
	}

	public boolean isLoaded() {
		if ( currentReport == null ) {
			loaded = false;
		}
		return loaded;
	}

	public boolean isNewdoc() {
		return newdoc;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public boolean isSaved() {
		return saved;
	}

	public boolean isValid() {
		if ( "".equals(sampleKey) ) {
			valid = true;
		} else if ( currentReport == null ) {
			valid = false;
		}
		return valid;
	}

	public void setCurrentReport(PSReportModel currentReport) {
		this.currentReport = currentReport;
	}

	public void setHasMessages(boolean hasMessages) {
		this.hasMessages = hasMessages;
	}

	public void setLoaded(boolean loaded) {
		this.loaded = loaded;
	}

	public void setMessages(ArrayList<String> messages) {
		this.messages = messages;
	}

	public void setNewdoc(boolean newdoc) {
		this.newdoc = newdoc;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public void setSampleKey(String sampleKey) {
		this.sampleKey = sampleKey;
	}

	public void setSaved(boolean saved) {
		this.saved = saved;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public boolean isHasSubmitErrors() {
		return hasSubmitErrors;
	}

	public void setHasSubmitErrors(boolean hasSubmitErrors) {
		this.hasSubmitErrors = hasSubmitErrors;
	}
}
