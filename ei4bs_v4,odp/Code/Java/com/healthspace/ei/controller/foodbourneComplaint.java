package com.healthspace.ei.controller;

import java.util.ArrayList;

import com.healthspace.ei.FoodbourneComplaint;
import com.healthspace.general.HS_Util;

import controller.base;

public class foodbourneComplaint extends com.healthspace.ei.controller.base {

	private static final long serialVersionUID = 1L;

	private boolean readOnly;
	private boolean valid;
	private boolean newdoc;
	private boolean saved;
	private ArrayList<String> messages;
	private boolean hasMessages;

	private FoodbourneComplaint currentComplaint;

	public foodbourneComplaint() {
		// empty constructor
		this.readOnly = true;
	}

	public void pageInit() {
		try {
			this.messages = new ArrayList<String>();
			this.messages.add("");

			this.currentComplaint = new FoodbourneComplaint();
			if (base.getParam("unique").isEmpty()) {
				// No key. Create a new Project
				if (this.isDebug()) this.debug("Create FoodbourneComplaint");
				this.currentComplaint.create();
				this.readOnly = false;
				this.newdoc = true;
			} else {
				if (this.isDebug()) this.debug("Loading FoodbourneComplaint - Key = " + base.getParam("unique"));
				this.currentComplaint.load(base.getParam("unique"));
				this.readOnly = true;
				this.newdoc = false;
			}

			this.valid = this.currentComplaint.isValid();
		} catch (Exception e) {
			this.valid = false;
			HS_Util.debug(e.toString(), "error", "foodbourneComplaint.pageInit");
		}
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public boolean isValid() {
		return valid;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public boolean isNewdoc() {
		return newdoc;
	}

	public FoodbourneComplaint getCurrentComplaint() {
		return currentComplaint;
	}

	public void setNewdoc(boolean newdoc) {
		this.newdoc = newdoc;
	}

	public void setCurrentComplaint(FoodbourneComplaint currentComplaint) {
		this.currentComplaint = currentComplaint;
	}

	public boolean submit() {
		if (!this.validate()) {
			// TODO signal that the validation failed
			return false;
		}
		if (this.currentComplaint.save()) {
			this.setNewdoc(false);
			this.setSaved(true);
			this.addMessage("Document saved.");
			return true;
		}
		return false;
	}

	private boolean validate() {
		boolean result = true;

		// TODO Auto-generated method stub

		return result;
	}

	public void addMessage(String msg) {
		if ("".equals(messages.get(0))) messages.remove(0);
		this.messages.add(msg);
	}

	public void clearMessages() {
		this.messages = new ArrayList<String>();
		this.messages.add("");
	}

	public ArrayList<String> getMessages() {
		return messages;
	}

	public void setMessages(ArrayList<String> messages) {
		this.messages = messages;
	}

	public boolean isHasMessages() {
		hasMessages = !("".equals(messages.get(0)));
		// HS_Util.debug("messages.get(0):[" + messages.get(0) + "] hasMessages:[" + hasMessages + "]", "debug",
		// "foodbourneComplaint.isHasMessage");
		return hasMessages;
	}

	public void setHasMessages(boolean hasMessages) {
		this.hasMessages = hasMessages;
	}

	public boolean isSaved() {
		return saved;
	}

	public void setSaved(boolean saved) {
		this.saved = saved;
	}

}
