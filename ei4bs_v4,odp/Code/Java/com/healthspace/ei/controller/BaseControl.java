package com.healthspace.ei.controller;

import java.util.ArrayList;

import com.healthspace.ei.model.PayInvoiceModel;

import controller.base;

public class BaseControl extends com.healthspace.ei.controller.base {

	private static final long serialVersionUID = 1L;

	private boolean readOnly;
	private boolean valid;
	private boolean loaded;
	private boolean newdoc;
	private boolean saved;
	private ArrayList<String> messages;
	private boolean hasMessages;
	private boolean hasSubmitErrors;

	private String invoiceKey;

	private PayInvoiceModel currentInvoice;

	public BaseControl() {
		invoiceKey = "";
		messages = new ArrayList<String>();
		currentInvoice = new PayInvoiceModel();
	}

	public void init() {
		invoiceKey = "";
		messages = new ArrayList<String>();
		currentInvoice = new PayInvoiceModel();
		if ( !base.getParam("invoice").isEmpty() ) {
			invoiceKey = base.getParam("invoice");
			this.loadPayInvoice();
		}

	}

	private void loadPayInvoice() {
		// TODO Auto-generated method stub

	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public boolean isLoaded() {
		return loaded;
	}

	public void setLoaded(boolean loaded) {
		this.loaded = loaded;
	}

	public boolean isNewdoc() {
		return newdoc;
	}

	public void setNewdoc(boolean newdoc) {
		this.newdoc = newdoc;
	}

	public boolean isSaved() {
		return saved;
	}

	public void setSaved(boolean saved) {
		this.saved = saved;
	}

	public ArrayList<String> getMessages() {
		return messages;
	}

	public void setMessages(ArrayList<String> messages) {
		this.messages = messages;
	}

	public boolean isHasMessages() {
		if ( this.messages.size() == 0 ) {
			hasMessages = false;
		} else if ( "".equals(this.messages.get(0)) ) {
			hasMessages = false;
		} else
			hasMessages = true;
		return hasMessages;
	}

	public void setHasMessages(boolean hasMessages) {
		this.hasMessages = hasMessages;
	}

	public boolean isHasSubmitErrors() {
		return hasSubmitErrors;
	}

	public void setHasSubmitErrors(boolean hasSubmitErrors) {
		this.hasSubmitErrors = hasSubmitErrors;
	}

	public String getInvoiceKey() {
		return invoiceKey;
	}

	public void setInvoiceKey(String invoiceKey) {
		this.invoiceKey = invoiceKey;
	}

	public PayInvoiceModel getCurrentInvoice() {
		return currentInvoice;
	}

	public void setCurrentInvoice(PayInvoiceModel currentInvoice) {
		this.currentInvoice = currentInvoice;
	}
}
