package com.healthspace.ei.controller;

import java.util.ArrayList;
import java.util.Vector;

import com.healthspace.ei.model.ComplaintModel;
import com.healthspace.general.HS_Util;

public class ComplaintControl extends com.healthspace.ei.controller.base {

	private static final long	serialVersionUID	= 1L;

	private boolean				readOnly;
	private boolean				valid;
	private boolean				loaded;
	private boolean				newdoc;
	private boolean				saved;
	private ArrayList<String>	messages;
	private boolean				hasMessages;
	private boolean				hasSubmitErrors;

	private String				documentId;

	private ComplaintModel		currentComplaint;

	public ComplaintControl() {
	}

	public void addMockData() {
		currentComplaint.addMockData();
	}

	public ComplaintModel getCurrentComplaint() {
		return currentComplaint;
	}

	public String getDocumentId() {
		return documentId;
	}

	public ArrayList<String> getMessages() {
		if ( messages == null ) messages = new ArrayList<String>();
		return messages;
	}

	public void init() {
		final String msgContext = this.getClass().getName() + ".init";
		final boolean localDebug = ("Yes".equals(HS_Util.getAppSettingString("debug_complaints")));
		try {
			documentId = "";
			messages = new ArrayList<String>();
			if ( currentComplaint == null ) currentComplaint = new ComplaintModel();
			if ( !base.getParam("documentId").isEmpty() ) {
				documentId = base.getParam("documentId");
				this.loadDocument(documentId);
				if ( localDebug ) HS_Util.debug("ComplaintIntake init with id: " + this.currentComplaint.getDocumentID(), "debug", msgContext);
			} else {
				if ( localDebug ) HS_Util.debug("ComplaintIntake init with no document id parameter.", "debug", msgContext);
				if ( "".equals(this.currentComplaint.getDocumentID()) ) {
					documentId = "";
					this.loadDocument(documentId);
				}
				if ( !base.getParam("type").isEmpty() ) {
					this.currentComplaint.setType(base.getParam("type"));
					if ( localDebug ) HS_Util.debug("ComplaintIntake init with type parameter found:[" + base.getParam("type") + "]", "debug", msgContext);
				}
			}
		} catch (Exception e) {
			System.out.print("Error " + e.toString() + " in ComplaintControl.init()");
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}
	}

	public Object[] getAnimalList(String dtypes) {
		return this.getAnimalList(dtypes.split(";"));
	}

	public Object[] getAnimalList(String[] dtypes) {
		final String msgContext = this.getClass().getName() + ".getAnimalList";
		final boolean localDebug = false;
		Vector<String> outTypes = new Vector<String>();
		try {
			for (String dt : dtypes) {
				if ( !"".equals(dt) ) outTypes.add(dt);
			}

			String[] curTypes = this.currentComplaint.getAnimalType().split(";");
			if ( localDebug ) HS_Util.debug("Current Animal Type is:[" + this.currentComplaint.getAnimalType() + "]", "debug", msgContext);
			for (String ct : curTypes) {
				if ( !outTypes.contains(ct) ) {
					if ( !"".equals(ct) ) outTypes.add(ct);
				}
			}
			return outTypes.toArray();
		} catch (Exception e) {
			outTypes.add("Error");
			HS_Util.debug(e.toString(), "error", msgContext);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}
		return null;
	}

	public boolean isHasMessages() {
		if ( this.messages.size() == 0 ) {
			hasMessages = false;
		} else if ( "".equals(this.messages.get(0)) ) {
			hasMessages = false;
		} else
			hasMessages = true;
		return hasMessages;
	}

	public boolean isHasSubmitErrors() {
		return hasSubmitErrors;
	}

	public boolean isLoaded() {
		return loaded;
	}

	public boolean isNewdoc() {
		return newdoc;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public boolean isSaved() {
		return saved;
	}

	public boolean isValid() {
		return valid;
	}

	private void loadDocument(String id) {
		final String msgContext = this.getClass().getName() + ".loadDocument";
		final boolean localDebug = ("Yes".equals(HS_Util.getAppSettingString("debug_complaints")));

		if ( "".equals(id) ) {
			if ( localDebug ) HS_Util.debug("Loading new complaint", "debug", msgContext);
			currentComplaint.loadNew();
			this.loaded = true;
			this.newdoc = true;
			this.valid = true;
			this.saved = false;
			this.readOnly = false;
		} else {
			if ( localDebug ) HS_Util.debug("Loading exisiting complaintwith id:[" + id + "]", "debug", msgContext);
			if ( currentComplaint.load(id) ) {
				this.loaded = true;
				this.newdoc = false;
				this.valid = true;
				this.saved = false;
				this.readOnly = true;
			} else {
				this.valid = false;
			}
		}

	}

	public void setCurrentComplaint(ComplaintModel currentComplaint) {
		this.currentComplaint = currentComplaint;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public void setHasMessages(boolean hasMessages) {
		this.hasMessages = hasMessages;
	}

	public void setHasSubmitErrors(boolean hasSubmitErrors) {
		this.hasSubmitErrors = hasSubmitErrors;
	}

	public void setLoaded(boolean loaded) {
		this.loaded = loaded;
	}

	public void setMessages(ArrayList<String> messages) {
		this.messages = messages;
	}

	public void setNewdoc(boolean newdoc) {
		this.newdoc = newdoc;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public void setSaved(boolean saved) {
		this.saved = saved;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public boolean submit() {
		final String msgContext = this.getClass().getName() + ".submit";
		final boolean localDebug = true;
		this.clearMessages();

		if ( !this.validate() ) {
			// TODO signal that the validation failed
			if ( localDebug ) HS_Util.debug("Validation failed", "debug", msgContext);
			return false;
		}
		try {
			if ( localDebug ) HS_Util.debug("Saving the complaint", "debug", msgContext);
			if ( this.currentComplaint.save() ) {
				if ( localDebug ) HS_Util.debug("complaintSave - Form:[" + this.currentComplaint.getForm() + "] unid:[" + this.currentComplaint.getUnid() + "]", "debug", msgContext);
				this.setNewdoc(false);
				this.setSaved(true);
				this.addMessage("Document saved.");
				return true;
			} else {
				this.addMessage("Error occurred during save!");
				this.hasSubmitErrors = true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			HS_Util.debug(e.toString(), "error", msgContext);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}
		return false;
	}

	private boolean validate() {
		boolean result = true;
		this.setHasSubmitErrors(false);

		if ( "".equals(this.currentComplaint.getType()) || "<select>".equals(this.currentComplaint.getType()) ) {
			addMessage("Please select a type of complaint.");
			result = false;
		}
		if ( !"Yes".equals(this.currentComplaint.getAnonymousInput()) ) {
			if ( "".equals(this.currentComplaint.getComplainantName()) ) {
				addMessage("Please provide your name and contact information.");
				result = false;
			} else if ( "".equals(this.currentComplaint.getComplainantPhone()) && "".equals(this.currentComplaint.getComplainantEveningPhone()) && "".equals(this.currentComplaint.getComplainantEmail()) ) {
				addMessage("Please provide a phone number or email address.");
				result = false;
			}
		}

		if ( "Animal Welfare".equals(this.currentComplaint.getType()) ) {
			if ( "".equals(this.currentComplaint.getAnimalType()) ) {
				addMessage("Please enter an Animal Type.");
				result = false;
			}
			if ( "".equals(this.currentComplaint.getCountyIncident()) || "<select>".equals(this.currentComplaint.getCountyIncident()) ) {
				addMessage("Please enter a County of Incident.");
				result = false;
			}
			if ( "".equals(this.currentComplaint.getAbuseDateStr()) ) {
				addMessage("Please enter a Date Abuse Witnessed.");
				result = false;
			}
		}

		if ( "".equals(this.currentComplaint.getDetails()) ) {
			addMessage("Please provide details of your complaint");
			result = false;
		}
		this.setHasSubmitErrors(!result);
		return result;
	}

	public void addMessage(String msg) {
		if ( messages == null ) clearMessages();
		if ( messages.size() == 0 ) messages.add("");
		if ( "".equals(messages.get(0)) ) messages.remove(0);
		this.messages.add(msg);
	}

	public void clearMessages() {
		this.messages = new ArrayList<String>();
		this.messages.add("");
	}
}
