package com.healthspace.ei.controller;

import java.util.ArrayList;
import java.util.Vector;

import com.healthspace.ei.model.PermitAppModel;
import com.healthspace.ei.model.Reference;
import com.healthspace.general.HS_Util;
import com.healthspace.tools.JSFUtil;

import controller.base;

public class PermitAppControl extends base {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private boolean readOnly;
	private boolean valid;
	private boolean loaded;
	private boolean newdoc;
	private boolean saved;
	private ArrayList<String> messages;
	private boolean hasMessages;
	private boolean hasSubmitErrors;

	private boolean appInfoSaved;

	private Vector<String> onlineApplicaitonTypes;
	private String locationType;
	private String locationID;
	private boolean locationLoaded;
	private boolean locationSaved;

	private String ownerType;
	private String ownerID;
	private boolean ownerLoaded;
	private boolean ownerSaved;

	private boolean refsEditable;

	private PermitAppModel currentPermitApp;

	public PermitAppControl() {

		currentPermitApp = new PermitAppModel();
	}

	public String cancelPermit() {
		final boolean localDebug = true;
		final String msgContext = this.getClass().getName() + ".cancelPermit";
		String retUrl = "";
		messages = new ArrayList<String>();

		if ( this.currentPermitApp == null ) {
			messages.add("No permit application loaded so cannot be cancelled.");
		} else if ( this.newdoc ) {
			messages.add("Cancel permit not needed when permit is not saved.");
		} else {
			this.currentPermitApp.setStatus("Cancelled");
			try {
				if ( !currentPermitApp.save() ) {
					messages.add("Failed to save the current Permit applicaition document!");
					if ( localDebug ) HS_Util.debug("Failed to save the current Permit applicaition document!", "warn", msgContext);
				} else {
					this.newdoc = false;
					retUrl = JSFUtil.getUrl();
					retUrl = retUrl.substring(0, retUrl.toLowerCase().indexOf(".nsf")) + ".nsf/formPermitAppCancelled.xsp";
					// if ( localDebug ) HS_Util.debug("retUrl Step 2:[" + retUrl + "]", "debug", msgContext);
					retUrl += "?open&unid=" + currentPermitApp.getUnid();
					// if ( localDebug ) HS_Util.debug("retUrl Step 3:[" + retUrl + "]", "debug", msgContext);

					if ( localDebug ) HS_Util.debug("Cancel done. URL is:[" + retUrl + "]", "debug", msgContext);
					return retUrl;
				}
			} catch (Exception e) {
				messages.add(e.toString());
				if ( localDebug ) HS_Util.debug("ERROR: " + e.toString(), "error", msgContext);
				if ( HS_Util.isDebugServer() ) e.printStackTrace();
			}
		}

		hasSubmitErrors = (messages.size() == 0) ? false : ("".equals(messages.get(0))) ? false : true;

		return "";
	}

	public void clearAppInfo() {
		appInfoSaved = false;
	}

	public void clearLocation() {
		locationLoaded = false;
		locationSaved = false;
		currentPermitApp.clearLocaction();
		this.locationID = "";
		this.locationType = "";
	}

	public void clearOwner() {
		ownerLoaded = false;
		ownerSaved = false;
		currentPermitApp.clearOwner();
		this.ownerID = "";
		this.ownerType = "";
	}

	public void copyLocation2Owner() {
		this.ownerLoaded = currentPermitApp.copyLocation2Owner();
	}

	public PermitAppModel getCurrentPermitApp() {
		return currentPermitApp;
	}

	public String getLocationID() {
		return locationID;
	}

	public String getLocationType() {
		return locationType;
	}

	public ArrayList<String> getMessages() {
		return messages;
	}

	public String getOwnerID() {
		return ownerID;
	}

	public String getOwnerType() {
		return ownerType;
	}

	public void init() {
		final String msgContext = this.getClass().getName() + ".init";
		// final boolean localDebug = ("Yes".equals(HS_Util.getAppSettingString("debug_permitApp")));
		final boolean localDebug = false;
		loaded = true;
		valid = false;
		newdoc = true; // might not be based on URL parameter.
		saved = false;

		this.setLocationType("");
		appInfoSaved = false;
		locationID = "";
		locationLoaded = false;
		locationSaved = false;
		ownerID = "";
		ownerLoaded = false;
		ownerSaved = false;
		messages = new ArrayList<String>();

		if ( !(base.getParam("unid").isEmpty()) && !"".equals(currentPermitApp.getUnid()) ) {
			if ( localDebug )
				HS_Util.debug("In init but the doc with unid:[" + currentPermitApp.getUnid() + "] already seems to be loaded!", "warn", msgContext);
		} else if ( !(base.getParam("unid").isEmpty()) ) {
			currentPermitApp.setUnid(base.getParam("unid"));
			// if ( localDebug ) HS_Util.debug("Loading doc with unid:[" + currentPermitApp.getUnid() + "]", "debug", msgContext);
			if ( currentPermitApp.loadPermitApp() ) {
				newdoc = false;
				appInfoSaved = true;

				locationLoaded = !("".equals(currentPermitApp.getLocationName()));
				locationSaved = !("".equals(currentPermitApp.getLocationName()));

				if ( locationLoaded ) {
					locationType = ("".equals(currentPermitApp.getLocationUnid())) ? "new" : "existing";
					locationID = currentPermitApp.getLocationID();
				}
				// if ( localDebug )
				// HS_Util.debug("locationLoaded:[" + locationLoaded + "] locationType:[" + locationType + "] locationID:[" + locationID + "] ", "debug",
				// msgContext);
				ownerLoaded = !("".equals(currentPermitApp.getOwnerName()));
				ownerSaved = !("".equals(currentPermitApp.getOwnerName()));

				if ( ownerLoaded ) {
					ownerType = ("".equals(currentPermitApp.getOwnerUnid())) ? "new" : "existing";
					ownerID = currentPermitApp.getOwnerID();
				}
				// if ( localDebug )
				// HS_Util.debug("ownerLoaded:[" + ownerLoaded + "] ownerType:[" + ownerType + "] ownerID:[" + ownerID + "] ", "debug", msgContext);
			} else {
				if ( "Cancelled".equals(currentPermitApp.getStatus()) ) {
					messages.add("The application with this key is cancelled.");
				} else {
					messages.add("Unable to reload the permit application with the id:[" + currentPermitApp.getUnid() + "]");
					hasSubmitErrors = true;
				}
			}
		} else {
			currentPermitApp.setStatus("Draft");

			// Development
			locationID = "NFRY-A9LVAM";
			ownerID = "NFRY-A9G5FY";
			currentPermitApp.setApplicantEmail("tester@henrynewberry.com");
			currentPermitApp.setApplicantName("");
			currentPermitApp.setApplicantPhone("604-555-1212");
		}
	}

	public boolean isAppInfoSaved() {
		return appInfoSaved;
	}

	public boolean isHasMessages() {
		// return hasMessages;
		if ( this.messages.size() == 0 ) {
			hasMessages = false;
		} else if ( "".equals(this.messages.get(0)) ) {
			hasMessages = false;
		} else
			hasMessages = true;
		return hasMessages;
	}

	public boolean isHasSubmitErrors() {
		return hasSubmitErrors;
	}

	public boolean isLoaded() {
		return loaded;
	}

	public boolean isLocationLoaded() {
		return locationLoaded;
	}

	public boolean isLocationSaved() {
		return locationSaved;
	}

	public boolean isNewdoc() {
		return newdoc;
	}

	public boolean isOwnerLoaded() {
		return ownerLoaded;
	}

	public boolean isOwnerSaved() {
		return ownerSaved;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public boolean isSaved() {
		return saved;
	}

	public boolean isValid() {
		return valid;
	}

	public void loadLocation() {
		messages = new ArrayList<String>();
		hasSubmitErrors = false;
		locationLoaded = false;
		messages.add("Searching for location with id:[" + this.locationID + "]");
		try {
			if ( currentPermitApp.findLocation(locationID) ) {
				locationLoaded = true;
			} else {
				messages.add("Unable to locate location with id:[" + this.locationID + "]");
			}
		} catch (Exception e) {
			messages.add("ERROR: " + e.toString());
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			hasSubmitErrors = true;
		}
	}

	public void loadOwner() {
		messages = new ArrayList<String>();
		hasSubmitErrors = false;
		ownerLoaded = false;
		messages.add("Searching for owner with id:[" + this.ownerID + "]");
		try {
			if ( currentPermitApp.findOwner(ownerID) ) {
				ownerLoaded = true;
			} else {
				messages.add("Unable to locate owner with id:[" + this.ownerID + "]");
			}
		} catch (Exception e) {
			messages.add("ERROR: " + e.toString());
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			hasSubmitErrors = true;
		}
	}

	public String savePermit() {
		final String msgContext = this.getClass().getName() + ".savePermit";
		// final boolean localDebug = false;
		final boolean localDebug = ("Yes".equals(HS_Util.getAppSettingString("debug_permitApp")));

		messages = new ArrayList<String>();
		String retUrl = "";
		this.valid = validate("save");

		if ( !this.valid ) {
			if ( localDebug ) HS_Util.debug("Validation failed messages.size():[" + messages.size() + "]", "debug", msgContext);
		} else {
			try {
				if ( "new".equals(getLocationType()) && ("".equals(currentPermitApp.getLocationID())) ) {
					currentPermitApp.setLocationID((String) HS_Util.getSession().evaluate("@Unique").get(0));
				}
				if ( !currentPermitApp.save() ) {
					messages.add("Failed to save the current Permit applicaition document!");
					if ( localDebug ) HS_Util.debug("Failed to save the current Permit applicaition document!", "warn", msgContext);
				} else {
					this.newdoc = false;
					appInfoSaved = true;
					locationSaved = !("".equals(currentPermitApp.getLocationName()));
					ownerLoaded = !("".equals(currentPermitApp.getOwnerName()));
					ownerSaved = !("".equals(currentPermitApp.getOwnerName()));
					retUrl = JSFUtil.getUrl();
					// if ( localDebug ) HS_Util.debug("retUrl Step 1:[" + retUrl + "]", "debug", msgContext);
					retUrl = retUrl.substring(0, retUrl.toLowerCase().indexOf(".nsf")) + ".nsf/formPermitAppSaved.xsp";
					// if ( localDebug ) HS_Util.debug("retUrl Step 2:[" + retUrl + "]", "debug", msgContext);
					retUrl += "?open&unid=" + currentPermitApp.getUnid();
					// if ( localDebug ) HS_Util.debug("retUrl Step 3:[" + retUrl + "]", "debug", msgContext);

					// if ( localDebug ) HS_Util.debug("Save done. URL is:[" + retUrl + "]", "debug", msgContext);
				}
			} catch (Exception e) {
				messages.add(e.toString() + " e1");
				HS_Util.debug("ERROR: " + e.toString(), "error", msgContext);
				if ( HS_Util.isDebugServer() ) e.printStackTrace();
			}
		}
		hasSubmitErrors = (messages.size() == 0) ? false : ("".equals(messages.get(0))) ? false : true;

		if ( localDebug ) HS_Util.debug("Returning URL:[" + retUrl + "]", "debug", msgContext);
		return retUrl;
	}

	public void setAppInfoSaved(boolean appInfoSaved) {
		this.appInfoSaved = appInfoSaved;
	}

	public void setCurrentPermitApp(PermitAppModel currentPermitApp) {
		this.currentPermitApp = currentPermitApp;
	}

	public void setHasMessages(boolean hasMessages) {
		this.hasMessages = hasMessages;
	}

	public void setHasSubmitErrors(boolean hasSubmitErrors) {
		this.hasSubmitErrors = hasSubmitErrors;
	}

	public void setLoaded(boolean loaded) {
		this.loaded = loaded;
	}

	public void setLocationID(String locationID) {
		this.locationID = locationID;
	}

	public void setLocationLoaded(boolean locationLoaded) {
		this.locationLoaded = locationLoaded;
	}

	public void setLocationSaved(boolean locationSaved) {
		this.locationSaved = locationSaved;
	}

	public void setLocationType(String locationType) {
		// System.out.print(this.getClass().getName() + ".locationType set to:[" + locationType + "]");
		this.locationType = locationType;
	}

	public void setMessages(ArrayList<String> messages) {
		this.messages = messages;
	}

	public void setNewdoc(boolean newdoc) {
		this.newdoc = newdoc;
	}

	public void setOwnerID(String ownerID) {
		this.ownerID = ownerID;
	}

	public void setOwnerLoaded(boolean ownerLoaded) {
		this.ownerLoaded = ownerLoaded;
	}

	public void setOwnerSaved(boolean ownerSaved) {
		this.ownerSaved = ownerSaved;
	}

	public void setOwnerType(String ownerType) {
		this.ownerType = ownerType;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public void setSaved(boolean saved) {
		this.saved = saved;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public String submitPermit() {
		messages = new ArrayList<String>();
		this.valid = validate("save");

		messages.add("submitPermit not fully implemented.");

		hasSubmitErrors = (messages.size() == 0) ? false : ("".equals(messages.get(0))) ? false : true;

		return "";
	}

	private boolean validate(String mode) {
		if ( "".equals(mode) ) return false;

		boolean result = currentPermitApp.validate(mode, messages);
		return result;
	}

	public Vector<String> getOnlineApplicaitonTypes() {
		if ( onlineApplicaitonTypes == null ) {
			onlineApplicaitonTypes = HS_Util.getKeywordValues("OnlineApplicationTypes");
		}
		return onlineApplicaitonTypes;
	}

	public void setOnlineApplicaitonTypes(Vector<String> onlineApplicaitonTypes) {
		this.onlineApplicaitonTypes = onlineApplicaitonTypes;
	}

	public void setRefsEditable(boolean refsEditable) {
		this.refsEditable = refsEditable;
	}

	public boolean isRefsEditable() {
		return refsEditable;
	}

	public void addReferences() {
		this.setRefsEditable(true);
		Reference ref = new Reference();
		this.currentPermitApp.getReferences().add(ref);
	}

	public void editReferences() {
		this.setRefsEditable(true);
		while (this.currentPermitApp.getReferences().size() < 1) {
			Reference ref = new Reference();
			this.currentPermitApp.getReferences().add(ref);
		}
	}

	public void removeReference(int refidx) {
		final String msgContext = this.getClass().getName() + ".removeReference";
		// final boolean localDebug = true;
		final boolean localDebug = ("Yes".equals(HS_Util.getAppSettingString("debug_permitApp")));
		if ( localDebug ) HS_Util.debug("removing refence:[" + refidx + "]", "debug", msgContext);
		try {
			Vector<Reference> crefs = this.currentPermitApp.getReferences();
			Vector<Reference> out = new Vector<Reference>();
			for (int i = 0; i < crefs.size(); i++) {
				if ( i != refidx ) out.add(crefs.get(i));
			}
			this.currentPermitApp.setReferences(out);
		} catch (Exception e) {
			HS_Util.debug("Error: " + e.toString(), "error", msgContext);
		}
	}

}
