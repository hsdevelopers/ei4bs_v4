package com.healthspace.ei.controller;

import java.io.Serializable;

import com.healthspace.general.HS_Util;

@SuppressWarnings("serial")
public class base implements Serializable {

	public void print(String msg) {
		HS_Util.print(msg);
	}

	public void debug(String msg) {
		HS_Util.debug(msg, "debug", HS_Util.debugCalledFrom());
	}

	public static String getParam(String key) {
		return HS_Util.getParam(key);
	}

	public boolean isDebug() {
		return HS_Util.isDebugger() && HS_Util.isDebugServer();
	}
}
