package com.healthspace.ei;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.View;

import com.healthspace.general.HS_Util;
import com.healthspace.general.HS_database;
import com.healthspace.rest.HS_JsonWriter;
import com.healthspace.rest.RestUtil;
import com.healthspace.tools.JSFUtil;

public class PayInvoicePostback implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String logMsgs;

	public PayInvoicePostback() {

	}

	@SuppressWarnings("unchecked")
	public String dopost() {
		String msgContext = "PostPayInvoicePostback.dopost";
		StringWriter sw = new StringWriter();
		HS_JsonWriter jw = new HS_JsonWriter(sw, false);
		Vector<String> pnames = new Vector<String>();
		Map<String, String> pmap = new HashMap();

		// int recordNum = -1;
		// int recordCount = -1;
		// String batch = "";
		Date sdt = new Date();

		try {
			jw.startObject();
			jw.addProperty("xsource", msgContext);
			jw.addProperty("version", "2016-06-16-005");
			jw.addProperty("isDebugOn", ("Yes".equals(HS_Util.getAppSettingString("debug_payInvoicePostBack")) ? "true" : "false"));
			jw.addProperty("debug_payInvoicePostBack", HS_Util.getAppSettingString("debug_payInvoicePostBack"));

			HttpServletRequest req = (HttpServletRequest) JSFUtil.getFacesContext().getExternalContext().getRequest();

			for (Enumeration<String> e = req.getParameterNames(); e.hasMoreElements();) {
				String ename = e.nextElement();
				pnames.add(ename);
			}
			Collections.sort(pnames, Sort_String_Vector_Order);
			// if ( "Yes".equals(HS_Util.getAppSettingString("debug_payInvoicePostBack")) ) jw.addProperty("parameterNames", pnames);
			// if ( "Yes".equals(HS_Util.getAppSettingString("debug_payInvoicePostBack")) ) System.out.print(msgContext + ": Processing post ");

			Iterator<String> pitr = pnames.iterator();
			if ( "Yes".equals(HS_Util.getAppSettingString("debug_payInvoicePostBack")) ) jw.startProperty("postedValues");
			if ( "Yes".equals(HS_Util.getAppSettingString("debug_payInvoicePostBack")) ) jw.startObject();
			while (pitr.hasNext()) {
				String pname = pitr.next();
				String pvalue = req.getParameter(pname);
				if ( "Yes".equals(HS_Util.getAppSettingString("debug_payInvoicePostBack")) ) jw.addProperty(pname, pvalue);
				pmap.put(pname, pvalue);
			}
			if ( "Yes".equals(HS_Util.getAppSettingString("debug_payInvoicePostBack")) ) jw.endObject();
			if ( "Yes".equals(HS_Util.getAppSettingString("debug_payInvoicePostBack")) ) jw.endProperty();

			this.processTheMap(pmap, jw);

			// if ( "Yes".equals(HS_Util.getAppSettingString("debug_payInvoicePostBack")) ) {
			if ( "Yes".equals(HS_Util.getAppSettingString("debug_payInvoicePostBack")) ) {
				this.logMsgs = RestUtil.addDebugProperties(jw, sw, null);
				this.logMsgs += "\n\n" + msgContext + " completed.\n\n" + HS_Util.emsecs(sdt);
				RestUtil.writeToAgentLog(this.logMsgs, msgContext);
			}
			jw.endObject();
			return sw.toString();
		} catch (IOException e) {
			// HS_Util.debug(e.toString(), "error", msgContext);
			return "{\"error\":\"" + e.toString() + " in " + msgContext + "\",\"status\":\"error\"}";
		} catch (Exception e1) {
			// HS_Util.debug(e1.toString(), "error", msgContext);
			if ( "Yes".equals(HS_Util.getAppSettingString("debug_payInvoicePostBack")) ) e1.printStackTrace();
			return "{\"error\":\"" + e1.toString() + " in " + msgContext + "\",\"status\":\"error\"}";
		}
	}

	private void processTheMap(Map<String, String> pmap, HS_JsonWriter jw) throws IOException {
		String msgContext = "PostPayInvoicePostback.processTheMap";
		Database paydb = null;
		View docidView = null;
		Document txdoc = null;
		try {
			if ( !pmap.containsKey("documentid") ) throw (new Exception("Post data missing the document id."));

			String docid = pmap.get("documentid");
			String up1 = pmap.get("UserPart1");
			// String up2 = pmap.get("UserPart2");
			// String up3 = pmap.get("UserPart3");
			String up4 = pmap.get("UserPart4");
			String up5 = pmap.get("UserPart5");
			// String up6 = pmap.get("UserPart6");

			HS_database hsdb = new HS_database();
			paydb = hsdb.getdbAsSigner("payments");
			if ( paydb == null ) throw (new Exception("Unable to access payments database"));
			if ( "Yes".equals(HS_Util.getAppSettingString("debug_payInvoicePostBack")) )
				jw.addProperty("paymentDB", paydb.getServer() + "!!" + paydb.getFilePath() + " access level:[" + paydb.getCurrentAccessLevel() + "]");

			docidView = paydb.getView("(DocumentIDLookup)");
			if ( docidView == null ) throw (new Exception("Unable to access view \"(DocumentIDLookup)\" on payments database"));

			txdoc = docidView.getFirstDocumentByKey(docid);
			if ( txdoc == null ) throw (new Exception("Unable to access document with key \"" + docid + "\" \"(DocumentIDLookup)\" on payments database"));

			// valdate the document
			if ( !txdoc.getItemValueString("UserPart1").equals(up1) ) throw (new Exception("Posted transaction does not match document! - billing name"));
			if ( !txdoc.getItemValueString("pmt_Invoice").equals(up4) ) throw (new Exception("Posted transaction does not match document! - pmt_Invoice"));
			if ( !txdoc.getItemValueString("InvoiceType").equals(up5) ) throw (new Exception("Posted transaction does not match document! - InvoiceType"));

			String ehsstatus = txdoc.getItemValueString("EHSStatus");
			jw.addProperty("startingStatus", ehsstatus);
			if ( !("Pending".equals(ehsstatus)) ) {
				jw.addProperty("status", "post skipped");
				return;
			}
			// Agent agt = paydb.getAgent("(TDAFISTransactionPoster)");
			// if ( agt == null ) agt = paydb.getAgent("TDAFISTransactionPoster");
			// if ( agt == null ) throw (new Exception("Failed to get handle to agent \"TDAFISTransactionPoster\"in payments database."));

			this.updateTxDocFromMap(txdoc, pmap, "AccountLast4");
			this.updateTxDocFromMap(txdoc, pmap, "PaymentDescription");
			this.updateTxDocFromMap(txdoc, pmap, "PaymentMethodCode");
			this.updateTxDocFromMap(txdoc, pmap, "TransactionDateStamp");
			this.updateTxDocFromMap(txdoc, pmap, "TransactionID");

			this.updateTxDocFromMap(txdoc, pmap, "BillingAddress");
			this.updateTxDocFromMap(txdoc, pmap, "BillingCity");
			this.updateTxDocFromMap(txdoc, pmap, "BillingCountry");
			this.updateTxDocFromMap(txdoc, pmap, "BillingEmail");
			this.updateTxDocFromMap(txdoc, pmap, "BillingFullName");
			this.updateTxDocFromMap(txdoc, pmap, "BillingPhone");
			this.updateTxDocFromMap(txdoc, pmap, "BillingState");
			this.updateTxDocFromMap(txdoc, pmap, "BillingZip");

			txdoc.replaceItemValue("EHSStatus", "Post in process");

			if ( !(txdoc.save(false, false)) ) {
				jw.addProperty("status", "error");
				jw.addProperty("error", "Failed to save the transaction document in payments database.");
				return;
			}
			jw.addProperty("status", "doc ready for agent");
			// String unid = txdoc.getUniversalID();
			// jw.addProperty("txdocunid", unid);
			// String noteid = txdoc.getNoteID();
			// jw.addProperty("txdocnoteid", noteid);

			// txdoc = null;

			// int agtresult = agt.runOnServer(noteid);
			// if ( agtresult != 0 ) {
			// jw.addProperty("status", "error");
			// String emsg = "Agent \"" + agt.getName() + "\" FAILED to run on the server \"" + paydb.getServer() + "\" : [" + agtresult + "]";
			// jw.addProperty("error", emsg);
			// txdoc = paydb.getDocumentByID(noteid);
			// txdoc.replaceItemValue("EHSStatus", "Error");
			// txdoc.replaceItemValue("Error", emsg);
			// txdoc.save(false, false);
			// return;
			// }

			// txdoc = paydb.getDocumentByUNID(unid);
			// txdoc = paydb.getDocumentByID(noteid);
			// ehsstatus = txdoc.getItemValueString("EHSStatus");
			//
			// jw.addProperty("postAgentStatus", ehsstatus);
			//
			// jw.addProperty("status", ehsstatus);
		} catch (Exception e1) {
			jw.addProperty("status", "error");
			jw.addProperty("error", e1.toString() + " in " + msgContext);
			if ( txdoc != null ) {
				txdoc.replaceItemValue("EHSStatus", "Error");
				txdoc.replaceItemValue("Error", e1.toString() + " in " + msgContext);
				txdoc.save(false, false);
			}
			if ( "Yes".equals(HS_Util.getAppSettingString("debug_payInvoicePostBack")) ) e1.printStackTrace();
			if ( "Yes".equals(HS_Util.getAppSettingString("debug_payInvoicePostBack")) ) RestUtil.sendAlert("ERROR: " + e1.toString() + " in " + msgContext);
		}
	}

	private void updateTxDocFromMap(Document txdoc, Map<String, String> pmap, String fname) {
		txdoc.replaceItemValue(fname, (pmap.keySet().contains(fname)) ? pmap.get(fname) : fname + " - not in posted values");
	}

	public String getLogMsgs() {
		return logMsgs;
	}

	public void setLogMsgs(String logMsgs) {
		this.logMsgs = logMsgs;
	}

	static final Comparator<String> Sort_String_Vector_Order = new Comparator<String>() {
		public int compare(String snum0, String snum1) {
			try {
				if ( snum0 == null ) snum0 = "";
				if ( snum1 == null ) snum1 = "";
				return snum0.compareTo(snum1);
			} catch (Exception e) {
				return 0;
			}
		}
	};
}
