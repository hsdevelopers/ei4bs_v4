package com.healthspace.general;

import java.io.Serializable;

public class HS_Page implements Serializable {
	private static final long serialVersionUID = 1L;

	public HS_Page() {
	}

	private String key;
	private String title;
	private String html;
	private String menu_hierarchy;
	private String menu_position;
	private String menu_status;
	private String menu_icon;
	private String author;
	private String modified;
	private String unid;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getKey() {
		return key;
	}

	public String getTitle() {
		return title;
	}

	public String getHtml() {
		return html;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setHtml(String html) {
		this.html = html;
	}

	public String getMenu_hierarchy() {
		return menu_hierarchy;
	}

	public String getMenu_position() {
		return menu_position;
	}

	public String getMenu_status() {
		return menu_status;
	}

	public void setMenu_hierarchy(String hierarchy) {
		menu_hierarchy = hierarchy;
	}

	public void setMenu_position(String position) {
		menu_position = position;
	}

	public void setMenu_status(String status) {
		menu_status = status;
	}

	public String getAuthor() {
		return author;
	}

	public String getModified() {
		return modified;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public String getUnid() {
		return unid;
	}

	public void setUnid(String unid) {
		this.unid = unid;
	}

	public String getMenu_icon() {
		return menu_icon;
	}

	public void setMenu_icon(String menu_icon) {
		this.menu_icon = menu_icon;
	}

}
