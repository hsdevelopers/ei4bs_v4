package com.healthspace.general;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.DocumentCollection;
import org.openntf.domino.Item;
import org.openntf.domino.RichTextItem;
import org.openntf.domino.View;

public class HS_BO_Billing {

	// Function GetFees( Facility As NotesDocument, SpecificFeeType As String, sEvent As String, allfees As NotesDocumentCollection ) As String
	public static String getFees(Document facility, String specificFeeType, String sEvent, DocumentCollection allfees) {
		final boolean localDebug = true;
		final String msgContext = "HS_BO_Billing.getFees";

		// if ( localDebug ) System.out.print(msgContext + " - Auto-generated method stub - getFees FUNCTION NOT IMPLEMENTED");
		// if ( true ) return "TODO Auto-generated method stub - getFees FUNCTION NOT IMPLEMENTED";

		View feeview = null;
		String sType;
		String sSubType = "";
		// String tmp = "";
		String sBillType = "";
		String msg = "";

		boolean bBillingType = false;

		try {
			if ( facility == null ) return "";
			if ( !facility.hasItem("Type") ) return "Facility document does not have a type";

			feeview = facility.getParentDatabase().getView("FeeAmtLookup");
			if ( feeview == null ) return "FeeAmtLookup view not found";

			// ' Decode the facility type using the form name
			sType = xlatModule(facility.getFormName());
			if ( "INVALID".equalsIgnoreCase(sType) ) return sType + " type (" + facility.getFormName() + ") is not recognized by fee engine.";
			sSubType = (facility.getFormName().endsWith("Report")) ? facility.getItemValueString("FacilityType") : facility.getItemValueString("Type");

			// If BillingSettings.HasItem( "BillingTypeMods" ) Then vBillingTypeMods = BillingSettings.BillingTypeMods 'mods using billing type
			Vector<String> vBillingTypeMods = HS_Util.getProfileVector("BillingSettings", "BillingTypeMods");
			Iterator<String> vbtmItr = vBillingTypeMods.iterator();
			bBillingType = false;
			// for (String v : vBillingTypeMods) {}
			while (vbtmItr.hasNext()) {
				String v = vbtmItr.next();
				// need to strip off the spaces
				while (v.indexOf(" ") > -1)
					v = v.replace(" ", "");
				sBillType = v;
				if ( "MobileHome".equalsIgnoreCase(sType) ) {
					// If sBillType = "MobileHomePark" Then bBillingType = True 'Mobile home park is a bastard
					if ( "MobileHomePark".equalsIgnoreCase(sBillType) ) bBillingType = true; // Mobile home park is a bastard
				} else {
					// If sBillType = sType Then bBillingType = True
					if ( sBillType.equalsIgnoreCase(sType) ) bBillingType = true;
				}
			}
			// If BillingSettings.BillingTypeOnSpecificFees(0) = "No" And sEvent = "Specific" Then
			if ( "No".equals(HS_Util.getProfileString("BillingSettings", "BillingTypeOnSpecificFees")) && "Specific".equals(sEvent) ) bBillingType = false;
			if ( "Violation".equals(specificFeeType) ) bBillingType = false;
			// If LCase(SpecificFeeType) = "additional inspection fee" Or LCase(SpecificFeeType) = "plan review fee" Then
			if ( "additional inspection fee".equalsIgnoreCase(specificFeeType) ) bBillingType = false;
			if ( "plan review fee".equalsIgnoreCase(specificFeeType) ) bBillingType = false;
			if ( bBillingType ) sSubType += " - " + facility.getItemValueString("BillingType"); // 'if billing settings is configured to use billing types

			Vector<String> keys = new Vector<String>();
			// If sType = "MobileHomePark" Then keys(0) = "MobileHomeTypes" Else keys(0) = sType & "Types"
			if ( "MobileHomePark".equals(sType) ) {
				keys.add("MobileHomeTypes");
			} else {
				keys.add(sType + "Types");
			}
			keys.add(sEvent);
			keys.add(sSubType);
			keys.add(specificFeeType);
			if ( localDebug ) {
				System.out.print(msgContext + " - sType:[" + sType + "]");
				System.out.print(msgContext + " - specificFeeType:[" + specificFeeType + "]");
				System.out.print(msgContext + " - sEvent:[" + sEvent + "]");
				System.out.print(msgContext + " - bBillingType:[" + bBillingType + "] sBillType:[" + sBillType + "]");
				System.out.print(msgContext + " - Searching with keys: " + keys.toString() + ".");
			}

			DocumentCollection removeThese = null;
			allfees = feeview.getAllDocumentsByKey(keys.toArray(), true);
			if ( allfees.getCount() > 0 ) {
				if ( localDebug ) System.out.print(msgContext + " - feeview search found:[" + allfees.getCount() + "] docs.");
				String feesbyregion = HS_Util.getProfileString("BillingSettings", "FeesByRegion");
				// ' Get county name
				String county = ("Yes".equals(feesbyregion)) ? facility.getItemValueString("PhysicalMunicipality") : "";
				for (Document thisfee : allfees) {
					Vector<Object> testval = thisfee.getItemValue("Amount");
					Vector<Object> region = thisfee.getItemValue("Region");
					if ( !(testval.get(0) instanceof Number) ) {
						msg = "Non valid amount for config document (" + sType + " - " + sSubType
								+ "),  Check the Fees\\Fee Configuration view to verify an amount has been entered for this document.";
						if ( localDebug ) System.out.print(msgContext + " - " + msg);
						return msg;
					}
					if ( "Yes".equals(feesbyregion) ) {
						Iterator<Object> regitr = region.iterator();
						boolean removeFee = true;
						while (regitr.hasNext()) {
							String r = (String) regitr.next();
							if ( county.equals(r) ) removeFee = false;
						}
						if ( removeFee ) {
							if ( removeThese == null ) removeThese = facility.getParentDatabase().search("Form = \"NeverEverFindAnything\"");
							removeThese.addDocument(thisfee, true);
						}
					}
				}
				if ( removeThese != null ) {
					allfees.subtract(removeThese); // Does this work in java with a collection???
					if ( allfees.getCount() == 0 )
						return "No fee configurations were found for a "
								+ sEvent
								+ " of this type. ("
								+ sType
								+ " - "
								+ sSubType
								+ " - "
								+ specificFeeType
								+ " - "
								+ county
								+ ")"
								+ "Check the Fees\\Configuration view and make sure that an accurate fee configuration document for this type of facility has been created.";
				}
				if ( localDebug ) System.out.print(msgContext + " - After removeThese removed. allfees has:[" + allfees.getCount() + "] docs...");
			} else {
				return "No fee configurations were found for a "
						+ sEvent
						+ " of this type. ("
						+ sType
						+ " - "
						+ sSubType
						+ " - "
						+ specificFeeType
						+ ")"
						+ " Check the Fees\\Configuration view and make sure that an accurate fee configuration document for this type of facility has been created.";
			}
			return "";
		} catch (Exception e) {
			return msgContext + " ERROR: " + e.toString();
		}
	}

	public static String xlatModule(String sForm) {
		// Select Case sForm
		if ( "CampFacility".equalsIgnoreCase(sForm) || "CampReport".equalsIgnoreCase(sForm) ) return "Campground";
		if ( "LaborCampFacility".equalsIgnoreCase(sForm) || "LaborCampReport".equalsIgnoreCase(sForm) ) return "LaborCamp";
		if ( "HotelFacility".equalsIgnoreCase(sForm) || "HotelReport".equalsIgnoreCase(sForm) ) return "Hotel";
		if ( "PersonalService".equalsIgnoreCase(sForm) || "PersonalServiceReport".equalsIgnoreCase(sForm) ) return "PersonalService";
		if ( "SewageApplication".equalsIgnoreCase(sForm) || "SewageReport".equalsIgnoreCase(sForm) || "SewageSystem".equalsIgnoreCase(sForm)
				|| "SewageDisposalSystem".equalsIgnoreCase(sForm) ) return "Sewage";
		if ( "SpecialEvent".equalsIgnoreCase(sForm) ) return "SpecialEvent";
		if ( "WaterPrivateWell".equalsIgnoreCase(sForm) || "PrivateWell".equalsIgnoreCase(sForm) ) return "PrivateWell";
		if ( "GarbageFacility".equalsIgnoreCase(sForm) ) return "GarbageHauler";
		if ( "TobaccoSalesFacility".equalsIgnoreCase(sForm) ) return "Tobacco";

		if ( sForm.endsWith("Facility") ) return sForm.substring(0, sForm.length() - 8);
		if ( sForm.endsWith("Report") ) return sForm.substring(0, sForm.length() - 6);
		// 'Elseif Right( sForm, 7 ) = "Request" Then
		// 'If Left( sForm, 6 ) = "Sewage" Then
		// 'xlatModule = "Sewage"
		// 'Elseif Left( sForm, 11 ) = "PrivateWell" Then
		// 'xlatModule = "PrivateWell"
		// 'Else
		// 'xlatModule = "INVALID"
		// 'End If
		return "INVALID";
	}

	// Function GetFees( Facility As NotesDocument, SpecificFeeType As String, sEvent As String, allfees As NotesDocumentCollection ) As String
	public static String getFees(Document facility, String specificFeeType, String sEvent, Map<String, Object> allfeesMap) {
		final boolean localDebug = true;
		final String msgContext = "HS_BO_Billing.getFees.mapversion";

		View feeview = null;
		String sType;
		String sSubType = "";
		// String tmp = "";
		String sBillType = "";
		String msg = "";

		boolean bBillingType = false;

		try {
			if ( facility == null ) return "";
			if ( !facility.hasItem("Type") ) return "Facility document does not have a type";

			feeview = facility.getParentDatabase().getView("FeeAmtLookup");
			if ( feeview == null ) return "FeeAmtLookup view not found";

			// ' Decode the facility type using the form name
			sType = xlatModule(facility.getFormName());
			if ( "INVALID".equalsIgnoreCase(sType) ) return sType + " type (" + facility.getFormName() + ") is not recognized by fee engine.";
			sSubType = (facility.getFormName().endsWith("Report")) ? facility.getItemValueString("FacilityType") : facility.getItemValueString("Type");

			// If BillingSettings.HasItem( "BillingTypeMods" ) Then vBillingTypeMods = BillingSettings.BillingTypeMods 'mods using billing type
			Vector<String> vBillingTypeMods = HS_Util.getProfileVector("BillingSettings", "BillingTypeMods");
			Iterator<String> vbtmItr = vBillingTypeMods.iterator();
			bBillingType = false;
			// for (String v : vBillingTypeMods) {}
			while (vbtmItr.hasNext()) {
				String v = vbtmItr.next();
				// need to strip off the spaces
				while (v.indexOf(" ") > -1)
					v = v.replace(" ", "");
				sBillType = v;
				if ( "MobileHome".equalsIgnoreCase(sType) ) {
					// If sBillType = "MobileHomePark" Then bBillingType = True 'Mobile home park is a bastard
					if ( "MobileHomePark".equalsIgnoreCase(sBillType) ) bBillingType = true; // Mobile home park is a bastard
				} else {
					// If sBillType = sType Then bBillingType = True
					if ( sBillType.equalsIgnoreCase(sType) ) bBillingType = true;
				}
			}
			// If BillingSettings.BillingTypeOnSpecificFees(0) = "No" And sEvent = "Specific" Then
			if ( "No".equals(HS_Util.getProfileString("BillingSettings", "BillingTypeOnSpecificFees")) && "Specific".equals(sEvent) ) bBillingType = false;
			if ( "Violation".equals(specificFeeType) ) bBillingType = false;
			// If LCase(SpecificFeeType) = "additional inspection fee" Or LCase(SpecificFeeType) = "plan review fee" Then
			if ( "additional inspection fee".equalsIgnoreCase(specificFeeType) ) bBillingType = false;
			if ( "plan review fee".equalsIgnoreCase(specificFeeType) ) bBillingType = false;
			if ( bBillingType ) sSubType += " - " + facility.getItemValueString("BillingType"); // 'if billing settings is configured to use billing types

			Vector<String> keys = new Vector<String>();
			// If sType = "MobileHomePark" Then keys(0) = "MobileHomeTypes" Else keys(0) = sType & "Types"
			if ( "MobileHomePark".equals(sType) ) {
				keys.add("MobileHomeTypes");
			} else {
				keys.add(sType + "Types");
			}
			keys.add(sEvent);
			keys.add(sSubType);
			keys.add(specificFeeType);
			if ( localDebug ) {
				System.out.print(msgContext + " - sType:[" + sType + "]");
				System.out.print(msgContext + " - specificFeeType:[" + specificFeeType + "]");
				System.out.print(msgContext + " - sEvent:[" + sEvent + "]");
				System.out.print(msgContext + " - bBillingType:[" + bBillingType + "] sBillType:[" + sBillType + "]");
				System.out.print(msgContext + " - Searching with keys: " + keys.toString() + ".");
			}

			DocumentCollection removeThese = null;

			DocumentCollection allfees = feeview.getAllDocumentsByKey(keys.toArray(), true);
			if ( allfees.getCount() > 0 ) {
				if ( localDebug ) System.out.print(msgContext + " - feeview search found:[" + allfees.getCount() + "] docs.");
				String feesbyregion = HS_Util.getProfileString("BillingSettings", "FeesByRegion");
				// ' Get county name
				String county = ("Yes".equals(feesbyregion)) ? facility.getItemValueString("PhysicalMunicipality") : "";
				for (Document thisfee : allfees) {
					Vector<Object> testval = thisfee.getItemValue("Amount");
					Vector<Object> region = thisfee.getItemValue("Region");
					if ( !(testval.get(0) instanceof Number) ) {
						msg = "Non valid amount for config document (" + sType + " - " + sSubType
								+ "),  Check the Fees\\Fee Configuration view to verify an amount has been entered for this document.";
						if ( localDebug ) System.out.print(msgContext + " - " + msg);
						return msg;
					}
					if ( "Yes".equals(feesbyregion) ) {
						Iterator<Object> regitr = region.iterator();
						boolean removeFee = true;
						while (regitr.hasNext()) {
							String r = (String) regitr.next();
							if ( county.equals(r) ) removeFee = false;
						}
						if ( removeFee ) {
							if ( removeThese == null ) removeThese = facility.getParentDatabase().search("Form = \"NeverEverFindAnything\"");
							removeThese.addDocument(thisfee, true);
						}
					}
				}
				if ( removeThese != null ) {
					allfees.subtract(removeThese); // Does this work in java with a collection???
					if ( allfees.getCount() == 0 )
						return "No fee configurations were found for a "
								+ sEvent
								+ " of this type. ("
								+ sType
								+ " - "
								+ sSubType
								+ " - "
								+ specificFeeType
								+ " - "
								+ county
								+ ")"
								+ "Check the Fees\\Configuration view and make sure that an accurate fee configuration document for this type of facility has been created.";
				}
				if ( localDebug ) System.out.print(msgContext + " - After removeThese removed. allfees has:[" + allfees.getCount() + "] docs...");
				for (Document feedoc : allfees) {
					Map<String, Object> feemap = new HashMap<String, Object>();
					for (Item itm : feedoc.getItems()) {
						// if ( localDebug ) System.out.print(msgContext + " - Adding to fee doc map:" + itm.getName());
						if ( !itm.getName().startsWith("$") ) feemap.put(itm.getName().toLowerCase(), itm.getValues());
					}
					feemap.put("unid", feedoc.getUniversalID());
					allfeesMap.put(feedoc.getUniversalID(), feemap);
				}
				if ( localDebug ) System.out.print(msgContext + " - allfeesMap has [" + allfeesMap.size() + "] elements.");
			} else {
				return "No fee configurations were found for a "
						+ sEvent
						+ " of this type. ("
						+ sType
						+ " - "
						+ sSubType
						+ " - "
						+ specificFeeType
						+ ")"
						+ " Check the Fees\\Configuration view and make sure that an accurate fee configuration document for this type of facility has been created.";
			}
			return "";
		} catch (Exception e) {
			if ( HS_Util.isRestDebugServer() || HS_Util.isDebugServer() ) {
				System.out.print(msgContext + " ERROR: " + e.toString());
				e.printStackTrace();
			}
			return msgContext + " ERROR: " + e.toString();
		}
	}

	// Sub AddToFacilityAuditTrail(Inspection As NotesDocument, sError As String, WhatHappened As String)
	public static void addToFacilityAuditTrail(Inspection insp, String sError, String whatHappened) {
		addToFacilityAuditTrail(insp, null, sError, whatHappened);
	}

	public static void addToFacilityAuditTrail(Inspection insp, Document facility, String sError, String whatHappened) {
		final boolean localDebug = true;
		final String msgContext = "HS_BO_Billing.addToFacilityAuditTrail";

		Database db = null;
		Document docParent = null;
		RichTextItem rtStatusHistory = null;
		String msg = "";
		try {
			if ( facility == null ) {
				// db = insp.getParentDatabase();
				db = new HS_database().getdb("eiRoot");
				docParent = HS_Util.getDocumentByUNID(db, insp.getParentUnid(), false);
				while (facility == null && !(docParent == null)) {
					if ( docParent.getFormName().contains("Facility") ) {
						facility = docParent;
					} else {
						docParent = HS_Util.getDocumentByUNID(db, docParent.getParentDocumentUNID(), false);
					}
				}
			}
			if ( facility == null ) {
				if ( localDebug ) System.out.println(msgContext + ": could not access the facility document! [" + insp.getParentUnid() + "]");
				return;
			}
			if ( facility.hasItem("StatusHistory") ) {
				Item shist = facility.getFirstItem("StatusHistory");
				if ( shist.getTypeEx().equals(Item.Type.RICHTEXT) ) {
					rtStatusHistory = (RichTextItem) facility.getFirstItem("StatusHistory");
				} else if ( shist.getTypeEx().equals(Item.Type.TEXT) ) {
					if ( !"".equals(shist.getText()) ) {
						Vector<Object> shdata = shist.getValues();
						facility.removeItem("StatusHistory");
						rtStatusHistory = facility.createRichTextItem("StatusHistory");
						for (Object shd : shdata) {
							if ( !"".equals(shd) ) {
								rtStatusHistory.appendText((String) shd);
								rtStatusHistory.addNewLine(1, true);
							}
						}
						if ( localDebug )
							System.out.println(msgContext + ": StatusHistory converted from TEXT to RICHTEXT with [" + shdata.size() + "] entries");
					} else {
						if ( localDebug ) System.out.println(msgContext + ": StatusHistory converted to RICHTEXT with no content. [");
						facility.removeItem("StatusHistory");
						rtStatusHistory = facility.createRichTextItem("StatusHistory");
					}
				} else {
					if ( localDebug )
						System.out.println(msgContext + ": StatusHistory on the facility doc is NOT a rich text item! [" + shist.getTypeEx().toString() + "]");
					return;
				}
			} else {
				rtStatusHistory = facility.createRichTextItem("StatusHistory");
			}
			if ( "Error".equalsIgnoreCase(whatHappened) ) {
				msg = "An error occured on " + HS_Util.formatDate(new Date(), "mmm dd, yyyy");
				msg += " for " + HS_Util.getSession().getCommonUserName();
				msg += " during automatic inspection fee creation for Inspection <" + insp.getDocumentId() + ">";
				rtStatusHistory.appendText(msg);
				rtStatusHistory.addNewLine();
				rtStatusHistory.appendText(sError);
				rtStatusHistory.addNewLine(2, true);
				msg += " " + sError;
			} else if ( "Civil Penalty".equalsIgnoreCase(whatHappened) ) {
				msg += "An automatic email was sent to " + sError + " regarding an inspection fee. ";
				rtStatusHistory.appendText(msg);
				rtStatusHistory.addNewLine(1, true);
			} else if ( "Civil Penalty error".equalsIgnoreCase(whatHappened) ) {
				msg += "There was an issue with the automatic email regarding an inspection fee. " + sError;
				rtStatusHistory.appendText(msg);
				rtStatusHistory.addNewLine(1, true);
			} else {
				if ( localDebug ) System.out.print(msgContext + ": dunno how to log this[" + whatHappened + "]");
				return;
			}
			facility.save(true, true);
			if ( localDebug ) System.out.println(msgContext + ": logged: " + msg);
		} catch (Exception e) {
			System.out.print(msgContext + " ERROR: " + e.toString());
			if ( HS_Util.isRestDebugServer() || HS_Util.isDebugServer() ) e.printStackTrace();
			return;
		}
	}
}
