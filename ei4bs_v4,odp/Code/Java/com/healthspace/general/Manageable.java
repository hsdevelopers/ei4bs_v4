package com.healthspace.general;

import org.openntf.domino.Database;
import org.openntf.domino.Document;

public interface Manageable {

	public boolean save(Inspection inspection);

	public boolean load(Inspection inspection, Database wdb, String unid);

	public boolean load(Inspection inspection, Document idoc);

}
