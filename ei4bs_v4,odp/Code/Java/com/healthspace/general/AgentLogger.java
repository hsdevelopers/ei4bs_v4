package com.healthspace.general;

import java.util.Calendar;
import java.util.Enumeration;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import lotus.domino.NotesException;

import org.openntf.domino.ColorObject;
import org.openntf.domino.Database;
import org.openntf.domino.DateTime;
import org.openntf.domino.Document;
import org.openntf.domino.MIMEEntity;
import org.openntf.domino.RichTextItem;
import org.openntf.domino.RichTextStyle;
import org.openntf.domino.Session;
import org.openntf.domino.Stream;
import org.openntf.domino.View;
import org.openntf.domino.utils.Factory;

import com.healthspace.tools.JSFUtil;
import com.ibm.xsp.designer.context.ServletXSPContext;
import com.ibm.xsp.designer.context.XSPUrl;

/*
 * Derived from the LS class of a similar name
 * Based on the LS version 2.5
 * Java version by Newbs on 10 Oct 2013
 * 
 * This version is NOT serializable so it cannot be used in an XPage as-is
 * 
 * Updated to flag entries as "XPages event:" vs "Not a real agent "
 * 
 * 2016-06-30 : Added the Remote Address, URL, URI, and QueryString to the output document... 
 */

public class AgentLogger {
	public final String AgentLoggerVersion = "2.5.L";
	public final String HSAgentLogReplicaID = "8825694D001E6577";
	// private final String agtPrefix = "not a real agent ";
	private final String agtPrefix = "XPages event: ";

	private Database logDatabase = null;
	private Document logDocument = null;
	private RichTextItem logMessages = null;
	private RichTextStyle defaultTextStyle = null;
	private long countDocsRead;
	private long countDocsAdded;
	private long countDocsModified;
	private long countDocsDeleted;
	private String pAuthor;

	private String resolved;
	private String status;

	public Session getLogSession() {
		return getSession();
	}

	// public void setLogSession(Session logSession) {
	// this.logSession = logSession;
	// }

	public Database getLogDatabase() {
		return logDatabase;
	}

	// public void setLogDatabase(Database logDatabase) {
	// this.logDatabase = logDatabase;
	// }

	public Document getLogDocument() {
		return logDocument;
	}

	// public void setLogDocument(Document logDocument) {
	// this.logDocument = logDocument;
	// }

	public long getCountDocsRead() {
		return countDocsRead;
	}

	// public void setCountDocsRead(long countDocsRead) {
	// this.countDocsRead = countDocsRead;
	// }

	public long getCountDocsAdded() {
		return countDocsAdded;
	}

	// public void setCountDocsAdded(long countDocsAdded) {
	// this.countDocsAdded = countDocsAdded;
	// }

	public long getCountDocsModified() {
		return countDocsModified;
	}

	// public void setCountDocsModified(long countDocsModified) {
	// this.countDocsModified = countDocsModified;
	// }

	public long getCountDocsDeleted() {
		return countDocsDeleted;
	}

	// public void setCountDocsDeleted(long countDocsDeleted) {
	// this.countDocsDeleted = countDocsDeleted;
	// }

	public String getPAuthor() {
		return pAuthor;
	}

	public String getAgentAuthor() {
		return pAuthor;
	}

	// public void setPAuthor(String author) {
	// pAuthor = author;
	// }

	public String getResolved() {
		return resolved;
	}

	public void setResolved(String resolved) {
		this.resolved = resolved;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public AgentLogger() {
		countDocsRead = 0;
		countDocsAdded = 0;
		countDocsModified = 0;
		countDocsDeleted = 0;
		pAuthor = "";
		status = "Completed";
	}

	public void init(Session session, String author, String description, boolean temp) {
		init(session, author, description, temp, "Resolved");
	}

	public void init(Session session, String author, String description, boolean temp, String iresolve) {
		// logSession = session;
		pAuthor = author;
		resolved = iresolve;
		try {
			// Determine server to use. This gets around a bug introduced in
			// domino 6
			String server = (session.isOnServer()) ? "" : session.getServerName();

			// Open HS Agent Log database using it's replica id
			logDatabase = session.getDatabase("", "");
			if ( logDatabase.openByReplicaID(server, HSAgentLogReplicaID) == false ) {
				throw (new Exception("Could not locate HealthSpace agent log database (" + HSAgentLogReplicaID + ")"));
			}

			// Create a new document representing an agent run
			logDocument = logDatabase.createDocument();
			logDocument.replaceItemValue("Version", AgentLoggerVersion);
			logDocument.replaceItemValue("Form", "AgentLog");
			logDocument.replaceItemValue("Status", "Running");
			logDocument.replaceItemValue("StartTime", now());

			// Prepare and log instance details about the agent run
			Database agentDatabase = getSession().getCurrentDatabase();
			logDocument.replaceItemValue("Username", getSession().getUserName());
			logDocument.replaceItemValue("Database", agentDatabase.getTitle());
			logDocument.replaceItemValue("DatabaseFile", agentDatabase.getFileName());
			logDocument.replaceItemValue("DatabasePath", agentDatabase.getFilePath());
			logDocument.replaceItemValue("Server", agentDatabase.getServer());

			lotus.domino.AgentContext agentContext = session.getAgentContext();
			lotus.domino.Agent agent = null;
			if ( agentContext == null ) {
				logDocument.replaceItemValue("Agent", this.agtPrefix + description);
				logDocument.replaceItemValue("AgentOwner", this.agtPrefix + author);
			} else {
				agent = agentContext.getCurrentAgent();
				if ( agent == null ) {
					logDocument.replaceItemValue("Agent", this.agtPrefix + description);
					logDocument.replaceItemValue("AgentOwner", this.agtPrefix + author);
				} else {
					logDocument.replaceItemValue("Agent", agent.getName());
					logDocument.replaceItemValue("AgentOwner", agent.getOwner());
				}
			}

			// Log additional information about the agent
			logDocument.replaceItemValue("AgentDescription", description);
			logDocument.replaceItemValue("AgentAuthor", author);
			if ( temp ) {
				logDocument.replaceItemValue("Temporary", true);
			} else {
				logDocument.replaceItemValue("Temporary", false);
			}

			// Prepare log document style
			logMessages = logDocument.createRichTextItem("Output");
			defaultTextStyle = getSession().createRichTextStyle();
			ColorObject color = getSession().createColorObject();
			color.setRGB(0, 0, 255);
			defaultTextStyle.setColor(color.getNotesColor());
			defaultTextStyle.setFont(4);
			defaultTextStyle.setFontSize(10);
			logMessages.appendStyle(defaultTextStyle);

			// Log initial message
			addLogMessageItem("Agent started.", null);

			// * 2016-06-30 : Added the Remote Address, URL, URI, and QueryString to the output document...
			this.addXPitems();

			// Save initial log details
			logDocument.save(true, true);
		} catch (Exception e) {
			// deal with it??
		}
	}

	@SuppressWarnings("unchecked")
	private void addXPitems() {
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			logDocument.replaceItemValue("XPRemoteAddress", request.getRemoteAddr());
			logDocument.replaceItemValue("XPRequestURI", request.getRequestURI());
			logDocument.replaceItemValue("XPQueryString", request.getQueryString());

			XSPUrl thisurl = ((ServletXSPContext) JSFUtil.getVariableValue("context")).getUrl();
			// logDocument.replaceItemValue("XPUrl", thisurl.toString());
			String wrkurl = thisurl.getScheme() + "://" + thisurl.getHost() + request.getRequestURI();
			wrkurl += ("".equals(request.getQueryString())) ? "" : "?" + request.getQueryString();
			logDocument.replaceItemValue("XPUrl", wrkurl);
			logDocument.replaceItemValue("XPUrlHost", thisurl.getHost());
			logDocument.replaceItemValue("XPUrlScheme", thisurl.getScheme());

			Enumeration headerNames = request.getHeaderNames();
			while (headerNames.hasMoreElements()) {
				String key = (String) headerNames.nextElement();
				String value = request.getHeader(key);
				logDocument.replaceItemValue("XP_Header_" + key, value);
			}
		} catch (Exception e1) {
			logDocument.replaceItemValue("XPError", e1.toString());
		}
	}

	// This is the class' destructor
	public void recycle() {
		if ( logDocument != null ) {

			if ( "Running".equals(logDocument.getItemValueString("Status")) ) {
				logDocument.replaceItemValue("Status", "Failed");
				logDocument.replaceItemValue("Error", "Agent was interrupted or terminated.");
			}
			logDocument.save(true, true);
		}
	}

	// Records a runtime message to the agent log
	public void log(String message, boolean save) {
		addLogMessageItem(message, null);
		if ( save ) store();
	}

	// Records a runtime warning message to the agent log
	public void logWarning(String message, boolean save) {
		if ( logDocument != null ) {
			// Record warning in special document field
			logDocument.replaceItemValue("HasWarning", "True");

			// Prepare color for warning message
			ColorObject color = getSession().createColorObject();
			color.setRGB(255, 0, 0);

			// Record warning message
			addLogMessageItem(message, color);
			if ( save ) store();
		}
	}

	public void logHTML(String html, boolean save) {
		if ( logDocument != null ) {
			Session sess = getSession();
			boolean cvtsetting = sess.isConvertMIME();
			sess.setConvertMime(false);
			// RichTextItem rti = logDocument.createRichTextItem("Output");
			logDocument.removeItem("Output");
			MIMEEntity ment = logDocument.createMIMEEntity("Output");
			Stream stream = sess.createStream();
			stream.writeText(html);
			ment.setContentFromText(stream, "text/html;charset=UTF-8", MIMEEntity.ENC_NONE);

			if ( save ) store();
			sess.setConvertMime(cvtsetting);
		}
	}

	// Record that the agent encountered an unhandled error
	public void error(NotesException ne) {
		if ( logDocument != null ) {
			// Log final message
			ColorObject color = getSession().createColorObject();
			color.setRGB(255, 0, 0);
			addLogMessageItem("Agent failed (with internal notes error).", color);

			// Log error data
			logDocument.replaceItemValue("EndTime", now());
			logDocument.replaceItemValue("Status", "Failed");
			logDocument.replaceItemValue("Error", ne.text);
			logDocument.replaceItemValue("ErrorCode", ne.id);
			logDocument.replaceItemValue("ErrorLine", -1);
			store();
		}
	}

	// Record that the agent encountered an unhandled error
	public void error(Exception e) {
		if ( logDocument != null ) {
			// Log final message
			ColorObject color = getSession().createColorObject();
			color.setRGB(255, 0, 0);
			addLogMessageItem("Agent failed (with internal notes error).", color);

			// Log error data
			logDocument.replaceItemValue("EndTime", now());
			logDocument.replaceItemValue("Status", "Failed");
			logDocument.replaceItemValue("Error", e.toString());
			logDocument.replaceItemValue("ErrorCode", 0);
			logDocument.replaceItemValue("ErrorLine", -1);
			store();
		}
	}

	// Record that the agent completed successfully
	public void success() {
		if ( logDocument != null ) {
			if ( logDocument.hasItem("HasWarning") ) {
				addLogMessageItem("Agent completed successfully (with warnings).", null);
			} else {
				addLogMessageItem("Agent completed successfully.", null);
			}
			logDocument.replaceItemValue("EndTime", now());
			logDocument.replaceItemValue("Status", "Completed");
			store();
		}
	}

	// Record that the agent encountered a handled error
	public void failure(String message) {
		if ( logDocument != null ) {
			// Log final message
			ColorObject color = getSession().createColorObject();
			color.setRGB(255, 0, 0);
			addLogMessageItem("Agent failed (with custom error).", color);

			// Log error data
			logDocument.replaceItemValue("EndTime", now());
			logDocument.replaceItemValue("Status", "Completed");
			logDocument.replaceItemValue("Error", "(CUSTOM) " + message);
			store();
		}
	}

	// Adds a document link to the agent log
	public void addLink(Document doc) {
		if ( logDocument != null ) {
			// Catch exceptions here - an error will be thrown if the document
			// is in a database
			// without a default view!
			logMessages.appendDocLink(doc, "");
		}
	}

	// Add a link to the task system
	@SuppressWarnings("deprecation")
	public void taskLink(String ticketId) {
		if ( logDocument != null ) {
			Database taskDb = getSession().getDatabase("", "");
			taskDb.openByReplicaID(logDatabase.getServer(), "882566FD0060F4E0");
			if ( !taskDb.isOpen() ) taskDb.openByReplicaID("Wol/HealthSpace", "882566FD0060F4E0");
			if ( taskDb.isOpen() ) {
				View lookie = taskDb.getView("LookupNum");
				if ( lookie != null ) {
					Document taskDoc = lookie.getDocumentByKey(ticketId, true);
					if ( taskDoc != null ) {
						RichTextItem taskLink = null;
						if ( !logDocument.hasItem("TaskLink") ) {
							taskLink = logDocument.createRichTextItem("TaskLink");
						} else {
							taskLink = (RichTextItem) logDocument.getFirstItem("TaskLink");
						}
						taskLink.appendDocLink(taskDoc, taskDoc.getItemValueString("Title"));
						taskLink.appendText(" " + taskDoc.getItemValueString("Title"));
					}
				}
			}
		}
	}

	// Add some other field
	public void addField(String fldName, Object fldValue) {
		if ( logDocument != null ) {
			logDocument.replaceItemValue(fldName, fldValue);
		}
	}

	// Increment read document count
	public void incrementDocumentsRead() {
		countDocsRead++;
	}

	// Increment added document count
	public void incrementDocumentsAdded() {
		countDocsAdded++;
	}

	// Increment modified document count
	public void incrementDocumentsModified() {
		countDocsModified++;
	}

	// Increment deleted document count
	public void incrementDocumentsDeleted() {
		countDocsDeleted++;
	}

	// Adds a new coloured line of text to the rich text log
	private void addLogMessageItem(String message, ColorObject color) {
		if ( logMessages != null ) {
			// start a new line
			logMessages.addNewLine(1);

			// If there is no message to display, we don't need to continue
			// processing
			if ( !"".equals(message) ) {
				// Prepare the text style
				RichTextStyle textStyle;
				if ( color == null ) {
					textStyle = defaultTextStyle;
				} else {
					textStyle = getSession().createRichTextStyle();
					textStyle.setColor(color.getNotesColor());
				}

				// Add the text to the log file
				logMessages.appendStyle(textStyle);
				// logMessages.appendText( Format(Now, "hh:nn:ss") + " " +
				// Message )
				logMessages.appendText(now().getTimeOnly() + " " + message);
			}
		}

	}

	// Updates log statistics and saves the log file to the database
	private void store() {
		logDocument.closeMIMEEntities(true);
		logDocument.replaceItemValue("DocsModified", countDocsModified);
		logDocument.replaceItemValue("DocsAdded", countDocsAdded);
		logDocument.replaceItemValue("DocsDeleted", countDocsDeleted);
		logDocument.replaceItemValue("DocsRead", countDocsRead);
		logDocument.replaceItemValue("Resolved", resolved);
		logDocument.replaceItemValue("Status", status);
		logDocument.save(true, true);
	}

	private DateTime now() {
		Calendar wcal = Calendar.getInstance();
		DateTime rslt = getSession().createDateTime(wcal);
		rslt.setNow();
		// System.out.print("AgentLogger.now(): rslt.timeZone is:[" + rslt.getTimeZone() + "] wcal.timeZone.getOffset:[" +
		// wcal.getTimeZone().getOffset(new Date().getTime()) + "]");
		return rslt;
	}

	private Session getSession() {
		// return Factory.fromLotus(ExtLibUtil.getCurrentSession(), org.openntf.domino.Session.SCHEMA, null);
		return Factory.getSession();
	}
}
