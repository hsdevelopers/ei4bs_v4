package com.healthspace.general;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import lotus.domino.ACL;

import org.openntf.domino.Database;
import org.openntf.domino.DateTime;
import org.openntf.domino.Document;
import org.openntf.domino.DocumentCollection;
import org.openntf.domino.Item;
import org.openntf.domino.Name;
import org.openntf.domino.RichTextItem;
import org.openntf.domino.Session;
import org.openntf.domino.View;

import com.ibm.jscript.InterpretException;
import com.ibm.jscript.std.ObjectObject;
import com.ibm.xsp.extlib.util.ExtLibUtil;

@SuppressWarnings("serial")
public class HS_BusinessObjects implements Serializable, Manageable {

	public HS_BusinessObjects() {
	}

	public static HS_BusinessObjects getInstance() {
		return new HS_BusinessObjects();
	}

	// private final String standardDateMask = "MM/dd/yyyy";
	private String	standardDateMask	= "";

	public boolean load(Inspection insp, Database wdb, String unid) {
		Document idoc = wdb.getDocumentByUNID(unid);
		return this.load(insp, idoc);
	}

	public boolean load(Inspection insp, Document idoc) {
		final boolean localDebug = false;
		final String dbarLoc = "HS_BusinessObjects.load";
		if ( idoc == null ) return false;

		if ( localDebug ) HS_Util.debug("Loading from idoc.unid:[" + idoc.getUniversalID() + "] " + HS_Util.debugCalledFrom(), "debug", dbarLoc);
		// Do this first so we can compare it before the save to be sure it is
		// the same.
		try {
			insp.setLoadedForm(new HS_auditForm(idoc));
			if ( localDebug ) HS_Util.debug("setLoadedForm from idoc.unid:[" + idoc.getUniversalID() + "]", "debug", dbarLoc);
		} catch (Exception e) {
			HS_Util.debug("ERROR on insp.setLoadedForm: " + e.toString(), "error", dbarLoc);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			return false;
		}

		try {
			insp.setForm(idoc.getFormName());
			insp.setUnid(idoc.getUniversalID());
		} catch (Exception e) {
			HS_Util.debug("ERROR 1: " + e.toString(), "error", dbarLoc);
			return false;
		}
		try {
			this.loadValues(insp, idoc);
		} catch (Exception e) {
			HS_Util.debug("ERROR 2: " + e.toString(), "error", dbarLoc);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			return false;
		}
		try {
			HS_BO_Violations.loadViolations(insp, idoc);
		} catch (Exception e) {
			HS_Util.debug("ERROR 3: " + e.toString(), "error", dbarLoc);
			return false;
		}
		try {
			insp.setEhoData(this.loadSubOffice(insp, idoc));
		} catch (Exception e) {
			HS_Util.debug("ERROR 4: " + e.toString(), "error", dbarLoc);
			return false;
		}
		try {
			this.loadTimeTracking(insp, idoc);
		} catch (Exception e) {
			HS_Util.debug("ERROR 5: " + e.toString(), "error", dbarLoc);
			return false;
		}

		try {
			HS_BO_Sections.loadSectionData(insp, idoc);
		} catch (Exception e) {
			HS_Util.debug("ERROR rom loadSectionData 5: " + e.toString(), "error", dbarLoc);
			return false;
		}

		return true;
	}

	public boolean save(Inspection insp) {
		boolean result = true;
		// final boolean localDebug = (HS_Util.isDebugServer() ||
		// HS_Util.isDebugger());
		boolean localDebug = false;
		String msg = "";
		final String dbarLoc = "HS_BusinessObjects.save";

		// String auditEvent = "Document Saved - Mobile";
		String auditEvent = "Document Saved - " + insp.getEnvironment();

		HS_database hsdb = new HS_database();
		Database eidb = hsdb.getdb("eiRoot");
		Document idoc;
		if ( eidb == null ) {
			HS_Util.debug("hsdb.getdb failed to open eiRoot database.", "error", dbarLoc);
			return false;
		}
		if ( localDebug ) HS_Util.debug("eidb.server:[" + eidb.getServer() + "] path:[" + eidb.getFilePath() + "]", "debug", dbarLoc);
		if ( insp.getUnid() == null ) {
			idoc = eidb.createDocument();
			// auditEvent = "Document Created - Mobile";
			auditEvent = "Document Created - " + insp.getEnvironment();
			updateItem(insp, idoc, "Form", insp.getForm());
			updateItem(insp, idoc, "DBDesign", insp.getDbDesign());
			updateItem(insp, idoc, "Source", insp.getSource());
			Date ndt = new Date();
			updateItem(insp, idoc, "DateCreated", ndt);

			addToEIBSLog(insp, idoc, auditEvent);

			replaceValues(idoc, insp); // Helper method
			Document parentDoc = eidb.getDocumentByUNID(insp.getParentUnid());
			while (parentDoc.getFormName().contains("Report") || parentDoc.hasItem("InspectionDate")) {
				parentDoc = parentDoc.getParentDocument();
			}
			idoc.makeResponse(parentDoc);

			this.fixDates(idoc); // Fix the inspectionDate and newxtInspection
			// items to be date only

			result = idoc.save();
			if ( this.debugTablet ) System.out.println(dbarLoc + ": save result is:[" + result + "] as a new inspection.");

			if ( result ) {
				insp.setUnid(idoc.getUniversalID());
				if ( this.debugTablet ) System.out.println(dbarLoc + ": save result is:[" + result + "] as a new inspection. UNID:[" + idoc.getUniversalID() + "]");

				new HS_auditForm().writeAuditEvent(idoc, auditEvent);
				if ( localDebug ) {
					msg = "doc created unid:[" + idoc.getUniversalID() + "]";
					HS_Util.debug(msg, "debug", dbarLoc);
					insp.getSaveMessages().add(msg);

					msg = "Comments:[" + insp.getComments() + "]";
					insp.getSaveMessages().add(msg);
				}
				// this.saveTimeTrackingDocs(insp, idoc);
				this.postSaveActions(insp, idoc);
				insp.setLoadedForm(new HS_auditForm(idoc));
				// TODO add other post save actions here
			} else {
				if ( this.debugTablet ) {
					System.out.println(dbarLoc + ": ACCESS level:[" + eidb.getCurrentAccessLevel() + "] user:[" + eidb.getParent().getEffectiveUserName() + "]");
				} else {
					if ( localDebug ) HS_Util.debug("failed to save inspection doc.", "debug", dbarLoc);
				}
			}
		} else {
			idoc = eidb.getDocumentByUNID(insp.getUnid());

			if ( idoc != null ) {
				final boolean chkchgDebug = false;
				try {
					if ( insp.getLoadedForm() == null ) {
						msg = "LoadedForm is null on document update save!";
						HS_Util.debug(msg, "error", dbarLoc);
						insp.getSaveMessages().add(msg + " " + dbarLoc);
						return false;
					} else {
						if ( insp.getLoadedForm().checkChanges(idoc) ) {
							msg = "Changes detected since document loaded!";
							HS_Util.debug(msg, "error", dbarLoc);
							insp.getSaveMessages().add(msg + " " + dbarLoc);
							Vector<String> cfields = insp.getLoadedForm().getChangedFields();
							msg = "Changes detected to fields:" + cfields.toString();
							if ( chkchgDebug ) HS_Util.debug(msg, "debug", dbarLoc);
							insp.getSaveMessages().add(msg);
							if ( chkchgDebug && cfields.size() < 10 ) {
								String cvalue = "";
								String ovalue = "";
								for (String cfield : cfields) {
									cvalue = idoc.getFirstItem(cfield).getText();
									ovalue = insp.getLoadedForm().getOpenValues().get(cfield.toLowerCase());
									HS_Util.debug("field:[" + cfield + "] original:[" + ovalue + "]  current:[" + cvalue + "]", "debug", dbarLoc);
								}
							}
							return false;
						}
					}
				} catch (Exception e) {
					msg = "Exception during checkChanges: " + e.toString();
					HS_Util.debug(msg, "error", dbarLoc);
					insp.getSaveMessages().add(msg + " " + dbarLoc);
					if ( HS_Util.isDebugServer() ) e.printStackTrace();
					return false;
				}
				HS_auditForm iform = new HS_auditForm(idoc);

				replaceValues(idoc, insp); // Helper method
				if ( localDebug ) {
					HS_Util.debug("updating doc with unid:[" + idoc.getUniversalID() + "]", "debug", dbarLoc);
					HS_Util.debug("eidb is:[" + eidb.getServer() + "[-[" + eidb.getFilePath() + "]", "debug", dbarLoc);
					HS_Util.debug("Access is:[" + eidb.getCurrentAccessLevel() + "]", "debug", dbarLoc);
					HS_Util.debug("User is:[" + ExtLibUtil.getXspContext().getUser().getDistinguishedName() + "]", "debug", dbarLoc);
				}

				boolean changesDetected = false;
				try {
					changesDetected = iform.checkChanges(idoc);
				} catch (Exception e) {
					msg = "Exception during checkChanges: " + e.toString();
					HS_Util.debug(msg, "error", dbarLoc);
					insp.getSaveMessages().add(msg + " " + dbarLoc);
					if ( HS_Util.isDebugServer() ) e.printStackTrace();
					return false;
				}
				if ( changesDetected ) {
					if ( localDebug ) HS_Util.debug("changes in the document detected. Save is firing..", "debug", dbarLoc);
					if ( localDebug ) insp.getSaveMessages().add("Changes in the document detected. Save is firing..");
					if ( localDebug ) HS_Util.debug("changes to fields:" + iform.getChangedFields().toString(), "debug", dbarLoc);
					if ( localDebug ) insp.getSaveMessages().add("Changes to fields:" + iform.getChangedFields().toString());

				}

				if ( this.isTimeReportsChanged(insp) ) {
					changesDetected = true;
					if ( localDebug ) HS_Util.debug("changes to time tracking docs detected. Save is firing..", "debug", dbarLoc);
				}

				if ( changesDetected ) {
					// Fix the inspectionDate and newxtInspection items to be
					// date only
					this.fixDates(idoc);

					addToEIBSLog(insp, idoc, auditEvent);
					result = idoc.save(true, false);
					if ( this.debugTablet ) System.out.println(dbarLoc + ": save result is:[" + result + "] as an updated document");
					if ( result ) {
						iform.writeAuditEvent(idoc, auditEvent, iform.getChangedFields());
					} else {
						HS_Util.debug("idoc.save(true, false) FAILED!", "error", dbarLoc);
						insp.getSaveMessages().add("idoc.save(true, false) FAILED!");
						int alvl = idoc.getParentDatabase().getCurrentAccessLevel();
						if ( alvl < ACL.LEVEL_EDITOR ) {
							HS_Util.debug("You have insufficient access to the database!", "error", dbarLoc);
							insp.getSaveMessages().add("You have insufficient access to the database. ACL access must be at least editor.");
						}
						// localDebug = true; // get debug messages from here!
					}
					if ( localDebug ) {
						msg = "Changes detected to fields:" + iform.getChangedFields();
						HS_Util.debug(msg, "debug", dbarLoc);
						insp.getSaveMessages().add(msg);

						msg = "doc with unid:[" + idoc.getUniversalID() + "] updated.";
						HS_Util.debug(msg, "debug", dbarLoc);
						// insp.getSaveMessages().add(msg);
						if ( localDebug ) {
							msg = "Comments:[" + insp.getComments() + "]";
							insp.getSaveMessages().add(msg);
						}
					}
				} else {
					msg = "no changes in the document detected. Save not executed..";
					// msg += (HS_Util.isDebugServer()) ? " observedSeating:[" +
					// insp.getObservedSeating() + "]" : "";
					if ( localDebug ) HS_Util.debug(msg, "debug", dbarLoc);
					insp.getSaveMessages().add(msg);

					if ( localDebug ) {
						msg = "Comments:[" + insp.getComments() + "]";
						insp.getSaveMessages().add(msg);
					}
					result = true;
				}
				if ( result ) {
					this.postSaveActions(insp, idoc);
				} else {
					HS_Util.debug("failed to update inspection doc.", "error", dbarLoc);
				}
			} else {
				HS_Util.debug("failed to access doc for update.", "error", dbarLoc);
				HS_Util.debug("UNID:[" + insp.getUnid() + "]", "debug", dbarLoc);
				insp.getSaveMessages().add("failed to access doc for update. UNID:[" + insp.getUnid() + "]");
			}
		}
		return result;
	}

	private boolean postSaveActions(Inspection insp, Document idoc) {
		final String dbarLoc = "HS_BusinessObjects.postSaveActions";
		final boolean localDebug = true;

		boolean puid = false;
		this.saveTimeTrackingDocs(insp, idoc); // Update new ones if any.

		if ( localDebug ) HS_Util.debug("Post save signatureData1 is:[" + insp.getSignatureData1().length() + "] long.", "debug", dbarLoc);
		if ( this.isDebugTablet() ) System.out.println(dbarLoc + ": Post save signatureData1 is:[" + insp.getSignatureData1().length() + "] long.");

		if ( !"".equals(insp.getSignatureData1()) && !"".equals(insp.getSignatureData2()) ) {
			if ( this.isDebugTablet() ) System.out.println(dbarLoc + ": Syncing docs. Signtures are complete...l");
			puid = HS_SyncDocs.pushUpInspectionDetails(idoc);
		} else {
			if ( this.isDebugTablet() ) System.out.println(dbarLoc + ": NOT syncing docs. Signtures are incomplete...l");
		}
		if ( !"".equals(insp.getCreatedFromInspID()) ) {
			if ( this.isDebugTablet() ) System.out.println(dbarLoc + ": pushUpDisplayFollowup being called");
			HS_SyncDocs.pushUpDisplayFollowup(idoc, insp.getCreatedFromInspID());
		}

		/*
		 * Added this section on 22 Sep 14 for task KBIL-9NTGCS "Web App: Completed inspections still showing as due or overdue"
		 * 
		 * newbs.
		 */
		String inspUpdateFacility = HS_Util.getAppSettingString("inspUpdateFacility");

		if ( "onsave".equalsIgnoreCase(inspUpdateFacility) ) {
			// Has it already been done above?
			if ( !puid ) {
				if ( this.isDebugTablet() ) System.out.println(dbarLoc + ": Syncing docs. inspUpdateFacility:[" + inspUpdateFacility + "]");
				puid = HS_SyncDocs.pushUpInspectionDetails(idoc);
			}
		} else {
			if ( this.isDebugTablet() ) System.out.println(dbarLoc + ": NOT syncing docs. inspUpdateFacility:[" + inspUpdateFacility + "]");
		}
		if ( localDebug ) HS_Util.debug("inspUpdateFacility:[" + inspUpdateFacility + "] puid:[" + puid + "]", "debug", dbarLoc);
		if ( this.isDebugTablet() ) System.out.println(dbarLoc + ": inspUpdateFacility:[" + inspUpdateFacility + "] puid:[" + puid + "]");
		/*
		 * end of addition from 22 September 2014
		 */

		/*
		 * NEWBS Added this section on 23 Sep 2016 to suport JIRA task TDA-398 "Generate a Notification in AGILE from an iPad inspection for retail food"
		 */
		try {
			if ( HS_BO_Violations.violationFee(insp) ) {
				// TODO - WHAT to do now?? Save Inspection doc again??
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			if ( insp.isTabletDebug() ) System.out.print(dbarLoc + " ERROR: " + e.toString());
			if ( insp.isTabletDebug() ) e.printStackTrace();
		}
		/*
		 * end of addition from 23 Sep 2016
		 */

		// Update auditForm in case this is an interim save..
		// insp.getLoadedForm()
		insp.setLoadedForm(new HS_auditForm(idoc));
		return puid;
	}

	private void fixDates(Document idoc) {
		// Fix the inspectionDate and newxtInspection items to be date only
		final String msgContext = "HS_BusinessObjects.fixDates";
		final boolean localDebug = false;
		try {
			Vector<String> tflds = new Vector<String>();
			tflds.add("inspectionDate");
			tflds.add("nextInspection");
			tflds.add("ViewDate");
			Iterator<String> fitr = tflds.iterator();
			org.openntf.domino.impl.DateTime ndate = null;
			while (fitr.hasNext()) {
				String fname = fitr.next();
				Vector<Object> vdate = idoc.getItemValue(fname);
				Object wdate = null;
				if ( vdate.size() == 0 ) {
					if ( localDebug ) System.out.println(msgContext + ": " + fname + " is null");
				} else {
					wdate = vdate.get(0);
					if ( wdate.getClass().equals(org.openntf.domino.impl.DateTime.class) ) {
						ndate = (org.openntf.domino.impl.DateTime) wdate;
						if ( localDebug ) System.out.println(msgContext + ": " + fname + " is a DateTime [" + ndate.toString() + "] isAnyTime:[" + ndate.isAnyTime() + "] TZ:[" + ndate.getTimeZone() + "]");
						if ( !ndate.isAnyTime() ) {
							int tz = ndate.getTimeZone();
							ndate.setLocalTime((12 - tz), 0, 0, 0);
							// ndate.setAnyTime();
							idoc.removeItem(fname);
							idoc.replaceItemValue(fname, ndate);
							if ( localDebug ) System.out.println(msgContext + ": " + fname + " replaced with [" + ndate + "]");
						}
					} else {
						if ( localDebug ) System.out.println(msgContext + ": " + fname + " is a:[" + wdate.getClass().getName() + "] [" + wdate.toString() + "]");
					}
				}
			}
		} catch (Exception e) {
			System.out.println(msgContext + " - ERROR: " + e.toString());
		}
	}

	@SuppressWarnings("unchecked")
	private void addToEIBSLog(Inspection insp, Document idoc, String auditEvent) {
		try {
			Date ndt = new Date();
			Session sess = HS_Util.getSession();
			Name nname = sess.createName((String) sess.evaluate("@UserName").get(0));
			String newLogEntry = ndt.toString() + ": " + auditEvent + " - by: " + nname.getCommon();
			Vector<String> eibsLog = (Vector<String>) ((Vector<?>) idoc.getItemValue("EIBSLog"));
			eibsLog.add(0, newLogEntry);
			if ( eibsLog.size() > 100 ) {
				Vector<String> newLog = new Vector<String>();
				for (String le : eibsLog) {
					if ( newLog.size() < 100 ) newLog.add(le);
				}
				updateItem(insp, idoc, "EIBSLog", newLog);
			} else {
				updateItem(insp, idoc, "EIBSLog", eibsLog);
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", "HS_BusinessObjects.addToEIBSLog");
		}
	}

	private boolean updateItem(Inspection insp, Document idoc, String itemName, Object value) {
		final boolean localDebug = false;
		final String dbarLoc = "HS_BusinessObjects.updateItem";
		boolean result = false;
		Item nitem = null;
		try {
			if ( value == null ) value = "";
			nitem = idoc.replaceItemValue(itemName, value);
			if ( itemName.startsWith("SignatureData64") ) nitem.setSummary(false);
			if ( localDebug ) HS_Util.debug("item:[" + itemName + "] updated OK", "debug", dbarLoc);
			result = true;
		} catch (Exception e) {
			HS_Util.debug(e.toString() + " on item:[" + itemName + "]", "error", dbarLoc);
			if ( this.isDebugTablet() ) System.out.print(dbarLoc + ": ERROR: " + e.toString() + " on item:[" + itemName + "]");
		}

		return result;
	}

	private boolean updateItem(Inspection insp, Document idoc, String itemName, DateTime idate) {
		final boolean localDebug = false;
		final String dbarLoc = "HS_BusinessObjects.updateItem - date";
		boolean result = false;
		try {
			if ( idate == null ) {
				idoc.replaceItemValue(itemName, "");
				result = true;
			} else {
				if ( idate.isAnyTime() ) {
					if ( localDebug ) HS_Util.debug("Time zone is: [" + idate.getTimeZone() + "] idate is:[" + idate.toString() + "]", "debug", dbarLoc);
					// idate.setLocalTime(0, 0, 0, 0);
				}
				idoc.replaceItemValue(itemName, idate);
				if ( localDebug ) HS_Util.debug("item:[" + itemName + "] updated OK to:[" + idate.toString() + "]", "debug", dbarLoc);
				result = true;
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString() + " on item:[" + itemName + "]", "error", dbarLoc);
			if ( this.isDebugTablet() ) System.out.print(dbarLoc + ": ERROR: " + e.toString() + " on item:[" + itemName + "]");
		}
		return result;
	}

	private void updateReadersItem(Inspection insp, Document idoc, String itemName, Object value) {
		final String dbarLoc = this.getClass().getName() + ".updateReadersItem";
		try {
			if ( this.updateItem(insp, idoc, itemName, value) ) {
				idoc.getFirstItem(itemName).setReaders(true);
			}
		} catch (Exception e) {
			String emsg = e.toString() + " on item:[" + itemName + "]";
			HS_Util.debug(emsg, "error", dbarLoc);
			if ( this.isDebugTablet() ) System.out.print(dbarLoc + ": " + emsg);
		}
	}

	private void replaceValues(Document idoc, Inspection insp) {
		final boolean localDebug = false;
		final String dbarLoc = this.getClass().getName() + ".replaceValues";
		try {
			Session mySession = HS_Util.getSession();
			DateTime wdt = null;

			updateItem(insp, idoc, "Name", insp.getFacilityName());
			updateItem(insp, idoc, "FacilityType", insp.getFacilityType());
			updateItem(insp, idoc, "DocumentId", insp.getDocumentId());
			updateItem(insp, idoc, "FacilityName", insp.getFacilityName());
			updateItem(insp, idoc, "FacilityPhysicalBuilding", insp.getFacilityPhysicalBuilding());
			updateItem(insp, idoc, "FacilityPhysicalCity", insp.getFacilityPhysicalCity());
			updateItem(insp, idoc, "FacilityPhysicalDirection", insp.getFacilityPhysicalDirection());
			updateItem(insp, idoc, "FacilityPhysicalPostalCode", insp.getFacilityPhysicalPostalCode());
			updateItem(insp, idoc, "FacilityPhysicalProvince", insp.getFacilityPhysicalProvince());
			updateItem(insp, idoc, "FacilityPhysicalRegType", insp.getFacilityPhysicalRegType());
			updateItem(insp, idoc, "FacilityPhysicalStreetName", insp.getFacilityPhysicalStreetName());
			updateItem(insp, idoc, "FacilityPhysicalStreetSuffix", insp.getFacilityPhysicalStreetSuffix());
			updateItem(insp, idoc, "FacilityPhysicalStreetType", insp.getFacilityPhysicalStreetType());
			updateItem(insp, idoc, "FacilityPhysicalSuite", insp.getFacilityPhysicalSuite());
			updateItem(insp, idoc, "FacilityTelephone", insp.getFacilityTelephone());
			updateItem(insp, idoc, "FacilityID2", insp.getFacilityID2());
			updateItem(insp, idoc, "MagDistrict", insp.getMagDistrict());

			// This is confusing. Sometimes it seems like "Owners..." and other
			// times it is "Owner..."
			// TDA is "Owners..."
			updateItem(insp, idoc, "OwnersAddress", insp.getOwnersAddress());
			updateItem(insp, idoc, "OwnersCity", insp.getOwnersCity());
			updateItem(insp, idoc, "OwnersName", insp.getOwnersName());
			updateItem(insp, idoc, "OwnersPostalCode", insp.getOwnersPostalCode());
			updateItem(insp, idoc, "OwnersState", insp.getOwnersState());
			// updateItem(insp, idoc, "OwnersAddress", "");
			// updateItem(insp, idoc, "OwnersCity", "");
			// updateItem(insp, idoc, "OwnersName", "");
			// idoc.replaceItemValue("OwnersPostalCode", "");
			// updateItem(insp, idoc, "OwnersState", "");

			// TODO - Calculate Grade or Hazard Rating
			updateItem(insp, idoc, "LetterGrade", insp.getLetterGrade());

			// various config items here
			// updateItem(insp, idoc, "", insp.getX());
			updateItem(insp, idoc, "BillablePermit", insp.getBillablePermit());
			updateItem(insp, idoc, "Billable", insp.getBillable());
			updateItem(insp, idoc, "ComplianceSet", insp.getComplianceSet());
			updateItem(insp, idoc, "ChildRatio", insp.getChildRatio());
			updateItem(insp, idoc, "ComplaintDate", insp.getComplaintDate());
			updateItem(insp, idoc, "CriticalViolationRanges", insp.getCriticalViolationRanges());
			updateItem(insp, idoc, "DisplayFollowUp", insp.getDisplayFollowUp());
			updateItem(insp, idoc, "EHSID", insp.getEhsId());

			updateReadersItem(insp, idoc, "EHSReaders", insp.getEhsReaders());

			updateItem(insp, idoc, "DropGradeBasedOnNonCorrected", insp.getDgbonc());
			updateItem(insp, idoc, "DTMUHRSC", insp.getDtmuhrsc());
			updateItem(insp, idoc, "DTMULG", insp.getDtmulg());
			updateItem(insp, idoc, "FollowupTypes", insp.getFollowupTypes());
			updateItem(insp, idoc, "GradeBasedOnCriticals", insp.getGboc());
			updateItem(insp, idoc, "GradeRanges", insp.getGradeRanges());
			updateItem(insp, idoc, "GradeComputedByHazRating", insp.getGradeComputedByHazRating());
			updateItem(insp, idoc, "GradeComputedByNumCriticals", insp.getGradeComputedByNumCriticals());

			updateItem(insp, idoc, "HowToCountCriticals", insp.getHtcc());
			updateItem(insp, idoc, "InspectionFollowupTypes", insp.getFollowupTypes());
			updateItem(insp, idoc, "LetterGrades", insp.getLetterGrades());
			updateItem(insp, idoc, "TotHazardRating", insp.getTotHazardRating());
			updateItem(insp, idoc, "UseHazard", insp.getUseHazard());
			updateItem(insp, idoc, "UseHazardRating", insp.getUseHazardRating());

			updateItem(insp, idoc, "AnnouncedInsp", insp.getAnnounceInsp());
			updateItem(insp, idoc, "InspectionOutcome", insp.getInspectionOutcome());

			// if ( !idoc.hasItem("Locked") ) updateItem(insp, idoc, "Locked",
			// "No");
			updateItem(insp, idoc, "Locked", insp.getLocked());

			this.updateSubOffice(idoc, insp);
			HS_BO_Violations.updateViolations(idoc, insp);
			this.updateTemperatureData(idoc, insp);
			this.updateManagerData(idoc, insp);
			HS_BO_Sections.updateSectionData(idoc, insp);

			updateItem(insp, idoc, "ParentForm", insp.getParentForm());
			updateItem(insp, idoc, "ParentId", insp.getParentUnid());
			updateItem(insp, idoc, "PhysicalMunicipality", insp.getPhysicalMunicipality());

			updateItem(insp, idoc, "AllYearRound", insp.getAllYearRound());
			updateItem(insp, idoc, "RiskRating", insp.getRiskRating());

			updateItem(insp, idoc, "CreatedFromInspID", insp.getCreatedFromInspID());
			if ( "Yes".equals(insp.getFollowupInspectionRequired()) && "".equals(insp.getCreatedFromInspID()) ) {
				insp.setDisplayFollowUp("Yes");
			}
			updateItem(insp, idoc, "DisplayFollowUp", insp.getDisplayFollowUp());

			insp.updateViewDescription();
			updateItem(insp, idoc, "viewDescription", insp.getViewDescription());

			if ( "".equals(insp.getInspectionDateStr()) ) {
				updateItem(insp, idoc, "InspectionDate", "");
			} else {
				try {
					wdt = mySession.createDateTime(insp.getInspectionDateStr());
					wdt.setAnyTime();
					if ( localDebug ) HS_Util.debug("Inspection date saved as: wdt[" + wdt.toString() + "]", "debug", dbarLoc);
					if ( this.debugTablet && localDebug ) System.out.print(dbarLoc + " - Inspection date saved as: wdt[" + wdt.toString() + "]");
					updateItem(insp, idoc, "InspectionDate", wdt);
					updateItem(insp, idoc, "ViewDate", wdt);
					idoc.removeItem("InspectionDateError");
					if ( localDebug ) HS_Util.debug("  ------------ doc:[" + idoc.getItemValue("inspectionDate").get(0).toString() + "]", "debug", dbarLoc);
					if ( this.debugTablet && localDebug ) System.out.print(dbarLoc + " ------------- doc:[" + idoc.getItemValue("inspectionDate").get(0).toString() + "]");
				} catch (Exception e1) {
					updateItem(insp, idoc, "InspectionDateError", "On convert inspectionDateStr:[" + insp.getInspectionDateStr() + "]: " + e1.toString());
					HS_Util.debug("On convert inspectionDateStr:[" + insp.getInspectionDateStr() + "]: " + e1.toString(), "error", dbarLoc);
				}
			}

			// updateItem(insp, idoc, "Comments", insp.getComments()); // Saved
			// as RichText in addImagesToComments
			updateItem(insp, idoc, "Enforcement", insp.getEnforcement());

			updateItem(insp, idoc, "FoodSafe", insp.getFoodSafe());
			updateItem(insp, idoc, "CertifiedManager", insp.getCertifiedManager());
			updateItem(insp, idoc, "Certified", insp.getCertified());
			updateItem(insp, idoc, "CertNumber", insp.getCertNumber());
			updateItem(insp, idoc, "CertExpiration", insp.getCertExpiration());
			updateItem(insp, idoc, "ChokingPoster", insp.getChokingPoster());
			updateItem(insp, idoc, "Smoking", insp.getSmoking());
			updateItem(insp, idoc, "LicenseDisplayed", insp.getLicenseDisplayed());
			updateItem(insp, idoc, "ObservedSeating", insp.getObservedSeating());
			// if (HS_Util.isDebugServer()) HS_Util.debug("observedSeating is:["
			// + insp.getObservedSeating() + "]", "debug", dbarLoc);

			if ( insp.getRelease() == null ) {
				updateItem(insp, idoc, "Release", "");
			} else {
				wdt = mySession.createDateTime(insp.getRelease());
				wdt.setAnyTime();
				updateItem(insp, idoc, "Release", wdt);
			}
			updateItem(insp, idoc, "EHO", insp.getEho());
			updateItem(insp, idoc, "Type", insp.getInspType());
			updateItem(insp, idoc, "FollowupInspectionRequired", insp.getFollowupInspectionRequired());
			updateItem(insp, idoc, "ReceivedBy", insp.getReceivedBy());
			updateItem(insp, idoc, "ReceivedByTitle", insp.getReceivedByTitle());

			if ( "TemporaryVendorFacility".equalsIgnoreCase(insp.getParentForm()) ) {
				updateItem(insp, idoc, "EventEndDate", insp.getEventEndDate());
				updateItem(insp, idoc, "EventStartDate", insp.getEventStartDate());
				updateItem(insp, idoc, "EventId", insp.getEventId());
				updateItem(insp, idoc, "EventName", insp.getEventName());
			}
			if ( insp.getNextInspectionDate() == null ) {
				updateItem(insp, idoc, "NextInspection", "");
			} else if ( "".equals(insp.getNextInspectionDateStr()) ) {
				updateItem(insp, idoc, "NextInspection", "");
			} else {
				try {
					wdt = mySession.createDateTime(insp.getNextInspectionDateStr());
					wdt.setAnyTime();
					updateItem(insp, idoc, "NextInspection", wdt);
					if ( this.debugTablet )
						System.out.print(dbarLoc + " ------------- doc:[" + idoc.getItemValue("NextInspection").get(0).toString() + "] wdt:[" + wdt.toString() + "] insp value:["
												+ insp.getNextInspectionDateStr() + "]");
				} catch (Exception e1) {
					throw (new Exception("On convert nextInspectionDateStr: " + e1.toString()));
				}
			}

			if ( !idoc.hasItem("HazardRatingType") ) {
				updateItem(insp, idoc, "HazRatingType", insp.getHazRatingType());
				// updateItem(insp, idoc, "HazRatingType", "Observation");
			}

			// updateItem(insp, idoc, "TimeInspStr", insp.getTimeInspStr());
			// updateItem(insp, idoc, "TimeTravelStr", insp.getTimeTravelStr());

			updateItem(insp, idoc, "SignatureData1", insp.getSignatureData1());
			updateItem(insp, idoc, "SignatureData2", insp.getSignatureData2());
			updateItem(insp, idoc, "HasSig1", insp.getHasSig1());
			updateItem(insp, idoc, "HasSig2", insp.getHasSig2());
			updateItem(insp, idoc, "SignatureHistory", insp.getSignatureHistory());
			updateItem(insp, idoc, "SignatureData64_1", insp.getSignatureData64_1());
			updateItem(insp, idoc, "SignatureData64_2", insp.getSignatureData64_2());

			updateItem(insp, idoc, "BillingType", insp.getBillingType());
			updateItem(insp, idoc, "NextManualDue", insp.getNextManualDue());
			updateItem(insp, idoc, "NextInspDue", insp.getNextInspDue());

			updateItem(insp, idoc, "EmailSent", insp.getEmailSent());

			// if ( "LaborCampReport".equalsIgnoreCase(insp.getForm()) ) {
			updateItem(insp, idoc, "FacilityApprovedNumber", insp.getFacilityApprovedNumber());
			updateItem(insp, idoc, "FacilityRegType", insp.getFacilityRegType());
			// }

			// Adding WebApproval if this is doc does not have it. - 10.Mar.2015
			if ( !idoc.hasItem("WebApproval") ) updateItem(insp, idoc, "WebApproval", "Yes");

			if ( this.debugTablet && localDebug ) System.out.print(dbarLoc + " - looking at inspection.FullCloudMap");
			Map<String, Object> fcm = insp.getFullCloudMap();
			String[] ignoreKeys = { "record_finalized", "record_inspectiontype", "record_inspectorid", "record_score", "record_signaturedata_1", "record_signaturedata_2", "record_synced",
									"record_generalcomments", "record_comments", "record_inspectiontypeid", "record_inspectionid", "record_locationid" };
			String[] translateFnames = { "numrepeats:numrepeat", "numpriorityfoundationitems:numpfoundation", "numcoreitems:numcore", "numpriorityitems:numpriority" };
			// String[] ignoreKeys = { "record_inspectionid",
			// "record_locationid", "record_generalcomments", "record_score",
			// "record_signaturedata_1",
			// "record_signaturedata_2", "record_finalize", "record_" };
			if ( fcm != null ) {
				for (String key : fcm.keySet()) {
					if ( key.startsWith("record_") ) {
						Object fcv = fcm.get(key);
						String fname = translateFldName(key.substring(7), translateFnames);
						if ( idoc.hasItem(fname) ) {
							if ( this.debugTablet && localDebug ) System.out.print(dbarLoc + " skipped key:[" + key + "] already on document");
						} else if ( HS_Util.isMember(key, ignoreKeys) ) {
							if ( this.debugTablet && localDebug ) System.out.print(dbarLoc + " skipped key:[" + key + "] It is in ignoreKeys os I think we already used it");
						} else {
							updateItem(insp, idoc, fname, fcv);
							if ( this.debugTablet && localDebug ) System.out.print(dbarLoc + " added other item:[" + fname + "] from key:[" + key + "]");
						}
					}
				}
			}

			// Time Spent?? rules/????
			updateItem(insp, idoc, "TimeSpent", insp.getTimeSpent());

			if ( insp.isUseTobacco() ) this.updateTobaccoItems(insp, idoc);

			this.addImagesToComments(idoc, insp);

			if ( localDebug ) HS_Util.debug("completed replacing values", "debug", dbarLoc);
			if ( this.debugTablet && localDebug ) System.out.print(dbarLoc + " - completed replacing values");
		} catch (Exception e) {
			// TODO somethign else
			e.printStackTrace();
			HS_Util.debug(e.toString(), "error", dbarLoc);
		}
	}

	// private void updateHazardRatingType(Inspection insp, Document idoc) {
	// /*
	// * HRMod = MasterSettingsDoc.GetItemValue("HazardRatingModules") HRType = MasterSettingsDoc.GetItemValue("HazardRatingType") HRStartingScore = MasterSettingsDoc.GetItemValue("StartingScore") For m = 0
	// * To Ubound(HRMod) If HRMod(m) = Doc.Form(0) Then Select Case HRType(m) Case "Section", "By Section" Call Doc.ReplaceItemValue("HazRatingType","Section") Case "SectionCategory", "By Section Category"
	// * Call Doc.ReplaceItemValue("HazRatingType","SectionCategory") Case "Code", "By Code" Call Doc.ReplaceItemValue("HazRatingType","Code") Case "Observation", "By Observation" Call
	// * Doc.ReplaceItemValue("HazRatingType","Observation") End Select Call Doc.ReplaceItemValue("TotHazardRating",HRStartingScore(m)) Exit For End If Next
	// */
	// Vector<String> hrMod = HS_Util.getMasterSettingsVector("HazardRatingModules");
	// Vector<String> hrType = HS_Util.getMasterSettingsVector("HazardRatingType");
	// // Vector <String >hrStartingScore =
	// // HS_Util.getMasterSettingsVector("StartingScore");
	// for (int m = 0; m < hrMod.size(); m++) {
	// if ( idoc.getItemValueString("Form").equals(hrMod.get(m)) ) {
	// if ( "Section".equals(hrType.get(m)) || "By Section".equals(hrType.get(m)) ) {
	// updateItem(insp, idoc, "HazRatingType", "Section");
	// } else if ( "SectionCategory".equals(hrType.get(m)) || "By Section Category".equals(hrType.get(m)) ) {
	// updateItem(insp, idoc, "HazRatingType", "SectionCategory");
	// } else if ( "Code".equals(hrType.get(m)) || "By Code".equals(hrType.get(m)) ) {
	// updateItem(insp, idoc, "HazRatingType", "Code");
	// } else if ( "Observation".equals(hrType.get(m)) || "By Observation".equals(hrType.get(m)) ) {
	// updateItem(insp, idoc, "HazRatingType", "Observation");
	// }
	// }
	// }
	// }

	private String translateFldName(String fname, String[] translateFnames) {
		String retName = fname;
		for (String tname : translateFnames) {
			String[] wkey = tname.split(":");
			if ( wkey[0].equals(fname) ) {
				retName = wkey[1];
			}
		}
		return retName;
	}

	/*
	 * This routine will remove the comments text item and replace with a RichText item that includes links to the images.
	 * 
	 * This will probably break the XPages Inspection editors!!
	 */
	private void addImagesToComments(Document idoc, Inspection insp) {
		// if ( insp.getImageUnids().size() == 0 ) return; // nothing more to
		// add...
		final String msgContext = this.getClass().getName() + ".addImagesToComments";

		try {
			String comments = insp.getComments();
			idoc.removeItem("Comments");
			RichTextItem rtcomments = idoc.createRichTextItem("Comments");
			rtcomments.appendText(comments);
			if ( insp.getImageUnids().size() == 0 ) return; // nothing more to
			// add...
			rtcomments.addNewLine(2, true);
			int ictr = 0;
			rtcomments.appendText("Images uploaded with the inspection:");
			rtcomments.addNewLine(2, true);
			for (String iunid : insp.getImageUnids()) {
				ictr++;
				Document imagedoc = idoc.getParentDatabase().getDocumentByUNID(iunid);
				rtcomments.appendText("Image " + ictr + ": ");
				rtcomments.appendDocLink(imagedoc, "Image " + ictr);
				rtcomments.appendText(" " + imagedoc.getItemValueString("imageNotes"));
				rtcomments.addNewLine(1, true);
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private void loadValues(Inspection insp, Document idoc) {
		final boolean localDebug = false;
		final String dbarLoc = "HS_BusinessObjects.loadValues";

		try {
			insp.setAllYearRound(HS_Util.loadValueString(idoc, "AllYearRound"));
			insp.setComments(HS_Util.loadValueString(idoc, "Comments"));
			insp.setCreatedFromInspID(HS_Util.loadValueString(idoc, "CreatedFromInspID"));
			insp.setDateCreated(HS_Util.loadValueDate(idoc, "DateCreated"));
			insp.setDisplayFollowUp(HS_Util.loadValueString(idoc, "DisplayFollowUp"));
			insp.setDbDesign(HS_Util.loadValueString(idoc, "dbDesign"));
			insp.setDocumentId(HS_Util.loadValueString(idoc, "documentId"));
			insp.setEho(HS_Util.loadValueString(idoc, "EHO"));
			if ( localDebug ) HS_Util.debug("eho initially loaded as:[" + insp.getEho() + "]", "debug", dbarLoc);

			insp.setEnforcement(HS_Util.loadValueString(idoc, "Enforcement"));
			insp.setFacilityName(HS_Util.loadValueString(idoc, "FacilityName"));
			if ( "".equals(insp.getFacilityName()) ) {
				insp.setFacilityName(HS_Util.loadValueString(idoc, "Name"));
			}
			insp.setFacilityPhysicalBuilding(HS_Util.loadValueString(idoc, "FacilityPhysicalBuilding"));
			insp.setFacilityPhysicalCity(HS_Util.loadValueString(idoc, "FacilityPhysicalCity"));
			insp.setFacilityPhysicalDirection(HS_Util.loadValueString(idoc, "FacilityPhysicalDirection"));
			insp.setFacilityPhysicalPostalCode(HS_Util.loadValueString(idoc, "FacilityPhysicalPostalCode"));
			insp.setFacilityPhysicalProvince(HS_Util.loadValueString(idoc, "FacilityPhysicalProvince"));
			insp.setFacilityPhysicalRegType(HS_Util.loadValueString(idoc, "FacilityPhysicalRegType"));
			insp.setFacilityPhysicalStreetName(HS_Util.loadValueString(idoc, "FacilityPhysicalStreetName"));
			insp.setFacilityPhysicalStreetSuffix(HS_Util.loadValueString(idoc, "FacilityPhysicalStreetSuffix"));
			insp.setFacilityPhysicalSuite(HS_Util.loadValueString(idoc, "FacilityPhysicalSuite"));
			insp.setFacilityTelephone(HS_Util.loadValueString(idoc, "FacilityTelephone"));
			insp.setFacilityType(HS_Util.loadValueString(idoc, "FacilityType"));
			insp.setMagDistrict(HS_Util.loadValueString(idoc, "MagDistrict"));

			insp.setFollowupInspectionRequired(HS_Util.loadValueString(idoc, "FollowupInspectionRequired"));
			insp.setInspType(HS_Util.loadValueString(idoc, "Type"));
			// insp.setLetterGrade(HS_Util.loadValueString(idoc,
			// "LetterGrade"));

			insp.setInspectionDate(HS_Util.loadValueDate(idoc, "InspectionDate"));
			insp.setInspectionDateStr("");
			if ( insp.getInspectionDate() != null ) {
				// USe alternate format with bootstrap
				// insp.setInspectionDateStr(HS_Util.formatDate(insp.getInspectionDate(),
				// "yyyy-MM-dd"));
				insp.setInspectionDateStr(HS_Util.formatDate(insp.getInspectionDate(), this.getStandardDateMask()));

			}

			insp.setNextInspectionDate(HS_Util.loadValueDate(idoc, "NextInspection"));
			insp.setNextInspectionDateStr("");
			if ( insp.getNextInspectionDate() != null ) {
				// insp.setNextInspectionDateStr(HS_Util.formatDate(insp.getNextInspectionDate(),
				// "yyyy-MM-dd"));
				insp.setNextInspectionDateStr(HS_Util.formatDate(insp.getNextInspectionDate(), this.getStandardDateMask()));
			}

			insp.setNumCore(HS_Util.loadValueInt(idoc, "NumCore"));
			insp.setNumCritical(HS_Util.loadValueInt(idoc, "NumCritical"));
			insp.setNumKey(HS_Util.loadValueInt(idoc, "NumKey"));
			insp.setNumNonCritical(HS_Util.loadValueInt(idoc, "NumNonCritical"));
			insp.setNumOther(HS_Util.loadValueInt(idoc, "NumOther"));
			insp.setNumPFoundation(HS_Util.loadValueInt(idoc, "NumPFoundation"));
			insp.setNumPriority(HS_Util.loadValueInt(idoc, "NumPriority"));
			insp.setNumRepeat(HS_Util.loadValueInt(idoc, "NumRepeat"));
			insp.setNumRisk(HS_Util.loadValueInt(idoc, "NumRisk"));
			insp.setTotalViolations(HS_Util.loadValueInt(idoc, "TotalViolations"));
			insp.setTotHazardRating(HS_Util.loadValueInt(idoc, "TotHazardRating"));

			insp.setOwnerId(HS_Util.loadValueString(idoc, "ParentId"));
			insp.setOwnersAddress(HS_Util.loadValueString(idoc, "OwnerAddress"));
			insp.setOwnersCity(HS_Util.loadValueString(idoc, "OwnerCity"));
			insp.setOwnersName(HS_Util.loadValueString(idoc, "OwnerName"));
			insp.setOwnersPostalCode(HS_Util.loadValueString(idoc, "OwnerPostalCode"));
			insp.setOwnersState(HS_Util.loadValueString(idoc, "OwnerState"));

			insp.setParentForm(HS_Util.loadValueString(idoc, "ParentForm"));
			insp.setParentUnid(HS_Util.loadValueString(idoc, "ParentId"), false);
			insp.setPhysicalMunicipality(HS_Util.loadValueString(idoc, "PhysicalMunicipality"));
			insp.setRelease(HS_Util.loadValueDate(idoc, "Release"));
			insp.setRiskRating(HS_Util.loadValueString(idoc, "RiskRating"));
			insp.setViewDescription(HS_Util.loadValueString(idoc, "ViewDescription"));
			insp.setViolationSection(HS_Util.loadValueString(idoc, "violationSection"));
			insp.setViolationSubModule((Vector<String>) (Vector<?>) (idoc.getItemValue("ViolationSubModule")));

			insp.setViolationModule((Vector<String>) (Vector<?>) (idoc.getItemValue("ViolationModule")));

			insp.setFoodSafe(HS_Util.loadValueString(idoc, "FoodSafe"));

			insp.setCertifiedManager(HS_Util.loadValueString(idoc, "CertifiedManager"));
			insp.setCertified(HS_Util.loadValueString(idoc, "Certified"));
			insp.setCertNumber(HS_Util.loadValueString(idoc, "CertNumber"));
			insp.setCertExpiration(HS_Util.loadValueString(idoc, "CertExpiration"));

			insp.setChokingPoster(HS_Util.loadValueString(idoc, "ChokingPoster"));
			insp.setSmoking(HS_Util.loadValueString(idoc, "Smoking"));
			insp.setLicenseDisplayed(HS_Util.loadValueString(idoc, "LicenseDisplayed"));
			insp.setObservedSeating(HS_Util.loadValueInt(idoc, "ObservedSeating"));

			this.loadTemperatureData(insp, idoc);
			this.loadManagerData(insp, idoc);
			this.loadTobaccoItems(insp, idoc);

			// insp.setTimeInspStr(HS_Util.loadValueString(idoc,
			// "TimeInspStr"));
			// insp.setTimeTravelStr(HS_Util.loadValueString(idoc,"TimeTravelStr"));

			insp.setSignatureData1(HS_Util.loadValueString(idoc, "SignatureData1"));
			insp.setSignatureData2(HS_Util.loadValueString(idoc, "SignatureData2"));
			insp.setSignatureHistory(HS_Util.loadValueStringArray(idoc, "SignatureHistory"));
			insp.setSignatureData64_1(HS_Util.loadValueString(idoc, "SignatureData64_1"));
			insp.setSignatureData64_2(HS_Util.loadValueString(idoc, "SignatureData64_2"));
			insp.setSource(HS_Util.loadValueString(idoc, "Source"));
			insp.setReceivedBy(HS_Util.loadValueString(idoc, "ReceivedBy"));
			insp.setReceivedByTitle(HS_Util.loadValueString(idoc, "ReceivedByTitle"));

			insp.setBillingType(HS_Util.loadValueString(idoc, "BillingType"));
			insp.setNextManualDue(HS_Util.loadValueDate(idoc, "NextManualDate"));
			insp.setNextInspDue(HS_Util.loadValueDate(idoc, "NextInspDue"));
			insp.setLocked(HS_Util.loadValueString(idoc, "Locked"));

			if ( "TemporaryVendorFacility".equalsIgnoreCase(insp.getParentForm()) ) {
				insp.setEventStartDate(HS_Util.loadValueDate(idoc, "EventStartDate"));
				insp.setEventEndDate(HS_Util.loadValueDate(idoc, "EventEndDate"));
				insp.setEventId(HS_Util.loadValueString(idoc, "EventId"));
				insp.setEventName(HS_Util.loadValueString(idoc, "EventName"));
			}
			if ( "LaborCampReport".equalsIgnoreCase(insp.getForm()) ) {
				insp.setFacilityApprovedNumber(HS_Util.loadValueString(idoc, "FacilityApprovedNumber"));
				insp.setFacilityRegType(HS_Util.loadValueString(idoc, "FacilityRegType"));
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}
	}

	// this version is for reading in the values from the last inspection.
	public void loadTemperatureData(Inspection insp, String iunid, Database pdb) {
		final String dbarLoc = "HS_BusinessObjects.loadTemperatureData";
		final boolean localDebug = false;
		Document lastInspDoc = pdb.getDocumentByUNID(iunid);
		if ( lastInspDoc == null ) {
			HS_Util.debug("Unable to access inspection document with unid:[" + iunid + "]", "error", dbarLoc);
			return;
		} else {
			this.loadTemperatureData(insp, lastInspDoc);
			if ( localDebug ) HS_Util.debug("EquipDescrip:" + insp.getEquipDescrip().toString() + "", "debug", dbarLoc);
			insp.setEquipTemp(new Vector<String>());

			// Per task MKIR-9L6F6X from 17Jun2014 - Food temperatures should
			// not be copied over.
			// if (localDebug) HS_Util.debug("FoodDescrip:" +
			// insp.getFoodDescrip().toString() + "", "debug", dbarLoc);
			// insp.setFoodTemp(new Vector<String>());

			if ( localDebug ) HS_Util.debug("MachineName:" + insp.getMachineName().toString() + "", "debug", dbarLoc);
			insp.setWareWashTemp(new Vector<String>());
		}
	}

	public void loadTemperatureData(Inspection insp, Document idoc) {
		final String dbarLoc = "HS_BusinessObjects.loadTemperatureData";
		final boolean localDebug = false;
		// Temperature Arrays
		insp.setEquipDescrip(HS_Util.loadValueStringArray(idoc, "EquipDescrip"));
		insp.setEquipTemp(HS_Util.loadValueStringArray(idoc, "EquipTemp"));

		if ( localDebug ) HS_Util.debug("insp.getUnid():[" + insp.getUnid() + "]", "debug", dbarLoc);

		// Per task MKIR-9L6F6X from 17Jun2014 - Food temperatures should not be
		// copied over.
		if ( insp.getUnid() == null ) {
			insp.setFoodDescrip(new Vector<String>());
			insp.setFoodTemp(new Vector<String>());
			insp.setFoodState(new Vector<String>());
		} else {
			insp.setFoodDescrip(HS_Util.loadValueStringArray(idoc, "FoodDescrip"));
			insp.setFoodTemp(HS_Util.loadValueStringArray(idoc, "FoodTemp"));
			insp.setFoodState(HS_Util.loadValueStringArray(idoc, "FoodState"));
		}

		ArrayList<String> fnames = new ArrayList<String>() {
			{
				add("MachineName");
				add("SaniMethod");
				add("SaniName");
				add("SaniType");
				add("ThermoLabel");
				add("Ppm");
				add("WareWashTemp");
			}
		};
		Vector<String> wwdata = new Vector<String>();
		String wwval = "";
		int maxrow = -1;
		try {
			for (String fname : fnames) {
				// If this is wisconsin and the data has not been changed or
				// fixed....
				// Per task MKIR-9L6F6X from 17Jun2014 - Ware washing
				// temperatures should not be copied over.
				if ( insp.getUnid() == null ) {
					wwdata = new Vector<String>();
				} else {
					if ( "Wisconsin".equalsIgnoreCase(insp.getDbDesign()) && !(idoc.hasItem(fnames.get(0))) ) {
						wwdata = new Vector<String>();
						for (int i = 1; i < 50; i++) {
							wwval = "";
							if ( idoc.hasItem(fname + i) ) wwval = idoc.getItemValueString(fname + i);
							if ( fname.equals(fnames.get(0)) ) {
								if ( !("".equals(wwval)) ) {
									wwdata.add(wwval);
									maxrow = i;
								}
							} else if ( i <= maxrow ) {
								wwdata.add(wwval);
							}
						}
					} else {
						wwdata = HS_Util.loadValueStringArray(idoc, fname);
					}
				}
				try {
					Method smethod = insp.getClass().getMethod("set" + fname, Vector.class);
					smethod.invoke(insp, wwdata);
				} catch (SecurityException e) {
					HS_Util.debug("on:[set" + fname + "] SecurityException error: " + e.toString(), "error", dbarLoc);
				} catch (NoSuchMethodException e) {
					HS_Util.debug("on:[set" + fname + "] NoSuchMethodException error: " + e.toString(), "error", dbarLoc);
				} catch (IllegalArgumentException e) {
					HS_Util.debug("on:[set" + fname + "] IllegalArgumentException error: " + e.toString(), "error", dbarLoc);
				} catch (IllegalAccessException e) {
					HS_Util.debug("on:[set" + fname + "] IllegalAccessException error: " + e.toString(), "error", dbarLoc);
				} catch (InvocationTargetException e) {
					HS_Util.debug("on:[set" + fname + "] InvocationTargetException error: " + e.toString(), "error", dbarLoc);
				}
			}
		} catch (Exception e) {
			HS_Util.debug("general error: " + e.toString(), "error", dbarLoc);
			insp.setMachineName(HS_Util.loadValueStringArray(idoc, "MachineName"));
			insp.setSaniMethod(HS_Util.loadValueStringArray(idoc, "SaniMethod"));
			insp.setSaniName(HS_Util.loadValueStringArray(idoc, "SaniName"));
			insp.setSaniType(HS_Util.loadValueStringArray(idoc, "SaniType"));
			insp.setThermoLabel(HS_Util.loadValueStringArray(idoc, "ThermoLabel"));
			insp.setPpm(HS_Util.loadValueStringArray(idoc, "PPM"));
			insp.setWareWashTemp(HS_Util.loadValueStringArray(idoc, "WareWashTemp"));
		}
	}

	public void loadManagerData(Inspection insp, String iunid, Database pdb) {
		final String dbarLoc = "HS_BusinessObjects.loadManagerData";
		final boolean localDebug = false;
		Document lastInspDoc = pdb.getDocumentByUNID(iunid);
		if ( lastInspDoc == null ) {
			HS_Util.debug("Unable to access inspection document with unid:[" + iunid + "]", "error", dbarLoc);
			return;
		} else {
			this.loadManagerData(insp, lastInspDoc);
			if ( localDebug ) HS_Util.debug("Managers:" + insp.getManager().toString() + "", "debug", dbarLoc);
		}
	}

	@SuppressWarnings("unchecked")
	public void loadManagerData(Inspection insp, Document idoc) {
		final boolean localDebug = false;
		final String dbarLoc = "HS_BusinessObjects.loadManagerData";

		try {
			insp.setManager(HS_Util.loadValueStringArray(idoc, "Manager"));
			insp.setManagerAddress(HS_Util.loadValueStringArray(idoc, "ManagerAddress"));

			Vector<String> mcexp = (Vector<String>) (Vector<?>) (idoc.getItemValue("ManagerCertificateExpiration"));
			Vector<String> mcexpStr = new Vector<String>();
			Date wdt = null;
			for (Object wstr : mcexp) {
				if ( wstr instanceof String ) {
					if ( "".equals(((String) wstr).trim()) ) {
						mcexpStr.add(" ");
					} else {
						wdt = HS_Util.cvrtStringToDate((String) wstr);
						// mcexpStr.add(HS_Util.formatDate(wdt, "yyyy-MM-dd"));
						mcexpStr.add(HS_Util.formatDate(wdt, this.getStandardDateMask()));
					}
				} else if ( wstr instanceof DateTime ) {
					mcexpStr.add(HS_Util.formatDate(((DateTime) wstr).toJavaDate(), this.getStandardDateMask()));
				} else {
					HS_Util.debug("wstr is a:[" + wstr.getClass().getName() + "]", "warn", dbarLoc);
				}
			}
			if ( localDebug ) HS_Util.debug("ManagerCertificateExpirationStr set to:[" + mcexpStr + "] from:[" + mcexp + "]", "debug", dbarLoc);
			insp.setManagerCertificateExpirationStr(mcexpStr);
			insp.setManagerCertificateId(HS_Util.loadValueStringArray(idoc, "ManagerCertificateId"));
			insp.setManagerId(HS_Util.loadValueStringArray(idoc, "ManagerId"));
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}
	}

	private void updateTemperatureData(Document idoc, Inspection insp) {
		// final String msgContext = "HS_BusinessObjects.updateTemperatureData";

		updateItem(insp, idoc, "EquipDescrip", insp.getEquipDescrip());
		updateItem(insp, idoc, "EquipTemp", insp.getEquipTemp());
		updateItem(insp, idoc, "EquipDescripE", "");
		updateItem(insp, idoc, "EquipIDE", "");
		updateItem(insp, idoc, "EquipID", "");
		updateItem(insp, idoc, "EquipTempE", "");

		updateItem(insp, idoc, "FoodDescrip", insp.getFoodDescrip());
		updateItem(insp, idoc, "FoodTemp", insp.getFoodTemp());
		updateItem(insp, idoc, "FoodState", insp.getFoodState());
		updateItem(insp, idoc, "FoodDescripE", "");
		updateItem(insp, idoc, "FoodDescripIDE", "");
		updateItem(insp, idoc, "FoodID", "");
		updateItem(insp, idoc, "FoodIDE", "");
		updateItem(insp, idoc, "FoodStateE", "");
		updateItem(insp, idoc, "FoodTempE", "");
		updateItem(insp, idoc, "FoodTempIDE", "");

		updateItem(insp, idoc, "MachineID", "");
		updateItem(insp, idoc, "MachineIDE", "");
		updateItem(insp, idoc, "MachineName", insp.getMachineName());
		updateItem(insp, idoc, "MachineNameE", "");
		updateItem(insp, idoc, "MachineNameIDE", "");
		updateItem(insp, idoc, "SaniMethod", insp.getSaniMethod());
		updateItem(insp, idoc, "SaniMethodE", "");
		updateItem(insp, idoc, "SaniName", insp.getSaniName());
		updateItem(insp, idoc, "SaniNameE", "");
		updateItem(insp, idoc, "SaniType", insp.getSaniType());
		updateItem(insp, idoc, "SaniTypeE", "");
		updateItem(insp, idoc, "ThermoLabel", insp.getThermoLabel());
		updateItem(insp, idoc, "ThermoLabelE", "");
		updateItem(insp, idoc, "PPM", insp.getPpm());
		updateItem(insp, idoc, "PPME", "");
		updateItem(insp, idoc, "WareWashTemp", insp.getWareWashTemp());
		updateItem(insp, idoc, "WareWashTempE", "");

	}

	private void updateManagerData(Document idoc, Inspection insp) {
		final boolean localDebug = false;
		final String dbarLoc = "HS_BusinessObjects.updateManagerData";

		try {
			// String dateMask = (insp.isHSTouch()) ? "yyyy-mm-dd" : this.getStandardDateMask();
			idoc.replaceItemValue("Manager", insp.getManager());
			idoc.replaceItemValue("ManagerAddress", insp.getManagerAddress());
			idoc.replaceItemValue("ManagerCertificateId", insp.getManagerCertificateId());
			idoc.replaceItemValue("ManagerId", insp.getManagerId());
			Vector<DateTime> mcexp = new Vector<DateTime>();
			Vector<String> mcexpStr = insp.getManagerCertificateExpirationStr();
			if ( mcexpStr == null ) mcexpStr = new Vector<String>();
			DateTime wdt = null;
			for (String wstr : mcexpStr) {
				if ( "".equals(wstr.trim()) ) {
					mcexp.add(null);
				} else {
					try {
						// mcexp.add(HS_Util.formatDate(HS_Util.cvrtStringToDate(wstr), this.getStandardDateMask()));
						// mcexp.add(HS_Util.formatDate(HS_Util.cvrtStringToDate(wstr), dateMask));
						wdt = HS_Util.cvrtStringToDateTime(wstr);
						mcexp.add(wdt); // .formatDate(HS_Util.cvrtStringToDate(wstr), dateMask));
						if ( insp.isHSTouch() && localDebug ) System.out.print(dbarLoc + ": date string:[" + wstr + "] converted to date:[" + wdt.toString() + "]");
					} catch (Exception e1) {
						HS_Util.debug("wstr:[" + wstr + "] caused error: " + e1.toString(), "error", dbarLoc);
					}
				}
			}
			idoc.replaceItemValue("ManagerCertificateExpiration", mcexp);
			if ( localDebug ) idoc.replaceItemValue("ManagerCertificateExpirationStr", mcexpStr);
			// if ( localDebug ) HS_Util.debug("ManagerCertificateExpiration saved as:[" + mcexp + "]", "debug", dbarLoc);
			if ( insp.isHSTouch() && localDebug ) System.out.print(dbarLoc + ":ManagerCertificateExpiration saved as:[" + mcexp.toString() + "]");

			// This element is not on all inspections, in fact is specific to Michigan NW HD
			if ( insp.getManagerAllergenTraining() != null && !"".equals(insp.getManagerAllergenTraining().get(0)) ) {
				idoc.replaceItemValue("AllergenTraining", insp.getManagerAllergenTraining());
			}

		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}
	}

	private void updateTobaccoItems(Inspection insp, Document idoc) {
		updateItem(insp, idoc, "TobaccoSold", insp.getTobaccoSold());
		updateItem(insp, idoc, "TobaccoProducts2", insp.getTobaccoProducts2());
		updateItem(insp, idoc, "TobaccoSign", insp.getTobaccoSign());
		updateItem(insp, idoc, "TobaccoSold", insp.getTobaccoSold());
		updateItem(insp, idoc, "TobaccoProducts", insp.getTobaccoProducts());
		updateItem(insp, idoc, "TobaccoMachine", insp.getTobaccoMachine());
		updateItem(insp, idoc, "TobaccoObserved", insp.getTobaccoObserved());
		updateItem(insp, idoc, "TobaccoCounter", insp.getTobaccoCounter());
	}

	private void updateSubOffice(Document idoc, Inspection insp) {
		final boolean localDebug = false;
		final String dbarLoc = "HS_BusinessObjects.updateSubOffice";

		ObjectObject ehoData = insp.getEhoData();
		if ( ehoData != null ) {
			try {
				String parea = ehoData.get("phoneArea").stringValue();
				String pnum = ehoData.get("phoneNumber").stringValue();
				if ( "".equals(parea) && "".equals(pnum) ) {
					// nothing
				} else if ( "".equals(parea) ) {
					// nothing
				} else {
					pnum = "(" + parea + ") " + pnum;
				}
				updateItem(insp, idoc, "EHOPhoneNumber", pnum);
				updateItem(insp, idoc, "SubOffice", ehoData.get("subOffice").stringValue());
				updateItem(insp, idoc, "SubOfficeAddress", ehoData.get("subOfficeAddress").stringValue());
				updateItem(insp, idoc, "SubOfficeCity", ehoData.get("subOfficeCity").stringValue());
				updateItem(insp, idoc, "SubOfficePostalCode", ehoData.get("subOfficePostalCode").stringValue());
				updateItem(insp, idoc, "SubOfficePhone", ehoData.get("subOfficePhone").stringValue());
				if ( localDebug ) HS_Util.debug("done", "debug", dbarLoc);
			} catch (InterpretException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				HS_Util.debug(e.toString(), "error", dbarLoc);
			}
		} else {
			if ( localDebug ) HS_Util.debug("No EHO data found", "debug", dbarLoc);
			updateItem(insp, idoc, "EHOPhoneNumber", "");
			updateItem(insp, idoc, "SubOffice", "");
			updateItem(insp, idoc, "SubOfficeAddress", "");
			updateItem(insp, idoc, "SubOfficeCity", "");
			updateItem(insp, idoc, "SubOfficePostalCode", "");
			updateItem(insp, idoc, "SubOfficePhone", "");
		}
	}

	private ObjectObject loadSubOffice(Inspection insp, Document idoc) {
		// final boolean localDebug = false;
		// final String dbarLoc = "HS_BusinessObjects.loadSubOffice";

		ObjectObject ehoData = insp.loadEhoData(insp.getEho());
		return ehoData;
	}

	private void loadTobaccoItems(Inspection insp, Document idoc) {
		insp.setUseTobacco(idoc.hasItem("TobaccoSold"));
		if ( insp.isUseTobacco() ) {
			insp.setTobaccoSold(HS_Util.loadValueString(idoc, "TobaccoSold"));
			insp.setTobaccoProducts2(HS_Util.loadValueString(idoc, "TobaccoProducts2"));
			insp.setTobaccoSign(HS_Util.loadValueString(idoc, "TobaccoSign"));
			insp.setTobaccoProducts(HS_Util.loadValueString(idoc, "TobaccoProducts"));
			insp.setTobaccoMachine(HS_Util.loadValueString(idoc, "TobaccoMachine"));
			insp.setTobaccoObserved(HS_Util.loadValueString(idoc, "TobaccoObserved"));
			insp.setTobaccoCounter(HS_Util.loadValueString(idoc, "TobaccoCounter"));
		}
	}

	private boolean loadTimeTracking(Inspection insp, Document idoc) {
		// final boolean localDebug = false;
		// final String dbarLoc = "HS_BusinessObjects.loadTimeTracking";
		final String ttviewname = "TimeEmbedded";
		boolean result = false;

		View ttview = idoc.getParentDatabase().getView(ttviewname);
		if ( ttview == null ) return result;

		DocumentCollection ttcol = ttview.getAllDocumentsByKey(insp.getDocumentId());
		if ( ttcol.getCount() == 0 ) return result;

		for (Document ttdoc : ttcol) {
			TimeTracking ttr = new TimeTracking();
			ttr.setBMultiTime(HS_Util.loadValueString(ttdoc, "BMultiTime"));
			ttr.setDate(HS_Util.loadValueDate(ttdoc, "Date"));
			ttr.setDateCreated(HS_Util.loadValueDate(ttdoc, "DateCreated"));
			// ttr.setDateStr(HS_Util.formatDate(ttr.getDate(), "yyyy-MM-dd"));
			ttr.setDateStr(HS_Util.formatDate(ttr.getDate(), this.getStandardDateMask()));
			// ttr.setDateStr(HS_Util.formatDate(ttr.getDate(), "d-MMM-yyyy"));
			ttr.setDocumentId(HS_Util.loadValueString(ttdoc, "DocumentId"));
			ttr.setEditModule(HS_Util.loadValueString(ttdoc, "EditModule"));
			ttr.setEho(HS_Util.loadValueString(ttdoc, "EHO"));
			ttr.setFacilityName(HS_Util.loadValueString(ttdoc, "FacilityName"));
			ttr.setForm(HS_Util.loadValueString(ttdoc, "Form"));
			ttr.setHours(HS_Util.loadValueInt(ttdoc, "Hours"));
			ttr.setInspectionType(HS_Util.loadValueString(ttdoc, "InspectionType"));
			ttr.setMagDistrict(HS_Util.loadValueString(ttdoc, "MagDistrict"));
			ttr.setMileage(HS_Util.loadValueString(ttdoc, "Mileage"));
			ttr.setMin(HS_Util.loadValueInt(ttdoc, "Min"));
			ttr.setModule(HS_Util.loadValueString(ttdoc, "Module"));
			ttr.setParentDocumentId(HS_Util.loadValueString(ttdoc, "ParentDocumentId"));
			ttr.setParentForm(HS_Util.loadValueString(ttdoc, "ParentForm"));
			ttr.setSource(HS_Util.loadValueString(ttdoc, "Source"));
			ttr.setTimeIn(HS_Util.loadValueString(ttdoc, "TimeIn"));
			ttr.setTimeOut(HS_Util.loadValueString(ttdoc, "TimeOut"));
			ttr.setTimeSpent(HS_Util.loadValueDouble(ttdoc, "TimeSpent"));
			ttr.setTimeType(HS_Util.loadValueString(ttdoc, "TimeType"));
			ttr.setTrackingComments(HS_Util.loadValueString(ttdoc, "TrackingComments"));
			ttr.setUnid(ttdoc.getUniversalID());
			ttr.setViewDate(HS_Util.loadValueDate(ttdoc, "ViewDate"));
			ttr.setViewDescription(HS_Util.loadValueString(ttdoc, "ViewDescription"));
			ttr.setDeleteMeFlag(false);

			insp.getTimeTrackingRecords().add(ttr);
		}

		return result;
	}

	private void saveTimeTrackingDocs(Inspection insp, Document idoc) {
		final boolean localDebug = false;
		final String dbarLoc = "HS_BusinessObjects.saveTimeTrackingDocs";

		try {
			Database eidb = idoc.getParentDatabase();
			Document ttdoc = null;
			if ( localDebug ) HS_Util.debug("saving [" + insp.getTimeTrackingRecords().size() + "] time tracking docs.", "debug", dbarLoc);
			for (TimeTracking ttr : insp.getTimeTrackingRecords()) {
				if ( "".equals(ttr.getUnid()) ) {
					if ( !ttr.isDeleteMeFlag() ) {
						ttdoc = eidb.createDocument();
						ttdoc.replaceItemValue("Form", ttr.getForm());
					} else {
						ttdoc = null;
					}
				} else {
					if ( ttr.isDeleteMeFlag() ) {
						ttdoc = eidb.getDocumentByUNID(ttr.getUnid());
						if ( ttdoc != null ) {
							ttdoc.replaceItemValue("Deleted", new Date());
							ttdoc.replaceItemValue("DeletedBy", ExtLibUtil.getXspContext().getUser().getCommonName());
							ttdoc.save();
							new HS_auditForm().writeAuditEvent(ttdoc, "Document Deleted - Mobile");

							ttdoc = null;
							if ( localDebug ) HS_Util.debug("saved deleted time tracking doc.", "debug", dbarLoc);
						}
					} else {
						ttdoc = null;
					}
				}
				if ( ttdoc != null ) {
					Date wdt = null;
					DateTime wdt1 = null;
					String dtstr = "";
					ttdoc.replaceItemValue("BMultiTime", ttr.getBMultiTime());

					dtstr = ttr.getDateStr();
					if ( localDebug ) HS_Util.debug("dateStr:[" + dtstr + "]", "debug", dbarLoc);
					wdt = HS_Util.cvrtStringToDate(dtstr);
					if ( localDebug ) HS_Util.debug("wdt:[" + wdt + "]", "debug", dbarLoc);
					if ( wdt == null ) {
						wdt = ttr.getDate();
						if ( wdt == null ) {
							wdt = new Date();
						}
					}
					wdt1 = ttr.getSession().createDateTime(wdt);
					wdt1.setAnyTime();
					ttdoc.replaceItemValue("Date", wdt1);

					ttdoc.replaceItemValue("DateCreated", ttr.getDateCreated());
					ttdoc.replaceItemValue("DocumentId", ttr.getDocumentId());
					ttdoc.replaceItemValue("EditModule", ttr.getEditModule());
					ttdoc.replaceItemValue("EHO", ttr.getEho());
					ttdoc.replaceItemValue("FacilityName", ttr.getFacilityName());
					ttdoc.replaceItemValue("InspectionType", ttr.getInspectionType());
					ttdoc.replaceItemValue("MagDistrict", ttr.getMagDistrict());
					ttdoc.replaceItemValue("Mileage", ttr.getMileage());
					ttdoc.replaceItemValue("Module", ttr.getModule());
					ttdoc.replaceItemValue("ParentDocumentId", insp.getDocumentId());
					ttdoc.replaceItemValue("ParentForm", insp.getForm());
					ttdoc.replaceItemValue("RiskRating", ttr.getRiskRating());
					ttdoc.replaceItemValue("Source", ttr.getSource());
					ttdoc.replaceItemValue("TrackingComments", ttr.getTrackingComments());
					// ttdoc.replaceItemValue("TimeIn", ttr.getTimeIn());
					ttdoc.replaceItemValue("TimeIn", HS_Util.cvrtStringToTime(ttr.getTimeIn()));
					// ttdoc.replaceItemValue("TimeOut", ttr.getTimeOut());
					ttdoc.replaceItemValue("TimeOut", HS_Util.cvrtStringToTime(ttr.getTimeOut()));
					ttdoc.replaceItemValue("TimeType", ttr.getTimeType());
					ttdoc.replaceItemValue("TimeSpent", ttr.getTimeSpent());

					wdt = ttr.getViewDate();
					if ( wdt == null ) wdt = ttr.getDate();
					if ( wdt == null ) wdt = new Date();
					wdt1 = ttr.getSession().createDateTime(wdt);
					wdt1.setAnyTime();
					ttdoc.replaceItemValue("ViewDate", wdt1);

					ttdoc.replaceItemValue("ViewDescription", ttr.getViewDescription());
					ttdoc.replaceItemValue("Hours", ttr.getHours());
					ttdoc.replaceItemValue("Min", ttr.getMin());
					// ttdoc.replaceItemValue("xx", ttr.get);

					if ( ttdoc.save() ) {
						new HS_auditForm().writeAuditEvent(ttdoc, "Document Created - Mobile");

						ttr.setUnid(ttdoc.getUniversalID());
						if ( localDebug ) HS_Util.debug("saved time tracking doc with form:[" + ttr.getForm() + "] unid:[" + ttr.getUnid() + "] magDistrict:[" + ttr.getMagDistrict() + "]", "debug", dbarLoc);
					}
				}
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			if ( this.isDebugTablet() ) e.printStackTrace();
		}
	}

	private boolean isTimeReportsChanged(Inspection insp) {
		boolean result = false;

		for (TimeTracking ttr : insp.getTimeTrackingRecords()) {
			if ( "".equals(ttr.getUnid()) ) {
				if ( !ttr.isDeleteMeFlag() ) {
					result = true;
				}
			} else if ( ttr.isDeleteMeFlag() ) {
				result = true;
			}
		}
		return result;
	}

	public String getStandardDateMask() {
		if ( "".equals(standardDateMask) ) {
			Vector<String> idf = HS_Util.getAppSettingsValues("inspDateFormat");
			standardDateMask = (idf == null) ? "d-MMM-yyyy" : (idf.size() == 0) ? "d-MMM-yyyy" : idf.get(0);
		}
		return standardDateMask;
	}

	public void setStandardDateMask(String standardDateMask) {
		this.standardDateMask = standardDateMask;
	}

	private boolean	debugTablet	= false;

	public boolean isDebugTablet() {
		return debugTablet;
	}

	public void setDebugTablet(boolean debugTablet) {
		this.debugTablet = debugTablet;
	}

}
