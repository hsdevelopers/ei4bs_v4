package com.healthspace.general;

import java.util.Vector;

public class ViolationModule {

	public static Vector<String> getViolationModule(String module) {
		return getViolationModule(module, false);
	}

	public static Vector<String> getViolationModule(String module, boolean localDebug) {
		String msgContext = "ViolationModule.getViolationModule.1";
		Vector<String> results = new Vector<String>();

		if ( "Food".equals(module) ) {
			results = getViolationModule("FoodFacility", "FoodReport", "", "Food Facilities", "Restaurant", "Operator", localDebug);
		} else if ( "Recreational Water".equals(module) ) {
			results = getViolationModule("PoolFacility", "PoolReport", "", "Pool Facilities", "Public Pool (RPP)", "Public Pool (RPP)", localDebug);
		} else if ( "Pool".equals(module) ) {
			results = getViolationModule("PoolFacility", "PoolReport", "", "Pool Facilities", "Public Pool (RPP)", "Public Pool (RPP)", localDebug);
		} else if ( "Body Art".equals(module) ) {
			results = getViolationModule("BodyartFacility", "BodyartReport", "", "Bodyart Facilities", "Tattoo Establishment (BTP)", "Tattoo Establishment (BTP)", localDebug);
		} else {
			results.add("UNKNOWN!");
		}
		if ( localDebug ) {
			HS_Util.debug("for module [" + module + "] violationModule is:[" + results.toString() + "]", "debug", msgContext);
			System.out.print(msgContext + " - for module [" + module + "] violationModule is:[" + results.toString() + "]");
		}
		return results;
	}

	public static Vector<String> getViolationModule(String strPForm, String strForm, String strDbDesign, String strFacilityType, String strSubType, String strBillingType) {
		return getViolationModule(strPForm, strForm, strDbDesign, strFacilityType, strSubType, strBillingType, false);
	}

	@SuppressWarnings("unchecked")
	/*
	 * Attempted to duplicate functionality from the LotusScript code in the library Inspections method setViolationModule
	 */
	public static Vector<String> getViolationModule(String strPForm, String strForm, String strDbDesign, String strFacilityType, String strSubType, String strBillingType, boolean localDebug) {
		final String msgContext = "ViolationModule.getViolationModule.2";
		Vector<String> result = new Vector<String>();
		String strMSModule = "";
		Vector<String> mastSettingsModule = null;
		if ( "".equals(strDbDesign) ) strDbDesign = HS_Util.getMasterSettingsString("DBDesign");

		if ( localDebug ) HS_Util.debug("strForm:[" + strForm + "] strPForm:[" + strPForm + "] strDbDesign:[" + strDbDesign + "]", "debug", msgContext);
		if ( localDebug ) System.out.print(msgContext + " - strForm:[" + strForm + "] strPForm:[" + strPForm + "] strDbDesign:[" + strDbDesign + "]");

		if ( "AgricultureReport".equals(strForm) ) {
			result.add("FoodReport");
			return result;
		}
		if ( "AviaryReport".equals(strForm) ) {
			result.add("Aviary");
			return result;
		}
		if ( "BareReport".equals(strForm) ) {
			mastSettingsModule = HS_Util.getMasterSettingsVector("BareModule");
			if ( !"".equals(mastSettingsModule.get(0)) ) {
				result = returnViolationModule(mastSettingsModule, strSubType, true);
			} else {
				result.add("Bare");
			}
			return result;
		}
		if ( "BodyartReport".equals(strForm) ) {
			mastSettingsModule = HS_Util.getMasterSettingsVector("BodyartModule");
			if ( mastSettingsModule == null ) {
				mastSettingsModule = new Vector<String>();
				mastSettingsModule.add("");
			}
			if ( mastSettingsModule.size() < 1 ) mastSettingsModule.add("");
			if ( !"".equals(mastSettingsModule.get(0)) ) {
				result = returnViolationModule(mastSettingsModule, strSubType, true);
				return result;
			} else {
				if ( "WestVirginia".equalsIgnoreCase(strDbDesign) ) {
					if ( strSubType.contains("Piercing") ) {
						result.add("Body Piercing Studio");
						return result;
					} else if ( strSubType.contains("Tattoo") ) {
						result.add("Tattoo Studio");
						return result;
					}
				}
			}
		}
		if ( "BeachReport".equals(strForm) ) {
			if ( "Ohio".equals(strDbDesign) ) {
				result.add("Bathing Beach");
				return result;
			}
			if ( "Missouri".equals(strDbDesign) ) {
				result.add("Beach");
				return result;
			}
			// TODO Is there something else as a default?
		}
		if ( "CampReport".equals(strForm) ) {
			if ( strDbDesign == "Ohio" ) {
				result.add("RV Park/Campground");
				return result;
			}
			// TODO Is there something else as a default?
		}
		if ( "FairgroundReport".equals(strForm) ) {
			result.add("Fairground");
			return result;
		}
		if ( "CampReport".equals(strForm) ) {
			if ( strDbDesign == "Ohio" ) {
				result.add("RV Park/Campground");
				return result;
			}
		}
		if ( "FoodReport".equals(strForm) ) {
			if ( "TemporaryVendorFacility".equals(strPForm) ) {
				Vector<String> tvstr = (Vector<String>) HS_profiles.get("eiRoot", "MasterSettings", "TempVendModule");
				String tvm = (tvstr == null) ? "" : (tvstr.size() == 0) ? "" : tvstr.get(0);
				if ( "".equals(tvm) ) {
					result.add("Temporary Food");
				} else {
					result = returnViolationModule(tvstr, strSubType, true);
				}
				return result;
			}

			Vector<String> foodModule = (Vector<String>) HS_profiles.get("eiRoot", "MasterSettings", "FoodModule");
			strMSModule = (foodModule == null) ? strForm : (foodModule.size() == 0) ? strForm : foodModule.get(0);
			if ( localDebug ) HS_Util.debug("for [" + strForm + "][" + strPForm + "] MSModule:[" + strMSModule + "]  foodModule has " + foodModule.size() + " values.", "debug", msgContext);

			String pType = strSubType;
			String bType = strBillingType;

			if ( "Wisconsin".equals(strDbDesign) && "Vending".equals(pType) ) {
				if ( bType.contains("Commissary") || bType.contains("Storage") ) {
					result = returnViolationModule(foodModule, strSubType, true);
					if ( "".equals(result.toString()) ) {
						result.add(strForm);
					}
					return result;
				}
				result.add("Vending");
				return result;
			}
			if ( !("".equals(strMSModule)) ) {
				if ( localDebug ) HS_Util.debug("calling returnViolationModule", "debug", msgContext);
				result = returnViolationModule(foodModule, strSubType, true);
				if ( result.size() == 0 ) result.add(strForm);
				if ( localDebug ) HS_Util.debug("Returning: " + result.toString() + " [" + result.size() + "]", "debug", msgContext);
				return result;
			}
			if ( pType.contains("Mobile") ) {
				if ( "Michigan".equals(strDbDesign) ) {
					result.add("Food");
					return result;
				} else if ( strDbDesign != "Arizona" && strDbDesign != "Wisconsin" && strDbDesign != "Ohio" && strDbDesign != "WA" && strDbDesign != "Colorado" ) {
					result.add("Mobile Food Service & Pushcarts");
					return result;
				}
			} else {
				if ( pType.contains("Vending") ) {
					result.add("Vending");
					return result;
				}
			}

			// if (strDbDesign == "Wisconsin" && (parentFacilityDoc.getItemValueInteger("BillingType") > 0 ||
			// parentFacilityDoc.getItemValueInteger("Commissary") > 0)) {
			// result.add("FoodReport");
			// return result;
			// }
			// IS THIS THE DEFAULT??
			if ( "".equals(result.toString()) ) {
				result.add(strForm);
			}
			return result;
		}
		if ( "GarbageReport".equals(strForm) ) {
			result.add("Garbage Hauler");
			return result;
		}
		if ( "HousingReport".equals(strForm) ) {
			result.add("Housing");
			return result;
		}
		if ( "JailReport".equals(strForm) ) {
			result.add("Jail");
			return result;
		}
		if ( "MobileHomeReport".equals(strForm) ) {
			result.add("Mobile Home Park");
			return result;
		}
		if ( "LaborCampReport".equals(strForm) ) {
			// result.add(strForm + parentFacilityDoc.getItemValueString("Regulations"));
			result.add(strForm + "DO NOT HAVE THE DATA FOR THIS");
			return result;
		}
		if ( "PoolReport".equals(strForm) ) {
			result.add("Pool");
			return result;
		}
		if ( "SchoolReport".equals(strForm) ) {
			result.add("School");
			return result;
		}
		if ( "SepticRemovalReport".equals(strForm) ) {
			if ( strDbDesign == "WA" ) {
				result.add("Septic Removal");
				return result;
			}
		}
		if ( "SummerCampReport".equals(strForm) ) {
			if ( strDbDesign == "Arizona" || strDbDesign == "Wisconsin" ) {
				result.add("Recreational Educational Camp");
				return result;
			} else if ( strDbDesign == "Ohio" ) {
				result.add("Resident Camp");
				return result;
			}
		}
		result.add(strForm);
		return result;
	}

	private static Vector<String> returnViolationModule(Vector<String> module, String pType, Boolean useType) {
		final String dbarLoc = "InspectionBean.returnViolationModule";
		final boolean localDebug = false;
		// final boolean localDebug = HS_Util.isDebugger() &&
		// HS_Util.isDebugServer();

		Vector<String> tempModule = new Vector<String>();
		// String pType = pFacDoc.getItemValueString("Type");
		if ( localDebug ) HS_Util.debug("type: [" + pType + "] useType: [" + useType + "]", "debug", dbarLoc);

		for (String mstr : module) {
			if ( useType ) {
				if ( mstr.contains(pType) ) {
					tempModule.add(mstr.substring(0, mstr.indexOf(" - ")));
				} else {
					if ( localDebug ) HS_Util.debug("[" + mstr + "] does not contain:[" + pType + "]", "debug", dbarLoc);
				}
			} else {
				tempModule.add(mstr.substring(0, mstr.indexOf(" - ")));
			}
		}
		if ( localDebug ) {
			// HS_Util.debug("module: " + module, "debug", dbarLoc);
			HS_Util.debug("returning: " + tempModule, "debug", dbarLoc);
		}
		return tempModule;
	}
}
