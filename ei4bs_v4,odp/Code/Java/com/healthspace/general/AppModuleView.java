package com.healthspace.general;

import java.io.Serializable;

import org.openntf.domino.Document;

public class AppModuleView implements Serializable {

	private static final long serialVersionUID = 1L;

	private String dbkey;
	private String label;
	private String v4rname;
	private String vname;
	private Double position;
	private String hwformula;
	private boolean includePager;

	public AppModuleView() {
	}

	public String getDbkey() {
		return dbkey;
	}

	public String getLabel() {
		return label;
	}

	public String getV4rname() {
		return v4rname;
	}

	public String getVname() {
		return vname;
	}

	public Double getPosition() {
		return position;
	}

	public String getHwformula() {
		return hwformula;
	}

	public boolean isIncludePager() {
		return includePager;
	}

	public void setIncludeLetterPager(boolean includePager) {
		this.includePager = includePager;
	}

	public void setDbkey(String dbkey) {
		this.dbkey = dbkey;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setV4rname(String v4rname) {
		this.v4rname = v4rname;
	}

	public void setVname(String vname) {
		this.vname = vname;
	}

	public void setPosition(Double position) {
		this.position = position;
	}

	public void setHwformula(String hwformula) {
		this.hwformula = hwformula;
	}

	public void init(Document menuDoc) {
		this.dbkey = menuDoc.getItemValueString("dbkey");
		this.label = menuDoc.getItemValueString("menuLabel");
		this.vname = menuDoc.getItemValueString("menuView");
		this.v4rname = menuDoc.getItemValueString("menuViewReader");
		this.position = (Double) (menuDoc.getItemValue("menuPos").get(0));
		this.hwformula = menuDoc.getItemValueString("menuHW");
		this.includePager = "Yes".equalsIgnoreCase(menuDoc.getItemValueString("menuIncludePager"));
	}
}
