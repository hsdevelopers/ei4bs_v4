package com.healthspace.general;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Vector;

import org.openntf.domino.Database;
import org.openntf.domino.DateTime;
import org.openntf.domino.Document;
import org.openntf.domino.DocumentCollection;
import org.openntf.domino.Item;

public class HS_GetMostRecent {

	public HS_GetMostRecent() {

	}

	/*
	 * inspectionByFormUNID(db As NotesDatabase, facility As NotesDocument, InspForm As String )
	 */
	public String inspectionByFormUNID(Document facility, String inspForm) {
		if (facility == null) return "";
		return inspectionByFormUNID(facility.getParentDatabase(), facility, inspForm);
	}

	public String inspectionByFormUNID(Database db, Document facility, String inspForm) {
		return typeInspectionUNID(db, facility, "ANYTYPE", "Form", inspForm);
	}

	public static String inspectionUNID(Document facility) {
		if (facility == null) return "";
		return inspectionUNID(facility.getParentDatabase(), facility);
	}

	public static String inspectionUNID(Database unused, Document facility) {
		// final boolean localDebug = HS_Util.isDebugServer();
		final boolean localDebug = false;
		final String dbarLoc = "HS_GetMostRecent.inspectionUNID";

		String result = "";
		// ' Abort if no facility document
		if (facility == null) return result;

		String dbDesign = facility.getItemValueString("DBDesign");

		if ("VDH".equals(dbDesign)) {
			result = typeInspectionUNID(facility, "ANYTYPE", "", "");
			return result;
		}

		// ' Check the facility document to ensure that it actually is a facility document
		// ' note this is inaccurate, it will consider WaterPrintPermit a facility, but it
		// ' does help most of the time.
		if ((facility.getFormName().indexOf("Facility") < 0) && (facility.getFormName().indexOf("Permit") < 0)) {
			// 'is there some meaningful error message we can display?
			HS_Util.debug("Aborting because form name is " + facility.getFormName(), "error", dbarLoc);
			return result;
		}
		long stime = new Date().getTime();
		String deleted = "";
		DateTime newestdate = HS_Util.getSession().createDateTime("01/01/1980");
		DateTime testDate = null;

		DocumentCollection allInspections = facility.getResponses();
		if (allInspections != null) {
			if (allInspections.getCount() > 0) {
				for (Document wrkInspection : allInspections) {
					deleted = (!(wrkInspection.hasItem("deleted"))) ? "" : wrkInspection.getFirstItem("deleted").getText();
					if ("".equals(deleted)) {
						// if ("".equals(wrkInspection.getItemValueString("deleted"))) {
						if (wrkInspection.getFormName().indexOf("Report") > -1) {
							if (wrkInspection.hasItem("InspectionDate")) {
								// testDate =
								// HS_Util.getSession().createDateTime(wrkInspection.getItemValueDateTimeArray("Inspectiondate").get(0));
								testDate = (DateTime) wrkInspection.getItemValue("Inspectiondate").get(0);
								if (newestdate.compareTo(testDate) < 0) {
									newestdate = testDate;
									result = wrkInspection.getUniversalID();
									// if (localDebug) HS_Util.debug("using inspection for date; " + newestdate.toString(), "debug",
									// dbarLoc);
								}
							}
						}
					}
				}
			}
		}
		long etime = new Date().getTime();
		if (localDebug) HS_Util.debug("returning; [" + result + "] [" + (etime - stime) + "]", "debug", dbarLoc);

		return result;
	}

	public static String routineInspectionUNID(Database unused, Document facility) {
		return routineInspectionUNID(facility);
	}

	public static String routineInspectionUNID(Document facility) {
		// final boolean localDebug = HS_Util.isDebugServer();
		final boolean localDebug = false;
		final String dbarLoc = "HS_GetMostRecent.routineInspectionUNID";
		//
		String result = "";
		// String retform = "";
		// String dbDesign = facility.getItemValueString("DBDesign");

		ArrayList<String> iunids = routineInspectionsUNID(facility, 1);
		if (iunids.size() == 0) {
			result = "";
		} else {
			result = iunids.get(0);
		}
		if (localDebug) HS_Util.debug("returning:[" + result + "]", "debug", dbarLoc);
		return result;
	}

	public static ArrayList<String> routineInspectionsUNID(Document facility, int noPreviousInsp) {
		// final boolean localDebug = HS_Util.isDebugServer();
		final boolean localDebug = false;
		final String dbarLoc = "HS_GetMostRecent.routineInspectionsUNID";

		ArrayList<String> result = new ArrayList<String>();
		// ' Abort if no facility document
		if (facility == null) return result;

		String dbDesign = facility.getItemValueString("DBDesign");

		if ("VDH".equals(dbDesign)) {
			result.add(typeInspectionUNID(facility, "Routine Critical Procedures Training Risk Factor Assessment", "", ""));
			return result;
		}

		// ' Check the facility document to ensure that it actually is a facility document
		// ' note this is inaccurate, it will consider WaterPrintPermit a facility, but it
		// ' does help most of the time.
		if ((facility.getFormName().indexOf("Facility") < 0) && (facility.getFormName().indexOf("Permit") < 0)) {
			// 'is there some meaningful error message we can display?
			HS_Util.debug("Aborting because form name is " + facility.getFormName(), "error", dbarLoc);
			return result;
		}
		long stime = new Date().getTime();

		// DateTime newestdate = HS_Util.getSession().createDateTime("01/01/1980");
		DateTime testDate = null;

		// Set InspMasterSettings = HSGetProfileDocument(facility.Parentdatabase, "MasterSettings" )
		// vInspSchedTypes = InspMasterSettings.InspectionScheduledTypes
		Vector<String> vInspSchedTypes = HS_Util.getMasterSettingsVector("InspectionScheduledTypes");

		// if(!vInspSchedTypes.contains("Routine")) vInspSchedTypes.add("Routine");
		// if(!vInspSchedTypes.contains("Initial")) vInspSchedTypes.add("Initial");
		// if("Ohio".equals(dbDesign)) {
		// //if("Routine".equals(itype) || "Initial".equals(itype)) bsched = true;
		// } else {
		// if(!vInspSchedTypes.contains("Pre-inspection")) vInspSchedTypes.add("Pre-inspection");
		// if(!vInspSchedTypes.contains("Second Inspection")) vInspSchedTypes.add("Second Inspection");
		// }

		// boolean bSched = false;
		String itype = "";
		String deleted = "";
		DocumentCollection allInspections = facility.getResponses();
		ArrayList<String> inspectionList = new ArrayList<String>();
		// if (localDebug) HS_Util.debug("There are [" + allInspections.getCount() + "] inspections.", "debug", dbarLoc);
		if (allInspections != null) {
			if (allInspections.getCount() > 0) {
				for (Document wrkInsp : allInspections) {
					// if ("".equals(wrkInsp.getItemValueString("deleted"))) {
					deleted = (!(wrkInsp.hasItem("deleted"))) ? "" : wrkInsp.getFirstItem("deleted").getText();
					if ("".equals(deleted)) {
						if (wrkInsp.getFormName().indexOf("Report") > -1) {
							itype = wrkInsp.getItemValueString("Type");
							if (vInspSchedTypes.contains(itype)) {
								if (wrkInsp.hasItem("InspectionDate")) {
									// inspectionList.add(wrkInsp);
									testDate = (DateTime) wrkInsp.getItemValue("Inspectiondate").get(0);
									inspectionList.add(HS_Util.formatDate(testDate.toJavaDate(), "yyyy-MM-dd") + ":" + wrkInsp.getUniversalID() + ":"
											+ wrkInsp.getFormName());
									// if (newestdate.compareTo(testDate) < 0) {
									// newestdate = testDate;
									// result.add(wrkInsp.getUniversalID());
									// retform = wrkInsp.getFormName();
									// // if (localDebug) HS_Util.debug("using inspection for date; " + newestdate.toString(), "debug",
									// // dbarLoc);
									// }
								}
							}
						}
					}
				}
			}
		}
		if (inspectionList.size() > noPreviousInsp) {
			Collections.sort(inspectionList, Sort_Inspection_Order);
			ArrayList<String> nlist = new ArrayList<String>();
			for (int i = 0; i < noPreviousInsp; i++) {
				nlist.add(inspectionList.get(i));
			}
		}
		for (String winsp : inspectionList) {
			// if (localDebug) HS_Util.debug(winsp, "debug", dbarLoc);
			result.add(winsp.split(":")[1]);
		}
		long etime = new Date().getTime() - stime;
		if (localDebug) HS_Util.debug("returning [" + result.size() + "] in [" + etime + "] msecs", "debug", dbarLoc);
		return result;
	}

	static final Comparator<String> Sort_Inspection_Order = new Comparator<String>() {
		public int compare(String insp0, String insp1) {
			try {
				String dt0 = insp0.substring(0, 10);
				String dt1 = insp1.substring(0, 10);
				if (dt0.equals(dt1)) return 0;
				return dt0.compareTo(dt1) * -1; // Get descending order (newest first??)
			} catch (Exception e) {
				return 0;
			}
		}
	};

	/*
	 * Private Function GetMostRecentNeedsFollowupUNID(db As NotesDatabase, facility As NotesDocument) As String 'not used anywhere
	 * 
	 * GetMostRecentNeedsFollowupUNID = GetMostRecentTypeInspectionUNID( _ db, facility,
	 * "Routine Critical Procedures Training Risk Factor Assessment", _ "FollowUpInspectionRequired", "Yes" )
	 * 
	 * End Function
	 */
	public String needsFollowUpUNID(Database db, Document facility) {
		return typeInspectionUNID(db, facility, "Routine Critical Procedures Training Risk Factor Assessment", "FollowUpInspectionRequired", "Yes");
	}

	/*
	 * Private Function GetMostRecentTypeInspectionUNID(db As NotesDatabase, facility As NotesDocument, _ InspType As String, SpecialField
	 * As String, SpecialValue As String ) As String
	 */
	private static String typeInspectionUNID(Document facility, String inspType, String specialField, String specialValue) {
		if (facility == null) return "";
		return typeInspectionUNID(facility.getParentDatabase(), facility, inspType, specialField, specialValue);
	}

	private static String typeInspectionUNID(Database unused, Document facility, String inspType, String specialField, String specialValue) {
		// final boolean localDebug = HS_Util.isDebugServer();
		final boolean localDebug = false;
		final String dbarLoc = "HS_GetMostRecent.typeInspectionUNID";

		String result = "";
		// TODO Auto-generated method stub

		// ' Abort if no facility document
		if (facility == null) return result;

		// ' Check the facility document to ensure that it actually is a facility document
		// ' note this is inaccurate, it will consider WaterPrintPermit a facility, but it
		// ' does help most of the time
		if (!"PersonalService".equals(facility.getFormName()) && !facility.getFormName().contains("Facility") && !facility.getFormName().contains("Permit")) {
			// 'is there some meaningful error message we can display?
			// 'Msgbox "Aborting typeInspectionUNID() because form name is " & facility.Form(0)
			HS_Util.debug("Aborting because form name is " + facility.getFormName(), "warn", dbarLoc);
			return result;
		}
		long stime = new Date().getTime();

		DateTime newestdate = HS_Util.getSession().createDateTime("01/01/1980");
		DateTime testDate = null;
		boolean continueit;
		Item item = null;
		DocumentCollection allInspections = facility.getResponses();
		if (allInspections != null) {
			if (allInspections.getCount() > 0) {
				for (Document wrkInspection : allInspections) {
					if ("".equals(wrkInspection.getItemValueString("deleted"))) {
						if (wrkInspection.getFormName().contains("Report") || wrkInspection.getFormName().contains("Inspection")) {
							if ("ANYTYPE".equals(inspType) || inspType.contains(wrkInspection.getItemValueString("Type"))) {
								continueit = true;
								if (!"".equals(specialField)) {
									item = wrkInspection.getFirstItem(specialField);
									if (item == null) {
										continueit = false;
									} else {
										continueit = item.getText().contains(specialValue);
									}
								}
								if (continueit && wrkInspection.hasItem("InspectionDate")) {
									// testDate =
									// HS_Util.getSession().createDateTime(wrkInspection.getItemValueDateTimeArray("Inspectiondate").get(0));
									testDate = (DateTime) wrkInspection.getItemValue("Inspectiondate").get(0);
									if (newestdate.compareTo(testDate) < 0) {
										newestdate = testDate;
										result = wrkInspection.getUniversalID();
										// if (localDebug) HS_Util.debug("using inspection for date; " + newestdate.toString(), "debug",
										// dbarLoc);
									}
								}
							}
						}
					}
				}
			}
		}
		long etime = new Date().getTime();
		if (localDebug) HS_Util.debug("returning; [" + result + "] [" + (etime - stime) + "]", "debug", dbarLoc);

		return result;
	}
}
