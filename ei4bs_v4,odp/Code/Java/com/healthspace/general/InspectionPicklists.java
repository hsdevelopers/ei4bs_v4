package com.healthspace.general;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Vector;

import org.openntf.domino.Database;
import org.openntf.domino.View;
import org.openntf.domino.ViewEntry;
import org.openntf.domino.ViewEntryCollection;

import com.ibm.jscript.InterpretException;
import com.ibm.jscript.std.ObjectObject;
import com.ibm.jscript.types.FBSUtility;

public class InspectionPicklists extends HS_base {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public InspectionPicklists() {

	}

	private static final String dbarHead = "InspectionPicklists.";

	private String lastSectVModule;
	private String lastViolVModule;
	private String lastObserveVModule;

	private Vector<String> pl_inspectionTypes;
	private Vector<String> plGroups;
	private Vector<String> plCategories;
	private Vector<String> plSections;

	private Vector<ObjectObject> dataObservation;
	private Vector<ObjectObject> dataSections;
	private Vector<ObjectObject> dataViolations;

	public Vector<ObjectObject> loadDataObservations(String vModule) {
		return loadDataObservations(vModule, false);
	}

	public Vector<ObjectObject> loadDataObservationsAsSigner(String vModule) {
		return loadDataObservations(vModule, true);
	}

	public Vector<ObjectObject> loadDataObservations(String vModule, boolean useSigner) {
		final String dbarLoc = this.getDbarHead() + "loadDataObservations";
		final boolean localDebug = false;
		final String viewName = "XPagesObservationsByModule";
		int errctr = 0;
		Date sdt = new Date();

		if ( "".equals(vModule) ) {
			dataObservation = null;
			HS_Util.debug("ViolationModule is blank!: [" + vModule + "]", "error", dbarLoc, sdt);
			return dataObservation;
		}
		if ( dataObservation != null ) {
			if ( vModule.equals(getLastObserveVModule()) ) {
				if ( localDebug ) HS_Util.debug("ViolationModule is unchanged: [" + vModule + "]", "debug", dbarLoc, sdt);
				return dataObservation;
			}
			if ( localDebug ) HS_Util.debug("ViolationModule changed: from:[" + getLastObserveVModule() + "] to:[" + vModule + "]", "debug", dbarLoc, sdt);
			dataObservation = null;
		}

		if ( dataObservation == null ) {
			setLastObserveVModule(vModule);

			final String[] observeParser = new String[] { "module~Module", "code~Code", "observation~Observation", "correction~Correction",
					"legalText~LegalObservationTextComputed" };
			dataObservation = new Vector<ObjectObject>();
			try {
				HS_database hsdb = new HS_database();
				Database eidb = (useSigner) ? hsdb.getdbAsSigner("eiRoot") : hsdb.getdb("eiRoot");
				if ( eidb == null ) {
					HS_Util.debug("unable to access eiRoot database:", "error", dbarLoc, sdt);
					return dataObservation;
				}
				View observeView = eidb.getView(viewName);
				if ( observeView == null ) {
					HS_Util.debug("unable to access view " + viewName + " in eiRoot database:", "error", dbarLoc, sdt);
					return dataObservation;
				}
				observeView.setAutoUpdate(false);
				// ViewNavigator onav =
				// observeView.createViewNavFromCategory(vModule);
				// if (localDebug)
				// HS_Util.debug("found [" + onav.getCount() +
				// "] observation entries for [" + vModule + "] in:[" +
				// eidb.getServer() + "!!"
				// + eidb.getFileName() + "]", "debug", dbarLoc, sdt);
				// for (ViewEntry ve : onav) {

				String cval = "";
				// String newLine = (String)
				// this.getSession().evaluate("@NewLine").get(0);
				ViewEntryCollection vec = observeView.getAllEntriesByKey(vModule, true);
				if ( localDebug )
					HS_Util.debug("found [" + vec.getCount() + "] observation entries for [" + vModule + "] in:[" + eidb.getServer() + "!!"
							+ eidb.getFileName() + "]", "debug", dbarLoc, sdt);
				for (ViewEntry ve : vec) {
					ObjectObject nobj = new ObjectObject();

					for (String oParser : observeParser) {
						String[] op = oParser.split("~");
						try {
							cval = ve.getColumnValue(op[1]).toString();
							if ( "module".equalsIgnoreCase(op[0]) ) {
								cval = vModule;
							}
							nobj.put(op[0], FBSUtility.wrap(cval).toFBSString());
						} catch (InterpretException e) {
							HS_Util.debug("ON " + oParser + "InterpretException: " + e.toString(), "error", dbarLoc, sdt);
						} catch (Exception esp) {
							if ( errctr++ < 10 ) HS_Util.debug("ON " + oParser + ": Error: " + esp.toString(), "error", dbarLoc, sdt);
						}
					}
					try {
						nobj.put("unid", FBSUtility.wrap(ve.getUniversalID()).toFBSString());
						nobj.put("used", FBSUtility.wrap("no").toFBSString());
					} catch (InterpretException e) {
						HS_Util.debug(e.toString(), "error", dbarLoc, sdt);
					}
					dataObservation.add(nobj);
				}
				if ( localDebug ) HS_Util.debug("added [" + dataObservation.size() + "]", "debug", dbarLoc, sdt);
				observeView.setAutoUpdate(true);
			} catch (Exception e) {
				HS_Util.debug(e.toString(), "error", dbarLoc, sdt);
				if ( isDebugServer() ) e.printStackTrace();
			}
		}
		return dataObservation;
	}

	public Vector<ObjectObject> loadDataSections(String vModule) {
		return loadDataSections(vModule, false, false);
	}

	public Vector<ObjectObject> loadDataSectionsAsSigner(String vModule) {
		return loadDataSections(vModule, false, true);
	}

	public Vector<ObjectObject> loadDataSections(String vModule, boolean localDebug, boolean useSigner) {
		final String dbarLoc = this.getDbarHead() + "loadDataSections";
		final String viewName = "XPagesSectionLookup";

		Date sdt = new Date();

		if ( "".equals(vModule) ) {
			dataSections = null;
			HS_Util.debug("ViolationModule is blank!: [" + vModule + "]", "error", dbarLoc, sdt);
			System.out.print("ViolationModule is blank!: [" + vModule + "]");
			return dataSections;
		}
		if ( dataSections != null ) {
			if ( vModule.equals(getLastSectVModule()) ) {
				HS_Util.debug("ViolationModule is the same: [" + vModule + "]", "debug", dbarLoc, sdt);
				return dataSections;
			}
			if ( localDebug ) HS_Util.debug("ViolationModule changed: from:[" + getLastSectVModule() + "] to:[" + vModule + "]", "debug", dbarLoc, sdt);
			dataSections = null;
		}
		if ( dataSections == null ) {
			final String[] sectionParser = new String[] { "module~Module", "groupNumber~SectionGroupNumber", "number~SectionNumber", "category~Category",
					"jurisdiction~Jurisdiction", "group~SectionGroup", "description~Description", "mandatory~Mandatory", "reuse~Reuse",
					"documentId~DocumentID", "hzrdRating~HazardRating", "buttons~Buttons" };
			dataSections = new Vector<ObjectObject>();
			setLastSectVModule("");
			try {
				HS_database hsdb = new HS_database();
				// Database eidb = hsdb.getdb("eiRoot");
				Database eidb = (useSigner) ? hsdb.getdbAsSigner("eiRoot") : hsdb.getdb("eiRoot");
				if ( eidb == null ) {
					HS_Util.debug("unable to access eiRoot database:", "error", dbarLoc, sdt);
					System.out.print("unable to access eiRoot database:");
					return dataSections;
				}
				if ( localDebug ) {
					HS_Util.debug("using DB:[" + eidb.getServer() + "!!" + eidb.getFilePath() + "]", "debug", dbarLoc, sdt);
					System.out.print("using DB:[" + eidb.getServer() + "!!" + eidb.getFilePath() + "]");
				}
				View sectionView = eidb.getView(viewName);
				if ( sectionView == null ) {
					HS_Util.debug("unable to access view " + viewName + " in eiRoot database:" + eidb.getServer() + "!!" + eidb.getFilePath(), "error",
							dbarLoc, sdt);
					System.out.print("unable to access view " + viewName + " in eiRoot database:" + eidb.getServer() + "!!" + eidb.getFilePath());
					return dataSections;
				}
				sectionView.setAutoUpdate(false);
				ViewEntryCollection vec = sectionView.getAllEntriesByKey(vModule, true);
				if ( localDebug ) {
					HS_Util.debug("found [" + vec.getCount() + "] entries for [" + vModule + "]", "debug", dbarLoc, sdt);
					System.out.print("found [" + vec.getCount() + "] entries for [" + vModule + "]");
				}
				for (ViewEntry ve : vec) {
					ObjectObject nobj = new ObjectObject();

					for (String sParser : sectionParser) {
						String[] sp = sParser.split("~");
						try {
							nobj.put(sp[0], FBSUtility.wrap(ve.getColumnValue(sp[1]).toString()).toFBSString());
						} catch (InterpretException e) {
							HS_Util.debug("ON " + sParser + "InterpretException: " + e.toString(), "error", dbarLoc, sdt);
						} catch (Exception esp) {
							HS_Util.debug("ON " + sParser + ": Error: " + esp.toString(), "error", dbarLoc, sdt);
						}
					}
					try {
						nobj.put("unid", FBSUtility.wrap(ve.getUniversalID()).toFBSString());
						nobj.put("used", FBSUtility.wrap("no").toFBSString());
					} catch (InterpretException e) {
						HS_Util.debug(e.toString(), "error", dbarLoc, sdt);
					}
					dataSections.add(nobj);
				}
				setLastSectVModule(vModule);
				if ( localDebug ) {
					HS_Util.debug("added [" + dataSections.size() + "]", "debug", dbarLoc, sdt);
					System.out.print("added [" + dataSections.size() + "]");
				}
				// Collections.sort(dataSections, Section_Order);
				sectionView.setAutoUpdate(true);
			} catch (Exception e) {
				HS_Util.debug(e.toString(), "error", dbarLoc, sdt);
				if ( isDebugServer() ) e.printStackTrace();
			}
		}
		return dataSections;
	}

	public Vector<ObjectObject> loadDataViolationsAsSigner(String vModule) {
		return loadDataViolations(vModule, true);
	}

	public Vector<ObjectObject> loadDataViolations(String vModule) {
		return loadDataViolations(vModule, false);
	}

	public Vector<ObjectObject> loadDataViolations(String vModule, boolean useSigner) {
		final String dbarLoc = this.getDbarHead() + "loadDataViolations";
		final boolean localDebug = false;
		final String viewName = "XPagesViolationsByModuleJurisdiction";

		Date sdt = new Date();

		if ( "".equals(vModule) ) {
			dataViolations = null;
			if ( localDebug ) HS_Util.debug("ViolationModule is blank!: [" + vModule + "]", "error", dbarLoc, sdt);
			return dataViolations;
		}
		if ( dataViolations != null ) {
			if ( vModule.equals(getLastViolVModule()) ) {
				if ( localDebug ) HS_Util.debug("ViolationModule and Section are the same: [" + vModule + "]", "debug", dbarLoc, sdt);
				return dataViolations;
			}
			if ( localDebug ) HS_Util.debug("ViolationModule changed: from:[" + getLastViolVModule() + "] to:[" + vModule + "]", "debug", dbarLoc, sdt);
			dataViolations = null;
		}

		if ( dataViolations == null ) {
			final String[] sectionParser = new String[] { "module~Module", "jurisdiction~Jurisdiction", "category~Category", "code~Code",
					"number~SectionNumber", "critical~Critical", "description~Description", "documentId~DocumentID", "legalText~LegalText",
					"publicType~PublicType", "hzrdRating~HazardRating" };
			dataViolations = new Vector<ObjectObject>();
			try {
				HS_database hsdb = new HS_database();
				Database eidb = (useSigner) ? hsdb.getdbAsSigner("eiRoot") : hsdb.getdb("eiRoot");
				if ( eidb == null ) {
					HS_Util.debug("unable to access eiRoot database:", "error", dbarLoc, sdt);
					return dataViolations;
				}
				View violView = eidb.getView(viewName);
				if ( violView == null ) {
					HS_Util.debug("unable to access view " + viewName + " in eiRoot database:", "error", dbarLoc, sdt);
					return dataViolations;
				}
				violView.setAutoUpdate(false);
				// String vKey = this.getViolationModule().get(0);
				ViewEntryCollection vec = violView.getAllEntriesByKey(vModule, true);
				if ( localDebug ) HS_Util.debug("found [" + vec.getCount() + "] entries for [" + vModule + "]", "debug", dbarLoc, sdt);
				long itctr = -1;
				for (ViewEntry ve : vec) {
					ObjectObject nobj = new ObjectObject();
					itctr++;

					for (String sParser : sectionParser) {
						String[] sp = sParser.split("~");
						try {
							if ( localDebug && "code".equals(sp[0]) && itctr == 0 ) {
								String cval = ve.getColumnValue(sp[1]).toString();
								debug("Found code for row zero:[" + cval + "]", "debug", dbarLoc);
							}
							nobj.put(sp[0], FBSUtility.wrap(ve.getColumnValue(sp[1]).toString()).toFBSString());
						} catch (InterpretException e) {
							HS_Util.debug("ON " + sParser + "InterpretException: " + e.toString(), "error", dbarLoc, sdt);
						} catch (Exception esp) {
							HS_Util.debug("ON " + sParser + ": Error: " + esp.toString(), "error", dbarLoc, sdt);
						}
					}
					try {
						nobj.put("unid", FBSUtility.wrap(ve.getUniversalID()).toFBSString());
						nobj.put("used", FBSUtility.wrap("no").toFBSString());
					} catch (InterpretException e) {
						HS_Util.debug(e.toString(), "error", dbarLoc, sdt);
					}
					dataViolations.add(nobj);
				}
				try {
					Collections.sort(dataViolations, Violation_Order);
				} catch (Exception e) {
					HS_Util.debug("Error in sorting: " + e.toString(), "error", dbarLoc, sdt);
				}
				setLastViolVModule(vModule);
				if ( localDebug ) HS_Util.debug("added [" + dataViolations.size() + "]", "debug", dbarLoc, sdt);
				violView.setAutoUpdate(true);
			} catch (Exception e) {
				HS_Util.debug(e.toString(), "error", dbarLoc, sdt);
				if ( isDebugServer() ) e.printStackTrace();
			}
		}
		return dataViolations;
	}

	static final Comparator<ObjectObject> Violation_Order = new Comparator<ObjectObject>() {
		public int compare(ObjectObject viol0, ObjectObject viol1) {
			try {
				String code0 = viol0.get("code").stringValue();
				String code1 = viol1.get("code").stringValue();
				if ( code0 == null ) return 0;
				if ( code1 == null ) return 0;
				return code1.compareTo(code0);
			} catch (InterpretException e) {
				return 0;
			}
		}
	};

	public Vector<ObjectObject> getDataSections() {
		return dataSections;
	}

	public void setDataSections(Vector<ObjectObject> dataSections) {
		this.dataSections = dataSections;
	}

	public Vector<ObjectObject> getDataViolations() {
		if ( dataViolations == null ) {

		}
		return dataViolations;
	}

	public void setDataViolations(Vector<ObjectObject> dataViolations) {
		this.dataViolations = dataViolations;
	}

	public Vector<ObjectObject> getDataObservation() {
		return dataObservation;
	}

	public void setDataObservation(Vector<ObjectObject> dataObservation) {
		this.dataObservation = dataObservation;
	}

	public String getDbarHead() {
		return dbarHead;
	}

	public Vector<String> getPl_inspectionTypes() {
		return pl_inspectionTypes;
	}

	public void setPl_inspectionTypes(Vector<String> pl_inspectionTypes) {
		this.pl_inspectionTypes = pl_inspectionTypes;
	}

	public Vector<String> getPlGroups() {
		return plGroups;
	}

	public void setPlGroups(Vector<String> plGroups) {
		this.plGroups = plGroups;
	}

	public Vector<String> getPlCategories() {
		return plCategories;
	}

	public void setPlCategories(Vector<String> plCategories) {
		this.plCategories = plCategories;
	}

	public Vector<String> getPlSections() {
		return plSections;
	}

	public void setPlSections(Vector<String> plSections) {
		this.plSections = plSections;
	}

	public String getLastSectVModule() {
		if ( lastSectVModule == null ) lastSectVModule = "";
		return lastSectVModule;
	}

	public void setLastSectVModule(String lastSectVModule) {
		this.lastSectVModule = lastSectVModule;
	}

	public String getLastViolVModule() {
		if ( lastViolVModule == null ) lastViolVModule = "";
		return lastViolVModule;
	}

	public void setLastViolVModule(String lastViolVModule) {
		this.lastViolVModule = lastViolVModule;
	}

	public String getLastObserveVModule() {
		if ( lastObserveVModule == null ) lastObserveVModule = "";
		return lastObserveVModule;
	}

	public void setLastObserveVModule(String lastObserveVModule) {
		this.lastObserveVModule = lastObserveVModule;
	}

}
