package com.healthspace.general;

import java.io.IOException;
import java.io.StringWriter;

import com.ibm.domino.services.util.JsonWriter;

public class ZOld_RestPostBasic {

	public ZOld_RestPostBasic() {

	}

	// @SuppressWarnings("unchecked")
	public String basicPost() {
		String msgContext = "RestPostBasic.basicPost";
		StringWriter sw = new StringWriter();
		JsonWriter jw = new JsonWriter(sw, false);

		try {
			jw.startObject();
			jw.startProperty("objectSource");
			jw.outStringLiteral(msgContext);
			jw.endProperty();
			jw.startProperty("error");
			jw.outStringLiteral("this object should not be used.");
			jw.endProperty();
			jw.endObject();
			return sw.toString();
			/*
			 * // List<Map<String, Object>> rmap = //
			 * JSFUtil.getFacesContext().getExternalContext().getRequestMap();
			 * HttpServletRequest req = (HttpServletRequest)
			 * JSFUtil.getFacesContext().getExternalContext().getRequest();
			 * 
			 * jw.startProperty("parameterNames"); jw.startArray(); for
			 * (Enumeration<String> e = req.getParameterNames();
			 * e.hasMoreElements();) { // System.out.println(e.nextElement());
			 * jw.startArrayItem(); jw.outStringLiteral(e.nextElement());
			 * jw.endArrayItem(); } jw.endArray(); jw.endProperty();
			 * 
			 * if (this.createOutputDoc(req, jw)) { jw.startProperty("status");
			 * jw.outStringLiteral("success"); jw.endProperty(); } else {
			 * jw.startProperty("status");
			 * jw.outStringLiteral("no soup for you"); jw.endProperty(); }
			 * jw.endObject(); return sw.toString();
			 */

		} catch (IOException e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			return "{\"error\":\"" + e.toString() + " in " + msgContext + "\"}";
		}
	}

	/*
	 * @SuppressWarnings("unchecked") private boolean
	 * createOutputDoc(HttpServletRequest req, JsonWriter jw) { final String
	 * msgContext = "restPostBasic.createOutputDoc"; try { Session sess =
	 * Factory.getSession(); Database db = sess.getCurrentDatabase(); Document
	 * doc = db.createDocument(); doc.replaceItemValue("Form", "RestBasicPost");
	 * doc.replaceItemValue("PostAuthor", sess.getEffectiveUserName());
	 * Vector<String> pnames = new Vector<String>(); String pname = ""; for
	 * (Enumeration<String> e = req.getParameterNames(); e.hasMoreElements();) {
	 * pname = e.nextElement(); pnames.add(pname); doc.replaceItemValue(pname,
	 * req.getParameter(pname)); } doc.replaceItemValue("PostItems", pnames);
	 * 
	 * if (doc.save()) { jw.startProperty("unid");
	 * jw.outStringLiteral(doc.getUniversalID()); jw.endProperty(); return true;
	 * } else { jw.startProperty("unid"); jw.outStringLiteral("");
	 * jw.endProperty(); jw.startProperty("error");
	 * jw.outStringLiteral("save failed"); jw.endProperty(); return false;
	 * 
	 * }
	 * 
	 * } catch (Exception e) { HS_Util.debug(e.toString(), "error", msgContext);
	 * try { jw.startProperty("error"); jw.outStringLiteral(e.toString() +
	 * " in " + msgContext); jw.endProperty(); } catch (IOException e1) { }
	 * 
	 * } return false; }
	 */
}
