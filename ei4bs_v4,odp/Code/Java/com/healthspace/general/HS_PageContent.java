package com.healthspace.general;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Set;
import java.util.Vector;

import org.openntf.domino.View;
import org.openntf.domino.ViewEntry;
import org.openntf.domino.ViewNavigator;
import org.openntf.domino.utils.Factory;

public class HS_PageContent implements Serializable {
	private static final long serialVersionUID = 1L;

	public HS_PageContent() {
	}

	private static HashMap<String, HS_Page> pages;

	public static void clearPages() {
		// pages.clear();
		pages = new HashMap<String, HS_Page>();
	}

	private static void loadAllPages() {
		final boolean localDebug = "Yes".equals(HS_Util.getAppSettingString("debug_content"));
		final String dbarLoc = "HS_PageContent.loadAllPages";

		HS_Page wrkpage = null;
		pages = new HashMap<String, HS_Page>();
		try {
			HS_Util.getSession().setConvertMime(false);

			View cview = HS_Util.getSession().getCurrentDatabase().getView("code.content.lookup");
			ViewNavigator cvnav = cview.createViewNav();
			for (ViewEntry cvntry : cvnav) {
				wrkpage = new HS_Page();
				wrkpage.setKey(cvntry.getColumnValue("ContentKey").toString());

				wrkpage.setMenu_hierarchy(cvntry.getColumnValue("ContentMenuHierarchy").toString());
				wrkpage.setMenu_position(cvntry.getColumnValue("ContentMenuPosition").toString());
				wrkpage.setMenu_status(cvntry.getColumnValue("ContentMenuStatus").toString());
				wrkpage.setMenu_icon(cvntry.getColumnValue("ContentMenuIcon").toString());
				wrkpage.setAuthor(cvntry.getColumnValue("HSContentAuthor").toString());
				wrkpage.setModified(cvntry.getColumnValue("ContentUpdated").toString());
				wrkpage.setTitle(cvntry.getColumnValue("ContentTitle").toString());

				wrkpage.setUnid(cvntry.getUniversalID());

				wrkpage.setHtml(cvntry.getDocument().getItemValueString("html"));

				pages.put(wrkpage.getKey(), wrkpage);
				// if (localDebug) HS_Util.debug("added page key:[" + wrkpage.getKey() + "] title:[" + wrkpage.getTitle() + "]", "debug",
				// dbarLoc);
			}
			if (localDebug) HS_Util.debug("added [" + pages.size() + "] pages", "debug", dbarLoc);
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			if (HS_Util.isDebugServer()) e.printStackTrace();
		} finally {
			HS_Util.getSession().setConvertMime(true);
		}
	}

	/*
	 * This method returns JUST the key, title, and html components in a map...
	 */
	public static HashMap<String, String> loadPage(String pkey) {
		final boolean localDebug = "Yes".equals(HS_Util.getAppSettingString("debug_content"));
		final String dbarLoc = "HS_PageContent.loadPage";

		HashMap<String, String> result = new HashMap<String, String>();
		result.put("key", pkey);
		HS_Page wrkpage = null;
		// String msg = "";
		// String html = "";
		if (pages == null) HS_PageContent.loadAllPages();
		if (pages.containsKey(pkey)) wrkpage = pages.get(pkey);

		if (wrkpage != null) {
			result.put("title", (wrkpage.getTitle() == null) ? "title is null" : wrkpage.getTitle());
			result.put("html", (wrkpage.getHtml() == null) ? "html is null" : wrkpage.getHtml());
			if (localDebug) HS_Util.debug("for pkey:[" + pkey + "] returning page title:[" + result.get("title") + "]", "debug", dbarLoc);
		}
		return result;
	}

	/*
	 * This method returns the HS_Page object
	 */
	public HS_Page getPage(String pkey) {
		// final boolean localDebug = "Yes".equals(HS_Util.getAppSettingString("debug_content"));
		final boolean localDebug = false;
		final String dbarLoc = "HS_PageContent.getPage";

		if (localDebug) HS_Util.debug("Looking for page:[" + pkey + "]", "debug", dbarLoc);
		HS_Page result = null;
		if (pages == null) HS_PageContent.loadAllPages();
		if (pages.isEmpty()) HS_PageContent.loadAllPages();
		result = pages.get(pkey);
		if ((result != null) && localDebug) HS_Util.debug("found page:[" + result.getTitle() + "]", "debug", dbarLoc);
		return result;
	}

	public long getSerialVersionUID() {
		return serialVersionUID;
	}

	public HashMap<String, HS_Page> getPages() {
		if (pages == null) HS_PageContent.loadAllPages();
		return pages;
	}

	public Vector<HS_Page> getPagesVector() {
		final boolean localDebug = "Yes".equals(HS_Util.getAppSettingString("debug_content"));
		final String dbarLoc = "HS_PageContent.getPagesVector";

		Vector<HS_Page> result = new Vector<HS_Page>();

		if (pages == null) HS_PageContent.loadAllPages();

		if (pages.isEmpty()) HS_PageContent.loadAllPages();
		if (pages.isEmpty()) {
			if (localDebug) HS_Util.debug("pages are empty. returning nothing", "debug", dbarLoc);
			return result;
		}

		Set<String> keys = pages.keySet();
		// if (localDebug) HS_Util.debug("there are [" + keys.size() + "] page keys to process.", "debug", dbarLoc);
		for (String key : keys) {
			result.add(pages.get(key));
		}

		// if (localDebug) HS_Util.debug("there are [" + result.size() + "] page keys to sort.", "debug", dbarLoc);
		Collections.sort(result, Sort_Pages_By_Key_Order);
		return result;
	}

	public Vector<HS_Page> getPagesForMenu() {
		final boolean localDebug = "Yes".equals(HS_Util.getAppSettingString("debug_content"));
		final String dbarLoc = "HS_PageContent.getPagesVector";

		Vector<HS_Page> result = new Vector<HS_Page>();

		if (pages == null) HS_PageContent.loadAllPages();

		if (pages.isEmpty()) HS_PageContent.loadAllPages();
		if (pages.isEmpty()) {
			if (localDebug) HS_Util.debug("pages are empty. returning nothing", "debug", dbarLoc);
			return result;
		}

		Set<String> keys = pages.keySet();
		// if (localDebug) HS_Util.debug("there are [" + keys.size() + "] page keys to process.", "debug", dbarLoc);
		for (String key : keys) {
			result.add(pages.get(key));
		}

		// if (localDebug) HS_Util.debug("there are [" + result.size() + "] page keys to sort.", "debug", dbarLoc);
		Collections.sort(result, Sort_Pages_By_Menu_Order);
		return result;
	}

	public static Vector<HS_Page> getTopLevelPagesForMenu() {
		final boolean localDebug = "Yes".equals(HS_Util.getAppSettingString("debug_content"));
		final String dbarLoc = "HS_PageContent.getTopLevelPagesForMenu";

		Vector<HS_Page> result = new Vector<HS_Page>();

		if (pages == null) HS_PageContent.loadAllPages();

		if (pages.isEmpty()) HS_PageContent.loadAllPages();
		if (pages.isEmpty()) {
			if (localDebug) HS_Util.debug("pages are empty. returning nothing", "debug", dbarLoc);
			return result;
		}
		String user = Factory.getSession().getEffectiveUserName();
		Set<String> keys = pages.keySet();
		// if (localDebug) HS_Util.debug("there are [" + keys.size() + "] page keys to process.", "debug", dbarLoc);
		for (String key : keys) {
			HS_Page page = pages.get(key);
			if (isOkToUse(page)) {
				if (!page.getMenu_hierarchy().contains("\\")) {
					if (localDebug)
						HS_Util.debug("using page:[" + page.getKey() + "]:[" + page.getMenu_hierarchy() + "] status:[" + page.getMenu_status() + "] user:[" + user + "]", "debug", dbarLoc);
					result.add(page);
				}
			}

		}

		if (localDebug) HS_Util.debug("there are [" + result.size() + "] page keys to sort.", "debug", dbarLoc);
		Collections.sort(result, Sort_Pages_By_Menu_Order);
		return result;
	}

	private static boolean isOkToUse(HS_Page page) {
		return isOkToUse(page, Factory.getSession().getEffectiveUserName());
	}

	private static boolean isOkToUse(HS_Page page, String user) {
		return isOkToUse(page, user, false);
	}

	private static boolean isOkToUse(HS_Page page, String user, boolean localDebug) {
		// final boolean localDebug = "Yes".equals(HS_Util.getAppSettingString("debug_content"));
		// final boolean localDebug = false;
		final String dbarLoc = "HS_PageContent.isOkToUse";

		boolean result = false;
		if ("".equals(page.getMenu_hierarchy())) {
			// if (localDebug) HS_Util.debug("skipping page:[" + page.getKey() + "]:[" + page.getMenu_hierarchy() + "]", "debug", dbarLoc);
			result = false;
		} else {
			if ("Retired".equals(page.getMenu_status())) {
				if (localDebug) HS_Util.debug("skipping page:[" + page.getKey() + "]:[" + page.getMenu_hierarchy() + "] status:[" + page.getMenu_status() + "]", "debug", dbarLoc);
				result = false;
			} else {
				if ("Anonymous".equals(user) && "Draft".equals(page.getMenu_status())) {
					if (localDebug) HS_Util.debug("skipping page:[" + page.getKey() + "]:[" + page.getMenu_hierarchy() + "] status:[" + page.getMenu_status() + "]", "debug", dbarLoc);
					result = false;
				} else {
					// if (localDebug)
					// HS_Util.debug("allowing page:[" + page.getKey() + "]:[" + page.getMenu_hierarchy() + "] status:[" +
					// page.getMenu_status() + "] user:[" + user + "]", "debug", dbarLoc);
					result = true;
				}
			}

		}
		return result;
	}

	public void setPages(HashMap<String, HS_Page> pages) {
		HS_PageContent.pages = pages;
	}

	public static boolean hasChildren(HS_Page page) {
		boolean result = false;
		final boolean localDebug = "Yes".equals(HS_Util.getAppSettingString("debug_content"));
		// final boolean localDebug = false;
		final String dbarLoc = "HS_PageContent.hasChildren";

		String user = Factory.getSession().getEffectiveUserName();
		Set<String> keys = pages.keySet();
		for (String key : keys) {
			HS_Page tpage = pages.get(key);
			if (isOkToUse(tpage, user, localDebug)) {
				if (!("".equals(tpage.getMenu_hierarchy())) && (!page.getMenu_hierarchy().equals(tpage.getMenu_hierarchy()))) {
					if (tpage.getMenu_hierarchy().startsWith(page.getMenu_hierarchy() + "\\")) {
						if (localDebug) HS_Util.debug("for [" + page.getMenu_hierarchy() + "] found a child at:[" + tpage.getMenu_hierarchy() + "]", "debug", dbarLoc);
						result = true;
					}
				}
			}
		}

		return result;
	}

	public Vector<HS_Page> getChildren(HS_Page page) {
		Vector<HS_Page> result = new Vector<HS_Page>();
		// final boolean localDebug = "Yes".equals(HS_Util.getAppSettingString("debug_content"));
		final boolean localDebug = false;
		final String dbarLoc = "HS_PageContent.getChildren";

		Set<String> keys = pages.keySet();
		for (String key : keys) {
			HS_Page tpage = pages.get(key);
			if (HS_PageContent.isOkToUse(tpage)) {
				if (!("".equals(tpage.getMenu_hierarchy())) && (!page.getMenu_hierarchy().equals(tpage.getMenu_hierarchy()))) {
					if (tpage.getMenu_hierarchy().startsWith(page.getMenu_hierarchy() + "\\")) {
						if (localDebug) HS_Util.debug("for [" + page.getMenu_hierarchy() + "] found a child at:[" + tpage.getMenu_hierarchy() + "]", "debug", dbarLoc);
						result.add(tpage);
					}
				}
			}
		}
		return result;
	}

	static final Comparator<HS_Page> Sort_Pages_By_Key_Order = new Comparator<HS_Page>() {
		public int compare(HS_Page page0, HS_Page page1) {
			try {
				String key0 = page0.getKey().toLowerCase();
				String key1 = page1.getKey().toLowerCase();
				if (key0.equals(key1)) return 0;
				return key0.compareTo(key1);
			} catch (Exception e) {
				return 0;
			}
		}
	};

	static final Comparator<HS_Page> Sort_Pages_By_Menu_Order = new Comparator<HS_Page>() {
		public int compare(HS_Page page0, HS_Page page1) {
			String key0 = "";
			String key1 = "";
			try {
				key0 = page0.getMenu_position().toLowerCase();
				key1 = page1.getMenu_position().toLowerCase();
				if (!key0.equals(key1)) return key0.compareTo(key1);

				key0 = page0.getMenu_hierarchy().toLowerCase();
				key1 = page1.getMenu_hierarchy().toLowerCase();
				if (!key0.equals(key1)) return key0.compareTo(key1);

				return 0;
			} catch (Exception e) {
				return 0;
			}
		}
	};
}
