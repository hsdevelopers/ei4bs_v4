package com.healthspace.general;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringEscapeUtils;
import org.openntf.EMailBean;
import org.openntf.domino.Database;

import com.ibm.domino.services.util.JsonWriter;
import com.ibm.xsp.extlib.beans.UserBean;

public class HS_Support implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HS_Support() {
	}

	public static String sendErrorToSupport() {
		final boolean localDebug = true;
		final String msgContext = "QueryFacilities.sendErrorToSupport";
		HashMap<String, String> result = new HashMap<String, String>();

		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		String sourceUrl = request.getParameter("sourceUrl");
		result.put("sourceUrl", sourceUrl);
		result.put("status", "post started.");
		// result.put("db", "");
		// result.put("server", "");

		// EMailBean emailBean = EMailBean.get();
		UserBean ubean = UserBean.get();

		try {
			Database db = HS_Util.getSession().getCurrentDatabase();

			String sptemail = HS_Util.getAppSettingString("SupportEMail");
			String odata = request.getParameter("otherData");
			String errorSource = request.getParameter("errorSource");
			if ("".equals(errorSource)) errorSource = "<h4>no source</h4>";
			String errorException = request.getParameter("errorException");
			String stackTrace = request.getParameter("stackTrace");
			String style = request.getParameter("style");
			String subject = "Error message from " + ubean.getDisplayName() + " in database: " + db.getFilePath();
			String sender = "DoNotReply@healthspace.com";
			String principal = HS_Util.getSession().getCommonUserName() + " - " + db.getTitle();
			String banner = "<p>An error message was reported to the user <strong>" + ubean.getDisplayName() + "</strong></p>";
			String body = "<p>URL: " + unescape(sourceUrl) + "<br/>";

			if ("".equals(sptemail)) throw (new Exception("Support email is not setup for this application."));
			// if ("".equals(sptemail)) sptemail = "support@healthspace.ca";
			if (localDebug) HS_Util.debug("sptemail:[" + sptemail + "]", "debug", msgContext);
			if (localDebug) HS_Util.debug("senderName:[" + sender + "]", "debug", msgContext);

			result.put("db", db.getTitle() + " (" + db.getFilePath() + ")");
			result.put("server", db.getServer());

			body += "Database: <strong>" + db.getTitle() + "</strong> (" + db.getFilePath() + ")<br/>";
			body += "Server: <strong>" + db.getServer() + "</strong></p>";
			body += "<style>" + style + "</style>";
			if (!"".equals(odata)) {
				body += "<h2 style=\"font-size: 14pt; margin-bottom: 2px; margin-top: 12px;\">Other data</h2>";
				body += "<p style=\"margin-bottom: 2px; margin-top: 2px;\">" + odata + "</p>";
			}
			body += errorSource;
			body += errorException;
			body += "<h2>Stack trace</h2><pre>" + stackTrace + "</pre>";
			// body += "";

			result.put("status", sendEMail(sptemail, sender, principal, subject, banner, body));

			return writeJson(result);
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if (HS_Util.isDebugServer()) e.printStackTrace();
			System.out.print("HS_Support.sendErrorToSupport: Error: " + e.toString());
			return "{\"status\" : \"Error: " + e.toString() + "\"}";
		}
	}

	private static String sendEMail(String sptemail, String sender, String principal, String subject, String banner, String body) {
		try {
			EMailBean emailBean = new EMailBean();
			emailBean.setSendTo(sptemail);
			emailBean.setSubject(subject);
			emailBean.setSenderEmail(sender);
			// emailBean.setSenderName(principal);

			emailBean.setFieldName("Body");
			emailBean.setBannerHTML(banner);
			emailBean.addHTML(body);
			emailBean.send();

			return "message sent to " + emailBean.getSendTo();
		} catch (Exception e) {
			return "Error: " + e.toString();
		}
	}

	private static String writeJson(HashMap<String, String> result) {
		// final boolean localDebug = false;
		final String msgContext = "QueryFacilities.writeJson";

		StringWriter sw = new StringWriter();
		JsonWriter jw = new JsonWriter(sw, false);
		try {
			jw.startObject();
			for (Entry<String, String> valEntry : result.entrySet()) {
				jw.startProperty(valEntry.getKey());
				jw.outStringLiteral(valEntry.getValue());
				jw.endProperty();
			}
			jw.endObject();
			return sw.toString();
		} catch (IOException e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if (HS_Util.isDebugServer()) e.printStackTrace();
			return "{\"status\" : \"Error: " + e.toString() + "\"}";
		}

	}

	private static String unescape(String src) {
		return StringEscapeUtils.unescapeJava(src);
		// return src;
	}
}
