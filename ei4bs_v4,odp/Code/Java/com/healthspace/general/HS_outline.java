package com.healthspace.general;

import org.openntf.domino.Database;
import org.openntf.domino.Outline;

public class HS_outline extends HS_base {
	private static final long serialVersionUID = 1L;

	public HS_outline() {

	}

	public Outline getOutline(Database wdb, String olName) {
		Outline ol = null;
		// int i = 1;

		final boolean localDebug = (HS_Util.isDebugServer() || HS_Util.isDebugger());
		String dbarLoc = getSession().getCurrentDatabase().getFilePath() + "." + "HS_outline.getOutline()";

		try {
			if (localDebug) debug("Looking for outline " + olName + " in db: " + wdb.getFilePath(), "debug", dbarLoc);
			ol = wdb.getOutline(olName);

			return ol;
		} catch (Exception e) {
			debug("exception : " + e.toString(), "error", dbarLoc);
			System.out.print(dbarLoc + " exception : " + e.toString());
			System.out.print("*************************************************************************************************************");
		} finally {

		}

		return ol;
	}
}
