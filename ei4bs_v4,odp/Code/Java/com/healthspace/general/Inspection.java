package com.healthspace.general;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;

import org.apache.commons.lang.StringUtils;
import org.openntf.domino.Database;
import org.openntf.domino.DateTime;
import org.openntf.domino.Document;
import org.openntf.domino.View;

import com.ibm.jscript.InterpretException;
import com.ibm.jscript.std.ArrayObject;
import com.ibm.jscript.std.ObjectObject;
import com.ibm.jscript.types.FBSUtility;
import com.ibm.jscript.types.FBSValue;
import com.ibm.xsp.component.UIViewRootEx2;
import com.ibm.xsp.extlib.util.ExtLibUtil;

public class Inspection implements Serializable {

	/**
	 * 
	 */
	private static final long					serialVersionUID		= 1L;

	static final Comparator<InspectionSection>	Sort_Section_Order		= new Comparator<InspectionSection>() {
																			public int compare(InspectionSection sec0, InspectionSection sec1) {
																				try {
																					String snum0 = sec0.getNumber();
																					String snum1 = sec1.getNumber();
																					if ( snum0 == null ) snum0 = "";
																					if ( snum1 == null ) snum1 = "";
																					if ( snum0.compareTo(snum1) != 0 ) return snum0.compareTo(snum1);

																					String desc0 = sec0.getDescription();
																					String desc1 = sec1.getDescription();
																					if ( desc0 == null ) return 0;
																					if ( desc1 == null ) return 0;
																					return desc0.compareTo(desc1);
																				} catch (Exception e) {
																					return 0;
																				}
																			}
																		};

	static final Comparator<Violation>			Sort_Violation_Order	= new Comparator<Violation>() {
																			public int compare(Violation viol0, Violation viol1) {
																				try {
																					String code0 = viol0.getCode();
																					String code1 = viol1.getCode();

																					/*
																					 * As entered or stored | std By code | code By criticality or priority | priority By description | description
																					 */
																					String vso = HS_Util.getAppSettingString("inspViolationsOrder");
																					if ( "".equals(vso) || "std".equals(vso) ) {
																						if ( code0 == null ) return 0;
																						if ( code1 == null ) return 0;
																						return code0.compareTo(code1);
																					} else if ( "priority".equals(vso) ) {
																						String pri0 = viol0.getCriticalSet().toLowerCase();
																						String pri1 = viol1.getCriticalSet().toLowerCase();
																						if ( pri0 == null ) return 0;
																						if ( pri1 == null ) return 0;
																						pri0 = (pri0.indexOf("core") > -1 || pri0.indexOf("no") > -1) ? "30-" + pri0 : (pri0.indexOf("foundation") > -1 || pri0
																												.indexOf("potential") > -1) ? "20-" + pri0 : (pri0.indexOf("priority") > -1 || pri0
																												.indexOf("yes") > -1) ? "10-" + pri0 : pri0;
																						pri1 = (pri1.indexOf("core") > -1 || pri1.indexOf("no") > -1) ? "30-" + pri1 : (pri1.indexOf("foundation") > -1 || pri1
																												.indexOf("potential") > -1) ? "20-" + pri1 : (pri1.indexOf("priority") > -1 || pri1
																												.indexOf("yes") > -1) ? "10-" + pri1 : pri1;
																						// System.out.println("inspection.Sort_Violation_Order: vso:["
																						// +
																						// vso
																						// +
																						// "] pri0:["
																						// +
																						// pri0
																						// +
																						// "] pri1:["
																						// +
																						// pri1
																						// +
																						// "]");
																						if ( pri0.compareTo(pri1) != 0 ) return pri0.compareTo(pri1);
																						if ( code0 == null ) return 0;
																						if ( code1 == null ) return 0;
																						return code0.compareTo(code1);
																					} else if ( "description".equals(vso) ) {
																						String desc0 = viol0.getDescription().toLowerCase();
																						String desc1 = viol1.getDescription().toLowerCase();
																						if ( desc0 == null ) return 0;
																						if ( desc1 == null ) return 0;
																						return desc0.compareTo(desc1);
																					} else if ( "section".equals(vso) ) {
																						String sect0 = viol0.getSection();
																						String sect1 = viol1.getSection();
																						if ( !(sect0 == null) || (sect1 == null) ) {
																							if ( !sect0.equals(sect1) ) return sect0.compareTo(sect1);
																						}
																						;
																						String desc0 = viol0.getDescription().toLowerCase();
																						String desc1 = viol1.getDescription().toLowerCase();
																						if ( desc0 == null ) return 0;
																						if ( desc1 == null ) return 0;
																						return desc0.compareTo(desc1);
																					}
																					return 0;
																				} catch (Exception e) {
																					return 0;
																				}
																			}
																		};

	private final static String					vdScopeKey				= "HS_InspectionViewData";

	public static Inspection get() {
		return (Inspection) resolveVariable("inspection");
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public static Comparator<InspectionSection> getSort_Section_Order() {
		return Sort_Section_Order;
	}

	public static Comparator<Violation> getSort_Violation_Order() {
		return Sort_Violation_Order;
	}

	public static String getVdScopeKey() {
		return vdScopeKey;
	}

	private static Object resolveVariable(String variableName) {
		FacesContext context = FacesContext.getCurrentInstance();
		return context.getApplication().getVariableResolver().resolveVariable(context, variableName);
	}

	private String						allYearRound;
	private String						billingType;
	private String						createdFromInspID;
	private String						displayFollowUp;
	private String						dbDesign;
	private String						documentId;
	private String						eho;

	// This is used to handle the EHO Phone and SubOffice information
	private ObjectObject				ehoData;
	private String						ehsId;
	private Vector<Object>				ehsReaders;
	private String						enforcement;

	private Date						eventEndDate;
	private Date						eventStartDate;
	private String						eventId;
	private String						eventName;
	private String						facilityName;
	private String						facilityPhysicalBuilding;
	private String						facilityPhysicalCity;
	private String						facilityPhysicalDirection;
	private String						facilityPhysicalPostalCode;
	private String						facilityPhysicalProvince;
	private String						facilityPhysicalRegType;
	private String						facilityPhysicalStreetName;
	private String						facilityPhysicalStreetSuffix;
	private String						facilityPhysicalStreetType;
	private String						facilityPhysicalSuite;
	private String						facilityTelephone;

	private String						facilityType;
	private String						facilityApprovedNumber;
	private String						facilityRegType;

	private String						facilityID2;
	private String						magDistrict;
	private String						followupInspectionRequired;
	private String						form;

	private Date						inspectionDate;
	private String						inspectionDateStr;
	private InspectionPicklists			inspPlObj;
	private String						inspType;
	private String						letterGrade;

	private Date						nextInspectionDate;
	private String						nextInspectionDateStr;
	private Date						nextManualDue;
	private Date						nextInspDue;
	private String						foodSafe;
	private String						certifiedManager;
	private String						certified;
	private String						certNumber;
	private String						certExpiration;

	private String						chokingPoster;
	private String						smoking;

	private Integer						observedSeating;
	private String						licenseDisplayed;
	private String						comments;
	private List<String>				imageUnids;
	private int							numCore;
	private int							numCritical;
	private int							numKey;
	private int							numNonCritical;
	private int							numOther;
	private int							numPFoundation;

	private int							numPriority;
	private int							numRepeat;
	private int							numRisk;

	private int							computedCriticals;
	private Vector<String>				repeatC;

	private Vector<String>				repeatK;
	private Vector<String>				repeatO;
	private String						ownerId;
	private String						ownersAddress;
	private String						ownersCity;
	private String						ownersName;
	private String						ownersPostalCode;

	private String						ownersState;
	private String						parentForm;

	private String						parentUnid;
	private String						physicalMunicipality;
	// Temperature lists
	private Vector<String>				equipDescrip;

	private Vector<String>				equipTemp;
	private Vector<String>				foodDescrip;
	private Vector<String>				foodTemp;
	private Vector<String>				foodState;
	private Vector<String>				machineName;
	private Vector<String>				saniMethod;
	private Vector<String>				thermoLabel;

	private Vector<String>				ppm;
	private Vector<String>				saniName;
	private Vector<String>				saniType;
	private Vector<String>				wareWashTemp;
	private String						signatureData1;
	private String						signatureData2;
	private String						hasSig1;
	private String						hasSig2;
	private Vector<String>				signatureHistory;
	private String						signatureData64_1;
	private String						signatureData64_2;

	private String						source;
	private Date						dateCreated;
	private String						receivedBy;
	private String						receivedByTitle;
	// Certified Manager Elements
	private Vector<String>				manager;

	private Vector<String>				managerAddress;
	private Vector<String>				managerCertificateExpirationStr;
	private Vector<String>				managerCertificateId;
	private Vector<String>				managerId;
	private Vector<String>				managerAllergenTraining;

	// Tobacco Inspection Questions from TDA
	private boolean						useTobacco;
	private String						tobaccoSold;
	private String						tobaccoProducts2;
	private String						tobaccoSign;

	private String						tobaccoProducts;
	private String						tobaccoMachine;
	private String						tobaccoObserved;
	private String						tobaccoCounter;
	// Various pick lists
	private Vector<String>				pl_inspectionTypes;

	private Vector<String>				plCategories;

	private Vector<String>				plGroups;

	private Vector<ObjectObject>		plObservationData;
	private Vector<String>				plSections;
	private Vector<String>				plViolationList;
	// private Vector<ObjectObject> plSectionsData;
	private Vector<ObjectObject>		plViolationsData;
	private String						prevVMFacType;
	private Date						release;
	private String						riskRating;

	private Vector<TimeTracking>		timeTrackingRecords;

	private TimeTracking				timeTracking;

	private double						timeSpent;
	// private String timeTravelStr;
	// private String timeInspStr;
	private int							totalViolations;
	private int							totHazardRating;

	private String						hazRatingType;

	// Section fields implemented 10 Oct 2014 to track in - out - n/a - n/o
	private Vector<InspectionSection>	sections;
	private String						unid;
	private Vector<String>				validationMessages;				// Inspection
	// Validation
	// Messages

	private String						viewDescription;
	private Vector<String>				violationModule;
	private Vector<Violation>			violations;
	private Vector<Violation>			correctedViolations;
	private String						violationSection;
	private Vector<String>				violationSubModule;
	private String						wrkCat;							// Not
	// saved
	// to
	// doc
	private Date						wrkCorrectByDate;					// Formatted
	// and
	// appended
	// to
	// Corrective
	// Action
	private String						wrkCorrectedSet;					// ViolCorrectedSet

	private String						wrkCorrectiveAction;				// ViolCorrectiveAction
	private String						wrkCritical;						// ViolCriticalSet

	private String						wrkDescription;					// ViolDescription
	private String						wrkGrp;							// Not
	// saved
	// to
	// doc

	private String						wrkHzrdRating;						// ViolHzrdRating
	private String						wrkInOut;							// Not
	// saved
	// to
	// doc
	private String						wrkLegalText;						// ViolLegalText

	private Vector<String>				wrkMessages;						// Violation
	// Validation
	// Messages

	private String						wrkObservation;					// ViolObservation

	private String						wrkRepeat;							// ViolRepeat

	private String						wrkSection;						// ViolSction
	private String						wrkStatus;							// ViolStatus

	private String						wrkViolation;						// ViolCode
	private HS_auditForm				loadedForm;

	private ArrayList<Violation>		previousViolations;

	private ArrayList<String>			saveMessages;
	private String						uniqueViewId;
	private String						violationSortOrder;
	private String						environment;
	// Configuration items from Notes...
	private String						useHazardRating;
	private String						useHazard;
	private String						dtmuhrsc;							// DTMUHRSC
	// =
	// 'Does
	// This
	// Module
	// Use
	// Hazard
	// Rating
	// Score Calculation'
	private String						dtmulg;							// DTMULG
	// =
	// 'Does
	// This
	// Module
	// Use
	// Letter
	// Grades'
	private String						gboc;								// GradeBasedOnCriticals
	private String						dgbonc;							// DropGradeBasedOnNonCorrected
	private String						letterGrades;
	private String						gradeRanges;
	private String						criticalViolationRanges;

	private String						htcc;								// HowToCountCriticals
	private Vector<String>				followupTypes;

	private String						gradeComputedByHazRating;
	private String						gradeComputedByNumCriticals;
	private String						announceInsp;
	private String						inspectionOutcome;
	private String						billablePermit;

	private String						billable;

	private String						childRatio;

	private Date						complaintDate;						// Should
	// this
	// be
	// a
	// date?

	private String						complianceSet;

	private String						locked;

	private Date						scheduledInspectionDate;

	private Map<String, Object>			fullCloudMap;

	private String						emailSent;

	private boolean						tabletDebug;
	private boolean						hSTouch;

	private transient UIComponent		commentInput;

	public Inspection() {
		// TODO - Main constroctor - disable debug stuff on final releases
		final boolean localDebug = false;
		tabletDebug = false;
		final String dbarLoc = "inspectionbean.constructor";
		try {
			UIViewRoot vr = FacesContext.getCurrentInstance().getViewRoot();
			UIViewRootEx2 vr2 = (UIViewRootEx2) vr;
			if ( vr2 != null ) {
				this.uniqueViewId = vr2.getUniqueViewId();

				if ( localDebug ) {
					System.out.println(dbarLoc + " Constructor executed  - getUniqueViewId:[" + uniqueViewId + "]");
				}
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			e.printStackTrace();
		}
	}

	public Vector<ObjectObject> buildManagerDisplay() {
		// final boolean localDebug = false;
		final String dbarLoc = "inspection.buildManagerDisplay.2";

		final String labelMgr = "manager";
		final String labelAddr = "address";
		final String labelCertId = "certificateId";
		final String labelCertExp = "certificateExp";
		final String labelMgrId = "managerId";

		// String design = this.getDbDesign();

		Vector<ObjectObject> rslts = new Vector<ObjectObject>();
		ObjectObject rslt = new ObjectObject();
		try {
			Vector<String> mgrs = this.getManager();
			if ( mgrs == null ) mgrs = new Vector<String>();
			Vector<String> mgrIds = this.getManagerId();
			Vector<String> mgrAddrs = this.getManagerAddress();
			Vector<String> certExpStr = this.getManagerCertificateExpirationStr();
			Vector<String> certIds = this.getManagerCertificateId();
			if ( mgrAddrs == null ) mgrAddrs = new Vector<String>();
			while (mgrAddrs.size() < mgrs.size())
				mgrAddrs.add("");
			while (mgrIds.size() < mgrs.size())
				mgrIds.add("");
			while (certExpStr.size() < mgrs.size())
				certExpStr.add("");
			while (certIds.size() < mgrs.size())
				certIds.add("");
			for (int i = 0; i < mgrs.size(); i++) {
				try {
					rslt = new ObjectObject();
					rslt.put(labelMgr, FBSUtility.wrap(mgrs.get(i)).toFBSString());
					rslt.put(labelAddr, FBSUtility.wrap(mgrAddrs.get(i)).toFBSString());
					rslt.put(labelMgrId, FBSUtility.wrap(mgrIds.get(i)).toFBSString());
					rslt.put(labelCertId, FBSUtility.wrap(certIds.get(i)).toFBSString());
					rslt.put(labelCertExp, FBSUtility.wrap(certExpStr.get(i)).toFBSString());
					rslts.add(rslt);
				} catch (InterpretException e2) {
					// ignore it
				}
			}
		} catch (Exception e1) {
			HS_Util.debug(e1.toString(), "error", dbarLoc);
			if ( HS_Util.isDebugServer() ) e1.printStackTrace();
			try {
				rslt = new ObjectObject();
				rslt.put(labelMgr, FBSUtility.wrap(e1.toString()).toFBSString());
				rslt.put(labelAddr, FBSUtility.wrap("-").toFBSString());
				rslt.put(labelCertId, FBSUtility.wrap("ERROR").toFBSString());
				rslt.put(labelCertExp, FBSUtility.wrap("-").toFBSString());
				rslt.put(labelMgrId, FBSUtility.wrap("-").toFBSString());
				rslts.add(rslt);
			} catch (InterpretException e2) {
				// ignore it
			}
		}

		return rslts;
	}

	public Vector<ObjectObject> buildManagerDisplay(Document idoc) {
		final boolean localDebug = false;
		final String dbarLoc = "inspection.buildManagerDisplay";

		try {
			String unid = this.getUnid();
			if ( unid == null ) unid = "invalid";
			if ( !unid.equals(idoc.getUniversalID()) ) {
				if ( localDebug ) HS_Util.debug("loading doc for:[" + idoc.getUniversalID() + "] for Manager display.", "debug", dbarLoc);
				HS_BusinessObjects bo = new HS_BusinessObjects();
				bo.load(this, idoc);
				if ( localDebug ) HS_Util.debug("loadinf doc AND ManagerData for:[" + idoc.getUniversalID() + "]", "debug", dbarLoc);
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}
		return this.buildManagerDisplay();
	}

	public Vector<String> buildTemperatureDisplay(String ttype) {
		final boolean localDebug = false;
		final String dbarLoc = "inspection.buildTemperatureDisplay";

		Vector<String> rslts = new Vector<String>();
		String rslt = "";
		if ( "equipment".equals(ttype) ) {
			try {
				Vector<String> edescs = this.getEquipDescrip();
				if ( localDebug ) HS_Util.debug("edescs.length:[" + edescs.size() + "] - [" + edescs + "]", "debug", dbarLoc);
				Vector<String> etemps = this.getEquipTemp();
				if ( edescs.size() == 0 ) {
					rslts.add("No equipment temperatures recorded");
				} else {
					while (etemps.size() < edescs.size()) {
						etemps.add("");
					}
					for (int i = 0; i < edescs.size(); i++) {
						// "Description: " + @Subset(@Subset(EquipDescrip; i); -
						// 1) + "      ";
						// sEquipTemp := @If(@Subset(@Subset(EquipTemp ; i); -
						// 1) = " "; "";
						// "Temperature: " + @Subset(@Subset(EquipTemp ; i); -
						// 1) + "�F");
						rslt = "Description: " + edescs.get(i) + "      ";
						rslt += "Temperature: ";
						// if (localDebug) HS_Util.debug("etemps.get(i) is a:["
						// + etemps.get(i).getClass().getName() + "]", "debug",
						// dbarLoc);
						rslt += etemps.get(i).toString();
						rslt += "�F";

						if ( localDebug ) HS_Util.debug("Adding:[" + rslt + "] at:[" + rslts.size() + "]", "debug", dbarLoc);
						rslts.add(rslt);

					}
				}
			} catch (Exception e) {
				HS_Util.debug(e.toString(), "error", dbarLoc + ".equipment");
				if ( HS_Util.isDebugServer() ) e.printStackTrace();
				rslts.add(e.toString());
			}
		} else if ( "food".equals(ttype) ) {
			// rslts.add("Food temps WIP");
			try {
				Vector<String> fdescs = this.getFoodDescrip();
				Vector<String> ftemps = this.getFoodTemp();
				Vector<String> fstates = this.getFoodState();

				if ( fdescs.size() == 0 ) {
					rslts.add("No food temperatures recorded");
				} else {
					while (ftemps.size() < fdescs.size()) {
						ftemps.add("");
					}
					while (fstates.size() < fdescs.size()) {
						fstates.add("");
					}

					for (int i = 0; i < fdescs.size(); i++) {
						rslt = "Description: " + fdescs.get(i) + "      ";
						rslt += ("".equals(ftemps.get(i))) ? "" : "Temperature: " + ftemps.get(i) + "�F   ";
						rslt += ("".equals(fstates.get(i))) ? "" : "State of food: " + fstates.get(i);
						rslts.add(rslt);
					}
				}
			} catch (Exception e) {
				HS_Util.debug(e.toString(), "error", dbarLoc + ".food");
				if ( HS_Util.isDebugServer() ) e.printStackTrace();
				rslts.add(e.toString());
			}
		} else if ( "washing".equals(ttype) ) {
			rslts.add("Ware Washing Equipment temps WIP");
		}
		return rslts;
	}

	public Vector<ObjectObject> buildTemperatureDisplayV2(Document idoc, String ttype) {
		final boolean localDebug = false;
		final String dbarLoc = "inspection.buildTemperatureDisplayV2";

		Vector<ObjectObject> results = null;
		try {
			String unid = this.getUnid();
			if ( unid == null ) unid = "invalid";
			if ( !unid.equals(idoc.getUniversalID()) ) {
				if ( localDebug ) HS_Util.debug("loading doc for:[" + idoc.getUniversalID() + "] for temperature display for [" + ttype + "]", "debug", dbarLoc);
				HS_BusinessObjects bo = new HS_BusinessObjects();
				bo.load(this, idoc);
				if ( localDebug ) HS_Util.debug("loaded doc AND TemperatureData for:[" + idoc.getUniversalID() + "]", "debug", dbarLoc);
			}
			results = this.buildTemperatureDisplayV2(ttype);
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			results = new Vector<ObjectObject>();
		}

		return results;
	}

	public Vector<ObjectObject> buildTemperatureDisplayV2(String ttype) {
		final boolean localDebug = false;
		final String dbarLoc = "inspection.buildTemperatureDisplayV2";

		final String labelDesc = "description";
		final String labelTemp = "temperature";
		final String labelState = "state";
		final String labelMachineName = "machinename";
		final String labelSaniMethod = "sanimethod";
		final String labelThermoLabel = "thermolabel";
		final String labelPpm = "ppm";
		final String labelSaniName = "saniname";
		final String labelSaniType = "sanitype";
		final String labelDel = "allowDelete";

		Vector<ObjectObject> rslts = new Vector<ObjectObject>();
		ObjectObject rslt = new ObjectObject();

		if ( "equipment".equals(ttype) ) {
			try {
				Vector<String> edescs = this.getEquipDescrip();
				if ( edescs == null || edescs.size() == 0 ) {
					rslt.put(labelDesc, FBSUtility.wrap("No equipment temperatures recorded").toFBSString());
					rslt.put(labelTemp, FBSUtility.wrap("").toFBSString());
					rslt.put(labelDel, FBSUtility.wrap(false).toFBSBoolean());
					rslts.add(rslt);
				} else {
					if ( localDebug ) HS_Util.debug("edescs.length:[" + edescs.size() + "] - [" + edescs + "]", "debug", dbarLoc);
					Vector<String> etemps = this.getEquipTemp();

					while (etemps.size() < edescs.size()) {
						etemps.add("");
					}
					for (int i = 0; i < edescs.size(); i++) {
						// "Description: " + @Subset(@Subset(EquipDescrip; i); -
						// 1) + "      ";
						// sEquipTemp := @If(@Subset(@Subset(EquipTemp ; i); -
						// 1) = " "; "";
						// "Temperature: " + @Subset(@Subset(EquipTemp ; i); -
						// 1) + "�F");
						rslt = new ObjectObject();
						rslt.put(labelDesc, FBSUtility.wrap(edescs.get(i)).toFBSString());
						rslt.put(labelTemp, FBSUtility.wrap(etemps.get(i).toString()).toFBSString());
						rslt.put(labelDel, FBSUtility.wrap(true).toFBSBoolean());

						rslts.add(rslt);

					}
				}
			} catch (Exception e) {
				HS_Util.debug(e.toString(), "error", dbarLoc + ".equipment");
				if ( HS_Util.isDebugServer() ) e.printStackTrace();
				try {
					rslt.put(labelDesc, FBSUtility.wrap("ERROR").toFBSString());
					rslt.put(labelTemp, FBSUtility.wrap(e.toString()).toFBSString());
					rslt.put(labelDel, FBSUtility.wrap(false).toFBSBoolean());
				} catch (InterpretException e1) {
					// ignore it.
				}
				rslts.add(rslt);
			}
		} else if ( "food".equals(ttype) ) {
			// rslts.add("Food temps WIP");
			try {
				Vector<String> fdescs = this.getFoodDescrip();
				if ( fdescs == null || fdescs.size() == 0 ) {
					rslt.put(labelDesc, FBSUtility.wrap("No food temperatures recorded").toFBSString());
					rslt.put(labelTemp, FBSUtility.wrap("-").toFBSString());
					rslt.put(labelState, FBSUtility.wrap("-").toFBSString());
					rslt.put(labelDel, FBSUtility.wrap(false).toFBSBoolean());

					if ( localDebug ) HS_Util.debug("Adding:[" + rslt.get(labelDesc).stringValue() + "] at:[" + rslts.size() + "]", "debug", dbarLoc);
					rslts.add(rslt);
				} else {
					if ( localDebug ) HS_Util.debug("fdescs.length:[" + fdescs.size() + "] - [" + fdescs + "]", "debug", dbarLoc);
					Vector<String> ftemps = this.getFoodTemp();
					Vector<String> fstates = this.getFoodState();

					while (ftemps.size() < fdescs.size()) {
						ftemps.add("");
					}
					while (fstates.size() < fdescs.size()) {
						fstates.add("");
					}

					for (int i = 0; i < fdescs.size(); i++) {
						rslt = new ObjectObject();
						rslt.put(labelDesc, FBSUtility.wrap(fdescs.get(i)).toFBSString());
						rslt.put(labelTemp, FBSUtility.wrap(ftemps.get(i)).toFBSString());
						rslt.put(labelState, FBSUtility.wrap(fstates.get(i)).toFBSString());
						rslt.put(labelDel, FBSUtility.wrap(true).toFBSBoolean());
						if ( localDebug ) HS_Util.debug("Adding:[" + rslt.get(labelDesc).stringValue() + "] at:[" + rslts.size() + "]", "debug", dbarLoc);
						rslts.add(rslt);
					}
				}
			} catch (Exception e) {
				HS_Util.debug(e.toString(), "error", dbarLoc + ".food");
				if ( HS_Util.isDebugServer() ) e.printStackTrace();
				rslt = new ObjectObject();
				try {
					rslt.put(labelDesc, FBSUtility.wrap(e.toString()).toFBSString());
					rslt.put(labelTemp, FBSUtility.wrap("-").toFBSString());
					rslt.put(labelState, FBSUtility.wrap("ERROR").toFBSString());
					rslt.put(labelDel, FBSUtility.wrap(false).toFBSBoolean());
				} catch (InterpretException e1) {
					// ignore it.
				}
				rslts.add(rslt);
			}
		} else if ( "washing".equals(ttype) ) {
			try {
				Vector<String> mnames = this.getMachineName();
				if ( mnames == null || mnames.size() == 0 ) {
					rslt = new ObjectObject();
					rslt.put(labelMachineName, FBSUtility.wrap("No Ware Washing temperatures").toFBSString());
					rslt.put(labelSaniMethod, FBSUtility.wrap("-").toFBSString());
					rslt.put(labelThermoLabel, FBSUtility.wrap("-").toFBSString());
					rslt.put(labelPpm, FBSUtility.wrap("-").toFBSString());
					rslt.put(labelSaniName, FBSUtility.wrap("-").toFBSString());
					rslt.put(labelSaniType, FBSUtility.wrap("-").toFBSString());
					rslt.put(labelTemp, FBSUtility.wrap("-").toFBSString());
					rslt.put(labelDel, FBSUtility.wrap(false).toFBSBoolean());
					rslts.add(rslt);
				} else {
					if ( localDebug ) HS_Util.debug("manmes.length:[" + mnames.size() + "] - [" + mnames + "]", "debug", dbarLoc);
					Vector<String> smethods = this.getSaniMethod();
					Vector<String> tlabels = this.getThermoLabel();
					Vector<String> ppms = this.getPpm();
					Vector<String> snames = this.getSaniName();
					Vector<String> stypes = this.getSaniType();
					Vector<String> wwtemps = this.getWareWashTemp();

					while (smethods.size() < mnames.size())
						smethods.add("");
					while (tlabels.size() < mnames.size())
						tlabels.add("");
					while (ppms.size() < mnames.size())
						ppms.add("");
					while (snames.size() < mnames.size())
						snames.add("");
					while (stypes.size() < mnames.size())
						stypes.add("");
					while (wwtemps.size() < mnames.size())
						wwtemps.add("");

					for (int i = 0; i < mnames.size(); i++) {
						rslt = new ObjectObject();
						rslt.put(labelMachineName, FBSUtility.wrap(mnames.get(i)).toFBSString());
						rslt.put(labelSaniMethod, FBSUtility.wrap(smethods.get(i)).toFBSString());
						rslt.put(labelThermoLabel, FBSUtility.wrap(tlabels.get(i)).toFBSString());
						rslt.put(labelPpm, FBSUtility.wrap(ppms.get(i)).toFBSString());
						rslt.put(labelSaniName, FBSUtility.wrap(snames.get(i)).toFBSString());
						rslt.put(labelSaniType, FBSUtility.wrap(stypes.get(i)).toFBSString());
						rslt.put(labelTemp, FBSUtility.wrap(wwtemps.get(i)).toFBSString());
						rslt.put(labelDel, FBSUtility.wrap(true).toFBSBoolean());
						rslts.add(rslt);
					}
				}
			} catch (InterpretException e1) {
				HS_Util.debug(e1.toString(), "error", dbarLoc + ".washing");
				if ( HS_Util.isDebugServer() ) e1.printStackTrace();
				try {
					rslt = new ObjectObject();
					rslt.put(labelMachineName, FBSUtility.wrap(e1.toString()).toFBSString());
					rslt.put(labelSaniMethod, FBSUtility.wrap("ERROR").toFBSString());
					rslt.put(labelThermoLabel, FBSUtility.wrap("-").toFBSString());
					rslt.put(labelPpm, FBSUtility.wrap("-").toFBSString());
					rslt.put(labelSaniName, FBSUtility.wrap("-").toFBSString());
					rslt.put(labelSaniType, FBSUtility.wrap("-").toFBSString());
					rslt.put(labelTemp, FBSUtility.wrap("-").toFBSString());
					rslt.put(labelDel, FBSUtility.wrap(false).toFBSBoolean());
					rslts.add(rslt);
				} catch (InterpretException e2) {
					// ignore it
				}
			}
		}
		return rslts;
	}

	public void clearInspection() {
		final String dbarLoc = "InspectionBean.clearInspection";
		final boolean localDebug = false;

		if ( localDebug ) HS_Util.debug("Starting", "debug", dbarLoc);
		this.form = null;
		this.unid = null;
		this.documentId = null;
		this.parentUnid = null;
		this.parentForm = null;
		this.billingType = null;
		this.facilityName = null;
		this.facilityPhysicalBuilding = null;
		this.facilityPhysicalCity = null;
		this.facilityPhysicalDirection = null;
		this.facilityPhysicalPostalCode = null;
		this.facilityPhysicalProvince = null;
		this.facilityPhysicalRegType = null;
		this.facilityPhysicalStreetName = null;
		this.facilityPhysicalStreetSuffix = null;
		this.facilityPhysicalStreetType = null;
		this.facilityPhysicalSuite = null;
		this.facilityTelephone = null;
		this.facilityType = null;
		this.facilityApprovedNumber = "";
		this.facilityRegType = null;
		this.facilityID2 = "";

		this.foodSafe = null;
		this.allYearRound = null;
		this.createdFromInspID = null;
		this.eho = null;
		this.ehsId = null;
		this.enforcement = null;
		this.eventEndDate = null;
		this.eventId = null;
		this.eventName = null;
		this.eventStartDate = null;
		this.riskRating = null;
		this.nextInspDue = null;
		this.nextManualDue = null;
		this.ownerId = null;
		this.ownersAddress = null;
		this.ownersCity = null;
		this.ownersName = null;
		this.ownersPostalCode = null;
		this.ownersState = null;
		this.physicalMunicipality = null;
		// Temperature stuff
		this.equipDescrip = null;
		this.equipTemp = null;
		this.foodDescrip = null;
		this.foodState = null;
		this.foodTemp = null;
		this.machineName = null;
		this.saniMethod = null;
		this.saniName = null;
		this.saniType = null;
		this.thermoLabel = null;
		this.ppm = null;
		this.wareWashTemp = null;

		this.inspType = null;
		this.inspectionDate = null;
		this.inspectionDateStr = null;
		this.followupInspectionRequired = null;
		this.nextInspectionDate = null;
		this.nextInspectionDateStr = "";
		this.comments = "";
		this.violationModule = null;
		this.violations = null;
		this.letterGrade = null;
		this.numCore = 0;
		this.numCritical = 0;
		this.numKey = 0;
		this.numNonCritical = 0;
		this.numOther = 0;
		this.numPFoundation = 0;
		this.numPriority = 0;
		this.numRepeat = 0;
		this.numRisk = 0;
		this.totalViolations = 0;
		this.totHazardRating = 0;
		this.computedCriticals = 0;

		this.timeTrackingRecords = new Vector<TimeTracking>();
		// create a dummy one here to allow the reuse of the objects
		this.timeTracking = new TimeTracking();
		this.timeTracking.loadDefaults(this);

		this.signatureData1 = "";
		this.signatureData2 = "";

		this.signatureHistory = null;
		this.signatureData64_1 = "";
		this.signatureData64_2 = "";
		this.source = HS_Util.getSession().getCommonUserName() + " (via HSTouch)";
		this.dateCreated = null;
		this.receivedBy = "";
		this.receivedByTitle = "";
		this.displayFollowUp = "Yes"; // New inspections get the yes here

		this.certified = null;
		this.certifiedManager = null;
		this.certExpiration = null;
		this.certNumber = null;

		this.manager = null;
		this.managerAddress = null;
		this.managerCertificateExpirationStr = null;
		this.managerCertificateId = null;
		this.managerId = null;
		this.managerAllergenTraining = null;

		this.violations = null;
		this.correctedViolations = null;
		this.previousViolations = null;

		if ( localDebug ) HS_Util.debug("Done with items", "debug", dbarLoc);
	}

	public void clearViolation() {
		final String dbarLoc = "InspectionBean.clearViolation";
		final boolean localDebug = false;

		Vector<String> pl = this.getPlGroups();
		this.setWrkGrp(pl.get(0));

		pl = this.getPlCategories();
		this.setWrkCat(pl.get(0));

		pl = this.getPlSections();
		this.setWrkSection(pl.get(0));

		this.setWrkInOut("");
		this.setWrkStatus("");

		if ( localDebug ) HS_Util.debug("Running with getPlViolations", "debug", dbarLoc);
		pl = this.getPlViolationList();
		this.setWrkViolation(pl.get(0));

		this.setWrkCritical("");
		this.setWrkCorrectiveAction("");
		this.setWrkCorrectedSet("");
		this.setWrkCorrectByDate(null);
		this.setWrkDescription("");
		this.setWrkHzrdRating("");
		this.setWrkLegalText("");
		this.setWrkMessages(new Vector<String>());
		this.setWrkObservation("");
		this.setWrkRepeat("");

		if ( localDebug ) HS_Util.debug("done", "debug", dbarLoc);
	}

	public int countTimeTrackingRecords() {
		int result = 0;
		for (TimeTracking ttr : this.getTimeTrackingRecords()) {
			if ( !ttr.isDeleteMeFlag() ) result += 1;
		}
		return result;
	}

	private String fixYesOrNo(String inp) {
		if ( "yes".equals(inp) ) return "Yes";
		if ( "no".equals(inp) ) return "No";
		return inp;
	}

	// @SuppressWarnings("deprecation")
	private String formatDate(Date jdt) {
		// return HS_Util.formatDate(jdt, "dd-MMM-yyyy");
		return HS_Util.formatDate(jdt, "d-MMM-yyyy");
	}

	public String formattedInspectionDate() {
		if ( this.inspectionDate == null ) return "";
		return HS_Util.formatDate(this.getInspectionDate(), "dd-MMM-yyyy");
	}

	public String getAllYearRound() {
		if ( allYearRound == null ) allYearRound = "";
		return allYearRound;
	}

	public String getAnnounceInsp() {
		if ( announceInsp == null ) announceInsp = "No"; // This is the default
		return announceInsp;
	}

	public String getBillable() {
		if ( billable == null ) {
			/*
			 * Disabled := @IsMember( "Billing"; @GetProfileField( "MasterSettings"; "Disabled" ) ); OtherTV := (SourceDB != "" & SourceDB != @GetProfileField( "MasterSettings"; "ReplicaID" )); FacType := @If(
			 * 
			 * @Right( Form; 6 ) = "Report"; @Left( Form; "Report" ); Form ); FacType := @If(FacType = "Camp"; "Campground"; FacType); InspFeeTypes := @GetProfileField( "BillingSettings";
			 * "BillableInspectionTypes" ); REM {I dont know how to get a grand parent in formula};
			 * 
			 * @If( @IsNotMember( FacType; InspFeeTypes ) | OtherTV | Disabled; "No"; "Yes" )
			 */
			boolean disabled = HS_Util.getMasterSettingsVector("Disabled").contains("Billing");
			// String sourceDb = "";
			boolean otherTV = false;
			String facType = this.getForm();
			if ( facType.endsWith("Report") ) facType = facType.substring(0, facType.length() - 6);
			if ( "Camp".equals(facType) ) facType = "Campground";
			Vector<String> inspFeeTypes = HS_Util.getProfileVector("BillingSettings", "BillableInspectionTypes");
			billable = (!(inspFeeTypes.contains(facType)) || otherTV || disabled) ? "No" : "Yes";
		}
		return billable;
	}

	public String getBillablePermit() {
		if ( billablePermit == null ) {
			try {
				// @If(@IsError(@GetDocField( $ref; "Billable" ));
				// "";@GetDocField( $ref; "Billable" ))
				billablePermit = this.getParentDoc(this.getParentUnid()).getItemValueString("Billable");
			} catch (Exception e) {
				billablePermit = "";
			}
		}
		return billablePermit;
	}

	public String getBillingType() {
		return billingType;
	}

	public String getCertExpiration() {
		return certExpiration;
	}

	public String getCertified() {
		return certified;
	}

	public String getCertifiedManager() {
		return certifiedManager;
	}

	public String getCertNumber() {
		return certNumber;
	}

	public String getChildRatio() {
		if ( this.childRatio == null ) {
			// Only applies if this is a ChildcareReport - Form !=
			// "ChildcareReport" - then is an input item...
			this.childRatio = "";
		}
		return childRatio;
	}

	public String getChokingPoster() {
		return chokingPoster;
	}

	public UIComponent getCommentInput() {
		return commentInput;
	}

	public String getComments() {
		if ( comments == null ) comments = "";
		return comments;
	}

	public Date getComplaintDate() {
		return complaintDate;
	}

	public String getComplianceSet() {
		if ( complianceSet == null ) {
			/*
			 * Only applies to Arizona - Otherwise it is blank... Only applies if the Inspection Type is "Compliance" Computed in the Inut Translation Formula of the Type field on the inspectionInfo subform
			 * 
			 * @If(
			 * 
			 * @GetProfileField( "MasterSettings"; "DBDesign" ) = "Arizona";
			 * 
			 * @If(
			 * 
			 * @IsMember( Type; "Compliance" );
			 * 
			 * @If(ComplianceSet != "Yes";
			 * 
			 * @SetField( "FollowupInspectionRequired"; "Yes" ) & @SetField( "Type"; "Compliance" ) & @SetField( "ComplianceSet"; "Yes" );
			 * 
			 * @SetField( "Type"; "Compliance" )); Type & @SetField( "ComplianceSet"; "No" )); Type)
			 */
			complianceSet = "";
			if ( "Arizona".equals(this.getDbDesign()) ) {
				complianceSet = ("Compliance".equals(this.getInspType())) ? "Yes" : "No";
			}
		}
		return complianceSet;
	}

	public int getComputedCriticals() {
		/*
		 * FORMULA CODE IS:
		 * 
		 * @If((@Elements(ViolSection) = @Elements(ViolCriticalSet)) & HowToCountCriticals = "Section";
		 * 
		 * @For(n := 1; n <= @Elements(ViolSection); n := n + 1;
		 * 
		 * @If(@Contains(ViolCriticalSet[n]; "Critical");
		 * 
		 * @If(ViolCriticals = ""; ViolCriticals := ViolSection[n]; ViolCriticals := ViolCriticals:ViolSection[n]); "")); "");
		 * 
		 * @If((@Elements(ViolCode) = @Elements(ViolCriticalSet)) & (HowToCountCriticals = "Violation" | HowToCountCriticals = "Observation");
		 * 
		 * @For(n := 1; n <= @Elements(ViolCode); n := n + 1;
		 * 
		 * @If(@Contains(ViolCriticalSet[n]; "Critical");
		 * 
		 * @If(ViolCriticals = ""; ViolCriticals := ViolCode[n]; ViolCriticals := ViolCriticals:ViolCode[n]); "")); ""); total :=
		 * 
		 * @If(HowToCountCriticals = "Section";
		 * 
		 * @Elements(@Unique(@Text(ViolCriticals))); HowToCountCriticals = "Violation";
		 * 
		 * @Elements(@Unique(ViolCriticals)); HowToCountCriticals = "Observation";
		 * 
		 * @Elements(ViolCriticals); NumCritical);
		 * 
		 * @If(@IsError(total); total := NumCritical; 0);
		 * 
		 * total
		 */
		Vector<String> violCriticals = new Vector<String>();
		String wsect = "";
		String wCritSet = "";
		if ( "Section".equals(this.getHtcc()) ) {
			for (int n = 0; n < this.violations.size(); n++) {
				wsect = violations.get(n).getSection();
				wCritSet = violations.get(n).getCriticalSet();
				if ( wCritSet.contains("Critical") && !violCriticals.contains(wsect) ) {
					violCriticals.add(wsect);
				}
			}
		} else if ( "Violation".equals(this.getHtcc()) || "Observation".equals((this.getHtcc())) ) {
			for (int n = 0; n < this.violations.size(); n++) {
				wsect = violations.get(n).getCode();
				wCritSet = violations.get(n).getCriticalSet();
				if ( wCritSet.contains("Critical") && (!violCriticals.contains(wsect) || "Observation".equals((this.getHtcc()))) ) {
					violCriticals.add(wsect);
				}
			}
		}
		computedCriticals = violCriticals.size();
		return computedCriticals;
	}

	public Vector<Violation> getCorrectedViolations() {
		if ( correctedViolations == null ) correctedViolations = new Vector<Violation>();
		return correctedViolations;
	}

	public String getCreatedFromInspID() {
		if ( createdFromInspID == null ) createdFromInspID = "";
		return createdFromInspID;
	}

	public String getCriticalViolationRanges() {
		if ( criticalViolationRanges == null ) {
			// REM {Need to make sure inspections created before this value is
			// changed are not affected};
			// @Implode(@GetProfileField("GlobalSettings"; "GradeRanges"); ";");
			Vector<String> tmpCriticalViolationRanges = HS_Util.getGlobalSettingsVector("CriticalViolationRanges");
			criticalViolationRanges = StringUtils.join(tmpCriticalViolationRanges, ";");
		}
		return criticalViolationRanges;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public String getDbDesign() {
		if ( dbDesign == null ) dbDesign = "";
		if ( "".equals(dbDesign) ) {
			dbDesign = HS_Util.getMasterSettingsString("DBDesign");
		}
		return dbDesign;
	}

	public String getDgbonc() {
		if ( dgbonc == null ) {
			// REM {Need to make sure inspections created before this value is
			// changed are not affected};
			// @If(@GetProfileField("GlobalSettings";
			// "DropGradeBasedOnNonCorrected") = "Yes"; "Yes"; "No");
			dgbonc = HS_Util.getGlobalSettingsString("DropGradeBasedOnNonCorrected");
			dgbonc = ("".equals(dgbonc)) ? "No" : dgbonc;
		}
		return dgbonc;
	}

	public String getDisplayFollowUp() {
		// Set to "Yes" unless there is another inspection doc already created
		// from it. On NEW docs it will alwyas be "Yes"
		if ( displayFollowUp == null ) displayFollowUp = "Yes";
		return displayFollowUp;
	}

	public String getDocumentId() {
		if ( documentId == null ) documentId = "";
		if ( "".equals(documentId) ) {
			documentId = HS_Util.getSession().evaluate("@Unique").get(0).toString();
		}
		return documentId;
	}

	public String getDtmuhrsc() {
		if ( dtmuhrsc == null ) {
			// TODO - Need to replicate this formula logic. - These sections are
			// using TDA Values as o 23 Aug 2016
			// DTMUHRSC = 'Does This Module Use Hazard Rating Score Calculation'
			// HRFacTypes:= @Replace(@GetProfileField("GlobalSettings";
			// "HazardRatingFacilityTypes");"Food Report 2009";"FoodReport");
			// @Contains(HRFacTypes; Form); "Yes"; "No")
			dtmuhrsc = "Yes";
		}
		return dtmuhrsc;
	}

	public String getDtmulg() {
		if ( dtmulg == null ) {
			// TODO - Need to replicate this formula logic.
			// REM {DTMULG = 'Does This Module Use Letter Grades' };
			// REM {Need to make sure inspections created before this value is
			// changed are not affected};
			// LGFacTypes:= @Replace(@GetProfileField("GlobalSettings";
			// "LetterGradedFacilityTypes");"Food Report 2009";"FoodReport");
			// @If(@Contains(LGFacTypes; Form); "Yes"; "No");
			dtmulg = "No";
		}
		return dtmulg;
	}

	public String getEho() {
		if ( eho == null ) eho = "";
		return eho;
	}

	public ObjectObject getEhoData() {
		if ( ehoData == null ) {
			if ( !("".equals(getEho())) ) {
				ehoData = loadEhoData(getEho());
			}
		}
		return ehoData;
	}

	public String getEhsId() {
		if ( ehsId == null ) {
			Vector<Object> tmp = HS_Util.getSession().evaluate("@ReplicaId");
			ehsId = (String) tmp.get(0);
		}
		return ehsId;
	}

	public Vector<Object> getEhsReaders() {
		return ehsReaders;
	}

	public String getEmailSent() {
		return emailSent;
	}

	public String getEnforcement() {
		if ( enforcement == null ) enforcement = "";
		return enforcement;
	}

	public String getEnvironment() {
		if ( environment == null ) environment = "Mobile";
		return environment;
	}

	public Vector<String> getEquipDescrip() {
		// if (equipDescrip == null) equipDescrip = new Vector<String>();
		return equipDescrip;
	}

	public Vector<String> getEquipTemp() {
		// if (equipTemp == null) equipTemp = new Vector<String>();
		return equipTemp;
	}

	public Date getEventEndDate() {
		return eventEndDate;
	}

	public String getEventId() {
		return eventId;
	}

	public String getEventName() {
		return eventName;
	}

	public Date getEventStartDate() {
		return eventStartDate;
	}

	public String getFacilityApprovedNumber() {
		return facilityApprovedNumber;
	}

	public String getFacilityID2() {
		return facilityID2;
	}

	public String getFacilityName() {
		// if (facilityName == null) {
		// if (this.parentUnid != null)
		// loadDefaultFacilityInfo(getParentUnid());
		// }
		return facilityName;
	}

	public String getFacilityPhysicalBuilding() {
		return facilityPhysicalBuilding;
	}

	public String getFacilityPhysicalCity() {
		return facilityPhysicalCity;
	}

	public String getFacilityPhysicalDirection() {
		return facilityPhysicalDirection;
	}

	public String getFacilityPhysicalPostalCode() {
		return facilityPhysicalPostalCode;
	}

	public String getFacilityPhysicalProvince() {
		return facilityPhysicalProvince;
	}

	public String getFacilityPhysicalRegType() {
		return facilityPhysicalRegType;
	}

	public String getFacilityPhysicalStreetName() {
		return facilityPhysicalStreetName;
	}

	public String getFacilityPhysicalStreetSuffix() {
		return facilityPhysicalStreetSuffix;
	}

	public String getFacilityPhysicalStreetType() {
		return facilityPhysicalStreetType;
	}

	public String getFacilityPhysicalSuite() {
		return facilityPhysicalSuite;
	}

	public String getFacilityRegType() {
		return facilityRegType;
	}

	public String getFacilityTelephone() {
		return facilityTelephone;
	}

	public String getFacilityType() {
		if ( facilityType == null ) facilityType = "";
		return facilityType;
	}

	public String getFollowupInspectionRequired() {
		if ( followupInspectionRequired == null ) followupInspectionRequired = "No";
		return followupInspectionRequired;
	}

	public Vector<String> getFollowupTypes() {
		// if ( followupTypes == null ) {
		followupTypes = HS_Util.getMasterSettingsVector("InspectionFollowupTypes");
		// }
		return followupTypes;
	}

	public Vector<String> getFoodDescrip() {
		// if (foodDescrip == null) foodDescrip = new Vector<String>();
		return foodDescrip;
	}

	public String getFoodSafe() {
		return foodSafe;
	}

	public Vector<String> getFoodState() {
		// if (foodState == null) foodState = new Vector<String>();
		return foodState;
	}

	public Vector<String> getFoodTemp() {
		// if (foodTemp == null) foodTemp = new Vector<String>();
		return foodTemp;
	}

	public String getForm() {
		// final boolean localDebug = HS_Util.isDebugger();
		final boolean localDebug = false;
		final String dbarLoc = "inspectionBean.getForm";
		if ( form == null ) form = "";
		if ( "".equals(form) ) {
			String pform = this.getParentForm();
			if ( pform != null ) {
				form = ("".equals(pform)) ? "FoodReport" : form;
				form = ("CampFacility".equalsIgnoreCase(pform)) ? "CampReport" : form;
				form = ("FoodFacility".equalsIgnoreCase(pform)) ? "FoodReport" : form;
				form = ("PoolFacility".equalsIgnoreCase(pform)) ? "PoolReport" : form;
				form = ("WaterFacility".equalsIgnoreCase(pform)) ? "WaterReport" : form;
				form = ("BodyartFacility".equalsIgnoreCase(pform)) ? "BodyartReport" : form;
				form = ("".equals(form)) ? "FoodReport" : form;
				if ( localDebug ) HS_Util.debug("for pform:[" + pform + "] form computed as:[" + form + "]", "debug", dbarLoc);
			}
		}
		return form;
	}

	public Map<String, Object> getFullCloudMap() {
		return fullCloudMap;
	}

	public String getGboc() {
		if ( gboc == null ) {
			// REM {Need to make sure inspections created before this value is
			// changed are not affected};
			// @If(@GetProfileField("GlobalSettings"; "GradeBasedOnCriticals") =
			// "Yes"; "Yes"; "No")
			gboc = HS_Util.getGlobalSettingsString("GradeBasedOnCriticals");
		}
		return gboc;
	}

	public String getGradeComputedByHazRating() {
		/*
		 * @If(UseHazard != "Yes" | DTMUHRSC != "Yes" | !@IsDocBeingSaved;
		 * 
		 * @Return(GradeComputedByHazRating); "");
		 * 
		 * LetterGradesFirstElement := @If(@Contains(LetterGrades; ";");
		 * 
		 * @Left(LetterGrades; ";"); LetterGrades); LetterGradesRestOfElements :=
		 * 
		 * @Right(LetterGrades; ";");
		 * 
		 * GradeRangesFirstElement := @If(@Contains(GradeRanges; ";");
		 * 
		 * @Left(GradeRanges; ";"); GradeRanges); GradeRangesRestOfElements :=
		 * 
		 * @Right(GradeRanges; ";");
		 * 
		 * FinalLetterGrade := "";
		 * 
		 * @While(LetterGradesFirstElement != "" & FinalLetterGrade = ""; LowGrade := @TextToNumber(@Left(GradeRangesFirstElement; "-")); HighGrade :=
		 * 
		 * @TextToNumber(@Right(GradeRangesFirstElement; "-"));
		 * 
		 * @If(LowGrade <= TotHazardRating & TotHazardRating <= HighGrade; FinalLetterGrade := LetterGradesFirstElement; ""); LetterGradesFirstElement :=
		 * 
		 * @If(@Contains(LetterGradesRestOfElements; ";");
		 * 
		 * @Left(LetterGradesRestOfElements; ";"); LetterGradesRestOfElements); LetterGradesRestOfElements :=
		 * 
		 * @Right(LetterGradesRestOfElements; ";"); GradeRangesFirstElement :=
		 * 
		 * @If(@Contains(GradeRangesRestOfElements; ";");
		 * 
		 * @Left(GradeRangesRestOfElements; ";"); GradeRangesRestOfElements); GradeRangesRestOfElements := @Right(GradeRangesRestOfElements; ";") );
		 * 
		 * FinalLetterGrade
		 */
		// TODO Need to test this with real data. TDA has nothing in these
		// items.
		if ( !"Yes".equals(this.getUseHazard()) && !"Yes".equals(this.getDtmuhrsc()) ) {
			String[] lgs = this.getLetterGrades().split(";");
			String[] grs = this.getGradeRanges().split(";");
			int highGrade;
			int lowGrade;
			int thr = this.getTotHazardRating();
			gradeComputedByHazRating = "";
			for (int i = 0; i < lgs.length; i++) {
				String[] grades = grs[i].split("-");
				lowGrade = Integer.parseInt(grades[0]);
				highGrade = Integer.parseInt(grades[1]);
				if ( lowGrade <= thr && thr <= highGrade ) gradeComputedByHazRating = lgs[i];
			}
		}
		return gradeComputedByHazRating;
	}

	public String getGradeComputedByNumCriticals() {
		return gradeComputedByNumCriticals;
	}

	public String getGradeRanges() {
		if ( gradeRanges == null ) {
			// REM {Need to make sure inspections created before this value is
			// changed are not affected};
			// @Implode(@GetProfileField("GlobalSettings"; "GradeRanges"); ";");
			Vector<String> tmpGradeRanges = HS_Util.getGlobalSettingsVector("GradeRanges");
			gradeRanges = StringUtils.join(tmpGradeRanges, ";");
		}
		return gradeRanges;
	}

	public String getHasSig1() {
		hasSig1 = (!"".equals(this.getSignatureData64_1())) ? "Yes" : "";
		return hasSig1;
	}

	public String getHasSig2() {
		hasSig2 = (!"".equals(this.getSignatureData64_2())) ? "Yes" : "";
		return hasSig2;
	}

	public String getHazRatingType() {
		if ( hazRatingType == null ) hazRatingType = "";
		if ( "".equals(hazRatingType) ) {

			// HRMod = MasterSettingsDoc.GetItemValue("HazardRatingModules")
			// HRType = MasterSettingsDoc.GetItemValue("HazardRatingType")
			// HRStartingScore = MasterSettingsDoc.GetItemValue("StartingScore")
			// For m = 0 To Ubound(HRMod)
			// If HRMod(m) = Doc.Form(0) Then
			// Select Case HRType(m)
			// Case "Section", "By Section"
			// Call Doc.ReplaceItemValue("HazRatingType","Section")
			// Case "SectionCategory", "By Section Category"
			// Call Doc.ReplaceItemValue("HazRatingType","SectionCategory")
			// Case "Code", "By Code"
			// Call Doc.ReplaceItemValue("HazRatingType","Code")
			// Case "Observation", "By Observation"
			// Call Doc.ReplaceItemValue("HazRatingType","Observation")
			// End Select
			// Call Doc.ReplaceItemValue("TotHazardRating",HRStartingScore(m))
			// Exit For
			// End If
			// Next

			Vector<String> hrMod = HS_Util.getMasterSettingsVector("HazardRatingModules");
			Vector<String> hrType = HS_Util.getMasterSettingsVector("HazardRatingType");
			// Vector <String >hrStartingScore =
			// HS_Util.getMasterSettingsVector("StartingScore");
			for (int m = 0; m < hrMod.size(); m++) {
				// if ( idoc.getItemValueString("Form").equals(hrMod.get(m)) ) {
				if ( form.equals(hrMod.get(m)) ) {
					if ( "Section".equals(hrType.get(m)) || "By Section".equals(hrType.get(m)) ) {
						hazRatingType = "Section";
					} else if ( "SectionCategory".equals(hrType.get(m)) || "By Section Category".equals(hrType.get(m)) ) {
						hazRatingType = "SectionCategory";
					} else if ( "Code".equals(hrType.get(m)) || "By Code".equals(hrType.get(m)) ) {
						hazRatingType = "Code";
					} else if ( "Observation".equals(hrType.get(m)) || "By Observation".equals(hrType.get(m)) ) {
						hazRatingType = "Observation";
					}
				}
			}
		}
		return hazRatingType;
	}

	public String getHtcc() {
		if ( htcc == null ) {
			// REM {Need to make sure inspections created before this value is
			// changed are not affected};
			// @If(@Day(@Created) = @Day(@Now);
			// @GetProfileField("GlobalSettings"; "HowToCountCriticals");
			// HowToCountCriticals)
			htcc = HS_Util.getGlobalSettingsString("HowToCountCriticals");
		}
		return htcc;
	}

	public List<String> getImageUnids() {
		if ( imageUnids == null ) imageUnids = new ArrayList<String>();
		return imageUnids;
	}

	public Date getInspectionDate() {
		if ( inspectionDate == null ) inspectionDate = new Date();
		return inspectionDate;
	}

	public String getInspectionDateStr() {
		return inspectionDateStr;
	}

	public String getInspectionOutcome() {
		if ( inspectionOutcome == null ) inspectionOutcome = "";
		return inspectionOutcome;
	}

	public InspectionPicklists getInspPlObj() {
		if ( inspPlObj == null ) inspPlObj = new InspectionPicklists();
		return inspPlObj;
	}

	public String getInspType() {
		if ( inspType == null ) inspType = "";
		return inspType;
	}

	public String getLetterGrade() {
		return letterGrade;
	}

	public String getLetterGrades() {
		if ( letterGrades == null ) {
			// REM {Need to make sure inspections created before this value is
			// changed are not affected};
			// @Implode(@GetProfileField("GlobalSettings"; "LetterGrades"); ";")
			Vector<String> tmpLetterGrades = HS_Util.getGlobalSettingsVector("LetterGrades");
			letterGrades = StringUtils.join(tmpLetterGrades.toArray(), ";");
		}
		return letterGrades;
	}

	public String getLicenseDisplayed() {
		return licenseDisplayed;
	}

	public HS_auditForm getLoadedForm() {
		return loadedForm;
	}

	public String getLocked() {
		if ( locked == null ) locked = "No";
		if ( "".equals(locked) ) locked = "No";
		return locked;
	}

	public Vector<String> getMachineName() {
		// if (machineName == null) machineName = new Vector<String>();
		return machineName;
	}

	public String getMagDistrict() {
		return magDistrict;
	}

	public Vector<String> getManager() {
		return manager;
	}

	public Vector<String> getManagerAddress() {
		return managerAddress;
	}

	public Vector<String> getManagerAllergenTraining() {
		return managerAllergenTraining;
	}

	public Vector<String> getManagerCertificateExpirationStr() {
		return managerCertificateExpirationStr;
	}

	public Vector<String> getManagerCertificateId() {
		return managerCertificateId;
	}

	public Vector<String> getManagerId() {
		return managerId;
	}

	public Date getNextInspDue() {
		return nextInspDue;
	}

	public Date getNextInspectionDate() {
		return nextInspectionDate;
	}

	public String getNextInspectionDateStr() {
		return nextInspectionDateStr;
	}

	public Date getNextManualDue() {
		return nextManualDue;
	}

	public int getNumCore() {
		return numCore;
	}

	public int getNumCritical() {
		return numCritical;
	}

	public int getNumKey() {
		return numKey;
	}

	public int getNumNonCritical() {
		return numNonCritical;
	}

	public int getNumOther() {
		return numOther;
	}

	public int getNumPFoundation() {
		return numPFoundation;
	}

	public int getNumPriority() {
		return numPriority;
	}

	public int getNumRepeat() {
		return numRepeat;
	}

	public int getNumRisk() {
		return numRisk;
	}

	public Integer getObservedSeating() {
		return observedSeating;
	}

	@SuppressWarnings("deprecation")
	private Document getOwnerDoc(Database eidb, String ownerId) {
		final String dbarLoc = "InspectionBean.getOwnerDoc";
		final boolean localDebug = false;
		Document result = null;

		/*
		 * LS Version of the code:
		 * 
		 * Function HSGetOwnerByID(DB As NotesDatabase, OwnerID As String) As NotesDocument Dim LookupView As NotesView Dim OwnerDoc As NotesDocument
		 * 
		 * If DB Is Nothing Then Set HSGetOwnerByID = Nothing Else Set LookupView = DB.GetView("(DocumentIDLookup)") Set OwnerDoc = LookupView.GetDocumentByKey(OwnerID) If Not OwnerDoc Is Nothing Then If
		 * Lcase(OwnerDoc.Form(0)) <> "business" Then Set OwnerDoc = Nothing End If
		 * 
		 * Set HSGetOwnerByID = OwnerDoc End If End Function
		 */
		if ( eidb == null ) return result;
		if ( "".equals(ownerId) ) return result;

		View luView = eidb.getView("(DocumentIDLookup)");
		if ( luView == null ) {
			HS_Util.debug("View (DocumentIDLookup) not found.", "error", dbarLoc);
			return result;
		}
		result = luView.getDocumentByKey(ownerId, true);
		if ( result != null ) {
			if ( !"business".equalsIgnoreCase(result.getItemValueString("Form")) ) result = null;
		}
		if ( result != null ) {
			if ( localDebug ) HS_Util.debug("Found owner doc for id: [" + ownerId + "]", "debug", dbarLoc);
		} else {
			HS_Util.debug("Could not find owner doc for id: [" + ownerId + "]", "warn", dbarLoc);
		}
		return result;
	}

	public String getOwnerId() {
		if ( ownerId == null ) ownerId = "";
		return ownerId;
	}

	public String getOwnersAddress() {
		return ownersAddress;
	}

	public String getOwnersCity() {
		return ownersCity;
	}

	public String getOwnersName() {
		return ownersName;
	}

	public String getOwnersPostalCode() {
		return ownersPostalCode;
	}

	public String getOwnersState() {
		return ownersState;
	}

	private Document getParentDoc(String unid) {
		final String dbarLoc = "InspectionBean.getParentDoc";
		final boolean localDebug = false;

		HS_database hsdb = new HS_database();
		Database eidb = hsdb.getdb("eiRoot");
		if ( eidb == null ) {
			if ( localDebug ) HS_Util.debug("could not get access to eiRoot database", "error", dbarLoc);
			return null;
		}
		return eidb.getDocumentByUNID(unid);
	}

	public String getParentForm() {
		if ( parentForm == null || "".equals(parentForm) ) {
			String punid = this.getParentUnid();
			Document pdoc = (punid != null) ? this.getParentDoc(punid) : null;
			parentForm = (pdoc != null) ? pdoc.getFormName() : (punid != null) ? "bad punid" : null;
		}
		return parentForm;
	}

	public String getParentUnid() {
		return parentUnid;
	}

	public String getPhysicalMunicipality() {
		return physicalMunicipality;
	}

	public Vector<String> getPl_inspectionTypes() {
		final String dbarLoc = "Inspection.getPl_inspectionTypes";
		final boolean localDebug = false;

		try {
			Vector<String> sList = HS_Util.getMasterSettingsVector("InspectionTypes");
			if ( sList == null ) HS_Util.debug("InspectionTypes is null", "error", dbarLoc);

			Vector<String> sFollowUp = HS_Util.getMasterSettingsVector("InspectionFollowupTypes");
			if ( sFollowUp == null ) {
				sFollowUp = new Vector<String>();
				sFollowUp.add("No Follow Up Types");
			}
			if ( localDebug ) HS_Util.debug("InspectionTypes has [" + sList.size() + "] elements", "debug", dbarLoc);
			if ( localDebug ) HS_Util.debug("InspectionFollowupTypes has [" + sFollowUp.size() + "] elements", "debug", dbarLoc);
			Vector<String> notFU = new Vector<String>();
			for (String sItem : sList) {
				if ( !this.isMember(sItem, sFollowUp) ) {
					notFU.add(sItem);
				}
			}
			if ( localDebug ) HS_Util.debug("notFU has [" + notFU.size() + "] elements", "debug", dbarLoc);

			if ( "".equals(this.getCreatedFromInspID()) ) {
				pl_inspectionTypes = notFU;
			} else {
				pl_inspectionTypes = sFollowUp;
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			pl_inspectionTypes = new Vector<String>();
			pl_inspectionTypes.add("ERROR in " + dbarLoc);
			pl_inspectionTypes.add(e.toString());
		}
		// }
		return pl_inspectionTypes;
	}

	public Vector<String> getPlCategories() {
		final String dbarLoc = "InspectionBean.getPlCategories";
		final boolean localDebug = false;
		// final String vsGrpKey = "violationGroup";

		if ( plCategories == null ) {
			plCategories = new Vector<String>();
			plCategories.add("-select category-");
		}
		String catname = "";
		String grpname = "";

		String wgrp = getWrkGrp();
		if ( wgrp == null ) {
			Vector<String> wgpl = getPlGroups();
			wgrp = wgpl.get(0);
		}
		if ( localDebug ) HS_Util.debug("wrkGrp is:[" + wgrp + "]", "debug", dbarLoc);
		if ( wgrp == null ) {
			plCategories = new Vector<String>();
			plCategories.add("-select category-");
			return plCategories;
		}
		if ( "".equals(wgrp) ) {
			plCategories = new Vector<String>();
			plCategories.add("-select category-");
			return plCategories;
		}
		if ( wgrp.contains("-select") ) {
			plCategories = new Vector<String>();
			plCategories.add("-select category-");
			return plCategories;
		}

		plCategories = new Vector<String>();
		plCategories.add("-select category-");
		try {
			Vector<ObjectObject> sdata = getPlSectionsData();
			for (ObjectObject sd : sdata) {
				catname = "";
				grpname = "";
				try {
					catname = sd.get("category").stringValue();
					grpname = sd.get("group").stringValue();
				} catch (InterpretException ie) {
					catname = "Interperet Error: " + ie.toString();
				}
				if ( wgrp.equals(grpname) ) {
					if ( !(plCategories.contains(catname)) ) {
						plCategories.add(catname);
					}
				}
			}
			if ( localDebug ) HS_Util.debug("for group [" + wgrp + "] plCategories has " + plCategories.size() + " entries.", "debug", dbarLoc);
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			plCategories.add("ERROR: " + e.toString());
		}

		return plCategories;
	}

	public Vector<String> getPlGroups() {
		final String dbarLoc = "InspectionBean.getPlGroups";
		final boolean localDebug = false;

		if ( plGroups == null ) {
			plGroups = new Vector<String>();
		}
		if ( plGroups.size() == 0 ) {
			try {
				String grpname = "";
				Vector<ObjectObject> sdata = getPlSectionsData();
				for (ObjectObject sd : sdata) {
					grpname = "";
					try {
						grpname = sd.get("group").stringValue();
					} catch (InterpretException ie) {
						grpname = "Interperet Error: " + ie.toString();
					}
					if ( !(plGroups.contains(grpname)) ) {
						plGroups.add(grpname);
					}
				}
				if ( plGroups.size() > 1 ) {
					Vector<String> templist = new Vector<String>();
					templist.add("-select group-");
					for (String grp : plGroups) {
						templist.add(grp);
					}
					plGroups = templist;
				} else if ( plGroups.size() < 1 ) {
					plGroups.add("No Groups Found");
				}
				if ( localDebug ) HS_Util.debug("plGroups has " + plGroups.size() + " entries.", "debug", dbarLoc);
			} catch (Exception e) {
				HS_Util.debug(e.toString(), "error", dbarLoc);
				if ( HS_Util.isDebugServer() ) e.printStackTrace();
				plGroups.add("ERROR: " + e.toString());
			}
		}
		return plGroups;
	}

	public Vector<ObjectObject> getPlObservationData() {
		String vKey = this.getViolationModule().get(0);
		plObservationData = this.getInspPlObj().loadDataObservations(vKey);
		return plObservationData;
	}

	public Vector<String> getPlSections() {
		final String dbarLoc = "InspectionBean.getPlSections";
		final boolean localDebug = false;

		String wcat = "";
		if ( plSections != null ) {
			// String wcat = (String) ExtLibUtil.getViewScope().get(
			// "violationCategory");
			wcat = getWrkCat();
			if ( localDebug ) HS_Util.debug("wrkCat is:[" + wcat + "]", "debug", dbarLoc);

			if ( wcat == null ) return plSections;
			if ( "".equals(wcat) ) return plSections;

			// if (wcat.equals(getWrkCat()))
			// return plSections;
			// setWrkCat(wcat);
		}
		plSections = new Vector<String>();
		plSections.add("-select section-");
		String sname = "";
		String snum = "";
		String sdesc = "";
		String catname = "";
		String used = "";
		String reuse = "";

		Vector<ObjectObject> sdata = getPlSectionsData();
		for (ObjectObject sd : sdata) {
			catname = "";
			try {
				catname = sd.get("category").stringValue();
				used = sd.get("used").stringValue();
				reuse = sd.get("reuse").stringValue();
			} catch (InterpretException e) {
				// nothing
			}
			if ( wcat.equals(catname) && ("no".equals(used) || "yes".equals(reuse)) ) {
				sname = "";
				snum = "";
				sdesc = "";
				try {
					snum = sd.get("number").stringValue();
					sdesc = sd.get("description").stringValue();

					sname = snum;
					sname += ("".equals(sdesc)) ? "" : " - " + sdesc;
					sname += "|" + snum;
				} catch (InterpretException e) {
					sname = "ERROR " + e.toString();
				}
				if ( !(plSections.contains(sname)) ) {
					plSections.add(sname);
				} else {
					if ( localDebug ) HS_Util.debug("Duplicate Item: " + sname, "debug", dbarLoc);
				}
			}
		}
		Collections.sort(plSections);
		if ( localDebug ) HS_Util.debug("for [" + wcat + "] plSections has " + plSections.size() + " entries.", "debug", dbarLoc);

		return plSections;
	}

	public Vector<ObjectObject> getPlSectionsData() {
		final String dbarLoc = "InspectionBean.getPlSectionsData";
		final boolean localDebug = false;

		String vKey = this.getViolationModule().get(0);
		Vector<ObjectObject> result = this.getInspPlObj().loadDataSections(vKey);
		if ( localDebug ) HS_Util.debug("for:[" + vKey + "] returning:[" + result.size() + "] entries.", "debug", dbarLoc);
		return result;
	}

	public Vector<String> getPlViolationList() {
		final String dbarLoc = "InspectionBean.getPlViolationList";
		final boolean localDebug = false;

		Date sdate = new Date();

		if ( plViolationList == null ) plViolationList = new Vector<String>();
		String wsect = getWrkSection();
		String wio = getWrkInOut();
		boolean donull = ("".equals(wsect)) ? true : (wsect.contains("-select")) ? true : ("Out".equals(wio)) ? false : true;
		plViolationList = new Vector<String>();
		plViolationList.add("-select violation-");
		setWrkViolation("-select violation-");
		if ( localDebug ) HS_Util.debug("wsect:[" + wsect + "] wio:[" + wio + "] donull:[" + donull + "]", "debug", dbarLoc);
		Vector<ObjectObject> violData = this.getPlViolationsData();
		try {
			for (ObjectObject viol : violData) {
				String sect = viol.get("number").stringValue();
				if ( donull || (sect.equals(wsect)) ) {
					String code = viol.get("code").stringValue();
					String desc = viol.get("description").stringValue();
					String plvalue = code + " - " + desc + "|" + code;
					plViolationList.add(plvalue);
				}
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
		}
		if ( plViolationList.size() > 1 ) {
			Collections.sort(plViolationList);
		}
		Date edate = new Date();
		long msecs = edate.getTime() - sdate.getTime();

		if ( localDebug )
			HS_Util.debug("for [" + wsect + "] plViolationList has [" + plViolationList.size() + "] entries from [" + violData.size() + "] possible entries. in [" + msecs + "] msecs.", "debug", dbarLoc);
		return plViolationList;
	}

	public Vector<ObjectObject> getPlViolationsData() {
		// final String dbarLoc = "InspectionBean.getPlViolationsData";
		// final boolean localDebug = false;
		String vKey = this.getViolationModule().get(0);
		plViolationsData = this.getInspPlObj().loadDataViolations(vKey);
		return plViolationsData;
	}

	public Vector<String> getPpm() {
		// if (ppm == null) ppm = new Vector<String>();
		return ppm;
	}

	public ArrayList<Violation> getPreviousViolations() {
		if ( previousViolations == null ) previousViolations = new ArrayList<Violation>();
		return previousViolations;
	}

	public String getPrevVMFacType() {
		if ( prevVMFacType == null ) prevVMFacType = "";
		return prevVMFacType;
	}

	public String getReceivedBy() {
		return receivedBy;
	}

	public String getReceivedByTitle() {
		return receivedByTitle;
	}

	public Date getRelease() {
		return release;
	}

	public Vector<String> getRepeatC() {
		if ( repeatC == null ) {
			repeatC = new Vector<String>();
			// TODO - Calculation for this??
		}
		return repeatC;
	}

	public Vector<String> getRepeatK() {
		if ( repeatK == null ) {
			repeatK = new Vector<String>();
			// TODO - Calculation for this??
		}
		return repeatK;
	}

	public Vector<String> getRepeatO() {
		if ( repeatO == null ) {
			repeatO = new Vector<String>();
			// TODO - Calculation for this??
		}
		return repeatO;
	}

	public String getRiskRating() {
		if ( riskRating == null ) riskRating = "";
		return riskRating;
	}

	public Vector<String> getSaniMethod() {
		// if (saniMethod == null) saniMethod = new Vector<String>();
		return saniMethod;
	}

	public Vector<String> getSaniName() {
		// if (saniName == null) saniName = new Vector<String>();
		return saniName;
	}

	public Vector<String> getSaniType() {
		// if (saniType == null) saniType = new Vector<String>();
		return saniType;
	}

	public ArrayList<String> getSaveMessages() {
		return saveMessages;
	}

	public Date getScheduledInspectionDate() {
		if ( scheduledInspectionDate == null ) {
			// TODO How is this computed? - Leave it as null for now...
		}
		return scheduledInspectionDate;
	}

	public String getSectionJson() {
		return HS_BO_Sections.getJson(this);
	}

	/*
	 * Section management functions getter adn setter...
	 */
	public Vector<InspectionSection> getSections() {
		if ( sections == null ) sections = new Vector<InspectionSection>();
		return sections;
	}

	public String getSignatureData1() {
		if ( signatureData1 == null ) signatureData1 = "";
		return signatureData1;
	}

	public String getSignatureData2() {
		if ( signatureData2 == null ) signatureData2 = "";
		return signatureData2;
	}

	public String getSignatureData64_1() {
		return signatureData64_1;
	}

	public String getSignatureData64_2() {
		return signatureData64_2;
	}

	public Vector<String> getSignatureHistory() {
		if ( signatureHistory == null ) signatureHistory = new Vector<String>();
		return signatureHistory;
	}

	public String getSmoking() {
		return smoking;
	}

	public String getSource() {
		if ( source == null ) source = "";
		if ( "".equals(source) ) source = HS_Util.getSession().getCommonUserName() + " (via HSTouch)";
		return source;
	}

	public Vector<String> getThermoLabel() {
		// if (thermoLabel == null) thermoLabel = new Vector<String>();
		return thermoLabel;
	}

	public double getTimeSpent() {
		// private Vector<TimeTracking> timeTrackingRecords;
		// private double timeSpent;
		if ( this.timeTrackingRecords == null ) {
			timeSpent = 0;
		} else {
			timeSpent = 0;
			Iterator<TimeTracking> ttitr = this.timeTrackingRecords.iterator();
			TimeTracking timetr = null;
			while (ttitr.hasNext()) {
				timetr = ttitr.next();
				timeSpent += timetr.computeTimeSpent();
			}
		}
		return timeSpent;
	}

	public TimeTracking getTimeTracking() {
		if ( this.timeTracking == null ) {
			try {
				this.timeTracking = new TimeTracking();
				this.timeTracking.loadDefaults(this);
			} catch (Exception e) {
				HS_Util.debug(e.toString(), "error", "inspectionBean.getTimeTracking");
			}
		}
		return timeTracking;
	}

	public Vector<TimeTracking> getTimeTrackingRecords() {
		if ( this.timeTrackingRecords == null ) this.timeTrackingRecords = new Vector<TimeTracking>();
		return timeTrackingRecords;
	}

	public String getTobaccoCounter() {
		if ( tobaccoCounter == null ) tobaccoCounter = "";
		return fixYesOrNo(tobaccoCounter);
	}

	public String getTobaccoMachine() {
		if ( tobaccoMachine == null ) tobaccoMachine = "";
		return fixYesOrNo(tobaccoMachine);
	}

	public String getTobaccoObserved() {
		if ( tobaccoObserved == null ) tobaccoObserved = "";
		return fixYesOrNo(tobaccoObserved);
	}

	public String getTobaccoProducts() {
		if ( tobaccoProducts == null ) tobaccoProducts = "";
		return fixYesOrNo(tobaccoProducts);
	}

	public String getTobaccoProducts2() {
		if ( tobaccoProducts == null ) tobaccoProducts = "";
		return fixYesOrNo(tobaccoProducts2);
	}

	public String getTobaccoSign() {
		if ( tobaccoSign == null ) tobaccoSign = "";
		return fixYesOrNo(tobaccoSign);
	}

	public String getTobaccoSold() {
		if ( tobaccoSold == null ) tobaccoSold = "";
		return fixYesOrNo(tobaccoSold);
	}

	public int getTotalViolations() {
		return totalViolations;
	}

	public int getTotHazardRating() {
		return totHazardRating;
	}

	public String getUnid() {
		return unid;
	}

	public String getUniqueViewId() {
		return uniqueViewId;
	}

	public String getUseHazard() {
		if ( useHazard == null ) {
			useHazard = HS_Util.getGlobalSettingsString("UseHazardRating");
		}
		return useHazard;
	}

	public String getUseHazardRating() {
		if ( useHazardRating == null ) {
			useHazardRating = HS_Util.getGlobalSettingsString("UseHazardRating");
		}
		return useHazardRating;
	}

	public Vector<String> getValidationMessages() {
		if ( validationMessages == null ) validationMessages = new Vector<String>();
		return validationMessages;
	}

	public String getViewDescription() {
		if ( viewDescription == null ) viewDescription = "";
		return viewDescription;
	}

	public Vector<String> getViolationModule() {
		final String dbarLoc = this.getClass().getName() + ".getViolationModule";
		// final boolean localDebug = false;
		final boolean localDebug = "Yes".equals(HS_Util.getAppSettingString("debug_InspectionRead"));
		// final boolean localDebug = HS_Util.isDebugger() &&
		// HS_Util.isDebugServer();
		try {
			if ( violationModule == null ) {
				violationModule = new Vector<String>();
			}
			if ( localDebug ) {
				HS_Util.debug("On entry prev type:[" + getPrevVMFacType() + "] curr type:[" + getFacilityType() + "] " + HS_Util.debugCalledFrom(), "debug", dbarLoc);
			}
			if ( violationModule.size() > 0 ) {
				if ( !getPrevVMFacType().equals(getFacilityType()) ) {
					if ( localDebug ) HS_Util.debug("Facility type changed from:[" + getPrevVMFacType() + "] to:[" + getFacilityType() + "]", "debug", dbarLoc);
					violationModule = new Vector<String>();
				}
			}
			Document parentFacilityDoc = null;
			if ( violationModule.size() == 0 ) {
				// Compute the violation module value...
				try {
					if ( localDebug ) HS_Util.debug("looking for parent with unid:[" + this.getParentUnid() + "]", "debug", dbarLoc);
					parentFacilityDoc = this.getParentDoc(this.getParentUnid());
				} catch (Exception e) {
					// do thing here
				}
				if ( parentFacilityDoc == null ) {
					HS_Util.debug("Unable to access facility with UNID:[" + this.getParentUnid() + "]", "error", dbarLoc);
				} else {
					String strForm = this.getForm();
					String strDbDesign = this.getDbDesign();
					violationModule = this.getViolationModule(parentFacilityDoc, strForm, strDbDesign);
					if ( localDebug ) HS_Util.debug("for: strForm:[" + strForm + "] and strDbDesign:[" + strDbDesign + "]", "debug", dbarLoc);
					if ( localDebug ) HS_Util.debug("set to: " + violationModule.toString(), "debug", dbarLoc);
					setPrevVMFacType(getFacilityType());
				}
			}
			if ( violationModule.size() == 0 ) {
				HS_Util.debug("violationModule is empty. Using defaults! [" + this.getForm() + "]", "warn", dbarLoc);
				violationModule.add(this.getForm());
				// violationModule.add("FoodReportState");
				// violationModule.add("FoodReportState");
				// violationModule.add("FoodReportState");
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			violationModule = new Vector<String>();
			violationModule.add(this.getForm());
		}
		return violationModule;
	}

	@SuppressWarnings("unchecked")
	/*
	 * Attempted to duplicate functionality from the LotusScript code in the library Inspections method setViolationModule
	 */
	private Vector<String> getViolationModule(Document parentFacilityDoc, String strForm, String strDbDesign) {
		final String dbarLoc = "InspectionBean.getViolationModule.2";
		final boolean localDebug = false;
		// final boolean localDebug = HS_Util.isDebugger() &&
		// HS_Util.isDebugServer();
		Vector<String> result = new Vector<String>();
		// HS_profiles hspro = new HS_profiles();
		String strMSModule = "";
		Vector<String> mastSettingsModule = null;
		String strPForm = parentFacilityDoc.getItemValueString("Form");
		// Object tempObj = null;

		if ( localDebug ) HS_Util.debug("strForm:[" + strForm + "] strPForm:[" + strPForm + "] strDbDesign:[" + strDbDesign + "]", "debug", dbarLoc);

		if ( "AgricultureReport".equals(strForm) ) {
			result.add("FoodReport");
			return result;
		}
		if ( "AviaryReport".equals(strForm) ) {
			result.add("Aviary");
			return result;
		}
		if ( "BareReport".equals(strForm) ) {
			mastSettingsModule = HS_Util.getMasterSettingsVector("BareModule");
			if ( !"".equals(mastSettingsModule.get(0)) ) {
				result = this.returnViolationModule(mastSettingsModule, parentFacilityDoc, true);
			} else {
				result.add("Bare");
			}
			return result;
		}
		if ( "BodyartReport".equals(strForm) ) {
			mastSettingsModule = HS_Util.getMasterSettingsVector("BodyartModule");
			if ( mastSettingsModule == null ) {
				mastSettingsModule = new Vector<String>();
				mastSettingsModule.add("");
			}
			if ( mastSettingsModule.size() < 1 ) mastSettingsModule.add("");
			if ( !"".equals(mastSettingsModule.get(0)) ) {
				result = this.returnViolationModule(mastSettingsModule, parentFacilityDoc, true);
				return result;
			} else {
				if ( "WestVirginia".equalsIgnoreCase(strDbDesign) ) {
					if ( this.getFacilityType().contains("Piercing") ) {
						result.add("Body Piercing Studio");
						return result;
					} else if ( this.getFacilityType().contains("Tattoo") ) {
						result.add("Tattoo Studio");
						return result;
					}
				}
			}
		}
		if ( "BeachReport".equals(strForm) ) {
			if ( "Ohio".equals(strDbDesign) ) {
				result.add("Bathing Beach");
				return result;
			}
			if ( "Missouri".equals(strDbDesign) ) {
				result.add("Beach");
				return result;
			}
			// TODO Is there something else as a default?
		}
		if ( "CampReport".equals(strForm) ) {
			if ( strDbDesign == "Ohio" ) {
				result.add("RV Park/Campground");
				return result;
			}
			// TODO Is there something else as a default?
		}
		if ( "FairgroundReport".equals(strForm) ) {
			result.add("Fairground");
			return result;
		}
		if ( "CampReport".equals(strForm) ) {
			if ( strDbDesign == "Ohio" ) {
				result.add("RV Park/Campground");
				return result;
			}
		}
		if ( "FoodReport".equals(strForm) ) {
			if ( "TemporaryVendorFacility".equals(strPForm) ) {
				Vector<String> tvstr = (Vector<String>) HS_profiles.get("eiRoot", "MasterSettings", "TempVendModule");
				String tvm = (tvstr == null) ? "" : (tvstr.size() == 0) ? "" : tvstr.get(0);
				if ( "".equals(tvm) ) {
					result.add("Temporary Food");
				} else {
					result = returnViolationModule(tvstr, parentFacilityDoc, true);
				}
				return result;
			}

			Vector<String> foodModule = (Vector<String>) HS_profiles.get("eiRoot", "MasterSettings", "FoodModule");
			strMSModule = (foodModule == null) ? strForm : (foodModule.size() == 0) ? strForm : foodModule.get(0);
			if ( localDebug ) HS_Util.debug("for [" + strForm + "][" + strPForm + "] MSModule:[" + strMSModule + "]  foodModule has " + foodModule.size() + " values.", "debug", dbarLoc);

			String pType = parentFacilityDoc.getItemValueString("Type");
			String bType = parentFacilityDoc.getItemValueString("BillingType");

			if ( "Wisconsin".equals(strDbDesign) && "Vending".equals(pType) ) {
				if ( bType.contains("Commissary") || bType.contains("Storage") ) {
					result = returnViolationModule(foodModule, parentFacilityDoc, true);
					if ( "".equals(result.toString()) ) {
						result.add(strForm);
					}
					return result;
				}
				result.add("Vending");
				return result;
			}
			if ( !("".equals(strMSModule)) ) {
				if ( localDebug ) HS_Util.debug("calling returnViolationModule", "debug", dbarLoc);
				result = returnViolationModule(foodModule, parentFacilityDoc, true);
				if ( result.size() == 0 ) result.add(strForm);
				if ( localDebug ) HS_Util.debug("Returning: " + result.toString() + " [" + result.size() + "]", "debug", dbarLoc);
				return result;
			}
			if ( pType.contains("Mobile") ) {
				if ( "Michigan".equals(strDbDesign) ) {
					result.add("Food");
					return result;
				} else if ( strDbDesign != "Arizona" && strDbDesign != "Wisconsin" && strDbDesign != "Ohio" && strDbDesign != "WA" && strDbDesign != "Colorado" ) {
					result.add("Mobile Food Service & Pushcarts");
					return result;
				}
			} else {
				if ( pType.contains("Vending") ) {
					result.add("Vending");
					return result;
				}
			}

			if ( strDbDesign == "Wisconsin" && (parentFacilityDoc.getItemValueInteger("BillingType") > 0 || parentFacilityDoc.getItemValueInteger("Commissary") > 0) ) {
				result.add("FoodReport");
				return result;
			}
			// IS THIS THE DEFAULT??
			if ( "".equals(result.toString()) ) {
				result.add(strForm);
			}
			return result;
		}
		if ( "GarbageReport".equals(strForm) ) {
			result.add("Garbage Hauler");
			return result;
		}
		if ( "HousingReport".equals(strForm) ) {
			result.add("Housing");
			return result;
		}
		if ( "JailReport".equals(strForm) ) {
			result.add("Jail");
			return result;
		}
		if ( "MobileHomeReport".equals(strForm) ) {
			result.add("Mobile Home Park");
			return result;
		}
		if ( "LaborCampReport".equals(strForm) ) {
			result.add(strForm + parentFacilityDoc.getItemValueString("Regulations"));
			return result;
		}
		if ( "PoolReport".equals(strForm) ) {
			result.add("Pool");
			return result;
		}
		if ( "SchoolReport".equals(strForm) ) {
			result.add("School");
			return result;
		}
		if ( "SepticRemovalReport".equals(strForm) ) {
			if ( strDbDesign == "WA" ) {
				result.add("Septic Removal");
				return result;
			}
		}
		if ( "SummerCampReport".equals(strForm) ) {
			if ( strDbDesign == "Arizona" || strDbDesign == "Wisconsin" ) {
				result.add("Recreational Educational Camp");
				return result;
			} else if ( strDbDesign == "Ohio" ) {
				result.add("Resident Camp");
				return result;
			}
		}
		result.add(strForm);
		return result;
	}

	public Vector<Violation> getViolations() {
		if ( violations == null ) violations = new Vector<Violation>();
		return violations;
	}

	public String getViolationSection() {
		if ( violationSection == null ) violationSection = "";
		return violationSection;
	}

	public String getViolationSortOrder() {
		if ( violationSortOrder == null ) {
			violationSortOrder = HS_Util.getAppSettingString("inspViolationsOrder");
		}
		return violationSortOrder;
	}

	public Vector<String> getViolationSubModule() {
		if ( violationSubModule == null ) {
			violationSubModule = new Vector<String>();
		}
		if ( violationSubModule.size() == 0 ) {
			// TODO Compute the violation Sub module value...
		}
		return violationSubModule;
	}

	public Vector<String> getWareWashTemp() {
		// if (wareWashTemp == null) wareWashTemp = new Vector<String>();
		return wareWashTemp;
	}

	public String getWrkCat() {
		if ( wrkCat == null ) wrkCat = "";
		return wrkCat;
	}

	public Date getWrkCorrectByDate() {
		return wrkCorrectByDate;
	}

	public String getWrkCorrectedSet() {
		if ( wrkCorrectedSet == null ) wrkCorrectedSet = "";
		return wrkCorrectedSet;
	}

	public String getWrkCorrectiveAction() {
		if ( wrkCorrectiveAction == null ) wrkCorrectiveAction = "";
		return wrkCorrectiveAction;
	}

	public String getWrkCritical() {
		if ( wrkCritical == null ) wrkCritical = "";
		return wrkCritical;
	}

	public String getWrkDescription() {
		if ( wrkDescription == null ) wrkDescription = "";
		return wrkDescription;
	}

	public String getWrkDescription(String snum, String code) {
		final boolean localDebug = false;
		final String dbarLoc = "inspection.getWrkDescription";

		wrkDescription = "";
		if ( "".equals(code) ) return wrkDescription;
		if ( "".equals(snum) ) return wrkDescription;

		wrkDescription = snum + " : " + code + " not found";
		String vcode = "";
		String vnum = "";
		String vdesc = "";
		Vector<ObjectObject> vdata = this.getPlViolationsData();
		if ( vdata.size() > 0 ) {
			for (ObjectObject vd : vdata) {
				try {
					vcode = vd.get("code").stringValue();
					vnum = vd.get("number").stringValue();
					vdesc = vd.get("description").stringValue();
				} catch (InterpretException e) {
					// nothing
					vcode = "";
					vnum = "";
				}
				if ( code.equals(vcode) && snum.equals(vnum) ) {
					wrkDescription = vdesc;
					if ( localDebug ) HS_Util.debug("for:[" + snum + "]:[" + code + "] got:[" + vdesc, "debug", dbarLoc);
					return wrkDescription;
				}
			}
		}
		if ( localDebug ) HS_Util.debug("for:[" + snum + "]:[" + code + "] got:[NOT FOUND]", "debug", dbarLoc);
		return wrkDescription;
	}

	public String getWrkGrp() {
		if ( wrkGrp == null ) wrkGrp = "";
		return wrkGrp;
	}

	public String getWrkHzrdRating() {
		if ( wrkHzrdRating == null ) wrkHzrdRating = "";
		return wrkHzrdRating;
	}

	public String getWrkInOut() {
		if ( wrkInOut == null ) wrkInOut = "";
		return wrkInOut;
	}

	public String getWrkLegalText() {
		if ( wrkLegalText == null ) wrkLegalText = "";
		return wrkLegalText;
	}

	public Vector<String> getWrkMessages() {
		if ( wrkMessages == null ) wrkMessages = new Vector<String>();
		return wrkMessages;
	}

	public String getWrkObservation() {
		if ( wrkObservation == null ) wrkObservation = "";
		return wrkObservation;
	}

	public String getWrkRepeat() {
		if ( wrkRepeat == null ) wrkRepeat = "";
		return wrkRepeat;
	}

	public String getWrkSection() {
		if ( wrkSection == null ) wrkSection = "";
		// if ("".equals(wrkSection)) {
		// wrkSection = (String) ExtLibUtil.getViewScope().get(
		// "violationSection");
		// if (wrkSection == null)
		// wrkSection = "";
		// }
		return wrkSection;
	}

	public String getWrkStatus() {
		if ( wrkStatus == null ) wrkStatus = "";
		return wrkStatus;
	}

	public String getWrkViolation() {
		if ( wrkViolation == null ) wrkViolation = "";
		return wrkViolation;
	}

	public boolean isHSTouch() {
		return hSTouch;
	}

	private boolean isMember(String tstr, String[] list) {
		boolean result = false;
		for (String x : list) {
			if ( x.equals(tstr) ) result = true;
		}
		return result;
	}

	private boolean isMember(String tstr, Vector<String> vlist) {
		boolean result = false;
		for (String x : vlist) {
			if ( x.equals(tstr) ) result = true;
		}
		return result;
	}

	public boolean isTabletDebug() {
		return tabletDebug;
	}

	public boolean isUseTobacco() {
		return useTobacco;
	}

	public boolean load(Document idoc) {
		HS_BusinessObjects bo = new HS_BusinessObjects();
		return bo.load(this, idoc);
	}

	public boolean load(Manageable bo, Document idoc) {
		return bo.load(this, idoc);
	}

	public boolean load(Manageable bo, String unid) {
		final String dbarLoc = "InspectionBean.loadDefaultFacilityInfo";
		final boolean localDebug = false;
		HS_database hsdb = new HS_database();
		Database wdb = hsdb.getdb("eiRoot");
		if ( localDebug ) HS_Util.debug("Loading from db:[" + wdb.getFilePath() + "]-[" + unid + "] " + HS_Util.debugCalledFrom(), "debug", dbarLoc);

		return bo.load(this, wdb, unid);
	}

	public String loadDataForFollowup(String srcunid) {
		String result = "";
		HS_database hsdb = new HS_database();
		Database wdb = hsdb.getdb("eiRoot");
		result = HS_BO_Violations.loadDataForFollowup(this, wdb, srcunid);

		return result;
	}

	public void loadDefaultFacilityInfo(String unid) {
		final String dbarLoc = "InspectionBean.loadDefaultFacilityInfo";
		// final boolean localDebug = HS_Util.isDebugServer();
		final boolean localDebug = false;

		// if (unid == null) return;

		Document pdoc = getParentDoc(unid);
		if ( pdoc != null ) {
			if ( localDebug ) HS_Util.debug("parent Doc unid:[" + unid + "] form:[" + pdoc.getFormName() + "] " + HS_Util.debugCalledFrom(), "debug", dbarLoc);
			// if ( this.tabletDebug ) System.out.print(dbarLoc +
			// ": parent Doc unid:[" + unid + "] form:[" + pdoc.getFormName() +
			// "] " +
			// HS_Util.debugCalledFrom());
			this.facilityName = pdoc.getItemValueString("Name");
			this.facilityType = pdoc.getItemValueString("Type");

			this.facilityPhysicalBuilding = pdoc.getItemValueString("PhysicalBuilding");
			this.facilityPhysicalCity = pdoc.getItemValueString("PhysicalCity");
			this.facilityPhysicalDirection = pdoc.getItemValueString("PhysicalDirection");
			this.facilityPhysicalPostalCode = pdoc.getItemValueString("PhysicalPostalCode");
			this.facilityPhysicalProvince = pdoc.getItemValueString("PhysicalProvince");
			this.facilityPhysicalRegType = pdoc.getItemValueString("PhysicalRegType");
			this.facilityPhysicalStreetName = pdoc.getItemValueString("PhysicalStreetName");
			this.facilityPhysicalStreetSuffix = pdoc.getItemValueString("PhysicalStreetSuffix");
			this.facilityPhysicalStreetType = pdoc.getItemValueString("PhysicalStreetType");
			this.facilityPhysicalSuite = pdoc.getItemValueString("PhysicalSuite");
			this.facilityID2 = (!pdoc.hasItem("DocumentID3")) ? "" : (pdoc.getItemValue("DocumentID3").size() == 0) ? "" : "" + pdoc.getItemValue("DocumentID3").get(0);
			if ( this.facilityID2.endsWith(".0") ) this.facilityID2 = this.facilityID2.substring(0, this.facilityID2.length() - 2);

			this.billingType = pdoc.getItemValueString("BillingType");

			String wphone = pdoc.getItemValueString("PhoneDayArea");
			wphone += ("".equals(wphone)) ? "" : " ";
			wphone += pdoc.getItemValueString("PhoneDayNumber");
			this.facilityTelephone = wphone;
			if ( localDebug ) HS_Util.debug("this.facilityTelephone =  set to:[" + wphone + "]", "debug", dbarLoc);

			this.magDistrict = pdoc.getItemValueString("MagDistrict");

			this.allYearRound = pdoc.getItemValueString("AllYearRound");
			this.riskRating = pdoc.getItemValueString("RiskRating");
			this.parentForm = pdoc.getItemValueString("Form");

			this.physicalMunicipality = pdoc.getItemValueString("physicalMunicipality");
			this.ownersName = pdoc.getItemValueString("ownersName");

			// TODO These are not in the facility document - Where do I get
			// them...
			this.ownerId = pdoc.getItemValueString("OwnerId");
			Document odoc = this.getOwnerDoc(pdoc.getParentDatabase(), this.ownerId);
			if ( odoc != null ) {
				this.ownersAddress = odoc.getItemValueString("PhysicalAddress");
				this.ownersCity = odoc.getItemValueString("PhysicalCity");
				this.ownersPostalCode = odoc.getItemValueString("PhysicalPostalCode");
				this.ownersState = odoc.getItemValueString("PhysicalProvince");
			} else {
				this.ownersAddress = "";
				this.ownersCity = "";
				this.ownersPostalCode = "";
				this.ownersState = "";
			}
			this.ehsId = pdoc.getParentDatabase().getReplicaID();

			// Newbs - 2017-02-10 Fixed to handle non-existant or empty readers
			// field on parent .
			if ( pdoc.hasItem("EHSReaders") ) {
				this.ehsReaders = pdoc.getItemValue("EHSReaders");
			} else {
				this.ehsReaders = new Vector<Object>();
			}
			if ( this.ehsReaders.size() == 0 ) this.ehsReaders.add("*");

			if ( this.tabletDebug && localDebug ) System.out.print(dbarLoc + ": ehsid:[" + this.ehsId + "] ehsReaders:[" + this.ehsReaders.toString() + "] facilityID2:{" + this.facilityID2 + "]");
			this.eho = pdoc.getItemValueString("EHO");
			// TODO Is there more of this??

			if ( localDebug ) HS_Util.debug("Set facility name:[" + this.facilityName + "] equipDescrip:[" + this.equipDescrip + "]", "debug", dbarLoc);
			if ( this.tabletDebug && localDebug ) System.out.print(dbarLoc + ": Set facility name:[" + this.facilityName + "] equipDescrip:[" + this.equipDescrip + "]");
			// if we are here and this.equipDescrip is null then we must be
			// loading a new inspection
			// So we need to go get teh temperature data from the previous
			// inspection.
			if ( this.equipDescrip == null ) this.loadNewTemperatureData(pdoc);
		} else {
			if ( localDebug ) HS_Util.debug("parent Doc not found for unid:[" + unid + "]", "error", dbarLoc);
			if ( this.tabletDebug ) System.out.print(dbarLoc + ": parent Doc not found for unid:[" + unid + "]");
		}
	}

	@SuppressWarnings("deprecation")
	public ObjectObject loadEhoData(String ehoName) {
		ObjectObject result = null;
		final String dbarLoc = "InspectionBean.loadEhoData";
		final boolean localDebug = false;

		HS_database hsdb = new HS_database();
		Database eidb = hsdb.getdb("eiRoot");
		if ( eidb == null ) {
			HS_Util.debug("could not get access to eiRoot database", "error", dbarLoc);
			return result;
		}
		View ehoView = eidb.getView("StaffLookUp");
		Document ehoDoc = ehoView.getDocumentByKey(ehoName, true);
		if ( ehoDoc == null ) {
			HS_Util.debug("could not get eho document for [" + ehoName + "]", "warn", dbarLoc);
			return result;
		}
		if ( localDebug ) HS_Util.debug("Building ehodata for [" + ehoName + "]", "debug", dbarLoc);
		result = new ObjectObject();
		try {
			result.put("phoneArea", FBSUtility.wrap(ehoDoc.getItemValueString("DayAreaCode")).toFBSString());
			result.put("phoneNumber", FBSUtility.wrap(ehoDoc.getItemValueString("DayNumber")).toFBSString());
			result.put("subOffice", FBSUtility.wrap(ehoDoc.getItemValueString("SubOffice")).toFBSString());
			result.put("subOfficeAddress", FBSUtility.wrap(ehoDoc.getItemValueString("SubOfficeAddress")).toFBSString());
			result.put("subOfficeCity", FBSUtility.wrap(ehoDoc.getItemValueString("SubOfficeCity")).toFBSString());
			result.put("subOfficePostalCode", FBSUtility.wrap(ehoDoc.getItemValueString("SubOfficePostalCode")).toFBSString());
			result.put("subOfficePhone", FBSUtility.wrap(ehoDoc.getItemValueString("SubOfficePhone")).toFBSString());
		} catch (InterpretException e) {
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			HS_Util.debug(e.toString(), "error", dbarLoc);
		}

		return result;
	}

	private void loadNewTemperatureData(Document pdoc) {
		final String dbarLoc = "InspectionBean.loadNewTemperatureData";
		final boolean localDebug = false;
		final String unidKey = "inspUnids";

		if ( localDebug ) HS_Util.debug(HS_Util.debugCalledFrom(), "debug", dbarLoc);

		Map<String, Object> sscope = ExtLibUtil.getSessionScope();
		ObjectObject ssdata = null;
		if ( sscope == null ) return;
		if ( !sscope.containsKey(vdScopeKey) ) {
			if ( !HS_Util.isDebugServer() ) {
				HS_Util.debug("Unable to access sessionScope." + vdScopeKey, "error", dbarLoc);
				return;
			}
			HS_Util.debug("Unable to access sessionScope." + vdScopeKey + " - but continuing...", "error", dbarLoc);
			try {
				ssdata = new ObjectObject();
				ssdata.put("facName", FBSUtility.wrap(pdoc.getItemValueString("Name")).toFBSString());
				ssdata.put("facUnid", FBSUtility.wrap(pdoc.getItemValueString("facUnid")).toFBSString());
				ssdata.put("facUrl", FBSUtility.wrap("?openDocument").toFBSString());
				ssdata.put("inspSelected", FBSUtility.wrap("").toFBSString());
				// Build the inspUnids array...
				ArrayObject iunids = new ArrayObject();
				iunids.addArrayValue(FBSUtility.wrap(pdoc.getUniversalID()).toFBSString());
				ssdata.put(unidKey, iunids);

				sscope.put(vdScopeKey, ssdata);
			} catch (InterpretException e) {
				HS_Util.debug(e.toString(), "error", dbarLoc);
			}
			return;
		}

		try {
			ssdata = (ObjectObject) sscope.get(vdScopeKey);
			ArrayObject iunids = null;
			if ( ssdata.hasProperty(unidKey) ) iunids = (ArrayObject) ssdata.get(unidKey);
			if ( iunids == null ) {
				HS_Util.debug("Unable to access sessionScope." + vdScopeKey + "." + unidKey, "error", dbarLoc);
			} else if ( iunids.getArrayLength() == 0 ) {
				if ( localDebug ) HS_Util.debug("No prior inspections found. iunids.getArrayLength():[" + iunids.getArrayLength() + "]", "debug", dbarLoc);
				// TODO - Set all the values to empty... Maybe we do not need to
				// this...
			} else {
				String lunid = iunids.getLastArrayValue().stringValue();
				if ( localDebug ) HS_Util.debug("Loading prior inspection temps from unid:[" + lunid + "]", "debug", dbarLoc);
				HS_BusinessObjects bo = new HS_BusinessObjects();
				bo.loadTemperatureData(this, lunid, pdoc.getParentDatabase());
				// Do the same for managers here??
				bo.loadManagerData(this, lunid, pdoc.getParentDatabase());
			}
		} catch (InterpretException e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}
	}

	public void moveViolation(int vidx, String direction) {
		this.moveViolation(vidx, direction, "");
	}

	public void moveViolation(int vidx, String direction, String actionTaken) {
		final String dbarLoc = "InspectionBean.moveViolation";
		final boolean localDebug = HS_Util.isDebugServer();
		try {
			if ( localDebug ) HS_Util.debug("moving entry [" + vidx + "] to:[" + direction + "] " + "actionTaken:[" + actionTaken + "]", "debug", dbarLoc);
			Vector<Violation> fromviols = null;
			Vector<Violation> toviols = null;
			if ( "Corrected".equals(direction) ) {
				fromviols = this.getViolations();
				toviols = this.getCorrectedViolations();
			} else if ( "".equals(direction) ) {
				toviols = this.getViolations();
				fromviols = this.getCorrectedViolations();
			}
			if ( fromviols == null || toviols == null ) {
				HS_Util.debug("fromviols or toviols not found for direction:[" + direction + "]", "error", dbarLoc);
			} else {
				if ( localDebug ) HS_Util.debug("moving entry [" + vidx + "] to:[" + direction + "] " + "fromviols.size:[" + fromviols.size() + "] toviols.size:[" + toviols.size() + "]", "debug", dbarLoc);
				Violation moveviol = fromviols.remove(vidx);
				if ( !"".equals(actionTaken) ) {
					String caction = moveviol.getCorrectiveAction() + "  Action taken: " + actionTaken;
					moveviol.setCorrectiveAction(caction);
				}
				toviols.add(moveviol);
				if ( localDebug ) HS_Util.debug("moved entry [" + vidx + "] [" + moveviol.getCode() + "] to:[" + direction + "]  actionTaken:[" + actionTaken + "]", "debug", dbarLoc);
			}
			this.sortViolations();
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}
	}

	public boolean removeManagerItem(int idx) {
		final String dbarLoc = "InspectionBean.removeManagerItem";
		final boolean localDebug = HS_Util.isDebugServer();
		boolean result = false;

		try {
			if ( localDebug ) HS_Util.debug("managers before:[" + this.manager.toString() + "]", "debug", dbarLoc);
			Vector<String> mgrs = HS_Util.removeVectorItem(this.manager, idx);
			Vector<String> mgrAddrs = HS_Util.removeVectorItem(this.managerAddress, idx);
			Vector<String> mgrCertIds = HS_Util.removeVectorItem(this.managerCertificateId, idx);
			Vector<String> mgrCertExps = HS_Util.removeVectorItem(this.managerCertificateExpirationStr, idx);
			if ( localDebug ) HS_Util.debug("managers after:[" + mgrs + "]", "debug", dbarLoc);

			this.manager = mgrs;
			this.managerAddress = mgrAddrs;
			this.managerCertificateId = mgrCertIds;
			this.managerCertificateExpirationStr = mgrCertExps;
			result = true;
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}
		return result;
	}

	public boolean removeSection(int wnum) {
		return HS_BO_Sections.removeSection(wnum, this);
	}

	public boolean removeTemperatureItem(String ttype, int idx) {
		final String dbarLoc = "InspectionBean.removeTemperatureItem";
		final boolean localDebug = false;
		boolean result = false;

		if ( "equipment".equals(ttype) ) {
			try {
				if ( localDebug ) HS_Util.debug("edescs before:[" + this.equipDescrip + "]", "debug", dbarLoc);
				Vector<String> edescs = HS_Util.removeVectorItem(this.equipDescrip, idx);
				Vector<String> etemps = HS_Util.removeVectorItem(this.equipTemp, idx);
				if ( localDebug ) HS_Util.debug("edescs after:[" + edescs + "]", "debug", dbarLoc);

				this.equipDescrip = edescs;
				this.equipTemp = etemps;
				result = true;
			} catch (Exception e) {
				HS_Util.debug(e.toString(), "error", dbarLoc);
				if ( HS_Util.isDebugServer() ) e.printStackTrace();
			}
		} else if ( "food".equals(ttype) ) {
			try {
				if ( localDebug ) HS_Util.debug("fdescs before:[" + this.foodDescrip + "]", "debug", dbarLoc);
				Vector<String> fdescs = HS_Util.removeVectorItem(this.foodDescrip, idx);
				Vector<String> ftemps = HS_Util.removeVectorItem(this.foodTemp, idx);
				Vector<String> fstates = HS_Util.removeVectorItem(this.foodState, idx);
				if ( localDebug ) HS_Util.debug("edescs after:[" + fdescs + "]", "debug", dbarLoc);

				this.foodDescrip = fdescs;
				this.foodTemp = ftemps;
				this.foodState = fstates;
				result = true;
			} catch (Exception e) {
				HS_Util.debug(e.toString(), "error", dbarLoc);
				if ( HS_Util.isDebugServer() ) e.printStackTrace();
			}
		} else if ( "washing".equals(ttype) ) {
			try {
				if ( localDebug ) HS_Util.debug("mnames before:[" + this.machineName + "]", "debug", dbarLoc);
				Vector<String> mnames = HS_Util.removeVectorItem(this.machineName, idx);
				Vector<String> smethods = HS_Util.removeVectorItem(this.saniMethod, idx);
				Vector<String> tlabels = HS_Util.removeVectorItem(this.thermoLabel, idx);
				Vector<String> snames = HS_Util.removeVectorItem(this.saniName, idx);
				Vector<String> stypes = HS_Util.removeVectorItem(this.saniType, idx);
				Vector<String> ppms = HS_Util.removeVectorItem(this.ppm, idx);
				Vector<String> wtemps = HS_Util.removeVectorItem(this.wareWashTemp, idx);

				if ( localDebug ) HS_Util.debug("mnames after:[" + mnames + "]", "debug", dbarLoc);

				this.machineName = mnames;
				this.saniMethod = smethods;
				this.thermoLabel = tlabels;
				this.saniName = snames;
				this.saniType = stypes;
				this.ppm = ppms;
				this.wareWashTemp = wtemps;
				result = true;
			} catch (Exception e) {
				HS_Util.debug(e.toString(), "error", dbarLoc);
				if ( HS_Util.isDebugServer() ) e.printStackTrace();
			}
		}

		return result;
	}

	public void removeViolation(int vidx) {
		final String dbarLoc = "InspectionBean.removeViolation";
		final boolean localDebug = false;
		try {
			Vector<Violation> viols = this.getViolations();
			Violation rviol = viols.remove(vidx);
			if ( localDebug ) HS_Util.debug("removed entry [" + vidx + "] [" + rviol.getCode() + "]", "debug", dbarLoc);
			// this.setViolations(viols);
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
		}
	}

	public void resetSaveMessages() {
		this.saveMessages = new ArrayList<String>();
	}

	private Vector<String> returnViolationModule(Vector<String> module, Document pFacDoc, Boolean useType) {
		final String dbarLoc = "InspectionBean.returnViolationModule";
		final boolean localDebug = false;
		// final boolean localDebug = HS_Util.isDebugger() &&
		// HS_Util.isDebugServer();

		Vector<String> tempModule = new Vector<String>();
		String pType = pFacDoc.getItemValueString("Type");
		if ( localDebug ) HS_Util.debug("type: [" + pType + "] useType: [" + useType + "]", "debug", dbarLoc);

		for (String mstr : module) {
			if ( useType ) {
				if ( mstr.contains(pType) ) {
					tempModule.add(mstr.substring(0, mstr.indexOf(" - ")));
				} else {
					if ( localDebug ) HS_Util.debug("[" + mstr + "] does not contain:[" + pType + "]", "debug", dbarLoc);
				}
			} else {
				tempModule.add(mstr.substring(0, mstr.indexOf(" - ")));
			}
		}
		if ( localDebug ) {
			// HS_Util.debug("module: " + module, "debug", dbarLoc);
			HS_Util.debug("returning: " + tempModule, "debug", dbarLoc);
		}
		return tempModule;
	}

	public boolean save(Manageable bo) { // See next section
		final String dbarLoc = "InspectionBean.save";
		final boolean localDebug = HS_Util.isDebugServer();
		boolean result = true;
		try {
			if ( !this.validate() ) {
				if ( localDebug ) HS_Util.debug("inspection validation failed", "debug", dbarLoc);
				return false;
			}
			// this.setSaveMessages(new ArrayList<String>());
			if ( localDebug ) HS_Util.debug("calling bo.save(this) using bean with uniqueViewId:[" + this.uniqueViewId + "]", "debug", dbarLoc);
			result = bo.save(this);
			if ( result ) {
				saveUnidInInspectionViewData(this.unid);
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			this.getSaveMessages().add("ERROR:" + e.toString());
		}
		return result;
	}

	public boolean saveEditedViolation(int vidx) {
		final String dbarLoc = "InspectionBean.saveEditedViolation";
		final boolean localDebug = false;

		Violation viol = this.getViolations().get(vidx);
		if ( viol == null ) {
			HS_Util.debug("Could not access violation for update:[" + vidx + "]", "error", dbarLoc);
			return false;
		}
		if ( this.saveViolation(viol) ) {
			this.sortViolations();
			if ( localDebug ) HS_Util.debug("violation updated - status:[" + viol.getStatus() + "] critical:[" + viol.getCriticalSet() + "]", "debug", dbarLoc);
			this.clearViolation();
			return true;
		}
		if ( localDebug ) HS_Util.debug("Violation update error", "error", dbarLoc);
		return false;
	}

	public boolean saveNewViolation() {
		final String dbarLoc = "InspectionBean.saveNewViolation";
		final boolean localDebug = false;
		try {
			String code = this.getWrkViolation();
			String snum = this.getWrkSection();

			if ( snum.contains("-select") || "".equals(snum) ) {
				if ( violationFindSectionInfo(code) ) {
					snum = this.getWrkSection();
				} else {
					HS_Util.debug("Failed to find section data for code [" + code + "]", "error", dbarLoc);
				}
			}
			if ( localDebug ) HS_Util.debug("Saving violation with code:[" + code + "] section number:[" + snum + "] critical:[" + this.getWrkCritical() + "]", "debug", dbarLoc);
			Violation viol = new Violation();
			if ( !this.saveViolation(viol) ) {
				return false;
			}
			this.getViolations().add(viol);
			this.sortViolations();

			if ( localDebug ) HS_Util.debug("violation saved - status:[" + viol.getStatus() + "] critical:[" + viol.getCriticalSet() + "]", "debug", dbarLoc);
			this.clearViolation();
			return true;
		} catch (Exception e) {
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			HS_Util.debug(e.toString(), "error", dbarLoc);
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	private void saveUnidInInspectionViewData(String newUnid) {
		final String dbarLoc = "InspectionBean.saveUnidInInspectionViewData";
		final boolean localDebug = false;

		try {
			Map<String, Object> sscope = ExtLibUtil.getSessionScope();
			if ( sscope == null ) return;
			ObjectObject ssdata = (ObjectObject) sscope.get(vdScopeKey);
			if ( localDebug ) HS_Util.debug("ssdata is a:[" + ssdata.getClass().getName(), "debug", dbarLoc);

			ArrayObject iunids = (ArrayObject) ssdata.get("inspUnids");
			if ( localDebug ) HS_Util.debug("iuniids is a:[" + iunids.getClass().getName(), "debug", dbarLoc);
			// iunids.add(0, newUnid);
			ArrayObject newIunids = new ArrayObject();
			// newIunids.addArrayValue(FBSUtility.wrap(newUnid).toFBSString());
			Iterator<FBSValue> itr = iunids.iterateValues();
			while (itr.hasNext()) {
				newIunids.addArrayValue(itr.next());
			}
			newIunids.addArrayValue(FBSUtility.wrap(newUnid).toFBSString());
			// ssdata.remove("inspUnids");
			ssdata.delete("inspUnids");
			// ssdata.put("inspUnids", iunids);
			ssdata.put("inspUnids", newIunids);
			ExtLibUtil.getSessionScope().put(vdScopeKey, ssdata);
			if ( localDebug ) HS_Util.debug(vdScopeKey + "inspUnids update with [" + iunids.getArrayLength() + "] members", "debug", dbarLoc);
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
		}
	}

	private boolean saveViolation(Violation viol) {
		final String dbarLoc = "InspectionBean.saveViolation";
		// final boolean localDebug = false;

		try {
			viol.setCode(this.getWrkViolation());
			viol.setCorrectedSet(this.getWrkCorrectedSet());

			// Add logic to append Corrected By Date if defined. - Done with
			// CSJS
			viol.setCorrectiveAction(this.getWrkCorrectiveAction());
			viol.setCriticalSet(this.getWrkCritical());

			viol.setDescription(this.getWrkDescription(this.getWrkSection(), this.getWrkViolation()));
			viol.setHzrdRating(this.getWrkHzrdRating());
			viol.setLegalTxt(this.getWrkLegalText());
			viol.setObservations(this.getWrkObservation());

			viol.setRepeat(this.getWrkRepeat());

			viol.setGroup(this.getWrkGrp());
			viol.setCategory(this.getWrkCat());
			viol.setSection(this.getWrkSection());
			viol.setStatus(this.getWrkStatus());
			return true;
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
		}
		return false;
	}

	public void setAllYearRound(String allYearRound) {
		this.allYearRound = allYearRound;
	}

	public void setAnnounceInsp(String announceInsp) {
		this.announceInsp = announceInsp;
	}

	public void setBillable(String billable) {
		this.billable = billable;
	}

	public void setBillablePermit(String billablePermit) {
		this.billablePermit = billablePermit;
	}

	public void setBillingType(String billingType) {
		this.billingType = billingType;
	}

	public void setCertExpiration(String certExpiration) {
		this.certExpiration = certExpiration;
	}

	public void setCertified(String certified) {
		this.certified = certified;
	}

	public void setCertifiedManager(String certifiedManager) {
		this.certifiedManager = certifiedManager;
	}

	public void setCertNumber(String certNumber) {
		this.certNumber = certNumber;
	}

	public void setChildRatio(String childRatio) {
		this.childRatio = childRatio;
	}

	public void setChokingPoster(String chokingPoster) {
		this.chokingPoster = chokingPoster;
	}

	public void setCommentInput(UIComponent component) {
		commentInput = component;
	}

	public void setComments(String comments) {
		final boolean localDebug = false;
		final String dbarLoc = "inspectionbean.setComments";
		if ( localDebug ) HS_Util.debug("Comment: [" + comments + "] uniqueViewId:[" + this.uniqueViewId + "]", "debug", dbarLoc);
		this.comments = comments;
	}

	public void setComplaintDate(Date complaintDate) {
		this.complaintDate = complaintDate;
	}

	public void setComplianceSet(String complianceSet) {
		this.complianceSet = complianceSet;
	}

	public void setComputedCriticals(int ComputedCriticals) {
		this.computedCriticals = ComputedCriticals;
	}

	public void setCorrectedViolations(Vector<Violation> correctedViolations) {
		this.correctedViolations = correctedViolations;
	}

	public void setCreatedFromInspID(String createdFromInspID) {
		this.createdFromInspID = createdFromInspID;
	}

	public void setCriticalViolationRanges(String criticalViolationRanges) {
		this.criticalViolationRanges = criticalViolationRanges;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public void setDbDesign(String dbDesign) {
		this.dbDesign = dbDesign;
	}

	public void setDgbonc(String dgbonc) {
		this.dgbonc = dgbonc;
	}

	public void setDisplayFollowUp(String displayFollowUp) {
		this.displayFollowUp = displayFollowUp;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public void setDtmuhrsc(String dtmuhrsc) {
		this.dtmuhrsc = dtmuhrsc;
	}

	public void setDtmulg(String dtmulg) {
		this.dtmulg = dtmulg;
	}

	public void setEho(String eho) {
		this.eho = eho;
	}

	public void setEhoData(ObjectObject ehoData) {
		this.ehoData = ehoData;
	}

	public void setEhsId(String ehsId) {
		this.ehsId = ehsId;
	}

	public void setEhsReaders(Vector<Object> ehsReaders) {
		this.ehsReaders = ehsReaders;
	}

	public void setEmailSent(String emailSent) {
		this.emailSent = emailSent;
	}

	public void setEnforcement(String enforcement) {
		this.enforcement = enforcement;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public void setEquipDescrip(Vector<String> equipDescrip) {
		this.equipDescrip = equipDescrip;
	}

	public void setEquipTemp(Vector<String> equipTemp) {
		this.equipTemp = equipTemp;
	}

	public void setEventEndDate(Date eventEndDate) {
		this.eventEndDate = eventEndDate;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public void setEventStartDate(Date eventStartDate) {
		this.eventStartDate = eventStartDate;
	}

	public void setFacilityApprovedNumber(String facilityApprovedNumber) {
		this.facilityApprovedNumber = facilityApprovedNumber;
	}

	public void setFacilityID2(String facilityID2) {
		this.facilityID2 = facilityID2;
	}

	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}

	public void setFacilityPhysicalBuilding(String facilityPhysicalBuilding) {
		this.facilityPhysicalBuilding = facilityPhysicalBuilding;
	}

	public void setFacilityPhysicalCity(String facilityPhysicalCity) {
		this.facilityPhysicalCity = facilityPhysicalCity;
	}

	public void setFacilityPhysicalDirection(String facilityPhysicalDirection) {
		this.facilityPhysicalDirection = facilityPhysicalDirection;
	}

	public void setFacilityPhysicalPostalCode(String facilityPhysicalPostalCode) {
		this.facilityPhysicalPostalCode = facilityPhysicalPostalCode;
	}

	public void setFacilityPhysicalProvince(String facilityPhysicalProvince) {
		this.facilityPhysicalProvince = facilityPhysicalProvince;
	}

	public void setFacilityPhysicalRegType(String facilityPhysicalRegType) {
		this.facilityPhysicalRegType = facilityPhysicalRegType;
	}

	public void setFacilityPhysicalStreetName(String facilityPhysicalStreetName) {
		this.facilityPhysicalStreetName = facilityPhysicalStreetName;
	}

	public void setFacilityPhysicalStreetSuffix(String facilityPhysicalStreetSuffix) {
		this.facilityPhysicalStreetSuffix = facilityPhysicalStreetSuffix;
	}

	public void setFacilityPhysicalStreetType(String facilityPhysicalStreetType) {
		this.facilityPhysicalStreetType = facilityPhysicalStreetType;
	}

	public void setFacilityPhysicalSuite(String facilityPhysicalSuite) {
		this.facilityPhysicalSuite = facilityPhysicalSuite;
	}

	public void setFacilityRegType(String facilityRegType) {
		this.facilityRegType = facilityRegType;
	}

	public void setFacilityTelephone(String facilityTelephone) {
		this.facilityTelephone = facilityTelephone;
	}

	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	public void setFollowupInspectionRequired(String fuiReq) {
		if ( "on".equals(fuiReq) ) fuiReq = "Yes";
		if ( "off".equals(fuiReq) ) fuiReq = "No";
		this.followupInspectionRequired = fuiReq;
	}

	public void setFollowupTypes(Vector<String> followupTypes) {
		this.followupTypes = followupTypes;
	}

	public void setFoodDescrip(Vector<String> foodDescrip) {
		this.foodDescrip = foodDescrip;
	}

	public void setFoodSafe(String foodSafe) {
		this.foodSafe = foodSafe;
	}

	public void setFoodState(Vector<String> foodState) {
		this.foodState = foodState;
	}

	public void setFoodTemp(Vector<String> foodTemp) {
		this.foodTemp = foodTemp;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public void setFullCloudMap(Map<String, Object> imap) {
		this.fullCloudMap = imap;
	}

	public void setGboc(String gboc) {
		this.gboc = gboc;
	}

	public void setGradeComputedByHazRating(String gradeComputedByHazRating) {
		this.gradeComputedByHazRating = gradeComputedByHazRating;
	}

	public void setGradeComputedByNumCriticals(String gradeComputedByNumCriticals) {
		this.gradeComputedByNumCriticals = gradeComputedByNumCriticals;
	}

	public void setGradeRanges(String gradeRanges) {
		this.gradeRanges = gradeRanges;
	}

	public void setHasSig1(String hasSig1) {
		this.hasSig1 = hasSig1;
	}

	public void setHasSig2(String hasSig2) {
		this.hasSig2 = hasSig2;
	}

	public void setHazRatingType(String hazRatingType) {
		this.hazRatingType = hazRatingType;
	}

	public void setHSTouch(boolean touch) {
		hSTouch = touch;
	}

	public void setHtcc(String htcc) {
		this.htcc = htcc;
	}

	public void setImageUnids(List<String> imageUnids) {
		this.imageUnids = imageUnids;
	}

	public void setInspectionDate(Date inspectionDate) {
		this.inspectionDate = inspectionDate;
	}

	public void setInspectionDateStr(String inspectionDateStr) {
		this.inspectionDateStr = inspectionDateStr;
		if ( !"".equals(inspectionDateStr) ) {
			DateTime wdt = HS_Util.getSession().createDateTime(inspectionDateStr);
			this.inspectionDate = wdt.toJavaDate();
		}
	}

	public void setInspectionOutcome(String inspectionOutcome) {
		this.inspectionOutcome = inspectionOutcome;
	}

	public void setInspPlObj(InspectionPicklists inspPlObj) {
		this.inspPlObj = inspPlObj;
	}

	public void setInspType(String inspType) {
		this.inspType = inspType;
	}

	public void setLetterGrade(String letterGrade) {
		this.letterGrade = letterGrade;
	}

	public void setLetterGrades(String letterGrades) {
		this.letterGrades = letterGrades;
	}

	public void setLicenseDisplayed(String licenseDisplayed) {
		this.licenseDisplayed = licenseDisplayed;
	}

	public void setLoadedForm(HS_auditForm loadedForm) {
		this.loadedForm = loadedForm;
	}

	public void setLocked(String locked) {
		this.locked = locked;
	}

	public void setMachineName(Vector<String> machineName) {
		this.machineName = machineName;
	}

	public void setMagDistrict(String magDistrict) {
		this.magDistrict = magDistrict;
	}

	public void setManager(Vector<String> manager) {
		this.manager = manager;
	}

	public void setManagerAddress(Vector<String> managerAddress) {
		this.managerAddress = managerAddress;
	}

	public void setManagerAllergenTraining(Vector<String> managerAllergenTraining) {
		this.managerAllergenTraining = managerAllergenTraining;
	}

	public void setManagerCertificateExpirationStr(Vector<String> managerCertificateExpirationStr) {
		this.managerCertificateExpirationStr = managerCertificateExpirationStr;
	}

	public void setManagerCertificateId(Vector<String> managerCertificateId) {
		this.managerCertificateId = managerCertificateId;
	}

	public void setManagerId(Vector<String> managerId) {
		this.managerId = managerId;
	}

	public void setNextInspDue(Date nextInspDue) {
		this.nextInspDue = nextInspDue;
	}

	public void setNextInspectionDate(Date nextInspectionDate) {
		this.nextInspectionDate = nextInspectionDate;
	}

	public void setNextInspectionDateStr(String nextInspectionDateStr) {
		this.nextInspectionDateStr = nextInspectionDateStr;
		if ( !"".equals(nextInspectionDateStr) ) {
			DateTime wdt = HS_Util.getSession().createDateTime(nextInspectionDateStr);
			this.nextInspectionDate = wdt.toJavaDate();
		}
	}

	public void setNextManualDue(Date nextManualDue) {
		this.nextManualDue = nextManualDue;
	}

	public void setNumCore(int numCore) {
		this.numCore = numCore;
	}

	public void setNumCritical(int numCritical) {
		this.numCritical = numCritical;
	}

	public void setNumKey(int numKey) {
		this.numKey = numKey;
	}

	public void setNumNonCritical(int numNonCritical) {
		this.numNonCritical = numNonCritical;
	}

	public void setNumOther(int numOther) {
		this.numOther = numOther;
	}

	public void setNumPFoundation(int numPFoundation) {
		this.numPFoundation = numPFoundation;
	}

	public void setNumPriority(int numPriority) {
		this.numPriority = numPriority;
	}

	public void setNumRepeat(int numRepeat) {
		this.numRepeat = numRepeat;
	}

	public void setNumRisk(int numRisk) {
		this.numRisk = numRisk;
	}

	public void setObservedSeating(Integer observedSeating) {
		final boolean localDebug = false;
		final String dbarLoc = "inspectionBean.setObservedSeating";
		if ( localDebug ) HS_Util.debug("observedSeating being set to:[" + observedSeating + "]", "debug", dbarLoc);
		this.observedSeating = observedSeating;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public void setOwnersAddress(String ownersAddress) {
		this.ownersAddress = ownersAddress;
	}

	public void setOwnersCity(String ownersCity) {
		this.ownersCity = ownersCity;
	}

	public void setOwnersName(String ownersName) {
		this.ownersName = ownersName;
	}

	public void setOwnersPostalCode(String ownersPostalCode) {
		this.ownersPostalCode = ownersPostalCode;
	}

	public void setOwnersState(String ownersState) {
		this.ownersState = ownersState;
	}

	public void setParentForm(String parentForm) {
		this.parentForm = parentForm;
	}

	public void setParentUnid(String parentUnid) {
		this.setParentUnid(parentUnid, true);
	}

	public void setParentUnid(String parentUnid, boolean doLookup) {
		// final boolean localDebug = HS_Util.isDebugServer();
		final boolean localDebug = false;
		final String dbarLoc = "inspectionBean.setParentUnid";
		String cfrom = HS_Util.debugCalledFrom();
		// if (cfrom.indexOf("NativeMethodAccessorImpl") > -1) {
		// if (localDebug)
		// HS_Util.debug(cfrom + " with [" + parentUnid + "] curentValue:[" +
		// this.parentUnid + "] " +
		// "so not doing loadDefaultFacilityInfo", "debug",
		// dbarLoc);
		// } else {
		if ( localDebug ) HS_Util.debug("ParentUNID changing from:[" + this.parentUnid + "] to:[" + parentUnid + "] " + cfrom, "debug", dbarLoc);
		if ( this.isTabletDebug() && localDebug ) System.out.print(dbarLoc + ": - ParentUNID changing from:[" + this.parentUnid + "] to:[" + parentUnid + "] " + cfrom);
		if ( this.parentUnid == null ) {
			if ( doLookup ) this.loadDefaultFacilityInfo(parentUnid);
		} else if ( parentUnid.equals(this.parentUnid) ) {
			// do nothing since it did not change
		} else if ( !this.parentUnid.equals(parentUnid) ) {
			this.loadDefaultFacilityInfo(parentUnid);
		}
		// }
		this.parentUnid = parentUnid;
	}

	public void setPhysicalMunicipality(String physicalMunicipality) {
		this.physicalMunicipality = physicalMunicipality;
	}

	public void setPl_inspectionTypes(Vector<String> pl_inspectionTypes) {
		this.pl_inspectionTypes = pl_inspectionTypes;
	}

	public void setPlCategories(Vector<String> plCategories) {
		this.plCategories = plCategories;
	}

	public void setPlGroups(Vector<String> plGroups) {
		this.plGroups = plGroups;
	}

	public void setPlObservationData(Vector<ObjectObject> plObservationData) {
		this.plObservationData = plObservationData;
	}

	public void setPlSections(Vector<String> plSections) {
		this.plSections = plSections;
	}

	public void setPlViolationList(Vector<String> plViolationList) {
		this.plViolationList = plViolationList;
	}

	public void setPlViolationsData(Vector<ObjectObject> plViolationsData) {
		this.plViolationsData = plViolationsData;
	}

	public void setPpm(Vector<String> ppm) {
		this.ppm = ppm;
	}

	public void setPreviousViolations(ArrayList<Violation> previousViolations) {
		this.previousViolations = previousViolations;
	}

	public void setPrevVMFacType(String prevVMFacType) {
		this.prevVMFacType = prevVMFacType;
	}

	public void setReceivedBy(String receivedBy) {
		this.receivedBy = receivedBy;
	}

	public void setReceivedByTitle(String receivedByTitle) {
		this.receivedByTitle = receivedByTitle;
	}

	public void setRelease(Date release) {
		this.release = release;
	}

	public void setRepeatC(Vector<String> repeatC) {
		this.repeatC = repeatC;
	}

	public void setRepeatK(Vector<String> repeatK) {
		this.repeatK = repeatK;
	}

	public void setRepeatO(Vector<String> repeatO) {
		this.repeatO = repeatO;
	}

	public void setRiskRating(String riskRating) {
		this.riskRating = riskRating;
	}

	public void setSaniMethod(Vector<String> saniMethod) {
		this.saniMethod = saniMethod;
	}

	public void setSaniName(Vector<String> saniName) {
		this.saniName = saniName;
	}

	public void setSaniType(Vector<String> saniType) {
		this.saniType = saniType;
	}

	public void setSaveMessages(ArrayList<String> saveMessages) {
		this.saveMessages = saveMessages;
	}

	public void setScheduledInspectionDate(Date scheduledInspectionDate) {
		this.scheduledInspectionDate = scheduledInspectionDate;
	}

	public void setSections(Vector<InspectionSection> sections) {
		this.sections = sections;
	}

	public void setSignatureData1(String signatureData1) {
		this.signatureData1 = signatureData1;
	}

	public void setSignatureData2(String signatureData2) {
		this.signatureData2 = signatureData2;
	}

	public void setSignatureData64_1(String signatureData64_1) {
		this.signatureData64_1 = signatureData64_1;
	}

	public void setSignatureData64_2(String signatureData64_2) {
		this.signatureData64_2 = signatureData64_2;
	}

	public void setSignatureHistory(Vector<String> signatureHistory) {
		this.signatureHistory = signatureHistory;
	}

	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public void setTabletDebug(boolean tabletDebug) {
		this.tabletDebug = tabletDebug;
	}

	public void setThermoLabel(Vector<String> thermoLabel) {
		this.thermoLabel = thermoLabel;
	}

	public void setTimeSpent(double timeSpent) {
		this.timeSpent = timeSpent;
	}

	public void setTimeTracking(TimeTracking timeTracking) {
		this.timeTracking = timeTracking;
	}

	public void setTimeTrackingRecords(Vector<TimeTracking> timeTrackingRecords) {
		this.timeTrackingRecords = timeTrackingRecords;
	}

	public void setTobaccoCounter(String tobaccoCounter) {
		this.tobaccoCounter = fixYesOrNo(tobaccoCounter);
	}

	public void setTobaccoMachine(String tobaccoMachine) {
		this.tobaccoMachine = fixYesOrNo(tobaccoMachine);
	}

	public void setTobaccoObserved(String tobaccoObserved) {
		this.tobaccoObserved = fixYesOrNo(tobaccoObserved);
	}

	public void setTobaccoProducts(String tobaccoProducts) {
		this.tobaccoProducts = fixYesOrNo(tobaccoProducts);
	}

	public void setTobaccoProducts2(String tobaccoProducts2) {
		this.tobaccoProducts2 = fixYesOrNo(tobaccoProducts2);
	}

	public void setTobaccoSign(String tobaccoSign) {
		this.tobaccoSign = fixYesOrNo(tobaccoSign);
	}

	public void setTobaccoSold(String tobaccoSold) {
		this.tobaccoSold = fixYesOrNo(tobaccoSold);
	}

	public void setTotalViolations(int totalViolations) {
		this.totalViolations = totalViolations;
	}

	public void setTotHazardRating(int totHazardRating) {
		this.totHazardRating = totHazardRating;
	}

	public void setUnid(String unid) {
		this.unid = unid;
	}

	public void setUniqueViewId(String uniqueViewId) {
		this.uniqueViewId = uniqueViewId;
	}

	public void setUseHazard(String useHazard) {
		this.useHazard = useHazard;
	}

	public void setUseHazardRating(String useHazardRating) {
		this.useHazardRating = useHazardRating;
	}

	public void setUseTobacco(boolean useTobacco) {
		this.useTobacco = useTobacco;
	}

	public void setValidationMessages(Vector<String> validationMessages) {
		this.validationMessages = validationMessages;
	}

	public void setViewDescription(String viewDescription) {
		this.viewDescription = viewDescription;
	}

	public void setViolationModule(Vector<String> violationModule) {
		this.violationModule = violationModule;
	}

	public void setViolations(Vector<Violation> violations) {
		this.violations = violations;
	}

	public void setViolationSection(String violationSection) {
		this.violationSection = violationSection;
	}

	public void setViolationSortOrder(String violationSortOrder) {
		this.violationSortOrder = violationSortOrder;
	}

	public void setViolationSubModule(Vector<String> violationSubModule) {
		this.violationSubModule = violationSubModule;
	}

	public void setWareWashTemp(Vector<String> wareWashTemp) {
		this.wareWashTemp = wareWashTemp;
	}

	public void setWrkCat(String wrkCat) {
		this.wrkCat = wrkCat;
	}

	public void setWrkCorrectByDate(Date wrkCorrectByDate) {
		this.wrkCorrectByDate = wrkCorrectByDate;
	}

	public void setWrkCorrectedSet(String wrkCorrectedSet) {
		this.wrkCorrectedSet = wrkCorrectedSet;
	}

	public void setWrkCorrectiveAction(String wrkCorrectiveAction) {
		this.wrkCorrectiveAction = wrkCorrectiveAction;
	}

	public void setWrkCritical(String wrkCritical) {
		this.wrkCritical = wrkCritical;
	}

	public void setWrkDescription(String wrkDescription) {
		this.wrkDescription = wrkDescription;
	}

	public void setWrkGrp(String iwrkGrp) {
		final boolean localDebug = false;
		final String dbarLoc = "InspectionBean.setWrkGrp";
		if ( localDebug ) HS_Util.debug("setting it to: [" + iwrkGrp + "]", "debug", dbarLoc);
		this.wrkGrp = iwrkGrp;
	}

	public void setWrkHzrdRating(String wrkHzrdRating) {
		this.wrkHzrdRating = wrkHzrdRating;
	}

	public void setWrkInOut(String wrkInOut) {
		this.wrkInOut = wrkInOut;
	}

	public void setWrkLegalText(String wrkLegalText) {
		this.wrkLegalText = wrkLegalText;
	}

	public void setWrkMessages(Vector<String> wrkMessages) {
		this.wrkMessages = wrkMessages;
	}

	public void setWrkObservation(String wrkObservation) {
		this.wrkObservation = wrkObservation;
	}

	public void setWrkRepeat(String wrkRepeat) {
		this.wrkRepeat = wrkRepeat;
	}

	public void setWrkSection(String wrkSection) {
		this.wrkSection = wrkSection;
	}

	public void setWrkStatus(String wrkStatus) {
		this.wrkStatus = wrkStatus;
	}

	public void setWrkViolation(String wrkViolation) {
		this.wrkViolation = wrkViolation;
	}

	public boolean showInOutButtons() {
		boolean result = true;
		// String vsect = (String) ExtLibUtil.getViewScope().get(
		// "violationSection");
		String vsect = getWrkSection();
		if ( vsect == null )
			result = false;
		else if ( "".equals(vsect) )
			result = false;
		else if ( vsect.contains("-select") )
			result = false;
		else
			result = true;
		if ( !result ) {
			this.setWrkInOut("");
		}

		return result;
	}

	public void sortSections() {
		// final boolean localDebug = "TabletApp".equals(this.getEnvironment());
		final String dbarLoc = "inspection.sortSections";
		try {
			// if ( localDebug ) {
			// String vso = HS_Util.getAppSettingString("inspViolationsOrder");
			// System.out.print(dbarLoc + ": inspViolationsOrder:[" + vso +
			// "]");
			// }
			Collections.sort(this.getSections(), Sort_Section_Order);
		} catch (Exception e) {
			HS_Util.debug("Error in section sorting: " + e.toString(), "error", dbarLoc);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}
	}

	public void sortViolations() {
		final boolean localDebug = "TabletApp".equals(this.getEnvironment());
		final String dbarLoc = "inspection.sortViolations";
		try {
			if ( localDebug ) {
				String vso = HS_Util.getAppSettingString("inspViolationsOrder");
				if ( this.isTabletDebug() ) System.out.print(dbarLoc + ": inspViolationsOrder:[" + vso + "] " + HS_Util.debugCalledFrom());
			}
			Collections.sort(this.getViolations(), Sort_Violation_Order);
			Collections.sort(this.getCorrectedViolations(), Sort_Violation_Order);
		} catch (Exception e) {
			HS_Util.debug("Error in sorting: " + e.toString(), "error", dbarLoc);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}
	}

	public int timeTrackingAdd() {
		// this.getTimeTracking().add(new TimeTracking());
		this.timeTracking = new TimeTracking();
		this.timeTracking.loadDefaults(this);
		return this.getTimeTrackingRecords().size();
	}

	public void timeTrackingDelete(int tidx) {
		final String dbarLoc = "InspectionBean.timeTrackingDelete";
		final boolean localDebug = false;

		if ( tidx >= this.timeTrackingRecords.size() ) {
			HS_Util.debug("ERROR: requested index is too high[" + tidx + "]", "error", dbarLoc);
		} else {
			this.timeTrackingRecords.get(tidx).setDeleteMeFlag(true);
			if ( localDebug ) HS_Util.debug("DeleteMeFlagSet for [" + tidx + "]", "debug", dbarLoc);
		}
	}

	public String timeTrackingDisplay(TimeTracking ttr) {
		String result = ttr.buildViewDescription();
		return result;
	}

	/*
	 * inspection.updateInspectionDate(); inspection.updateViewDescription();
	 */
	public void updateInspectionDate() {
		final String dbarLoc = "InspectionBean.updateInspectionDate";
		final boolean localDebug = false;

		DateTime ndt = HS_Util.getSession().createDateTime("Today");
		// ndt.setAnyTime();
		this.inspectionDate = ndt.toJavaDate();
		if ( localDebug ) HS_Util.debug("inspectionDate set to:" + this.inspectionDate.toString(), "debug", dbarLoc);
	}

	public void updateViewDescription() {
		final String dbarLoc = "InspectionBean.updateViewDescription";
		final boolean localDebug = false;

		final String[] ritypes = new String[] { "Re-inspection", "Follow-up" };
		final String[] futypes = new String[] { "FU" };
		String strStatus = "";
		String strType = "";
		String strIDate = "";
		String strEho = "";
		String enf = "";
		try {
			strType = (isMember(this.getInspType(), ritypes)) ? "Re-inspection" : isMember(this.getInspType(), futypes) ? "Follow up" : this.getInspType();
			if ( localDebug ) HS_Util.debug("inspectionDate:[" + this.getInspectionDate() + "]", "debug", dbarLoc);
			strIDate = " on " + formatDate(this.getInspectionDate());

			strEho = ", by " + this.getEho();

			enf = this.getEnforcement();
			if ( "Release".equals(enf) ) {
				strStatus += ", License Status: " + enf;

				Date rdt = getRelease();
				if ( rdt != null ) {
					strStatus += " on " + formatDate(rdt);
				}
			} else if ( !("".equals(enf)) ) {
				strStatus = ", License Status: " + enf;
			}

			this.viewDescription = "Inspection " + strType + strIDate + strEho + strStatus;
			if ( localDebug ) HS_Util.debug("viewDescription set to:" + this.viewDescription, "debug", dbarLoc);
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			HS_Util.debug("strType is:[" + strType + "]", "error", dbarLoc);
			HS_Util.debug("strIDate is:[" + strIDate + "]", "error", dbarLoc);
			HS_Util.debug("strEho is:[" + strEho + "]", "error", dbarLoc);
			HS_Util.debug("enf is:[" + enf + "]", "error", dbarLoc);
			HS_Util.debug("strStatus is:[" + strStatus + "]", "error", dbarLoc);
		}
	}

	public void updateViolationDetails() {
		final String dbarLoc = "InspectionBean.updateViolationDetails";
		final boolean localDebug = false;

		String csect = this.getWrkSection();
		String vsect = "";
		String vcode = getWrkViolation();
		String odcode = "";
		if ( "".equals(vcode) || vcode.contains("-select") ) {
			this.setWrkObservation("");
			this.setWrkCorrectiveAction("");
			this.setWrkCritical("");
			if ( localDebug ) HS_Util.debug("Cleared Violation Details for [" + vcode + "]", "debug", dbarLoc);
		} else {
			if ( localDebug ) HS_Util.debug("Updating Violation Details for [" + vcode + "]", "debug", dbarLoc);
			Vector<ObjectObject> vdata = this.getPlViolationsData();
			for (ObjectObject vd : vdata) {
				try {
					vsect = vd.get("number").stringValue();
					odcode = vd.get("code").stringValue();
				} catch (InterpretException e) {
					vsect = "Error: " + e.toString();
				}
				if ( vsect.equals(csect) && odcode.equals(vcode) ) {
					try {
						this.setWrkCritical(vd.get("critical").stringValue());
						if ( localDebug ) HS_Util.debug("critical set to:[" + getWrkCritical() + "]", "debug", dbarLoc);
					} catch (InterpretException e) {
						// ignore
					}
				}
			}
			Vector<ObjectObject> odata = this.getPlObservationData();
			for (ObjectObject od : odata) {
				try {
					odcode = od.get("code").stringValue();
				} catch (InterpretException e) {
					odcode = e.toString();
				}
				if ( vcode.equals(odcode) ) {
					try {
						this.setWrkObservation(od.get("observation").stringValue());
						if ( localDebug ) HS_Util.debug("observation set to:[" + getWrkObservation() + "]", "debug", dbarLoc);
						this.setWrkCorrectiveAction(od.get("correction").stringValue());
						// TODO Leave with the first one must deal with
						// multiples someday
						return;
					} catch (InterpretException e) {
						this.setWrkCorrectiveAction("ERROR: " + e.toString());
					}
				}
			}
			this.setWrkObservation("Observation for violation " + vcode);
			this.setWrkCorrectiveAction("Corrective action for " + vcode);
			this.setWrkCritical("Maybe and maybe not");
		}
	}

	public boolean validate() {
		final String dbarLoc = "InspectionBean.validate";
		final boolean localDebug = false;
		boolean result = true;
		try {
			this.validationMessages = new Vector<String>();
			if ( localDebug ) HS_Util.debug("Type:[" + this.getInspType() + "] FU Req:[" + this.getFollowupInspectionRequired() + "] NIDtStr:[" + this.getNextInspectionDateStr() + "]", "debug", dbarLoc);
			if ( "".equals(this.getInspType()) ) {
				result = false;
				this.validationMessages.add("Inspection type is null.");
				if ( localDebug ) HS_Util.debug(this.validationMessages.get(this.validationMessages.size() - 1), "warn", dbarLoc);
			}

			/*
			 * 14 June 17 - Newbs modified to check the Next Inspection Date against the inspection date to allow for delays in transmission of documents.
			 */
			DateTime todayDT = HS_Util.getSession().createDateTime("Today");
			DateTime inspDT = null;
			DateTime nextInspDT = null;

			if ( "".equals(this.getInspectionDateStr()) ) {
				result = false;
				this.validationMessages.add("Inspection date must be specified.");
				if ( localDebug ) HS_Util.debug(this.validationMessages.get(this.validationMessages.size() - 1), "warn", dbarLoc);
			} else {
				try {
					inspDT = HS_Util.getSession().createDateTime(this.getInspectionDateStr());
				} catch (Exception e) {
					inspDT = null;
				}
				if ( inspDT == null ) {
					result = false;
					this.validationMessages.add("Inspection date is not a valid date - Use format dd-MMM-yyyy.");
					if ( localDebug ) HS_Util.debug(this.validationMessages.get(this.validationMessages.size() - 1), "warn", dbarLoc);
				} else {
					// todayDT.adjustDay(1);
					// System.out.println(dbarLoc +
					// " - todayDT.compareTo(idt):[" + todayDT.compareTo(idt) +
					// "]");
					if ( todayDT.compareTo(inspDT) < 0 ) {
						result = false;
						this.validationMessages.add("Inspection date cannot be in the future.");
						if ( localDebug ) HS_Util.debug(this.validationMessages.get(this.validationMessages.size() - 1), "warn", dbarLoc);
					}
				}
			}

			if ( !"".equals(this.getNextInspectionDateStr()) ) {
				try {
					nextInspDT = HS_Util.getSession().createDateTime(this.nextInspectionDateStr);
				} catch (Exception e) {
					nextInspDT = null;
				}
				if ( nextInspDT == null ) {
					result = false;
					this.validationMessages.add("Next Inspection date is not a valid date - Use format dd-MMM-yyyy.");
					if ( localDebug ) HS_Util.debug(this.validationMessages.get(this.validationMessages.size() - 1), "warn", dbarLoc);
				} else {
					// if (todayDT.compareTo(wdt) >= 0) {
					if ( inspDT.compareTo(nextInspDT) >= 0 ) {
						result = false;
						// this.validationMessages.add("Next inspection date must be in the future.");
						this.validationMessages.add("Next inspection date must be after the inpection date.");
						if ( localDebug ) HS_Util.debug(this.validationMessages.get(this.validationMessages.size() - 1), "warn", dbarLoc);
					}
					if ( localDebug ) HS_Util.debug("NIDtStr:[" + this.getNextInspectionDateStr() + "] created date of:[" + nextInspDT.getDateOnly() + "]", "debug", dbarLoc);
				}
			}
			if ( "Yes".equalsIgnoreCase(this.getFollowupInspectionRequired()) ) {
				if ( "".equals(this.getNextInspectionDateStr()) ) {
					result = false;
					this.validationMessages.add("Next inspection date must be specified if follow-up is required.");
					if ( localDebug ) HS_Util.debug(this.validationMessages.get(this.validationMessages.size() - 1), "warn", dbarLoc);
				}
			}
			if ( localDebug ) HS_Util.debug("TimeTrackingRecords().size():[" + this.getTimeTrackingRecords().size() + "]", "debug", dbarLoc);
			// if (this.getTimeTrackingRecords().size() == 0) {
			if ( HS_Util.getGlobalSettingsVector("TimeTrackingMandatory").contains("Inspections") && this.countTimeTrackingRecords() == 0 ) {
				result = false;
				this.validationMessages.add("Please enter one or more time tracking records before saving..");
				if ( localDebug ) HS_Util.debug(this.validationMessages.get(this.validationMessages.size() - 1), "warn", dbarLoc);
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
		}
		return result;
	}

	public boolean validateViolation() {
		final String dbarLoc = "InspectionBean.validateViolation";
		final boolean localDebug = false;

		boolean result = true;

		String cgrp = this.getWrkGrp();
		// String ccat = this.getWrkCat();
		String csnum = this.getWrkSection();
		String cinout = this.getWrkInOut();
		String cstat = this.getWrkStatus();
		String cviol = this.getWrkViolation();
		String cdesc = this.getWrkDescription();
		// String cobs = this.getWrkObservation();

		if ( localDebug ) {
			HS_Util.debug("wrkGrp: [" + cgrp + "]", "debug", dbarLoc);
			// HS_Util.debug("wrkCat: [" + ccat + "]", "debug", dbarLoc);
			// HS_Util.debug("wrkSection: [" + csnum + "]", "debug", dbarLoc);
			// HS_Util.debug("wrkInOut: [" + cinout + "]", "debug", dbarLoc);
			// HS_Util.debug("wrkStatus: [" + cstat + "]", "debug", dbarLoc);
			// HS_Util.debug("wrkViolation: [" + cviol + "]", "debug", dbarLoc);
			// HS_Util.debug("wrkDescription: [" + cdesc + "]", "debug",
			// dbarLoc);
			// HS_Util.debug("wrkObservation: [" + cobs + "]", "debug",
			// dbarLoc);
		}
		this.setWrkMessages(new Vector<String>());

		// Is their a violation selected, then set cstat to Out
		if ( !("".equals(cviol)) && "".equals(cstat) ) {
			if ( localDebug ) HS_Util.debug("Found violation with no status. setting status to Out.", "debug", dbarLoc);
			cstat = "Out";
			cinout = "Out";
			this.setWrkStatus(cstat);
			this.setWrkInOut(cinout);
			if ( "".equals(csnum) ) {
				if ( violationFindSectionInfo(cviol) ) {
					csnum = this.getWrkSection();
				} else {
					// TODO not found... Deal with it??
				}
			}
			cdesc = this.getWrkDescription(csnum, cviol);
			this.setWrkDescription(cdesc);
		} else {
			if ( localDebug ) HS_Util.debug("cviol: [" + cviol + "] snum:[" + csnum + "] cstat:[" + cstat + "] cinout:[" + cinout + "]", "debug", dbarLoc);
		}
		if ( "Out".equals(cstat) ) {
			if ( cviol.contains("-select") || "".equals(cviol) ) {
				this.getWrkMessages().add("Please select a violation which is out of compliance.");
				result = false;
			}
		} else if ( "".equals(cstat) ) {
			this.getWrkMessages().add("Please select a group, category and section and specify if it is In, Out, N/A, or N/O.");
			result = false;
		} else {
			this.getWrkMessages().add(cstat + " sections are not yet implemented.");
			result = false;
		}

		if ( localDebug ) HS_Util.debug("returning:" + result, "debug", dbarLoc);
		return result;
	}

	public boolean violationFindSectionInfo(String code) {
		final String dbarLoc = "InspectionBean.violationFindSectionInfo";
		final boolean localDebug = false;
		boolean result = false;
		Vector<ObjectObject> violData = null;
		Iterator<ObjectObject> vitr = null;
		String vmod = this.getViolationModule().get(0);
		try {
			violData = this.getInspPlObj().getDataViolations();
			if ( violData == null ) {
				violData = this.getInspPlObj().loadDataViolations(vmod);
				if ( violData == null ) throw (new Exception("Violations data returned a null value! vmod:[" + vmod + "]"));
			}
			vitr = violData.iterator();
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
		}
		ObjectObject viol = null;
		String vcode = "";
		String snum = "";
		String crit = "";
		String cat = "";
		if ( vitr == null ) {
			if ( localDebug ) HS_Util.debug("For code:[" + code + "] found snum:[" + snum + "] crit:[" + crit + "] cat:[" + cat + "] ", "debug", dbarLoc);
			return false;
		} else {
			while (vitr.hasNext()) {
				viol = vitr.next();
				try {
					vcode = viol.get("code").stringValue();
					if ( code.equals(vcode) ) {
						snum = viol.get("number").stringValue();
						crit = viol.get("critical").stringValue();
						cat = viol.get("category").stringValue();
						this.setWrkSection(snum);
						this.setWrkCritical(crit);
						this.setWrkCat(cat);
						if ( localDebug ) HS_Util.debug("For code:[" + code + "] found snum:[" + snum + "] crit:[" + crit + "] cat:[" + cat + "] ", "debug", dbarLoc);
						return true;
					}
				} catch (InterpretException e) {
					HS_Util.debug(e.toString(), "error", dbarLoc);
				}
			}
		}
		return result;
	}

}
