package com.healthspace.general;

import java.io.Serializable;

public class InspectionSection implements Serializable {

	private static final long serialVersionUID = 1L;

	private String category;
	private String description;
	private String display;
	private String group;
	private String module;
	private String number;
	private String status;
	private String observation;

	public InspectionSection() {
		this.group = "";
		this.category = "";
		this.description = "";
		this.module = "";
		this.number = "";
		this.status = "";
		this.observation = "";
	}

	public InspectionSection(String grp, String cat, String desc, String mod, String num, String stat) {
		this.group = grp;
		this.category = cat;
		this.description = desc;
		this.module = mod;
		this.number = num;
		this.status = stat;
		this.observation = "";
		HS_Util.debug("newInspection created for [" + num + "]", "debug", "InspectionSection");
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDisplay() {
		if ( display != null ) return display;
		// Section 1B - Person in Charge present, demonstrates knowledge and performs duties - In
		String result = "Section " + this.getNumber() + " - " + this.getDescription();
		result += ("".equals(this.getObservation())) ? "" : ": " + this.getObservation();
		result += " - ";

		String icon = ("In".equals(this.getStatus())) ? "ok" : ("Out".equals(this.getStatus())) ? "remove" : ("N/A".equals(this.getStatus())) ? "import"
				: ("N/O".equals(this.getStatus())) ? "export" : "";
		String color = ("In".equals(this.getStatus())) ? "success" : ("Out".equals(this.getStatus())) ? "danger" : ("N/A".equals(this.getStatus())) ? "primary"
				: ("N/O".equals(this.getStatus())) ? "warning" : "";
		String inspSectDetFmt = HS_Util.getAppSettingString("inspSectionDetailFormat");
		inspSectDetFmt = ("".equals(inspSectDetFmt)) ? "nohtml" : inspSectDetFmt;

		if ( "html".equals(inspSectDetFmt) ) {
			result += "<span class=\"text-" + color + "\">";
			result += "<i class=\"glyphicon glyphicon-" + icon + "\"></i>";
			result += "&nbsp;" + this.status + "</span>";
		} else {
			result += "" + this.status;
		}

		// if ( HS_Util.isDebugServer() ) {
		// result += " - cat: " + this.getCategory() + " grp: " + this.getGroup();
		// }
		display = result;
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

	public String getJson() {
		String result = "{";

		result += "'category' : '" + this.getCategory() + "',";
		result += "'description' : '" + this.getDescription() + "',";
		result += "'group' : '" + this.getGroup() + "',";
		result += "'module' : '" + this.getModule() + "',";
		result += "'number' : '" + this.getNumber() + "',";
		result += "'status' : '" + this.getStatus() + "'";

		result += "}";
		return result;
	}

}
