package com.healthspace.general;

public class Violation extends HS_base {

	private static final long serialVersionUID = 1L;

	public Violation() {

	}

	private String code; // ok
	private String correctedSet; // ok
	private String correctiveAction; // ok
	private String criticalSet; // ok
	private String description; // ok
	private String hzrdRating; // ok
	private String legalTxt; // ok
	private String observations; // ok
	private String repeat; // ok
	private String room;
	private String section; // ok
	private String status; // ok

	/*
	 * These are not saved to the documents but are neded for the data entry process.
	 */
	private String category;
	private String group;

	private boolean used = false;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCorrectedSet() {
		return correctedSet;
	}

	public void setCorrectedSet(String correctedSet) {
		this.correctedSet = correctedSet;
	}

	public String getCorrectiveAction() {
		return correctiveAction;
	}

	public void setCorrectiveAction(String correctiveAction) {
		this.correctiveAction = correctiveAction;
	}

	public String getCriticalSet() {
		return criticalSet;
	}

	public void setCriticalSet(String criticalSet) {
		this.criticalSet = criticalSet;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getHzrdRating() {
		return hzrdRating;
	}

	public void setHzrdRating(String hzrdRating) {
		this.hzrdRating = hzrdRating;
	}

	public String getLegalTxt() {
		return legalTxt;
	}

	public void setLegalTxt(String legalTxt) {
		this.legalTxt = legalTxt;
	}

	public String getObservations() {
		return observations;
	}

	public void setObservations(String observations) {
		this.observations = observations;
	}

	public String getRepeat() {
		return repeat;
	}

	public void setRepeat(String repeat) {
		this.repeat = repeat;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public boolean isUsed() {
		return used;
	}

	public void setUsed(boolean used) {
		this.used = used;
	}

	private String[] validationFields;
	private String[] validationMessages;

	public String[] getValidationFields() {
		return validationFields;
	}

	public void setValidationFields(String[] validationFields) {
		this.validationFields = validationFields;
	}

	public String[] getValidationMessages() {
		return validationMessages;
	}

	public void setValidationMessages(String[] validationMessages) {
		this.validationMessages = validationMessages;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public boolean validate() {
		boolean result = true;
		validationFields = new String[] {};
		validationMessages = new String[] {};

		return result;
	}
}
