package com.healthspace.general;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import java.util.Map.Entry;

import org.openntf.domino.Database;
import org.openntf.domino.DateTime;
import org.openntf.domino.Document;
import org.openntf.domino.DocumentCollection;
import org.openntf.domino.RichTextItem;
import org.openntf.domino.View;
import org.openntf.domino.ViewEntry;
import org.openntf.domino.ViewEntryCollection;

import com.healthspace.tools.JSFUtil;

public class HS_BO_Violations {

	public static String loadDataForFollowup(Inspection insp, Database wdb, String srcunid) {
		final String dbarLoc = "HS_BO_Violations.loadDataForFollowup";
		final boolean localDebug = HS_Util.isDebugServer();

		// updated for bootstrap version
		String result = "";
		Document currdoc = null;
		if ( localDebug ) HS_Util.debug("accessing source doc with unid:[" + srcunid + "] " + "in db:[" + wdb.getServer() + "!!" + wdb.getFilePath() + "]", "debug", dbarLoc);
		try {
			currdoc = wdb.getDocumentByUNID(srcunid);
			if ( "ExistingSystemReport".equals(currdoc.getFormName()) ) {
				// Manageable bo = HS_BusinessObjects;
				insp.load(HS_BusinessObjects.getInstance(), currdoc);
				// Source updated when saved for the first time.
				// DateCreated updated when saved for the first time.
			}
			copyViolationsToFollowup(insp, currdoc);

			insp.setLoadedForm(null);

			insp.setUnid(null); // Force a new document when saved.
			insp.setDocumentId((String) HS_Util.getSession().evaluate("@Unique").get(0));
			insp.setInspectionDate(null);
			insp.setFollowupInspectionRequired("");
			insp.setNextInspectionDate(null);
			insp.setSignatureData1("");
			insp.setSignatureData2("");
			insp.setSignatureHistory(null);
			insp.setReceivedBy("");
			insp.setReceivedByTitle("");

			// if("TemporaryVendorFacility".equalsIgnoreCase(currdoc.getItemValueString("ParentForm")))
			// {
			if ( "TemporaryVendorFacility".equalsIgnoreCase(insp.getParentForm()) ) {
				// Vector<DateTime> tdt =
				// currdoc.getItemValueDateTimeArray("EventEndDate");
				// insp.setEventEndDate(tdt.get(0).toJavaDate());
				insp.setEventEndDate(HS_Util.loadValueDate(currdoc, "EventEndDate"));
				insp.setEventId(currdoc.getItemValueString("EventId"));
				insp.setEventName(currdoc.getItemValueString("EventName"));
				// tdt = currdoc.getItemValueDateTimeArray("EventStartDate");
				// insp.setEventStartDate(tdt.get(0).toJavaDate());
				insp.setEventStartDate(HS_Util.loadValueDate(currdoc, "EventStartDate"));
			}

			if ( "FoodDPISchoolReport".equalsIgnoreCase(currdoc.getFormName()) ) {
				insp.setInspType("Second Inspection");
			} else if ( "FoodRetailReport".equalsIgnoreCase(currdoc.getFormName()) ) {
				insp.setInspType("Process Review");
			} else if ( "BareFacility".equalsIgnoreCase(currdoc.getFormName()) && "WestVirginia".equals(insp.getDbDesign()) ) {
				insp.setInspType("Clean Indoor Air");
			} else {
				// insp.setInspType(HS_Util.getMasterSettingsString("InspectionRoutineDefault"));
				insp.setInspType(HS_Util.getMasterSettingsString("InspectionFollowupDefault"));
			}
			if ( localDebug ) HS_Util.debug("Insp Type set to:[" + insp.getInspType() + "]", "debug", dbarLoc);

			if ( currdoc.hasItem("BillingType") ) insp.setBillingType(currdoc.getItemValueString("BillingType"));
			if ( currdoc.hasItem("RiskRating") ) insp.setRiskRating(currdoc.getItemValueString("RiskRating"));
			if ( currdoc.hasItem("MagDistrict") ) insp.setMagDistrict(currdoc.getItemValueString("MagDistrict"));
			if ( currdoc.hasItem("AllYearRound") ) insp.setAllYearRound(currdoc.getItemValueString("AllYearRound"));

			// insp.setParentUnid(currdoc.getUniversalID());
			// insp.setParentUnid(currdoc.getItemValueString("ParentId"));
			insp.setParentUnid(currdoc.getParentDocumentUNID());

			if ( currdoc.hasItem("NextInspection") ) insp.setNextManualDue(HS_Util.loadValueDate(currdoc, "NextInspection"));
			if ( currdoc.hasItem("NextInspDue") ) insp.setNextInspDue(HS_Util.loadValueDate(currdoc, "NextInspDue"));

			if ( "LaborCampReport".equalsIgnoreCase(insp.getForm()) ) {
				if ( currdoc.hasItem("NumMigrants") ) insp.setFacilityApprovedNumber("" + currdoc.getItemValueInteger("NumMigrants"));
				if ( currdoc.hasItem("Regulaions") ) insp.setFacilityRegType(currdoc.getItemValueString("Regulaions"));
			}
			// if ("Arizona".equalsIgnoreCase(insp.getDbDesign())) {
			if ( currdoc.hasItem("FoodSafe") ) insp.setFoodSafe(currdoc.getItemValueString("FoodSafe"));
			if ( localDebug ) HS_Util.debug("FoodSafe set to:[" + insp.getFoodSafe() + "]", "debug", dbarLoc);
			// }

			// ' Copy over manager information
			if ( currdoc.hasItem("CertifiedManager") ) insp.setCertifiedManager(currdoc.getItemValueString("CertifiedManager"));
			if ( localDebug ) HS_Util.debug("CertifiedManager set to:[" + insp.getCertifiedManager() + "]", "debug", dbarLoc);
			if ( "Certified Manager".equals(currdoc.getItemValueString("Certified")) ) {
				insp.setCertified("Certified Manager");
				if ( currdoc.hasItem("CertNumber") ) insp.setCertNumber(currdoc.getItemValueString("CertNumber"));
				if ( currdoc.hasItem("CertExpiration") ) insp.setCertExpiration(currdoc.getItemValueString("CertExpiration"));
			}
			/*
			 * ' Copy over manager information If lastinsp.HasItem("CertifiedManager") Then inspdoc.CertifiedManager = lastinsp.CertifiedManager If lastinsp.Certified(0) = "Certified Manager" Then
			 * inspdoc.Certified = "Certified Manager" If lastinsp.HasItem("CertNumber") Then inspdoc.CertNumber = lastinsp.CertNumber If lastinsp.HasItem("CertExpiration") Then inspdoc.CertExpiration =
			 * lastinsp.CertExpiration End If
			 */
			insp.setCreatedFromInspID(currdoc.getItemValueString("DocumentId"));
			result += "complete";
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			result += "ERROR:[" + e.toString() + "] ";
		}
		if ( localDebug ) {
			if ( insp.getUnid() != null ) {
				HS_Util.debug("completing routine. unid:[" + insp.getUnid() + "]", "debug", dbarLoc);
			} else {
				HS_Util.debug("completing routine. unid:[null]", "debug", dbarLoc);
			}
		}
		if ( localDebug ) HS_Util.debug("results:[" + result + "]", "debug", dbarLoc);
		return result;
	}

	public static boolean loadViolations(Inspection insp, Document idoc) {
		boolean result = false;
		final String dbarLoc = "HS_BO_Violations.loadViolations";

		try {
			insp.setViolations(loadViolationsWithDir(insp, idoc, ""));
			// Not needed if we DO NOT have a follow-up inspection type??
			if ( isFollowUp(insp) ) insp.setCorrectedViolations(loadViolationsWithDir(insp, idoc, "Corrected"));
			// insp.setPreviousViolations(getPrevViols(insp, idoc));
			result = true;
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			result = false;
		}
		return result;
	}

	public static Vector<Violation> getViolationsFromDoc(Document idoc) {
		return getViolationsFromDoc(idoc, "");
	}

	@SuppressWarnings("unchecked")
	public static Vector<Violation> getViolationsFromDoc(Document idoc, String fldprefix) {
		final String dbarLoc = "HS_BO_Violations.getViolationsFromDoc";
		final boolean localDebug = (("Yes".equals(HS_Util.getAppSettingString("debug_InspectionRead"))) | ("Yes".equals(HS_Util.getAppSettingString("debug_InspectionView"))));
		// final boolean localDebug = false;

		Vector<Violation> viols = null;
		try {
			Vector<String> violCode = (Vector<String>) (Vector<?>) (idoc.getItemValue(fldprefix + "Code"));
			int vsize = violCode.size();

			Vector<String> violCorrectedSet = loadViolItem(idoc, fldprefix + "CorrectedSet", vsize);
			Vector<String> violCorrectiveActions = loadViolItem(idoc, fldprefix + "CorrectiveActions", vsize);
			Vector<String> violCriticalSet = loadViolItem(idoc, fldprefix + "CriticalSet", vsize);
			Vector<String> violDescription = loadViolItem(idoc, fldprefix + "Description", vsize);
			Vector<String> violHzrdRating = loadViolItem(idoc, fldprefix + "HzrdRating", vsize);
			Vector<String> violLegalTxt = loadViolItem(idoc, fldprefix + "LegalTxt", vsize);
			Vector<String> violObservations = loadViolItem(idoc, fldprefix + "Observations", vsize);
			Vector<String> violRepeat = loadViolItem(idoc, fldprefix + "Repeat", vsize);
			Vector<String> violRoom = loadViolItem(idoc, fldprefix + "Room", vsize);
			Vector<String> violSection = loadViolItem(idoc, fldprefix + "Section", vsize);
			Vector<String> violStatus = loadViolItem(idoc, fldprefix + "Status", vsize);

			if ( localDebug ) {
				HS_Util.debug(fldprefix + "Code has [" + violCode.size() + "] elements.", "debug", dbarLoc);
			}

			viols = new Vector<Violation>();
			Violation viol = null;

			if ( violCode.size() > 0 ) {
				for (int i = 0; i < violCode.size(); i++) {
					viol = new Violation();
					viol.setCode(violCode.get(i));
					viol.setCorrectedSet(violCorrectedSet.get(i));
					viol.setCorrectiveAction(violCorrectiveActions.get(i));
					// String cstmp =
					// ("Critical".equals(violCriticalSet.get(i))) ? "Yes" :
					// "No";
					viol.setCriticalSet(violCriticalSet.get(i));
					viol.setDescription(violDescription.get(i));
					viol.setHzrdRating(violHzrdRating.get(i));
					viol.setLegalTxt(violLegalTxt.get(i));
					viol.setObservations(violObservations.get(i));
					viol.setRepeat(violRepeat.get(i));
					viol.setRoom(violRoom.get(i));
					viol.setSection(violSection.get(i));
					viol.setStatus(violStatus.get(i));

					// TODO Figure out the Group and Category from the snum.
					viols.add(viol);
				}
			}

			return viols;
		} catch (Exception e) {
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			HS_Util.debug(e.toString(), "error", dbarLoc);
			return viols;
		}
	}

	public static ArrayList<Violation> getPrevViols(Inspection insp, Document idoc) {
		ArrayList<Violation> result = new ArrayList<Violation>();
		final String dbarLoc = "HS_BO_Violations.getPrevViols";
		final boolean localDebug = HS_Util.isDebugServer();

		try {
			int noPreviousInsp = HS_Util.getGlobalSettingsDouble("NoPreviousInsp").intValue();
			Double checkSchdInspViolation = HS_Util.getGlobalSettingsDouble("CheckSchdInspViolation");
			String repeatNonCritical = HS_Util.getGlobalSettingsString("RepeatNonCritical");

			if ( localDebug )
				HS_Util.debug("noPreviousInsp:[" + noPreviousInsp + "] " + "checkSchdInspViolation:[" + checkSchdInspViolation + "] " + "repeatNonCritical:[" + repeatNonCritical + "]", "debug", dbarLoc);

			Document facility = idoc.getParentDatabase().getDocumentByUNID(insp.getParentUnid());
			ArrayList<String> runids = HS_GetMostRecent.routineInspectionsUNID(facility, noPreviousInsp);

			for (String runid : runids) {
				Document rinsp = idoc.getParentDatabase().getDocumentByUNID(runid);
				if ( rinsp != null ) {
					Vector<Violation> rviols = getViolationsFromDoc(idoc);
					for (Violation rviol : rviols) {
						result.add(rviol);
					}
				}
			}

			if ( localDebug ) HS_Util.debug("Returning [" + result.size() + "] previous inspections.", "debug", dbarLoc);
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			result = null;
		}
		return result;
	}

	public static Vector<Violation> loadViolationsWithDir(Inspection insp, Document idoc, String direction) {
		Vector<Violation> viols = getViolationsFromDoc(idoc, "Viol" + direction);
		return viols;
	}

	@SuppressWarnings("unchecked")
	public static boolean copyViolationsToFollowup(Inspection insp, Document currdoc) {
		final boolean localDebug = HS_Util.isDebugServer();
		;
		final String dbarLoc = "HS_BO_Violations.copyViolationsToFollowup";

		try {
			Vector<String> currViolCode = (Vector<String>) (Vector<?>) (currdoc.getItemValue("ViolCode"));
			int vsize = currViolCode.size();

			Vector<String> currViolCorrectedSet = loadViolItem(insp, currdoc, "ViolCorrectedSet", vsize);
			Vector<String> currViolCorrectiveActions = loadViolItem(insp, currdoc, "ViolCorrectiveActions", vsize);
			Vector<String> currViolCriticalSet = loadViolItem(insp, currdoc, "ViolCriticalSet", vsize);
			Vector<String> currViolDescription = loadViolItem(insp, currdoc, "ViolDescription", vsize);
			Vector<String> currViolHzrdRating = loadViolItem(insp, currdoc, "ViolHzrdRating", vsize);
			Vector<String> currViolLegalTxt = loadViolItem(insp, currdoc, "ViolLegalTxt", vsize);
			Vector<String> currViolObservations = loadViolItem(insp, currdoc, "ViolObservations", vsize);
			// Vector<String> currViolRepeat = loadViolItem(insp, currdoc,
			// "ViolRepeat", vsize);
			Vector<String> currViolSection = loadViolItem(insp, currdoc, "ViolSection", vsize);
			Vector<String> currViolStatus = loadViolItem(insp, currdoc, "ViolStatus", vsize);

			if ( localDebug ) HS_Util.debug("currViolCode has [" + currViolCode.size() + "] elements.", "debug", dbarLoc);

			// strDefaultRepeat = InspGlobalSettings.DefaultRepeat(0)
			String strDefaultRepeat = HS_Util.getGlobalSettingsString("DefaultRepeat");
			// strRepeatNonCritical = InspGlobalSettings.RepeatNonCritical(0)
			String strRepeatNonCritical = HS_Util.getGlobalSettingsString("RepeatNonCritical");
			// UseHazRat = InspGlobalSettings.UseHazardRating(0) = "Yes"
			boolean useHazRat = "Yes".equals(HS_Util.getGlobalSettingsString("UseHazardRating"));
			// InspMasterSettings.DisplayViolLegalText(0) = "Yes"
			boolean useLglTxt = "Yes".equals(HS_Util.getGlobalSettingsString("DisplayViolLegalText"));
			// InspGlobalSettings.GetItemValue("UseLegalTextOnFollowup")(0) =
			// "No"
			boolean moveCorrected = "Yes".equals(HS_Util.getGlobalSettingsString("MoveCorrected"));
			if ( "No".equalsIgnoreCase(HS_Util.getGlobalSettingsString("UseLegalTextOnFollowup")) ) useLglTxt = false;
			if ( localDebug ) HS_Util.debug("moveCorrected is:[" + moveCorrected + "]", "debug", dbarLoc);

			Vector<Violation> viols = new Vector<Violation>();
			Violation viol = null;
			String cset = "";

			if ( currViolCode.size() > 0 ) {
				for (int i = 0; i < currViolCode.size(); i++) {
					cset = currViolCorrectedSet.get(i);
					if ( moveCorrected || (!"Corrected".equalsIgnoreCase(cset)) ) {
						if ( localDebug ) HS_Util.debug("Copying violation:[" + i + "] cset:[" + cset + "] description:[" + currViolDescription.get(i) + "]", "debug", dbarLoc);
						viol = new Violation();
						viol.setCode(currViolCode.get(i));
						viol.setSection(currViolSection.get(i));
						viol.setCriticalSet(currViolCriticalSet.get(i));

						// 'Make repeat violtions on re-inspection configurable
						// viol.setRepeat(currViolRepeat.get(i));
						if ( "No".equalsIgnoreCase(strDefaultRepeat) ) {
							viol.setRepeat("");
						} else {
							if ( "No".equalsIgnoreCase(strRepeatNonCritical) ) {
								if ( "Critical".equalsIgnoreCase(currViolCriticalSet.get(i)) ) {
									viol.setRepeat("Repeat");
								} else {
									viol.setRepeat("");
								}
							} else {
								viol.setRepeat("Repeat");
							}
						}

						viol.setCorrectiveAction(currViolCorrectiveActions.get(i));
						viol.setObservations(currViolObservations.get(i));
						viol.setDescription(currViolDescription.get(i));
						viol.setCorrectedSet(currViolCorrectedSet.get(i));

						viol.setHzrdRating((useHazRat) ? currViolHzrdRating.get(i) : "");
						viol.setLegalTxt((useLglTxt) ? currViolLegalTxt.get(i) : "");

						viol.setStatus(currViolStatus.get(i));

						// TODO Figure out the Group and Category from the snum.
						viols.add(viol);
					}
				}
			}
			insp.setViolations(viols);
			if ( localDebug ) HS_Util.debug("currViolCode has [" + currViolCode.size() + "] elements. new ciols has:[" + viols.size() + "] elements.", "debug", dbarLoc);
			return true;
		} catch (Exception e) {
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			HS_Util.debug(e.toString(), "error", dbarLoc);
			return false;
		}
	}

	private static Vector<String> loadViolItem(Inspection unused, Document idoc, String iname, int vsize) {
		return loadViolItem(idoc, iname, vsize);
	}

	@SuppressWarnings("unchecked")
	private static Vector<String> loadViolItem(Document idoc, String iname, int vsize) {
		final boolean localDebug = ("Yes".equals(HS_Util.getAppSettingString("debug_InspectionRead")));
		final String dbarLoc = "HS_BO_Violations.loadViolItem";
		Vector<String> result = null;
		int actr = 0;
		try {
			result = (Vector<String>) (Vector<?>) (idoc.getItemValue(iname));
			while (result.size() < vsize) {
				result.add("");
				actr++;
			}
			if ( localDebug && actr > 0 ) HS_Util.debug("added [" + actr + "] elements to [" + iname + "]", "debug", dbarLoc);
		} catch (Exception e) {
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			HS_Util.debug(e.toString(), "error", dbarLoc);
		}
		return result;
	}

	public static boolean updateViolations(Document idoc, Inspection insp) {
		// final boolean localDebug = HS_Util.isDebugServer();
		final boolean localDebug = false;
		final String dbarLoc = "HS_BO_Violations.updateViolations";
		boolean result = false;
		if ( localDebug ) HS_Util.debug("starting", "debug", dbarLoc);

		try {
			updateViolItem(insp, idoc, "ViolationModule", insp.getViolationModule());
			updateViolItem(insp, idoc, "ViolationSubModule", insp.getViolationSubModule());
			updateViolItem(insp, idoc, "ViolationSection", insp.getViolationSection());

			countViolations(insp);
			updateViolItem(insp, idoc, "TotalViolations", insp.getTotalViolations());
			updateViolItem(insp, idoc, "NumCore", insp.getNumCore());
			updateViolItem(insp, idoc, "NumCritical", insp.getNumCritical());
			updateViolItem(insp, idoc, "NumKey", insp.getNumKey());
			updateViolItem(insp, idoc, "NumNonCritical", insp.getNumNonCritical());
			updateViolItem(insp, idoc, "NumOther", insp.getNumOther());
			updateViolItem(insp, idoc, "NumPriority", insp.getNumPriority());
			updateViolItem(insp, idoc, "NumPFoundation", insp.getNumPFoundation());
			updateViolItem(insp, idoc, "NumRepeat", insp.getNumRepeat());
			updateViolItem(insp, idoc, "NumRisk", insp.getNumRisk());
			updateViolItem(insp, idoc, "ComputedCriticals", insp.getComputedCriticals());
			result = updateViolationsWithDirection(idoc, insp, "");
			if ( result && isFollowUp(insp) ) result = updateViolationsWithDirection(idoc, insp, "Corrected");

			// Added to support the statuatory requirements
			checkRepeatBilling(insp);
			updateViolItem(insp, idoc, "RepeatC", insp.getRepeatC());
			updateViolItem(insp, idoc, "RepeatO", insp.getRepeatO());
			updateViolItem(insp, idoc, "RepeatK", insp.getRepeatK());
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			result = false;
		}
		return result;
	}

	private static boolean updateViolationsWithDirection(Document idoc, Inspection insp, String direction) {
		final boolean localDebug = false;
		final String dbarLoc = "HS_BO_Violations.updateViolationsWithDirection.[" + direction + "]";
		boolean result = false;

		if ( localDebug ) HS_Util.debug("starting", "debug", dbarLoc);
		try {
			Vector<String> violCode = new Vector<String>();
			Vector<String> violCorrectedSet = new Vector<String>();
			Vector<String> violCorrectiveActions = new Vector<String>();
			Vector<String> violCriticalSet = new Vector<String>();
			Vector<String> violDescription = new Vector<String>();
			Vector<String> violHzrdRating = new Vector<String>();
			Vector<String> violLegalTxt = new Vector<String>();
			Vector<String> violObservations = new Vector<String>();
			Vector<String> violRepeat = new Vector<String>();
			Vector<String> violRoom = new Vector<String>();
			Vector<String> violSection = new Vector<String>();
			Vector<String> violStatus = new Vector<String>();

			// String strCrit = "";
			String strCritTxlated = "";
			String strRepeat = "";
			String strRoom = "";
			boolean unvCritical = useNewViolTypes(insp, "Foodservice");
			Integer vctr = 0;
			Vector<Violation> viols = ("".equals(direction)) ? insp.getViolations() : ("Corrected".equals(direction)) ? insp.getCorrectedViolations() : null;
			if ( viols != null ) {
				for (Violation viol : viols) {
					vctr++;
					violCode.add(viol.getCode());
					violCorrectedSet.add(HS_Util.cvrtToSpace(viol.getCorrectedSet()));
					violCorrectiveActions.add(viol.getCorrectiveAction());
					strCritTxlated = translateCritical(viol.getCriticalSet(), unvCritical);

					if ( localDebug ) HS_Util.debug("unvCritical:[" + unvCritical + "] viol.getCriticalSet():[" + viol.getCriticalSet() + "] strCritTxlated:[" + strCritTxlated + "]", "debug", dbarLoc);
					// if ( insp.isTabletDebug() ) System.out.print(dbarLoc + " - unvCritical:[" + unvCritical + "] viol.getCriticalSet():[" + viol.getCriticalSet() + "] strCritTxlated:[" + strCritTxlated +
					// "]");
					if ( insp.isTabletDebug() ) System.out.print(dbarLoc + " - Viol[" + vctr.toString() + "] - CorrectiveAction:[" + viol.getCorrectiveAction() + "] ");

					// violCriticalSet.add(HS_Util.cvrtToSpace(strCrit));
					violCriticalSet.add(HS_Util.cvrtToSpace(strCritTxlated));
					violDescription.add(viol.getDescription());
					violHzrdRating.add(viol.getHzrdRating());
					violLegalTxt.add(viol.getLegalTxt());
					violObservations.add(viol.getObservations());
					strRepeat = viol.getRepeat();
					strRepeat = ("yes".equalsIgnoreCase(strRepeat)) ? "Repeat" : "";
					violRepeat.add(strRepeat);
					strRoom = (viol.getRoom() == null) ? " " : ("".equals(viol.getRoom().trim())) ? " " : viol.getRoom();
					violRoom.add(strRoom);
					violSection.add(viol.getSection());
					violStatus.add(HS_Util.cvrtToSpace(viol.getStatus()));
				}
			}

			updateViolItem(insp, idoc, "Viol" + direction + "Code", violCode);
			updateViolItem(insp, idoc, "Viol" + direction + "CorrectedSet", violCorrectedSet);
			updateViolItem(insp, idoc, "Viol" + direction + "CorrectiveActions", violCorrectiveActions);
			if ( insp.isTabletDebug() ) System.out.print(dbarLoc + " - Saved CorrectiveActions:" + violCorrectiveActions.toString() + "");
			updateViolItem(insp, idoc, "Viol" + direction + "CriticalSet", violCriticalSet);
			updateViolItem(insp, idoc, "Viol" + direction + "Description", violDescription);
			updateViolItem(insp, idoc, "Viol" + direction + "HzrdRating", violHzrdRating);
			updateViolItem(insp, idoc, "Viol" + direction + "LegalTxt", lookupLegalTextAgain(insp, violCode, violLegalTxt, true));
			updateViolItem(insp, idoc, "Viol" + direction + "Observations", violObservations);
			updateViolItem(insp, idoc, "Viol" + direction + "Repeat", violRepeat);
			updateViolItem(insp, idoc, "Viol" + direction + "Room", violRoom);
			updateViolItem(insp, idoc, "Viol" + direction + "Section", violSection);
			updateViolItem(insp, idoc, "Viol" + direction + "Status", violStatus);
			if ( localDebug ) HS_Util.debug("done", "debug", dbarLoc);
			result = true;
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}
		return result;
	}

	public static Vector<String> lookupLegalTextAgain(Inspection insp, Vector<String> srcCodes, Vector<String> srcLegalText) {
		return lookupLegalTextAgain(insp, srcCodes, srcLegalText, false);
	}

	public static Vector<String> lookupLegalTextAgain(Inspection insp, Vector<String> srcCodes, Vector<String> srcLegalText, boolean readDoc) {
		final boolean localDebug = false;
		final String dbarLoc = "HS_BO_Violations.lookupLegalTextAgain";
		Vector<String> results = new Vector<String>();
		String result = "";
		try {
			String vmodule = insp.getViolationModule().get(0);
			HS_database hsdb = new HS_database();
			Database eidb = hsdb.getdb("eiRoot");
			View oView = eidb.getView("XPagesObservationsByModule");
			oView.setAutoUpdate(false);
			ViewEntryCollection vec = oView.getAllEntriesByKey(vmodule, true);
			Document odoc = null;
			String srcCode = "";
			for (int i = 0; i < srcCodes.size(); i++) {
				srcCode = srcCodes.get(i);
				result = (srcLegalText.size() >= i) ? "" : srcLegalText.get(i);
				for (ViewEntry onve : vec) {
					if ( srcCode.equals(onve.getColumnValue("Code")) ) {
						if ( readDoc ) {
							odoc = onve.getDocument();
							result = odoc.getItemValueString("LegalObservationText");
						} else {
							result = (String) onve.getColumnValue("LegalObservationTextComputed");
						}
						break;
					}
				}
				results.add(result);
				if ( localDebug ) HS_Util.debug("for code:[" + srcCode + "] newLegalText:[" + result + "]", "debug", dbarLoc);
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			results.add("ERROR in " + dbarLoc + ": " + e.toString());
		}
		return results;
	}

	private static boolean updateViolItem(Inspection insp, Document idoc, String itemName, Object value) {
		final boolean localDebug = false;
		final String dbarLoc = "HS_BO_Violations.updateViolItem";
		boolean result = false;
		try {
			if ( value == null ) value = "";
			idoc.replaceItemValue(itemName, value);
			if ( localDebug ) HS_Util.debug("item:[" + itemName + "] updated OK", "debug", dbarLoc);
			result = true;
		} catch (Exception e) {
			HS_Util.debug(e.toString() + " on item:[" + itemName + "]", "error", dbarLoc);
		}

		return result;
	}

	private static int countViolations(Inspection insp) {
		final boolean localDebug = false;
		final String dbarLoc = "HS_BO_Violations.countViolations";
		int core = 0;
		int crit = 0;
		int key = 0;
		int noncrit = 0;
		int other = 0;
		int pfound = 0;
		int priority = 0;
		int repeat = 0;
		int risk = 0;
		int total = 0;

		boolean unv = useNewViolTypes(insp, "Foodservice");
		String cset = "";

		if ( localDebug )
			HS_Util.debug("Starting:[" + core + "][" + crit + "][" + key + "][" + noncrit + "][" + other + "][" + pfound + "][" + priority + "][" + repeat + "][" + risk + "][" + total + "] unv:[" + unv + "]",
									"debug", dbarLoc);
		if ( localDebug ) HS_Util.debug("There are [" + insp.getViolations().size() + "] violations.", "debug", dbarLoc);
		Iterator<Violation> vitr = insp.getViolations().iterator();
		Violation viol = null;
		while (vitr.hasNext()) {
			viol = vitr.next();
			if ( localDebug ) HS_Util.debug("Violation with status of [" + viol.getStatus() + "] being evaluated. total:[" + total + "] critical:[" + viol.getCriticalSet() + "]", "debug", dbarLoc);
			if ( "Out".equals(viol.getStatus()) ) {
				total += 1;
				cset = viol.getCriticalSet().toLowerCase();
				if ( !unv && ("yes".equals(cset) || "critical".equals(cset)) ) {
					crit += 1;
				} else if ( !unv && ("no".equals(cset) || "".equals(cset)) ) {
					noncrit += 1;
				} else if ( unv && ("priority item".equals(cset) || "yes".equals(cset)) ) {
					priority += 1;
				} else if ( unv && ("priority foundation item".equals(cset) || "potential".equals(cset)) ) {
					pfound += 1;
				} else if ( unv && ("core item".equals(cset) || "no".equals(cset)) ) {
					core += 1;
				} else {
					other += 1;
				}
				repeat += ("Repeat".equalsIgnoreCase(viol.getRepeat())) ? 1 : ("yes".equalsIgnoreCase(viol.getRepeat())) ? 1 : 0;
			}
			// if (localDebug) HS_Util.debug("Violation with status of [" +
			// viol.getStatus() + "] has code of [" + viol.getCode() + "]",
			// "debug", dbarLoc);
		}
		insp.setTotalViolations(total);
		insp.setNumCritical(crit);
		insp.setNumNonCritical(noncrit);
		insp.setNumRepeat(repeat);

		// TODO Not sure how these are counted.
		insp.setNumCore(core);
		insp.setNumKey(key);
		insp.setNumOther(other);
		insp.setNumPFoundation(pfound);
		insp.setNumPriority(priority);
		insp.setNumRisk(risk);

		if ( localDebug )
			HS_Util.debug("Finished:[" + core + "][" + crit + "][" + key + "][" + noncrit + "][" + other + "][" + pfound + "][" + priority + "][" + repeat + "][" + risk + "][" + total + "] unv:[" + unv + "]",
									"debug", dbarLoc);

		return insp.getTotalViolations();
	}

	private static boolean useNewViolTypes(Inspection insp, String establishmentType) {
		boolean result = false;
		final boolean localDebug = false;
		final String dbarLoc = "HS_BO_Violations.useNewViolTypes";

		try {

			/*
			 * UseCodes := @GetProfileField( "MasterSettings"; "FoodCode2009Districts" ); CodeStartDate := @GetProfileField( "MasterSettings"; "FoodCode2009Date" ); IsAfterDate := @If(
			 * 
			 * @IsTime( CodeStartDate ); @If( InspectionDate >= CodeStartDate;
			 * 
			 * @True; @False ); @False );
			 * 
			 * @If( EstablishmentType = "Foodservice" & UseCodes = "Yes" & IsAfterDate;
			 * 
			 * @True;
			 * 
			 * @False )
			 */
			Date inspectionDate = insp.getInspectionDate();
			String useCodes = HS_Util.getMasterSettingsString("FoodCode2009Districts");

			Object codeStartDate = HS_Util.getMasterSettingsObject("FoodCode2009Date");
			if ( "".equals(establishmentType) ) establishmentType = "Foodservice";
			boolean isAfterDate = ((codeStartDate.getClass().getName()).indexOf("DateTime") == -1) ? false : (inspectionDate.getTime() >= ((DateTime) codeStartDate).toJavaDate().getTime()) ? true : false;
			if ( localDebug )
				HS_Util.debug("useCodes:[" + useCodes + "]" + " codeStartDate:[" + codeStartDate + "] " + " inspectionDate:[" + inspectionDate + "] " + " establishmentType:[" + establishmentType + "] "
										+ " isAfterDate:[" + isAfterDate + "]", "debug", dbarLoc);

			result = ("Foodservice".equals(establishmentType) && "Yes".equals(useCodes) && isAfterDate);

			return result;
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			return true;
		}
	}

	public static boolean isFollowUp(Inspection insp) {
		// final boolean localDebug = HS_Util.isDebugger();
		final boolean localDebug = false;
		final String dbarLoc = "HS_BO_Violations.isFollowUp";
		boolean result = false;
		try {
			// @IsNotMember(Type; @GetProfileField("MasterSettings";
			// "InspectionFollowupTypes")) | violCode = ""
			Vector<String> futypes = HS_Util.getMasterSettingsVector("InspectionFollowupTypes");
			if ( futypes == null ) {
				result = false;
				HS_Util.debug("MasterSettings.InspectionFollowupTypes is null!", "warn", dbarLoc);
			} else {
				result = futypes.contains(insp.getInspType());
				// if (!result) {
				// if (insp.getInspType().toLowerCase().indexOf("follow-up") >
				// -1) result = true;
				// }
				if ( localDebug ) HS_Util.debug("futypes are:" + futypes.toString() + " InspType:[" + insp.getInspType() + "] result:[" + result + "]", "debug", dbarLoc);
			}
			return result;
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			return false;
		}
	}

	public static boolean checkRepeatBilling(Inspection insp) {
		final boolean localDebug = true;
		final String msgContext = "HS_BO_Violations.checkRepeatBilling";

		boolean result = false;

		boolean unvCritical = useNewViolTypes(insp, "Foodservice");
		Database eidb = null;
		View inspView = null;
		DocumentCollection inspections = null;
		// DocumentCollection inspCollection = null;
		int criticalCount = 0;
		int keyCount = 0;
		int otherCount = 0;
		int violationCounter = 0;
		Vector<String> vcodes = null;
		Vector<String> vrepeats = null;
		Vector<String> vcritset = null;

		Vector<String> inspunids = new Vector<String>();
		Map<String, Vector<String>> vcodesMap = new HashMap<String, Vector<String>>();
		Map<String, Vector<String>> vrepeatsMap = new HashMap<String, Vector<String>>();
		Map<String, Vector<String>> vcritsetMap = new HashMap<String, Vector<String>>();

		Vector<String> repeatC = new Vector<String>();
		Vector<String> repeatK = new Vector<String>();
		Vector<String> repeatO = new Vector<String>();
		Date thisDate = null;
		Date lastDate = null;
		String thisForm = "";
		try {
			if ( localDebug ) System.out.print(msgContext + " - Starting with parent UNID:[" + insp.getParentUnid() + "] for DBDesign:[" + insp.getDbDesign() + "]");
			if ( "".equals(insp.getParentUnid()) ) return result;

			repeatC.add("");
			repeatK.add("");
			repeatO.add("");

			insp.setRepeatC(repeatC);
			insp.setRepeatK(repeatK);
			insp.setRepeatO(repeatO);

			if ( !("TDA".equalsIgnoreCase(insp.getDbDesign()) || "Florida".equalsIgnoreCase(insp.getDbDesign())) ) return result;

			repeatC = new Vector<String>();
			repeatK = new Vector<String>();
			repeatO = new Vector<String>();

			thisDate = insp.getInspectionDate();
			thisForm = insp.getForm();

			// Iterate through inspections looking at routine inspections older than current inspection
			eidb = new HS_database().getdb("eiRoot");
			inspView = eidb.getView("InspectionsParents");
			if ( inspView == null ) return result;
			// inspections = inspView.getAllEntriesByKey(insp.getParentUnid(), true);
			inspections = inspView.getAllDocumentsByKey(insp.getParentUnid(), true);
			if ( localDebug ) System.out.print(msgContext + " - found[" + inspections.getCount() + "] responses docs to check out!");
			for (Document cInspection : inspections) {
				if ( localDebug ) System.out.print(msgContext + " - checking doc. Form:[" + cInspection.getFormName() + "] ViewDescription:[" + cInspection.getItemValueString("ViewDescription") + "]");
				if ( thisForm.equals(cInspection.getFormName()) ) {
					// insp.setInspectionDate(HS_Util.loadValueDate(idoc, "InspectionDate"));
					lastDate = HS_Util.loadValueDate(cInspection, "InspectionDate");
					if ( lastDate.getTime() < thisDate.getTime() ) {
						vcodes = HS_Util.loadValueStringArray(cInspection, "ViolCode");
						vrepeats = HS_Util.loadValueStringArray(cInspection, "ViolRepeat");
						vcritset = HS_Util.loadValueStringArray(cInspection, "ViolCriticalSet");
						inspunids.add(cInspection.getUniversalID());
						vcodesMap.put(cInspection.getUniversalID(), vcodes);
						vrepeatsMap.put(cInspection.getUniversalID(), vrepeats);
						vcritsetMap.put(cInspection.getUniversalID(), vcritset);

						// if ( inspCollection == null ) inspCollection = eidb.search("Form=\"NeverEverFindAnything\"", null, 0);
						// inspCollection.add(cInspection);
					}
				}
			}
			// if ( inspCollection == null ) return result;
			// if ( inspCollection.getCount() == 0 ) return result;
			if ( inspunids.size() == 0 ) return result;

			if ( localDebug ) System.out.print(msgContext + " - found[" + inspunids.size() + "] inspections to process!");
			for (Violation curViol : insp.getViolations()) {
				String curCode = curViol.getCode();
				String curCritical = translateCritical(curViol.getCriticalSet(), unvCritical);

				// if ( localDebug )
				// System.out.print(msgContext + " looking at violation code:[" + curCode + "] critical:[" + curCritical + "] repeat:[" + curViol.getRepeat()
				// + "]");
				criticalCount = 0;
				keyCount = 0;
				otherCount = 0;
				if ( "Repeat".equalsIgnoreCase(curViol.getRepeat()) || "Yes".equalsIgnoreCase(curViol.getRepeat()) ) {
					if ( "Critical".equalsIgnoreCase(curCritical) || "Priority Item".equals(curCritical) ) criticalCount = criticalCount + 1;
					if ( "Key".equalsIgnoreCase(curCritical) || "Core Item".equals(curCritical) ) keyCount = keyCount + 1;
					if ( "Other".equalsIgnoreCase(curCritical) || "Priority Foundation Item".equals(curCritical) ) otherCount = otherCount + 1;
				}
				// for (Document wrkInspection : inspCollection) {
				Iterator<String> iuniditr = inspunids.iterator();
				while (iuniditr.hasNext()) {
					String iunid = iuniditr.next();
					vcodes = vcodesMap.get(iunid);
					vrepeats = vrepeatsMap.get(iunid);
					vcritset = vcritsetMap.get(iunid);
					if ( vcodes != null ) {
						if ( vcodes.contains(curCode) ) {
							violationCounter = vcodes.indexOf(curCode);
							if ( "Repeat".equalsIgnoreCase(vrepeats.get(violationCounter)) || "Yes".equalsIgnoreCase(vrepeats.get(violationCounter)) ) {
								String wrkcset = vcritset.get(violationCounter);
								if ( "Critical".equalsIgnoreCase(wrkcset) || "Priority Item".equalsIgnoreCase(wrkcset) ) criticalCount = criticalCount + 1;
								if ( "Key".equalsIgnoreCase(wrkcset) || "Core Item".equalsIgnoreCase(wrkcset) ) keyCount = keyCount + 1;
								if ( "Other".equalsIgnoreCase(wrkcset) || "Priority Foundation Item".equalsIgnoreCase(wrkcset) ) otherCount = otherCount + 1;
							}
						}
					}
				}

				String outstr = "";
				if ( "Key".equalsIgnoreCase(curCritical) && keyCount > 0 ) {
					outstr = "" + keyCount + "X Key Repeat of Violation Code " + curCode + " = ";
					outstr += (keyCount == 1) ? "$100" : (keyCount == 2) ? "$200" : (keyCount == 3) ? "Dealer Suspend 7 days " : "Dealer revoked";
					repeatK.add(outstr);
				}
				if ( "Core Item".equalsIgnoreCase(curCritical) && keyCount > 0 ) {
					outstr = "" + keyCount + "X Core Repeat of Violation Code " + curCode;
					repeatK.add(outstr);
				}
				if ( " ".equalsIgnoreCase(curCritical) && keyCount > 0 ) {
					outstr = "" + keyCount + "X Potential Repeat of Violation Code " + curCode;
					repeatK.add(outstr);
				}
				if ( "Other".equalsIgnoreCase(curCritical) && otherCount > 0 ) {
					outstr = "" + otherCount + "X Other Repeat of Violation Code " + curCode + " = ";
					outstr += (otherCount == 1) ? "$25" : (otherCount == 2) ? "$50" : (otherCount == 3) ? "Dealer Suspend 7 days " : (otherCount == 4) ? "Dealer Suspend 14 days " : "Dealer revoked";
					repeatO.add(outstr);
				}
				if ( "Priority Foundation Item".equalsIgnoreCase(curCritical) && otherCount > 0 ) {
					outstr = "" + otherCount + "X Priority Foundation Repeat of Violation Code " + curCode;
					repeatO.add(outstr);
				}
				if ( "Critical".equalsIgnoreCase(curCritical) && criticalCount > 0 ) {
					outstr = "" + criticalCount + "X Critical Repeat of Violation Code " + curCode + " = ";
					outstr += (criticalCount == 1) ? "$500" : (criticalCount == 2) ? "$1000" : (criticalCount == 3) ? "Dealer Suspend 7 days " : "Dealer revoked";
					repeatC.add(outstr);
				}
				if ( "Priority Item".equalsIgnoreCase(curCritical) && criticalCount > 0 ) {
					outstr = "" + criticalCount + "X Priority Repeat of Violation Code " + curCode;
					repeatC.add(outstr);
				}
				if ( localDebug ) System.out.print(msgContext + " curCritical:[" + curCritical + "] outstr:[" + outstr + "]");
			} // for(Violation curViol : insp.getViolations()) {
			if ( repeatC.size() > 0 ) insp.setRepeatC(repeatC);
			if ( repeatK.size() > 0 ) insp.setRepeatK(repeatK);
			if ( repeatO.size() > 0 ) insp.setRepeatO(repeatO);

		} catch (Exception e) {
			System.out.print(msgContext + " ERROR: " + e.toString());
			if ( localDebug ) e.printStackTrace();
		}
		return result;
	}

	private static String translateCritical(String strCritSrc, boolean unvCritical) {
		String result = "";
		if ( unvCritical ) {
			result = ("yes".equalsIgnoreCase(strCritSrc)) ? "Priority Item" : ("no".equalsIgnoreCase(strCritSrc)) ? "Core Item" : ("Potential".equalsIgnoreCase(strCritSrc)) ? "Priority Foundation Item"
									: strCritSrc;
		} else {
			result = ("yes".equalsIgnoreCase(strCritSrc)) ? "Critical" : ("no".equalsIgnoreCase(strCritSrc)) ? "" : strCritSrc;
		}
		return result;
	}

	/*
	 * ViolationFee - Converted from LS. Used to send notices or create new fees when an inspection has been savedand there are repeats that were thracked by the CheckRepeatBilling method...
	 */
	@SuppressWarnings("unchecked")
	public static boolean violationFee(Inspection insp) throws Exception {
		final boolean localDebug = true;
		final String msgContext = "HS_BO_Violations.violationFee";

		boolean result = false;
		if ( !("TDA".equalsIgnoreCase(insp.getDbDesign()) || "Florida".equalsIgnoreCase(insp.getDbDesign())) ) return result;

		Database eidb = null;
		Document facdoc = null;
		Map<String, Object> allfeesMap = null;
		String sError = "";

		String notifyhow = HS_Util.getProfileString("BillingSettings", "SpecificFeeAuto");
		Vector<String> notifyWho = HS_Util.getProfileVector("BillingSettings", "SpecificFeesEmail");

		if ( "".equals(notifyhow) ) return result;
		if ( !("Email".equalsIgnoreCase(notifyhow)) ) throw (new Exception("Invaild value for \"SpecificFeeAuto\" in Billing Settings: [" + notifyhow + "]"));

		try {
			if ( localDebug )
				System.out.print(msgContext + " - Starting with for DBDesign:[" + insp.getDbDesign() + "] Sizes: C:[" + insp.getRepeatC().size() + "] K:[" + insp.getRepeatK().size() + "]- O:["
										+ insp.getRepeatO().size() + "].");
			if ( "".equals(insp.getParentUnid()) ) return result;
			if ( (insp.getRepeatC().size() == 0) && (insp.getRepeatK().size() == 0) && (insp.getRepeatO().size() == 0) ) return result;
			if ( ("".equals(insp.getRepeatC().get(0))) && ("".equals(insp.getRepeatK().get(0))) && ("".equals(insp.getRepeatO().get(0))) ) return result;

			eidb = new HS_database().getdb("eiRoot");
			facdoc = eidb.getDocumentByUNID(insp.getParentUnid());
			if ( facdoc == null ) return result;

			allfeesMap = new HashMap<String, Object>();
			sError = HS_BO_Billing.getFees(facdoc, insp.getInspType(), "Violation", allfeesMap);
			if ( !"".equals(sError) ) throw (new Exception("getFees error: " + sError));
			if ( allfeesMap == null ) {
				if ( localDebug ) System.out.print(msgContext + " -  allfeesMap collection came back as null!");
				return result;
			}

			if ( localDebug ) System.out.print(msgContext + " - allfees has [" + allfeesMap.size() + "] entires.");

			for (Entry<String, Object> feemapNtry : allfeesMap.entrySet()) {
				Object feemapObj = feemapNtry.getValue();
				if ( feemapObj instanceof Map ) {
					Map<String, Object> feemap = (Map<String, Object>) feemapObj;
					String feeCode = ((Vector<String>) feemap.get("feecode")).get(0);
					if ( localDebug ) System.out.print(msgContext + " - found feemap.FeeCode is:[" + feeCode + "]");
					for (String rptC : insp.getRepeatC()) {
						if ( rptC.startsWith(feeCode) ) {
							if ( "Email".equalsIgnoreCase(notifyhow) ) violationFeeEmail(facdoc, insp, rptC, notifyWho);
						}
					}
					for (String rptK : insp.getRepeatK()) {
						if ( rptK.startsWith(feeCode) ) {
							if ( "Email".equalsIgnoreCase(notifyhow) ) violationFeeEmail(facdoc, insp, rptK, notifyWho);
						}
					}
					for (String rptO : insp.getRepeatO()) {
						if ( rptO.startsWith(feeCode) ) {
							if ( "Email".equalsIgnoreCase(notifyhow) ) violationFeeEmail(facdoc, insp, rptO, notifyWho);
						}
					}
				}

			}
		} catch (Exception e) {
			System.out.print(msgContext + " ERROR: " + e.toString());
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}

		return result;
	}

	private static void violationFeeEmail(Document facdoc, Inspection insp, String subject, Vector<String> notifyWho) {
		final boolean localDebug = true;
		final String msgContext = "HS_BO_Violations.violationFeeEmail";
		try {
			Database db = facdoc.getParentDatabase();
			Document memo = db.createDocument();
			memo.replaceItemValue("Form", "Memo");
			memo.replaceItemValue("Subject", subject);
			RichTextItem rtitem = memo.createRichTextItem("Body");
			rtitem.appendText("Facility name: " + facdoc.getItemValueString("Name") + " - " + facdoc.getItemValueString("DocumentID"));
			rtitem.addNewLine(1);
			rtitem.appendText("Assigned to: " + facdoc.getItemValueString("EHO"));
			rtitem.addNewLine(1);
			Date wdt = ((DateTime) facdoc.getItemValue("LastInspection").get(0)).toJavaDate();
			rtitem.appendText("Last inspection date: " + HS_Util.formatDate(wdt, "mm/dd/yyyy"));
			if ( localDebug ) {
				rtitem.addNewLine(2);
				rtitem.appendText("Produced from REST Services using " + msgContext);
				rtitem.addNewLine(1);
				rtitem.appendText("DB: " + db.getTitle() + "  (" + db.getServer() + "!!" + db.getFilePath());
				System.out.print(msgContext + " : Sending email to:[" + notifyWho.toString() + "] for:[" + subject + "]");
			}
			if ( HS_Util.sendEmail(memo, false, notifyWho, false) ) {
				// throw (new Exception("EMAIL SENT BUT AddToFacilityAuditTrail not implemented..."));
				HS_BO_Billing.addToFacilityAuditTrail(insp, facdoc, notifyWho.toString(), "Civil Penalty");
				// ViolationfeeEmail = True
			} else {
				// throw (new Exception("EMAIL NOT AND AddToFacilityAuditTrail not implemented..."));
				// sError = "Email notification failed to send to " + notifyWho
				// Call AddToFacilityAuditTrail(inspDoc, sError, "Civil Penalty error")
				HS_BO_Billing.addToFacilityAuditTrail(insp, facdoc, "Email notification failed to send to " + notifyWho.toString(), "Civil Penalty error");
			}
		} catch (Exception e) {
			System.out.print(msgContext + " ERROR: " + e.toString());
			if ( HS_Util.isRestDebugServer() || HS_Util.isDebugServer() ) e.printStackTrace();
			JSFUtil.getViewScope().put("savedErrorMessage", msgContext + " ERROR: " + e.toString());
			JSFUtil.getViewScope().put("savedStackTrace", e.getStackTrace());
			HS_Util.sendErrorToSupport();
		}
	}

}
