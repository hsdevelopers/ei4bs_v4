package com.healthspace.general;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import javax.faces.context.FacesContext;

import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.Name;
import org.openntf.domino.Session;
import org.openntf.domino.View;
import org.openntf.domino.ViewEntry;
import org.openntf.domino.ViewEntryCollection;

import com.ibm.jscript.InterpretException;
import com.ibm.jscript.std.ObjectObject;
import com.ibm.jscript.types.FBSUtility;
import com.ibm.jscript.types.FBSValue;
import com.ibm.xsp.designer.context.XSPContext;
import com.ibm.xsp.extlib.util.ExtLibUtil;

/*
 * HS_Databasev2 IS INTENDED TO BE IMPLEMENTED AS A MANAGED BEAN
 * 
 * Here is the XML for the faces-config.xml file:

 <managed-bean id="hs_Databasev2">
 <managed-bean-name>hs_Databasev2</managed-bean-name>
 <managed-bean-class>com.healthspace.general.HS_databasev2</managed-bean-class>
 <managed-bean-scope>application</managed-bean-scope>
 </managed-bean>

 * 
 * Changes:
 * v2 - 6 Nov 2012 - converted to work with org.openntf.domino api
 */

public class HS_database implements Serializable {
	private static final long	serialVersionUID		= 1L;

	private final String		hscc_scopeKey			= "HS_ConfigCenterDatabase";
	private final String		hseir_scopeKey1			= "HS_EiRootDatabase";
	private final String		hsdb_scopeKey			= "HS_DatabaseLinksData";
	private final String		hsdbProfile_scopeKey	= "dbProfiles";
	private final String		dbLookupViewName		= "DBLookupByName";
	private final String		xpDbLookupViewName		= "XPagesDBLookupByName";
	private final String		hsdb_configStatus		= "HS_ConfigStatus";
	private Date				debugStartDt			= null;

	private ArrayList<String>	missingDbs;
	private String				lastServer;
	private String				lastPath;
	private String				lastReplId;

	private boolean				useSigner;

	public HS_database() {
		useSigner = false;
	}

	@SuppressWarnings("unchecked")
	public Database getdb() {
		Database retdb = null;
		final boolean localDebug = false;
		final String dbarLoc = "HS_database.getdb()";
		String dbkey = "";
		try {
			if ( localDebug ) HS_Util.debug(dbarLoc + " checking database with no key");
			// ObjectObject dbProfiles = (ObjectObject) ExtLibUtil
			// .getApplicationScope().get(hsdbProfile_SkopeKey);
			Vector<ObjectObject> dbProfiles = (Vector<ObjectObject>) ExtLibUtil.getApplicationScope().get(hsdbProfile_scopeKey);
			if ( dbProfiles == null ) {
				if ( localDebug ) HS_Util.debug(dbarLoc + " - dbProfiles is null.");
				dbProfiles = buildDbProfiles();
				// return null;
			}
			dbkey = dbProfiles.firstElement().get("key").stringValue();
			if ( localDebug ) HS_Util.debug(dbarLoc + " using the key: " + dbkey);

			retdb = this.getdb(dbkey);
		} catch (Exception e) {
			System.out.print(dbarLoc + " exception : " + e.toString());
			System.out.print("*************************************************************************************************************");
			HS_Util.debug("Error accessing DB with key:[" + dbkey + "] " + e.toString(), "error", dbarLoc);
		} finally {

		}
		return retdb;
	}

	public Database getdbAsSigner(String dbkey) {
		useSigner = true;
		return getdb(dbkey);
	}

	public Database getdb(String dbkey) {
		Database retdb = null;
		// final boolean localDebug = false;
		final boolean localDebug = ("Yes".equals(HS_Util.getAppSettingString("debug_HSDatabase")));
		final String dbarLoc = "HS_database.getdb(\"" + dbkey + "\")";
		try {
			if ( localDebug ) HS_Util.debug("Checking database with key: [" + dbkey + "]", "debug", dbarLoc);
			if ( "eiRoot".equalsIgnoreCase(dbkey) ) {
				retdb = this.getEiRootDb();
				if ( retdb != null ) return retdb;
			}
			if ( "eiWeb".equalsIgnoreCase(dbkey) ) {
				retdb = this.getEiWebDb();
				if ( retdb != null ) return retdb;
			}
			if ( "configCenter".equalsIgnoreCase(dbkey) ) {
				if ( "ignore".equals(HS_Util.getAppSettingString("ConfigCenter")) ) {
					if ( !ExtLibUtil.getApplicationScope().containsKey("ignoreConfigCenterMsg") ) {
						if ( localDebug ) HS_Util.debug("ignoring config center database 0", "warn", dbarLoc);
						ExtLibUtil.getApplicationScope().put("ignoreConfigCenterMsg", "ignoring config center database 0");
					}
					return null;
				}
				retdb = this.getConfigCenterDb();
				if ( retdb != null ) return retdb;

			}

			ObjectObject dbProfile = getDbProfile(dbkey);
			if ( dbProfile == null ) {
				if ( getMissingDbs().contains(dbkey) ) return null;
				getMissingDbs().add(dbkey);
				if ( "support".equals(dbkey) ) return null; // Ignore the error
				if ( "designForum".equals(dbkey) ) return null; // Ignore the error
				throw (new Exception("Unable to find database profile for key: " + dbkey));
			}
			String dbname = dbProfile.get("name").stringValue();
			String path = dbProfile.get("path").stringValue();
			String replId = dbProfile.get("replId").stringValue();
			String server = dbProfile.get("server").stringValue();

			if ( "".equals(path) ) {
				if ( "support".equals(dbkey) ) return null; // Ignore the error
				if ( "designForum".equals(dbkey) ) return null; // Ignore the error
				throw (new Exception("Unable to find database config data for key: " + dbkey));
			}
			Session sess = this.getSession();
			Database currdb = sess.getCurrentDatabase();

			if ( currdb.getServer().equalsIgnoreCase(server) && currdb.getFilePath().equalsIgnoreCase(path) ) {
				retdb = currdb;
			}
			if ( retdb == null ) {
				if ( localDebug ) HS_Util.debug("Not local DB, trying to open it at [" + currdb.getServer() + "!!" + path + "]", "debug", dbarLoc);
				retdb = sess.getDatabase(currdb.getServer(), path);
				if ( retdb != null ) {
					if ( !retdb.isOpen() ) {
						retdb = null;
					}
				}
			}
			if ( retdb == null ) {
				if ( localDebug ) HS_Util.debug("Not found at [" + currdb.getServer() + "!!" + path + "]", "debug", dbarLoc);
				retdb = sess.getDatabase("", "");
				retdb.openByReplicaID(currdb.getServer(), replId);
				if ( !retdb.isOpen() ) {
					retdb = null;
				}
			}
			if ( retdb == null ) {
				if ( localDebug ) HS_Util.debug("not found at [" + currdb.getServer() + "!!" + replId + "]", "debug", dbarLoc);
				retdb = sess.getDatabase(server, path);
				if ( retdb != null ) {
					if ( !retdb.isOpen() ) {
						retdb = null;
					}
				}
			}
			if ( retdb != null ) {
				if ( localDebug ) HS_Util.debug("found " + dbname + " at [" + retdb.getServer() + "!!" + retdb.getFilePath() + "]", "debug", dbarLoc);
				if ( retdb.getCurrentAccessLevel() == 0 ) {
					HS_Util.debug("Acces Level in database " + dbkey + " is NO ACCESS [" + retdb.getServer() + "!!" + retdb.getFilePath() + "]", "error", dbarLoc);
					retdb = null;
				}
				return retdb;
			}
		} catch (Exception e) {
			// System.out.print(dbarLoc + " - exception: " + e.toString());
			HS_Util.debug("Error accessing database [" + dbkey + "]: " + e.toString(), "error", dbarLoc);
		} finally {

		}
		return retdb;
	}

	public String getdbBang(String dbkey) {
		String bangStr = "";
		String server = "";
		String path = "";
		Name sname = null;
		ObjectObject dbProfile = null;
		Database wrkdb = null;

		final boolean localDebug = false;
		final String dbarLoc = "HS_database.getdbBang(\"" + dbkey + "\")";
		try {
			if ( localDebug ) HS_Util.debug("Checking database with key: [" + dbkey + "]", "debug", dbarLoc);
			if ( "eiRoot".equalsIgnoreCase(dbkey) ) {
				dbProfile = (ObjectObject) ExtLibUtil.getSessionScope().get(hseir_scopeKey1);
				if ( dbProfile == null ) {
					wrkdb = getdb(dbkey);
					if ( wrkdb != null ) {
						sname = HS_Util.getSession().createName(wrkdb.getServer());
						bangStr = sname.getAbbreviated() + "!!" + wrkdb.getFilePath();
					}
				} else {
					sname = HS_Util.getSession().createName(dbProfile.get("Server").stringValue());
					bangStr = sname.getAbbreviated() + "!!" + dbProfile.get("FileName").stringValue();
				}
			} else if ( "eiWeb".equalsIgnoreCase(dbkey) ) {
				dbProfile = (ObjectObject) ExtLibUtil.getSessionScope().get(hseir_scopeKey1);
				if ( dbProfile == null ) {
					wrkdb = getdb(dbkey);
					if ( wrkdb != null ) {
						sname = HS_Util.getSession().createName(wrkdb.getServer());
						bangStr = sname.getAbbreviated() + "!!" + wrkdb.getFilePath();
					}
				} else {
					sname = HS_Util.getSession().createName(HS_Util.getSession().getServerName());
					bangStr = sname.getAbbreviated() + "!!" + dbProfile.get("WebsitePath").stringValue();
				}
			} else if ( "configCenter".equalsIgnoreCase(dbkey) ) {
				dbProfile = (ObjectObject) ExtLibUtil.getSessionScope().get(hscc_scopeKey);
				if ( dbProfile == null ) {
					wrkdb = getdb(dbkey);
					if ( wrkdb != null ) {
						sname = HS_Util.getSession().createName(wrkdb.getServer());
						bangStr = sname.getAbbreviated() + "!!" + wrkdb.getFilePath();
					}
				} else {
					sname = HS_Util.getSession().createName(dbProfile.get("server").stringValue());
					bangStr = sname.getAbbreviated() + "!!" + dbProfile.get("path").stringValue();
				}
			}
			if ( "".equals(bangStr) ) {
				dbProfile = getDbProfile(dbkey);
				if ( dbProfile == null ) {
					if ( getMissingDbs().contains(dbkey) ) return "";
					getMissingDbs().add(dbkey);
					if ( "support".equals(dbkey) ) return null; // Ignore the error
					if ( "designForum".equals(dbkey) ) return null; // Ignore the error
					throw (new Exception("Unable to find database profile for key: " + dbkey));
				}
				path = dbProfile.get("path").stringValue();
				server = dbProfile.get("server").stringValue();
				sname = HS_Util.getSession().createName(server);
				bangStr = sname.getAbbreviated() + "!!" + path;
			}
		} catch (Exception e) {
			// System.out.print(dbarLoc + " - exception: " + e.toString());
			HS_Util.debug("Exception: " + e.toString(), "error", dbarLoc);
		} finally {

		}
		return bangStr;
	}

	@SuppressWarnings("unchecked")
	private ObjectObject getDbProfile(String dbkey) {
		ObjectObject dbProfile = null;
		final boolean localDebug = false;
		final String dbarLoc = "HS_database.getDbProfile(\"" + dbkey + "\")";
		try {
			if ( localDebug ) HS_Util.debug("Checking database with key: [" + dbkey + "]", "debug", dbarLoc);
			Vector<ObjectObject> dbProfiles = (Vector<ObjectObject>) ExtLibUtil.getApplicationScope().get(hsdbProfile_scopeKey);
			if ( dbProfiles == null ) {
				if ( localDebug ) HS_Util.debug("dbProfiles is null.", "debug", dbarLoc);
				dbProfiles = buildDbProfiles();
			}
			if ( localDebug ) HS_Util.debug("dbProfiles is: " + dbProfiles.getClass().getName(), "debug", dbarLoc);

			Iterator<ObjectObject> itr = dbProfiles.iterator();

			String wdbkey = "";

			FBSValue localFBS = null;
			while (itr.hasNext()) {
				dbProfile = itr.next();
				localFBS = dbProfile.get("key");
				wdbkey = localFBS.stringValue();
				if ( dbkey.equalsIgnoreCase(wdbkey) ) {
					return dbProfile;
				}
			}
		} catch (Exception e) {
			HS_Util.debug("Exception: " + e.toString(), "error", dbarLoc);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private Database getEiRootDb() {
		Database retdb = null;
		final boolean localDebug = ("Yes".equals(HS_Util.getAppSettingString("debug_HSDatabase")));
		final String dbarLoc = "HS_database.getEiRootdb";
		debugStartDt = new Date();

		try {
			Session session = this.getSession();
			ObjectObject sdata = (ObjectObject) ExtLibUtil.getSessionScope().get(hseir_scopeKey1);
			if ( sdata != null ) {
				String suser = sdata.get("User").stringValue();
				if ( session.getEffectiveUserName().equalsIgnoreCase(suser) ) {
					lastServer = getCCServer(sdata.get("Server").stringValue());
					lastPath = sdata.get("FileName").stringValue();
					lastReplId = sdata.get("ReplicaID").stringValue();
					retdb = openDb(lastPath, lastReplId, lastServer);
					if ( retdb == null ) {
						HS_Util.debug("failed to access eiRoot database (1).", "error", dbarLoc);
					}
					if ( localDebug ) {
						HS_Util.debug("got " + hseir_scopeKey1 + " for user: " + suser + " server!!path: [" + lastServer + "!!" + lastPath + "]" + debugShowMSecs(), "debug", dbarLoc);
					}
					return retdb;
				}
				// remove the database links data so it gets rebuilt.
				ExtLibUtil.getSessionScope().remove(hsdb_scopeKey);
				if ( localDebug ) HS_Util.debug("Scoped user: [" + suser + "] is different from effective user.[" + session.getEffectiveUserName() + "]", "debug", dbarLoc);
			}
			Vector<ObjectObject> linksData = (Vector<ObjectObject>) ExtLibUtil.getSessionScope().get(hsdb_scopeKey);
			if ( linksData == null ) {
				if ( "ignore".equals(HS_Util.getAppSettingString("ConfigCenter")) ) {
					if ( !ExtLibUtil.getApplicationScope().containsKey("ignoreConfigCenterMsg") ) {
						if ( localDebug ) HS_Util.debug("ignoring config center database 1", "warn", dbarLoc);
						ExtLibUtil.getApplicationScope().put("ignoreConfigCenterMsg", "ignoring config center database 1");
					}
					return this.getEiRootdbNoConfig();
				}
				if ( localDebug ) HS_Util.debug("sessionScope." + hsdb_scopeKey + "  is null.", "debug", dbarLoc);
				linksData = buildLinksData();
			}
			return this.getEiRootdbNoConfig();
			// IGNORE ALL THIS LinksData stuff. It does not really work really..

			// if ( linksData == null ) {
			// if ( localDebug ) HS_Util.debug("sessionScope." + hsdb_scopeKey +
			// " is null after build....", "warn", dbarLoc);
			// return null;
			// }
			// if ( linksData.size() == 0 ) {
			// if ( localDebug ) HS_Util.debug("sessionScope." + hsdb_scopeKey +
			// "  is empty. Attempting rebuild..", "debug", dbarLoc);
			// linksData = buildLinksData();
			// }
			// if ( linksData.size() == 0 ) {
			// if ( localDebug ) HS_Util.debug("sessionScope." + hsdb_scopeKey +
			// "  has no entries.", "warn", dbarLoc);
			// return null;
			// }
			// ObjectObject dbl = null;
			// if ( linksData.size() == 1 ) {
			// dbl = linksData.get(0);
			// lastPath = dbl.get("FileName").stringValue();
			// lastReplId = dbl.get("ReplicaID").stringValue();
			// lastServer = getCCServer();
			// retdb = openDb(lastPath, lastReplId, lastServer);
			// if ( retdb == null ) {
			// HS_Util.debug("failed to access eiRoot database (2).", "error",
			// dbarLoc);
			// if ( localDebug )
			// HS_Util.debug("LinksData has one entry but was not openned.. [" +
			// dbl.get("Title").stringValue() + "]", "debug", dbarLoc);
			// ExtLibUtil.getSessionScope().remove(hseir_scopeKey1);
			// return retdb;
			// } else if ( retdb.getCurrentAccessLevel() == 0 ) {
			// HS_Util.debug("Access Level in eiRoot database is NO ACCESS [" +
			// retdb.getServer() + "!!" + retdb.getFilePath() + "]", "error",
			// dbarLoc);
			// retdb = null;
			// return null;
			// } else {
			// if ( localDebug ) HS_Util.debug("LinksData has one entry. [" +
			// retdb.getTitle() + "]", "debug", dbarLoc);
			// }
			// sdata = new ObjectObject();
			// sdata.put("Title",
			// FBSUtility.wrap(dbl.get("Title").stringValue()));
			// sdata.put("FileName",
			// FBSUtility.wrap(dbl.get("FileName").stringValue()));
			// sdata.put("Server", FBSUtility.wrap(retdb.getServer()));
			// sdata.put("ReplicaID",
			// FBSUtility.wrap(dbl.get("ReplicaID").stringValue()));
			// sdata.put("WebsiteID",
			// FBSUtility.wrap(dbl.get("WebsiteID").stringValue()));
			// sdata.put("WebsitePath",
			// FBSUtility.wrap(dbl.get("WebsitePath").stringValue()));
			// sdata.put("User",
			// FBSUtility.wrap(session.getEffectiveUserName()));
			// ExtLibUtil.getSessionScope().put(hseir_scopeKey1, sdata);
			// if ( localDebug )
			// HS_Util.debug("Added scoped variable for user: " +
			// sdata.get("User").stringValue() + debugShowMSecs(), "debug",
			// dbarLoc);
			// return retdb;
			// }
			// if ( session.getEffectiveUserName().contains("Henry Newberry") )
			// {
			// dbl = null;
			// for (int ni = 0; ni < linksData.size(); ni++) {
			// String dName =
			// linksData.get(ni).get("DistrictName").stringValue();
			// if ( dName.contains("Hamilton County") ) {
			// dbl = linksData.get(ni);
			// if ( localDebug ) HS_Util.debug("Assigning ni #" + ni +
			// " because it is newbs.", "debug", dbarLoc);
			// }
			// }
			// if ( dbl == null ) dbl = linksData.get(0);
			// } else {
			// dbl = linksData.get(0);
			// }
			// lastPath = dbl.get("FileName").stringValue();
			// lastReplId = dbl.get("ReplicaID").stringValue();
			// lastServer = getCCServer();
			// retdb = openDb(lastPath, lastReplId, lastServer);
			// if ( retdb == null ) {
			// HS_Util.debug("failed to access eiRoot database (3).", "error",
			// dbarLoc);
			// return retdb;
			// } else if ( retdb.getCurrentAccessLevel() == 0 ) {
			// HS_Util.debug("Acces Level in eiRoot database is NO ACCESS [" +
			// retdb.getServer() + "!!" + retdb.getFilePath() + "]", "error",
			// dbarLoc);
			// retdb = null;
			// return retdb;
			// }
			// if ( localDebug )
			// HS_Util.debug("linksData has [" + linksData.size() +
			// "] entries. using first one: [" + retdb.getTitle() + "]" +
			// debugShowMSecs(), "debug",
			// dbarLoc);
			// sdata = new ObjectObject();
			// sdata.put("Title",
			// FBSUtility.wrap(dbl.get("Title").stringValue()));
			// sdata.put("FileName",
			// FBSUtility.wrap(dbl.get("FileName").stringValue()));
			// sdata.put("Server", FBSUtility.wrap(retdb.getServer()));
			// sdata.put("WebsiteID",
			// FBSUtility.wrap(dbl.get("WebsiteID").stringValue()));
			// sdata.put("WebsitePath",
			// FBSUtility.wrap(dbl.get("WebsitePath").stringValue()));
			//
			// sdata.put("User",
			// FBSUtility.wrap(session.getEffectiveUserName()));
			// ExtLibUtil.getSessionScope().put(hseir_scopeKey1, sdata);
			// if ( localDebug )
			// HS_Util.debug("Added scoped variable for user: " +
			// sdata.get("User").stringValue() + debugShowMSecs(), "debug",
			// dbarLoc);
			// return retdb;

		} catch (Exception e) {
			// System.out.print(dbarLoc + " - exception: " + e.toString());
			HS_Util.debug("Exception: " + e.toString(), "error", dbarLoc);
		}
		return retdb;
	}

	@SuppressWarnings("unchecked")
	private Database getEiRootdbNoConfig() {
		Database retdb = null;
		final boolean localDebug = ("Yes".equals(HS_Util.getAppSettingString("debug_HSDatabase")));
		;
		final String dbarLoc = "HS_database.getEiRootdbNoConfig";
		debugStartDt = new Date();
		String host = "";
		ObjectObject eiProfile = null;
		try {
			host = XSPContext.getXSPContext(FacesContext.getCurrentInstance()).getUrl().getHost();
			if ( localDebug ) HS_Util.debug("looking for eiRoot with host:[" + host + "]", "debug", dbarLoc, debugStartDt);
			Vector<ObjectObject> dbProfiles = (Vector<ObjectObject>) ExtLibUtil.getApplicationScope().get(hsdbProfile_scopeKey);
			if ( dbProfiles == null ) dbProfiles = this.buildDbProfiles();

			for (ObjectObject dbProfile : dbProfiles) {
				if ( "eiRoot".equalsIgnoreCase(dbProfile.get("key").stringValue()) ) {
					if ( host.equalsIgnoreCase(dbProfile.get("host").stringValue()) ) {
						eiProfile = dbProfile;
					}
				}
			}
			// If there is no host match then return null and it will use the
			// first one...
			if ( eiProfile == null ) {
				if ( localDebug ) HS_Util.debug("No matching eiroot host found.", "debug", dbarLoc, debugStartDt);
				return null;
			}
			if ( localDebug ) HS_Util.debug("Matching eiroot host found:[" + eiProfile.get("name").stringValue() + "]", "debug", dbarLoc, debugStartDt);
			lastPath = eiProfile.get("path").stringValue();
			lastReplId = eiProfile.get("replId").stringValue();
			lastServer = getCCServer(eiProfile.get("server").stringValue());
			retdb = openDb(lastPath, lastReplId, lastServer);
			if ( retdb == null ) {
				HS_Util.debug("failed to access eiRoot database (2).", "error", dbarLoc);
				ExtLibUtil.getSessionScope().remove(hseir_scopeKey1);
				return retdb;
			} else if ( retdb.getCurrentAccessLevel() == 0 ) {
				HS_Util.debug("Acces Level in eiRoot database is NO ACCESS [" + retdb.getServer() + "!!" + retdb.getFilePath() + "]", "error", dbarLoc);
				retdb = null;
				return null;
			} else {
				if ( localDebug ) HS_Util.debug("Got database with title:[" + retdb.getTitle() + "]", "debug", dbarLoc, debugStartDt);
			}
			ObjectObject sdata = new ObjectObject();
			sdata.put("Title", FBSUtility.wrap(eiProfile.get("name").stringValue()));
			sdata.put("FileName", FBSUtility.wrap(eiProfile.get("path").stringValue()));
			sdata.put("Server", FBSUtility.wrap(retdb.getServer()));
			sdata.put("ReplicaID", FBSUtility.wrap(eiProfile.get("replId").stringValue()));
			sdata.put("WebsiteID", FBSUtility.wrap(""));
			sdata.put("WebsitePath", FBSUtility.wrap(""));
			sdata.put("User", FBSUtility.wrap(HS_Util.getSession().getEffectiveUserName()));
			ExtLibUtil.getSessionScope().put(hseir_scopeKey1, sdata);
			if ( localDebug ) HS_Util.debug("Added scoped variable for user: " + sdata.get("User").stringValue() + debugShowMSecs(), "debug", dbarLoc);
			return retdb;
		} catch (Exception e) {
			// System.out.print(dbarLoc + " - exception: " + e.toString());
			HS_Util.debug("Exception: " + e.toString(), "error", dbarLoc, debugStartDt);
		}
		return retdb;
	}

	private Database getEiWebDb() {
		Database retdb = null;
		// final boolean localDebug = false;;
		final String dbarLoc = "HS_database.getEiWebDb";
		try {
			Session session = this.getSession();
			ObjectObject sdata = (ObjectObject) ExtLibUtil.getSessionScope().get(hseir_scopeKey1);
			if ( sdata == null ) {
				// Open root to get scope stuff setup
				retdb = getEiRootDb();
				retdb = null;
				sdata = (ObjectObject) ExtLibUtil.getSessionScope().get(hseir_scopeKey1);
			}
			String suser = sdata.get("User").stringValue();
			if ( !(session.getEffectiveUserName().equalsIgnoreCase(suser)) ) {
				// Open root to get scope stuff setup
				retdb = getEiRootDb();
				retdb = null;
				sdata = (ObjectObject) ExtLibUtil.getSessionScope().get(hseir_scopeKey1);
			}
			lastPath = sdata.get("WebsitePath").stringValue();
			if ( "".equals(lastPath) ) lastPath = "reallybadfilenameandpath.nsf";
			lastReplId = sdata.get("WebsiteID").stringValue();
			retdb = openDb(lastPath, lastReplId);
			if ( retdb == null ) {
				HS_Util.debug("failed to access eiWebDb database.", "error", dbarLoc);
			} else if ( retdb.getCurrentAccessLevel() == 0 ) {
				HS_Util.debug("Acces Level in eiRoot database is NO ACCESS [" + retdb.getServer() + "!!" + retdb.getFilePath() + "]", "error", dbarLoc);
				retdb = null;
			}
			return retdb;
		} catch (Exception e) {
			// System.out.print(dbarLoc + " - exception: " + e.toString());
			HS_Util.debug(dbarLoc + " - exception: " + e.toString(), "error");
		}
		return retdb;
	}

	@SuppressWarnings("unchecked")
	private Database getConfigCenterDb() {
		Database retdb = null;
		final boolean localDebug = false;
		final String dbarLoc = "HS_database.getConfigCenterDb";
		debugStartDt = new Date();
		try {
			// Session session = this.getSession();
			if ( "ignore".equals(HS_Util.getAppSettingString("ConfigCenter")) ) {
				return null;
			}
			ObjectObject ccProfile = (ObjectObject) ExtLibUtil.getSessionScope().get(hscc_scopeKey);
			boolean rebuild = (ccProfile == null);
			if ( !rebuild ) {
				if ( localDebug ) HS_Util.debug("ccProfile.User: [" + ccProfile.get("User").stringValue() + "]", "debug", dbarLoc);
				if ( !(ccProfile.get("User").stringValue().equalsIgnoreCase(this.getSession().getEffectiveUserName())) ) {
					rebuild = true;
					ExtLibUtil.getSessionScope().remove(hsdbProfile_scopeKey);
					if ( localDebug ) HS_Util.debug("SessionScope." + hscc_scopeKey + " found for wrong user looking it up.", "debug", dbarLoc);
				}
			} else {
				if ( localDebug ) HS_Util.debug("SessionScope." + hscc_scopeKey + " not found looking it up.", "debug", dbarLoc);

			}
			if ( rebuild ) {
				Vector<ObjectObject> dbProfiles = (Vector<ObjectObject>) ExtLibUtil.getApplicationScope().get(hsdbProfile_scopeKey);
				if ( dbProfiles == null ) {
					if ( localDebug ) HS_Util.debug("applicatioScope.dbProfiles is null.", "debug", dbarLoc);
					dbProfiles = buildDbProfiles();
					if ( dbProfiles == null ) {
						HS_Util.debug("Unable to build database Profiles", "error", dbarLoc);
						return null;
					}
				}
				for (ObjectObject dbProfile : dbProfiles) {
					if ( "configCenter".equalsIgnoreCase(dbProfile.get("key").stringValue()) ) {
						ccProfile = dbProfile;
					}
				}
				if ( ccProfile == null ) {
					HS_Util.debug("Unable to find configCenter database profile", "warn", dbarLoc);
					return null;
				}
				ccProfile.put("User", FBSUtility.wrap(this.getSession().getEffectiveUserName()));
				ExtLibUtil.getSessionScope().put(hscc_scopeKey, ccProfile);
			} else {
				if ( localDebug ) HS_Util.debug("SessionScope." + hscc_scopeKey + " found for current user.", "debug", dbarLoc);
			}
			lastServer = getCCServer();
			lastPath = ccProfile.get("path").stringValue();
			lastReplId = ccProfile.get("replId").stringValue();

			if ( "".equals(lastPath) ) lastPath = "reallybadfilenameandpath.nsf";
			lastServer = ccProfile.get("server").stringValue();
			if ( localDebug ) HS_Util.debug("Attempting open of configCenter on server:[" + lastServer + "] filepath:[" + lastPath + "] replid:[" + lastReplId + "]" + debugShowMSecs(), "debug", dbarLoc);
			retdb = openDb(lastPath, lastReplId, lastServer);
			if ( retdb == null ) {
				HS_Util.debug("failed to access eiConfigCenterDb database.", "error", dbarLoc);
			} else if ( retdb.getCurrentAccessLevel() == 0 ) {
				HS_Util.debug("Acces Level in Config Center database is NO ACCESS [" + retdb.getServer() + "!!" + retdb.getFilePath() + "]", "error", dbarLoc);
				retdb = null;
			}
			return retdb;
		} catch (Exception e) {
			// System.out.print(dbarLoc + " - exception: " + e.toString());
			HS_Util.debug(dbarLoc + " - exception: " + e.toString(), "error");
		}
		return retdb;
	}

	private Database openDb(String filepath, String replid) {
		Database retdb = null;
		// final boolean localDebug = false;
		final String dbarLoc = "HS_database.openDb";
		try {
			lastServer = ExtLibUtil.getCurrentDatabase().getServer();
			retdb = openDb(filepath, replid, lastServer);
		} catch (Exception e) {
			HS_Util.debug(dbarLoc + " - exception: " + e.toString(), "error");
		}
		return retdb;
	}

	private Database openDb(String filepath, String replid, String server) {
		Database retdb = null;
		final boolean localDebug = false;
		final String dbarLoc = "HS_database.openDb(2)";

		try {
			Session session = this.getSession();
			try {
				retdb = session.getDatabase(server, filepath);
			} catch (Exception e1) {
				retdb = null;
			}
			if ( retdb == null ) {
				if ( localDebug ) HS_Util.debug("db could not open with [" + server + "!!" + filepath + "] by user: [" + session.getEffectiveUserName() + "]", "warn", dbarLoc);
				// if (localDebug)
				// HS_Util.debug("failed to open with server/filepath, trying replid: ["
				// + replid + "]", "debug", dbarLoc);
				try {
					retdb = session.getDatabase("", "");
					retdb.openByReplicaID(server, replid);
				} catch (Exception e) {
					HS_Util.debug("db could not open using path [" + server + "!!" + filepath + "] or replid: [" + replid + "] by user: [" + session.getEffectiveUserName() + "]", "error", dbarLoc);
					retdb = null;
				}
			}
		} catch (Exception e) {
			HS_Util.debug("exception: opening [" + server + "!!" + filepath + "] replid: [" + replid + "] : " + e.toString(), "error", dbarLoc);
			retdb = null;
		}
		return retdb;
	}

	public Vector<ObjectObject> buildLinksData() throws Exception {

		final boolean localDebug = false;
		final String dbarLoc = "HS_database.buildLinksData";

		Vector<ObjectObject> retLinks = new Vector<ObjectObject>();
		Database cdb = null;
		View linksView = null;
		ViewEntryCollection linkEntries = null;
		// ViewEntry linkEntry = null;
		// ViewEntry nEntry = null;

		Document linkDoc = null;
		boolean thrownAnError = false;
		// Document nxtDoc = null;

		try {
			Name dbUser = HS_Util.getSession().createName(HS_Util.getSession().getEffectiveUserName());
			if ( localDebug ) HS_Util.debug("dbUser is:" + dbUser.getAbbreviated() + " [" + HS_Util.getSession().getEffectiveUserName() + "]", "debug", dbarLoc);
			boolean allAccess = false;
			if ( allAccess || "Office".equalsIgnoreCase(dbUser.getOrgUnit1()) ) {
				allAccess = true;
			}
			if ( allAccess || "Service".equalsIgnoreCase(dbUser.getOrgUnit1()) ) {
				allAccess = true;
			}
			if ( allAccess || ("Ohio".equalsIgnoreCase(dbUser.getOrgUnit1()) && "ODH".equalsIgnoreCase(dbUser.getOrgUnit2())) ) {
				allAccess = true;
			}
			if ( localDebug ) HS_Util.debug("allAccess is [" + allAccess + "] dbUserOu1:[" + dbUser.getOrgUnit1() + "] dbUserOu2:[" + dbUser.getOrgUnit2() + "]", "debug", dbarLoc);

			/* Change this to accesss the alternate database */
			// cdb = ExtLibUtil.getCurrentDatabase();
			// cdb = getdb("configCenter");
			if ( "ignore".equals(HS_Util.getAppSettingString("ConfigCenter")) ) {
				if ( !ExtLibUtil.getApplicationScope().containsKey("ignoreConfigCenterMsg") ) {
					HS_Util.debug("ignoring config center database 2", "warn", dbarLoc);
					ExtLibUtil.getApplicationScope().put("ignoreConfigCenterMsg", "ignoring config center database 2");
				}
				return null;
			} else {
				ExtLibUtil.getSessionScope().put(hsdb_configStatus, "-");
				cdb = this.getConfigCenterDb();
				if ( cdb == null ) {
					ExtLibUtil.getSessionScope().put(hsdb_configStatus, "Cannot access config center database");
					throw (new Exception("Cannot access config center database"));
				}
				if ( localDebug ) HS_Util.debug("Building LinksData from db: " + cdb.getTitle() + " on server: [" + cdb.getServer() + "]", "debug", dbarLoc);
				ObjectObject db = null;

				if ( localDebug ) HS_Util.debug("Building dbProfiles", "debug", dbarLoc);

				linksView = cdb.getView(xpDbLookupViewName);
				if ( linksView == null ) {
					linksView = cdb.getView(dbLookupViewName);
				}
				if ( linksView == null ) {
					ExtLibUtil.getSessionScope().put(
											hsdb_configStatus,
											"Cannot access required view \"" + dbLookupViewName + "\" or \"" + xpDbLookupViewName + "\" in db: \"" + cdb.getTitle() + "\" server:[" + cdb.getServer()
																	+ "] path:[" + cdb.getFilePath() + "]");
					throw (new Exception("Cannot access required view \"" + dbLookupViewName + "\" or \"" + xpDbLookupViewName + "\" in db: \"" + cdb.getTitle() + "\" server:[" + cdb.getServer() + "] path:["
											+ cdb.getFilePath() + "]"));
				}
				linksView.setAutoUpdate(false);
				linkEntries = linksView.getAllEntries();
				if ( linkEntries == null ) {
					HS_Util.debug(linksView.getName() + ".getAllEntries() returned null.", "error", dbarLoc);
					ExtLibUtil.getSessionScope().put(hsdb_configStatus, linksView.getName() + ".getAllEntries() returned null. [" + cdb.getServer() + "!!" + cdb.getFilePath() + "]");
					db = new ObjectObject();
					db.put("DistrictName", FBSUtility.wrap("dbView.getAllEntries() returned null."));
					db.put("Source", FBSUtility.wrap("dbView.getTopLevelEntryCount() :[" + linksView.getTopLevelEntryCount() + "]"));
					db.put("OrgUnit", FBSUtility.wrap("server: [" + cdb.getServer() + "]"));
					db.put("LHDID", FBSUtility.wrap("path: [" + cdb.getFilePath() + "]"));
					db.put("Title", FBSUtility.wrap("View name is: [" + linksView.getName() + "]."));
					db.put("Server", FBSUtility.wrap(""));
					db.put("FileName", FBSUtility.wrap(""));
					db.put("ReplicaID", FBSUtility.wrap(""));
					db.put("url", FBSUtility.wrap(""));
					db.put("WebsiteID", FBSUtility.wrap(""));
					db.put("WebsitePath", FBSUtility.wrap(""));
					retLinks.addElement(db);
				} else {
					if ( linkEntries.getCount() == 0 ) {
						HS_Util.debug(linksView.getName() + ".getAllEntries() returned no entries. [" + cdb.getServer() + "!!" + cdb.getFilePath() + "]", "error", dbarLoc);
						ExtLibUtil.getSessionScope().put(hsdb_configStatus, linksView.getName() + ".getAllEntries() returned has no entries. [" + cdb.getServer() + "!!" + cdb.getFilePath() + "]");
						db = new ObjectObject();
						db.put("DistrictName", FBSUtility.wrap("There are no database link docs."));
						db.put("Source", FBSUtility.wrap(""));
						db.put("OrgUnit", FBSUtility.wrap("server: [" + cdb.getServer() + "]"));
						db.put("LHDID", FBSUtility.wrap("path: [" + cdb.getFilePath() + "]"));
						db.put("Title", FBSUtility.wrap("There are no database link docs in view named: [" + linksView.getName() + "]."));
						db.put("Server", FBSUtility.wrap(""));
						db.put("FileName", FBSUtility.wrap(""));
						db.put("ReplicaID", FBSUtility.wrap(""));
						db.put("url", FBSUtility.wrap(""));
						db.put("WebsiteID", FBSUtility.wrap(""));
						db.put("WebsitePath", FBSUtility.wrap(""));
						retLinks.addElement(db);
					} else {
						if ( localDebug ) HS_Util.debug("processing [" + linkEntries.getCount() + "] entries from view:[" + linksView.getName() + "]", "debug", dbarLoc);
						Name srvrName = HS_Util.getSession().createName(cdb.getServer());
						for (ViewEntry linkEntry : linkEntries) {
							if ( linkEntry.isConflict() ) {
								// ignore conflict documents
							} else {
								Vector<Object> cvals = linkEntry.getColumnValues();

								Name tuser = HS_Util.getSession().createName("John Doe/" + cvals.get(2));
								/*
								 * The decision process here is:
								 * 
								 * 1. Is this database record setup as a LIVE database - (If not, excluded it)
								 * 
								 * 2. Is this an AllAccess user - (If Yes, include it)
								 * 
								 * 3. Does the OrgUnit1 and OrgUnit2 match the test user (if Yes include it)
								 * 
								 * 4. Is the user Anoymous and the array empty )if yes, include it)
								 * 
								 * LAST DECISION IS WRONG, NEEDS TO LOOK AT HOST NAME
								 */

								boolean isLiveDb = ("Yes".equalsIgnoreCase(HS_Util.getColValAsString(cvals.get(5))));

								boolean ou1Match = (tuser.getOrgUnit1().equalsIgnoreCase(dbUser.getOrgUnit1()));
								boolean ou2Match = (tuser.getOrgUnit2().equalsIgnoreCase(dbUser.getOrgUnit2()));
								boolean isAnonDb = ("Anonymous".equals(dbUser.getCommon()) && retLinks.isEmpty());
								if ( isLiveDb && (allAccess || (ou1Match && ou2Match) || isAnonDb) ) {
									String turl = "notes://" + srvrName.getCommon() + "/" + (HS_Util.getColValAsString(cvals.get(7))).replace("\\", "/");
									linkDoc = linkEntry.getDocument();
									db = new ObjectObject();

									db.put("DistrictName", FBSUtility.wrap(HS_Util.getColValAsString(cvals.get(0))));
									db.put("Source", FBSUtility.wrap(HS_Util.getColValAsString(cvals.get(1))));
									db.put("OrgUnit", FBSUtility.wrap(HS_Util.getColValAsString(cvals.get(2))));
									db.put("LHDID", FBSUtility.wrap(HS_Util.getColValAsString(cvals.get(3))));
									db.put("Title", FBSUtility.wrap(HS_Util.getColValAsString(cvals.get(4))));
									db.put("Live", FBSUtility.wrap(HS_Util.getColValAsString(cvals.get(5))));
									db.put("Server", FBSUtility.wrap(HS_Util.getColValAsString(cvals.get(6))));
									db.put("FileName", FBSUtility.wrap(HS_Util.getColValAsString(cvals.get(7))));
									db.put("ReplicaID", FBSUtility.wrap(HS_Util.getColValAsString(cvals.get(8))));
									db.put("url", FBSUtility.wrap(turl));
									if ( cvals.size() > 9 ) {
										db.put("WebsiteID", FBSUtility.wrap(HS_Util.getColValAsString(cvals.get(9))));
									} else {
										db.put("WebsiteID", FBSUtility.wrap(linkDoc.getItemValueString("WebsiteID")));
									}
									if ( cvals.size() > 10 ) {
										db.put("WebsitePath", FBSUtility.wrap(HS_Util.getColValAsString(cvals.get(10))));
									} else {
										db.put("WebsitePath", FBSUtility.wrap(linkDoc.getItemValueString("WebsitePath")));
									}
									retLinks.addElement(db);
								} else {
									if ( localDebug )
										HS_Util.debug("Entry skipped: isLiveDb:[" + isLiveDb + "] allAccess:[" + allAccess + "] ou1Match:[" + ou1Match + "] ou2Match:[" + ou2Match + "] isAnonDb:[" + isAnonDb
																+ "]", "debug", dbarLoc);
								}
							}
						}
					}

				}
				Map<String, Object> sscope = ExtLibUtil.getSessionScope();
				sscope.put(hsdb_scopeKey, retLinks);
				if ( localDebug ) HS_Util.debug("updated SessionScope." + hsdb_scopeKey + " with [" + retLinks.size() + "] entries ", "debug", dbarLoc);
			}
		} catch (Exception e) {
			// System.out.print(dbarLoc + " - exception: " + e.toString());
			HS_Util.debug("Exception: " + e.toString(), "error", dbarLoc);
			if ( HS_Util.isDebugServer() && !thrownAnError ) e.printStackTrace();
			thrownAnError = true;
		}
		return retLinks;
	}

	private Vector<ObjectObject> buildDbProfiles() {
		final boolean localDebug = false;
		final String dbarLoc = "HS_database.buildDbProfiles()";

		Database cdb = null;
		View dbview = null;
		Document dbDoc = null;
		Document nxtDoc = null;
		Vector<ObjectObject> dbs = new Vector<ObjectObject>();

		// added to test USClusterMate access to USClusterMate2
		Boolean uscmFlag = false;
		String srvr = "";

		try {
			cdb = HS_Util.getSession().getCurrentDatabase();
			// added to test USClusterMate access to USClusterMate2
			if ( cdb.getServer().equalsIgnoreCase("cn=usclustermate/o=healthspace") ) uscmFlag = true;

			ObjectObject db = null;

			if ( localDebug ) HS_Util.debug(dbarLoc + " - Building dbProfiles");

			dbview = cdb.getView("HS_DbConfigurations");
			if ( dbview == null ) throw (new Exception("Cannot access required view \"HS_DbConfigurations\""));
			dbDoc = dbview.getFirstDocument();
			while (dbDoc != null) {
				nxtDoc = dbview.getNextDocument(dbDoc);
				db = new ObjectObject();
				db.put("key", FBSUtility.wrap(dbDoc.getItemValueString("dbKey")));
				db.put("name", FBSUtility.wrap(dbDoc.getItemValueString("dbName")));

				// added to test USClusterMate access to USClusterMate2
				srvr = dbDoc.getItemValueString("dbServer");
				if ( uscmFlag && srvr.equalsIgnoreCase("cn=quail/o=healthspace") ) srvr = "CN=USClusterMate2/O=HealthSpace";
				db.put("server", FBSUtility.wrap(srvr));

				db.put("path", FBSUtility.wrap(dbDoc.getItemValueString("dbPath")));
				db.put("replId", FBSUtility.wrap(dbDoc.getItemValueString("dbReplId")));
				db.put("host", FBSUtility.wrap(dbDoc.getItemValueString("dbHost")));

				if ( localDebug ) HS_Util.debug(dbarLoc + " - Adding database profile for " + db.get("key").stringValue() + " - " + dbDoc.getItemValueString("dbKey"));

				dbs.addElement(db);
				dbDoc = nxtDoc;
			}
			Map<String, Object> ascope = ExtLibUtil.getApplicationScope();
			ascope.put(hsdbProfile_scopeKey, dbs);

		} catch (Exception e) {
			HS_Util.debug("ERROR: " + e.toString(), "error", dbarLoc);
		}
		return dbs;
	}

	private String getCCServer() {
		String server = HS_Util.getSession().getServerName();
		return getCCServer(server);
	}

	private String getCCServer(String dfltServer) {
		String server = dfltServer;

		ObjectObject ccProfile = (ObjectObject) ExtLibUtil.getSessionScope().get(hscc_scopeKey);
		if ( ccProfile != null ) {
			try {
				server = ccProfile.get("server").stringValue();
			} catch (InterpretException e) {
				// do nothing let it default.
			}
		}
		return server;
	}

	private Session getSession() throws Exception {
		if ( useSigner ) return HS_Util.getSessionAsSigner();
		return HS_Util.getSession();
	}

	public Date getDebugStartDt() {
		return debugStartDt;
	}

	public void setDebugStartDt(Date debugStartDt) {
		this.debugStartDt = debugStartDt;
	}

	public String getLastServer() {
		if ( lastServer == null ) lastServer = "";
		return lastServer;
	}

	public void setLastServer(String lastServer) {
		this.lastServer = lastServer;
	}

	public String getLastPath() {
		if ( lastPath == null ) lastPath = "";
		return lastPath;
	}

	public void setLastPath(String lastPath) {
		this.lastPath = lastPath;
	}

	public String getLastReplId() {
		if ( lastReplId == null ) lastReplId = "";
		return lastReplId;
	}

	public void setLastReplId(String lastReplId) {
		this.lastReplId = lastReplId;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getHscc_scopeKey() {
		return hscc_scopeKey;
	}

	public String getHseir_scopeKey1() {
		return hseir_scopeKey1;
	}

	public String getHsdb_scopeKey() {
		return hsdb_scopeKey;
	}

	public String getHsdbProfile_SkopeKey() {
		return hsdbProfile_scopeKey;
	}

	public String getHsdbProfile_scopeKey() {
		return hsdbProfile_scopeKey;
	}

	public String getDbLookupViewName() {
		return dbLookupViewName;
	}

	public String getXpDbLookupViewName() {
		return xpDbLookupViewName;
	}

	public String getHsdb_configStatus() {
		return hsdb_configStatus;
	}

	public boolean isUseSigner() {
		return useSigner;
	}

	public void setUseSigner(boolean useSigner) {
		this.useSigner = useSigner;
	}

	public ArrayList<String> getMissingDbs() {
		if ( missingDbs == null ) missingDbs = new ArrayList<String>();
		return missingDbs;
	}

	public void setMissingDbs(ArrayList<String> missingDbs) {
		this.missingDbs = missingDbs;
	}

	private String debugShowMSecs() {
		if ( debugStartDt == null ) return "";
		String msecs = " took:[";
		Date endDt = new Date();
		msecs += endDt.getTime() - debugStartDt.getTime();
		msecs += "] msecs.";
		return msecs;
	}
}
