package com.healthspace.general;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

import org.openntf.domino.Document;
import org.openntf.domino.Stream;
import org.openntf.domino.email.DominoEmail;
import org.openntf.domino.email.EmailAttachment;

import com.healthspace.tools.PDFGenerator;
import com.ibm.xsp.extlib.util.ExtLibUtil;

public class HS_EmailInspection implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HS_EmailInspection() {
	}

	@SuppressWarnings( { "unchecked", "deprecation" })
	public static boolean sendInspectionAsPdf(boolean saveEmailDoc) {
		boolean result = false;
		final String msgContext = "HS_EmailInspection.sendInspectionAsPdf";
		final boolean localDebug = true;

		Map<String, Object> vscope = ExtLibUtil.getViewScope();
		Map<String, Object> sscope = ExtLibUtil.getSessionScope();
		try {

			// String[] msgs = { "" };
			// String msg = "";

			DominoEmail pdfEmail = new DominoEmail();
			Object stobj = vscope.get("emailAddress");
			ArrayList<String> sendTo = new ArrayList<String>();
			;
			if (stobj instanceof String) {
				sendTo.add((String) stobj);
			} else if (stobj instanceof ArrayList) {
				sendTo = (ArrayList<String>) stobj;
			} else {
				HS_Util.debug("viewScope.sendTo is a:[" + stobj.getClass().getName() + "]", "debug", msgContext);
				throw (new Exception("Unexpected format of sent to in viewScope: [" + stobj.getClass().getName() + "]"));
			}
			pdfEmail.setTo(sendTo);

			pdfEmail.setSubject((String) vscope.get("emailSubject"));
			pdfEmail.setSenderName("Inspection Reports");
			pdfEmail.setSenderEmail("DoNotReply@healthspace.com");

			String purl = (String) sscope.get("pdfurl");

			String emailName = (String) vscope.get("emailName");
			if ("".equals(emailName)) emailName = "-unknown user-";
			String emailOrg = (String) vscope.get("emailOrg");
			if ("".equals(emailOrg)) emailOrg = "-unknown organization-";

			StringBuilder body = new StringBuilder();

			String bstr = (String) vscope.get("emailBody");
			bstr = bstr.replace("<<recipientName>>", emailName);
			bstr = bstr.replace("<<recipientOrg>>", emailOrg);

			PDFGenerator pdfgen = new com.healthspace.tools.PDFGenerator();
			vscope.put("emailSent", "not yet");

			// emailBean.setBannerHTML("<p>Hi " + sendTo + ",</p>");
			body.append("<p>Hi " + emailName + ",</p>");
			String[] bdata = bstr.split("\\n");
			for (int i = 0; i < bdata.length; i++) {
				body.append("<p>" + bdata[i] + "</p>");
			}

			body.append("<p>Kind regards,<br/>Inspection Reports<br/>0044 1234 5678</p>");
			pdfEmail.addHTML(body);

			if (localDebug) HS_Util.debug("STARTING PDFGenerator.runConverterToInputStream", "debug", msgContext);
			InputStream pdfStream = pdfgen.runConverterToInputStream(purl, false, false);
			if (pdfStream == null) {
				throw (new Exception("Unexpected null result from PDFGenerator.runConverterToInputStream"));
			}
			Stream dominoStream = HS_Util.getSession().createStream();
			dominoStream.setContents(pdfStream);
			int streamSize = dominoStream.getBytes();
			if (localDebug) HS_Util.debug("pdfStream.getBytes():[" + streamSize + "]", "debug", msgContext);
			if (streamSize < 2000) {
				if (localDebug) HS_Util.debug("generated pdf is too small! [" + streamSize + "]", "debug", msgContext);
				vscope.put("emailSent", "generated pdf is too small! [" + streamSize + "]");
			} else {
				// Need to determine how the attachment is defined as a pdf??
				EmailAttachment pdf = new EmailAttachment(pdfStream, "InspectionReport.pdf", false);

				pdfEmail.addAttachment(pdf);
				Document emailDoc = pdfEmail.send();
				if (localDebug) HS_Util.debug("email sent.", "debug", msgContext);
				if (saveEmailDoc) {
					emailDoc.closeMIMEEntities(true, "body");
					emailDoc.save();
				}
				vscope.put("emailSent", "sent");
				result = true;
			}

		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			vscope.put("emailSent", "error: " + e.toString());
			if (HS_Util.isDebugServer()) e.printStackTrace();
			result = false;
		}

		return result;
	}
}
