package com.healthspace.general;

import java.io.Serializable;

import org.openntf.domino.Session;

public class HS_base implements Serializable {
	private static final long serialVersionUID = 1L;

	public HS_base() {

	}

	public Session getSession() {
		return HS_Util.getSession();
	}

	public boolean isDebugServer() {
		return HS_Util.isDebugServer();
	}

	// private static final DebugToolbar dBar = DebugToolbar.get();

	public void debug(String msg) {
		HS_Util.debug(msg, "debug", "");
	}

	public void debug(String msg, String mtype) {
		HS_Util.debug(msg, mtype, this.getClass().toString());
	}

	public void debug(String msg, String mtype, String msgContext) {
		HS_Util.debug(msg, mtype, msgContext);
	}
}
