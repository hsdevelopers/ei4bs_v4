package com.healthspace.general;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.Map.Entry;

import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.View;
import org.openntf.domino.ViewEntry;
import org.openntf.domino.ViewNavigator;

import com.healthspace.tools.JSFUtil;
import com.ibm.domino.services.util.JsonWriter;
import com.ibm.jscript.std.ObjectObject;
import com.ibm.xsp.extlib.util.ExtLibUtil;

//import com.ibm.domino.xsp.module.nsf.NotesContext;
//import com.ibm.xsp.extlib.util.ExtLibUtil;

public class ZOld_QueryFacilities {
	// private String eho;
	private String factype;
	private String query;
	private int maxResults;

	private String format;
	private String unid;

	public ZOld_QueryFacilities() {
		@SuppressWarnings("unused")
		final boolean localDebug = false;
		@SuppressWarnings("unused")
		final String msgContext = "QueryFacilities.constructor";

	}

	public String writeJson() {
		return this.writeJson("ei");
	}

	// valid formats are "ei" and "ig"
	public String writeJson(String iformat) {
		final boolean localDebug = ("ig".equals(iformat));
		final String msgContext = "QueryFacilities.writeJson";

		this.format = iformat;
		try {
			// var query:string = context.getUrlParameter("query");
			String factypeParam = JSFUtil.getParamValue("FacType");
			if ( localDebug ) HS_Util.debug("factypeParam:[" + factypeParam + "]", msgContext);
			this.factype = ("".equals(factypeParam)) ? "-All" : factypeParam;

			String queryParam = JSFUtil.getParamValue("query");
			if ( localDebug ) HS_Util.debug("queryParam:[" + queryParam + "]", msgContext);
			this.query = ("".equals(queryParam)) ? "" : queryParam;

			String maxParam = JSFUtil.getParamValue("maxcount");
			if ( localDebug ) HS_Util.debug("maxParam:[" + maxParam + "]", msgContext);
			this.maxResults = ("".equals(maxParam)) ? 19 : Integer.parseInt(maxParam);

			if ( localDebug )
				HS_Util.debug("query:[" + this.query + "] factype:[" + this.factype + "] maxResults:[" + this.maxResults + "]", "debug", msgContext);
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}
		StringWriter sw = new StringWriter();
		JsonWriter jw = new JsonWriter(sw, false);
		try {
			jw.startArray();
			// if (localDebug) HS_Util.debug("calling writeEntries", "debug",
			// msgContext);
			writeEntries(jw);
			// if (localDebug) HS_Util.debug("ending", "debug", msgContext);
			jw.endArray();
			if ( localDebug ) HS_Util.debug("done", "debug", msgContext);
		} catch (IOException e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}
		return sw.toString();
	}

	/*
	 * Added this on my own on Friday, while Newbs travels back to the Great White North This is called to return the names of the needed fields in the Facility
	 * doc, and the corresponding field names in the IG info so that the corresponding values can be obtained within the writeFacilityDownload() method
	 */
	@SuppressWarnings("unused")
	private Map<String, String> getFacilityFieldMapTEST() {
		// These are just dummy fields to set up the structure, but the real
		// fields will be added
		Map<String, String> fldmap = new HashMap<String, String>();
		fldmap.put("DocumentID", "igDocID");
		fldmap.put("FacilityType", "igFacilityType");
		fldmap.put("Form", "igForm");
		fldmap.put("OwnersName", "igOwnerName");
		fldmap.put("OwnerID", "igOwnerID");
		return fldmap;
	}

	/*
	 * This is the next phase of the field map feature development. This method will perform the lookup for the indicated field map, retrieves the map, sets the
	 * map into a Hash Map, then returns it to the calling code.
	 */
	@SuppressWarnings( { "deprecation", "unchecked" })
	private Map<String, String> getFieldMap() {
		final boolean localDebug = false;
		final String msgContext = "QueryFacilities.getFieldMap";

		Map<String, String> fldmap = new HashMap<String, String>();
		try {
			// ftype is in the QueryString; it indicates the field map to use
			String ftypeParam = JSFUtil.getParamValue("ftype");
			if ( localDebug ) HS_Util.debug("ftype:[" + ftypeParam + "]", msgContext);
			if ( "".equals(ftypeParam) ) ftypeParam = "facility"; // if blank use Facility by default

			// get the ei database and the field maps view
			// Database eidb = new HS_database().getdb("eiRoot");
			Database thisdb = HS_Util.getSession().getCurrentDatabase();

			View mapView = thisdb.getView("code.fld.maps.by.name");
			if ( mapView == null ) throw (new Exception("Unable to get handle to view [code.fld.maps.by.name]"));

			// get Field Map doc specified in ftype query string param
			ViewEntry mapEntry = mapView.getEntryByKey(ftypeParam.toLowerCase(), true);
			if ( mapEntry == null ) throw (new Exception("Unable to find Field Map document [key: " + ftypeParam + "]"));

			// get the field map itself from the second column of the view
			Vector ecols = mapEntry.getColumnValues();
			Vector fmap = (Vector) ecols.elementAt(1);

			// split up the field map, and create the field map hashmap to
			// return on the function
			for (int i = 0; i < fmap.size(); i++) {
				String[] parts = fmap.elementAt(i).toString().split("~");
				fldmap.put(parts[0], parts[1]);
			}

		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			fldmap.put("FieldMapERROR", e.toString());
		}

		return fldmap;
	}

	public String writeFacilityDownload() {
		final boolean localDebug = false;
		final String msgContext = "QueryFacilities.writeFacilityDownload";

		StringWriter sw = new StringWriter();
		JsonWriter jw = new JsonWriter(sw, false);
		try {
			// var query:string = context.getUrlParameter("unid");
			String unidParam = JSFUtil.getParamValue("unid");
			if ( localDebug ) HS_Util.debug("unid:[" + unidParam + "]", msgContext);
			this.unid = unidParam;

			jw.startObject();
			jw.startProperty("unid");
			jw.outStringLiteral(unid);
			jw.endProperty();

			Database eidb = new HS_database().getdb("eiRoot");
			Document facdoc = eidb.getDocumentByUNID(this.unid);
			if ( facdoc == null ) {
				throw (new Exception("Facility Document (unid:[" + this.unid + "] not found."));
			}
			jw.startProperty("name");
			jw.outStringLiteral(facdoc.getItemValueString("name"));
			jw.endProperty();

			// add fields to JSON object based on field mapping
			for (Map.Entry<String, String> entry : getFieldMap().entrySet()) {
				String hsfld = entry.getKey();
				String igfld = entry.getValue();
				if ( facdoc.hasItem(hsfld) ) {
					String fldval = facdoc.getItemValueString(hsfld);
					jw.startProperty(igfld);
					jw.outStringLiteral(fldval);
					jw.endProperty();
				} else {
					jw.startProperty(igfld);
					jw.outStringLiteral("Field[" + hsfld + "] not found.");
					jw.endProperty();
				}
			}

		} catch (IOException e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			try {
				jw.startProperty("error");
				jw.outStringLiteral(e.toString());
				jw.endProperty();
			} catch (IOException e1) {
				// Nothing to see here, move along...
			}
		}
		try {
			jw.endObject();
		} catch (IOException e) {
			// Nothing to see here, move along...
		}
		return sw.toString();
	}

	private void writeEntries(JsonWriter jw) {
		final boolean localDebug = false;
		final String msgContext = "QueryFacilities.writeEntries";
		String useFT = HS_Util.getAppSettingString("FacilitySearchFTIndex");
		try {
			List<Map<String, String>> entryList = null;
			if ( "Yes".equals(useFT) ) {
				entryList = getEntryListFT();
			} else {
				entryList = getEntryList();
			}
			if ( localDebug ) HS_Util.debug("got [" + entryList.size() + "] entries.", "debug", msgContext);
			for (Map<String, String> valMap : entryList) {
				jw.startArrayItem();
				jw.startObject();
				for (Entry<String, String> valEntry : valMap.entrySet()) {
					jw.startProperty(valEntry.getKey());
					jw.outStringLiteral(valEntry.getValue());
					jw.endProperty();
				}
				jw.endObject();
				jw.endArrayItem();
			}
		} catch (IOException e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}
	}

	@SuppressWarnings( { "unchecked", "deprecation" })
	private List<Map<String, String>> getEntryList() {
		final boolean localDebug = false;
		final String msgContext = "QueryFacilities.getEntryList";

		final String fslimit = HS_Util.getAppSettingString("FacilitySearchLimits");
		if ( localDebug ) HS_Util.debug("FacilitySearchLimits:[" + fslimit + "]", "debug", msgContext);
		Vector<String> enabledFtypes = new Vector<String>();
		if ( "enabled".equals(fslimit) ) {
			// enabledFtypes.add("Food Facilities");
			enabledFtypes = getEnabledFacilityTypes();
		}

		List<Map<String, String>> entryList = new ArrayList<Map<String, String>>();
		try {
			// Session sess = NotesContext.getCurrent().getCurrentSession();
			// Session sess = HS_Util.getSession();
			HS_database hsdb = new HS_database();
			Database eidb = hsdb.getdb("eiRoot");
			// View entryView =
			// ExtLibUtil.getCurrentDatabase().getView("Events");
			View entryView = eidb.getView("XPagesFacilitiesByType");
			entryView.setAutoUpdate(false);
			// ViewNavigator viewNav = entryView.createViewNav();
			Vector<String> useCols = this.getUseCols();
			Vector<String> cnames = entryView.getColumnNames();
			String fkey = factype + "-" + getQuery().toUpperCase().charAt(0);
			if ( localDebug ) HS_Util.debug("key is:[" + fkey + "]", "debug", msgContext);
			// View fnav:NotesViewNavigator =
			// fview.createViewNavFromCategory(fkey);
			ViewNavigator viewNav = entryView.createViewNavFromCategory(fkey);

			viewNav.setBufferMaxEntries(400);

			ViewEntry startEnt = getStartEntry(viewNav, getQuery());
			boolean doThisEntry = false;
			if ( startEnt != null ) {
				ViewEntry viewEnt = startEnt;
				String facname = "";
				String ftype = "";
				while (viewEnt != null) {
					Vector colValues = viewEnt.getColumnValues();
					facname = colValues.get(1).toString().toLowerCase();
					if ( facname.startsWith(getQuery().toLowerCase()) ) {
						doThisEntry = true;
						if ( "enabled".equals(fslimit) ) {
							ftype = colValues.get(3).toString();
							if ( !enabledFtypes.contains(ftype) ) {
								doThisEntry = false;
								if ( localDebug ) HS_Util.debug("ftype:[" + ftype + "] being skipped.", "debug", msgContext);
							}
						}
						if ( doThisEntry ) {
							entryList.add(this.addEntry(cnames, viewEnt, useCols));
						}
						viewEnt = viewNav.getNext(viewEnt);
					} else {
						viewEnt = null;
					}
				}
			} else {
				Map<String, String> errorMap = new HashMap<String, String>();
				errorMap.put("Error", "no start entry found!");
				errorMap.put("query", this.getQuery());
				errorMap.put("facilityType", this.getFactype());
				errorMap.put("maxResults", "" + this.getMaxResults());
				entryList.add(errorMap);
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}
		return entryList;
	}

	@SuppressWarnings("unchecked")
	private List<Map<String, String>> getEntryListFT() {
		final boolean localDebug = true;
		final String msgContext = "QueryFacilities.getEntryListFT";

		final String fslimit = HS_Util.getAppSettingString("FacilitySearchLimits");
		// if (localDebug) HS_Util.debug("FacilitySearchLimits:[" + fslimit +
		// "]", "debug", msgContext);
		Vector<String> enabledFtypes = new Vector<String>();

		if ( "enabled".equals(fslimit) ) {
			// enabledFtypes.add("Food Facilities");
			enabledFtypes = getEnabledFacilityTypes();
		}
		String query = this.getQuery();

		List<Map<String, String>> entryList = new ArrayList<Map<String, String>>();
		try {
			// Session sess = NotesContext.getCurrent().getCurrentSession();
			// Session sess = HS_Util.getSession();
			HS_database hsdb = new HS_database();
			Database eidb = hsdb.getdb("eiRoot");
			if ( !eidb.isFTIndexed() ) {
				if ( localDebug ) HS_Util.debug("db is not FT indexed. returning old style", "error", msgContext);
				return this.getEntryList();
			}

			// View entryView =
			// ExtLibUtil.getCurrentDatabase().getView("Events");
			View entryView = eidb.getView("XPagesFacilitiesByType");
			// ViewNavigator viewNav = entryView.createViewNav();

			entryView.setAutoUpdate(false);
			// ViewNavigator viewNav = entryView.createViewNav();
			Vector<String> useCols = this.getUseCols();

			Vector<String> cnames = entryView.getColumnNames();

			// int ftct = entryView.FTSearchSorted(query, 0, 2, false, false,
			// false, false);
			int ftct = entryView.FTSearch(query, 100);

			if ( localDebug ) HS_Util.debug("FTSearch returned:[" + ftct + "] for:[" + query + "]", "debug", msgContext);

			if ( ftct > 0 ) {

				boolean doThisEntry = false;
				Document viewDoc = entryView.getFirstDocument();
				String facname = "";
				String ftype = "";
				while (viewDoc != null) {
					Vector colValues = viewDoc.getColumnValues();
					facname = colValues.get(1).toString().toLowerCase();
					doThisEntry = true;
					if ( "enabled".equals(fslimit) ) {
						ftype = colValues.get(3).toString();
						if ( localDebug ) HS_Util.debug("for facname:[" + facname + "] ftype is:[" + ftype + "]", "debug", msgContext);
						if ( !enabledFtypes.contains(ftype) ) {
							doThisEntry = false;
							if ( localDebug ) HS_Util.debug("being skipped.", "debug", msgContext);
						}
					}
					if ( doThisEntry ) {
						entryList.add(this.addEntry(cnames, viewDoc, useCols));
					}
					viewDoc = entryView.getNextDocument(viewDoc);
				}
			} else {
				Map<String, String> errorMap = new HashMap<String, String>();
				errorMap.put("Error", "no entries found for query: '" + query + "' in db:[" + eidb.getFilePath() + "]");
				errorMap.put("query", query);
				errorMap.put("facilityType", this.getFactype());
				errorMap.put("maxResults", "" + this.getMaxResults());
				entryList.add(errorMap);
			}
			if ( entryList.size() == 0 ) {
				Map<String, String> errorMap = new HashMap<String, String>();
				errorMap.put("Error", "no entries found for " + query);
				errorMap.put("query", query);
				errorMap.put("facilityType", this.getFactype());
				errorMap.put("maxResults", "" + this.getMaxResults());
				entryList.add(errorMap);
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}
		return entryList;
	}

	private Vector<String> getUseCols() {
		Vector<String> useCols = new Vector<String>();
		if ( "ei".equals(this.format) ) {
			useCols.add("FacilityID");
			useCols.add("FacilityType");
			useCols.add("Address");
			useCols.add("City");
			useCols.add("PermitNumber");
			useCols.add("Province");
			useCols.add("PostalCode");
			useCols.add("PhoneDay");
		} else if ( "ig".equals(this.format) ) {
			useCols.add("FacilityID~establishmentNum");
			useCols.add("SubType~establishmentType");
			useCols.add("Address~address~1");
			useCols.add("City~address~2");
			useCols.add("PermitNumber~permitNum");
			useCols.add("Province~address~3");
			useCols.add("PostalCode~address~4");
			useCols.add("PhoneDay~phoneNumber");
			useCols.add("Risk~risk");
			useCols.add("NextInspection~reinspectionDate");
		}
		return useCols;
	}

	@SuppressWarnings("unchecked")
	private Map<String, String> addEntry(Vector<String> cnames, ViewEntry viewEnt, Vector<String> useCols) {
		Vector colValues = viewEnt.getColumnValues();
		String unid = viewEnt.getUniversalID();
		return this.addEntry(cnames, colValues, useCols, unid);
	}

	@SuppressWarnings("unchecked")
	private Map<String, String> addEntry(Vector<String> cnames, Document viewDoc, Vector<String> useCols) {
		Vector colValues = viewDoc.getColumnValues();
		String unid = viewDoc.getUniversalID();
		return this.addEntry(cnames, colValues, useCols, unid);
	}

	@SuppressWarnings("unchecked")
	private Map<String, String> addEntry(Vector<String> cnames, Vector colValues, Vector<String> useCols, String unid) {
		// final boolean localDebug = HS_Util.isDebugServer();
		// final boolean localDebug = false;
		final String msgContext = "QueryFacilities.addEntry";

		Map<String, String> valueMap = new HashMap<String, String>();
		try {
			if ( "ei".equals(this.format) ) {
				valueMap.put("name", colValues.get(1).toString());
				for (int i = 2; i < cnames.size(); i++) {
					if ( useCols.contains(cnames.get(i)) ) {
						valueMap.put(cnames.get(i), colValues.get(i).toString());
					}
				}
				valueMap.put("unid", unid);
			} else if ( "ig".equals(this.format) ) {
				valueMap.put("name", colValues.get(1).toString());
				Vector<String> uc0 = new Vector<String>();
				Vector<String> uc1 = new Vector<String>();
				Vector<String> uc2 = new Vector<String>();
				String[] parms = null;
				for (int v = 0; v < useCols.size(); v++) {
					parms = useCols.get(v).split("~");
					uc0.add(parms[0]);
					uc1.add(parms[1]);
					uc2.add((parms.length > 2) ? parms[2] : "-");
				}
				int x = 0;
				String address = "";
				String tag = "";
				for (int i = 2; i < cnames.size(); i++) {
					if ( uc0.contains(cnames.get(i)) ) {
						tag = "";
						x = uc0.indexOf(cnames.get(i));
						tag = uc1.get(x);
						if ( "address".equals(tag) ) {
							address += ("".equals(address)) ? "" : ("4".equals(uc2.get(x))) ? " " : ", ";
							address += colValues.get(i).toString();
							// address += " [" + uc2.get(x) + "]";
						} else {
							valueMap.put(tag, colValues.get(i).toString());
						}

					}
				}

				valueMap.put("address", address);
				valueMap.put("unid", unid);
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}

		return valueMap;
	}

	@SuppressWarnings("unchecked")
	private Vector<String> getEnabledFacilityTypes() {
		// final boolean localDebug = HS_Util.isDebugServer();
		final boolean localDebug = false;
		final String msgContext = "QueryFacilities.getEnabledFacilityTypes";

		Vector<String> rslt = new Vector<String>();
		try {
			Map<String, Object> ascope = ExtLibUtil.getApplicationScope();
			if ( !ascope.containsKey("moduleData") ) return rslt;

			Vector<ObjectObject> mdata = (Vector<ObjectObject>) ascope.get("moduleData");
			Object forms = null;
			for (ObjectObject module : mdata) {
				if ( !("-Blank-".equalsIgnoreCase(module.get("label").stringValue())) ) {
					forms = module.get("forms").toJavaObject();
					if ( forms != null ) {
						if ( forms instanceof Vector ) {
							for (Object frm : (Vector) forms) {
								if ( frm instanceof java.lang.String ) {
									StringBuilder frmstr = new StringBuilder();
									frmstr.append(frm);
									String alias = frmstr.substring(frmstr.indexOf("~") + 1);
									/*
									 * This is based on the formula for the view
									 * 
									 * Form = "CampFacility"; "Campgrounds"; Form = "LaborCampFacility"; "Labor Camps"; Form = "SummerCampFacility";
									 * "Summer Camps"; Form = "HotelFacility"; "Hotels / Motels"; Form = "JailFacility"; "Correctional Institute Facilities";
									 * Form = "WaterFacility"; "Water Systems"; Form = "SepticRemovalFacility"; "Septic Removals"; Form =
									 * "TemporaryVendorFacility"; "Temporary Vendors"; Form = "MobileHomeFacility"; "Manufactured Home Communities";
									 * 
									 * @Right( Form; 8 ) = "Facility"; @Left( Form; "Facility" ) + " Facilities"; Form = "SewageSystem";
									 * "On-site Sewage Systems"; Form = "SubdivisionApplication"; "Subdivision Applications"; Form = "WaterPrivateWell";
									 * "Private Wells"; "<" + Form + ">")
									 */
									if ( "CampFacility".equals(alias) ) {
										rslt.add("Campgrounds");
									} else if ( "LaborCampFacility".equals(alias) ) {
										rslt.add("Labor Camps");
									} else if ( "SummerCampFacility".equals(alias) ) {
										rslt.add("Summer Camps");
									} else if ( "HotelFacility".equals(alias) ) {
										rslt.add("Hotels / Motels");
									} else if ( "JailFacility".equals(alias) ) {
										rslt.add("Correctional Institute Facilities");
									} else if ( "WaterFacility".equals(alias) ) {
										rslt.add("Water Systems");
									} else if ( "SepticRemovalFacility".equals(alias) ) {
										rslt.add("Septic Removals");
									} else if ( "TemporaryVendorFacility".equals(alias) ) {
										rslt.add("Temporary Vendors");
									} else if ( "MobileHomeFacility".equals(alias) ) {
										rslt.add("Manufactured Home Communities");
									} else if ( "SewageSystem".equals(alias) ) {
										rslt.add("On-site Sewage Systems");
									} else if ( "SubdivisionApplication".equals(alias) ) {
										rslt.add("Subdivision Applications");
									} else if ( "WaterPrivateWell".equals(alias) ) {
										rslt.add("Private Wells");
									} else if ( alias.endsWith("Facility") ) {
										rslt.add(alias.substring(0, alias.lastIndexOf("Facility")) + " Facilities");
									}
								}
							}
						}
					}
				}
			}

			if ( localDebug ) HS_Util.debug("Enabled facilities are:[" + rslt + "]", "debug", msgContext);

			return rslt;
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			rslt.add("Food Facilities");
			return rslt;
		}
	}

	@SuppressWarnings("unchecked")
	private ViewEntry getStartEntry(ViewNavigator viewNav, String squery) {
		ViewEntry ent = null;
		String facname = "";
		for (ViewEntry went : viewNav) {
			Vector colValues = went.getColumnValues();
			facname = colValues.get(1).toString().toLowerCase();
			if ( facname.startsWith(squery.toLowerCase()) ) {
				ent = went;
				// return ent;
				break;
			}
		}
		return ent;
	}

	public String getFactype() {
		return factype;
	}

	public String getQuery() {
		return query;
	}

	public int getMaxResults() {
		return maxResults;
	}

	public void setFactype(String factype) {
		this.factype = factype;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public void setMaxResults(int maxResults) {
		this.maxResults = maxResults;
	}

}
