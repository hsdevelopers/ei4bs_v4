package com.healthspace.general;

import java.io.Serializable;
import java.util.Vector;

public class AppModule implements Serializable {
	// aaccess true
	// dbkey eiRoot
	// faccustom
	// forms
	// [0] Food Facility~FoodFacility
	// [1] Food Report~FoodReport
	// hpage true
	// image icn_par_food.jpg
	// label Food
	// mobViewPage vwFoodFacilities
	// oname Food
	// oviewcustom
	// slabel Food
	// tbar true
	// v4rname XPagesFacilitiesByStatus
	// vname XPagesFacilitiesByType
	// xdb_csize 17
	private boolean aaccess;
	private String dbkey;
	private String faccustom;
	private String[] forms;
	private boolean hpage;
	private String image;
	private String label;
	private String mobViewPage;
	private String oname;
	private String oviewcustom;
	private String slabel;
	private boolean tbar;
	private String v4rname;
	private String vname;
	private AppModuleView[] views;

	private int xdb_csize;

	private static final long serialVersionUID = 1L;

	public AppModule() {

	}

	public boolean isAaccess() {
		return aaccess;
	}

	public String getDbkey() {
		return dbkey;
	}

	public String getFaccustom() {
		return faccustom;
	}

	public String[] getForms() {
		return forms;
	}

	public boolean isHpage() {
		return hpage;
	}

	public String getImage() {
		return image;
	}

	public String getLabel() {
		return label;
	}

	public String getMobViewPage() {
		return mobViewPage;
	}

	public String getOname() {
		return oname;
	}

	public String getOviewcustom() {
		return oviewcustom;
	}

	public String getSlabel() {
		return slabel;
	}

	public boolean getTbar() {
		return tbar;
	}

	public String getV4rname() {
		return v4rname;
	}

	public String getVname() {
		return vname;
	}

	public int getXdb_csize() {
		return xdb_csize;
	}

	public AppModuleView[] getViews() {
		return views;
	}

	public void setAaccess(boolean aaccess) {
		this.aaccess = aaccess;
	}

	public void setDbkey(String dbkey) {
		this.dbkey = dbkey;
	}

	public void setFaccustom(String faccustom) {
		this.faccustom = faccustom;
	}

	public void setForms(String[] forms) {
		this.forms = forms;
	}

	public void setHpage(boolean hpage) {
		this.hpage = hpage;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setMobViewPage(String mobViewPage) {
		this.mobViewPage = mobViewPage;
	}

	public void setOname(String oname) {
		this.oname = oname;
	}

	public void setOviewcustom(String oviewcustom) {
		this.oviewcustom = oviewcustom;
	}

	public void setSlabel(String slabel) {
		this.slabel = slabel;
	}

	public void setTbar(boolean tbar) {
		this.tbar = tbar;
	}

	public void setV4rname(String v4rname) {
		this.v4rname = v4rname;
	}

	public void setVname(String vname) {
		this.vname = vname;
	}

	public void setXdb_csize(int xdb_csize) {
		this.xdb_csize = xdb_csize;
	}

	public void setViews(AppModuleView[] views) {
		this.views = views;
	}

	// {dbkey:"eiRoot", hpage:true, tbar:true, image:"icn_par_food.jpg", label:"Food", oname:"Food", slabel:"Food",
	// vname:"Food\\By Status"},
	public void init(String dbkey, boolean hpage, boolean tbar, String image, String label, String oname, String slabel, String vname) {
		this.dbkey = dbkey;
		this.hpage = hpage;
		this.tbar = tbar;
		this.image = image;
		this.label = label;
		this.oname = oname;
		this.slabel = slabel;
		this.vname = vname;
		this.views = null;
	}

	public void init(Vector<Object> cvals) {
		this.dbkey = (String) cvals.get(1);
		this.hpage = ("Y".equals(cvals.get(2))) ? true : false;
		this.image = (String) cvals.get(3);
		this.label = (String) cvals.get(4);
		this.oname = (String) cvals.get(5);
		this.slabel = (String) cvals.get(6);
		this.tbar = ("Y".equals(cvals.get(7))) ? true : false;
		this.vname = (String) cvals.get(8);
		this.v4rname = (cvals.size() < 16) ? "" : (String) cvals.get(15);
		// Object tmp = cvals.get(9);
		// if(tmp.getClass().getName().contains("Vector")) {
		//			
		// }
		this.forms = new String[0];
		this.mobViewPage = (String) cvals.get(10);
		this.oviewcustom = (cvals.size() < 14) ? "" : (String) cvals.get(13);
		this.faccustom = (cvals.size() < 15) ? "" : (String) cvals.get(14);
		this.xdb_csize = cvals.size();
		this.aaccess = true;
		this.views = null;
	}

}
