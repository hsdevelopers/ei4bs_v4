/**
 * 
 */
package com.healthspace.general;

import java.io.Serializable;
import java.util.Date;
import java.util.Vector;

import org.openntf.domino.Database;
import org.openntf.domino.DateTime;
import org.openntf.domino.Document;
import org.openntf.domino.Item;

//import com.ibm.icu.util.Calendar;

/**
 * @author Henry Newberry
 * 
 */
public class HS_CalculateNext implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public HS_CalculateNext() {

	}

	private static String dbDesign;
	private static int interval;
	private static Vector<String> subinterval;
	private static Vector<String> subintervalnums;

	public static String getDbDesign() {
		if (dbDesign == null) {
			dbDesign = HS_Util.getMasterSettingsString("DBDesign");
		}
		return dbDesign;
	}

	public static int getInterval() {
		return interval;
	}

	public static Vector<String> getSubinterval() {
		return subinterval;
	}

	public static Vector<String> getSubintervalnums() {
		return subintervalnums;
	}

	public static void setDbDesign(String dbDesign) {
		HS_CalculateNext.dbDesign = dbDesign;
	}

	public static void setInterval(int interval) {
		HS_CalculateNext.interval = interval;
	}

	public static void setSubinterval(Vector<String> subinterval) {
		HS_CalculateNext.subinterval = subinterval;
	}

	public static void setSubintervalnums(Vector<String> subintervalnums) {
		HS_CalculateNext.subintervalnums = subintervalnums;
	}

	private enum HSForm {
		FoodReport, FoodFacility, WaterReport, WaterFacility, PoolReport, PoolFacility, HotelReport, HotelFacility, GeneralReport, GeneralFacility, SummerCampReport, SummerCampFacility, LaborCampReport, LaborCampFacility, CampReport, CampFacility, BodyartReport, BodyartFacility, DairyReport, DairyFacility, AviaryReport, AviaryFacility, GarbageReport, GarbageFacility, SchoolReport, SchoolFacility, JailReport, JailFacility, BeachReport, BeachFacility, MobileReport, MobileHomeFacility, SolidWasteReport, SolidWasteFacility, RadiationReport, RadiationFacility, FairgroundReport, FairgroundFacility, HousingReport, HousingFacility, AgricultureReport, AgricultureFacility, ChildcareReport, ChildcareFacility, HealthUnitReport, HealthUnitFacility, ShellfishReport, ShellfishFacility
	}

	public static boolean calculateNext(Document facility) {
		// final boolean localDebug = (HS_Util.isDebugServer() || HS_Util.isDebugger());
		final boolean localDebug = false;
		final String dbarLoc = "HS_CalculateNext.calculateNext";

		boolean result = false;
		if (facility == null) {
			if (localDebug) HS_Util.debug("Called with null facility document", "warn", dbarLoc);
			return result;
		}
		try {
			HS_auditForm facform = new HS_auditForm(facility);

			if (localDebug) {
				Vector<Object> wrkdbg = null;
				if (facility.hasItem("NextInspDue")) {
					wrkdbg = facility.getItemValue("NextInspDue");
				} else {
					wrkdbg = new Vector<Object>();
					wrkdbg.add("NOT DEFINED");
				}
				HS_Util.debug("starting with NextInspDue:[" + wrkdbg + "]", "debug", dbarLoc);

				if (facility.hasItem("NextInspection")) {
					wrkdbg = facility.getItemValue("NextInspection");
				} else {
					wrkdbg = new Vector<Object>();
					wrkdbg.add("NOT DEFINED");
				}
				HS_Util.debug("starting with NextInspection:[" + wrkdbg + "]", "debug", dbarLoc);

				if (facility.hasItem("NextInspectionDate")) {
					wrkdbg = facility.getItemValue("NextInspectionDate");
				} else {
					wrkdbg = new Vector<Object>();
					wrkdbg.add("NOT DEFINED");
				}
				HS_Util.debug("starting with NextInspectionDate:[" + wrkdbg + "]", "debug", dbarLoc);
			}
			// if (localDebug) HS_Util.debug("calling calculateNextInspection.", "debug", dbarLoc);
			calculateNextInspection(facility);

			result = facform.checkChanges(facility);
			if (result && localDebug) HS_Util.debug("returning:[" + result + "] - changes hav been made", "debug", dbarLoc);
			if (!result && localDebug) HS_Util.debug("returning:[" + result + "] - no changes detected", "debug", dbarLoc);
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			if (HS_Util.isDebugServer()) e.printStackTrace();
		}
		return result;
	}

	private static void calculateNextInspection(Document facility) {
		// final boolean localDebug = (HS_Util.isDebugServer() || HS_Util.isDebugger());
		final boolean localDebug = false;
		final String dbarLoc = "HS_CalculateNext.calculateNextInspection";

		try {
			if ("VDH".equals(getDbDesign())) {
				calculateNextInspectionVDH(facility);
			} else {
				// if (localDebug) HS_Util.debug("calling calculateNextOther.", "debug", dbarLoc);
				calculateNextOther(facility, facility.getParentDatabase());

				// 'this overrides the calculations for Opening and Provisional inspections
				if ("Arizona".equals(getDbDesign())) calculateNextInspAZ(facility);
			}
			if (localDebug) HS_Util.debug("calling checkOverDueByDoc.", "debug", dbarLoc);
			checkOverDueByDoc(facility);
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			if (HS_Util.isDebugServer()) e.printStackTrace();
		}
		try {
			if (localDebug) {
				dbugcloser("NextInspection", facility, dbarLoc);
				dbugcloser("NextInspDue", facility, dbarLoc);
			}
		} catch (Exception e) {
			HS_Util.debug("ERROR ON DEBUGGING AT CLOSE: " + e.toString(), "error", dbarLoc);
		}
	}

	private static void dbugcloser(String dbfld, Document facility, String dbarLoc) {
		// final String dbarLoc = "HS_CalculateNext.dbugcloser";
		try {
			Vector<Object> dbvalue = null;

			if (facility.hasItem(dbfld)) {
				dbvalue = facility.getItemValue(dbfld);
				if (dbvalue == null) {
					HS_Util.debug("returning with [" + dbfld + "]: null.", "debug", dbarLoc);
				} else if (dbvalue.size() == 0) {
					HS_Util.debug("returning with [" + dbfld + "]: empty.", "debug", dbarLoc);
				} else {
					HS_Util.debug("returning with [" + dbfld + "]:" + facility.getItemValue(dbfld).get(0), "debug", dbarLoc);
				}
			} else {
				HS_Util.debug("returning with [" + dbfld + "] NOT SET.", "debug", dbarLoc);
			}
		} catch (Exception e) {
			HS_Util.debug("ERROR ON DEBUGGING AT CLOSE: " + e.toString(), "error", dbarLoc);
		}
	}

	@SuppressWarnings( { "deprecation" })
	private static void calculateNextOther(Document facility, Database ehsdb) {
		// final boolean localDebug = (HS_Util.isDebugServer() || HS_Util.isDebugger());
		final boolean localDebug = false;
		final String dbarLoc = "HS_CalculateNext.calculateNextOther";

		try {
			if ("VDH".equals(getDbDesign())) {
				calculateNextInspectionVDH(facility);
				return;
			}

			// Session sess = HS_Util.getSession();
			boolean followupflag = false;

			Item tempitem = null;
			Item nextsched = null;
			String strsubtypeinterval;
			boolean subintervalfound;
			DateTime baseDate = null;
			// ' Abort if not a facility form
			if (!facility.getFormName().toLowerCase().endsWith("facility")) return;

			// ' Get a handle on the most recent inspections (routine and all)
			String lastRoutineUnid = HS_GetMostRecent.routineInspectionUNID(ehsdb, facility);
			Document lastRoutineInspection = HS_Util.getDocumentByUNID(ehsdb, lastRoutineUnid, false);
			// if (localDebug)
			// HS_Util.debug("for lastRoutineUnid:[" + lastRoutineUnid + "] got a:["
			// + ((lastRoutineInspection != null) ? lastRoutineInspection.getClass().getName() : "null") + "]", "debug", dbarLoc);
			String lastAnyUnid = HS_GetMostRecent.inspectionUNID(ehsdb, facility);
			Document lastAnyInspection = HS_Util.getDocumentByUNID(ehsdb, lastAnyUnid, false);
			// if (localDebug)
			// HS_Util.debug("for lastAnyUnid:[" + lastAnyUnid + "] got a:[" + ((lastAnyInspection != null) ?
			// lastAnyInspection.getClass().getName() : "null")
			// + "]", "debug", dbarLoc);

			// ' Set the inspection follow-up flag. First try inspection then fall back to facility.
			if (lastAnyInspection != null) {
				// If LastAnyInspection.FollowUpInspectionRequired(0) = "Yes" Then
				followupflag = "Yes".equalsIgnoreCase(lastAnyInspection.getItemValueString("FollowUpInspectionRequired"));
				if (localDebug) HS_Util.debug("followupflag:[" + followupflag + "] lastAnyInspection is NOT null:[" + lastAnyInspection.getFormName() + "]", "debug", dbarLoc);
			} else {
				followupflag = "Yes".equalsIgnoreCase(facility.getItemValueString("FollowUpInspectionRequired"));
				// if (localDebug) HS_Util.debug("followupflag:[" + followupflag + "] lastAnyInspection is null", "debug", dbarLoc);
			}

			// ' Look up inspection interval
			interval = inspectionInterval(facility);

			tempitem = getLastDate(facility, lastRoutineInspection);
			strsubtypeinterval = getSubtypeInterval(facility);
			// If strsubtypeinterval = "" Then subintervalfound = False Else subintervalfound = True
			subintervalfound = (!"".equals(strsubtypeinterval));
			// if (localDebug) HS_Util.debug("strsubtypeinterval:[" + strsubtypeinterval + "] subintervalfound:[" + subintervalfound + "]",
			// "debug", dbarLoc);

			if (tempitem != null) {
				if (tempitem.getType() == 1024) {
					// Calendar tcal = Calendar.getInstance();
					// Calendar tcal = tempitem.getDateTimeValue().toJavaCal();
					// Calendar.get(Calendar.YEAR) - 1900.
					Date jdt = tempitem.getDateTimeValue().toJavaDate();
					int iyear = jdt.getYear() + 1900;
					if (iyear > 1950) {
						baseDate = tempitem.getDateTimeValue();
						if (subintervalfound) {
							if (localDebug) HS_Util.debug("Adjusting baseDate:[" + baseDate.toString() + "] by subinterval:[" + strsubtypeinterval + "]", "debug", dbarLoc);
							baseDate.adjustDay(Integer.parseInt(strsubtypeinterval));
						} else {
							if (localDebug) HS_Util.debug("Adjusting baseDate:[" + baseDate.toString() + "] by interval:[" + interval + "]", "debug", dbarLoc);
							baseDate.adjustDay(interval);
						}
						// if (localDebug) HS_Util.debug("Adjusted baseDate is:[" + baseDate.toString() + "]", "debug", dbarLoc);
					} else {
						if (localDebug) HS_Util.debug("tempItem year is before 1950:[" + iyear + "]", "debug", dbarLoc);
					}
				} else {
					if (localDebug) HS_Util.debug("tempItem type is not 1024:[" + tempitem.getType() + "]", "debug", dbarLoc);
				}
			} else {
				if (localDebug) HS_Util.debug("tempItem is null..", "debug", dbarLoc);
			}

			// ' Adjust for months closed and weekends
			// If Not BaseDate Is Nothing Then
			if (baseDate != null) {
				// Set BaseDate = New NotesDateTime(CheckForClosed(Facility, BaseDate.DateOnly))
				// baseDate = sess.createDateTime(checkForClosed(facility, baseDate.getDateOnly()));
				if (localDebug) HS_Util.debug("baseDate is:[" + baseDate.getDateOnly() + "] - calling checkForClosed.", "debug", dbarLoc);
				baseDate = checkForClosed(facility, baseDate);
			}

			// ' For follow-ups, push the next inspection date up to the facility form from the inspection form
			// if (localDebug) HS_Util.debug("followupflag:[" + followupflag + "] ", "debug", dbarLoc);

			if (followupflag && (lastAnyInspection != null)) {
				if (lastAnyInspection.hasItem("NextInspection")) {
					nextsched = lastAnyInspection.getFirstItem("NextInspection");
					if (localDebug) HS_Util.debug("nextsched type:[" + nextsched.getType() + "]", "debug", dbarLoc);
					if (nextsched.getType() == 1024) {
						facility.replaceItemValue("NextInspection", nextsched.getDateTimeValue());
					}
				}
			}

			if (baseDate != null) {
				if (localDebug) HS_Util.debug("baseDate being set as:[" + baseDate.getDateOnly() + "]", "debug", dbarLoc);
				if (lastRoutineInspection != null) {
					facility.replaceItemValue("NextInspDue", baseDate);
					facility.replaceItemValue("dspNextInspDue", baseDate);
				}
				if (subintervalfound) {
					// Facility.Freq = Round(CInt(strsubtypeinterval) * 12/365, 0) 'convert days to months
					facility.replaceItemValue("Freq", Math.round((Float.parseFloat(strsubtypeinterval) * 12 / 365)));
				} else {
					// Facility.Freq= Round(Interval * 12/365, 0) 'convert days to months
					facility.replaceItemValue("Freq", Math.round(interval * 12 / 365));
				}
			} else {
				// ' Calculate based on no inspections just using initial inspection interval
				// if (localDebug) HS_Util.debug("baseDate is now null]", "debug", dbarLoc);
				facility.removeItem("NextInspDue");
				facility.removeItem("dspNextInspDue");
				facility.removeItem("NextStateInspDue"); // 'is this VDH specific?;
				if ("Pending".equals(facility.getItemValueString("Status")) && !("Closed".equals(facility.getItemValueString("Stage")))) {
					if (facility.hasItem("PermitDate")) {
						tempitem = facility.getFirstItem("PermitDate");
						if (tempitem.getType() == 1024) {
							baseDate = tempitem.getDateTimeValue();
							Date jdt = baseDate.toJavaDate();
							// baseDate.toJavaCal();
							if ((jdt.getYear() + 1900) > 1950) {
								baseDate.adjustDay((subintervalfound) ? Integer.parseInt(strsubtypeinterval) : interval);
							}
						}
						if (localDebug) HS_Util.debug("baseDate being set as:[" + baseDate.getDateOnly() + "]", "debug", dbarLoc);
						if (lastRoutineInspection != null) {
							facility.replaceItemValue("NextInspDue", baseDate);
							facility.replaceItemValue("NextStateInspDue", baseDate.getLocalTime());
						}
						facility.replaceItemValue("Freq", (subintervalfound) ? Math.round(Integer.parseInt(strsubtypeinterval) * 12 / 365) : Math.round(interval * 12 / 365));
					}
				}
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			if (HS_Util.isDebugServer()) e.printStackTrace();
		}
		// if (localDebug) HS_Util.debug("returning from the function", "debug", dbarLoc);
		try {
			if (localDebug) {
				dbugcloser("NextInspection", facility, dbarLoc);
				dbugcloser("NextInspDue", facility, dbarLoc);
			}
		} catch (Exception e) {
			HS_Util.debug("ERROR ON DEBUGGING AT CLOSE: " + e.toString(), "error", dbarLoc);
		}
	}

	@SuppressWarnings( { "deprecation" })
	private static DateTime checkForClosed(Document facility, DateTime inspdate) {
		// final boolean localDebug = (HS_Util.isDebugServer() || HS_Util.isDebugger());
		final boolean localDebug = false;
		final String dbarLoc = "HS_CalculateNext.checkForClosed";

		DateTime result = null;
		// ' Check to ensure that the facility is a valid document
		if (facility == null) return result;
		if (!facility.isValid()) return result;
		if (facility.getItems().size() < 1) return result;

		// ' Check the date passed in to ensure that it is valid
		if (inspdate == null) return result;
		try {
			// inspdate.toJavaCal();
			Date idt = inspdate.toJavaDate();
			int inspmonth = idt.getMonth() + 1;
			int inspyear = idt.getYear() + 1900;
			int inspday = idt.getDate();

			// ' Check to see if the facility is open all year, which makes the months open test irrelevant
			boolean skipmonths = true;
			String ayr = (facility.hasItem("AllYearRound")) ? facility.getItemValueString("AllYearRound") : "No";
			skipmonths = "Yes".equals(ayr);
			// if (localDebug) HS_Util.debug("1. skipmonths:[" + skipmonths + "] ayr:[" + ayr + "]", "debug", dbarLoc);

			// ' If there is no MonthsOpen item, then default is open all year
			if (!skipmonths && !facility.hasItem("MonthsOpen")) skipmonths = true;
			// if (localDebug) HS_Util.debug("2. skipmonths:[" + skipmonths + "]", "debug", dbarLoc);

			Vector<Object> arr = new Vector<Object>();
			if (!skipmonths) {
				arr = facility.getItemValue("MonthsOpen");
				if (arr.size() < 1) skipmonths = true;
			}
			// if (localDebug) HS_Util.debug("3. skipmonths:[" + skipmonths + "] arr:[" + arr.toString() + "]", "debug", dbarLoc);

			if (!skipmonths) {
				// 'Only check for one year in the future - stops endless loops

				boolean bDone = false;
				// If Not IsNull(ArrayGetIndex(arr, inspmonth)) Then
				if (arr.contains("" + inspmonth)) bDone = true;
				// if (localDebug)
				// HS_Util.debug("Before loop. inspyear:[" + inspyear + "] inspmonth:[" + inspmonth + "] inspday:[" + inspday + "] bDone:["
				// + bDone + "]",
				// "debug", dbarLoc);
				for (int i = 1; (i <= 12 && !bDone); i++) {
					inspmonth += 1;
					if (inspmonth > 12) {
						inspmonth = 1;
						inspyear++;
					}
					if (arr.contains("" + inspmonth)) bDone = true;
					// if (localDebug)
					// HS_Util.debug("loop set[" + i + "]. ] inspyear:[" + inspyear + "]inspmonth:[" + inspmonth + " inspday:[" + inspday +
					// "] bDone:["
					// + bDone + "]", "debug", dbarLoc);
				}
				// if (localDebug)
				// HS_Util.debug("After loop. ] inspyear:[" + inspyear + "] inspmonth:[" + inspmonth + "] inspday:[" + inspday + "] bDone:["
				// + bDone + "]",
				// "debug", dbarLoc);
				if (bDone) {
					inspdate = dateNumber(inspyear, inspmonth, inspday);
				}
			}
			if (localDebug) HS_Util.debug("1. inspdate:[" + inspdate.getDateOnly() + "]", "debug", dbarLoc);
			// inspdate.toJavaCal();
			Date idt2 = inspdate.toJavaDate();
			// if (Calendar.DOW_LOCAL == 1) inspdate.adjustDay(1);
			// if (Calendar.DAY_OF_WEEK == Calendar.SUNDAY) inspdate.adjustDay(1);
			if (idt2.getDay() == 0) inspdate.adjustDay(1);
			// if (Calendar.DAY_OF_WEEK == Calendar.SATURDAY) inspdate.adjustDay(2);
			if (idt2.getDay() == 6) inspdate.adjustDay(2);
			if (localDebug) HS_Util.debug("returning:[" + inspdate.getDateOnly() + "]", "debug", dbarLoc);
			result = HS_Util.getSession().createDateTime(inspdate.getDateOnly());
			return result;
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			if (HS_Util.isDebugServer()) e.printStackTrace();
			return null;
		}
	}

	private static DateTime dateNumber(int yr, int mm, int dd) {
		final String dbarLoc = "HS_CalculateNext.dateNumbet";
		// final boolean localDebug = (HS_Util.isDebugServer() || HS_Util.isDebugger());
		final boolean localDebug = false;

		DateTime result = null;
		try {
			// if (localDebug) HS_Util.debug("setting cal with yr:[" + yr + "] mm:[" + mm + "] dd:[" + dd + "]", "debug", dbarLoc);
			result = HS_Util.getSession().createDateTime("Today");
			result.setLocalDate(yr, mm - 1, dd);
			if (localDebug) HS_Util.debug("2. result is:[" + result.toString() + "]", "debug", dbarLoc);
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			result = null;
		}
		return result;
	}

	private static String getSubtypeInterval(Document facility) {
		// final boolean localDebug = (HS_Util.isDebugServer() || HS_Util.isDebugger());
		final boolean localDebug = false;
		final String dbarLoc = "HS_CalculateNext.getSubtypeInterval";

		String result = "";
		if (subinterval == null || subintervalnums == null) {
			HS_Util.debug("subinterval or subintervalnums Items were null...", "warn", dbarLoc);
			return result;
		}

		String sType = "";
		int indexresult = -1;
		if ("FoodFacility".equalsIgnoreCase(facility.getFormName())) {
			if ("Yes".equals(HS_Util.getMasterSettingsString("UseRiskRatingFreq"))) {
				// If facility.HasItem( "RiskRating" ) Then sType = facility.Type(0) & "-" & facility.RiskRating(0) Else sType = ""
				sType = "";
				if (facility.hasItem("RiskRating")) sType = facility.getItemValueString("Type") + "-" + facility.getItemValueString("RiskRating");
			} else if ("Ohio".equals(getDbDesign())) {
				sType = facility.getItemValueString("Type") + "-" + facility.getItemValueString("RiskRating");
			} else {
				sType = facility.getItemValueString("Type");
			}
			if (!"".equals(sType)) {
				if (subinterval.contains(sType.toLowerCase())) {
					indexresult = subinterval.indexOf(sType.toLowerCase());
					result = subintervalnums.get(indexresult);
				}
			}
		} else if ("PoolFacility".equalsIgnoreCase(facility.getFormName())) {
			if ("Arizona".equals(getDbDesign()) || "WestVirginia".equals(getDbDesign())) {
				sType = "";
				if (facility.hasItem("SubType")) sType = facility.getItemValueString("Type") + "-" + facility.getItemValueString("SubType");
			} else if ("Yes".equals(HS_Util.getMasterSettingsString("UsePoolRiskRatingFreq"))) {
				sType = facility.getItemValueString("Type") + "-" + facility.getItemValueString("RiskRating");
			} else {
				sType = facility.getItemValueString("Type");
			}
			if (!"".equals(sType)) {
				if (subinterval.contains(sType.toLowerCase())) {
					indexresult = subinterval.indexOf(sType.toLowerCase());
					if ("".equals(subintervalnums.get(0))) {
						result = "";
					} else if (subintervalnums.size() >= indexresult) {
						result = subintervalnums.get(indexresult);
					} else {
						result = "";
					}
				}
			}
		} else {
			sType = facility.getItemValueString("Type");
			if (subinterval.contains(sType.toLowerCase())) {
				indexresult = subinterval.indexOf(sType.toLowerCase());
				if ("".equals(subintervalnums.get(0))) {
					result = "";
				} else if (subintervalnums.size() >= indexresult) {
					result = subintervalnums.get(indexresult);
				} else {
					result = "";
				}
			}
		}

		if (localDebug) HS_Util.debug("sType is:[" + sType + "] result:[" + result + "]", "debug", dbarLoc);
		return result;
	}

	private static Item getLastDate(Document facility, Document lastRoutineInspection) {
		// final boolean localDebug = (HS_Util.isDebugServer() || HS_Util.isDebugger());
		final boolean localDebug = false;
		final String dbarLoc = "HS_CalculateNext.getLastDate";

		Item result = null;
		if (lastRoutineInspection != null) {
			if (localDebug) HS_Util.debug("lastRoutineInspection.hasItem(InspectionDate):[" + lastRoutineInspection.hasItem("InspectionDate") + "]", "debug", dbarLoc);
			if (lastRoutineInspection.hasItem("InspectionDate")) {
				result = lastRoutineInspection.getFirstItem("InspectionDate");
				if (localDebug) HS_Util.debug("result obtained from lastRoutineInspection.InspectionDate:[" + result.getText() + "]", "debug", dbarLoc);
			}
		} else {
			if (localDebug) HS_Util.debug("lastRoutineInspection is null", "debug", dbarLoc);
		}
		if (result == null) {
			if (localDebug) HS_Util.debug("Looking for LastRoutineInsp in facility:[" + facility.hasItem("LastRoutineInsp") + "]", "debug", dbarLoc);
			if (facility.hasItem("LastRoutineInsp")) {
				result = facility.getFirstItem("LastRoutineInsp");
				if (localDebug) HS_Util.debug("result obtained from facility.LastRoutineInsp:[" + result.getText() + "]", "debug", dbarLoc);
				if ("".equals(result.getText())) result = null;
			}
		}
		if ("Arizona".equals(getDbDesign())) {
			if (result == null) {
				if (facility.hasItem("LastInspection")) {
					result = facility.getFirstItem("LastInspection");
					if ("".equals(result.getText())) result = null;
				}
			}
			if (result == null) {
				if (facility.hasItem("PermitDate")) {
					result = facility.getFirstItem("PermitDate");
					if ("".equals(result.getText())) result = null;
				}
			}
			if (result == null) {
				if (facility.hasItem("ApplicationDate")) {
					result = facility.getFirstItem("ApplicationDate");
					if ("".equals(result.getText())) result = null;
				}
			}
		}
		if (localDebug) {
			if (result != null) HS_Util.debug("Returning item:[" + result.getName() + "] value:[" + result.getText() + "]", "debug", dbarLoc);
			if (result == null) HS_Util.debug("Returning null item", "debug", dbarLoc);
		}
		return result;
	}

	private static int inspectionInterval(Document facility) {
		// final boolean localDebug = (HS_Util.isDebugServer() || HS_Util.isDebugger());
		final boolean localDebug = false;
		final String dbarLoc = "HS_CalculateNext.inspectionInterval";

		int result = 364;
		subinterval = new Vector<String>();
		subintervalnums = new Vector<String>();
		HSForm hsForm = null;
		Double gsFreq = null;
		try {
			hsForm = HSForm.valueOf(facility.getFormName()); // surround with try/catch
		} catch (Exception e) {
			HS_Util.debug(facility.getFormName() + " caused error: " + e.toString(), "error", dbarLoc);
			return result;
		}
		switch (hsForm) {
		case FoodReport:
		case FoodFacility:
			gsFreq = HS_Util.getGlobalSettingsDouble("FoodFrequency");
			if (gsFreq != 0) result = gsFreq.intValue();
			subinterval = HS_Util.getGlobalSettingsVector("FoodSubtypes");
			subintervalnums = HS_Util.getGlobalSettingsVector("FoodSubtypesFrequency");

			break;
		case WaterReport:
		case WaterFacility:
			gsFreq = HS_Util.getGlobalSettingsDouble("WaterFrequency");
			if (gsFreq != 0) result = gsFreq.intValue();
			subinterval = HS_Util.getGlobalSettingsVector("WaterSubtypes");
			subintervalnums = HS_Util.getGlobalSettingsVector("WaterSubtypesFrequency");
			break;
		case PoolReport:
		case PoolFacility:
			gsFreq = HS_Util.getGlobalSettingsDouble("PoolFrequency");
			if (gsFreq != 0) result = gsFreq.intValue();
			subinterval = HS_Util.getGlobalSettingsVector("PoolSubtypes");
			subintervalnums = HS_Util.getGlobalSettingsVector("PoolSubtypesFrequency");
			break;
		case HotelReport:
		case HotelFacility:
			gsFreq = HS_Util.getGlobalSettingsDouble("HotelFrequency");
			if (gsFreq != 0) result = gsFreq.intValue();
			subinterval = HS_Util.getGlobalSettingsVector("HotelSubtypes");
			subintervalnums = HS_Util.getGlobalSettingsVector("HotelSubtypesFrequency");
			break;
		case GeneralReport:
		case GeneralFacility:
			gsFreq = HS_Util.getGlobalSettingsDouble("GeneralFrequency");
			if (gsFreq != 0) result = gsFreq.intValue();
			subinterval = HS_Util.getGlobalSettingsVector("GeneralSubtypes");
			subintervalnums = HS_Util.getGlobalSettingsVector("GeneralSubtypesFrequency");
			break;
		case SummerCampReport:
		case SummerCampFacility:
			gsFreq = HS_Util.getGlobalSettingsDouble("SummerFrequency");
			if (gsFreq != 0) result = gsFreq.intValue();
			subinterval = HS_Util.getGlobalSettingsVector("SummerSubtypes");
			subintervalnums = HS_Util.getGlobalSettingsVector("SummerSubtypesFrequency");
			break;
		case LaborCampReport:
		case LaborCampFacility:
			gsFreq = HS_Util.getGlobalSettingsDouble("IndustrialFrequency");
			if (gsFreq != 0) result = gsFreq.intValue();
			// subinterval = HS_Util.getGlobalSettingsVector("FoodSubtypes");
			// subintervalnums = HS_Util.getGlobalSettingsVector("FoodSubtypesFrequency");
			break;
		case CampReport:
		case CampFacility:
			gsFreq = HS_Util.getGlobalSettingsDouble("CampgroundFrequency");
			if (gsFreq != 0) result = gsFreq.intValue();
			subinterval = HS_Util.getGlobalSettingsVector("CampgroundSubtypes");
			subintervalnums = HS_Util.getGlobalSettingsVector("CampgroundSubtypesFrequency");
			break;
		case BodyartReport:
		case BodyartFacility:
			gsFreq = HS_Util.getGlobalSettingsDouble("BodyartFrequency");
			if (gsFreq != 0) result = gsFreq.intValue();
			subinterval = HS_Util.getGlobalSettingsVector("BodyartSubtypes");
			subintervalnums = HS_Util.getGlobalSettingsVector("BodyartSubtypesFrequency");
			break;
		case DairyReport:
		case DairyFacility:
			gsFreq = HS_Util.getGlobalSettingsDouble("DairyFrequency");
			if (gsFreq != 0) result = gsFreq.intValue();
			subinterval = HS_Util.getGlobalSettingsVector("DairySubtypes");
			subintervalnums = HS_Util.getGlobalSettingsVector("DairySubtypesFrequency");
			break;
		case AviaryReport:
		case AviaryFacility:
			gsFreq = HS_Util.getGlobalSettingsDouble("AviaryFrequency");
			if (gsFreq != 0) result = gsFreq.intValue();
			// subinterval = HS_Util.getGlobalSettingsVector("AviarySubtypes");
			// subintervalnums = HS_Util.getGlobalSettingsVector("AviarySubtypesFrequency");
			break;
		case GarbageReport:
		case GarbageFacility:
			gsFreq = HS_Util.getGlobalSettingsDouble("GarbageFrequency");
			if (gsFreq != 0) result = gsFreq.intValue();
			// subinterval = HS_Util.getGlobalSettingsVector("GarbageSubtypes");
			// subintervalnums = HS_Util.getGlobalSettingsVector("GarbageSubtypesFrequency");
			break;
		case SchoolReport:
		case SchoolFacility:
			gsFreq = HS_Util.getGlobalSettingsDouble("SchoolFrequency");
			if (gsFreq != 0) result = gsFreq.intValue();
			// subinterval = HS_Util.getGlobalSettingsVector("SchoolSubtypes");
			// subintervalnums = HS_Util.getGlobalSettingsVector("SchoolSubtypesFrequency");
			break;
		case JailReport:
		case JailFacility:
			gsFreq = HS_Util.getGlobalSettingsDouble("JailFrequency");
			if (gsFreq != 0) result = gsFreq.intValue();
			// subinterval = HS_Util.getGlobalSettingsVector("JailSubtypes");
			// subintervalnums = HS_Util.getGlobalSettingsVector("JailSubtypesFrequency");
			break;
		case BeachReport:
		case BeachFacility:
			gsFreq = HS_Util.getGlobalSettingsDouble("BeachFrequency");
			if (gsFreq != 0) result = gsFreq.intValue();
			// subinterval = HS_Util.getGlobalSettingsVector("BeachSubtypes");
			// subintervalnums = HS_Util.getGlobalSettingsVector("BeachSubtypesFrequency");
			break;
		case MobileReport:
		case MobileHomeFacility:
			gsFreq = HS_Util.getGlobalSettingsDouble("MobileHomeFrequency");
			if (gsFreq != 0) result = gsFreq.intValue();
			// subinterval = HS_Util.getGlobalSettingsVector("MobileHomeSubtypes");
			// subintervalnums = HS_Util.getGlobalSettingsVector("MobileHomeSubtypesFrequency");
			break;
		case SolidWasteReport:
		case SolidWasteFacility:
			gsFreq = HS_Util.getGlobalSettingsDouble("SolidWasteFrequency");
			if (gsFreq != 0) result = gsFreq.intValue();
			// subinterval = HS_Util.getGlobalSettingsVector("SolidWasteSubtypes");
			// subintervalnums = HS_Util.getGlobalSettingsVector("SolidWasteSubtypesFrequency");
			break;
		case RadiationReport:
		case RadiationFacility:
			gsFreq = HS_Util.getGlobalSettingsDouble("RadiationFrequency");
			if (gsFreq != 0) result = gsFreq.intValue();
			// subinterval = HS_Util.getGlobalSettingsVector("RadiationSubtypes");
			// subintervalnums = HS_Util.getGlobalSettingsVector("RadiationSubtypesFrequency");
			break;
		case FairgroundReport:
		case FairgroundFacility:
			gsFreq = HS_Util.getGlobalSettingsDouble("FairgroundsFrequency");
			if (gsFreq != 0) result = gsFreq.intValue();
			// subinterval = HS_Util.getGlobalSettingsVector("FairgroundSubtypes");
			// subintervalnums = HS_Util.getGlobalSettingsVector("FairgroundSubtypesFrequency");
			break;
		case HousingReport:
		case HousingFacility:
			gsFreq = HS_Util.getGlobalSettingsDouble("HousingFrequency");
			if (gsFreq != 0) result = gsFreq.intValue();
			// subinterval = HS_Util.getGlobalSettingsVector("HusingSubtypes");
			// subintervalnums = HS_Util.getGlobalSettingsVector("HousingSubtypesFrequency");
			break;
		case AgricultureReport:
		case AgricultureFacility:
			gsFreq = HS_Util.getGlobalSettingsDouble("AgricultureFrequency");
			if (gsFreq != 0) result = gsFreq.intValue();
			// subinterval = HS_Util.getGlobalSettingsVector("AgricultureSubtypes");
			// subintervalnums = HS_Util.getGlobalSettingsVector("AgricultureSubtypesFrequency");
			break;
		case ChildcareReport:
		case ChildcareFacility:
			gsFreq = HS_Util.getGlobalSettingsDouble("ChildcareFrequency");
			if (gsFreq != 0) result = gsFreq.intValue();
			// subinterval = HS_Util.getGlobalSettingsVector("ChildcareSubtypes");
			// subintervalnums = HS_Util.getGlobalSettingsVector("ChildcareSubtypesFrequency");
			break;
		case HealthUnitReport:
		case HealthUnitFacility:
			gsFreq = HS_Util.getGlobalSettingsDouble("HealthUnitFrequency");
			if (gsFreq != 0) result = gsFreq.intValue();
			// subinterval = HS_Util.getGlobalSettingsVector("HealthUnitSubtypes");
			// subintervalnums = HS_Util.getGlobalSettingsVector("HealthUnitSubtypesFrequency");
			break;
		case ShellfishReport:
		case ShellfishFacility:
			gsFreq = HS_Util.getGlobalSettingsDouble("ShellfishFrequency");
			if (gsFreq != 0) result = gsFreq.intValue();
			// subinterval = HS_Util.getGlobalSettingsVector("ShellfishSubtypes");
			// subintervalnums = HS_Util.getGlobalSettingsVector("ShellfishSubtypesFrequency");
			break;
		default:
			HS_Util.debug("Form not supported:[" + facility.getFormName(), "warn", dbarLoc);
			break;
		}
		if (subinterval.size() > 0) {
			Vector<String> newSubTypes = new Vector<String>();
			for (String st : subinterval) {
				newSubTypes.add(st.toLowerCase());
			}
			subinterval = newSubTypes;
		}
		if (localDebug) HS_Util.debug("Global Settings Freq: [" + gsFreq + "] returning:[" + result + "]", "debug", dbarLoc);
		// if (localDebug) HS_Util.debug("returning subinterval:" + subinterval, "debug", dbarLoc);
		// if (localDebug) HS_Util.debug("returning subintervalnums:" + subintervalnums, "debug", dbarLoc);

		return result;
	}

	private static void checkOverDueByDoc(Document source) {
		// final boolean localDebug = (HS_Util.isDebugServer() || HS_Util.isDebugger());
		final boolean localDebug = false;
		final String dbarLoc = "HS_CalculateNext.checkOverDueByDoc";
		if (localDebug) HS_Util.debug("FUNCTION NOT CODED", "warn", dbarLoc);
		// TODO Auto-generated method stub

	}

	private static void calculateNextInspectionVDH(Document source) {
		// final boolean localDebug = (HS_Util.isDebugServer() || HS_Util.isDebugger());
		// final boolean localDebug = (HS_Util.isDebugServer() || HS_Util.isDebugger());
		final boolean localDebug = false;
		final String dbarLoc = "HS_CalculateNext.calculateNextInspectionVDH";
		if (localDebug) HS_Util.debug("FUNCTION NOT CODED", "warn", dbarLoc);
		// TODO Auto-generated method stub

	}

	private static void calculateNextInspAZ(Document source) {
		// final boolean localDebug = (HS_Util.isDebugServer() || HS_Util.isDebugger());
		final boolean localDebug = false;
		final String dbarLoc = "HS_CalculateNext.calculateNextInspAZ";
		if (localDebug) HS_Util.debug("FUNCTION NOT CODED", "warn", dbarLoc);

		// TODO Auto-generated method stub

	}
}
