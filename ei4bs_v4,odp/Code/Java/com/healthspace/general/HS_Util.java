package com.healthspace.general;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

import javax.faces.context.FacesContext;

import org.openntf.domino.ACL;
import org.openntf.domino.ACLEntry;
import org.openntf.domino.Database;
import org.openntf.domino.DateTime;
import org.openntf.domino.Document;
import org.openntf.domino.Item;
import org.openntf.domino.Name;
import org.openntf.domino.RichTextItem;
import org.openntf.domino.Session;
import org.openntf.domino.View;
import org.openntf.domino.utils.DominoUtils;
import org.openntf.domino.utils.Factory;
import org.openntf.domino.utils.Factory.SessionType;
import org.openntf.xsp.debugtoolbar.beans.DebugToolbarBean;

import com.healthspace.tools.JSFUtil;
import com.ibm.domino.services.util.JsonWriter;
import com.ibm.jscript.types.FBSValue;
import com.ibm.xsp.designer.context.XSPContext;
import com.ibm.xsp.extlib.util.ExtLibUtil;

public class HS_Util {

	private enum HSForm {
		FoodReport, FoodFacility, WaterReport, WaterFacility, PoolReport, PoolFacility, HotelReport, HotelFacility, GeneralReport, GeneralFacility, SummerCampReport, SummerCampFacility, LaborCampReport, LaborCampFacility, CampReport, CampFacility, BodyartReport, BodyartFacility, DairyReport, DairyFacility, AviaryReport, AviaryFacility, GarbageReport, GarbageFacility, SchoolReport, SchoolFacility, JailReport, JailFacility, BeachReport, BeachFacility, MobileReport, MobileHomeFacility, SolidWasteReport, SolidWasteFacility, RadiationReport, RadiationFacility, FairgroundReport, FairgroundFacility, HousingReport, HousingFacility, AgricultureReport, AgricultureFacility, ChildcareReport, ChildcareFacility, HealthUnitReport, HealthUnitFacility, ShellfishReport, ShellfishFacility, FoodSafetyReport, MobileHomeReport, SepticRemovalReport, MarinaReport, RetailReport, TextileReport, PersonalServiceReport, TanningReport, AgricultureTVFacility, FoodTVFacility
	}

	public static void addProperty(JsonWriter jw, String propertyName, FBSValue value) throws IOException {
		jw.startProperty(propertyName);
		jw.outStringLiteral(value.stringValue());
		jw.endProperty();
	}

	public static void addProperty(JsonWriter jw, String name, String value) throws IOException {
		jw.startProperty(name);
		jw.outStringLiteral(value);
		jw.endProperty();

	}

	public static void copyItemToDocument(Document source, String sourceFieldName, Document dest, String destFieldName) {
		if ( (source == null || dest == null) ) return;
		if ( source.hasItem(sourceFieldName) ) {
			Item item = source.getFirstItem(sourceFieldName);
			item.copyItemToDocument(dest, destFieldName);
		} else {
			dest.replaceItemValue(destFieldName, "");
		}
	}

	public static DateTime cvrtStringToDateTime(String dstr) {
		Session sess = HS_Util.getSession();
		DateTime wdt = sess.createDateTime(cvrtStringToDate(dstr));
		wdt.setAnyTime();
		return wdt;
	}

	public static Date cvrtStringToDate(String dtstr) {
		Date wdt = null;
		// Calendar cal = Calendar.getInstance();
		String[] dtparts = null;
		// int yridx = 0;
		// int mmidx = 1;
		// int ddidx = 2;
		// int year = 0;
		// int mon = 0;
		// int date = 0;
		//		
		if ( dtstr.contains(", ") ) {
			dtstr = dtstr.substring(dtstr.indexOf(", ") + 2);
		}

		if ( dtstr.contains("-") ) {
			dtparts = dtstr.split("-");
			if ( !HS_Util.isStringNumeric(dtparts[1]) ) {
				// Assumes dd-MMM-yyyy format.
				try {
					wdt = new SimpleDateFormat("dd-MMM-yyyy", Locale.US).parse(dtstr);
				} catch (ParseException e) {
					wdt = null;
				}
			} else {
				try {
					wdt = new SimpleDateFormat("yyyy-dd-MM", Locale.US).parse(dtstr);
				} catch (ParseException e) {
					wdt = null;
				}
			}
		} else if ( dtstr.contains("/") ) { // Assumes MM/DD/YYYY format
			try {
				wdt = new SimpleDateFormat("MM/dd/yyyy", Locale.US).parse(dtstr);
			} catch (ParseException e) {
				wdt = null;
			}
			return wdt;
		} else {
			// dtparts = "".split(":");
			try {
				wdt = new SimpleDateFormat("dd:MM:yyyy", Locale.US).parse(dtstr);
			} catch (ParseException e) {
				wdt = null;
			}
			return wdt;
		}
		/*
		 * if (dtparts.length == 3) {
		 * 
		 * 
		 * year = Float.valueOf(dtparts[yridx]).intValue(); mon = Float.valueOf(dtparts[mmidx]).intValue() - 1; date = Float.valueOf(dtparts[ddidx]).intValue(); cal.set(year, mon, date); wdt = cal.getTime(); }
		 * else if (wdt == null) { //wdt = new Date(); wdt = null; }****
		 */
		return wdt;

	}

	public static DateTime cvrtStringToTime(String tstr) {
		DateTime wdt = getSession().createDateTime(tstr);
		wdt.isAnyDate();

		return wdt;
	}

	public static String cvrtToSpace(String inp) {
		if ( inp == null ) return " ";
		if ( "".equals(inp) ) return " ";
		return inp;
	}

	public static void debug(String msg) {
		debug(msg, "debug");
	}

	public static void debug(String msg, String mtype) {
		debug(msg, mtype, "");
	}

	public static void debug(String msg, String mtype, String msgContext) {
		// import org.openntf.xsp.debugtoolbar.beans.DebugToolbarBean;
		String path = getSession().getCurrentDatabase().getFilePath();

		// DebugToolbar dBar = DebugToolbar.get();
		try {
			// System.out.print(msg);
			if ( "debug".equalsIgnoreCase(mtype) ) {
				DebugToolbarBean.get().debug(msg, msgContext);
			} else if ( "error".equalsIgnoreCase(mtype) ) {
				DebugToolbarBean.get().error(msg, msgContext);
				if ( isDebugServer() ) System.out.print("[" + path + "." + msgContext + "] - " + msg);
			} else if ( "info".equalsIgnoreCase(mtype) ) {
				DebugToolbarBean.get().info(msg, msgContext);
			} else if ( "warn".equalsIgnoreCase(mtype) ) {
				DebugToolbarBean.get().warn(msg, msgContext);
				if ( isDebugServer() ) System.out.print("[" + path + "." + msgContext + "] - " + msg);
			} else
				DebugToolbarBean.get().debug(msg, msgContext);
		} catch (Exception e) {
			System.out.print("[" + path + ".HS_Util.debug - ERROR OCCURRED DURING DEBUG: " + e.toString());
			System.out.print(msg);
		}
	}

	public static void debug(String msg, String mtype, String msgContext, Date sdt) {
		Date edt = new Date();
		String lmsg = msg + " msecs:[" + (edt.getTime() - sdt.getTime()) + "]";
		debug(lmsg, mtype, msgContext);
	}

	public static String debugCalledFrom() {
		String result = "";

		StackTraceElement[] ste = Thread.currentThread().getStackTrace();
		result = "called from:[" + ste[4].getClassName() + "." + ste[4].getMethodName() + "]";

		return result;
	}

	public static String emsecs(Date sdt) {
		Date edt = new Date();
		String lmsg = " msecs:[" + (edt.getTime() - sdt.getTime()) + "]";
		return lmsg;
	}

	public static String formatDate(Date jdt, String mask) {
		// final boolean localDebug = false;
		final String dbarLoc = "HS_Util.formatDate";
		String result = "";
		if ( jdt == null ) return "";
		Calendar cal = Calendar.getInstance();
		try {
			cal.add(Calendar.DATE, 1);

			SimpleDateFormat format1 = new SimpleDateFormat(mask);
			result = format1.format(jdt);
		} catch (Exception e) {
			HS_Util.debug("for jdt:[" + jdt.toString() + "] mask:[" + mask + "] ERROR: " + e.toString(), "error", dbarLoc);
		}
		return result;
	}

	public static String getApiVersion() {
		return Factory.getVersion();
	}

	public static Map<String, Object> getAppSettingBuildASData(String askey) {
		final boolean localDebug = false;
		final String dbarLoc = "HS_Util.getAppSettingBuildASData";
		final String sviewname = "XpagesSettingsView";
		Map<String, Object> asdata = new HashMap<String, Object>();
		try {
			if ( localDebug ) HS_Util.debug("building new map.", "debug", dbarLoc);
			Database db = HS_Util.getSession().getCurrentDatabase();
			View settingsView = db.getView(sviewname);
			if ( settingsView == null ) throw (new Exception("Could not access view: '" + sviewname + "'"));
			Document settingsDoc = settingsView.getFirstDocument();
			if ( settingsDoc == null ) throw (new Exception("Could not access a document in view: '" + sviewname + "'"));

			Vector<Item> items = settingsDoc.getItems();
			for (Item item : items) {
				if ( !item.getName().startsWith("$") ) asdata.put(item.getName(), item.getValues());
			}
			if ( localDebug ) HS_Util.debug("added [" + asdata.size() + "] entries to map..", "debug", dbarLoc);

		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			Vector<String> evector = new Vector<String>();
			evector.add(e.toString());
			asdata.put("error", evector);
		}
		ExtLibUtil.getApplicationScope().put(askey, asdata);
		if ( localDebug ) HS_Util.debug("applicationScope updated", "debug", dbarLoc);
		return asdata;
	}

	public static double getAppSettingDouble(String fname) {
		double result = 0;
		Vector<Object> fvalues = getAppSettingsValuesAsObjects(fname);
		try {
			if ( (fvalues != null) && (fvalues.size() > 0) ) {
				Object fvalue = fvalues.get(0);
				result = Double.parseDouble(fvalue.toString());
			}
		} catch (Exception e) {
			if ( isDebugServer() ) debug(e.toString(), "error", "HS_Util.getAppSettingDouble");
			// ignore errors and return a 0
		}

		return result;
	}

	public static String getAppSettingString(String fname) {
		String result = "";
		Vector<String> fvalues = getAppSettingsValues(fname);
		try {
			if ( (fvalues != null) && (fvalues.size() > 0) ) {
				result = fvalues.get(0);
			}
		} catch (Exception e) {
			if ( isDebugServer() ) debug(e.toString(), "error", "HS_Util.getAppSettingString");
			// ignore errors and return a null string...
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static Vector<String> getAppSettingsValues(String fname) {
		// final boolean localDebug = false;
		final String dbarLoc = "HS_Util.getAppSettingsValues";
		Vector<String> fvalues = new Vector<String>();
		Map<String, Object> asdata = null;
		Vector<String> asdatanf = null;
		// Map<String, Vector<String>> asdata = null;
		final String askey = "HS_AppSettings";
		final String askeynf = "HS_AppSettingsNotFound";
		// final String sviewname = "XpagesSettingsView";
		Map<String, Object> ascope = ExtLibUtil.getApplicationScope();
		if ( ascope.containsKey(askey) ) {
			asdata = (Map<String, Object>) ascope.get(askey);
			if ( !(asdata.containsKey(fname)) ) {
				asdata = null;
			}
		}
		if ( ascope.containsKey(askeynf) ) {
			asdatanf = (Vector<String>) ascope.get(askeynf);
		} else {
			asdatanf = new Vector<String>();
			ascope.put(askeynf, asdatanf);
		}

		if ( asdata == null ) {
			// asdata = new HashMap<String, Object>();
			asdata = getAppSettingBuildASData(askey);
		}
		if ( asdata.containsKey(fname) ) {
			fvalues = (Vector<String>) asdata.get(fname);
		} else {
			try {
				String dbas = (!asdata.containsKey("debug_AppSettings")) ? "Yes" : (((Vector<String>) asdata.get("debug_AppSettings")).size() == 0) ? "" : ((Vector<String>) asdata.get("debug_AppSettings"))
										.get(0);
				if ( "Yes".equals(dbas) && !asdatanf.contains(fname) ) {
					HS_Util.debug("could not find item [" + fname + "] in map.", "warn", dbarLoc);
					asdatanf.add(fname);
					ascope.put(askeynf, asdatanf);
				}
			} catch (Exception e) {
				HS_Util.debug("ERROR showing issue with [" + fname + "]: " + e.toString() + "  asdata.get(\"debug_AppSettings\") is a:[" + asdata.get("debug_AppSettings").getClass().getName() + "]", "error",
										dbarLoc);
			}
		}
		return fvalues;
	}

	@SuppressWarnings("unchecked")
	public static Vector<Object> getAppSettingsValuesAsObjects(String fname) {
		// final boolean localDebug = false;
		final String dbarLoc = "HS_Util.getAppSettingsValues";
		Vector<Object> fvalues = new Vector<Object>();
		Map<String, Object> asdata = null;
		final String askey = "HS_AppSettings";
		Map<String, Object> ascope = ExtLibUtil.getApplicationScope();
		if ( ascope.containsKey(askey) ) {
			asdata = (Map<String, Object>) ascope.get(askey);
			if ( !(asdata.containsKey(fname)) ) {
				asdata = null;
			}
		}
		if ( asdata == null ) {
			asdata = getAppSettingBuildASData(askey);
		}
		if ( asdata.containsKey(fname) ) {
			fvalues = (Vector<Object>) asdata.get(fname);
		} else {
			HS_Util.debug("could not find item [" + fname + "] in map.", "warn", dbarLoc);
		}
		return fvalues;
	}

	@SuppressWarnings("unchecked")
	public static String getColValAsString(Object cval) {
		String result = "";
		String cname = cval.getClass().getName();
		Object tobj = null;
		if ( cname.contains("Vector") ) {
			tobj = ((Vector<Object>) cval).get(0);
			while (tobj.getClass().getName().contains("Vector")) {
				tobj = ((Vector<Object>) tobj).get(0);
			}
			result = (String) tobj;
		} else {
			result = (String) cval;
		}
		return result;
	}

	public static Document getDocumentByUNID(Database db, String unid, Boolean allowDeleted) {
		final boolean localDebug = false;
		final String dbarLoc = "HS_Util.getDocumentByUNID";
		Document doc = null;
		if ( db == null ) {
			if ( localDebug ) debug("Database is null!", "error", dbarLoc);
			return doc;
		}
		try {
			doc = db.getDocumentByUNID(unid);
			if ( !(doc == null) ) {
				if ( doc.isValid() && !"".equals(doc.getUniversalID()) ) {
					// not a deletion stub
					if ( !allowDeleted ) {
						// 'Dont return soft deletions
						if ( doc.hasItem("DELETED") ) {
							if ( !"".equals(doc.getFirstItem("DELETED").getText()) ) {
								if ( localDebug ) debug("for unid:[" + unid + "] doc appears to be deleted.", "warn", dbarLoc);
								doc = null;
							}
						}
					}
				} else {
					debug("for unid:[" + unid + "] doc.isValid():[" + doc.isValid() + "]", "warn", dbarLoc);
					doc = null;
				}
			}

		} catch (Exception e) {
			debug(e.toString(), "error", dbarLoc);
			doc = null;
		}
		return doc;
	}

	public static String getEstablishmentType(String form) {
		final String dbarLoc = "HS_Util. getEstablishmentType";
		String etype = "";
		HSForm hsForm = null;
		try {
			hsForm = HSForm.valueOf(form); // surround with try/catch
		} catch (Exception e) {
			HS_Util.debug(form + " caused error: " + e.toString(), "error", dbarLoc);
			return etype;
		}
		switch (hsForm) {
		case AgricultureReport:
		case AgricultureFacility:
			etype = "Foodservice";
			break;
		case BeachReport:
			etype = "Beach";
			break;
		case BodyartReport:
			etype = "Body Art";
			break;
		case LaborCampReport:
			etype = "Migrant Labor Camp";
			break;
		case ChildcareReport:
			etype = "Child Care Facility";
			break;
		case DairyReport:
			etype = "Dairy";
			break;
		case FairgroundReport:
			etype = "Fairground Facility";
			break;
		case FoodReport:
		case FoodFacility:
			etype = "Foodservice";
			break;
		case FoodSafetyReport:
			etype = "Foodservice";
			break;
		case GarbageReport:
			etype = "Garbage Hauler";
			break;
		case GeneralReport:
			etype = "General Facility";
			break;
		case HotelFacility:
		case HotelReport:
			etype = "Lodging";
			break;
		case HousingReport:
			etype = "Housing Facility";
			break;
		case JailReport:
			etype = "Jail";
			break;
		case MobileHomeReport:
			etype = "Mobile Home Park";
			break;
		case RadiationReport:
			etype = "Radiation";
			break;
		case CampFacility:
		case CampReport:
			etype = "Campground";
			break;
		case PoolFacility:
		case PoolReport:
			etype = "Recreational Water";
			break;
		case SummerCampFacility:
		case SummerCampReport:
			etype = "Recreational Educational Camp";
			break;
		case SchoolReport:
			etype = "School Facility";
			break;
		case SolidWasteReport:
			etype = "Solid Waste";
			break;
		case SepticRemovalReport:
			etype = "Sewage Removal";
			break;
		case WaterReport:
			etype = "Drinking Water";
			break;
		case MarinaReport:
			etype = "General Facility";
			break;
		case RetailReport:
			etype = "Retailer";
			break;
		case TextileReport:
			etype = "Bedding/Upholstery";
			break;
		case PersonalServiceReport:
			etype = "Personal Service";
			break;
		case ShellfishReport:
			etype = "Shellfish Processing Plant";
			break;
		case TanningReport:
			etype = "Tanning Facility";
		case AgricultureTVFacility:
			etype = "Agriculture Temp Vendor";
			break;
		case FoodTVFacility:
			etype = "Food Temp Vendor";
			break;
		}
		return etype;
	}

	public static String getExtLibVersion() {
		String result = "version not found";
		try {
			result = ExtLibUtil.getExtLibVersion();
		} catch (Exception e) {
			result += " error: " + e.toString();
		}
		return result;
	}

	public static String[] getGlobalSettingsArray(String fname) {
		String[] result = null;
		try {
			Vector<String> w1 = getGlobalSettingsVector(fname);
			if ( w1 == null ) return result;
			if ( w1.size() == 0 ) {
				result = new String[] { "" };
				return result;
			}
			result = w1.toArray(new String[w1.size()]);
			return result;
		} catch (Exception e) {
			result = new String[] { "error: " + e.toString() };
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static Double getGlobalSettingsDouble(String fname) {
		Double result = new Double(0);
		try {
			Object tobj = HS_profiles.get("eiRoot", "GlobalSettings", fname);
			if ( tobj == null ) {
				// nothing
			} else if ( ((Vector<Double>) tobj).size() < 1 ) {
				// nothing
			} else {
				result = ((Vector<Double>) tobj).get(0);
			}
		} catch (Exception e) {
			// do nothing
		}
		return result;
	}

	public static String getGlobalSettingsString(String fname) {
		return HS_Util.getGlobalSettingsString(fname, false);
	}

	@SuppressWarnings("unchecked")
	public static String getGlobalSettingsString(String fname, boolean localDebug) {
		// final boolean localDebug = true;
		final String dbarLoc = "HS_Util.getGlobalSettingsString";
		String result = "";
		try {
			Object tobj = HS_profiles.get("eiRoot", "GlobalSettings", fname, localDebug);

			if ( tobj == null ) {
				if ( localDebug ) HS_Util.debug("field:[" + fname + "] returned a null value.", "debug", dbarLoc);
			} else {
				if ( localDebug ) HS_Util.debug("field:[" + fname + "] returned a:[" + tobj.getClass().getName() + "]", "debug", dbarLoc);
				if ( ((Vector<String>) tobj).size() < 1 ) {
					if ( localDebug ) HS_Util.debug("field:[" + fname + "] returned an empty vector.", "debug", dbarLoc);
				} else {
					result = ((Vector<String>) tobj).get(0);
				}
			}
		} catch (Exception e) {
			if ( localDebug ) HS_Util.debug("field:[" + fname + "] returned an error:[" + e.toString() + "]", "error", dbarLoc);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static Vector<String> getGlobalSettingsVector(String fname) {
		Vector<String> result = new Vector<String>();
		try {
			Object tobj = HS_profiles.get("eiRoot", "GlobalSettings", fname);
			if ( tobj == null ) {
				// nothing
			} else if ( ((Vector<String>) tobj).size() < 1 ) {
				// nothing
			} else {
				result = ((Vector<String>) tobj);
			}
		} catch (Exception e) {
			// do nothing
		}
		return result;
	}

	public static Vector<String> getKeywordValues(String key) {
		Vector<String> result = new Vector<String>();

		try {
			View kwview = HS_Util.getSession().getCurrentDatabase().getView("code.keywords");
			if ( kwview == null ) throw (new Exception("Unable to access view \"code.keywords\""));
			Document kwdoc = kwview.getFirstDocumentByKey(key);
			if ( kwdoc == null ) {

			} else {
				Vector<Object> kwvals = kwdoc.getItemValue("keywordValues");
				if ( kwvals.size() == 0 ) {
					result.add("");
				} else {
					Iterator<Object> kwitr = kwvals.iterator();
					while (kwitr.hasNext()) {
						Object to = kwitr.next();
						result.add((String) to);
					}
				}
			}
		} catch (Exception e) {
			result.add(key + " got error!" + e.toString());
		}
		if ( result.size() == 0 ) result.add(key + " not found");
		return result;

	}

	@SuppressWarnings("unchecked")
	public static Object getMasterSettingsObject(String fname) {
		try {
			return ((Vector<Object>) HS_profiles.get("eiRoot", "MasterSettings", fname)).get(0);
		} catch (Exception e) {
			return "";
		}
	}

	@SuppressWarnings("unchecked")
	public static String getMasterSettingsString(String fname) {
		String result = "";
		try {
			Object tobj = HS_profiles.get("eiRoot", "MasterSettings", fname);
			if ( tobj == null ) {
				// nothing
			} else if ( ((Vector<String>) tobj).size() < 1 ) {
				// nothing
			} else {
				result = ((Vector<String>) tobj).get(0);
			}
		} catch (Exception e) {
			// do nothing
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static Vector<String> getMasterSettingsVector(String fname) {
		Vector<String> result = new Vector<String>();
		try {
			Object tobj = HS_profiles.get("eiRoot", "MasterSettings", fname);
			if ( tobj == null ) {
				// nothing
			} else if ( ((Vector<String>) tobj).size() < 1 ) {
				// nothing
			} else {
				result = ((Vector<String>) tobj);
			}
		} catch (Exception e) {
			// do nothing
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static String getProfileString(String pname, String fname) {
		String result = "";
		try {
			Object tobj = HS_profiles.get("eiRoot", pname, fname);
			if ( tobj == null ) {
				// nothing
			} else if ( ((Vector<String>) tobj).size() < 1 ) {
				// nothing
			} else {
				result = ((Vector<String>) tobj).get(0);
			}
		} catch (Exception e) {
			// do nothing
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static Vector<String> getProfileVector(String pname, String fname) {
		Vector<String> result = new Vector<String>();
		try {
			Object tobj = HS_profiles.get("eiRoot", pname, fname);
			if ( tobj == null ) {
				// nothing
			} else if ( ((Vector<String>) tobj).size() < 1 ) {
				// nothing
			} else {
				result = ((Vector<String>) tobj);
			}
		} catch (Exception e) {
			// do nothing
		}
		return result;
	}

	public static String getODAFactoryVersion() {
		String result = "version not found";

		try {
			result = Factory.getVersion();
		} catch (Exception e) {
			result += " error: " + e.toString();
		}
		return result;
	}

	public static String getPageTitle() {
		String rslt = "";
		final boolean localDebug = false;
		final String dbarLoc = "HS_Util.getPageTitle";

		String dflt = "";
		String whatToUse = "";
		try {
			dflt = Factory.getSession().getCurrentDatabase().getTitle();
			whatToUse = getAppSettingString("pageTitleSource");
			if ( "".equals(whatToUse) ) whatToUse = "globalSettings";

			if ( "globalSettings".equals(whatToUse) ) {
				String HRegion = getGlobalSettingsString("HealthRegion", localDebug);
				rslt = (("".equals(HRegion)) ? dflt : HRegion);
			} else if ( "dbtitle".equals(whatToUse) ) {
				rslt = dflt;
			} else if ( "custom".equals(whatToUse) ) {
				String custom = getAppSettingString("pageTitleCustom");
				if ( custom.contains("<<host>>") ) {
					String host = JSFUtil.getDbUrl();
					// if (localDebug) debug("using " + whatToUse + " - host1:["
					// + host + "]", "debug", dbarLoc);
					host = host.replace("http://", "");
					host = host.replace("https://", "");
					host = host.substring(0, host.indexOf("."));
					// if (localDebug) debug("using " + whatToUse + " - host2:["
					// + host + "]", "debug", dbarLoc);
					custom = custom.replace("<<host>>", host);
				}
				rslt = ("".equals(custom)) ? "-blank custom title-" : custom;
			} else {
				rslt = "unknown Page Title Source:[" + whatToUse + "]";
			}
		} catch (Exception e) {
			rslt = "ERROR: " + e.toString();
			debug(e.toString(), "error", dbarLoc);
			if ( isDebugServer() ) e.printStackTrace();
		}
		if ( localDebug ) debug("using " + whatToUse + " - rslt:[" + rslt + "]", "debug", dbarLoc);

		return rslt;
	}

	@SuppressWarnings("unchecked")
	public static String getParam(String key) {
		// Using the Extention Library Latest Version
		try {
			Map<String, String> myMap = (Map<String, String>) ExtLibUtil.resolveVariable(FacesContext.getCurrentInstance(), "param");
			if ( myMap.containsKey(key) ) {
				return myMap.get(key);
			} else {
				return "";
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", "HS_Util.getParam");
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			return "";
		}
	}

	public static Session getSession() {

		Session _sess = Factory.getSession();
		// Suggested by Jesse Gallagher
		DominoUtils.setBubbleExceptions(true);

		return _sess;
	}

	public static Session getSessionAsSigner() throws Exception {
		Session ts = Factory.getSession(SessionType.SIGNER);
		if ( ts == null ) throw (new Exception("Unable to get session as a signer. Database MUST be signed by a single user for this to work."));
		DominoUtils.setBubbleExceptions(true);
		return ts;
	}

	public static boolean hasRole(String rolename) {
		String aroles = HS_Util.getSession().evaluate("@UserRoles").toString();
		return aroles.contains(rolename);
	}

	public static boolean hasRole2(String rolename) {
		String aroles = HS_Util.getSession().evaluate("@UserRoles").toString();
		if ( aroles.contains(rolename) ) return true;
		return false;
	}

	public static boolean isAdministrator() {
		if ( HS_Util.hasRole("[Administrator]") ) return true;
		Session sess = HS_Util.getSession();
		Name uname = sess.createName(sess.getUserName());
		if ( "Office".equals(uname.getOrgUnit1()) && "HealthSpace".equals(uname.getOrganization()) ) return true;
		return false;
	}

	public static boolean isDebugger() {
		return HS_Util.hasRole("[XPDebugger]");
	}

	public static boolean isDebugServer() {
		if ( getSession().getServerName().contains("Henry1") ) return true;
		String server = HS_Util.getSession().getServerName();
		ACL acl = HS_Util.getSession().getCurrentDatabase().getACL();
		ACLEntry srvrEntry = acl.getEntry(server);
		if ( srvrEntry == null ) {
			return false;
		}
		for (String role : srvrEntry.getRoles()) {
			if ( "[XPDebugger]".equals(role) ) return true;
		}
		return false;
	}

	public static boolean isEditor() {
		if ( "Anonymous".equals(getSession().getCommonUserName()) ) return false;
		Database db = getSession().getCurrentDatabase();
		if ( db.getCurrentAccessLevel() >= ACL.LEVEL_EDITOR ) return true;
		return false;
	}

	public static boolean isMember(String test, String[] testList) {
		for (String tv : testList) {
			if ( test.equals(tv) ) return true;
		}
		return false;
	}

	public static boolean isMobile() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		XSPContext context = XSPContext.getXSPContext(facesContext);
		String uagent = context.getUserAgent().getUserAgent();
		String[] matchStrings = { "iphone", "android", "ipad", "(compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0; Touch; MALCJS)" };
		for (String ms : matchStrings) {
			if ( uagent.toLowerCase().contains(ms.toLowerCase()) ) return true;
		}
		return false;
	}

	public static boolean isMobilePhone() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		XSPContext context = XSPContext.getXSPContext(facesContext);
		String uagent = context.getUserAgent().getUserAgent();
		String[] matchStrings = { "iphone", "android" };
		for (String ms : matchStrings) {
			if ( uagent.toLowerCase().contains(ms.toLowerCase()) ) return true;
		}
		return false;
	}

	public static boolean isReader() {
		Database db = getSession().getCurrentDatabase();
		if ( db.getCurrentAccessLevel() == ACL.LEVEL_READER ) return true;
		if ( db.getCurrentAccessLevel() < ACL.LEVEL_READER ) return false;
		if ( "Anonymous".equals(getSession().getCommonUserName()) ) return true;
		return false;
	}

	public static boolean isRestDebugServer() {
		String server = HS_Util.getSession().getServerName();
		ACL acl = HS_Util.getSession().getCurrentDatabase().getACL();
		ACLEntry srvrEntry = acl.getEntry(server);
		if ( srvrEntry == null ) {
			// System.out.print("isRestDebugServer: srvrEntry is null for:[" +
			// server + "]");
			return false;
		}
		for (String role : srvrEntry.getRoles()) {
			// System.out.print("isRestDebugServer: testing role:[" + role +
			// "]");
			if ( "[RestDebugServer]".equals(role) ) return true;
		}
		return false;
	}

	public static boolean isStringNumeric(String istr) {
		try {
			Integer.parseInt(istr);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static Date loadValueDate(Document idoc, String iname) {
		// return loadValueDate(null, idoc, iname);
		final boolean localDebug = false;
		final String dbarLoc = "HS_Util.loadValueDate";
		Date result = null;
		Vector<Object> wdt = null;
		try {
			wdt = idoc.getItemValue(iname);
			if ( wdt.size() == 0 ) {
				if ( localDebug ) HS_Util.debug(iname + " loaded as empty array", "debug", dbarLoc);
			} else {
				if ( localDebug ) HS_Util.debug(iname + " loaded as: [" + wdt.get(0).getClass().getName() + "] value:[" + ((DateTime) wdt.get(0)).toJavaDate().toString() + "]", "debug", dbarLoc);
				result = ((DateTime) wdt.get(0)).toJavaDate();
			}
		} catch (Exception e) {
			HS_Util.debug("for:[" + iname + "] ERROR: " + e.toString(), "error", dbarLoc);
		}
		return result;
	}

	public static double loadValueDouble(Document idoc, String iname) {
		final boolean localDebug = false;
		final String dbarLoc = "HS_Util.loadValueDouble";
		double result = 0;
		try {
			result = idoc.getItemValueDouble(iname);
			if ( localDebug ) HS_Util.debug(iname + " loaded as: [" + result + "]", "debug", dbarLoc);
		} catch (Exception e) {
			HS_Util.debug("for:[" + iname + "] ERROR: " + e.toString(), "error", dbarLoc);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static Vector<Double> loadValueDoubleArray(Document idoc, String iname) {
		final boolean localDebug = false;
		final String dbarLoc = "HS_Util.loadValueString";
		Vector<Double> result = new Vector<Double>();
		try {
			if ( idoc.hasItem(iname) ) result = (Vector<Double>) (Vector<?>) (idoc.getItemValue(iname));
			if ( localDebug ) HS_Util.debug(iname + " loaded as: [" + result.toString() + "]", "debug", dbarLoc);
		} catch (Exception e) {
			HS_Util.debug("for:[" + iname + "] ERROR: " + e.toString(), "error", dbarLoc);
		}
		return result;
	}

	public static int loadValueInt(Document idoc, String iname) {
		final boolean localDebug = false;
		final String dbarLoc = "HS_Util.loadValueInt";
		int result = -1;
		try {
			result = idoc.getItemValueInteger(iname);
			if ( localDebug ) HS_Util.debug(iname + " loaded as: [" + result + "]", "debug", dbarLoc);
		} catch (Exception e) {
			HS_Util.debug("for:[" + iname + "] ERROR: " + e.toString(), "error", dbarLoc);
		}
		return result;
	}

	public static String loadValueString(Document idoc, String iname) {
		final boolean localDebug = false;
		final String dbarLoc = "HS_Util.loadValueString";
		String result = "";
		try {
			result = idoc.getItemValueString(iname);
			if ( localDebug ) HS_Util.debug(iname + " loaded as: [" + result + "]", "debug", dbarLoc);
		} catch (Exception e) {
			HS_Util.debug("for:[" + iname + "] ERROR: " + e.toString(), "error", dbarLoc);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static Vector<String> loadValueStringArray(Document idoc, String iname) {
		final boolean localDebug = false;
		final String dbarLoc = "HS_Util.loadValueString";
		Vector<String> result = new Vector<String>();
		try {
			// if (idoc.hasItem(iname)) result = idoc.getItemValue(iname);
			if ( idoc.hasItem(iname) ) {
				// List<String> foo =
				// (List<String>)(List<?>)idoc.getItemValue(iname);
				result = (Vector<String>) (Vector<?>) (idoc.getItemValue(iname));
			}
			if ( localDebug ) HS_Util.debug(iname + " loaded as: [" + result.toString() + "]", "debug", dbarLoc);
		} catch (Exception e) {
			HS_Util.debug("for:[" + iname + "] ERROR: " + e.toString(), "error", dbarLoc);
		}
		return result;
	}

	public static void logException(String emsg, Object se, String msgContext) {
		// final boolean localDebug = HS_Util.isDebugger();
		final String dbarLoc = "HS_Util.logException";
		try {
			String html = "<h3>EIBS Exception reported from " + msgContext + "</h3>";
			html += "<strong>" + emsg + "</strong>";
			if ( se != null ) {
				html += "<br/>se is a:[" + se.getClass().getName() + "]";
				if ( se instanceof com.ibm.jscript.std.ErrorObject ) {
					// ErrorObject eo = (ErrorObject) se;

				}
				// for (StackTraceElement st : strace) {
				// html += "<br/>" + st.toString();
				// }
			}
			logHTMLerror("EIBS Exception reported from " + msgContext, html);
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			// e.getStackTrace()
		}
	}

	public static boolean logHTMLerror(String description, String html) {
		final boolean localDebug = HS_Util.isDebugger();
		final String dbarLoc = "HS_Util.logHTMLerror";
		boolean result = false;
		try {
			if ( localDebug ) HS_Util.debug("logging:[" + description + "] html.length:[" + html.length() + "]", "debug", dbarLoc);
			Session sess = getSession();
			AgentLogger alog = new AgentLogger();
			alog.init(sess, sess.getEffectiveUserName(), description, false);

			alog.logHTML(html, true);
			result = true;
		} catch (Exception e) {
			if ( localDebug ) HS_Util.debug(e.toString(), "error", dbarLoc);
		}
		return result;
	}

	// private static double tdd(DateTime d1, DateTime d2) {
	// // should be:
	// // return d1.timeDifferenceDouble(d2);
	// Date dt1 = d1.toJavaDate();
	// Date dt2 = d2.toJavaDate();
	//
	// return (dt1.getTime() - dt2.getTime()) / 1000 / 60 / 60 / 24;
	// }

	/*
	 * A couple functions from Mr. David Leedy - Notes in 9
	 */
	public static void print(String msg) {
		System.out.println(msg);
	}

	public static Vector<String> removeVectorItem(Vector<String> sv, int idx) {
		Vector<String> result = new Vector<String>();

		Iterator<String> itr = sv.iterator();
		String tval = "";
		int ctr = -1;
		while (itr.hasNext()) {
			tval = itr.next();
			ctr++;
			if ( ctr != idx ) result.add(tval);
		}
		return result;
	}

	public static boolean renderInspectionInViewByUnid(String unid) {
		// Database eidb =
		final boolean localDebug = ("Yes".equals(HS_Util.getAppSettingString("debug_InspectionView")) || "Yes".equals(HS_Util.getAppSettingString("debug_InspectionRead")));
		final String dbarLoc = "HS_Util.renderInspectionInViewByUnid";

		HS_database hsdb = null;
		Database eidb = null;
		Document idoc = null;
		Object date1 = null;
		Object date2 = null;
		String idate = "";
		try {
			hsdb = new HS_database();
			eidb = hsdb.getdb("eiRoot");
			idoc = HS_Util.getDocumentByUNID(eidb, unid, false);

			// var idate = eachInspection.getColumnValue("$Date");
			// if(idate == null) idate= eachInspection.getColumnValue("$5"); //
			// Backward compatible

			// REM {The programatic name for this column MUST be '$Date' -
			// newbs};
			// date1 := @If(Form="CCFLCompSubst"; RecievedDate; InspectionDate);
			// date2 := @If(@IsError(date1) | @Text(date1)=""; @Created; date1);
			// Months :=
			// "Jan":"Feb":"Mar":"Apr":"May":"Jun":"Jul":"Aug":"Sep":"Oct":"Nov":"Dec";
			// idate := @Text(@Day(date2)) + "-" + @Subset(@Subset(Months;
			// @Month(date2));-1) + "-" + @Text( @Year(date2));
			// idate
			date1 = ("CCFLCompSubst".equals(idoc.getForm())) ? idoc.getItemValue("ReceivedDate").get(0) : idoc.getItemValue("InspectionDate").get(0);
			date2 = ("".equals(date1)) ? idoc.getCreated() : date1;
			if ( date2.getClass().getName().contains("DateTime") ) {
				idate = HS_Util.formatDate(((DateTime) date2).toJavaDate(), "d-MMM-yyyy");
				if ( localDebug ) HS_Util.debug("Computed date str is:[" + idate + "]", "debug", dbarLoc);
				return renderInspectionInView(idate, 0);
			} else {
				HS_Util.debug("not sure how to deal with idate as a:[" + idate.getClass().getName() + "] value:[" + idate.toString() + "]", "warn", dbarLoc);
				return false;
			}
		} catch (Exception e) {
			HS_Util.debug("", "error", dbarLoc);
		}
		return false;
	}

	public static boolean renderInspectionInView(String idateStr, long eidx) {
		// boolean result = true;
		// final boolean localDebug = false;
		final boolean localDebug = ("Yes".equals(HS_Util.getAppSettingString("debug_InspectionView")) || "Yes".equals(HS_Util.getAppSettingString("debug_InspectionRead")));
		final String dbarLoc = "HS_Util.renderInspectionInView";

		try {
			Session sess = getSession();
			double inspDelayAccessDays = getAppSettingDouble("inspDelayAccessDays");
			// String inspDelayAccessDaysStr = ((tempv.size() < 1) ? "0" :
			// tempv.get(0));
			// int inspDelayAccessDays =
			// Integer.parseInt(inspDelayAccessDaysStr);

			Vector<Object> tempv = getAppSettingsValuesAsObjects("inspCutoffDate");
			// Object inspCDO = (tempv.size() < 1) ? null : tempv.get(0);
			// DateTime inspCutoffDate =
			// (inspCDO.getClass().equals(DateTime.class) ? inspCDO : null;
			DateTime inspCutoffDate = (tempv.size() < 1) ? null : (DateTime) tempv.get(0);
			// if (inspCutoffDate != null) inspCutoffDate.adjustDay(-1);
			if ( localDebug && eidx < 10 && inspCutoffDate != null ) HS_Util.debug("inspCutoffDate:[" + inspCutoffDate.getDateOnly() + "] idateStr:[" + idateStr + "]", "debug", dbarLoc);
			if ( localDebug && eidx < 10 && inspCutoffDate == null ) HS_Util.debug("inspCutoffDate:[NULL] idateStr:[" + idateStr + "]", "debug", dbarLoc);

			if ( idateStr == null ) return true;
			DateTime idate = sess.createDateTime(cvrtStringToDate(idateStr));

			DateTime lastDate = sess.createDateTime("Today");
			if ( isReader() && inspDelayAccessDays > 0 ) {
				lastDate.adjustDay((int) inspDelayAccessDays * -1);
			} else {
				// System.out.print("lastDate timeZone:[" +
				// lastDate.getTimeZone() + "] -[" + lastDate.getLocalTime() +
				// "]");
				lastDate.adjustYear(3); // go way in the future to assure we
				// catch all...
			}
			if ( localDebug && eidx < 10 ) {
				debug("inspDelayAccessDays:[" + inspDelayAccessDays + "]", "debug", dbarLoc);
				debug("inspCutoffDate:[" + inspCutoffDate + "] typeof:[" + inspCutoffDate.getClass().getName() + "]", "debug", dbarLoc);
				// debug("idate:[" + idate + "] typeof:[" +
				// idate.getClass().getName() +
				// "] inspCutoffDate.timeDifferenceDouble(idate):[" +
				// inspCutoffDate.timeDifferenceDouble(idate) + "]", "debug",
				// dbarLoc);
				debug("idate:[" + idateStr + "] typeof:[" + idate.getClass().getName() + "] inspCutoffDate.compareTo(idate):[" + inspCutoffDate.compareTo(idate) + "]", "debug", dbarLoc);
				debug("lastDate:[" + lastDate + "] lastDate.compareTo(idate):[" + lastDate.compareTo(idate) + "]", "debug", dbarLoc);
				// System.out.print("lastDate:[" + lastDate +
				// "] lastDate.compareTo(idate):[" + lastDate.compareTo(idate)+
				// "]");
			}
			// if (tdd(lastDate, idate) < 0) return false;
			// System.out.print("idate:[" + idateStr + "] inspCutoffDate:[" +
			// inspCutoffDate + "] idate.compareTo(inspCutoffDate):[" +
			// idate.compareTo(inspCutoffDate) + "]");
			if ( lastDate.compareTo(idate) < 0 ) return false;
			if ( inspCutoffDate != null ) {
				// if (tdd(inspCutoffDate, idate) > 0) return false;
				if ( idate.compareTo(inspCutoffDate) < 0 ) return false;
			}

			return true;
		} catch (Exception e) {
			debug(e.toString(), "error", dbarLoc);
			if ( eidx == 0 ) e.printStackTrace();
			return true;
		}
	}

	/*
	 * Sends an email to the appSetting item "SupportEMail" Based on the viewScope items "savedErrorMessage" and "SavedStackTrace"
	 */
	// Usage:
	// JSFUtil.getViewScope().put("savedErrorMessage", "-your error mesage-");
	// JSFUtil.getViewScope().put("savedStackTrace", e.getStackTrace());
	// HS_Util.sendErrorToSupport();
	public static boolean sendErrorToSupport() {
		final String msgContext = "HS_Util.sendErrorToSupport";
		final String sendto = getAppSettingString("SupportEMail");
		if ( "".equals(sendto) ) return false;

		Database currdb = null;
		try {
			currdb = getSessionAsSigner().getCurrentDatabase();
		} catch (Exception e1) {
			HS_Util.debug(e1.toString(), "error", msgContext);
			return false;
			// e1.printStackTrace();
		}
		Document edoc = currdb.createDocument();
		try {
			Map<String, Object> vscope = JSFUtil.getViewScope();
			edoc.replaceItemValue("Form", "Memo");
			edoc.replaceItemValue("Subject", "Error report from system: " + currdb.getTitle());
			RichTextItem body = edoc.createRichTextItem("Body");
			body.appendText("Sever: " + currdb.getServer());
			body.addNewLine(1);
			body.appendText("File path: " + currdb.getFilePath());
			body.addNewLine(1);
			body.appendText("URL: " + JSFUtil.getUrl());
			body.addNewLine(1);
			body.appendText("User: " + getSession().getEffectiveUserName());
			body.addNewLine(1, true);
			body.addNewLine(1);

			String savedEMsg = "error message not found";
			if ( vscope.containsKey("savedErrorMsg") ) savedEMsg = vscope.get("savedErrorMsg").toString();
			body.appendText(savedEMsg);
			body.addNewLine(1, true);

			// Vector<StackTraceElement> trace =
			// (vscope.containsKey("savedErrorMsg")) ?
			// (Vector<StackTraceElement>) vscope.get("savedStackTrace") : null;
			StackTraceElement[] trace = (vscope.containsKey("savedErrorMsg")) ? (StackTraceElement[]) vscope.get("savedStackTrace") : null;
			if ( trace != null ) {
				body.addNewLine();
				body.appendText("STACK TRACE");

				// body.appendText("trace is a [" + trace.getClass().getName() +
				// "]");
				// body.addNewLine();

				for (StackTraceElement te : trace) {
					body.addNewLine(1, true);
					body.addTab();
					body.appendText(te.toString());
				}
				// for (int i = 0; i < trace.size(); i++) {
				// body.addNewLine(1, true);
				// body.addTab();
				// body.appendText(trace.get(i).toString());
				// }
			}
			edoc.send(false, sendto);
			debug("message sent", "debug", msgContext);

			// @InfoMessage("message sent to: " + sendto);

			return true;
		} catch (Exception e) {
			debug(e.toString(), "error", msgContext);
			e.printStackTrace();
		}
		return false;
	}

	public static Vector<String> parsePhoneSaver(String iph) {
		final boolean localDebug = false;
		final String msgContext = "HS_Util.parsePhoneSaver";

		Vector<String> out = new Vector<String>();
		out.add("");
		out.add("");
		out.add("");

		if ( localDebug ) HS_Util.debug("Started with:[" + iph + "]", "debug", msgContext);

		String wph = iph.replaceAll("[()]", "");
		wph = wph.replaceAll(" ", "");
		wph = wph.replaceAll("-", "");
		wph = wph.replaceAll("[.]", "");

		if ( localDebug ) HS_Util.debug("After replaces 1:[" + wph + "]", "debug", msgContext);
		if ( wph.startsWith("+") ) wph = wph.substring(1);
		if ( wph.startsWith("1") ) wph = wph.substring(1);
		if ( localDebug ) HS_Util.debug("After replaces 2:[" + wph + "] length:" + wph.length() + ".", "debug", msgContext);
		if ( wph.length() > 10 ) {
			out.set(2, wph.substring(10));
			if ( out.get(2).startsWith("ext") ) out.set(2, out.get(2).substring(3));
			if ( out.get(2).startsWith("x") ) out.set(2, out.get(2).substring(1));
			wph = wph.substring(0, 10);
			if ( localDebug ) HS_Util.debug("Ext extracted as::[" + out.get(2) + "] left over is:[" + wph + "] length:" + wph.length() + ".", "debug", msgContext);
		}
		if ( wph.length() == 10 ) {
			out.set(0, wph.substring(0, 3));
			out.set(1, wph.substring(3, 6) + "-" + wph.substring(6));
			if ( localDebug ) HS_Util.debug("Area extracted as:[" + out.get(0) + "] phone extracted as:[" + out.get(1) + "]", "debug", msgContext);
		} else {
			out.set(1, iph);
			if ( localDebug ) HS_Util.debug("Could not figure out the parsing - phone extracted as:[" + out.get(1) + "]", "debug", msgContext);
		}
		return out;
	}

	public static boolean useNewViolTypes(Date inspectionDate, String establishmentType) {
		boolean result = false;
		final boolean localDebug = false;
		final String dbarLoc = "HS_Util.useNewViolTypes";

		try {
			/*
			 * UseCodes := @GetProfileField( "MasterSettings"; "FoodCode2009Districts" ); CodeStartDate := @GetProfileField( "MasterSettings"; "FoodCode2009Date" ); IsAfterDate := @If(
			 * 
			 * @IsTime( CodeStartDate ); @If( InspectionDate >= CodeStartDate;
			 * 
			 * @True; @False ); @False );
			 * 
			 * @If( EstablishmentType = "Foodservice" & UseCodes = "Yes" & IsAfterDate;
			 * 
			 * @True;
			 * 
			 * @False )
			 */
			// Date inspectionDate = insp.getInspectionDate();
			String useCodes = HS_Util.getMasterSettingsString("FoodCode2009Districts");

			Object codeStartDate = HS_Util.getMasterSettingsObject("FoodCode2009Date");
			if ( "".equals(establishmentType) ) establishmentType = "Foodservice";
			boolean isAfterDate = ((codeStartDate.getClass().getName()).indexOf("DateTime") == -1) ? false : (inspectionDate.getTime() >= ((DateTime) codeStartDate).toJavaDate().getTime()) ? true : false;
			if ( localDebug )
				HS_Util.debug("useCodes:[" + useCodes + "]" + " codeStartDate:[" + codeStartDate + "] " + " inspectionDate:[" + inspectionDate + "] " + " establishmentType:[" + establishmentType + "] "
										+ " isAfterDate:[" + isAfterDate + "]", "debug", dbarLoc);

			result = ("Foodservice".equals(establishmentType) && "Yes".equals(useCodes) && isAfterDate);

			return result;
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			return true;
		}
	}

	public static boolean sendEmail(Document emailDoc, boolean attachForm, Vector<String> recipients, boolean checkForCopy) {

		try {
			// if(checkForCopy && copyNotReplica(emailDoc.getParentDatabase()))
			// {
			// // not yet implemented
			// }
			emailDoc.send(attachForm, recipients);
			return true;
		} catch (Exception e) {
			emailDoc.replaceItemValue("HSError", "HS_Util.sendEmail got error " + e.toString());
			return false;
		}
	}

	public static String toTitleCase(String input) {
		StringBuilder titleCase = new StringBuilder();
		boolean nextTitleCase = true;

		for (char c : input.toCharArray()) {
			if ( Character.isSpaceChar(c) ) {
				nextTitleCase = true;
			} else if ( nextTitleCase ) {
				c = Character.toTitleCase(c);
				nextTitleCase = false;
			}

			titleCase.append(c);
		}

		return titleCase.toString();
	}
}
