package com.healthspace.general;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import java.util.Vector;

import org.openntf.domino.Database;
import org.openntf.domino.DateTime;
import org.openntf.domino.Document;
import org.openntf.domino.Item;
import org.openntf.domino.Name;
import org.openntf.domino.Session;

public class HS_auditForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private HashMap<String, String> openValues = null;
	private Vector<String> systemFields = null;
	private Vector<String> changedFields;
	private Vector<String> removedFields;

	public HS_auditForm() {
	};

	public HS_auditForm(Document doc) {
		this.init(doc);
	}

	public void init(Document doc) {
		final boolean localDebug = false;
		final String dbarLoc = "HS_auditForm,init";
		try {
			if (doc == null) return;
			if (doc.isNewNote()) {
				openValues = new HashMap<String, String>(0);
				return;
			}
			systemFields = new Vector<String>();
			systemFields.add("OriginalModTime".toLowerCase());
			systemFields.add("StatusHistory".toLowerCase());
			systemFields.add("dispStatusHistory".toLowerCase());
			systemFields.add("ViewDescription".toLowerCase());
			systemFields.add("VIOLATIONDISPLAY".toLowerCase());
			systemFields.add("dispViolations".toLowerCase());

			Vector<Item> items = doc.getItems();
			openValues = new HashMap<String, String>(items.size());
			int ictr = 0;
			int sctr = 0;
			int rtctr = 0;
			for (Item item : items) {
				if (item.getName().startsWith("$")) {
					// skip these items
					sctr++;
				} else if (systemFields.contains(item.getName().toLowerCase())) {
					// skip these items
					sctr++;
				} else if (item.getTypeEx() == Item.Type.RICHTEXT) {
					// skip rich text items
					rtctr++;
				} else {
					openValues.put(item.getName().toLowerCase(), item.getText());
					ictr++;
					if (localDebug && "ViolLegalTxt".equalsIgnoreCase(item.getName())) {
						HS_Util.debug(item.getName() + " loaded with value:[" + item.getText() + "]", "debug", dbarLoc);
					}
				}
			}
			if (localDebug) HS_Util.debug("ictr:[" + ictr + "] sctr:[" + sctr + "] rctr:[" + rtctr + "]", "debug", dbarLoc);
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			if (HS_Util.isDebugServer()) e.printStackTrace();
		}
	}

	public boolean checkChanges(Document doc) throws Exception {
		final boolean localDebug = false;
		final String dbarLoc = "HS_auditForm,checkChanges";
		boolean result = false;
		if (doc == null) return result;
		if (doc.isNewNote()) return result;

		Vector<Item> items = doc.getItems();
		changedFields = new Vector<String>();
		for (Item item : items) {
			// if ("$".equals(item.getName().charAt(0))) {
			try {
				if (item.getName().startsWith("$")) {
					// skip these items
					// if (localDebug) HS_Util.debug("skipping:[" + item.getName() + "]", "debug", dbarLoc);
				} else if (systemFields.contains(item.getName().toLowerCase())) {
					// skip these items
				} else if (item.getTypeEx() == Item.Type.RICHTEXT || item.getTypeEx() == Item.Type.MIME_PART) {
					// skip rich text items
				} else if (!openValues.containsKey(item.getName().toLowerCase())) {
					// New field
					changedFields.add(item.getName());
					result = true;
				} else if (!item.getText().equals(openValues.get(item.getName().toLowerCase()))) {
					changedFields.add(item.getName());
					result = true;
					if (localDebug) HS_Util.debug("changed field found:[" + item.getName() + "]", "debug", dbarLoc);
				}
			} catch (Exception e) {
				HS_Util.debug("Error on item:[" + item.getName() + "] type:[" + item.getTypeEx() + "] : " + e.toString(), "error", dbarLoc);
				throw (e);
			}
		}

		// check to see if any fields have been removed.
		removedFields = new Vector<String>();
		Set<String> openFields = openValues.keySet();
		for (String ofld : openFields) {
			if (!doc.hasItem(ofld)) {
				removedFields.add(ofld);
				result = true;
			}
		}

		return result;
	}

	public boolean writeAuditEvent(Document idoc, String event) {
		Database adb = idoc.getParentDatabase();
		return writeAuditEvent(adb, idoc, event, null);
	}

	public boolean writeAuditEvent(Document idoc, String event, Vector<String> cfields) {
		Database adb = idoc.getParentDatabase();
		return writeAuditEvent(adb, idoc, event, cfields);
	}

	public boolean writeAuditEvent(Database adb, Document idoc, String event, Vector<String> cfields) {
		final boolean localDebug = false;
		final String dbarLoc = "HS_AuditForm.writeAuditEvent";
		boolean result = false;

		Document adoc = null;
		Item aitem = null;
		DateTime ndt = null;
		Date jdt = new Date();
		Name nname = null;
		Session sess = HS_Util.getSession();
		try {
			adoc = adb.createDocument();
			if (adoc == null) {
				if (localDebug) HS_Util.debug("AuditEvent doc could not be created " + "in db:[" + adb.getServer() + "!!" + adb.getFilePath() + "]", "debug", dbarLoc);
				return false;
			}
			adoc.replaceItemValue("Form", "AuditEvent");

			Vector<String> readers = new Vector<String>();
			readers.add("LocalDomainServers");
			readers.add("[Auditor]");
			aitem = adoc.replaceItemValue("CanRead", readers);
			aitem.setReaders(true);

			ndt = sess.createDateTime(jdt);
			ndt.setAnyTime();
			adoc.replaceItemValue("Date", ndt);

			if (cfields == null) {
				adoc.replaceItemValue("Details", "");
			} else {
				String details = "Changed fields are: ";
				for (String fname : cfields) {
					details += fname + ", ";
				}
				adoc.replaceItemValue("Details", "Changed fields are: " + details);
			}
			adoc.replaceItemValue("Document", "");
			adoc.replaceItemValue("DocumentForm", idoc.getFormName());
			adoc.replaceItemValue("DocumentID", idoc.getItemValue("DocumentId"));
			adoc.replaceItemValue("Event", event);
			adoc.replaceItemValue("ID", idoc.getUniversalID());

			// nname = sess.createName(sess.getUserName());
			nname = sess.createName((String) sess.evaluate("@UserName").get(0));
			adoc.replaceItemValue("Name", nname.getCommon());

			ndt = sess.createDateTime(jdt);
			adoc.replaceItemValue("Time", ndt);
			adoc.replaceItemValue("User", nname.getCommon());
			adoc.replaceItemValue("UserAcl", adb.getCurrentAccessLevel());
			adoc.replaceItemValue("UserRoles", sess.evaluate("@UserRoles"));

			result = adoc.save(true, true);
			if (result) {
				if (localDebug) HS_Util.debug("AuditEvent doc save with unid:[" + adoc.getUniversalID() + "] " + "in db:[" + adb.getServer() + "!!" + adb.getFilePath() + "]", "debug", dbarLoc);
			} else {
				HS_Util.debug("AuditEvent doc failed to save.", "error", dbarLoc);
			}
		} catch (Exception e) {
			if (HS_Util.isDebugServer()) e.printStackTrace();
			HS_Util.debug(e.toString(), "error", dbarLoc);
			result = false;
		}

		return result;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public HashMap<String, String> getOpenValues() {
		return openValues;
	}

	public Vector<String> getSystemFields() {
		return systemFields;
	}

	public Vector<String> getChangedFields() {
		return changedFields;
	}

	public Vector<String> getRemovedFields() {
		return removedFields;
	}

	public void setOpenValues(HashMap<String, String> openValues) {
		this.openValues = openValues;
	}

	public void setSystemFields(Vector<String> systemFields) {
		this.systemFields = systemFields;
	}

	public void setChangedFields(Vector<String> changedFields) {
		this.changedFields = changedFields;
	}

	public void setRemovedFields(Vector<String> removedFields) {
		this.removedFields = removedFields;
	}

}
