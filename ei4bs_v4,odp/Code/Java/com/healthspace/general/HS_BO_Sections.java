package com.healthspace.general;

import java.util.Vector;

import org.openntf.domino.Document;

public class HS_BO_Sections {

	public static String getJson(Inspection insp) {
		String result = "[";
		for (InspectionSection isect : insp.getSections()) {
			result += (result.length() > 3) ? "," : "";
			result += isect.getJson();
		}
		result += "]";
		return result;
	}

	public static void loadSectionData(Inspection insp, Document idoc) {
		final String msgContext = "HS_BO_Sections.loadSectionData";
		final boolean localDebug = false;
		Vector<InspectionSection> result = new Vector<InspectionSection>();
		InspectionSection wsect = null;
		try {
			insp.setSections(result);
			if ( idoc.hasItem("SectionCategory") ) {
				Vector<String> scat = HS_Util.loadValueStringArray(idoc, "SectionCategory");
				int totsections = scat.size();
				if ( localDebug ) HS_Util.debug("SectionDisplay has [" + totsections + "] elements", "debug", msgContext);
				if ( totsections == 0 ) return;

				Vector<String> sdesc = HS_Util.loadValueStringArray(idoc, "SectionDescription");
				Vector<String> sgrp = HS_Util.loadValueStringArray(idoc, "SectionGroup");
				Vector<String> smod = HS_Util.loadValueStringArray(idoc, "SectionModule");
				Vector<String> snum = HS_Util.loadValueStringArray(idoc, "SectionNumber");
				Vector<String> sstat = HS_Util.loadValueStringArray(idoc, "SectionStatus");
				Vector<String> sobs = HS_Util.loadValueStringArray(idoc, "SectionObservation");

				if ( localDebug )
					HS_Util.debug("scat.size():[" + scat.size() + "]" + " sdesc.size():[" + sdesc.size() + "]" + " sgrp.size():[" + sgrp.size() + "]"
							+ " smod.size():[" + smod.size() + "]" + " snum.size():[" + snum.size() + "]" + " sstat.size():[" + sstat.size() + "]" + " ",
							"debug", msgContext);
				for (int i = 0; i < totsections; i++) {
					wsect = new InspectionSection();
					wsect.setCategory((scat.size() < i) ? " " : scat.get(i));
					wsect.setDescription((sdesc.size() < i) ? " " : sdesc.get(i));
					// wsect.setDisplay(sdisplay.get(i));
					wsect.setGroup((sgrp.size() < i) ? " " : sgrp.get(i));
					wsect.setModule((smod.size() < i) ? " " : smod.get(i));
					wsect.setNumber((snum.size() < i) ? " " : snum.get(i));
					wsect.setStatus((sstat.size() < i) ? " " : sstat.get(i));
					wsect.setObservation((sobs.size() < 1) ? "" : sobs.get(i));
					result.add(wsect);
					// if (localDebug) HS_Util.debug("Section Display [" + i + "] added:[" + wsect.getDisplay() + "] elements", "debug",
					// msgContext);
				}
				insp.setSections(result);
				if ( localDebug ) HS_Util.debug("Section Display returned [" + result.size() + "] elements", "debug", msgContext);
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			return;
		}
	}

	public static boolean removeSection(int wnum, Inspection insp) {
		final String dbarLoc = "HS_BO_Sections.removeSection";
		final boolean localDebug = true;
		try {
			Vector<InspectionSection> vsecs = insp.getSections();
			InspectionSection rsect = vsecs.remove(wnum);
			if ( localDebug ) HS_Util.debug("removed entry [" + wnum + "] [" + rsect.getDisplay() + "]", "debug", dbarLoc);
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
		}
		return false;
	}

	private static boolean updateItem(Inspection insp, Document idoc, String itemName, Object value) {
		final boolean localDebug = false;
		final String dbarLoc = "HS_BO_Inspections.updateItem";
		boolean result = false;
		try {
			if ( value == null ) value = "";
			idoc.replaceItemValue(itemName, value);
			if ( localDebug ) HS_Util.debug("item:[" + itemName + "] updated OK", "debug", dbarLoc);
			result = true;
		} catch (Exception e) {
			HS_Util.debug(e.toString() + " on item:[" + itemName + "]", "error", dbarLoc);
		}
		return result;
	}

	public static void updateSectionData(Document idoc, Inspection insp) {
		final boolean localDebug = false;
		final String msgContext = "HS_BusinessObjects.updateSectionData";

		try {
			Vector<String> scat = new Vector<String>();
			Vector<String> sdesc = new Vector<String>();
			Vector<String> sdisp = new Vector<String>();
			Vector<String> sgrp = new Vector<String>();
			Vector<String> smod = new Vector<String>();
			Vector<String> snum = new Vector<String>();
			Vector<String> sstat = new Vector<String>();
			Vector<String> sobs = new Vector<String>();
			for (InspectionSection isect : insp.getSections()) {
				scat.add(isect.getCategory());
				sdesc.add(isect.getDescription());
				sgrp.add(isect.getGroup());
				smod.add(isect.getModule());
				snum.add(isect.getNumber());
				sstat.add(isect.getStatus());
				sobs.add(isect.getObservation());

				sdisp.add(isect.getDisplay());
				sdisp.add(" "); // puts a null line after the entry.
			}

			updateItem(insp, idoc, "SectionCategory", scat);
			updateItem(insp, idoc, "SectionDescription", sdesc);
			updateItem(insp, idoc, "SectionDisplay", sdisp);
			updateItem(insp, idoc, "SectionGroup", sgrp);
			updateItem(insp, idoc, "SectionModule", smod);
			updateItem(insp, idoc, "SectionNumber", snum);
			updateItem(insp, idoc, "SectionStatus", sstat);
			updateItem(insp, idoc, "SectionObservation", sobs);

			if ( localDebug ) HS_Util.debug("Updated [" + snum.size() + "] sections.", "debug", msgContext);
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}
	}
}
