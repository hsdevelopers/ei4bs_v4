package com.healthspace.general;

import java.io.Serializable;

import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.View;

public class HS_SyncDocs implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public HS_SyncDocs() {

	}

	private static boolean pullUpMostRecentInspDetails(Document facility) {
		// final boolean localDebug = HS_Util.isDebugServer();
		final boolean localDebug = false;
		final String dbarLoc = "HS_SyncDocs.pullUpMostRecentInspDetails";

		boolean result = false;
		try {
			Database ehsDB = facility.getParentDatabase();
			String thestring = HS_GetMostRecent.inspectionUNID(ehsDB, facility);
			Document latestInsp = HS_Util.getDocumentByUNID(ehsDB, thestring, false);

			thestring = HS_GetMostRecent.routineInspectionUNID(ehsDB, facility);
			Document lastestRoutineInsp = HS_Util.getDocumentByUNID(ehsDB, thestring, false);

			String dbDesign = facility.getItemValueString("DBDesign");

			// AuditFormChanges_Init facility, facilityOpenValues);
			HS_auditForm auditForm = new HS_auditForm();
			auditForm.init(facility);

			if (latestInsp == null) { // The facility has no inspection.
				if (!("Wisconsin".equals(dbDesign) || "Erie".equals(dbDesign) || "Ohio".equals(dbDesign))) {
					if (facility.hasItem("LastInspection")) result = true;
					if (facility.hasItem("NumCritical")) result = true;
					if (facility.hasItem("NumNonCritical")) result = true;
					if (facility.hasItem("LastRoutineInsp")) result = true;
					if (facility.hasItem("LastInspectionUNID")) result = true;
					if (facility.hasItem("LastInspectionType")) result = true;
					if (facility.hasItem("FollowupRequired")) result = true;
					if (facility.hasItem("LastHazardRating")) result = true;
					if (facility.hasItem("HazardRatingChange")) result = true;
					if (facility.hasItem("HazardRating")) result = true;

					facility.removeItem("LastInspection");
					facility.removeItem("NumCritical");
					facility.removeItem("NumNonCritical");
					facility.removeItem("LastRoutineInsp");
					facility.removeItem("LastInspectionUNID");
					facility.removeItem("LastInspectionType");
					facility.removeItem("FollowupRequired");
					facility.removeItem("LastHazardRating");
					facility.removeItem("HazardRatingChange");
					facility.removeItem("HazardRating");
				}
			} else {
				HS_Util.copyItemToDocument(latestInsp, "TimeIn", facility, "LastInspectionTime");
				facility.replaceItemValue("NumCritical", latestInsp.getItemValue("NumCritical"));
				facility.replaceItemValue("NumNonCritical", latestInsp.getItemValue("NumNonCritical"));
				if ("VDH".equals(dbDesign)) {
					facility.replaceItemValue("TotalViolations", latestInsp.getItemValue("TotalViolations"));
					facility.replaceItemValue("NumCore", latestInsp.getItemValue("NumCore"));
					facility.replaceItemValue("NumPriority", latestInsp.getItemValue("NumPriority"));
					facility.replaceItemValue("NumPriorityFoundation", latestInsp.getItemValue("NumPriorityFoundation"));
					facility.replaceItemValue("NumRepeat", latestInsp.getItemValue("NumRepeat"));
					facility.replaceItemValue("NumVFIRFI", latestInsp.getItemValue("NumRisk"));
				} else {
					facility.replaceItemValue("TotalViolations", latestInsp.getItemValue("TotalViolations"));
				}
				if (lastestRoutineInsp != null) {
					facility.replaceItemValue("LastRoutineInsp", lastestRoutineInsp.getItemValue("InspectionDate"));
				}
				HS_Util.copyItemToDocument(latestInsp, "InspectionDate", facility, "LastInspection");
				facility.replaceItemValue("LastInspectionUNID", latestInsp.getUniversalID());
				facility.replaceItemValue("LastInspectionType", latestInsp.getItemValue("Type"));
				facility.replaceItemValue("FollowupRequired", latestInsp.getItemValue("FollowupInspectionRequired"));

				// ' Only pull up Enforcement and Release Date if the Inspection actually has the field "Enforcement"
				// ' TODO use HSCopyItem?
				if (latestInsp.hasItem("Enforcement")) {
					facility.replaceItemValue("Enforcement", latestInsp.getItemValue("Enforcement"));

					if ("Release".equals(latestInsp.getItemValueString("Enforcement"))) {
						facility.replaceItemValue("ReleaseDate", latestInsp.getItemValue("ReleaseDate"));
					} else {
						facility.replaceItemValue("ReleaseDate", "");
					}
				}

				// 'Pull up HazardRating if the inspection has one
				if (latestInsp.hasItem("HazardRating")) {
					if (facility.hasItem("HazardRating")) {
						if (facility.getItemValue("HazardRating").get(0) != latestInsp.getItemValue("HazardRating").get(0)) {
							facility.replaceItemValue("LastHazardRating", facility.getItemValue("HazardRating").get(0));
							facility.replaceItemValue("HazardRatingChange", latestInsp.getItemValue("InspectionDate"));
							facility.replaceItemValue("HazardRating", latestInsp.getItemValue("HazardRating").get(0));
						}
					} else {
						facility.replaceItemValue("HazardRatingChange", latestInsp.getItemValue("InspectionDate"));
						facility.replaceItemValue("HazardRating", latestInsp.getItemValue("HazardRating"));
					}
				}

				// 'Pull up Last Letter Grade and apply fee if applicable
				if (!("".equals(latestInsp.getItemValueString("LetterGrade")))) {
					// facility.replaceItemValue("", latestInsp.getItemValue(""));
					// facility.replaceItemValue("", latestInsp.getItemValue(""));
					if (facility.getItemValue("LastLetterGrade").get(0) != latestInsp.getItemValue("LetterGrade").get(0)) {
						facility.replaceItemValue("LastLetterGrade", latestInsp.getItemValue("LetterGrade"));
					}
				}

				// 'Pull up food safety info for AZ
				if ("Arizona".equals(dbDesign)) {
					if (latestInsp.hasItem("CertifiedManager")) {
						facility.replaceItemValue("CertifiedManager", latestInsp.getItemValue("CertifiedManager").get(0));
					}
				}
			}
			result = auditForm.checkChanges(facility);
			if (localDebug) HS_Util.debug("Changes found: [" + result + "] [" + auditForm.getChangedFields() + "]", "debug", dbarLoc);
		} catch (Exception e) {
			if (HS_Util.isDebugServer()) e.printStackTrace();
			HS_Util.debug(e.toString(), "error", dbarLoc);
		}
		return result;
	}

	public static boolean pushUpInspectionDetails(Document thisDoc) {
		// final boolean localDebug = HS_Util.isDebugServer();
		final boolean localDebug = false;
		final String dbarLoc = "HS_SyncDocs.pushUpInspectionDetails";

		boolean result = false;
		if (thisDoc == null) return result;

		Database ehsDB = null;
		Document facility = null;
		// ' Figure out if we are on an inspection or a facility
		// If Instr( ThisDoc.Form(0), "Report" ) > 0 _
		// Or Lcase(Right(ThisDoc.Form(0),10)) = "inspection" Then
		String form = "";
		String facUnid = "";
		try {
			ehsDB = thisDoc.getParentDatabase();
			form = thisDoc.getFormName();
			if (!(form.contains("Report") || form.toLowerCase().endsWith("inspection"))) {
				if (localDebug) HS_Util.debug("For form:[" + form + "] doing nothing!", "warn", dbarLoc);
				return result;
			}
			facUnid = thisDoc.getItemValueString("ParentDocumentUNID");
			if ("".equals(facUnid)) {
				facUnid = thisDoc.getItemValueString("ParentId");
				if (localDebug) HS_Util.debug("Trying ParentId for FacUNID:[" + facUnid + "] in ehsDB:[" + ehsDB.getServer() + "!!" + ehsDB.getFilePath() + "]", "debug", dbarLoc);
			}
			if ("".equals(facUnid)) {
				facUnid = (String) thisDoc.getItemValue("$REF").get(0);
				if (localDebug) HS_Util.debug("Trying $REF for FacUNID:[" + facUnid + "] in ehsDB:[" + ehsDB.getServer() + "!!" + ehsDB.getFilePath() + "]", "debug", dbarLoc);
			}
			if (!"".equals(facUnid)) {
				facility = HS_Util.getDocumentByUNID(ehsDB, facUnid, false);
			}
			if (facility == null) {
				facility = (Document) thisDoc.getParentDocument();
			}
			if (facility == null) {
				HS_Util.debug("Did not locate facility doc for facUnid:[" + facUnid + "] " + "in db:[" + ehsDB.getServer() + "!!" + ehsDB.getFilePath() + "]", "warn", dbarLoc);
				return result;
			}

			boolean pumrid = pullUpMostRecentInspDetails(facility);
			boolean calcNext = HS_CalculateNext.calculateNext(facility);
			if (pumrid || calcNext) {
				if (localDebug) HS_Util.debug("Saving facility: unid:[" + facility.getUniversalID() + "]", "debug", dbarLoc);
				facility.save(false, false);
				if (localDebug) HS_Util.debug("facility saved", "debug", dbarLoc);
				// End If
			}
			result = true;
		} catch (Exception e) {
			if (HS_Util.isDebugServer()) e.printStackTrace();
			HS_Util.debug(e.toString(), "error", dbarLoc);
			result = false;
		}
		return result;
		/*******
		 * Set Facility = HSGetDocumentByUNID( ehsDB, ThisDoc.ParentDocumentUNID, False )'done half right, why not use hsgetparent If
		 * Facility Is Nothing Then Exit Sub Else Set Facility = ThisDoc If Right$( Session.Username, 24 ) = "/OU=Office/O=HealthSpace" Then
		 * Msgbox "New Function Call Required!", 64, "Warning, re: JCOE-9EBN34" End If End If
		 * 
		 * If Facility.UniversalID = Facility.ParentDocumentUNID And isOnNotes(Session) Then Msgbox "Facility linked to itself"
		 * 
		 * If PullUpMostRecentInspDetails( Facility ) Or CalculateNext( Facility ) Then If Facility.NoteID <> ThisDoc.NoteID Then
		 * Facility.Save False, False End If End If
		 ****************/
	}

	@SuppressWarnings("deprecation")
	public static void pushUpDisplayFollowup(Document idoc, String createdFromInspID) {
		// final boolean localDebug = HS_Util.isDebugServer();
		final boolean localDebug = false;
		final String dbarLoc = "HS_SyncDocs.pushUpDisplayFollowup";

		try {
			Database ehsdb = idoc.getParentDatabase();
			View luview = ehsdb.getView("(DocumentIDLookup)");

			Document srcdoc = luview.getDocumentByKey(createdFromInspID, true);

			if ("Yes".equalsIgnoreCase(srcdoc.getItemValueString("DisplayFollowUp"))) {
				srcdoc.replaceItemValue("DisplayFollowUp", "No");
				srcdoc.save(true, false);
				if (localDebug) HS_Util.debug("Set DisplayFollowUp=No in doc with unid:[" + srcdoc.getUniversalID() + "]", "debug", dbarLoc);
			} else {
				// if (localDebug) HS_Util.debug("DisplayFollowUp already set to No in doc with unid:[" + srcdoc.getUniversalID() + "]",
				// "debug", dbarLoc);
			}
		} catch (Exception e) {
			if (HS_Util.isDebugServer()) e.printStackTrace();
			HS_Util.debug(e.toString(), "error", dbarLoc);
		}
	}
}
