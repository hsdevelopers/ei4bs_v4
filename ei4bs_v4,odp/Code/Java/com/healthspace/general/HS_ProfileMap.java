package com.healthspace.general;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.DocumentCollection;
import org.openntf.domino.Item;
import org.openntf.domino.NoteCollection;

import com.ibm.jscript.InterpretException;
import com.ibm.jscript.std.ObjectObject;
import com.ibm.xsp.extlib.util.ExtLibUtil;

/*
 * HS_ProfileMap IS INTENDED TO BE IMPLEMENTED AS A MANAGED BEAN
 * 
 * Here is the XML for the faces-config.xml file:

 <managed-bean id="hs_ProfileMap">
 <managed-bean-name>hs_ProfileMap</managed-bean-name>
 <managed-bean-class>com.healthspace.general.HS_ProfileMap</managed-bean-class>
 <managed-bean-scope>application</managed-bean-scope>
 </managed-bean>

 */

public class HS_ProfileMap extends HS_base {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String LoadedScopeName = "HSP_AllLoaded";
	public static final String ScopePrefix = "HSP_";

	// private Map<String, Object> profiles = null;

	public HS_ProfileMap() {
	}

	@SuppressWarnings("unchecked")
	public void init() {
		final boolean localDebug = false;
		String dbarLoc = getSession().getCurrentDatabase().getFilePath() + "." + "HS_ProfileMap.init()";
		/*
		 * Get the first database and use it for the processing
		 */
		Map<String, Object> appScope = ExtLibUtil.getApplicationScope();
		Vector<ObjectObject> dbProfiles = (Vector<ObjectObject>) appScope.get("dbProfiles");
		String dbkey;
		try {
			dbkey = dbProfiles.firstElement().get("key").stringValue();
		} catch (InterpretException e) {
			System.out.print(dbarLoc + " - ERROR: " + e.toString());
			return;
		}
		if (localDebug) debug(dbarLoc + " - callinig init with key: " + dbkey);
		this.init(dbkey);
	}

	public void init(String dbkey) {
		Map<String, Object> appScope = ExtLibUtil.getApplicationScope();
		String profilesLoaded = (String) appScope.get(LoadedScopeName);

		if ((profilesLoaded == null) || "loaded".equalsIgnoreCase(profilesLoaded)) {
			generateProfileMap(dbkey);
		} else {
			debug("profiles already loaded");
		}
	}

	private void generateProfileMap(String dbkey) {
		Database wrkdb = null;
		NoteCollection nc = null;
		Document doc1 = null;
		Document profileDocument = null;
		DocumentCollection profileDocColl = null;

		final boolean localDebug = false;
		String dbarLoc = getSession().getCurrentDatabase().getFilePath() + "." + "HS_ProfileMap.generateProfileMap(\"" + dbkey + "\")";

		try {
			HS_database hsdb = new HS_database();
			wrkdb = hsdb.getdb(dbkey);
			if (localDebug) debug(dbarLoc + " - Running on database: " + wrkdb.getTitle());

			nc = wrkdb.createNoteCollection(false);
			nc.setSelectProfiles(true);
			nc.buildCollection();
			String formName = "";

			// debug("nc.count is: " + nc.getCount());
			if (nc.getCount() > 0) {
				String id = nc.getFirstNoteID();
				HashSet<String> keys = new HashSet<String>();
				doc1 = null;

				while (id.length() > 0) {
					// We are putting all profile documents, Map overwrite same
					// keys.
					// So; we'll have a unique list of keys in our profiles Map
					doc1 = wrkdb.getDocumentByID(id);
					formName = doc1.getItemValueString("form");
					if (!"".equals(formName)) {
						keys.add(formName);
						if (localDebug) debug(dbarLoc + " - Added profile form: " + formName);
					}
					id = nc.getNextNoteID(id);
				}
				// After getting all profiles keys(profile form name list)
				// we populate them with the latest profile documents
				Map<String, Object> appScope = ExtLibUtil.getApplicationScope();

				for (String key : keys) {
					// debug("profile lookup for key: " + key);
					if (!appScope.containsKey(ScopePrefix + key)) {
						profileDocument = null;
						profileDocColl = wrkdb.getProfileDocCollection(key);
						profileDocument = profileDocColl.getFirstDocument();
						if (profileDocColl.getCount() > 1) {
							int pdocctr = 0;
							for (Document iterationDocument : profileDocColl) {
								if (pdocctr > 0) {
									Date pdlm = profileDocument.getLastModified().toJavaDate();
									Date idlm = iterationDocument.getLastModified().toJavaDate();
									if (pdlm.getTime() > idlm.getTime()) {
										profileDocument = iterationDocument; // Keep
										// latest
									}
								}
								pdocctr++;
							}
						}
						// At the end of the loop we'll have the latest version
						// of profile document

						if (localDebug) debug(dbarLoc + " - profile Being processed for key: " + key);

						HashMap<String, Object> profile = new HashMap<String, Object>();

						Iterator<Item> itr = profileDocument.getItems().iterator();
						Item itm = null;

						while (itr.hasNext()) {
							itm = itr.next();
							if (itm.getName().charAt(0) != '$') {
								try {
									profile.put(itm.getName(), itm.getValues());
								} catch (Exception e) {
									debug(dbarLoc + " - Exception getValue for item: " + itm.getName() + " type:" + itm.getTypeEx(), "error");
									System.out.print(dbarLoc + " - Exception getValue for item: " + itm.getName() + " type:" + itm.getTypeEx());
								}
							}
						}
						appScope.put(ScopePrefix + key, profile);
					}
				}
				if (localDebug) debug(dbarLoc + " - All profiles processed...");
				appScope.put(LoadedScopeName, "loaded");
			} else {
				if (localDebug) debug(dbarLoc + " - No profiles to process.");
			}
		} catch (Exception e) {
			System.out.print(dbarLoc + " - Exception: " + e.toString());
			debug(dbarLoc + " - Exception: " + e.toString(), "error");
		} finally {
		}
	}
}
