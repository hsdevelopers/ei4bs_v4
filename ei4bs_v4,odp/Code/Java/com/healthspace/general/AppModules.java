package com.healthspace.general;

import java.io.Serializable;
import java.util.Map;
import java.util.Vector;

import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.View;
import org.openntf.domino.ViewEntry;
import org.openntf.domino.ViewEntryCollection;

import com.healthspace.tools.JSFUtil;

public class AppModules implements Serializable {

	private static final long	serialVersionUID	= 1L;

	private static AppModule[]	modules;

	public AppModules() {
	}

	private static AppModule[] expand(AppModule[] orig) {
		AppModule[] newArray = new AppModule[orig.length + 1];
		System.arraycopy(orig, 0, newArray, 0, orig.length);

		// an alternative to using System.arraycopy would be a for-loop:
		// for(int i = 0; i < orig.length; i++)
		// newArray[i] = orig[i];

		return newArray;
	}

	private static AppModule[] buildModules() {
		final String dbarLoc = "AppModules.buildModules";
		String dbPath = "pathNotKnown.";
		final boolean localDebug = ("Yes".equals(HS_Util.getAppSettingString("debug_AppModules")));

		AppModule[] result = null;
		try {
			// if (localDebug) HS_Util.debug("starting routine", "debug", dbarLoc);
			if ( localDebug ) System.out.print(dbarLoc + ": starting routine");
			Database database = HS_Util.getSession().getCurrentDatabase();
			Database eidb = null;
			dbPath = database.getFilePath() + ".";
			Vector<Object> cvals = null;
			Vector<String> viewsNotFound = new Vector<String>();
			View mView = database.getView("LUAppModules");
			mView.refresh();
			ViewEntryCollection mEntries = (mView == null) ? null : mView.getAllEntries();
			// ViewEntry mEntry = (mEntries == null) ? null : mEntries.getFirstEntry();
			if ( mEntries == null || mEntries.getCount() == 0 ) {
				// {dbkey:"eiRoot", hpage:true, tbar:true, image:"icn_par_food.jpg", label:"Food", oname:"Food", slabel:"Food",
				// vname:"Food\\By Status"},
				result = new AppModule[0];
				result[0] = new AppModule();
				result[0].init("eiRoot", true, true, "icn_par_food.jpg", "Food", "Food", "Food", "Food\\ByStatus");
			} else {
				if ( localDebug ) System.out.print(dbarLoc + ": Found [" + mEntries.getCount() + "] entries to process");
				result = new AppModule[0];
				int idx = -1;
				int ccount = 0;
				for (ViewEntry mEntry : mEntries) {
					cvals = mEntry.getColumnValues();
					if ( "Y".equals(cvals.get(2)) ) {
						idx++;
						result = expand(result);
						result[idx] = new AppModule();
						result[idx].init(cvals);
						ccount = mEntry.getDocument().getResponses().getCount();
						if ( localDebug ) System.out.print(dbPath + dbarLoc + ": added entry [" + idx + "] for :[" + result[idx].getLabel() + "] has [" + ccount + "] children");
						if ( ccount > 0 ) {
							try {
								AppModuleView[] aviews = new AppModuleView[ccount];
								int aidx = -1;
								for (Document rdoc : mEntry.getDocument().getResponses()) {
									aidx++;
									aviews[aidx] = new AppModuleView();
									aviews[aidx].init(rdoc);
								}
								result[idx].setViews(aviews);
							} catch (Exception e2) {
								System.out.print(dbPath + dbarLoc + ".AppModuleView build ERROR: " + e2.toString());
							}
						}
						try {
							if ( viewsNotFound.contains(result[idx].getV4rname()) ) {
								result[idx].setV4rname(result[idx].getVname());
							} else {
								if ( eidb == null ) eidb = new HS_database().getdb("eiRoot");
								View testview = null;
								if ( eidb != null ) testview = eidb.getView(result[idx].getV4rname());
								if ( testview == null ) {
									viewsNotFound.add(result[idx].getV4rname());
									result[idx].setV4rname(result[idx].getVname());
								}
							}
						} catch (Exception e2) {
							System.out.print(dbPath + dbarLoc + ".v4rname.checks ERROR: " + e2.toString());
							viewsNotFound.add(result[idx].getV4rname());
							result[idx].setV4rname(result[idx].getVname());
						}
					}
				}
			}
		} catch (Exception e) {
			if ( localDebug ) System.out.print(dbPath + dbarLoc + ": ERROR: " + e.toString());
			e.printStackTrace();
		}
		try {
			JSFUtil.getApplicationScope().put("moduleDataJava", result);
			if ( localDebug ) System.out.print(dbPath + dbarLoc + ".appScopeAdd: added result to app scope with [" + result.length + "] entries");
		} catch (Exception e3) {
			if ( localDebug ) System.out.print(dbPath + dbarLoc + ".appScopeAdd: " + e3.toString());
		}
		return result;
	}

	public static AppModule[] getModules() {
		if ( modules == null ) {
			modules = buildModules();
		}
		return modules;
	}

	public static boolean resetModules() {
		final String dbarLoc = "AppModules.resetModules";
		boolean result = false;
		// final boolean localDebug = ("Yes".equals(HS_Util.getAppSettingString("debug_AppModules")));
		modules = null;
		try {
			Map<String, Object> ascope = JSFUtil.getApplicationScope();
			ascope.remove("moduleDataJava");
			ascope.remove("moduleData");
			Map<String, Object> sscope = JSFUtil.getSessionScope();
			sscope.remove("active_module");
			result = true;
		} catch (Exception e3) {
			System.out.print(dbarLoc + " ERROR: " + e3.toString());
		}
		return result;
	}

	public static String translateFormToFType(String alias) {
		String rslt = alias + " Facilitiy";
		if ( "CampFacility".equals(alias) ) {
			rslt = "Campgrounds";
		} else if ( "LaborCampFacility".equals(alias) ) {
			rslt = "Labor Camps";
		} else if ( "SummerCampFacility".equals(alias) ) {
			rslt = "Summer Camps";
		} else if ( "HotelFacility".equals(alias) ) {
			rslt = "Hotels / Motels";
		} else if ( "JailFacility".equals(alias) ) {
			rslt = "Correctional Institute Facilities";
		} else if ( "WaterFacility".equals(alias) ) {
			rslt = "Water Systems";
		} else if ( "SepticRemovalFacility".equals(alias) ) {
			rslt = "Septic Removals";
		} else if ( "TemporaryVendorFacility".equals(alias) ) {
			rslt = "Temporary Vendors";
		} else if ( "MobileHomeFacility".equals(alias) ) {
			rslt = "Manufactured Home Communities";
		} else if ( "SewageSystem".equals(alias) ) {
			rslt = "On-site Sewage Systems";
		} else if ( "SubdivisionApplication".equals(alias) ) {
			rslt = "Subdivision Applications";
		} else if ( "WaterPrivateWell".equals(alias) ) {
			rslt = "Private Wells";
		} else if ( alias.endsWith("Facility") ) {
			rslt = alias.substring(0, alias.lastIndexOf("Facility")) + " Facilities";
		}

		return rslt;
	}

	public static String translateOnameToFType(String oname) {
		String rslt = oname + " Facilities";
		if ( "Camps".equals(oname) ) {
			rslt = "Campgrounds";
		} else if ( "Labor Camps".equals(oname) ) {
			rslt = "Labor Camps";
		} else if ( "Summer Camps".equals(oname) ) {
			rslt = "Summer Camps";
		} else if ( "HotelMotel".equals(oname) ) {
			rslt = "Hotels / Motels";
		} else if ( "Jail".equals(oname) ) {
			rslt = "Correctional Institute Facilities";
		} else if ( "Water".equals(oname) ) {
			rslt = "Water Systems";
		} else if ( "Septic".equals(oname) ) {
			rslt = "Septic Removals";
		} else if ( "TemporaryVendor".equals(oname) ) {
			rslt = "Temporary Vendors";
		} else if ( "MobileHome".equals(oname) ) {
			rslt = "Manufactured Home Communities";
		} else if ( "SewageSystem".equals(oname) ) {
			rslt = "On-site Sewage Systems";
		} else if ( "SubdivisionApplication".equals(oname) ) {
			rslt = "Subdivision Applications";
		} else if ( "WaterPrivateWell".equals(oname) ) {
			rslt = "Private Wells";
		} else if ( "Recreational Water".equals(oname) ) {
			rslt = "Pool Facilities";
		} else if ( "RecreationalWater".equals(oname) ) {
			rslt = "Pool Facilities";
		} else if ( "BodyArt".equals(oname) ) {
			rslt = "Bodyart Facilities";
			// } else if (oname.endsWith("Facility")) {
			// rslt = oname.substring(0, oname.lastIndexOf("Facility")) + " Facilities";
		}

		return rslt;
	}
}
