package com.healthspace.general;

import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import com.ibm.xsp.extlib.util.ExtLibUtil;

public class TimeTracking extends HS_base {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	private String	form;
	private String	unid;

	private String	bMultiTime;
	private String	documentId;
	private String	editModule;
	private String	eho;
	private String	facilityName;
	private String	inspectionType;
	private String	magDistrict;
	private String	mileage;
	private String	module;
	private String	parentDocumentId;
	private String	parentForm;
	private String	riskRating;
	private String	source;
	private String	timeIn;
	private String	timeOut;
	private String	timeType;
	private String	trackingComments;
	private String	viewDescription;

	private Date	date;
	private String	dateStr;
	private Date	dateCreated;
	private Date	viewDate;

	private double	hours;
	private double	min;
	private double	timeSpent;

	private boolean	deleteMeFlag;

	public TimeTracking() {
	}

	public String getBMultiTime() {
		return bMultiTime;
	}

	public String buildViewDescription() {
		final String dbarLoc = "TimeTracking.buildViewDescription";
		final boolean localDebug = false;

		String result = "";
		Date wdt = null;
		try {
			// Date wdt = this.cvrtStringToDate(this.getDateStr());
			wdt = HS_Util.cvrtStringToDate(this.getDateStr());
		} catch (Exception e) {
			// HS_Util.debug("String date:[" + this.getDateStr() +
			// "] caused error: " + e.toString(), "error", dbarLoc);
			wdt = null;
		}
		if ( wdt == null ) wdt = this.getDate();
		String fmtdate = HS_Util.formatDate(wdt, "d-MMM-yyyy");
		if ( localDebug ) HS_Util.debug("DateStr:[" + this.getDateStr() + "] date is:[" + wdt.toString() + "] formatted:[" + fmtdate + "]", "debug", dbarLoc);
		result += fmtdate;
		result += " Time tracking for " + this.getEho();
		Double th = this.getTimeSpent();
		result += " - " + th.toString() + " hrs.";
		result += " (" + this.getTimeType() + ")";
		this.setViewDescription(result);
		return result;
	}

	public Date xcvrtStringToDate(String dtstr) {
		final String dbarLoc = "TimeTracking.xcvrtStringToDate";
		// final boolean localDebug = false;

		Date wdt = null;
		Calendar cal = Calendar.getInstance();
		String[] dtparts = dtstr.split("-");
		int year = 0;
		int mon = 0;
		int date = 0;
		if ( dtparts.length == 3 ) {
			try {
				year = Float.valueOf(dtparts[0]).intValue();
				mon = Float.valueOf(dtparts[1]).intValue();
				date = Float.valueOf(dtparts[2]).intValue();
				cal.set(year, mon, date);
				wdt = cal.getTime();
			} catch (Exception e) {
				debug(e.toString(), "debug", dbarLoc);
			}
		} else if ( wdt == null ) {
			wdt = null;
		}
		return wdt;
	}

	public String getDocumentId() {
		return documentId;
	}

	public String getEditModule() {
		return editModule;
	}

	public String getEho() {
		return eho;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public String getForm() {
		if ( form == null ) form = "TimeTracking";
		if ( "".endsWith(form) ) form = "TimeTracking";
		return form;
	}

	public String getInspectionType() {
		return inspectionType;
	}

	public String getMagDistrict() {
		return magDistrict;
	}

	public String getMileage() {
		if ( mileage == null ) mileage = "";
		return mileage;
	}

	public String getModule() {
		return module;
	}

	public String getParentDocumentId() {
		return parentDocumentId;
	}

	public String getParentForm() {
		return parentForm;
	}

	public String getRiskRating() {
		return riskRating;
	}

	public String getSource() {
		return source;
	}

	public String getTimeIn() {
		return timeIn;
	}

	public String getTimeOut() {
		return timeOut;
	}

	public String getTimeType() {
		return timeType;
	}

	public String getTrackingComments() {
		if ( trackingComments == null ) trackingComments = "";
		return trackingComments;
	}

	public String getUnid() {
		if ( unid == null ) unid = "";
		return unid;
	}

	public String getViewDescription() {
		return viewDescription;
	}

	public Date getDate() {
		return date;
	}

	public String getDateStr() {
		return dateStr;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public Date getViewDate() {
		return viewDate;
	}

	public double getHours() {
		return hours;
	}

	public double getMin() {
		return min;
	}

	public double getTimeSpent() {
		return timeSpent;
	}

	public void loadDefaults(Inspection insp) {
		this.source = this.getSession().getCommonUserName();
		this.date = new Date();
		this.dateCreated = new Date();
		this.dateStr = HS_Util.formatDate(this.date, "yyyy-MM-dd");
		this.documentId = (String) this.getSession().evaluate("@Unique").get(0);
		this.editModule = "";
		this.facilityName = insp.getFacilityName();
		this.form = "TimeTracking";
		this.hours = 0;
		this.inspectionType = insp.getInspType();
		this.mileage = "";
		this.min = 0;
		this.module = ""; // TODO Look this up
		this.parentDocumentId = "";
		this.source = ExtLibUtil.getXspContext().getUser().getCommonName();
		this.timeSpent = 0;
		this.trackingComments = "";
		this.deleteMeFlag = false;

		// HS_profiles hspro = new HS_profiles();
		// Object tempObj = null;
		// tempObj = HS_profiles.get("eiRoot", "MasterSettings", "InspectionMultiTime");
		String inspMT = HS_Util.getMasterSettingsString("InspectionMultiTime");
		boolean bmultiline = ("Yes".equalsIgnoreCase(inspMT));
		if ( insp.getForm().endsWith("Report") ) {
			this.date = insp.getInspectionDate();
			this.eho = insp.getEho();
			this.timeType = "Inspection";
			this.magDistrict = insp.getMagDistrict();

			this.riskRating = insp.getRiskRating();
			this.parentForm = ("TemporaryVendorFacility".equals(insp.getParentForm())) ? insp.getParentForm() : insp.getForm();
			this.bMultiTime = (bmultiline) ? "Yes" : "No";
			if ( !bmultiline ) {
				// TODO - Set up as one time report
			}
		}
		this.lsGetProgramArea();
	}

	// This is cloned from the GetProgramArea method in the Time Tracking LS
	// library

	private void lsGetProgramArea() {
		if ( !"".equals(this.module) ) return; // Already defined
		if ( "".equals(this.parentForm) ) return; // Cannot determine it

		// HS_profiles hspro = new HS_profiles();
		// Object tempObj = null;
		// tempObj = HS_profiles.get("eiRoot", "MasterSettings", "FacilityType");
		// Vector<String> facilityTypes = (Vector<String>) tempObj;
		Vector<String> facilityTypes = HS_Util.getMasterSettingsVector("FacilityType");
		if ( "".equals(facilityTypes.get(0)) ) return;

		Vector<String> facilityKeys = new Vector<String>();
		facilityKeys.add("beach");
		facilityKeys.add("bodyart");
		facilityKeys.add("camp");
		facilityKeys.add("childcare");
		facilityKeys.add("dairy");
		facilityKeys.add("fairground");
		facilityKeys.add("food");
		facilityKeys.add("garbage");
		facilityKeys.add("general");
		facilityKeys.add("hotel");
		facilityKeys.add("housing");
		facilityKeys.add("jail");
		facilityKeys.add("mobile");
		facilityKeys.add("pool");
		facilityKeys.add("well");
		facilityKeys.add("radiation");
		facilityKeys.add("school");
		facilityKeys.add("septic");
		facilityKeys.add("sewage");
		facilityKeys.add("waste");
		facilityKeys.add("water");
		facilityKeys.add("vendor");

		String matchKey = "";
		String pform = this.parentForm;
		if ( pform == null ) pform = "VeryBadFormName";
		for (String testkey : facilityKeys) {
			if ( pform.toLowerCase().contains(testkey) ) {
				matchKey = testkey;
			}
		}

		if ( "".equals(matchKey) ) return;

		if ( "camp".equals(matchKey) ) {
			if ( this.parentForm.toLowerCase().contains("summer") ) {
				matchKey = "summer";
			} else if ( this.parentForm.toLowerCase().contains("labor") ) {
				matchKey = "labor";
			}
		}

		for (String eachType : facilityTypes) {
			if ( eachType.toLowerCase().contains(matchKey) ) {
				if ( "camp".equals(matchKey) ) {
					if ( !(eachType.toLowerCase().contains("summer")) && !(eachType.toLowerCase().contains("labor")) ) {
						this.module = eachType;
					}
				} else {
					this.module = eachType;
				}
			}
		}
	}

	public void setBMultiTime(String multiTime) {
		bMultiTime = multiTime;
	}

	public void setDocumentId(String id) {
		documentId = id;
	}

	public void setEditModule(String module) {
		editModule = module;
	}

	public void setEho(String Eho) {
		this.eho = Eho;
	}

	public void setFacilityName(String name) {
		facilityName = name;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public void setInspectionType(String type) {
		inspectionType = type;
	}

	public void setMagDistrict(String district) {
		magDistrict = district;
	}

	public void setMileage(String mileage) {
		this.mileage = mileage;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public void setParentDocumentId(String documentId) {
		parentDocumentId = documentId;
	}

	public void setParentForm(String form) {
		parentForm = form;
	}

	public void setRiskRating(String rating) {
		riskRating = rating;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public void setTimeIn(String in) {
		timeIn = in;
	}

	public void setTimeOut(String out) {
		timeOut = out;
	}

	public void setTimeType(String type) {
		timeType = type;
	}

	public void setTrackingComments(String comments) {
		trackingComments = comments;
	}

	public void setUnid(String unid) {
		this.unid = unid;
	}

	public void setViewDescription(String description) {
		viewDescription = description;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}

	public void setDateCreated(Date created) {
		dateCreated = created;
	}

	public void setViewDate(Date date) {
		viewDate = date;
	}

	public void setHours(double hours) {
		this.hours = hours;
	}

	public void setMin(double min) {
		this.min = min;
	}

	public void setTimeSpent(double spent) {
		timeSpent = spent;
	}

	public boolean isDeleteMeFlag() {
		return deleteMeFlag;
	}

	public void setDeleteMeFlag(boolean deleteMeFlag) {
		this.deleteMeFlag = deleteMeFlag;
	}

	public double computeTimeSpent() {
		double spent = hours + (min / 60);
		if ( spent > 0 ) return spent;
		if ( !"".equals(timeIn) ) {
			if ( !"".equals(timeOut) ) {
				String[] iWrk = timeIn.split(":");
				double ihr = Double.parseDouble(iWrk[0]);
				double imin = Double.parseDouble(iWrk[1]);

				String[] oWrk = timeOut.split(":");
				double ohr = Double.parseDouble(oWrk[0]);
				double omin = Double.parseDouble(oWrk[1]);
				spent = (ihr < ohr) ? ohr - ihr : 0;
				spent += ((omin - imin) / 60);
			}
		}
		return spent;
	}

}
