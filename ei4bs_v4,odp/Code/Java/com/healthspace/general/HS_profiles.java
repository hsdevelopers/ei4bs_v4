package com.healthspace.general;

/*
 * HS_profiles IS INTENDED TO BE IMPLEMENTED AS A MANAGED BEAN
 * 
 * Here is the XML for the faces-config.xml file:
 * 	<managed-bean id="hs_profiles">
 * 		<managed-bean-name>hs_profiles</managed-bean-name>
 * 		<managed-bean-class>com.healthspace.general.HS_profiles</managed-bean-class>
 * 		<managed-bean-scope>application</managed-bean-scope>
 * 	</managed-bean>
 * 
 * primary interface is:
 * 	var values = hs_profiles(dbkey, profileName, fieldName);
 * 
 * 	Parmeters:
 * 		dbkey 		- 	database key used with the HS_database bean to get a handle to the database.
 * 		profileName -	Name of the profile to get the field from (eg: GlobalSettings, MasterSettings, etc
 * 		fieldName	-	Field name for the values to be returned
 * 
 * 	Results
 * 		values		- 	Vector of the values
 * 
 * CHANGES:
 * 6 Nov 2013 - newbs - modified to work with org.openntf.domino api
 */
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.faces.context.FacesContext;

import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.DocumentCollection;
import org.openntf.domino.Item;
import org.openntf.domino.RichTextItem;

import com.ibm.xsp.designer.context.XSPContext;

public class HS_profiles extends HS_base {
	private static final long serialVersionUID = 1L;

	// This map of profiles is keyed by dbkey + "-" + pname
	private static Map<String, Object> profileMap = null;

	public static Map<String, Object> getProfileMap() {
		return profileMap;
	}

	// public void setProfileMap(Map<String, Object> profileMap) {
	// this.profileMap = profileMap;
	// }

	private static HS_database hsdb;;

	public HS_database getHsdb() {
		return hsdb;
	}

	// public void setHsdb(HS_database hsdb) {
	// this.hsdb = hsdb;
	// }

	/**
	 * 
	 */
	public HS_profiles() {
		if (profileMap == null) {
			initProfileMap();
		}
	}

	public String getHTML(String dbkey, String pname, String fname) {
		final boolean localDebug = HS_Util.isDebugServer();
		final String dbarLoc = "HS_profiles.getHTML";

		Object wobj = get(dbkey, pname, fname);
		String result = "WIP";
		if (wobj == null) {
			if (localDebug) HS_Util.debug("dbkey:[" + dbkey + "] pname:[" + pname + "] fname:[" + fname + "] wobj is [null]", "debug", dbarLoc);
		} else if ("java.lang.String".equals(wobj.getClass().getName())) {
			result = "<span>" + wobj + "</span>";
		} else {
			result = "[" + wobj.getClass().getName() + "]";
			if (localDebug) HS_Util.debug("dbkey:[" + dbkey + "] pname:[" + pname + "] fname:[" + fname + "] wobj is a:[" + wobj.getClass().getName() + "]:[" + wobj + "]", "debug", dbarLoc);
		}
		return result;
	}

	public static Object get(String dbkey, String pname, String fname) {
		return HS_profiles.get(dbkey, pname, fname, false);
	}

	@SuppressWarnings("unchecked")
	public static Object get(String dbkey, String pname, String fname, boolean localDebug) {
		// String dbarLoc = "";
		// localDebug = getSession().getEffectiveUserName().contains("Henry Newberry");
		final String dbarLoc = "HS_profiles.get";
		Object rslt = null;
		try {
			String host = XSPContext.getXSPContext(FacesContext.getCurrentInstance()).getUrl().getHost();
			if (localDebug) HS_Util.debug("parms: dbkey:[" + dbkey + "] pname:[" + pname + "] fname:[" + fname + "] hst:[" + host + "]", "debug", dbarLoc);
			Map<String, Object> pmap = getPMap(dbkey + "-" + pname + "-" + host, localDebug);
			if (pmap == null) {
				// if (localDebug)
				HS_Util.debug("getPMap returned null value for key: " + dbkey + "-" + pname + "-" + fname + ".", "warn", dbarLoc);
				return rslt;
			}
			Map<String, Object> itemMap = (Map<String, Object>) pmap.get("items");
			Vector<String> richTextItems = (Vector<String>) pmap.get("rtitems");
			if (itemMap == null) {
				if (localDebug) HS_Util.debug("pmap does not have items or is null.", "debug", dbarLoc);
				return rslt;
			}
			if (richTextItems.contains(fname.toLowerCase())) {
				String rstr = "" + fname + " is a RICH TEXT item (2).";
				rstr += "<h3>Rich text items are:</h3><ul>";
				for (String rtiname : richTextItems) {
					rstr += "<li>" + rtiname + "</li>";
				}
				rstr += "</ul>";

				rslt = rstr + "";
				HS_Util.getSession().setConvertMime(true);
			} else if (itemMap.containsKey(fname.toLowerCase())) {
				rslt = itemMap.get(fname.toLowerCase());
				if (localDebug && rslt == null) HS_Util.debug("found a map entry for:[" + fname + "] which is null", "debug", dbarLoc);
			} else {
				Vector<String> missing = (Vector<String>) pmap.get("missing");
				if (!missing.contains(fname.toLowerCase())) {
					if (localDebug) HS_Util.debug("ItemMap for [" + dbkey + "-" + pname + "] does not have fieldname:[" + fname + "]", "warn", dbarLoc);
					missing.add(fname.toLowerCase());
					pmap.put("missing", missing);
				}
				// send back a single empty string.
				rslt = new java.util.Vector().add("");
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
			if (HS_Util.isDebugServer()) e.printStackTrace();
		}
		return rslt;
	}

	public static Map<String, Object> getPMap(String pMapKey) {
		return getPMap(pMapKey, false);
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> getPMap(String pMapKey, boolean localDebug) {
		String msgContext = "HS_profiles.getPMap";
		Map<String, Object> rslt = null;
		String user = HS_Util.getSession().getEffectiveUserName();
		if (profileMap == null) {
			if (localDebug) HS_Util.debug("initializing profile map", "debug", msgContext);
			initProfileMap();
		}
		String luser = (String) profileMap.get("user");
		// if (localDebug) HS_Util.debug("checking profile map for user:[" + luser + "]", "debug", msgContext);
		if (!(user.equals(luser))) {
			// User has changed, reinitialize the map
			if (localDebug) HS_Util.debug("User has changed, reinitialize the map", "debug", msgContext);
			initProfileMap();
		}
		// String host = XSPContext.getXSPContext(FacesContext.getCurrentInstance()).getUrl().getHost();
		// String phost = (String) profileMap.get("host");
		// if (localDebug) HS_Util.debug("checking profile map for host:[" + host + "] versus phost:[" + phost + "]", "debug", msgContext);
		// if (!(phost.equals(host))) {
		// // Host has changed, reinitialize the map
		// if (localDebug) HS_Util.debug("Host has changed, reinitialize the map. host:[" + host + "] versus phost:[" + phost + "]",
		// "debug", msgContext);
		// initProfileMap();
		// }
		if (!(profileMap.containsKey(pMapKey))) {
			addPMap(pMapKey, localDebug);
		}
		rslt = (Map<String, Object>) profileMap.get(pMapKey);
		String pstatus = (String) rslt.get("status");
		if (!("OK".equals(pstatus))) {
			HS_Util.debug("Profile " + pMapKey + " has a bad status: [" + pstatus + "]", "error", msgContext);
			return null;
		}
		return rslt;
	}

	public void resetProfileMap() {
		initProfileMap();
	}

	private static void initProfileMap() {
		profileMap = new HashMap<String, Object>();
		profileMap.put("status", "building");
		profileMap.put("user", HS_Util.getSession().getEffectiveUserName());
		String host = XSPContext.getXSPContext(FacesContext.getCurrentInstance()).getUrl().getHost();
		profileMap.put("host", host);

		hsdb = new HS_database();
	}

	@SuppressWarnings("unchecked")
	private static void addPMap(String pMapKey, boolean localDebug) {
		// final boolean localDebug = false;
		// localDebug = getSession().getEffectiveUserName().contains(
		// "Henry Newberry");

		final String dbarLoc = "HS_profiles.addPMap." + pMapKey;
		Date start = new Date();

		Map<String, Object> newMap = new HashMap<String, Object>();
		newMap.put("status", "building");
		profileMap.put(pMapKey, newMap);
		newMap = (Map<String, Object>) profileMap.get(pMapKey);
		String dbkey = pMapKey.split("-")[0];
		String pname = pMapKey.split("-")[1];

		if (localDebug) {
			HS_Util.debug("adding a map for [" + pMapKey + "]", "debug", dbarLoc);
		}
		Database tdb = gerProfileDb(dbkey);
		if (tdb == null) {
			newMap.put("dbReplicaID", "");
			if (localDebug) {
				HS_Util.debug("Database for key: " + dbkey + " not available.", "debug", dbarLoc);
				newMap.put("status", "error");
				return;
			}
		} else {

			newMap.put("dbReplicaID", tdb.getReplicaID());
			Document pdoc = getProfileDoc(tdb, pname);

			// if (localDebug && pdoc != null)
			// debug("profile found for " + pname + " with ["
			// + pdoc.getItems().size() + "] items.", "debug", dbarLoc);
			if (pdoc == null) {
				HS_Util.debug("NO PROFILE found for " + pname, "error", dbarLoc);
				return;
			}
			Item item = null;
			RichTextItem rtitem = null;
			java.util.Vector rtitemNames = new java.util.Vector();
			// org.openntf.domino.Base itemObj = null;
			Map<String, Object> itemMap = new HashMap<String, Object>();
			int ctr = 0;
			// /String lastItemName = "";
			for (org.openntf.domino.Base itemObj : pdoc.getItems()) {
				ctr++;
				if (itemObj.getClass().getName().endsWith(".Item")) {
					item = (Item) itemObj;
					try {
						if (!"$FILE".equalsIgnoreCase(item.getName())) {
							itemMap.put(item.getName().toLowerCase(), item.getValues());
						}
					} catch (Exception e) {
						HS_Util.debug("Got error on insert of item [" + item.getName() + "] " + e.toString(), "debug", dbarLoc);
					}
					// lastItemName = item.getName();
				} else if (itemObj.getClass().getName().endsWith("RichTextItem")) {
					rtitem = (RichTextItem) itemObj;
					rtitemNames.add(rtitem.getName().toLowerCase());
					// itemMap.put(rtitem.getName(), rtitem.getName() + " - IS A RICHTEXT ITEM");
				} else {
					if (localDebug) HS_Util.debug(ctr + ". not dealing with item of type: [" + itemObj.getClass().getName() + "]", "debug", dbarLoc);
				}
			}

			newMap.put("rtitems", rtitemNames);
			newMap.put("items", itemMap);
			newMap.put("status", "OK");
			newMap.put("missing", new java.util.Vector());
			long end = new Date().getTime() - start.getTime();
			newMap.put("buildTime", end);
			if (localDebug) {
				HS_Util.debug("Done building [" + pMapKey + "] in db: [" + tdb.getServer() + "!!" + tdb.getFilePath() + "] with [" + itemMap.size() + "] item entries and [" + rtitemNames.size()
						+ "] rt items. " + "Took [" + end + "] milliseconds.", "debug", dbarLoc);
			}
		}
	}

	private static Database gerProfileDb(String dbkey) {
		final String dbarLoc = "HS_profiles.getProfileDb";
		Database tdb = null;
		try {
			// tdb = Factory.fromLotus(hsdb.getdb(dbkey), org.openntf.domino.Database.class, null);
			tdb = hsdb.getdb(dbkey);
		} catch (Exception e) {
			HS_Util.debug("Error accessing database with key[" + dbkey + "]: " + e.toString(), "error", dbarLoc);
		}
		return tdb;
	}

	private static Document getProfileDoc(Database tdb, String pname) {
		Document profileDocument = null;
		int pdocctr = 0;
		DocumentCollection profileDocColl = null;

		final boolean localDebug = false;
		String dbarLoc = "";
		dbarLoc = HS_Util.getSession().getCurrentDatabase().getFilePath() + "." + "HS_profiles.getProfileDoc(\"" + pname + "\")";

		profileDocColl = tdb.getProfileDocCollection(pname);
		profileDocument = profileDocColl.getFirstDocument();
		if (profileDocColl.getCount() > 1) {
			for (Document iterationDocument : profileDocColl) {
				if (pdocctr > 0) {
					// if (profileDocument.getLastModified().timeDifference(iterationDocument.getLastModified()) > 0) {
					Date pdlm = profileDocument.getLastModified().toJavaDate();
					Date idlm = iterationDocument.getLastModified().toJavaDate();
					if (pdlm.getTime() > idlm.getTime()) {
						profileDocument = iterationDocument;
					}
				}
				pdocctr++;
			}
		}

		if (localDebug) {
			HS_Util.debug(dbarLoc + " got profile from [" + profileDocColl.getCount() + " possibilities.");
		}
		return profileDocument;
	}
}
