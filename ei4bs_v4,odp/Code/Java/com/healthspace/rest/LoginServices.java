package com.healthspace.rest;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.openntf.domino.ACL;
import org.openntf.domino.ACLEntry;
import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.Name;
import org.openntf.domino.Session;
import org.openntf.domino.View;
import org.openntf.domino.ViewEntry;
import org.openntf.domino.ViewEntryCollection;
import org.openntf.domino.utils.Factory;

import com.healthspace.general.HS_Util;

public class LoginServices {

	private String host = "";
	private String dbpath = "";
	// private String server = "";
	private StringBuilder baseUrl = null;

	private String[] sysurls;
	private String[] sysnames;
	// private String[] syshosts;
	// private String[] sysprotocols;
	private final int maxresults = 50;

	private String[] filenames;
	private String[] hosts;
	private String[] protocols;

	private String user = "";
	private String fullName = "";
	private String abbName = "";
	private Name nname = null;
	private String userOrg = "";
	private int touchNum = -1;

	private String loginType = "";
	private Date startDt;

	private boolean localDebug = false;

	public LoginServices() {
		this.localDebug = ("Yes".equals(HS_Util.getAppSettingString("debug_login_service")));

	}

	public String loginPost() {
		String msgContext = "LoginServices.loginPost";
		StringWriter sw = new StringWriter();
		HS_JsonWriter jw = new HS_JsonWriter(sw, false);
		boolean authorized = false;
		startDt = new Date();
		try {
			this.initStuff();
			if ( !this.isDebug() ) this.localDebug = "xHenry Newberry".equals(user);
			jw.startObject();
			jw.addProperty("version", "2016-05-027-001");
			RestUtil.addUserAgent(jw, sw);
			jw.addProperty("user", user);
			jw.addProperty("fullname", fullName);
			jw.addProperty("abbname", abbName);
			jw.addProperty("loginType", loginType);
			// if ("Yes".equals(HS_Util.getAppSettingString("debug_login_service") )) jw.addProperty("loginbaseurl", baseUrl.toString());

			// boolean authorized = isUserInHSTouchRole(jw);
			if ( "None".equals(loginType) ) {
				// Ignore this
			} else {
				authorized = ("anonymous".equalsIgnoreCase(fullName)) ? false : findRegCenterUserDb(jw);
				if ( authorized ) {
					jw.addProperty("authorized", "1");
					// if ("Yes".equals(HS_Util.getAppSettingString("debug_login_service") )) jw.addProperty("host", host);
					// jw.addProperty("baseurl", baseUrl.toString());
					if ( "MasterLogin".equals(loginType) ) {
						jw.startProperty("baseurl");
						jw.startArray();
						for (int i = 0; i < sysurls.length; i++) {
							if ( !("".equals(sysurls[i]) || null == sysurls[i]) ) {
								jw.startArrayItem();
								jw.startObject();
								jw.addProperty("url", sysurls[i]);
								jw.addProperty("name", sysnames[i]);
								jw.endObject();
								jw.endArrayItem();
							}
						}
						jw.endArray();
						jw.endProperty();
					}
				} else {
					jw.addProperty("authorized", "0");
					if ( "MasterLogin".equals(loginType) ) {
						if ( "Yes".equals(HS_Util.getAppSettingString("debug_login_service")) ) jw.addProperty("host", "");
						// jw.addProperty("baseurl", "");
						jw.startProperty("baseurl");
						jw.startArray();
						jw.endArray();
						jw.endProperty();
					}
				}
			}
			if ( this.isDebug() ) {
				jw.addProperty("xsource", msgContext);
				RestUtil.addDebugProperties(jw, sw, null);
				jw.addProperty("timeToCompletion", "Time to completion: [" + (new Date().getTime() - startDt.getTime()) + "]");
			}
			jw.endObject();
			// if (this.isDebug()) System.out.print(msgContext + ": User: [" + user + "] processed - authorized:[" + authorized + "]");
			if ( "Yes".equalsIgnoreCase(HS_Util.getAppSettingString("logLogins")) ) RestUtil.writeToAgentLog(sw.toString(), msgContext);
		} catch (IOException e) {
			System.out.print(msgContext + ": ERROR - " + e.toString());
			RestUtil.writeToAgentLog("ERROR: " + e.toString() + "\n\n" + sw.toString(), msgContext);
			if ( "Yes".equals(HS_Util.getAppSettingString("debug_login_service")) ) e.printStackTrace();
		} catch (Exception e) {
			System.out.print(msgContext + ": ERROR - " + e.toString());
			RestUtil.writeToAgentLog("ERROR: " + e.toString() + "\n\n" + sw.toString(), msgContext);
			if ( "Yes".equals(HS_Util.getAppSettingString("debug_login_service")) ) e.printStackTrace();
		}
		return sw.toString();
	}

	private boolean findRegCenterUserDb(HS_JsonWriter jw) throws IOException {
		final boolean localDebug = "Yes".equals(HS_Util.getAppSettingString("debug_login_service"));
		final String msgContext = "LoginServices.findRegCenterUserDb";
		final String regCtrReplicaid = "88256AFC00057048";
		boolean result = false;

		try {
			Session sess = Factory.getSession();
			String[] userParts = this.nname.getAbbreviated().split("/");
			userOrg = "";
			for (int i = 1; i < userParts.length; i++) {
				userOrg += "/" + userParts[i];
			}
			if ( localDebug ) jw.addProperty("userOrg", userOrg);

			if ( "Standard".equals(loginType) ) {
				result = checkDatabase(sess.getCurrentDatabase(), jw, 0);
			} else {
				Database regCenter = null;
				try {
					regCenter = HS_Util.getSessionAsSigner().getDatabase(regCtrReplicaid);
				} catch (Exception e1) {
					jw.addProperty("error", msgContext + ": " + e1.toString());
					if ( "Yes".equals(HS_Util.getAppSettingString("debug_login_service")) ) e1.printStackTrace();
					return false;
				}

				if ( regCenter == null ) {
					jw.addProperty("error", "registration center not found!");
					RestUtil.sendAlert(msgContext + ": registration center not found!");
					RestUtil.writeWarningToAgentLog("Registration Center nott Found!", "Henry Newberry/Office/HealthSpace", msgContext, "Resolved");
					return false;
				}
				// if (localDebug) jw.addProperty("regCenterPath", regCenter.getFilePath());
				// if (localDebug) jw.addProperty("regCenterServer", regCenter.getServer());
				if ( !regCenter.isOpen() ) {
					jw.addProperty("error", "user [" + user + "] does not have access to registration center!");
					RestUtil.sendAlert(msgContext + ": user [" + user + "] does not have access to registration center!");
					RestUtil.writeWarningToAgentLog("User [" + user + "] does not have access to registration center!", "Henry Newberry/Office/HealthSpace",
							msgContext, "Resolved");
					return false;
				}

				View dbview = regCenter.getView("DatabasesByOrgUnitForHSTouch");
				if ( dbview == null ) {
					jw.addProperty("error", "Unable to access regCenter view DatabasesByOrgUnit.");
					RestUtil.sendAlert(msgContext + ": Unable to access regCenter view DatabasesByOrgUnit.");
					RestUtil.writeWarningToAgentLog("Unable to access regCenter view DatabasesByOrgUnit.", "Henry Newberry/Office/HealthSpace", msgContext,
							"Resolved");
					return false;
				}

				this.sysurls = new String[maxresults];
				this.sysnames = new String[maxresults];
				String[] touchReplicaids = new String[maxresults];
				String[] prefservers = new String[maxresults];

				for (int idx = 0; idx < maxresults; idx++) {
					touchReplicaids[idx] = "";
					prefservers[idx] = "";
				}

				ViewEntryCollection dbvec = null;
				if ( "Office".equalsIgnoreCase(this.nname.getOrgUnit1()) || "Partner".equalsIgnoreCase(this.nname.getOrgUnit1()) ) {
					dbvec = dbview.getAllEntries();
				} else {
					dbvec = dbview.getAllEntriesByKey(userOrg);
				}

				if ( dbvec.getCount() == 0 ) {
					jw.addProperty("warning", "Found no entries in regCenter view [" + dbview.getName() + "] for userOrg:[" + userOrg + "]");
					dbvec = dbview.getAllEntriesByKey("*/HealthSpace");
					if ( dbvec.getCount() == 0 ) {
						String message = msgContext + ": Found no entries in regCenter view [" + dbview.getName() + "] for userOrg:[*/HealthSpace]";
						jw.addProperty("error", "Found no entries in regCenter view [" + dbview.getName() + "] for userOrg:[*/HealthSpace]");
						RestUtil.sendAlert(message);
						RestUtil.writeWarningToAgentLog(message, "Henry Newberry/Office/HealthSpace", msgContext, "Resolved");
						return false;
					}
				}

				int entrynum = -1;
				String torg = "";
				touchNum = -1; // Number of the valid hstouch entry.
				for (ViewEntry dbve : dbvec) {
					if ( dbve.isDocument() ) {
						entrynum += 1;
						torg = dbve.getColumnValues().get(0).toString();
						if ( localDebug ) jw.addProperty("debug" + entrynum, "[" + entrynum + "]: for org:[" + torg + "]");
						if ( "*/HealthSpace".equals(torg) && dbvec.getCount() > 1 ) {
							if ( localDebug ) jw.addProperty("debugxx" + entrynum, "[" + entrynum + "]: skipping demo entry.");
							entrynum = entrynum - 1;
						} else if ( entrynum < maxresults ) {
							touchReplicaids[entrynum] = dbve.getColumnValues().get(3).toString();
							filenames[entrynum] = dbve.getColumnValues().get(5).toString();
							protocols[entrynum] = dbve.getColumnValues().get(6).toString();
							hosts[entrynum] = dbve.getColumnValues().get(7).toString();
							prefservers[entrynum] = dbve.getColumnValues().get(8).toString();
						}
					}
				}
				// if (localDebug) {
				// jw.addProperty("db_touchReplicaids", "[" + touchReplicaids[0] + "][" + touchReplicaids[1] + "][" + touchReplicaids[2] +
				// "]");
				// jw.addProperty("db_protocols", "[" + protocols[0] + "][" + protocols[1] + "][" + protocols[2] + "]");
				// jw.addProperty("db_hosts", "[" + hosts[0] + "][" + hosts[1] + "][" + hosts[2] + "]");
				// jw.addProperty("db_filenames", "[" + filenames[0] + "][" + filenames[1] + "][" + filenames[2] + "]");
				// jw.addProperty("db_prefservers", "[" + prefservers[0] + "][" + prefservers[1] + "][" + prefservers[2] + "]");
				// // System.out.print(msgContext + " - touchReplicaids:[" + touchReplicaids[0] + "][" + touchReplicaids[1] + "]");
				// }

				// if (localDebug) {
				// jw.addProperty("timeToReplIdLookup", "Time to lookup replids: [" + (new Date().getTime() - startDt.getTime()) + "]");
				// }
				if ( "".equals(touchReplicaids[0]) ) {
					jw.addProperty("error", "No databases available for HS Touch users in userOrg:[" + userOrg + "]");
					RestUtil.sendAlert(msgContext + ": No databases available for HS Touch users in userOrg:[" + userOrg + "]");
					RestUtil.writeWarningToAgentLog("No databases available for HS Touch users in userOrg:[" + userOrg + "]",
							"Henry Newberry/Office/HealthSpace", msgContext, "Resolved");
					return false;
				}
				Database touchDb = null;
				for (int index = 0; index < maxresults; index++) {
					try {
						touchDb = null;
						if ( !"".equals(touchReplicaids[index]) ) {
							// if (localDebug) jw.addProperty("debug-gd-" + index, "Callng getDatabase with server[" + prefservers[index] +
							// "] replid:[" + touchReplicaids[index] + "]");
							touchDb = sess.getDatabase(prefservers[index], touchReplicaids[index]);
							if ( touchDb == null ) {
								touchDb = sess.getDatabase(touchReplicaids[index]);
							}
						}
					} catch (Exception e1) {
						touchDb = null;
						// jw.addProperty("error", "Could not access HS Touch database [" + index + "] for userOrg:[" + userOrg +
						// "] with replicaid [" + touchReplicaids[index] + "]");
					}

					if ( touchDb != null ) {
						// if (localDebug) jw.addProperty("touchDbPath_" + index, touchDb.getFilePath());
						// if (localDebug) jw.addProperty("touchDbServer_" + index, touchDb.getServer());

						if ( checkDatabase(touchDb, jw, index) ) result = true;
					} else if ( !"".equals(touchReplicaids[index]) ) {
						// jw.addProperty("DatabaseNotAccesed", "for index[" + index + "] relicaId:[" + touchReplicaids[index] + "]");
						jw.addProperty("error_" + index, "Could not access HS Touch database [" + index + "] for userOrg:[" + userOrg + "] with replicaid ["
								+ touchReplicaids[index] + "] on server:[" + prefservers[index] + "] file:[" + filenames[index] + "]");
						RestUtil.sendAlert(msgContext + ": Could not access HS Touch database [" + index + "] for userOrg:[" + userOrg + "] with replicaid ["
								+ touchReplicaids[index] + "] on server:[" + prefservers[index] + "] file:[" + filenames[index] + "]");
						RestUtil.writeWarningToAgentLog("Could not access HS Touch database [" + index + "] for userOrg:[" + userOrg + "] with replicaid ["
								+ touchReplicaids[index] + "] on server:[" + prefservers[index] + "] file:[" + filenames[index] + "]",
								"Henry Newberry/Office/HealthSpace", msgContext, "Resolved");
					}
				}
				if ( localDebug ) {
					jw.addProperty("timeToCompletion", "Time to completion: [" + (new Date().getTime() - startDt.getTime()) + "]");
				}
			}
			return result;
		} catch (Exception e) {
			jw.addProperty("error", msgContext + ": " + e.toString());
			if ( "Yes".equals(HS_Util.getAppSettingString("debug_login_service")) ) e.printStackTrace();
		}
		return false;
	}

	private boolean checkDatabase(Database touchDb, HS_JsonWriter jw, int index) throws IOException {
		boolean result = false;
		ACL acl = touchDb.getACL();
		// First look for entries with the exact USERS name
		ACLEntry acle = acl.getEntry(nname.getCanonical());
		if ( acle == null ) {
			// Next look for wildcard entries with the users organization...
			acle = acl.getEntry("*" + userOrg);
		}
		if ( acle == null ) {
			// Next Find any groups that have the HSTouch role and see if the user is a member....
			acle = findUserInHsTouchGroup(acl, nname, jw, index);
		}

		if ( acle == null ) {
			jw.addProperty("error_" + index, "No entry found in database:[" + touchDb.getTitle() + "] for user [" + nname.getCanonical() + "] server:["
					+ touchDb.getServer() + "]");
		} else {
			if ( !acle.getRoles().contains("[HSTouch]") ) {
				acle = acl.getEntry("*" + userOrg);
				if ( acle == null ) {
					jw.addProperty("error_" + index, "No entry found in database:[" + touchDb.getTitle() + "] for user [" + nname.getCanonical() + "] server:["
							+ touchDb.getServer() + "]");
					return false;
				}
			}
			if ( !acle.getRoles().contains("[HSTouch]") ) {
				jw.addProperty("error_" + index, "ACL Entry in database:[" + touchDb.getTitle() + "] for [" + acle.getName()
						+ "] does not contain the [HSTouch] role.");
			} else {
				touchNum += 1;
				if ( touchNum == 0 ) this.dbpath = touchDb.getFilePath();

				// this.sysurls[touchNum] = this.buildBaseUrl(protocols[index], hosts[index], touchDb.getFilePath());
				this.sysurls[touchNum] = this.buildBaseUrl(protocols[index], hosts[index], filenames[index]);
				this.sysnames[touchNum] = touchDb.getTitle();
				result = true;
			}
		}

		return result;
	}

	private ACLEntry findUserInHsTouchGroup(ACL acl, Name nname2, HS_JsonWriter jw, int index) throws IOException {
		List<String> groups = new ArrayList<String>();
		ACLEntry acle = acl.getFirstEntry();
		while (acle != null) {
			if ( acle.isGroup() && acle.getRoles().contains("[HSTouch]") && !(acle.getName().startsWith("*/")) ) groups.add(acle.getName());
			acle = acl.getNextEntry(acle);
		}
		jw.addProperty("hstgroups_" + index, groups);
		if ( groups.size() == 0 ) return null;

		try {
			Database nab = HS_Util.getSessionAsSigner().getDatabase("names.nsf");
			View usersView = nab.getView("($groups)");
			Document groupDoc = null;
			Vector<Object> members = null;
			for (String grp : groups) {
				groupDoc = usersView.getFirstDocumentByKey(grp, true);
				if ( groupDoc != null ) {
					members = groupDoc.getItemValue("members");
					if ( members.contains(nname2.getCanonical()) ) {
						jw.addProperty("hstgroup_found_" + index, "found the group: " + grp);
						acle = acl.getEntry(grp);
						return acle;
					} else {
						jw.addProperty("hstgroup_member_notfound", grp);
					}
				} else {
					jw.addProperty("hstgroup_document_notfound", grp);
				}
			}
		} catch (Exception e) {
			jw.addProperty("hstgroups_error_" + index, e.toString());
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private void initStuff() {

		Session sess = Factory.getSession();
		fullName = sess.getEffectiveUserName();
		nname = sess.createName(fullName);
		user = nname.getCommon();
		abbName = nname.getAbbreviated();

		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		Enumeration headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String hkey = (String) headerNames.nextElement();
			String hvalue = request.getHeader(hkey);
			if ( "host".equalsIgnoreCase(hkey) ) host = hvalue;
		}

		dbpath = sess.getCurrentDatabase().getFilePath();
		// server = sess.getServerName();
		buildBaseUrl();

		// Standard HSTouch login | Standard
		// No HSTouch login access | None
		// Master HSTouch Login Database | MasterLogin
		loginType = HS_Util.getAppSettingString("HSTouchLoginType");
		if ( "".equals(loginType) ) loginType = "Standard";

		localDebug = ("Yes".equalsIgnoreCase(HS_Util.getAppSettingString("debug_login_service")));

		sysurls = new String[maxresults];
		sysnames = new String[maxresults];
		filenames = new String[maxresults];
		hosts = new String[maxresults];
		protocols = new String[maxresults];

		for (int idx = 0; idx < maxresults; idx++) {
			this.sysurls[idx] = "";
			this.sysnames[idx] = "";
			this.filenames[idx] = "";
			this.hosts[idx] = "";
			this.protocols[idx] = "";
		}
	}

	private void buildBaseUrl() {
		baseUrl = new StringBuilder("http://" + host + "/" + dbpath);
		String bslash = "\\";
		String fslash = "/";
		int idx = baseUrl.indexOf(bslash);
		while (idx > -1) {
			baseUrl.replace(idx, idx + bslash.length(), fslash);
			idx += fslash.length();
			idx = baseUrl.indexOf(bslash, idx);
		}
	}

	private String buildBaseUrl(String protocol, String whost, String filePath) {
		StringBuilder result = new StringBuilder(protocol + "://" + whost + "/" + filePath);
		String bslash = "\\";
		String fslash = "/";
		int idx = result.indexOf(bslash);
		while (idx > -1) {
			result.replace(idx, idx + bslash.length(), fslash);
			idx += fslash.length();
			idx = result.indexOf(bslash, idx);
		}
		return result.toString();
	}

	private boolean isDebug() {

		return this.localDebug;
	}
}
