package com.healthspace.rest;

import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.Map.Entry;

import org.openntf.domino.Database;
import org.openntf.domino.DateTime;
import org.openntf.domino.Session;
import org.openntf.domino.View;
import org.openntf.domino.ViewEntry;
import org.openntf.domino.ViewNavigator;

import com.healthspace.general.HS_Util;
import com.healthspace.general.HS_database;
import com.healthspace.tools.JSFUtil;

public class QueryCalendar {
	private Long unixStart;
	private Long unixEnd;
	private String eho;

	public QueryCalendar() {
		final boolean localDebug = false;
		final String msgContext = "QueryCalendar.constructor";
		try {
			String startParam = JSFUtil.getParamValue("start");
			String endParam = JSFUtil.getParamValue("end");
			if ( startParam != null && !startParam.isEmpty() ) {
				setUnixStart(Long.parseLong(startParam));
			}
			if ( endParam != null && !endParam.isEmpty() ) {
				setUnixEnd(Long.parseLong(endParam));
			}
			if ( localDebug )
				System.out.print(msgContext + "startParam:[" + startParam + "] " + "unixStart:[" + unixStart + "] " + "endParam:[" + endParam + "] "
						+ "unixEnd:[" + unixEnd + "] ");
		} catch (Exception e) {
			System.out.print(msgContext + " ERROR: " + e.toString());
			if ( "Yes".equals(HS_Util.getAppSettingString("debug_View")) ) e.printStackTrace();
		}
	}

	public String doQueryFor() {
		return doQueryFor("-All-");
	}

	public String doQueryFor(String inpeho) {
		final boolean localDebug = false;
		final String msgContext = "QueryCalendar.doQueryFor";

		StringWriter sw = new StringWriter();
		HS_JsonWriter jw = new HS_JsonWriter(sw, false);
		try {
			this.setEho(inpeho);
			if ( localDebug ) System.out.print(msgContext + " starting eho:[" + inpeho + "]");
			jw.startArray();
			writeEntries(jw);
			jw.endArray();
			if ( localDebug ) System.out.print(msgContext + " - all done");
		} catch (IOException e) {
			System.out.print(msgContext + "- ERROR: " + e.toString());
			if ( "Yes".equals(HS_Util.getAppSettingString("debug_View")) ) e.printStackTrace();
		}
		return sw.toString();
	}

	private void writeEntries(HS_JsonWriter jw) {
		final boolean localDebug = false;
		final String msgContext = "QueryCalendar.writeEntries";
		try {
			List<Map<String, String>> entryList = getEntryList();
			if ( localDebug ) System.out.print(msgContext + " - got [" + entryList.size() + "] entries.");
			for (Map<String, String> valMap : entryList) {
				jw.startArrayItem();
				jw.startObject();
				for (Entry<String, String> valEntry : valMap.entrySet()) {
					// jw.startProperty(valEntry.getKey());
					// jw.outStringLiteral(valEntry.getValue());
					// jw.endProperty();
					jw.addProperty(valEntry.getKey(), valEntry.getValue());
				}
				jw.endObject();
				jw.endArrayItem();
			}
		} catch (IOException e) {
			System.out.print(msgContext + " - ERROR: " + e.toString());
			if ( "Yes".equals(HS_Util.getAppSettingString("debug_View")) ) e.printStackTrace();
		}
	}

	@SuppressWarnings("unused")
	private Vector<String> getUseCols() {
		final Vector<String> useCols = new Vector<String>();
		// if ("ei".equals(this.format)) {
		// useCols.add("FacilityID");
		// useCols.add("FacilityType");
		// useCols.add("Address");
		// useCols.add("City");
		// useCols.add("PermitNumber");
		// useCols.add("Province");
		// useCols.add("PostalCode");
		// useCols.add("PhoneDay");
		// } else if ("ig".equals(this.format)) {
		useCols.add("FacilityID~establishmentNum");
		useCols.add("SubType~establishmentType");
		useCols.add("Address~address~1");
		useCols.add("City~address~2");
		useCols.add("PermitNumber~permitNum");
		useCols.add("Province~address~3");
		useCols.add("PostalCode~address~4");
		useCols.add("PhoneDay~phoneNumber");
		useCols.add("Risk~risk");
		useCols.add("NextInspection~reinspectionDate");
		// }
		return useCols;
	}

	@SuppressWarnings("unchecked")
	private List<Map<String, String>> getEntryList() {
		final boolean localDebug = false;
		final String msgContext = "QueryCalendar.getEntryList";
		List<Map<String, String>> entryList = new ArrayList<Map<String, String>>();
		try {
			// Session sess = NotesContext.getCurrent().getCurrentSession();
			Session sess = HS_Util.getSession();
			HS_database hsdb = new HS_database();
			Database eidb = hsdb.getdb("eiRoot");
			// View entryView =
			// ExtLibUtil.getCurrentDatabase().getView("Events");
			View entryView = eidb.getView("XpagesCalendar");
			if ( entryView == null ) {
				System.out.print(msgContext + " - XpagesCalendar view NOT OPENNED in eidb:[" + eidb.getServer() + "!!" + eidb.getFilePath() + "]");
				return entryList;
			} else {
				if ( localDebug )
					System.out.print(msgContext + " - XpagesCalendar view opened in eidb:[" + eidb.getServer() + "!!" + eidb.getFilePath() + "]");
			}
			entryView.setAutoUpdate(false);

			ViewNavigator viewNav = entryView.createViewNav();
			Date startDate = null;
			DateTime startDateTime = sess.createDateTime("Today");
			if ( getUnixStart() != null ) {
				startDate = new Date(getUnixStart() * 1000);
				startDateTime = sess.createDateTime(startDate);
			}
			Date endDate = null;
			DateTime endDateTime = sess.createDateTime("Today");
			if ( getUnixEnd() != null ) {
				endDate = new Date(getUnixEnd() * 1000);
				endDateTime = sess.createDateTime(endDate);
				endDateTime.adjustDay(1);
			}
			if ( localDebug )
				System.out.print(msgContext + " - startDateTime:[" + startDateTime.getDateOnly() + "] endDateTime:[" + endDateTime.getDateOnly() + "]");
			ViewEntry startEnt = getStartEntry(entryView, startDateTime);
			ViewEntry endEnt = null;
			Vector colValues = null;
			if ( startEnt == null ) {
				startEnt = viewNav.getFirst();
				if ( startEnt != null ) {
					startDateTime = sess.createDateTime(((DateTime) startEnt.getColumnValues().get(0)).toJavaDate());
				}
				endEnt = viewNav.getLast();
				endDateTime = sess.createDateTime(((DateTime) endEnt.getColumnValues().get(0)).toJavaDate());
			}

			ViewEntry viewEnt = startEnt;
			DateTime entStartDateTime = null;
			DateTime entEndDateTime = null;
			Object esdt = null;
			String esdtClass = "";
			Object eedt = null;
			String eedtClass = "";
			String title = "";
			while (viewEnt != null) {
				colValues = viewEnt.getColumnValues();
				title = colValues.get(4).toString();
				try {
					// newbs - 04Sep14 - Fixed to work with domino.DateTime or
					// java.Util.date
					esdt = colValues.get(0);
					esdtClass = esdt.getClass().getName();
					eedt = colValues.get(1);
					eedtClass = eedt.getClass().getName();

					if ( (esdtClass.indexOf("domino.DateTime") > -1) || (esdtClass.indexOf("domino.impl.DateTime") > -1) ) {
						entStartDateTime = sess.createDateTime(((DateTime) esdt).toJavaDate());
						if ( localDebug && entryList.size() < 2 )
							System.out.print(msgContext + " - got entStartDateTime from esdt as " + esdtClass + ":[" + entStartDateTime.getDateOnly() + "]");
					} else if ( esdtClass.indexOf("util.Date") > -1 ) {
						entStartDateTime = sess.createDateTime((Date) esdt);
						if ( localDebug && entryList.size() < 2 )
							System.out.print(msgContext + " - got entStartDateTime from esdt as " + esdtClass + ":[" + entStartDateTime.getDateOnly() + "]");
					} else {
						entStartDateTime = sess.createDateTime("Today");
						entStartDateTime.adjustYear(50); // put it out of
						// time...
						if ( localDebug && entryList.size() < 2 )
							System.out.print(msgContext + " - esdt is not a recognized type:[ " + esdtClass + "] entStartDateTime:["
									+ entStartDateTime.getDateOnly() + "]");
					}
					if ( (eedtClass.indexOf("domino.DateTime") > -1) || (eedtClass.indexOf("domino.impl.DateTime") > -1) ) {
						entEndDateTime = sess.createDateTime(((DateTime) eedt).toJavaDate());
					} else if ( eedtClass.indexOf("util.Date") > -1 ) {
						entEndDateTime = sess.createDateTime(((Date) eedt));
					} else {
						entEndDateTime = sess.createDateTime("Today");
						entEndDateTime.adjustYear(50); // put it out of time...
						if ( localDebug && entryList.size() < 2 )
							System.out.print(msgContext + " - eedt is not a recognized type:[ " + eedtClass + "] entEndDateTime:["
									+ entEndDateTime.getDateOnly() + "]");
					}
				} catch (Exception e) {
					System.out.print(msgContext + " - WARNING: eedt:[" + eedt.toString() + "] [" + eedt.getClass().getName() + "]");
					System.out.print(msgContext + " - WARNING: esdt:[" + esdt.toString() + "] [" + esdt.getClass().getName() + "]");
					System.out.print(msgContext + " - ERROR: On entry:[" + title + "] got error:" + e.toString());
					entStartDateTime = sess.createDateTime("Today");
					entEndDateTime = sess.createDateTime("Today");
					entStartDateTime.adjustYear(50); // put it out of time...
					entEndDateTime.adjustYear(50); // put it out of time...
				}
				if ( entStartDateTime.toJavaDate().getTime() < endDateTime.toJavaDate().getTime() ) {
					if ( isEho(colValues.get(3).toString()) ) {
						Map<String, String> valueMap = new HashMap<String, String>();
						// if
						// (!colValues.get(2).toString().equals("Some Random Event"))
						// {
						// valueMap.put("color", "red");
						// }
						valueMap.put("start", getFormattedDate(entStartDateTime.toJavaDate()));
						valueMap.put("end", getFormattedDate(entEndDateTime.toJavaDate()));
						valueMap.put("address", "INCOMPLETE");
						valueMap.put("eho", colValues.get(3).toString());
						valueMap.put("establishmentNum", "INCOMPLETE");
						valueMap.put("establishmentType", "INCOMPLETE");
						valueMap.put("name", "INCOMPLETE");
						valueMap.put("permitNum", "INCOMPLETE");
						valueMap.put("phoneNumber", "INCOMPLETE");
						valueMap.put("reinspectionDate", "INCOMPLETE");
						valueMap.put("unid", colValues.get(7).toString());

						valueMap.put("title", title);
						//
						// valueMap.put("color", colValues.get(2).toString());
						// valueMap.put("eho", colValues.get(3).toString());
						// valueMap.put("facility",
						// colValues.get(4).toString());
						// valueMap.put("reinspection",
						// colValues.get(5).toString());
						// valueMap.put("lastinspection",
						// colValues.get(6).toString());
						// valueMap.put("unid", colValues.get(7).toString());
						// valueMap.put("url", JSFUtil.getDbUrl() +
						// "/formFacility.xsp?documentId=" +
						// colValues.get(7).toString());

						entryList.add(valueMap);
					}
					viewEnt = viewNav.getNext(viewEnt);
				} else {
					viewEnt = null;
				}
			}
		} catch (Exception e) {
			System.out.print(msgContext + " - ERROR: " + e.toString());
			if ( "Yes".equals(HS_Util.getAppSettingString("debug_View")) ) e.printStackTrace();
		}
		return entryList;
	}

	private boolean isEho(String entEho) {
		final boolean localDebug = false;
		final String msgContext = "QueryCalendar.getEntryList";
		if ( localDebug ) System.out.print(msgContext + " - entEho:[" + entEho + "] this.getEho():[" + this.getEho() + "]");
		if ( "".equals(this.getEho()) ) return true;
		if ( "-All-".equals(this.getEho()) ) return true;
		if ( entEho.equals(this.getEho()) ) return true;
		return false;
	}

	private String getFormattedDate(Date formatDate) {
		String returnDate = "";
		if ( formatDate != null ) {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			returnDate = formatter.format(formatDate);
		}
		return returnDate;
	}

	@SuppressWarnings("deprecation")
	private ViewEntry getStartEntry(View entryView, DateTime startDateTime) {
		ViewEntry ent = entryView.getEntryByKey(startDateTime);
		if ( ent == null ) {
			while (ent == null) {
				startDateTime.adjustDay(1);
				ent = entryView.getEntryByKey(startDateTime);
			}
		}
		return ent;
	}

	public Long getUnixStart() {
		return unixStart;
	}

	public void setUnixStart(Long unixStart) {
		this.unixStart = unixStart;
	}

	public Long getUnixEnd() {
		return unixEnd;
	}

	public void setUnixEnd(Long unixEnd) {
		this.unixEnd = unixEnd;
	}

	public String getEho() {
		return eho;
	}

	public void setEho(String eho) {
		this.eho = eho;
	}
}
