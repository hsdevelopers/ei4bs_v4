package com.healthspace.rest;

import java.io.IOException;
import java.util.Map;

import com.healthspace.general.Inspection;

public class PostTemperatures {

	public PostTemperatures() {
	}

	public static String processEquipmentTemps(HS_JsonWriter jw, Map<String, Object> tempmap, Inspection inspection) throws IOException {
		final String msgContext = "PostTemperatures.processEquipmentTemps";
		// {"tabIDEquip_TempsID":"88cf2c6f-16f6-4056-be52-b94f98aa324d"
		// "inspectionID":"30376d6b-6528-4615-bf3c-59e1a137851c"
		// "equipDescrip":"Equipment 1"
		// "equipTemp":"45"
		// "synced":"0"
		// "modifiedBy":"Henry Newberry"
		// "dateadd":"2015-1-9 9:29:40.668"
		// "datelastmodified":"2015-1-9 9:29:40.668"}

		// TDA FORMAT
		// {"tabIDEquipment_TemperaturesID":"afcf4825-2bfc-4dd2-8a64-b0b90ee753b0"
		// "inspectionID":"a4d77f4c-eaa9-4f50-840f-ecbc17b13955"
		// "description":"Freezer"
		// "temperature":"4"
		// "synced":"0"
		// "modifiedBy":"Henry Newberry"
		// "dateadd":"2016-5-17 10:24:24.195"
		// "datelastmodified":"2016-5-17 11:57:06.42"
		// "finalized":0}

		try {
			String equipDescrip = "-not in data-";
			String equipTemp = "-not in data-";

			if ( tempmap.containsKey("record_equipdescrip") ) {
				equipDescrip = (String) tempmap.get("record_equipdescrip");
				equipTemp = (String) tempmap.get("record_equiptemp");
			} else if ( tempmap.containsKey("record_description") ) {
				equipDescrip = (String) tempmap.get("record_description");
				equipTemp = (String) tempmap.get("record_temperature");
			}

			jw.addProperty("equipDescrip", equipDescrip);
			jw.addProperty("equipTemp", equipTemp);

			inspection.setEquipDescrip(RestUtil.addListItem(inspection.getEquipDescrip(), equipDescrip));
			inspection.setEquipTemp(RestUtil.addListItem(inspection.getEquipTemp(), equipTemp));
			return "";
		} catch (Exception e) {
			jw.addProperty("error", e.toString());
			jw.addProperty("errorRoutine", msgContext);
			return e.toString();
		}
	}

	public static String processFoodTemps(HS_JsonWriter jw, Map<String, Object> tempmap, Inspection inspection) throws IOException {
		final String msgContext = "PostTemperatures.processFoodTemps";
		// {"temperatureID":"a127b444-914f-49f6-9ec2-bdabf32d868b"
		// "inspectionID":"30376d6b-6528-4615-bf3c-59e1a137851c"
		// "foodDescrip":"Ambrosia Fruit Salad"
		// "foodProcess":"Preparation"
		// "foodState":"Deep Fryer"
		// "foodTemp":"78"
		// "deleted":"0"
		// "synced":"0"
		// "modifiedBy":"Henry Newberry"
		// "dateadd":"2015-1-9 9:30:22.329"
		// "datelastmodified":"2015-1-9 9:30:22.329"}

		// TDA FORMAT:
		// {"tabIDFood_TemperaturesID":"ec2a9e91-5dc2-4e41-bfcf-6f74303cad12"
		// "inspectionID":"a4d77f4c-eaa9-4f50-840f-ecbc17b13955"
		// "Description":"Salad"
		// "temperature":"43"
		// "stateFood":"Cold Holding"
		// "synced":"0"
		// "modifiedBy":"Henry Newberry"
		// "dateadd":"2016-5-17 10:25:04.558"
		// "datelastmodified":"2016-5-17 11:57:06.42"
		// "finalized":0}

		// NW Michigan HD Format...
		// {"inspectionID":"a72c0f1f-f211-4c7d-991b-dda05fd44d83"
		// "tabIDFood_TemperaturesID":"61d81b5c-ab8d-4a68-88e5-041889ee8fad"
		// "Description":"Foodone"
		// "temperature":"1"
		// "FoodState":"Storage"
		// "datelastmodified":"2017-11-10 13:18:58.838"
		// "modifiedBy":""
		// "dateadd":"2017-11-10 13:18:58.838"
		// "synced":0
		// "finalized":0}

		try {
			String foodDescrip = "-not in data-";
			String foodTemp = "-not in data-";
			String foodState = "-not in data-";

			if ( tempmap.containsKey("record_fooddescrip") ) {
				foodDescrip = (String) tempmap.get("record_fooddescrip");
				foodTemp = (String) tempmap.get("record_foodtemp");
				foodState = (String) tempmap.get("record_foodprocess");
			} else if ( tempmap.containsKey("record_description") ) {
				foodDescrip = (String) tempmap.get("record_description");
				foodTemp = (String) tempmap.get("record_temperature");
				foodState = (tempmap.containsKey("record_statefood")) ? (String) tempmap.get("record_statefood") : (tempmap.containsKey("record_foodstate")) ? (String) tempmap.get("record_foodstate")
										: "-not mapped in data-";
			}

			jw.addProperty("foodDescrip", foodDescrip);
			jw.addProperty("foodTemp", foodTemp);
			jw.addProperty("foodState", foodState);

			inspection.setFoodDescrip(RestUtil.addListItem(inspection.getFoodDescrip(), foodDescrip));
			inspection.setFoodTemp(RestUtil.addListItem(inspection.getFoodTemp(), foodTemp));
			inspection.setFoodState(RestUtil.addListItem(inspection.getFoodState(), foodState));
			return "";
		} catch (Exception e) {
			jw.addProperty("error", e.toString());
			jw.addProperty("errorRoutine", msgContext);
			return e.toString();
		}
	}

	public static String processWareWashingTemps(HS_JsonWriter jw, Map<String, Object> tempmap, Inspection inspection) throws IOException {
		final String msgContext = "PostTemperatures.processWareWashingTemps";
		// {"tabIDWare_Washing_TempsID":"88d14cf2-480f-4f83-871b-0106533d9cc7"
		// "inspectionID":"e20f7f85-53ac-41d3-a51c-3a71083486bd"
		// "machineName":"Machine1"
		// "saniMethod":"Sani meth1"
		// "thermoLabel":"Tl1"
		// "ppm":"99"
		// "saniName":"Sank name1"
		// "saniType":"sani type1"
		// "wareWashTemp":"235"
		// "synced":"0"
		// "modifiedBy":"Henry Newberry"
		// "dateadd":"2015-3-9 15:28:25.8"
		// "datelastmodified":"2015-3-9 15:28:25.8"}

		// TDA Syntax??
		// {"tabIDWarewashing_InfoID":"032024fa-6ddc-4507-95cf-cb8636a6385e"
		// "inspectionID":"a4d77f4c-eaa9-4f50-840f-ecbc17b13955"
		// "machineName":"Maytag"
		// "sanitizationMethod":"Hot water"
		// "thermoLabel":"123456"
		// "ppm":"2"
		// "sanitizerName":"Hot"
		// "sanitizerType":"Water"
		// "temperature":"170"
		// "synced":"0"
		// "modifiedBy":"Henry Newberry"
		// "dateadd":"2016-5-17 10:23:53.702"
		// "datelastmodified":"2016-5-17 11:57:06.41"
		// "finalized":0}
		try {
			String machineName = (String) tempmap.get("record_machinename");
			String saniMethod = "-not in data-";
			String thermoLabel = (String) tempmap.get("record_thermolabel");
			String ppm = (String) tempmap.get("record_ppm");
			String saniName = "-not in data-";
			String saniType = "-not in data-";
			String wareWashTemp = "-not in data-";

			if ( (tempmap.containsKey("record_sanimethod")) ) {
				saniMethod = (String) tempmap.get("record_sanimethod");
				saniName = (String) tempmap.get("record_saniname");
				saniType = (String) tempmap.get("record_sanitype");
				wareWashTemp = (String) tempmap.get("record_warewashtemp");
			} else if ( (tempmap.containsKey("record_sanitizationmethod")) ) {
				saniMethod = (String) tempmap.get("record_sanitizationmethod");
				saniName = (String) tempmap.get("record_sanitizername");
				saniType = (String) tempmap.get("record_sanitizertype");
				wareWashTemp = (String) tempmap.get("record_temperature");
			}

			jw.addProperty("machineName", machineName);
			jw.addProperty("saniMethod", saniMethod);
			jw.addProperty("thermoLabel", thermoLabel);
			jw.addProperty("ppm", ppm);
			jw.addProperty("saniName", saniName);
			jw.addProperty("saniType", saniType);
			jw.addProperty("wareWashTemp", wareWashTemp);
			// jw.addProperty("xxx", xxx);

			inspection.setMachineName(RestUtil.addListItem(inspection.getMachineName(), machineName));
			inspection.setSaniMethod(RestUtil.addListItem(inspection.getSaniMethod(), saniMethod));
			inspection.setThermoLabel(RestUtil.addListItem(inspection.getThermoLabel(), thermoLabel));
			inspection.setPpm(RestUtil.addListItem(inspection.getPpm(), ppm));
			inspection.setSaniName(RestUtil.addListItem(inspection.getSaniName(), saniName));
			inspection.setSaniType(RestUtil.addListItem(inspection.getSaniType(), saniType));
			inspection.setWareWashTemp(RestUtil.addListItem(inspection.getWareWashTemp(), wareWashTemp));
			return "";
		} catch (Exception e) {
			jw.addProperty("error", e.toString());
			jw.addProperty("errorRoutine", msgContext);
			return e.toString();
		}
	}
}
