package com.healthspace.rest;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.Map.Entry;

import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.View;
import org.openntf.domino.ViewEntry;
import org.openntf.domino.ViewNavigator;

import com.healthspace.general.AppModule;
import com.healthspace.general.AppModules;
import com.healthspace.general.HS_Util;
import com.healthspace.general.HS_database;
import com.healthspace.tools.JSFUtil;
import com.ibm.domino.services.util.JsonWriter;

public class QueryServices {
	// private String eho;
	private String	factype;
	private String	query;
	private int		maxResults;

	private String	format;
	// private String unid;

	private String	source		= "";
	private String	logmsgs		= "";

	private String	viewName	= "XPagesFacilitiesByType";

	public QueryServices() {
		// final boolean localDebug = false;
		// final String msgContext = "QueryServices.constructor";

	}

	public String queryFacilities() {
		return this.queryFacilities("ei");
	}

	// valid formats are "ei" and "ig"
	public String queryFacilities(String iformat) {
		final String msgContext = "QueryServices.queryFacilities";
		// final boolean localDebug = ("ig".equals(iformat));
		// final boolean localDebug = true;
		final boolean localDebug = ("ig".equals(iformat)) ? ("Yes".equals(HS_Util.getAppSettingString("debug_HSTSearch"))) : ("Yes".equals(HS_Util.getAppSettingString("debug_Search")));
		boolean forceLog = "Yes".equals(HS_Util.getAppSettingString("debug_Search")) || (("ig".equals(iformat)) && ("Yes".equals(HS_Util.getAppSettingString("debug_HSTSearch"))));
		// boolean forceLog = true;

		this.format = iformat;
		logmsgs = "\nformat:" + iformat;
		logmsgs += "\nAppSettingString.debug_HSTSearch :[" + HS_Util.getAppSettingString("debug_HSTSearch") + "] localDebug:[" + localDebug + "]";
		logmsgs += "\nUser is:[" + HS_Util.getSession().getEffectiveUserName() + "]";

		try {
			logmsgs += "\nURL from JSFUtil: " + JSFUtil.getUrl();
			// var query:string = context.getUrlParameter("query");
			String factypeParam = JSFUtil.getParamValue("FacType");
			if ( localDebug ) logmsgs += "\n FacTypeParm:[" + factypeParam + "]";

			this.factype = ("".equals(factypeParam)) ? "-All" : factypeParam;
			if ( localDebug ) logmsgs += "\n FacType:[" + this.factype + "]";

			if ( "-All".equals(this.factype) ) {
				factypeParam = JSFUtil.getParamValue("module");
				if ( !("".equals(factypeParam)) ) {
					this.factype = factypeParam;
				}
			}

			final String queryParam = JSFUtil.getParamValue("query");
			if ( localDebug ) logmsgs += "\nqueryParm:[" + queryParam + "]";

			this.query = ("".equals(queryParam)) ? "" : queryParam;

			// Added to prevent logging from XPages UI queries unless there is
			// an error
			final String sourceParam = JSFUtil.getParamValue("source");
			this.source = ("".equals(sourceParam)) ? "unknown" : sourceParam;
			if ( localDebug ) logmsgs += "\nsourceParm:[" + sourceParam + "]";

			if ( "util_searchEstablishments.cc".equals(this.source) ) forceLog = false;
			// if ("test_restQuery.xpage".equals(this.source)) forceLog = false;

			final String maxParam = JSFUtil.getParamValue("maxcount");
			if ( localDebug ) logmsgs += "\nmaxParm:[" + maxParam + "]";
			this.maxResults = ("".equals(maxParam)) ? 19 : Integer.parseInt(maxParam);
			if ( localDebug ) logmsgs += "\n maxResults:[" + this.maxResults + "]";

			final String viewParam = JSFUtil.getParamValue("viewname");
			if ( localDebug ) logmsgs += "\nviewParm:[" + viewParam + "]";
			if ( !"".equals(viewParam) ) this.viewName = viewParam;
			if ( localDebug ) logmsgs += "\nviewName:[" + this.viewName + "]";

			// IF the user is not identified and the query
			if ( localDebug ) {
				logmsgs += "\nquery:[" + this.query + "] factype:[" + this.factype + "] maxResults:[" + this.maxResults + "]";
				HS_Util.debug("viewName:[" + this.viewName + "] query:[" + this.query + "] factype:[" + this.factype + "] maxResults:[" + this.maxResults + "]", "debug", msgContext);
			}
		} catch (final Exception e) {
			logmsgs += "\nERROR: " + e.toString();
			forceLog = true;
			if ( "Yes".equals(HS_Util.getAppSettingString("debug_Search")) ) e.printStackTrace();
		}
		final StringWriter sw = new StringWriter();
		final JsonWriter jw = new JsonWriter(sw, false);
		try {
			jw.startArray();
			writeEntries(jw);
			jw.endArray();
		} catch (final IOException e) {
			logmsgs += "\n ERROR: " + e.toString();
			HS_Util.debug("ERROR: " + e.toString(), "error", msgContext);
			forceLog = true;
			if ( "Yes".equals(HS_Util.getAppSettingString("debug_Search")) ) e.printStackTrace();
		}
		if ( forceLog ) {
			RestUtil.writeToAgentLog(logmsgs + "\n\n" + RestUtil.getDebugProperties() + "\n\n" + sw.toString(), msgContext);
		}
		return sw.toString();
	}

	private void writeEntries(JsonWriter jw) {
		final boolean localDebug = ("Yes".equalsIgnoreCase(HS_Util.getAppSettingString("debug_Search")));
		final String msgContext = "QueryServices.writeEntries";
		final String useFT = HS_Util.getAppSettingString("FacilitySearchFTIndex");
		try {
			List<Map<String, String>> entryList = null;
			if ( "Anonymous".equals(HS_Util.getSession().getEffectiveUserName()) && "unknown".equals(this.source) ) {
				entryList = new ArrayList<Map<String, String>>();
				logmsgs += "\nInvalid access attempt - please go away!";
				final Map<String, String> errorMap = new HashMap<String, String>();
				errorMap.put("Error", "Invalid access attempt - please go away!");
				errorMap.put("query", "ignored");
				errorMap.put("facilityType", "none");
				errorMap.put("maxResults", "0");
				entryList.add(errorMap);
			} else if ( "Yes".equals(useFT) || "NameOnly".equals(useFT) ) {
				entryList = getEntryListFT(useFT);
				if ( entryList.size() == 0 ) {
					logmsgs += "\n FT got search found nothing using first character search";
					if ( localDebug ) HS_Util.debug("FT got search found nothing using first character search", "debug", msgContext);
					entryList = getEntryList();
				}
			} else {
				if ( localDebug ) HS_Util.debug("Doing only first character search", "debug", msgContext);
				entryList = getEntryList();
			}
			if ( entryList.size() > this.maxResults ) {
				logmsgs += "\n got [" + entryList.size() + "] entries - exceeds maxCount!.";
				if ( localDebug ) HS_Util.debug("got [" + entryList.size() + "] entries- - exceeds maxCount!", "debug", msgContext);

			} else {
				logmsgs += "\n got [" + entryList.size() + "] entries.";
				if ( localDebug ) HS_Util.debug("got [" + entryList.size() + "] entries.", "debug", msgContext);
			}

			// Sorting
			Vector<String> advOptions = HS_Util.getAppSettingsValues("AdvancedSearchOption");
			if ( !advOptions.contains("nosort") ) {
				Collections.sort(entryList, new Comparator<Map<String, String>>() {

					public int compare(Map<String, String> arg0, Map<String, String> arg1) {
						String fname0 = arg0.get("name");
						String fname1 = arg1.get("name");
						return fname0.compareTo(fname1);
						// return 0;
					}

				});
			}
			// TODO Only optput the number of entries in the MAXCOUNT.
			if ( (!advOptions.contains("returnmore")) && (entryList.size() > this.maxResults) ) {
				entryList = entryList.subList(0, this.maxResults);
			}

			for (final Map<String, String> valMap : entryList) {
				jw.startArrayItem();
				jw.startObject();
				for (final Entry<String, String> valEntry : valMap.entrySet()) {
					jw.startProperty(valEntry.getKey());
					jw.outStringLiteral(valEntry.getValue());
					jw.endProperty();
				}
				jw.endObject();
				jw.endArrayItem();
			}
		} catch (final IOException e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if ( HS_Util.isDebugServer() ) {
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("unchecked")
	private List<Map<String, String>> getEntryList() {
		final boolean localDebug = ("Yes".equalsIgnoreCase(HS_Util.getAppSettingString("debug_Search")));
		final String msgContext = "QueryServices.getEntryList";

		final String fslimit = HS_Util.getAppSettingString("FacilitySearchLimits");
		logmsgs += "\n In getEntryList - FacilitySearchLimits:[" + fslimit + "]";
		if ( localDebug ) HS_Util.debug("FacilitySearchLimits:[" + fslimit + "]", "debug", msgContext);
		Vector<String> enabledFtypes = new Vector<String>();
		if ( "enabled".equals(fslimit) ) {
			// enabledFtypes.add("Food Facilities");
			enabledFtypes = getEnabledFacilityTypes();
		}

		final List<Map<String, String>> entryList = new ArrayList<Map<String, String>>();
		try {
			// Session sess = NotesContext.getCurrent().getCurrentSession();
			// Session sess = HS_Util.getSession();
			final HS_database hsdb = new HS_database();
			final Database eidb = hsdb.getdb("eiRoot");
			// View entryView =
			// ExtLibUtil.getCurrentDatabase().getView("Events");
			if ( localDebug ) HS_Util.debug("Using view " + this.viewName + ".", "debug", msgContext);
			final View entryView = eidb.getView(this.viewName);
			entryView.setAutoUpdate(false);
			// ViewNavigator viewNav = entryView.createViewNav();
			final Vector<String> useCols = this.getUseCols();
			final Vector<String> cnames = entryView.getColumnNames();
			// final String fkey = factype + "-" +
			// getQuery().toUpperCase().charAt(0);
			final String fkey = "-All-" + getQuery().toUpperCase().charAt(0);
			logmsgs += "\n key is:[" + fkey + "]";
			if ( localDebug ) HS_Util.debug("search key is:[" + fkey + "]", "debug", msgContext);
			// View fnav:NotesViewNavigator =
			// fview.createViewNavFromCategory(fkey);
			final ViewNavigator viewNav = entryView.createViewNavFromCategory(fkey);

			// viewNav.setBufferMaxEntries(400);

			final ViewEntry startEnt = getStartEntry(viewNav, getQuery());
			boolean doThisEntry = false;
			if ( startEnt != null ) {
				ViewEntry viewEnt = startEnt;
				String facname = "";
				String ftype = "";
				while (viewEnt != null) {
					final Vector colValues = viewEnt.getColumnValues();
					facname = colValues.get(1).toString().toLowerCase();
					if ( facname.startsWith(getQuery().toLowerCase()) ) {
						doThisEntry = true;
						if ( "enabled".equals(fslimit) ) {
							ftype = colValues.get(3).toString();
							if ( !enabledFtypes.contains(ftype) ) {
								doThisEntry = false;
								if ( localDebug ) {
									HS_Util.debug("ftype:[" + ftype + "] being skipped.", "debug", msgContext);
								}
							} else if ( !("-All".equals(this.factype)) ) {
								String wrkFtype = AppModules.translateOnameToFType(this.factype);
								doThisEntry = (ftype.equals(wrkFtype));
								if ( localDebug && !doThisEntry ) {
									HS_Util.debug("ftype:[" + ftype + "] being skipped due no being to this.factype:[" + wrkFtype + "]", "debug", msgContext);
								}
							}
						}
						if ( doThisEntry ) {
							entryList.add(this.addEntry(cnames, viewEnt, useCols));
						}
						viewEnt = viewNav.getNext(viewEnt);
					} else {
						viewEnt = null;
					}
				}
			} else {
				if ( localDebug ) HS_Util.debug("ERROR: no start entry found!", "debug", msgContext);
				final Map<String, String> errorMap = new HashMap<String, String>();
				errorMap.put("Error", "no start entry found!");
				errorMap.put("query", this.getQuery());
				errorMap.put("facilityType", this.getFactype());
				errorMap.put("maxResults", "" + this.getMaxResults());
				entryList.add(errorMap);
			}
		} catch (final Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if ( HS_Util.isDebugServer() ) {
				e.printStackTrace();
			}
		}
		return entryList;
	}

	@SuppressWarnings("unchecked")
	private List<Map<String, String>> getEntryListFT(String useFT) {
		final boolean localDebug = ("Yes".equalsIgnoreCase(HS_Util.getAppSettingString("debug_Search")));
		final String msgContext = "QueryServices.getEntryListFT";

		final String fslimit = HS_Util.getAppSettingString("FacilitySearchLimits");
		logmsgs += "\n In " + msgContext + " - FacilitySearchLimits:[" + fslimit + "]";
		if ( localDebug ) HS_Util.debug("FacilitySearchLimits:[" + fslimit + "]", "debug", msgContext);
		Vector<String> enabledFtypes = new Vector<String>();

		if ( "enabled".equals(fslimit) ) {
			// enabledFtypes.add("Food Facilities");
			enabledFtypes = getEnabledFacilityTypes();
			logmsgs += "\nIn " + msgContext + " - enabledFtypes:" + enabledFtypes.toString() + ".";
		}
		String query = this.getQuery();
		if ( "NameOnly".equals(useFT) ) {
			// FIELD "form" CONTAINS "test"test"
			query = "FIELD \"Name\" CONTAINS ";
			query += "\"" + this.getQuery() + "\"";
		}

		final List<Map<String, String>> entryList = new ArrayList<Map<String, String>>();
		try {
			// Session sess = NotesContext.getCurrent().getCurrentSession();
			// Session sess = HS_Util.getSession();
			final HS_database hsdb = new HS_database();
			final Database eidb = hsdb.getdb("eiRoot");
			if ( !eidb.isFTIndexed() ) {
				logmsgs += "\n db " + eidb.getFilePath() + " is not FT indexed. returning old style";
				if ( localDebug ) HS_Util.debug("db is not FT indexed. returning old style", "debug", msgContext);
				return this.getEntryList();
			}

			// if ( localDebug ) HS_Util.debug("Using view " + this.viewName +
			// ".", "debug", msgContext);
			final View entryView = eidb.getView(this.viewName);
			entryView.setAutoUpdate(false);

			final Vector<String> useCols = this.getUseCols();

			final Vector<String> cnames = entryView.getColumnNames();

			// final int ftct = entryView.FTSearch(query, 500);
			final int ftct = entryView.FTSearch(query, 1000);
			String msg = "";

			logmsgs += "\nIn " + msgContext + " - FTSearch returned:[" + ftct + "] for:[" + query + "]";
			if ( localDebug ) HS_Util.debug("FTSearch returned:[" + ftct + "] for:[" + query + "]", "debug", msgContext);

			if ( ftct > 0 ) {

				boolean doThisEntry = false;
				Document viewDoc = entryView.getFirstDocument();
				String facname = "";
				String ftype = "";
				while (viewDoc != null) {
					final Vector colValues = viewDoc.getColumnValues();
					facname = colValues.get(1).toString().toLowerCase();
					doThisEntry = true;
					if ( "enabled".equals(fslimit) ) {
						ftype = colValues.get(3).toString();
						msg = "for facname:[" + facname + "] ftype is:[" + ftype + "] - ";
						if ( !enabledFtypes.contains(ftype) ) {
							doThisEntry = false;
							if ( localDebug ) HS_Util.debug(msg + "being skipped.", "debug", msgContext);
							// logmsgs += "\nIn " + msgContext + " - " + msg +
							// "being skipped.";
						} else if ( !("-All".equals(this.factype)) ) {
							String wrkFtype = AppModules.translateOnameToFType(this.factype);
							doThisEntry = (ftype.equals(wrkFtype));
							if ( localDebug && !doThisEntry ) HS_Util.debug(msg + "being skipped due no being to this.factype:[" + wrkFtype + "]", "debug", msgContext);
							// logmsgs += "\nIn " + msgContext + " - " + msg +
							// "doThisEntry:[" + doThisEntry + "]";
						}
					}
					if ( doThisEntry ) {
						entryList.add(this.addEntry(cnames, viewDoc, useCols));
					}
					viewDoc = entryView.getNextDocument(viewDoc);
				}
			} else {
				// final Map<String, String> errorMap = new HashMap<String,
				// String>();
				// errorMap.put("Error", "no entries found for query: '" + query
				// + "' in db:[" + eidb.getFilePath() + "]");
				// errorMap.put("query", query);
				// errorMap.put("facilityType", this.getFactype());
				// errorMap.put("maxResults", "" + this.getMaxResults());
				// entryList.add(errorMap);
				logmsgs += "\n" + msgContext + " got nothing.";
			}
			// REMOVED to allow secondary search on first characters.
			// if (entryList.size() == 0) {
			// final Map<String, String> errorMap = new HashMap<String,
			// String>();
			// errorMap.put("Error", "no entries found for " + query);
			// errorMap.put("query", query);
			// errorMap.put("facilityType", this.getFactype());
			// errorMap.put("maxResults", "" + this.getMaxResults());
			// entryList.add(errorMap);
			// }
		} catch (final Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if ( HS_Util.isDebugServer() ) {
				e.printStackTrace();
			}
		}
		return entryList;
	}

	private Vector<String> getUseCols() {
		final Vector<String> useCols = new Vector<String>();
		if ( "ei".equals(this.format) ) {
			useCols.add("FacilityID");
			useCols.add("FacilityType");
			useCols.add("Address");
			useCols.add("City");
			useCols.add("PermitNumber");
			useCols.add("Province");
			useCols.add("PostalCode");
			useCols.add("PhoneDay");
		} else if ( "ig".equals(this.format) ) {
			// useCols.add("FacilityID~establishmentNum");
			// useCols.add("SubType~establishmentType");
			// useCols.add("Address~address~1");
			// useCols.add("City~address~2");
			// useCols.add("PermitNumber~permitNum");
			// useCols.add("Province~address~3");
			// useCols.add("PostalCode~address~4");
			// useCols.add("PhoneDay~phoneNumber");
			// useCols.add("Risk~risk");
			// useCols.add("NextInspection~reinspectionDate");
			useCols.add("FacilityID~establishmentNum");
			useCols.add("SubType~establishmentType");
			useCols.add("Address~streetinfo");
			useCols.add("City~city");
			useCols.add("PermitNumber~permitNum");
			useCols.add("Province~state");
			useCols.add("PostalCode~zip");
			useCols.add("PhoneDay~phoneNumber");
			useCols.add("Risk~risk");
			useCols.add("NextInspection~reinspectionDate");
			useCols.add("LatDec~latitude");
			useCols.add("LongDec~longitude");
			useCols.add("Address~address");
		}
		return useCols;
	}

	@SuppressWarnings("unchecked")
	private Map<String, String> addEntry(Vector<String> cnames, ViewEntry viewEnt, Vector<String> useCols) {
		final Vector colValues = viewEnt.getColumnValues();
		final String unid = viewEnt.getUniversalID();
		return this.addEntry(cnames, colValues, useCols, unid);
	}

	@SuppressWarnings("unchecked")
	private Map<String, String> addEntry(Vector<String> cnames, Document viewDoc, Vector<String> useCols) {
		final Vector colValues = viewDoc.getColumnValues();
		final String unid = viewDoc.getUniversalID();
		return this.addEntry(cnames, colValues, useCols, unid);
	}

	@SuppressWarnings("unchecked")
	private Map<String, String> addEntry(Vector<String> cnames, Vector colValues, Vector<String> useCols, String unid) {
		// final boolean localDebug = HS_Util.isDebugServer();
		final boolean localDebug = true;
		final String msgContext = "QueryServices.addEntry";

		final Map<String, String> valueMap = new HashMap<String, String>();
		try {
			if ( "ei".equals(this.format) ) {
				valueMap.put("name", colValues.get(1).toString());
				for (int i = 2; i < cnames.size(); i++) {
					if ( useCols.contains(cnames.get(i)) ) {
						valueMap.put(cnames.get(i), colValues.get(i).toString());
					}
				}
				valueMap.put("unid", unid);
			} else if ( "ig".equals(this.format) ) {
				valueMap.put("name", colValues.get(1).toString());
				final Vector<String> uc0 = new Vector<String>();
				final Vector<String> uc1 = new Vector<String>();
				final Vector<String> uc2 = new Vector<String>();
				String[] parms = null;
				for (int v = 0; v < useCols.size(); v++) {
					parms = useCols.get(v).split("~");
					uc0.add(parms[0]);
					uc1.add(parms[1]);
					uc2.add((parms.length > 2) ? parms[2] : "-");
				}
				int x = 0;
				String address = "";
				String tag = "";
				for (int i = 2; i < cnames.size(); i++) {
					if ( uc0.contains(cnames.get(i)) ) {
						tag = "";
						x = uc0.indexOf(cnames.get(i));
						tag = uc1.get(x);
						// logmsgs += "\n procesing tag:[" + tag + "] for x:[" +
						// x + "] which maps to:[" + cnames.get(i) + "]";
						if ( "address".equals(tag) ) {
							address += ("".equals(address)) ? "" : ("4".equals(uc2.get(x))) ? " " : ", ";
							address += colValues.get(i).toString();
							// address += " [" + uc2.get(x) + "]";
							// logmsgs +=
							// "\n doing special address handling - address is:["
							// + address + "]";

						} else {
							valueMap.put(tag, colValues.get(i).toString());
						}

					}
				}

				// logmsgs += "\n after loop address is:[" + address + "]";

				if ( address.equals("") ) {
					address += (valueMap.containsKey("streetinfo")) ? valueMap.get("streetinfo") + ", " : "";
					address += (valueMap.containsKey("city")) ? valueMap.get("city") + ", " : "";
					address += (valueMap.containsKey("state")) ? valueMap.get("state") + " " : "";
					address += (valueMap.containsKey("zip")) ? valueMap.get("zip") : "";
				}
				valueMap.put("address", address);
				valueMap.put("unid", unid);

				for (int i = 0; i < uc0.size(); i++) {
					if ( !cnames.contains(uc0.get(i)) ) {
						if ( "LatDec".equalsIgnoreCase(uc0.get(i)) ) {

						}
						if ( "LatDec".equalsIgnoreCase(uc0.get(i)) ) {

						} else {
							if ( localDebug ) HS_Util.debug("uc0.get[" + i + "]:[" + uc0.get(i) + "] IS NOT in cnames.", "debug", msgContext);
						}
					}
				}

			}
		} catch (final Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if ( HS_Util.isDebugServer() ) {
				e.printStackTrace();
			}
		}

		return valueMap;
	}

	private Vector<String> getEnabledFacilityTypes() {
		final boolean localDebug = ("Yes".equalsIgnoreCase(HS_Util.getAppSettingString("debug_Search")));
		final String msgContext = "QueryServices.getEnabledFacilityTypes";

		final Vector<String> rslt = new Vector<String>();
		try {

			final AppModule[] mdata = AppModules.getModules();

			for (final AppModule module : mdata) {
				if ( !("-Blank-".equalsIgnoreCase(module.getLabel())) ) {

					rslt.add(AppModules.translateOnameToFType(module.getOname()));
				} else { // if
					// (!("-Blank-".equalsIgnoreCase(module.getLabel())))
					if ( localDebug ) HS_Util.debug("got a blank label:[" + module.getLabel() + "]", "debug", msgContext);
				}
			}// for (final AppModule module : mdata)

			if ( localDebug ) HS_Util.debug("Enabled facilities are:[" + rslt + "]", "debug", msgContext);

			return rslt;
		} catch (final Exception e) {
			HS_Util.debug(e.toString() + " returning \"Food Facilities\"", "error", msgContext);
			rslt.add("Food Facilities");
			return rslt;
		}
	}

	@SuppressWarnings("unchecked")
	private ViewEntry getStartEntry(ViewNavigator viewNav, String squery) {
		ViewEntry ent = null;
		String facname = "";
		for (final ViewEntry went : viewNav) {
			final Vector colValues = went.getColumnValues();
			facname = colValues.get(1).toString().toLowerCase();
			if ( facname.startsWith(squery.toLowerCase()) ) {
				ent = went;
				// return ent;
				break;
			}
		}
		return ent;
	}

	public String getFactype() {
		return factype;
	}

	public String getQuery() {
		return query;
	}

	public int getMaxResults() {
		return maxResults;
	}

	public void setFactype(String factype) {
		this.factype = factype;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public void setMaxResults(int maxResults) {
		this.maxResults = maxResults;
	}
}
