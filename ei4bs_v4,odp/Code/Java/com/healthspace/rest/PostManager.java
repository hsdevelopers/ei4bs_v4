package com.healthspace.rest;

import java.io.IOException;
import java.util.Map;

import com.healthspace.general.HS_Util;
import com.healthspace.general.Inspection;

public class PostManager {

	public PostManager() {
	}

	public static String processManager(HS_JsonWriter jw, Map<String, Object> mgrmap, Inspection inspection) throws IOException {
		final String msgContext = "PostManagers.processManager";
		// "manager":"Joe Smith"
		// "managerCertificate":"1234"
		// "managerCertificateExpiration":"2017-01-09"
		try {
			String manager = (String) mgrmap.get("record_manager");
			String managerCertificate = (mgrmap.containsKey("record_managercertificate")) ? (String) mgrmap.get("record_managercertificate") : "";
			String managerAddress = (mgrmap.containsKey("record_manageraddress")) ? (String) mgrmap.get("record_manageraddress") : "";
			String managerCertificateExpiration = (mgrmap.containsKey("record_managercertificateexpiration")) ? (String) mgrmap.get("record_managercertificateexpiration") : "";
			managerCertificateExpiration = HS_Util.formatDate(RestUtil.cvrtJsonDate(managerCertificateExpiration), "dd-MMM-yyyy");

			jw.addProperty("manager", manager);
			jw.addProperty("managerCertificate", managerCertificate);
			jw.addProperty("managerAddress", managerAddress);
			jw.addProperty("managerCertificateExpiration", managerCertificateExpiration);

			inspection.setManager(RestUtil.addListItem(inspection.getManager(), manager));
			inspection.setManagerAddress(RestUtil.addListItem(inspection.getManagerAddress(), managerAddress));
			inspection.setManagerCertificateExpirationStr(RestUtil.addListItem(inspection.getManagerCertificateExpirationStr(), managerCertificateExpiration));
			inspection.setManagerCertificateId(RestUtil.addListItem(inspection.getManagerCertificateId(), managerCertificate));
			return "";
		} catch (Exception e) {
			jw.addProperty("error", e.toString());
			jw.addProperty("errorRoutine", msgContext);
			return e.toString();
		}
	}
}
