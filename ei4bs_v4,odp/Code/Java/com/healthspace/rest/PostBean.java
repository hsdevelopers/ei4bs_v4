package com.healthspace.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.openntf.domino.ACL;
import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.Item;
import org.openntf.domino.Session;
import org.openntf.domino.View;
import org.openntf.domino.utils.Factory;

import com.healthspace.general.HS_Util;

public class PostBean implements Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	public PostBean() {

	}

	private Map<String, Object>	batches;

	public Map<String, Object> getBatches() {
		return batches;
	}

	public void setBatches(Map<String, Object> batches) {
		this.batches = batches;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Object getBatch(String batchkey) {
		final String msgContext = "PostBean.getBatch";
		if ( batches.containsKey(batchkey) ) {
			// System.out.print(msgContext + ": accessing batch map for [" +
			// batchkey + "]");
			return batches.get(batchkey);
		}
		if ( "Yes".equals(HS_Util.getAppSettingString("Debug_HSTPostBean")) ) System.out.print(msgContext + ": batch map for [" + batchkey + "] not found.");
		return null;
	}

	public Map<String, Object> newBatch(String batchkey, int records) throws Exception {
		final String msgContext = this.getClass().getName() + ".newBatch";
		if ( this.batches == null ) {
			this.batches = new HashMap<String, Object>();
		}
		if ( batches.containsKey(batchkey) ) throw (new Exception("Batch for key [" + batchkey + "] already exists!"));
		Map<String, Object> rslt = new HashMap<String, Object>();
		rslt.put("key", batchkey);

		Map<String, Object> rec = null;
		String wnum = "";
		String reckey = "";
		String rsltkey = "";
		for (int i = 1; i <= records; i++) {
			wnum = "0000" + i;
			reckey = "rec" + (wnum).substring(wnum.length() - 4);
			rsltkey = "rslt" + (wnum).substring(wnum.length() - 4);
			rec = new HashMap<String, Object>();
			rec.put("recordNumber", i);
			rslt.put(reckey, rec);
			rslt.put(rsltkey, "none");
		}
		batches.put(batchkey, rslt);
		if ( "Yes".equals(HS_Util.getAppSettingString("Debug_HSTPostBean")) || "Yes".equals(HS_Util.getAppSettingString("Debug_HSTUpload")) )
			System.out.print(msgContext + ": Created batch map for [" + batchkey + "] with [" + records + "] entries.");
		return rslt;
	}

	@SuppressWarnings("unchecked")
	public void buildMapFromDocs() {
		final String msgContext = "PostBean.buildMapFromDocs";
		Map map = new HashMap<String, Object>();
		this.batches = map;
		try {
			Map<String, Object> bmap = null;
			Map<String, Object> rmap = null;
			String batchkey = "";
			String wnum = "";
			String reckey = "";
			Vector<Item> items = null;
			Item item = null;
			View pview = HS_Util.getSession().getCurrentDatabase().getView("RestBasicPosts");
			Document pdoc = pview.getFirstDocument();
			while (pdoc != null) {
				if ( "1".equals(pdoc.getItemValueString("RestRecordNum")) ) {
					if ( bmap != null ) {
						map.put(batchkey, bmap);
						this.setBatches(map);
					}
					batchkey = pdoc.getItemValueString("RestBatch");
					bmap = new java.util.HashMap();
					bmap.put("key", batchkey);
				}
				rmap = new java.util.HashMap();
				wnum = "000" + pdoc.getItemValueString("RestRecordNum");
				reckey = "rec" + (wnum).substring(wnum.length() - 3);
				rmap.put("reckey", reckey);
				rmap.put("unid", pdoc.getUniversalID());

				items = pdoc.getItems();
				for (int i = 0; i < items.size(); i++) {
					item = items.get(i);
					if ( !("Form".equalsIgnoreCase(item.getName()) || "$UpdatedBy".equalsIgnoreCase(item.getName()) || "".equals(item.getName())) ) {
						rmap.put(item.getName().toLowerCase(), item.getText());
					}
				}
				bmap.put(reckey, rmap);

				pdoc = pview.getNextDocument(pdoc);
			}
			if ( bmap != null ) {
				map.put(batchkey, bmap);
				this.setBatches(map);
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if ( "Yes".equals(HS_Util.getAppSettingString("Debug_HSTPostBean")) ) System.out.print(msgContext + ": ERROR: " + e.toString());
		}

	}

	@SuppressWarnings("unchecked")
	public ArrayList<String> getBatchResultKeys(String batchkey) {
		Map<String, Object> batchmap = (Map<String, Object>) this.getBatch(batchkey);
		ArrayList<String> output = new ArrayList<String>();
		Set wrkkeys = batchmap.keySet();
		for (Object okey : wrkkeys.toArray()) {
			String key = (String) okey;
			if ( key.startsWith("rslt") ) output.add(key);
		}
		return output;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<String> getBatchRecordKeys(String batchkey) {
		Map<String, Object> batchmap = (Map<String, Object>) this.getBatch(batchkey);
		ArrayList<String> output = new ArrayList<String>();
		Set wrkkeys = batchmap.keySet();
		for (Object okey : wrkkeys.toArray()) {
			String key = (String) okey;
			if ( key.startsWith("rec") ) output.add(key);
		}
		return output;
	}

	@SuppressWarnings("unchecked")
	public boolean addResults(String batchkey, int recnum, String results) {
		try {
			Map<String, Object> batchmap = (Map<String, Object>) this.getBatch(batchkey);
			String wnum = "0000" + recnum;
			String rsltkey = "rslt" + (wnum).substring(wnum.length() - 4);
			batchmap.put(rsltkey, results);
			// if (
			// "Yes".equals(HS_Util.getAppSettingString("Debug_HSTPostBean")) )
			// System.out.print(this.getClass().getName() +
			// ".addResults Posted results for " + batchkey + "." + rsltkey +
			// "! length:[" + results.length()
			// + "]");
			return true;
		} catch (Exception e) {
			System.out.print(this.getClass().getName() + ".addResults ERROR: " + e.toString());
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public int saveBatch(String batch) {
		String msg;
		String alogMsgs = "";
		int result = 0;
		int debugLevel = 1; // 0=none, 1=minimal, 2=verbose
		final String mtype = "debug";
		final String msgContext = this.getClass().getName() + ".saveBatch";

		Map<String, Object> batchesmap = null;
		Map<String, Object> batchmap = null;
		Map<String, Object> recmap = null;
		Session sess = null;
		Database db = null;
		try {
			msg = "processing batch:[" + batch + "]";
			if ( debugLevel > 0 ) HS_Util.debug(msg, mtype, msgContext);

			batchesmap = this.getBatches();
			msg = "batchesmap keys are: " + batchesmap.keySet().toString();
			if ( debugLevel > 1 ) HS_Util.debug(msg, mtype, msgContext);
			alogMsgs += msg + "\n";

			batchmap = (Map<String, Object>) ((batchesmap.keySet().contains(batch)) ? batchesmap.get(batch) : null);
			if ( batchmap == null ) throw (new Exception("Could not access batch for key: " + batch));

			msg = "batchmap keys are: " + batchmap.keySet().toString();
			if ( debugLevel > 0 ) HS_Util.debug(msg, mtype, msgContext);
			alogMsgs += msg + "\n";

			sess = Factory.getSession();
			db = sess.getCurrentDatabase();

			if ( db.getCurrentAccessLevel() < ACL.LEVEL_AUTHOR ) {
				msg = "Database [" + db.getFilePath() + "] has insufficient access to save batch documents! [" + db.getCurrentAccessLevel() + "]";
				RestUtil.writeWarningToAgentLog(msg, sess.getEffectiveUserName(), msgContext + ".ERROR", "Unknown");
				alogMsgs += "ERROR: " + msg + "\n";
				RestUtil.writeToAgentLog(alogMsgs, msgContext);
				return -1;
			}
			// msg = "batchmap keys are:" + batchmap.keySet().toString();
			// HS_Util.debug(msg, mtype, msgContext);

			for (String rkey : batchmap.keySet()) {
				if ( rkey.startsWith("rec") ) {
					recmap = (Map<String, Object>) batchmap.get(rkey);
					if ( recmap != null ) {
						if ( saveOutputDoc(recmap, batch, db, batchmap, rkey) ) result++;
					}
				} else {
					// msg = "batch level element [" + rkey + "] found. value:["
					// + (String) batchmap.get(rkey) + "]";
					msg = "batch level element [" + rkey + "] found and not saved";
					alogMsgs += msg + "\n";
					if ( debugLevel > 1 ) HS_Util.debug(msg, mtype, msgContext);
				}
			}
			msg = "saved :" + result + " records";
			alogMsgs += msg + "\n";
			if ( debugLevel > 0 ) HS_Util.debug(msg, mtype, msgContext);
			if ( "Yes".equals(HS_Util.getAppSettingString("Debug_HSTPostBean")) ) RestUtil.writeToAgentLog(alogMsgs, msgContext);

			return result;
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			alogMsgs += "ERROR: " + e.toString() + "\n";
			RestUtil.writeToAgentLog(alogMsgs, msgContext);
		}
		return result;

	}

	private boolean saveOutputDoc(Map<String, Object> req, String restBatch, Database db, Map<String, Object> batchmap, String reckey) {
		final String msgContext = db.getFilePath() + ".PostBean.saveOutputDoc.new";

		Document doc = null;
		// Item nitem = null;
		final int flen = 16000;
		try {
			// if ( "schPhotos".equalsIgnoreCase(this.schema) ) {
			// return savePhotoDocument(req, jw);
			// }
			// if ( !req.keySet().contains("RestSchema") ) return false;
			// String schema = (String) req.get("RestSchema");

			doc = db.createDocument();
			doc.replaceItemValue("Form", "RestBasicPost");
			doc.replaceItemValue("PostAuthor", db.getParent().getEffectiveUserName());
			doc.replaceItemValue("RestBatch", restBatch);

			if ( req == null ) {
				doc.replaceItemValue("Error", msgContext + " - request map is null!");
			} else if ( req.keySet() == null ) {
				doc.replaceItemValue("Error", msgContext + " - request map keyset is null!");
			} else if ( req.keySet().isEmpty() ) {
				doc.replaceItemValue("Error", msgContext + " - request map keyset is empty!");
			} else if ( req.keySet().iterator() == null ) {
				doc.replaceItemValue("Error", msgContext + " - request map keyset iterator is null!");
			} else {
				// Integer recnum = (Integer) req.get("restrecordnumber");
				// Integer reccount = (Integer) req.get("restrecordcount");
				// String wnum = "0000" + recnum;
				String rsltkey = "rslt" + (reckey).substring(reckey.length() - 4);

				for (String fname : req.keySet()) {
					Object fvalue = req.get(fname);

					if ( "Auth_Password".equals(fname) ) {
						// security issue. No longer saving this...
					} else {
						if ( fvalue.toString().length() > flen ) {
							splitIntoPieces(fvalue.toString(), doc, flen, fname);
						} else {
							doc.replaceItemValue(fname, fvalue);
						}
					}
				}
				if ( batchmap.containsKey(rsltkey) ) {
					String results = (String) batchmap.get(rsltkey);
					if ( results.length() > flen ) {
						splitIntoPieces(results, doc, flen, "Batch_Results");
					} else {
						doc.replaceItemValue("Batch_Results", results);
					}

					int pos = results.indexOf("\"recordnum\":\"");
					String recnum = results.substring(pos + 13);
					recnum = recnum.substring(0, recnum.indexOf("\""));
					pos = results.indexOf("\"recordcount\":\"");
					String reccount = results.substring(pos + 15);
					reccount = reccount.substring(0, reccount.indexOf("\""));
					if ( "Yes".equals(HS_Util.getAppSettingString("Debug_HSTPostBean")) )
						System.out.print(msgContext + " - Saving record" + reckey + ".  rsltkey:[" + rsltkey + "]  recnum:[" + recnum + "] reccount:[" + reccount + "]");
					if ( (recnum.equals(reccount)) && (batchmap.containsKey("inspections")) ) {

						String binsp = (String) batchmap.get("inspections");
						splitIntoPieces(binsp, doc, flen, "batch_inspections");
						// Item binspItem = null;
						// if ( binsp.length() < flen ) {
						// binspItem = doc.replaceItemValue("batch_inspections", binsp);
						// binspItem.setSummary(false);
						// } else {
						// try {
						// int ctr = 0;
						// String fname;
						// while (binsp != null) {
						// fname = "batch_inspections";
						// fname += (ctr == 0) ? "" : "_" + ctr;
						// if ( binsp.length() < flen ) {
						// binspItem = doc.replaceItemValue(fname, binsp);
						// binspItem.setSummary(false);
						// binsp = null;
						// } else {
						// binspItem = doc.replaceItemValue(fname, binsp.substring(0, flen));
						// binspItem.setSummary(false);
						// binsp = binsp.substring(flen);
						// }
						// ctr++;
						// }
						// } catch (Exception e1) {
						// doc.replaceItemValue("batch_inspection_error", e1.toString());
						// }
						// }
					}
				} else {
					doc.replaceItemValue("Batch_Results", "No results found for [" + rsltkey + "]");
				}
			}

			if ( !doc.hasItem("RestSchema") ) return false;

			if ( doc.save() ) return true;

			return false;
		} catch (Exception e) {
			// HS_Util.debug(e.toString(), "error", msgContext);
			System.out.print(msgContext + " on " + reckey + ": " + e.toString());
			e.printStackTrace();
			if ( doc != null ) {
				doc.replaceItemValue("Error", e.toString() + " in " + msgContext);
				doc.save();
			}
			// try {
			// jw.addProperty("status", "error");
			// jw.addProperty("error", e.toString() + " in " + msgContext);
			// if ( this.recmap != null ) this.recmap.put("error", e.toString()
			// + " in " + msgContext);
			// } catch (IOException e1) {
			// }

		}
		return false;
	}

	private void splitIntoPieces(String srcValue, Document doc, int flen, String itemName) {
		Item outItem = null;
		if ( srcValue.length() < flen ) {
			outItem = doc.replaceItemValue(itemName, srcValue);
			outItem.setSummary(false);
		} else {
			try {
				int ctr = 0;
				String fname = "";
				while (srcValue != null) {
					fname = itemName;
					fname += (ctr == 0) ? "" : "_" + ctr;
					if ( srcValue.length() < flen ) {
						outItem = doc.replaceItemValue(fname, srcValue);
						outItem.setSummary(false);
						srcValue = null;
					} else {
						outItem = doc.replaceItemValue(fname, srcValue.substring(0, flen));
						outItem.setSummary(false);
						srcValue = srcValue.substring(flen);
					}
					ctr++;
				}
			} catch (Exception e1) {
				doc.replaceItemValue("splitIntoPiecesError", e1.toString());
				System.out.print("splitIntoPiecesError: " + e1.toString() + " - pn item:[" + itemName + "]");
			}
		}
	}

}
