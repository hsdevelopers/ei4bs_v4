package com.healthspace.rest;

import java.io.IOException;
import java.util.Map;

import com.healthspace.general.HS_Util;
import com.healthspace.general.Inspection;
import com.healthspace.general.TimeTracking;

public class PostTimeReport {

	public PostTimeReport() {
	}

	public static String processTimeReport(HS_JsonWriter jw, Map<String, Object> tempmap, Inspection inspection) throws IOException {
		// {"tabIDTime_reportsID":"050ecc26-7281-4d54-892a-32e5601c38d1"
		// "inspectionID":"e20f7f85-53ac-41d3-a51c-3a71083486bd"
		// "timeDate":"2015-03-09"
		// "timeEho":"Henry Newberry"
		// "timeType":"Travel"
		// "timeIn":""
		// "timeOut":""
		// "timeMin":"90"
		// "timeHours":""
		// "timeComments":"Travel"
		// "synced":"0"
		// "modifiedBy":"Henry Newberry"
		// "dateadd":"2015-3-9 15:30:08.404"
		// "datelastmodified":"2015-3-9 15:30:08.404"}
		final String msgContext = "PostTimeReport.processTimeReport";
		try {
			String timeDate = (String) tempmap.get("record_timedate");
			String timeEho = (String) tempmap.get("record_timeeho");
			String timeType = (String) tempmap.get("record_timetype");
			String timeIn = (String) tempmap.get("record_timein");
			String timeOut = (String) tempmap.get("record_timeout");
			String timeMin = (String) tempmap.get("record_timemin");
			String timeHours = (String) tempmap.get("record_timehours");
			String timeComments = (String) tempmap.get("record_timecomments");
			// String xxx = (String) tempmap.get("record_xxx");

			jw.addProperty("timeDate", timeDate);
			jw.addProperty("timeEho", timeEho);
			jw.addProperty("timeType", timeType);
			jw.addProperty("timeData", "i:[" + timeIn + "] o:[" + timeOut + "] h[" + timeHours + "] m[" + timeMin + "]");

			// inspection.setPpm(RestUtil.addListItem(inspection.getPpm(),
			// ppm));
			TimeTracking trpt = new TimeTracking();
			trpt.loadDefaults(inspection);
			// trpt.setDate(RestUtil.cvrtJsonDate(timeDate));
			trpt.setDateStr(HS_Util.formatDate(RestUtil.cvrtJsonDate(timeDate), "dd-MMM-yyyy"));
			trpt.setEho(timeEho);
			trpt.setTimeType(timeType);
			trpt.setTimeIn(timeIn);
			trpt.setTimeOut(timeOut);
			if ( !"".equals(timeMin) ) trpt.setMin(Double.parseDouble(timeMin));
			if ( !"".equals(timeHours) ) trpt.setHours(Double.parseDouble(timeHours));
			trpt.setTrackingComments(timeComments);

			trpt.setTimeSpent(trpt.computeTimeSpent());
			trpt.buildViewDescription();
			inspection.getTimeTrackingRecords().add(trpt);
			return "";
		} catch (Exception e) {
			jw.addProperty("error", e.toString());
			jw.addProperty("errorRoutine", msgContext);
			return e.toString();
		}
	}
}
