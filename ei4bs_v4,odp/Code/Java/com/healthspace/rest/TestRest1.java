package com.healthspace.rest;

import java.io.IOException;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ibm.domino.services.ServiceException;
import com.ibm.domino.services.rest.RestServiceEngine;
import com.ibm.xsp.extlib.component.rest.CustomService;
import com.ibm.xsp.extlib.component.rest.CustomServiceBean;

public class TestRest1 extends CustomServiceBean {

	public TestRest1() {
		// System.out.print(this.getClass().getName() + " initializing...");
	}

	@Override
	public void renderService(CustomService service, RestServiceEngine engine) throws ServiceException {
		// System.out.print(this.getClass().getName() + " renderService running...");

		HttpServletRequest request = engine.getHttpRequest();
		HttpServletResponse response = engine.getHttpResponse();

		String output = "";
		try {
			String method = request.getMethod();
			// System.out.print(this.getClass().getName() + " renderService method:[" + method + "]");
			response.setHeader("Content-Type", "text/plain; charset=UTF-8");

			if ( method.equals("GET") ) {
				output = this.get(engine);
			} else if ( method.equals("POST") ) {
				this.post(engine, request);
			} else {
				this.other(engine);
			}

		} catch (Exception e) {
			output = "{ \"ERROR\" : \"" + this.getClass().getName() + ".renderService ERROR: " + e.toString() + "\"}";
			System.out.print(output);
		} finally {
			try {
				response.getWriter().write(output);
				response.getWriter().close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void other(RestServiceEngine engine) {
		// TODO Auto-generated method stub

	}

	private void post(RestServiceEngine engine, HttpServletRequest request) {
		// TODO Auto-generated method stub

	}

	private String get(RestServiceEngine engine) {
		String msgContext = this.getClass().getName() + ".get";
		StringWriter sw = new StringWriter();
		HS_JsonWriter jw = new HS_JsonWriter(sw, false);
		try {
			jw.startObject();
			jw.addProperty("method", msgContext);

			jw.endObject();
			return sw.toString();

		} catch (IOException e) {
			e.printStackTrace();
			String output = "{ \"ERROR\" : \"" + this.getClass().getName() + ".renderService ERROR: " + e.toString() + "\"}";
			System.out.print(output);
			return output;
		}
	}

}
