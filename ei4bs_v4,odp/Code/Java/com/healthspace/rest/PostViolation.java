package com.healthspace.rest;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

import org.openntf.domino.Database;
import org.openntf.domino.Document;

import com.healthspace.general.HS_Util;
import com.healthspace.general.Inspection;
import com.healthspace.general.InspectionSection;
import com.healthspace.general.Violation;

public class PostViolation {

	public PostViolation() {

	}

	public static String processViolationSection(HS_JsonWriter jw, Map<String, Object> sectionmap, Inspection inspection, Database eidb) throws IOException {
		return processViolationSection(jw, sectionmap, inspection, eidb, false);
	}

	public static String processViolationSection(HS_JsonWriter jw, Map<String, Object> sectionmap, Inspection inspection, Database eidb, boolean localDebug) throws IOException {
		final String msgContext = RestUtil.showDBPath() + ".PostViolation.processViolationSection";
		// TDA Format
		// {"violationID":"fb134d32-5670-46d9-bbc3-3773c0d058d3"
		// "inspectionID":"a4d77f4c-eaa9-4f50-840f-ecbc17b13955"
		// "itemID":"01 Person in charge present demonstrates knowledge and performs duties."
		// "observationID":"IN"
		// "violationType":""
		// "cbDate":""
		// "codeID":""
		// "commentID":""
		// "comments":""
		// "synced":"0"
		// "finalized":0
		// "unid":"85257F87005FB15785257F42000AE57B"
		// "documentID":"NFRY-A6AT5J"}

		// * Actual violation format:
		// * {"violationID":"6609f7b2-519a-413c-b387-cec506bd7762"
		// "inspectionID":"280d9357-296f-495f-a884-35cec254788b"
		// "itemID":"01 Person in charge present demonstrates knowledge and performs duties."
		// "observationID":"OUT"
		// "violationType":"Repeat"
		// "cbDate":""
		// "codeID":"0080-04-09-.02(1)(a)"
		// "commentID":""
		// "comments":"Person in charge not present at time of inspection."
		// "synced":"0"
		// "[object Object]":""
		// "unid":"77775105240DF8FE85257F42000AE57B"
		// "documentID":"NFRY-A6AT5J"
		// "codeUnid":"B0AA7108CD9D8E7685257F42000AE5C9"
		// "isCritical":"No"}

		// This is what is coming from NW Michingan.
		// {"violationID":"172d1d84-1d70-4f28-aeac-ca1922fdd9dc"
		// "itemID":"01 Person in charge present demonstrates knowledge and performs duties."
		// "observationID":"OUT"
		// "inspectionID":"112acc8a-3635-4110-8ce7-703227ddcd6d"
		// "violationType":["COS","Repeat", "Violation"]
		// "cbDate":"2018-01-09"
		// "codeID":"2-101.11"
		// "commentID":""
		// "comments":" A person in charge shall be provided on premises at all times during hours of operation."
		// "correctiveActions":"Corrections entered in hst corrective action field."
		// "datelastmodified":"2018-01-08 14:45:05.371"
		// "modifiedBy":""
		// "dateadd":"2018-01-08 14:45:05.371"
		// "synced":0
		// "finalized":0
		// "codeUnid":"486BCE0455B795BA85257D2D0000CC3E"
		// "isCritical":"No"
		// "unid":"491F0E6614A7F56B85257D2D0000C985"
		// "documentID":"NFAR-88DSMV"}

		String errorMsg = "";
		// String msg = "";
		String sectionunid = "-not in data-";
		String itemid = "-not in data-";
		String observationId = "-not in data-";
		String documentID = "-not in data-";
		String status = "-not in data-";
		String violType = "-not in data-";
		String repeat = "";
		String correctedSet = "";
		String observation = "-not in data-";
		String correctiveAction = "";
		String cbDateStr = "";

		InspectionSection isec = null;
		Document sectiondoc = null;
		try {
			if ( (sectionmap.containsKey("record_unid")) ) {
				sectionunid = (String) sectionmap.get("record_unid");
				itemid = (String) sectionmap.get("record_itemid");
				observationId = (String) sectionmap.get("record_observationid");
				documentID = (String) sectionmap.get("record_documentid");
				if ( sectionmap.containsKey("record_violationtype") ) {
					if ( sectionmap.get("record_violationtype") instanceof String ) {
						violType = (String) sectionmap.get("record_violationtype");
					} else {
						violType = sectionmap.get("record_violationtype").toString();
					}
				} else
					violType = "";

				repeat = (violType.contains("Repeat")) ? "yes" : "";
				correctedSet = (violType.contains("COS")) ? "yes" : "";

				observation = (sectionmap.containsKey("record_comments")) ? (String) sectionmap.get("record_comments") : "";
				correctiveAction = (sectionmap.containsKey("record_correctiveactions")) ? (String) sectionmap.get("record_correctiveactions") : "";
				cbDateStr = (sectionmap.containsKey("record_cbdate")) ? (String) sectionmap.get("record_cbdate") : "";
			}

			jw.addProperty("sectionunid", sectionunid);
			jw.addProperty("itemid", itemid);
			jw.addProperty("observationId", observationId);
			jw.addProperty("documentID", documentID);

			if ( "-not in data-".equals(sectionunid) ) {
				// jw.addProperty("recordKeys", sectionmap.keySet().toString());
				String x1 = "";
				for (Object x : sectionmap.keySet()) {
					try {
						x1 = (String) x;
						if ( x1.startsWith("record_") ) jw.addProperty(x1, (String) sectionmap.get(x));
					} catch (Exception ex) {
						jw.addProperty("error", "on tag:[" + x.toString() + "] error: " + ex.toString());
					}
				}
			} else {
				sectiondoc = HS_Util.getDocumentByUNID(eidb, sectionunid, false);
			}
			if ( sectiondoc == null ) {
				jw.addProperty("error", "could not access section document with unid:[" + sectionunid + "]");
				errorMsg = "processViolationSection could not access section document with unid:[" + sectionunid + "]";
				return errorMsg;
			} else {
				isec = new InspectionSection();
				isec.setCategory(sectiondoc.getItemValueString("Category"));
				isec.setDescription(sectiondoc.getItemValueString("Description"));
				isec.setGroup(sectiondoc.getItemValueString("SectionGroup"));
				isec.setModule(inspection.getViolationModule().get(0));
				isec.setNumber(sectiondoc.getItemValueString("SectionNumber"));

				status = ("IN".equals(observationId)) ? "In" : ("OUT".equals(observationId)) ? "Out" : observationId;
				isec.setStatus(status);

				jw.addProperty("sectionCategory", isec.getCategory());
				jw.addProperty("sectionDescription", isec.getDescription());
				jw.addProperty("sectionGroup", isec.getGroup());
				jw.addProperty("sectionModule", isec.getModule());
				jw.addProperty("sectionNumber", isec.getNumber());
				jw.addProperty("sectionStatus", status);

				// TODO Probably should make sure that it is not already in the
				// list....

				inspection.getSections().add(isec);
			}

			if ( !"OUT".equalsIgnoreCase(status) ) {
				isec.setObservation(observation);
				jw.addProperty("sectionObservation", observation);
				return errorMsg;
			}
			if ( !sectionmap.containsKey("record_codeid") ) {
				errorMsg = "Section [" + isec.getNumber() + ". " + isec.getDescription() + "] marked out with no violation code!";
				jw.addProperty("error", errorMsg);
				return errorMsg;
			}
			String codeID = (String) sectionmap.get("record_codeid");
			jw.addProperty("codeID", codeID);
			if ( "".equals(codeID) ) {
				errorMsg = "Section [" + isec.getNumber() + ". " + isec.getDescription() + "] marked out with no violation code!";
				jw.addProperty("error", errorMsg);
				return errorMsg;
			}
			String codeunid = (String) sectionmap.get("record_codeunid");
			if ( codeunid == null ) {
				errorMsg = "Section [" + isec.getNumber() + ". " + isec.getDescription() + "] marked out for codeID: [" + codeID + "] with no code unid!";
				jw.addProperty("error", errorMsg);
				return errorMsg;
			}
			jw.addProperty("codeUnid", codeunid);
			Document vdoc = HS_Util.getDocumentByUNID(eidb, codeunid, false);
			if ( vdoc == null ) {
				errorMsg += "could not access violation document for codeID: [" + codeID + "] with unid:[" + codeunid + "]";
				jw.addProperty("error", errorMsg);
				return errorMsg;
			}
			if ( !"ViolationCode".equalsIgnoreCase(vdoc.getFormName()) ) {
				errorMsg += "violation document with unid:[" + codeunid + "] is not a ViolationCode document. It is a: [" + vdoc.getFormName() + "]";
				jw.addProperty("error", errorMsg);
				return errorMsg;
			}
			Violation viol = new Violation();
			viol.setGroup(sectiondoc.getItemValueString("SectionGroup"));
			viol.setCategory(sectiondoc.getItemValueString("Category"));
			viol.setSection(sectiondoc.getItemValueString("SectionNumber"));
			viol.setCode(codeID);

			viol.setCorrectedSet(correctedSet);
			jw.addProperty("violCorrectedSet", correctedSet);

			// if the correctBy date is set then add it to the Corrective Action
			if ( !"".equals(cbDateStr) ) {
				String dtformat = HS_Util.getAppSettingString("inspDateFormat");
				Date cbDate = RestUtil.cvrtJsonDate(cbDateStr);
				jw.addProperty("violCBDateStr", cbDateStr);
				jw.addProperty("violCBDate", cbDate.toString());

				// this.vcorrect += " Correct By: " +
				// this.formatDate(this.vcorrectby);
				String cbDateOut = ("".equals(correctiveAction) ? "" : "  ");
				cbDateOut += "Correct By: " + HS_Util.formatDate(cbDate, dtformat) + ".";
				jw.addProperty("violCorrectByDate", cbDateOut);
				correctiveAction += cbDateOut;
			}

			viol.setCorrectiveAction(correctiveAction);
			jw.addProperty("violCorrectiveAction", correctiveAction);
			if ( localDebug ) System.out.print(msgContext + " - Corrective Action set to:[" + correctiveAction + "]");
			viol.setCriticalSet(vdoc.getItemValueString("Critical"));
			jw.addProperty("violCriticalSet", viol.getCriticalSet());

			viol.setDescription(vdoc.getItemValueString("Description"));
			jw.addProperty("violDescription", vdoc.getItemValueString("Description"));

			// a - The HzrdRating value may come from the section document instead of the VIolation document.
			// If InspDoc.HazRatingType(0) = "Section" Or InspDoc.HazRatingType(0) = "SectionCategory" Then
			String hrt = inspection.getHazRatingType();
			String hrtDbug = "";
			if ( "Section".equalsIgnoreCase(hrt) || "SectionCategory".equalsIgnoreCase(hrt) ) {
				String hrvalue = sectiondoc.getItemValue("HazardRating").get(0).toString();
				hrtDbug = (sectiondoc.hasItem("HazardRating")) ? "section doc has the HazardRating item:[" + hrvalue + "]" : "HazardRating MISSING FROM SESSION DOC";
				viol.setHzrdRating(hrvalue);
				// } else if ( "SectionCategory".equalsIgnoreCase(hrt) ) {
				// // viol.setHzrdRating("ERROR SectionCategory Hazard Rating source not implemented");
				// throw new Exception("SectionCategory Hazard Rating source not implemented");
			} else if ( "Code".equalsIgnoreCase(hrt) ) {
				viol.setHzrdRating(vdoc.getItemValueString("HazardRating"));
			} else if ( "Observation".equalsIgnoreCase(hrt) ) {
				// viol.setHzrdRating("ERROR SectionCategory Hazard Rating source not implemented");
				throw new Exception("Observation Hazard Rating source not implemented");
			}
			jw.addProperty("inspHazRatingType", hrt);
			jw.addProperty("violHzrdRating", viol.getHzrdRating());
			if ( !"".equals(hrtDbug) ) jw.addProperty("violHzrdRatingDebug", hrtDbug);

			viol.setLegalTxt("");

			viol.setObservations((String) sectionmap.get("record_comments"));
			// viol.setObservations(lookupBasicObservation(codeID,
			// inspection.getViolationModule().get(0)));
			jw.addProperty("violObservations", viol.getObservations());

			// sectionmap.get("record_comments"));

			viol.setRepeat(repeat);
			jw.addProperty("violRepeat", repeat);
			viol.setRoom("");
			viol.setStatus("Out");
			viol.setUsed(true);

			inspection.getViolations().add(viol);

		} catch (Exception e) {
			errorMsg = msgContext + " : " + e.toString();
			jw.addProperty("error", e.toString());
			jw.addProperty("errorRoutine", msgContext);
			e.printStackTrace();
		}

		return errorMsg;
	}

	// private static View observeView = null;
	//
	//
	// private static String lookupBasicObservation(String codeID, String
	// module) {
	// String result = "";
	// if ( observeView == null ) {
	// observeView = eidb.getView("XPagesObservationsByModule");
	// }
	// if ( observeView == null ) return
	// "unable to access view : XPagesObservationsByModule";
	// Vector<String> keys = new Vector<String>();
	// keys.add(module);
	// keys.add(codeID);
	// Document odoc = observeView.getFirstDocumentByKey(keys, true);
	// if ( odoc == null ) return
	// "unable to access observation document with keys:" + keys.toString();
	// result = odoc.getItemValueString("observation");
	// return result;
	// }

}
