package com.healthspace.rest;

import java.io.IOException;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.openntf.domino.Database;
import org.openntf.domino.DateTime;
import org.openntf.domino.Document;
import org.openntf.domino.Item;
import org.openntf.domino.View;
import org.openntf.domino.ViewEntry;

import com.healthspace.general.HS_Util;
import com.healthspace.general.HS_database;
import com.healthspace.tools.JSFUtil;
import com.ibm.commons.util.io.json.JsonJavaObject;

public class QueryFacilities {
	/**
	 * PRIVATE getFieldMap(fmap) Returns the Field Map identified by the name in the fmap param. The Field Map contains a map of the EI app fields, the
	 * corresponding IG fields, and the Data Types of the different record types that are common between the EI App and the IG Tablet App. PARAMS -- fmap
	 * (String) == name of the Field Map to use to create the document HashMap object NOTE: if fmap == "", the Facility Field Map is returned RETURNS The Field
	 * Map is returned as a HashMap, where the KEY is the EI Field Name AND the Data Type (EI_FIELD^TYPE), and the VALUE is the corresponding IG Field. NOTE:
	 * There is an extra value returned in the Field Map HashMap - $MODIFIED$ -- $MODIFIED$ contains the Date/Time the Field Map was last saved.
	 */
	/*
	 * ************* UPDATE LOG *************** 07 JAN 2015 (1.49) - modified to provide field map name as a param, and some defensive coding - added support
	 * for Last Modified date (stored with key == $modified$) 14 JAN 2015 (1.50) - Changed from $modified$ to using version and codeContext for version info 24
	 * JAN 2015 (1.51) - Updated what's written to where, etc. for logging and errors - Added support for MAPINFO data type, so we can gather diagnostic info
	 * from the Field Map object itself 27 JAN 2015 (1.52) - Added support for the data type MAPINFO, to provide diagnostic info such as Field Map name/id,
	 * modified by, modified on 19 FEB 2015 (1.53) - Updated to add in proper logging using the RestUtil.writeToAgentLog() 9 March 2015 (1.54) - Newbs - Added
	 * more info to the log and setup get field map to not do its own logging. *****************************************
	 */
	private String logmsgs = "";

	@SuppressWarnings("unchecked")
	private Map<String, String> getFieldMap(String fmapParam) {
		final String version = "1.80, 14 Apr 2016";
		final String method = "QueryFacilities.getFieldMap";
		final String codeContext = method + " [" + version + "]";
		final boolean localDebug = ("Yes".equals(HS_Util.getAppSettingString("debug_HSTDownload")));
		// String logtxt = "";
		// final boolean rockDebug = false;

		final String mapViewName = "code.fld.maps.by.name";
		final Map<String, String> fldmap = new HashMap<String, String>();
		try {
			// retrieve the field.map name to use
			if ( "".equals(fmapParam) ) {
				fmapParam = JSFUtil.getParamValue("fmap");
				if ( "".equals(fmapParam) ) fmapParam = "Facility"; // if blank use Facility by default
			}
			if ( localDebug ) logmsgs += "\nField Map: " + fmapParam;

			// get the ei database and the field maps view
			final Database thisdb = HS_Util.getSession().getCurrentDatabase();
			final View mapView = thisdb.getView(mapViewName);
			if ( mapView == null ) {
				throw (new Exception("Unable to get handle to view [" + mapViewName + "]"));
			}

			// get Field Map doc specified in fmap query string param
			final ViewEntry mapEntry = mapView.getFirstEntryByKey(fmapParam.toLowerCase(), true);
			if ( mapEntry == null ) {
				throw (new Exception("Unable to find Field Map document [key: " + fmapParam + "]"));
			}

			// get the field map itself from the second column of the view
			final Vector ecols = mapEntry.getColumnValues();
			final Vector fmap = (Vector) ecols.elementAt(1);
			// split up the field map, and create the field map hashmap to
			// return on the function
			for (int i = 0; i < fmap.size(); i++) {
				final String[] parts = fmap.elementAt(i).toString().split("~");
				fldmap.put(parts[0], parts[1]);
			}
			fldmap.put("$MapID^MAPINFO", fmapParam);
			if ( localDebug ) fldmap.put("$MapModifiedOn^MAPINFO", ecols.elementAt(2).toString());
			if ( localDebug ) fldmap.put("$MapModifiedBy^MAPINFO", ecols.elementAt(3).toString());
		} catch (final Exception e) {
			/*
			 * RestUtil.writeToAgentLog(String message, String description) for handling the logging.. The message is the detail message to log ("\n" for new
			 * lines)... That could be the output JSON and the INPUT parms. Description is where it is called from (Class.Method).
			 */
			logmsgs += "\n\n ERROR IN: [" + codeContext + "]\n " + e.toString();
			// RestUtil.writeToAgentLog(logtxt, codeContext);
			HS_Util.debug(e.toString(), "ERROR", codeContext);
			if ( HS_Util.isDebugServer() ) {
				e.printStackTrace();
				// HS_Util.logException(e.toString(), curObj, codeContext);
			}
			fldmap.put("FAIL-ERROR-1", e.toString());
			fldmap.put("FAIL-CONTEXT-1", codeContext);
		}
		return fldmap;
	}

	/**
	 * PRIVATE getMappedFieldValue(Document srcDoc, String mapKey, Integer fldIdx) This function returns value(s) from a "mapped field" as defined in a Field
	 * Map. - If mapped field is multivalue, the value at the provided index number (fldIdx) is returned, cast to the provided data type - If fldIdx < 0, all
	 * values are returned - If fldIdx > Ubound(field values), the last value is returned - Supported Field Map data types are TEXT, NUMBER, DATE, or FORMULA.
	 * PARAMS -- srcDoc (Document) == the document containing the element field -- mapKey (String) == the name of the field and the data type
	 * (fldname^datatype); param usually provided from a Field Map -- fldIdx (Integer) == the index of the desired value from the field; - use -1 (or any
	 * integer < 0) to return ALL field values
	 */
	/*
	 * ************* UPDATE LOG *************** 14 JAN 2015 (1.50) - Added switch to support data types 27 JAN 2015 (1.51) - Updated error trapping, versioning
	 * info 28 JAN 2015 (1.60) - Changed the name from getMapFieldInfo() to getMappedFieldInfo(); this was done to make it more clear exactly what the function
	 * did - that it would get a value from the provided document, based on the field name/data type from a Field Map - not return info from a Field Map -
	 * Greatly modified so it can be the single method used to retrieve mapped field values from a document. This allows a single location for mapped field
	 * retrieval code, which was not the case before - Changed to support returning ALL values from a mapped field, if desired; this choice is indicated by
	 * setting fldIdx < 0 (usually -1) - Reworked FORMULA to work similarly to the other data type choices 19 FEB 2015 (1.61) - Added try/catch blocks and agent
	 * logging 20 FEB 2015 (1.62) - Fixed error with date/time processing and formatting 9 March 2015 (1.63) - Newbs - Added more info to the log and setup get
	 * field map to not do its own logging. *****************************************
	 */
	private Object getMappedFieldValue(Document srcDoc, String mapKey, Integer fldIdx) {
		final String version = "1.71, 6 JUL2016";
		final String method = this.getClass().getName() + ".getMappedFieldValue";
		final String codeContext = method + " [" + version + "]";
		final boolean localDebug = ("Yes".equals(HS_Util.getAppSettingString("debug_HSTDownload")));
		// final boolean rockDebug = false;
		Object retVal = "";
		// String debugStr = "";
		// String logtxt = "";

		// field ty[es converted to Integers for switch()
		final Map<String, Integer> dTypes = new HashMap<String, Integer>();
		dTypes.put("TEXT", 1);
		dTypes.put("DATE", 2);
		dTypes.put("NUMBER", 3);
		dTypes.put("FORMULA", 4);
		dTypes.put("MAPINFO", 5);

		try {
			if ( localDebug ) logmsgs += "\n\n srcDoc-UNID[" + srcDoc.getUniversalID() + "] MAP-KEY[" + mapKey + "] FLD-IDX[" + fldIdx.toString() + "]";
			final String[] hsfldtype = mapKey.split("\\^");
			final String fldName = hsfldtype[0];
			final String dType = hsfldtype[1];
			// if (rockDebug) {
			// debugStr = " [Type(n):" + dType + "(" + dTypes.get(dType.toUpperCase()) + ")]";
			// }
			Item tmpfld = srcDoc.replaceItemValue("tmpItem", null);
			if ( srcDoc.hasItem(fldName) ) {
				/*
				 * **************** GETTING THE RIGHT VALUE **************** A temp item is created with the proper value, and that is used to work with it and
				 * return the right value and data type - If the fldIdx is a negative, it means that the user wants all values - If it is positive, then check
				 * to see if fldIdx is larger than the number of elements in the field - if it is, then get the last value - otherwise, get the desired value -
				 * then the "raw" value is passed, as an Object, to the switch() so that it can be converted to the desired data type
				 * **********************************************************
				 */
				final Item fld = srcDoc.getFirstItem(fldName);
				final Integer valCount = fld.getValues().size();
				if ( localDebug ) logmsgs += " FLD-VAL-COUNT[" + valCount.toString() + "]";
				if ( fld.getValues().isEmpty() ) {
					tmpfld = srcDoc.replaceItemValue("tmpItem", "");
				} else {
					if ( fldIdx < 0 ) {
						tmpfld = fld;
					} else {
						if ( valCount >= fldIdx ) {
							try {
								tmpfld = srcDoc.replaceItemValue("tmpItem", fld.getValues().elementAt(fldIdx));
							} catch (Exception e1) {
								// if (localDebug) logmsgs += " \n ERROR in replaceItemValue: " + e1.toString();
								logmsgs += " \n\nERROR IN [" + codeContext + ".replaceItemValue]: \n " + e1.toString();
								logmsgs += " \nfld.getValues().size():[" + fld.getValues().size() + "]  fldIdx:[" + fldIdx + "] fldName:[" + fldName + "]";
								HS_Util.debug(e1.toString(), "ERROR", codeContext + ".replaceItemValue");
								if ( HS_Util.isDebugServer() ) e1.printStackTrace();
							}
						} else {
							tmpfld = srcDoc.replaceItemValue("tmpItem", fld.getValues().lastElement());
						}
					}
				}
				final String fldStr = tmpfld.getText();
				if ( fldStr.equals("") ) {
					retVal = "";
				} else {
					switch (dTypes.get(dType.toUpperCase())) {
					case 2: // Date
						// final Date flddate =
						// HS_Util.cvrtStringToDate(tmpfld.getDateTimeValue().getDateOnly());
						// retVal = HS_Util.formatDate(flddate, "yyyy-MM-dd");
						final DateTime flddate = tmpfld.getDateTimeValue();
						flddate.setAnyTime();
						retVal = HS_Util.formatDate(flddate.toJavaDate(), "yyyy-MM-dd");
						break;
					case 3: // Number
						if ( fldStr.equals("") ) {
							retVal = 0;
						} else {
							retVal = tmpfld.getValueDouble();
						}
						break;

					case 5:
						// TYPE: MapInfo - not really needed in this context
						// It's handled in the calling code, since the use is
						// dependent on the code using it
						retVal = "";
						break;

					case 1: // Text (also the default data type)
					default:
						retVal = tmpfld.getValueString();
						break;
					}
				}
			} else {
				if ( dType.toUpperCase().equals("FORMULA") ) {
					final Vector<?> feval = HS_Util.getSession().evaluate(fldName, srcDoc);
					if ( fldIdx < 0 ) {
						tmpfld = srcDoc.replaceItemValue("tmpItem", feval);
					} else {
						if ( feval.size() >= fldIdx ) {
							tmpfld = srcDoc.replaceItemValue("tmpItem", feval.lastElement());
						} else {
							tmpfld = srcDoc.replaceItemValue("tmpItem", feval.elementAt(fldIdx));
						}
					}
					retVal = tmpfld.getValueString();
				}
			}

			// if (rockDebug) {
			// debugStr += " VAL:" + retVal.toString();
			// retVal = debugStr;
			// }
			if ( localDebug ) logmsgs += " \n RET-VAL[" + retVal.toString() + "]";
		} catch (final Exception e) {
			/*
			 * RestUtil.writeToAgentLog(String message, String description) for handling the logging.. The message is the detail message to log ("\n" for new
			 * lines)... That could be the output JSON and the INPUT parms. Description is where it is called from (Class.Method).
			 */
			logmsgs += " \n\nERROR IN [" + codeContext + "]: \n " + e.toString();
			HS_Util.debug(e.toString(), "ERROR", codeContext);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
			// HS_Util.logException(e.toString(), logtxt, codeContext);
			// retVal = logtxt.toString();
			retVal = "Error";
		}
		return retVal;
	}

	/**
	 * PRIVATE getElementMapList(Document srcDoc, String fmap) Within an Inspection there are a series of multivalue fields that are used to capture a series of
	 * related values, where the element values correspond to other values at the same index. Examples include Sections, Violations, etc. These "element lists"
	 * must be converted to child nodes within the Inspection JSON object, similar to the way Inspection docs are child nodes within a Facility JSON object. The
	 * difference is that these are not individual docs, these are values within related fields. This function returns an ArrayList of HashMap objects, each one
	 * representing the related values from the fields indicated in the Field Map. "Related" meaning the values that are at the same index number. The
	 * individual values are returned with the desired data type as well (TEXT, NUMBER, DATE, or FORMULA), as indicated in the Field Map for this particular
	 * element type. PARAMS -- srcDoc (Document) == the document containing the element field -- fmap (String) == the name of the field map to use when creating
	 * the element HashMaps
	 */
	/*
	 * ************* UPDATE LOG *************** 27 JAN 2015 (1.51) - Updated debugging and error handling process/code - Added IDs for each element, to assist
	 * analysis 19 FEB 2015 (1.52) - Fixed bug where the same values were being returned repeatedly - Added in error logging with more detail
	 * *****************************************
	 */
	@SuppressWarnings("unchecked")
	private List<Map<String, Object>> getElementMapList(Document srcDoc, String fmap) {
		final String version = "1.60, 4 SEP 2015";
		final String method = "QueryFacilities.getElementMapList";
		final String codeContext = method + " [" + version + "]";
		// final boolean localDebug = ("Yes".equals(HS_Util.getAppSettingString("debug_HSTDownload")));
		// final boolean rockDebug = false;
		final String docID = srcDoc.getUniversalID();
		// final Map<String, Object> eleMap = new HashMap<String, Object>();
		// String logtxt = "";
		final List<Map<String, Object>> eleMaps = new ArrayList<Map<String, Object>>();
		Map<String, String> eleFldMap = null;
		Integer eleCount;

		try {
			eleFldMap = getFieldMap(fmap);
			if ( eleFldMap.containsKey("FAIL-CONTEXT-1") ) {
				return (List<Map<String, Object>>) eleFldMap;
			}
			eleCount = 0;
			final Iterator<String> fnames = eleFldMap.values().iterator();
			/*
			 * ---- Element Map - Workflow ---- Get Element field map, contains the fields containing the Element information Create an Element Map with
			 * arrayList as value Do a for() loop, and pull the value of each field at the index of the loop Add these fields to an Element HashMap, one for
			 * each index value Add each of these Element hashMaps to the ArrayList Elements map Return the element map list
			 */

			while (fnames.hasNext()) {
				final String fname = fnames.next();
				if ( srcDoc.hasItem(fname) ) {
					final Item curItem = srcDoc.getFirstItem(fname);
					// final String curVal = curItem.getText();
					if ( curItem.getValues().isEmpty() == false ) {
						eleCount = curItem.getValues().size();
						break;
					}
				}
			}
			for (int z = 0; z < eleCount; z++) {
				final Map<String, Object> eleDocMap = new HashMap<String, Object>();
				for (final Map.Entry<String, String> entry : eleFldMap.entrySet()) {
					final String mapkey = entry.getKey().toString();
					final String[] hsfldtype = mapkey.split("\\^");
					final String hsfld = hsfldtype[0];
					final String hstype = hsfldtype[1];
					final String igfld = entry.getValue();
					// if the field map entry is a MAPINFO item, set it
					// accordingly;
					// otherwise process it as normal
					if ( hstype.equals("MAPINFO") ) {
						eleDocMap.put(hsfld, igfld);
					} else {
						final Object eleFldVal = getMappedFieldValue(srcDoc, mapkey, z);
						eleDocMap.put(igfld, eleFldVal);
					}

				}
				final String eleID = docID + "-" + fmap + String.format("%04d", z + 1);
				eleDocMap.put("ElementID", eleID);
				eleMaps.add(eleDocMap);
			}
		} catch (final Exception e) {
			/*
			 * RestUtil.writeToAgentLog(String message, String description) for handling the logging.. The message is the detail message to log ("\n" for new
			 * lines)... That could be the output JSON and the INPUT parms. Description is where it is called from (Class.Method).
			 */
			logmsgs += " \n\n ERROR IN " + codeContext + ": \n " + e.toString();
			// RestUtil.writeToAgentLog(logtxt, codeContext);
			HS_Util.debug(e.toString(), "ERROR", codeContext);
			if ( HS_Util.isDebugServer() ) {
				e.printStackTrace();
			}
			// HS_Util.logException(e.toString(), " \n ERROR in " + codeContext
			// + ": \n " + e.toString(), codeContext);
			final Map<String, Object> errMap = new HashMap<String, Object>();
			errMap.put("FAIL-ERROR2-", e.toString());
			errMap.put("FAIL-LOG-2", " ERROR in " + codeContext);
			errMap.put("FAIL-CONTEXT-2", codeContext);
			eleMaps.add(errMap);
		}
		return eleMaps;
	}

	/**
	 * PRIVATE getDocumentMap(unid, fmap) Grabs the document identified by the unid QueryString, then creates a HashMap object containing the items specified in
	 * the Field Map identified by the fmap QueryString PARAMS -- unid (String) == the DocumentUniqueID of the desired document -- fmap (String) == name of the
	 * Field Map to use to create the document HashMap object
	 */
	/*
	 * ************* UPDATE LOG *************** 24 JAN 2015 (1.51) - Switched to using HS_Util.getDocumentByUNID for better error handling - Enhanced error
	 * trapping overall, including using HS_Util.logException() - Added a new variable, curObj, which will be set to the current object being used. This is so
	 * that it can be handed to logException() for enhanced log/err/debug analysis. 27 JAN 2015 (1.52) - Reworked & streamlined debug process to provide info
	 * using less code - Added new data type, MapInfo, to provide Field Map diagnostic info 28 JAN 2015 (1.60) - LARGE rewrite/rework - not in code amount, but
	 * in how it works; now it uses getMappedFieldValue() to retrieve ALL mapped field values. This was done so that there is a single, centralized way to
	 * retrieve all mapped values from a Field Map. Before there was quite a bit of duplicated code between here and getMappedFieldValue - now it's in one
	 * place. - Incremented from 1.5x to 1.6x to coincide with the major code change 19 FEB 2015 (1.61) - Added in logging and better error handling
	 * *****************************************
	 */
	private Map<String, Object> getDocumentMap(Database eidb, String unid, String fmap) {
		final String version = "1.81, 14 Dec 2016";
		final String method = "QueryFacilities.getDocumentMap";
		final String codeContext = method + " [" + version + "]";
		final boolean localDebug = false;
		final boolean rockDebug = false;
		final Map<String, Object> docMap = new HashMap<String, Object>();
		// Object curObj = null;
		// String logtxt = "";
		// final Integer eIdx = 0;
		try {
			// get unid and fmap queryString vals
			if ( "".equals(unid) ) {
				unid = JSFUtil.getParamValue("unid");
				if ( "".equals(unid) ) {
					throw (new Exception("UNID not found"));
				}
			}
			// logtxt = "UNID[" + unid + "]";
			if ( "".equals(fmap) ) {
				fmap = JSFUtil.getParamValue("fmap");
				// don't need to throw an exception if the fmap param is not
				// found, as getFieldMap() has a "default" value
			}
			if ( localDebug ) docMap.put("$debug_unid", unid);
			if ( localDebug ) docMap.put("$debug_fmap", fmap);
			// logtxt += " \n FLDMAP[" + fmap + "]";
			// get the EI Manager db (ehs.nsf) and retrieve the desired doc
			// (srcdoc)

			final Document srcdoc = HS_Util.getDocumentByUNID(eidb, unid, false);
			if ( srcdoc == null ) {
				// curObj = srcdoc;
				throw (new Exception("Source Document [unid: " + unid + "] not found."));
			}

			for (final Map.Entry<String, String> entry : getFieldMap(fmap).entrySet()) {
				final String mapkey = entry.getKey().toString();
				/*
				 * the "raw" value of the hashmap KEY contains one of the following: -- EI_FLD_NAME^TYPE == the source (EI) field name (EI_FLD_NAME) and the
				 * data type (TEXT, DATE, NUMBER, or FORMULA) -- FORMULA^FORMULA == @formula, computed agains source doc; the data type is FORMULA to indicate
				 * it is a formula, and not a source (EI) field name
				 */
				final String[] hsfldtype = mapkey.split("\\^");
				final String hsfld = hsfldtype[0];
				final String hstype = hsfldtype[1];
				final String igfld = entry.getValue();
				if ( rockDebug ) {
					docMap.put("DEBUG-MapInfo(" + igfld + ")", "KEY:" + mapkey);
				}
				// curObj = entry;
				if ( hstype.toUpperCase().equals("MAPINFO") ) {
					// TYPE: MapInfo - special data type to get Field Map
					// diagnostic info
					docMap.put(hsfld, igfld);
				} else {
					// index num of -1 tells getMappedFieldValue to return all
					// item values
					final Object fldVal = getMappedFieldValue(srcdoc, mapkey, -1);
					// curObj = fldVal;
					docMap.put(igfld, fldVal);
				}
			}
		} catch (final Exception e) {
			/*
			 * RestUtil.writeToAgentLog(String message, String description) for handling the logging.. The message is the detail message to log ("\n" for new
			 * lines)... That could be the output JSON and the INPUT parms. Description is where it is called from (Class.Method).
			 */
			logmsgs += " \n ERROR IN " + codeContext + ": \n " + e.toString();
			// RestUtil.writeToAgentLog(logtxt, codeContext);
			HS_Util.debug(e.toString(), "ERROR", codeContext);
			if ( HS_Util.isDebugServer() ) {
				e.printStackTrace();
			}
			docMap.put("FAIL-ERROR-3", e.toString());
			docMap.put("FAIL-LOG-3", " ERROR in " + codeContext);
			docMap.put("FAIL-CONTEXT-3", codeContext);
			// docMap.add(errMap);
		}
		docMap.put("docUNID", unid);
		if ( localDebug ) docMap.put("$CODE_" + method, version);
		return docMap;
	}

	/**
	 * PRIVATE getDocumentJson(unid, fmap) Version: 1.53, 27 JAN 2015 Grabs the document identified by the unid QueryString, then creates a serialized JSON
	 * object containing the items specified in the Field Map identified by the fmap QueryString PARAMS -- unid (String) == the DocumentUniqueID of the desired
	 * document -- fmap (String) == name of the Field Map to use to create the document JSON object
	 */
	/*
	 * ************* UPDATE LOG *************** 12 JAN 2015 (1.50) - added support for Field Map Last Modified 18 JAN 2015 (1.51) - Rewrote to use
	 * getDocumentMap() internally to retrieve the document info as a HashMap, then simply convert it to a JSON object using a single call to
	 * JsonJavaObject.putAll() 24 JAN 2015 (1.52) - Switched to using HS_Util.getDocumentByUNID for better error handling - Enhanced error trapping overall,
	 * including using HS_Util.logException() - Added a new variable, curObj, which will be set to the current object being used. This is so that it can be
	 * handed to logException() for enhanced log/err/debug analysis. 27 JAN 2015 (1.53) - MAJOR streamlining of code; since the detailed error trapping for the
	 * procurement of doc info is performed in the getDocumentMap() method, which is called by this method to retrieve the HashMap of doc info, which is then
	 * converted to a JSON object using a single call - Reworked & streamlined code identification and debug process 19 FEB 2015 (1.54) - Added in better
	 * logging and error handling ****************************************
	 */
	@SuppressWarnings("unused")
	private String getDocumentJson(Database eidb, String unid, String fmap) {
		final String version = "1.60, 4 SEP 2015";
		final String method = "QueryFacilities.getDocumentJson";
		final String codeContext = method + " [" + version + "]";
		final boolean localDebug = false;
		// final boolean rockDebug = false;
		final JsonJavaObject docJson = new JsonJavaObject(); // the final
		// Facility JSON
		// obj
		final Object curObj = null;
		String debugUnid = "";
		String debugFmap = "";
		// String logtxt = "";

		try {
			if ( "".equals(unid) ) {
				unid = JSFUtil.getParamValue("unid");
				if ( "".equals(unid) ) {
					throw (new Exception("UNID not found"));
				}
			}
			if ( "".equals(fmap) ) {
				fmap = JSFUtil.getParamValue("fmap");
				// don't need to throw an exception if the fmap param is not
				// found, as getFieldMap() has a "default" value
			}
			// logtxt = "UNID[" + unid + "] \n FLDMAP[" + fmap + "]";
			debugUnid = "unid:[" + unid + "]";
			debugFmap = "fmap:[" + fmap + "]";
			if ( localDebug ) {
				HS_Util.debug(debugUnid, codeContext);
				HS_Util.debug(debugFmap, codeContext);
			}

			final Map<String, Object> docMap = getDocumentMap(eidb, unid, fmap);
			docMap.put("$CODE_" + method, version);
			docJson.putAll(docMap);
		} catch (final Exception e) {
			/*
			 * RestUtil.writeToAgentLog(String message, String description) for handling the logging.. The message is the detail message to log ("\n" for new
			 * lines)... That could be the output JSON and the INPUT parms. Description is where it is called from (Class.Method).
			 */
			logmsgs += " \n ERROR in " + codeContext + ": \n " + e.toString();
			// RestUtil.writeToAgentLog(logtxt, codeContext);
			HS_Util.debug(e.toString(), "ERROR", codeContext);
			if ( HS_Util.isDebugServer() ) {
				e.printStackTrace();
			}
			HS_Util.logException(e.toString(), curObj, codeContext);
			docJson.putString("FAIL-ERROR-4", e.toString());
			docJson.putString("FAIL-CONTEXT-4", codeContext);
		}
		return docJson.toString();
	}

	/**
	 * PUBLIC getFacilityInfo() Returns all of the information related to the Facility identified by the UNID (unid) QueryString param. In addition to the
	 * information in the Facility document itself, it also returns"child" docs and other related nodes of information related to the Facility being processed.
	 * PARAMS The params for this method are passed via the QueryString calling the REST service. These are: -- unid (String) == the DocumentUniqueID of the
	 * desired Facility document (required) -- fmap (String) == the name of the Field Map to use for the Facility document; if not provided, the default value
	 * "Facility" is used
	 */
	/*
	 * ********** PROCESS BACKGROUND *********** For each Facility there may be 0-N number of related docs that are organized in a parent/child object
	 * hierarchy. This hierarchy, or noded tree, of information must be gathered and returned as a serialized JSON object, maintaining this hierarchical
	 * organization.In order to make this process as simple and clean as possible, the info for each individual node of information is gathered as a series of
	 * HashMaps. Each HashMap node is then arranged into a single HashMap structure. Finally, this structure is converted into a JSON object via a single method
	 * call, then it is serialized and returned as a string. ******* STRUCTURE & ORGANIZATION ******** The current structure of the Facility JSON object is as
	 * follows: -- Facility (document) -- Inspections (response document) -- Time Reports (response document) -- Sections (multivalue fields) -- Violations
	 * (multivalue fields) -- Food Temps (multivalue fields) -- Equipment Temps (multivalue fields) -- Ware Wash Temps (multivalue fields) -- Managers
	 * (multivalue fields)
	 * 
	 * ************* UPDATE LOG **************** 19 JAN 2015 (1.60) - added additional node arrays, including Time Reports, Temps, etc. 24 JAN 2015 (1.61) -
	 * Switched to using HS_Util.getDocumentByUNID for better error handling - Enhanced error trapping overall, including using HS_Util.logException() - Added a
	 * new variable, curObj, which will be set to the current object being used. This is so that it can be handed to logException() for enhanced log/err/debug
	 * analysis. 27 JAN 2015 (1.62) - Revised & streamlined debug and error trapping code; also changed to be consistent with other methods used for the
	 * Facility Info overall process 19 FEB 2015 (1.63) - Added in better logging and error handling 20 FEB 2015 (1.64) - Added in elapsed timer and summary log
	 * entry 9 March 2015 (1.65) - Newbs - Added more info to the log and setup get field map to not do its own logging.
	 * ****************************************
	 */
	public String getFacilityInfo() {
		final String version = "1.81, 14 Dec 2016";
		final String method = "QueryFacilities.getFacilityInfo";
		final String codeContext = method + " [" + version + "]";
		final boolean localDebug = ("Yes".equals(HS_Util.getAppSettingString("debug_HSTDownload")));
		boolean thereIsAnError = false;
		// final JsonJavaObject facJson = new JsonJavaObject(); // the final
		StringWriter sw = new StringWriter();
		HS_JsonWriter facJson = new HS_JsonWriter(sw, false);

		// Facility JSON
		// obj
		final String inspViewName = "XPagesEmbeddedInspections";
		final String inspMapName = "Inspection"; // the Field Map to use for
		// child ("insp") docs
		final String timeViewName = "TimeEmbedded";
		final String timeMapName = "TimeReport";
		// Object curObj = null;
		// String logtxt = "";
		final Date startTime = new Date();
		final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String summaryInfo = "\nCODE CONTEXT: " + codeContext + "\nPROCESSED_ON [" + df.format(startTime) + "]";

		try {
			// get unid and fmap queryString vals
			final String unid = JSFUtil.getParamValue("unid");
			String fmap = JSFUtil.getParamValue("fmap");

			if ( "".equals(unid) ) {
				throw (new Exception("UNID not found"));
			}
			// if fmap is not provided, use the Facility Field Map as default
			if ( "".equals(fmap) ) {
				fmap = "Facility";
			}
			if ( localDebug ) {
				logmsgs += "\nUNID[" + unid + "] \nFLDMAP[" + fmap + "]";
				summaryInfo += (" \n " + logmsgs);
			}
			// get the Inspection docs and create the List of their HashMaps
			final Database eidb = new HS_database().getdb("eiRoot");
			if ( eidb.isOpen() == false ) {
				// curObj = eidb;
				throw (new Exception("Unable to open database identified by Profile: eiRoot"));
			}

			final Map<String, Object> facDocMap = getDocumentMap(eidb, unid, fmap);

			// this list holds all of the "child" doc hashmaps
			// curObj = facDocMap;
			if ( facDocMap.containsKey("FAIL-CONTEXT-5") ) {
				// throw (new Exception("Unable to retrieve Facility doc [unid:" + unid + "]"));
			} else {
				summaryInfo += ("\nFACILITY NAME:[" + facDocMap.get("name").toString() + "]");

				final List<Map<String, Object>> inspMaps = new ArrayList<Map<String, Object>>();

				final View inspView = eidb.getView(inspViewName);
				if ( inspView == null ) {
					throw (new Exception("Unable to retrieve view [" + inspViewName + "]"));
				}
				inspView.setAutoUpdate(false);
				// get the Time Tracking view
				final View timeView = eidb.getView(timeViewName);
				if ( timeView == null ) {
					throw (new Exception("Unable to retrieve view [" + timeViewName + "]"));
				}
				timeView.setAutoUpdate(false);

				// cycle through all of the child ("insp") docs and add them to the
				// HashMap List
				for (final ViewEntry inspEntry : inspView.getAllEntriesByKey(unid, true)) {
					final String inspUNID = inspEntry.getUniversalID();
					final Document inspDoc = eidb.getDocumentByUNID(inspUNID);
					final Map<String, Object> inspDocMap = getDocumentMap(eidb, inspUNID, inspMapName);
					// ****** Time Reports START *******
					final String inspDocID = inspDocMap.get("documentID").toString();
					final List<Map<String, Object>> timeMaps = new ArrayList<Map<String, Object>>();
					for (final ViewEntry timeEntry : timeView.getAllEntriesByKey(inspDocID, true)) {
						final String timeUNID = timeEntry.getUniversalID();
						final Map<String, Object> timeDocMap = getDocumentMap(eidb, timeUNID, timeMapName);
						timeMaps.add(timeDocMap);
					}
					inspDocMap.put("timeReports", timeMaps);
					// ************* END ***************
					// ******* Elements START **********
					final List<Map<String, Object>> sectMaps = getElementMapList(inspDoc, "Section");
					inspDocMap.put("sections", sectMaps);
					final List<Map<String, Object>> violMaps = getElementMapList(inspDoc, "Violation");
					inspDocMap.put("violations", violMaps);
					final List<Map<String, Object>> equipMaps = getElementMapList(inspDoc, "EquipTemp");
					inspDocMap.put("equipTemps", equipMaps);
					final List<Map<String, Object>> foodMaps = getElementMapList(inspDoc, "FoodTemp");
					inspDocMap.put("foodTemps", foodMaps);
					final List<Map<String, Object>> wareWashMaps = getElementMapList(inspDoc, "WareWashTemp");
					inspDocMap.put("wareWashTemps", wareWashMaps);
					final List<Map<String, Object>> mgrMaps = getElementMapList(inspDoc, "Manager");
					inspDocMap.put("managers", mgrMaps);
					// ************* END ***************
					inspMaps.add(inspDocMap);
				}
				facDocMap.put("inspections", inspMaps);
			}

			facDocMap.put("$CODE_" + method, version);
			// convert the entire HashMap/List structure to JSON in one call
			// (KEWL!)
			// facJson.putAll(facDocMap);
			facJson.startObject();
			facJson.addMap(facDocMap);
			facJson.endObject();
		} catch (final Exception e) {
			/*
			 * RestUtil.writeToAgentLog(String message, String description) for handling the logging.. The message is the detail message to log ("\n" for new
			 * lines)... That could be the output JSON and the INPUT parms. Description is where it is called from (Class.Method).
			 */
			thereIsAnError = true;
			summaryInfo += " \n\n********************* ERROR ***********************************\n" + e.toString();
			// RestUtil.writeToAgentLog(summaryInfo + "\n\n" + logmsgs, method + " ERROR");
			HS_Util.debug(e.toString(), "ERROR", method);
			if ( HS_Util.isDebugServer() ) {
				e.printStackTrace();
			}
			try {
				facJson.startObject();
				facJson.addProperty("Error", e.toString() + " in " + codeContext);
				// HS_Util.logException(e.toString(), curObj, codeContext);
				facJson.addProperty("FAIL-ERROR-6", e.toString());
				facJson.addProperty("FAIL-CONTEXT-6", codeContext);
				facJson.endObject();
			} catch (IOException e1) {
				// ignoring this for now
			}
		}
		summaryInfo += "\nOUTPUT SIZE:[" + sw.toString().length() + "]";

		final String elapsed = "ELAPSED_TIME:[" + (new Date().getTime() - startTime.getTime()) + "ms]";
		summaryInfo += (" \n" + elapsed);
		// RestUtil.writeToAgentLog(summaryInfo, codeContext);
		if ( "Yes".equals(HS_Util.getAppSettingString("debug_Search")) || "Yes".equals(HS_Util.getAppSettingString("debug_HSTDownload")) || thereIsAnError ) {
			if ( localDebug ) {
				RestUtil.writeToAgentLog(summaryInfo + "\n\n" + RestUtil.getDebugProperties() + "\n\n" + logmsgs + "\n\nRESULTS:\n" + facJson.toString(),
						method);
			} else {
				RestUtil.writeToAgentLog(summaryInfo + "\n\n" + RestUtil.getDebugProperties(), method);
			}
		}
		// return facJson.toString();
		return sw.toString();
	}

	/*
	 * PUBLIC rockTestJson() Generic test harness that Rock can use to test various functions and methods that return JSON objects, such as the
	 * getDocumentJson() method above.
	 */
	public String rockTestJson() throws Exception {
		final String unidParam = JSFUtil.getParamValue("unid");
		final String fmapParam = JSFUtil.getParamValue("fmap");

		final String codeContext = "QueryFacilities.rockTest_getFieldMap";
		// final JsonJavaFactory jFactory = JsonJavaFactory.instanceEx;

		// final List<Map<String, String>> myMaps = new ArrayList();
		// final Map<String, String> baby1 = new HashMap<String, String>();
		// final Map<String, String> baby2 = new HashMap<String, String>();
		// final Map<String, Object> kids = new HashMap<String, Object>();
		final JsonJavaObject jsonObj = new JsonJavaObject();

		// get the Inspection docs and create the List of their HashMaps
		final Database eidb = new HS_database().getdb("eiRoot");
		if ( eidb.isOpen() == false ) {
			// curObj = eidb;
			throw (new Exception("Unable to open database identified by Profile: eiRoot"));
		}
		final Map<String, Object> fldMap = getDocumentMap(eidb, unidParam, fmapParam);

		try {

			jsonObj.putAll(fldMap);
		}

		catch (final Exception e) {
			HS_Util.debug(e.toString(), "Error", codeContext);
			if ( HS_Util.isDebugServer() ) {
				e.printStackTrace();
			}

			jsonObj.put("Error", e.toString());
		}

		return jsonObj.toString();
	}

}
