package com.healthspace.rest;

import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.Map.Entry;

import org.openntf.domino.Database;
import org.openntf.domino.DateTime;
import org.openntf.domino.Session;
import org.openntf.domino.View;
import org.openntf.domino.ViewEntry;
import org.openntf.domino.ViewNavigator;

import com.healthspace.general.HS_Util;
import com.healthspace.general.HS_database;
import com.healthspace.tools.JSFUtil;
import com.ibm.domino.services.util.JsonWriter;

//import com.ibm.domino.xsp.module.nsf.NotesContext;
//import com.ibm.xsp.extlib.util.ExtLibUtil;

public class CalendarAll {
	private Long	unixStart;
	private Long	unixEnd;
	private String	eho;

	public CalendarAll() {
		final boolean localDebug = false;
		final String msgContext = "CalendarAll.constructor";
		try {
			String startParam = JSFUtil.getParamValue("start");
			String endParam = JSFUtil.getParamValue("end");
			if ( startParam != null && !startParam.isEmpty() ) {
				setUnixStart(Long.parseLong(startParam));
			}
			if ( endParam != null && !endParam.isEmpty() ) {
				setUnixEnd(Long.parseLong(endParam));
			}
			if ( localDebug ) HS_Util.debug("startParam:[" + startParam + "] " + "unixStart:[" + unixStart + "] " + "endParam:[" + endParam + "] " + "unixEnd:[" + unixEnd + "] ", "debug", msgContext);
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}
	}

	public String writeJson() {
		return writeJson("-All-");
	}

	public String writeJson(String inpeho) {
		final boolean localDebug = false;
		final String msgContext = "CalendarAll.writeJson";

		StringWriter sw = new StringWriter();
		JsonWriter jw = new JsonWriter(sw, false);
		try {
			this.setEho(inpeho);
			if ( localDebug ) HS_Util.debug("starting eho:[" + inpeho + "]", "debug", msgContext);
			jw.startArray();
			// if (localDebug) HS_Util.debug("calling writeEntries", "debug", msgContext);
			writeEntries(jw);
			// if (localDebug) HS_Util.debug("ending", "debug", msgContext);
			jw.endArray();
			if ( localDebug ) HS_Util.debug("done", "debug", msgContext);
		} catch (IOException e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}
		return sw.toString();
	}

	private void writeEntries(JsonWriter jw) {
		final boolean localDebug = false;
		final String msgContext = "CalendarAll.writeEntries";
		try {
			List<Map<String, String>> entryList = getEntryList();
			if ( localDebug ) HS_Util.debug("got [" + entryList.size() + "] entries.", "debug", msgContext);
			for (Map<String, String> valMap : entryList) {
				jw.startArrayItem();
				jw.startObject();
				for (Entry<String, String> valEntry : valMap.entrySet()) {
					jw.startProperty(valEntry.getKey());
					jw.outStringLiteral(valEntry.getValue());
					jw.endProperty();
				}
				jw.endObject();
				jw.endArrayItem();
			}
		} catch (IOException e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private List<Map<String, String>> getEntryList() {
		final boolean localDebug = HS_Util.isDebugServer() || HS_Util.isDebugger();
		final String msgContext = "CalendarAll.getEntryList";
		List<Map<String, String>> entryList = new ArrayList<Map<String, String>>();
		int howManyPasses = 0;
		try {
			if ( localDebug ) System.out.print("Starting " + msgContext + "[" + this.getClass().getCanonicalName() + "]");
			// Session sess = NotesContext.getCurrent().getCurrentSession();
			Session sess = HS_Util.getSession();
			HS_database hsdb = new HS_database();
			Database eidb = hsdb.getdb("eiRoot");
			// View entryView = ExtLibUtil.getCurrentDatabase().getView("Events");
			View entryView = eidb.getView("XpagesCalendar");
			if ( entryView == null ) {
				System.out.print(msgContext + ": XpagesCalendar view NOT OPENNED in eidb:[" + eidb.getServer() + "!!" + eidb.getFilePath() + "]");
				return entryList;
			} else {
				if ( localDebug ) {
					HS_Util.debug("XpagesCalendar view opened in eidb:[" + eidb.getServer() + "!!" + eidb.getFilePath() + "]", "debug", msgContext);
					System.out.print(msgContext + ": XpagesCalendar view opened in eidb:[" + eidb.getServer() + "!!" + eidb.getFilePath() + "]");
				}
			}
			entryView.setAutoUpdate(false);

			ViewNavigator viewNav = entryView.createViewNav();
			Date startDate = null;
			DateTime startDateTime = sess.createDateTime("Today");
			startDateTime.setNow();
			if ( getUnixStart() != null ) {
				startDate = new Date(getUnixStart() * 1000);
				startDateTime = sess.createDateTime(startDate);
			}
			Date endDate = null;
			DateTime endDateTime = sess.createDateTime("Today");
			endDateTime.setNow();
			if ( getUnixEnd() != null ) {
				endDate = new Date(getUnixEnd() * 1000);
				endDateTime = sess.createDateTime(endDate);
				endDateTime.adjustDay(1);
			}
			if ( localDebug ) {
				HS_Util.debug("startDateTime:[" + startDateTime.getDateOnly() + "] endDateTime:[" + endDateTime.getDateOnly() + "]", "debug", msgContext);
				System.out.print(msgContext + ": startDateTime:[" + startDateTime.getDateOnly() + "] endDateTime:[" + endDateTime.getDateOnly() + "]");
			}
			ViewEntry startEnt = getStartEntry(entryView, startDateTime);
			ViewEntry endEnt = null;
			Vector colValues = null;
			if ( startEnt == null ) {
				System.out.print(msgContext + ": startEnt came back as null the first time.");
				startEnt = viewNav.getFirst();
				if ( startEnt != null ) {
					// TODO THIS line throws the error: java.util.Date incompatible with org.openntf.domino.DateTime
					startDateTime = sess.createDateTime(((DateTime) startEnt.getColumnValues().get(0)).toJavaDate());
					System.out.print(msgContext + ": startEnt came back as NOT null the second time. startDateTime:[" + startDateTime.toString() + "]");
				} else {
					System.out.print(msgContext + ": startEnt came back as null the second time.");
				}
				endEnt = viewNav.getLast();
				endDateTime = sess.createDateTime(((DateTime) endEnt.getColumnValues().get(0)).toJavaDate());
			} else {
				System.out.print(msgContext + ": startEnt came back as NOT null the first time.");
			}

			ViewEntry viewEnt = startEnt;
			DateTime entStartDateTime = null;
			DateTime entEndDateTime = null;
			Object esdt = null;
			String esdtClass = "";
			Object eedt = null;
			String eedtClass = "";
			String title = "";
			System.out.print(msgContext + ": Beginning while loop.");
			while (viewEnt != null) {
				try {
					howManyPasses++;
					if ( howManyPasses > 2000 ) throw (new Exception(msgContext + ".while entered too many times!"));
					colValues = viewEnt.getColumnValues();
					title = colValues.get(4).toString();
					// newbs - 04Sep14 - Fixed to work with domino.DateTime or java.Util.date
					esdt = colValues.get(0);
					esdtClass = esdt.getClass().getName();
					eedt = colValues.get(1);
					eedtClass = eedt.getClass().getName();

					if ( (esdtClass.indexOf("domino.DateTime") > -1) || (esdtClass.indexOf("domino.impl.DateTime") > -1) ) {
						entStartDateTime = sess.createDateTime(((DateTime) esdt).toJavaDate());
						if ( localDebug && entryList.size() < 2 ) {
							HS_Util.debug("got entStartDateTime from esdt as " + esdtClass + ":[" + entStartDateTime.getDateOnly() + "]", "debug", msgContext);
							System.out.print(msgContext + ": got entStartDateTime from esdt as " + esdtClass + ":[" + entStartDateTime.getDateOnly() + "]");
						}
					} else if ( esdtClass.indexOf("util.Date") > -1 ) {
						entStartDateTime = sess.createDateTime((Date) esdt);
						if ( localDebug && entryList.size() < 2 ) {
							HS_Util.debug("got entStartDateTime from esdt as " + esdtClass + ":[" + entStartDateTime.getDateOnly() + "]", "debug", msgContext);
							System.out.print(msgContext + ": got entStartDateTime from esdt as " + esdtClass + ":[" + entStartDateTime.getDateOnly() + "]");
						}
					} else {
						entStartDateTime = sess.createDateTime("Today");
						entStartDateTime.adjustYear(50); // put it out of time...
						if ( localDebug && entryList.size() < 2 ) {
							HS_Util.debug("esdt is not a recognized type:[ " + esdtClass + "] entStartDateTime:[" + entStartDateTime.toString() + "]", "warn", msgContext);
							System.out.print(msgContext + ": esdt is not a recognized type:[ " + esdtClass + "] entStartDateTime:[" + entStartDateTime.toString() + "]");
						}
						throw (new Exception("esdt is not a recognized type:[ " + esdtClass + "] entStartDateTime:[" + entStartDateTime.toString() + "]"));
					}
					if ( (eedtClass.indexOf("domino.DateTime") > -1) || (eedtClass.indexOf("domino.impl.DateTime") > -1) ) {
						entEndDateTime = sess.createDateTime(((DateTime) eedt).toJavaDate());
					} else if ( eedtClass.indexOf("util.Date") > -1 ) {
						entEndDateTime = sess.createDateTime(((Date) eedt));
					} else {
						entEndDateTime = sess.createDateTime("Today");
						entEndDateTime.adjustYear(50); // put it out of time...
						if ( localDebug && entryList.size() < 2 ) {
							HS_Util.debug("eedt is not a recognized type:[ " + eedtClass + "] entEndDateTime:[" + entEndDateTime.toString() + "]", "warn", msgContext);
							System.out.print(msgContext + ": eedt is not a recognized type:[ " + eedtClass + "] entEndDateTime:[" + entEndDateTime.toString() + "]");
						}
					}
				} catch (Exception e) {
					HS_Util.debug("eedt:[" + eedt.toString() + "] [" + eedt.getClass().getName() + "]", "warn", msgContext);
					HS_Util.debug("esdt:[" + esdt.toString() + "] [" + esdt.getClass().getName() + "]", "warn", msgContext);
					HS_Util.debug("On entry:[" + title + "] got error:" + e.toString(), "error", msgContext);
					System.out.print(msgContext + ": eedt:[" + eedt.toString() + "] [" + eedt.getClass().getName() + "]");
					System.out.print(msgContext + ": esdt:[" + esdt.toString() + "] [" + esdt.getClass().getName() + "]");
					System.out.print(msgContext + ": On entry:[" + title + "] got error:" + e.toString());
					entStartDateTime = sess.createDateTime("Today");
					entEndDateTime = sess.createDateTime("Today");
					entStartDateTime.adjustYear(50); // put it out of time...
					entEndDateTime.adjustYear(50); // put it out of time...
				}
				if ( entStartDateTime.toJavaDate().getTime() < endDateTime.toJavaDate().getTime() ) {
					if ( isEho(colValues.get(3).toString()) ) {
						Map<String, String> valueMap = new HashMap<String, String>();
						if ( !colValues.get(2).toString().equals("Some Random Event") ) {
							valueMap.put("color", "red");
						}
						valueMap.put("start", getFormattedDate(entStartDateTime.toJavaDate()));
						valueMap.put("end", getFormattedDate(entEndDateTime.toJavaDate()));
						valueMap.put("title", title);

						valueMap.put("color", colValues.get(2).toString());
						valueMap.put("eho", colValues.get(3).toString());
						valueMap.put("facility", colValues.get(4).toString());
						valueMap.put("reinspection", colValues.get(5).toString());
						valueMap.put("lastinspection", colValues.get(6).toString());
						valueMap.put("unid", colValues.get(7).toString());
						valueMap.put("url", JSFUtil.getDbUrl() + "/formFacility.xsp?documentId=" + colValues.get(7).toString());
						entryList.add(valueMap);
					}
					viewEnt = viewNav.getNext(viewEnt);
				} else {
					viewEnt = null;
				}
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			System.out.print(msgContext + ": ERROR: " + e.toString());
			if ( HS_Util.isDebugServer() ) e.printStackTrace();
		}
		return entryList;
	}

	private boolean isEho(String entEho) {
		final boolean localDebug = false;
		final String msgContext = "CalendarAll.getEntryList";
		if ( localDebug ) HS_Util.debug("entEho:[" + entEho + "] this.getEho():[" + this.getEho() + "]", "debug", msgContext);
		if ( "".equals(this.getEho()) ) return true;
		if ( "-All-".equals(this.getEho()) ) return true;
		if ( entEho.equals(this.getEho()) ) return true;
		return false;
	}

	private String getFormattedDate(Date formatDate) {
		String returnDate = "";
		if ( formatDate != null ) {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			returnDate = formatter.format(formatDate);
		}
		return returnDate;
	}

	private ViewEntry getStartEntry(View entryView, DateTime startDateTime) {
		ViewEntry ent = null;
		int passes = 0;
		DateTime wrkDateTime = startDateTime;
		try {
			ent = entryView.getFirstEntryByKey(wrkDateTime);
			if ( ent == null ) {
				while (ent == null && passes < 100) {
					passes++;
					wrkDateTime.adjustDay(1);
					ent = entryView.getFirstEntryByKey(wrkDateTime);
				}
			}
		} catch (Exception e) {
			ent = null;
			HS_Util.debug(e.toString(), "error", this.getClass().getName() + ".getStartEntry");
			// if(HS_Util.isDebugServer()) e.printStackTrace();
		}
		return ent;
	}

	public Long getUnixStart() {
		return unixStart;
	}

	public void setUnixStart(Long unixStart) {
		this.unixStart = unixStart;
	}

	public Long getUnixEnd() {
		return unixEnd;
	}

	public void setUnixEnd(Long unixEnd) {
		this.unixEnd = unixEnd;
	}

	public String getEho() {
		return eho;
	}

	public void setEho(String eho) {
		this.eho = eho;
	}

}
