package com.healthspace.rest;

import java.io.IOException;
import java.util.Map;

import com.healthspace.general.Inspection;

public class PostTobaccoInfo {

	public PostTobaccoInfo() {
	}

	public static String processTobaccoInfo(HS_JsonWriter jw, Map<String, Object> tobaccoMap, Inspection inspection) throws IOException {
		final String msgContext = "PostTobaccoInfo.processTobaccoInfo";
		// {"tabIDTobacco_InformationID":"ff70d01b-50bc-41a8-996a-2619638335e5"
		// "inspectionID":"e8b38930-d77e-4159-a4a2-23d6edd517c0"
		// "tobaccoSold":"Yes"
		// "eCigsVapor":"yes"
		// "tobaccoSign":"yes"
		// "tobaccoProduct":"Yes"
		// "vendingMachine":"Yes"
		// "noIllegal":"Yes"
		// "tobaccoParaphernalia":"Yes"
		// "synced":"0"
		// "modifiedBy":"Joe Willmott"
		// "dateadd":"2016-5-23 10:06:52.304"
		// "datelastmodified":"2016-5-23 10:06:52.304"}

		try {
			inspection.setUseTobacco(true);
			inspection.setTobaccoSold("not in data");
			inspection.setTobaccoProducts2("not in data");
			inspection.setTobaccoSign("not in data");
			inspection.setTobaccoProducts("not in data");
			inspection.setTobaccoMachine("not in data");
			inspection.setTobaccoObserved("not in data");
			inspection.setTobaccoCounter("not in data");

			if ( tobaccoMap.containsKey("record_tobaccosold") ) inspection.setTobaccoSold((String) tobaccoMap.get("record_tobaccosold"));
			if ( tobaccoMap.containsKey("record_ecigsvapor") ) inspection.setTobaccoProducts2((String) tobaccoMap.get("record_ecigsvapor"));
			if ( tobaccoMap.containsKey("record_tobaccosign") ) inspection.setTobaccoSign((String) tobaccoMap.get("record_tobaccosign"));
			if ( tobaccoMap.containsKey("record_tobaccoproduct") ) inspection.setTobaccoProducts((String) tobaccoMap.get("record_tobaccoproduct"));
			if ( tobaccoMap.containsKey("record_vendingmachine") ) inspection.setTobaccoMachine((String) tobaccoMap.get("record_vendingmachine"));
			if ( tobaccoMap.containsKey("record_noillegal") ) inspection.setTobaccoObserved((String) tobaccoMap.get("record_noillegal"));
			if ( tobaccoMap.containsKey("record_tobaccoparaphernalia") ) inspection.setTobaccoCounter((String) tobaccoMap.get("record_tobaccoparaphernalia"));

			jw.addProperty("tobaccoSold", inspection.getTobaccoSold());
			jw.addProperty("tobaccoProducts2", inspection.getTobaccoProducts2());
			jw.addProperty("tobaccoSign", inspection.getTobaccoSign());
			jw.addProperty("tobaccoProducts", inspection.getTobaccoProducts());
			jw.addProperty("tobaccoMachine", inspection.getTobaccoMachine());
			jw.addProperty("tobaccoObserved", inspection.getTobaccoObserved());
			jw.addProperty("tobaccoCounter", inspection.getTobaccoCounter());
			return "";
		} catch (Exception e) {
			jw.addProperty("error", e.toString());
			jw.addProperty("errorRoutine", msgContext);
			return e.toString();
		}
	}
}
