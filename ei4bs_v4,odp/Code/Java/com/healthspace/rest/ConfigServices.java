package com.healthspace.rest;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import javax.faces.context.FacesContext;

import org.apache.commons.lang.StringEscapeUtils;
import org.openntf.domino.Name;
import org.openntf.domino.View;
import org.openntf.domino.ViewEntry;

import com.healthspace.general.HS_Util;
import com.healthspace.general.HS_database;
import com.healthspace.general.InspectionPicklists;
import com.healthspace.general.ViolationModule;
import com.healthspace.tools.JSFUtil;
import com.ibm.jscript.InterpretException;
import com.ibm.jscript.std.ObjectObject;
import com.ibm.jscript.types.FBSDefaultObject.JSProperty;

public class ConfigServices {

	private String	logMsgs	= "";

	public ConfigServices() {

	}

	private String getPname() {
		String pname = JSFUtil.getParamValue("profile");
		pname = ("".equals(pname)) ? "GlobalSettings" : pname;
		return pname;
	}

	private String getFname() {
		String fname = JSFUtil.getParamValue("field");
		fname = ("".equals(fname)) ? "unknown" : fname;
		return fname;
	}

	public String getProfileString() {
		return getProfileString(getPname(), getFname());
	}

	public String getPickList() {
		String msgContext = RestUtil.showDBPath() + " ConfigServices.getPickList";
		StringWriter sw = new StringWriter();
		HS_JsonWriter jw = new HS_JsonWriter(sw, false);

		try {
			String plname = JSFUtil.getParamValue("picklist");
			plname = ("".equals(plname)) ? "unknown" : plname;
			jw.startObject();
			jw.addProperty("xsource", msgContext);
			jw.addProperty("picklist", plname);

			if ( "eho".equalsIgnoreCase(plname) ) this.getPickListEho(jw);
			if ( "enforcement".equalsIgnoreCase(plname) ) this.getPickListEnforcement(jw);

			jw.endProperty();
			jw.endObject();
		} catch (IOException e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			return "{\"error\":\"" + e.toString() + " in " + msgContext + "\"}";
		}
		return sw.toString();
	}

	public void getPickListEho(HS_JsonWriter jw) {

		/*
		 * 
		 * // was: @DbColumn("":""; "":""; "OfficerLookup"; 1); var scopeKey = "HS_ViewParms"; var viewParms = sessionScope.get(scopeKey); var bstr = (viewParms == null) ? null : viewParms.bangString; if(bstr
		 * == null) { bstr = hs_Database.getdbBang("eiRoot"); }
		 * 
		 * var ehos = @DbColumn(bstr, "OfficerLookup", 1); var cuser =
		 * 
		 * @Name("[CN]", @UserName()); if(!@IsMember(cuser, ehos)) ehos.push(cuser); return ehos
		 */
		String msgContext = RestUtil.showDBPath() + " ConfigServices.getPickListEho";
		try {
			jw.startProperty("values");

			jw.startArray();
			jw.addArrayItem("");
			View olview = new HS_database().getdb("eiRoot").getView("OfficerLookup");
			for (ViewEntry ve : olview.getAllEntries()) {
				String plitem = (String) ve.getColumnValue("FullName");
				Name plname = HS_Util.getSession().createName(plitem);
				jw.addArrayItem(plname.getCommon());
			}
			jw.endArray();
			jw.endProperty();
		} catch (IOException e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			// return "{\"error\":\"" + e.toString() + " in " + msgContext +
			// "\"}";
			try {
				jw.addProperty("error", e.toString());
			} catch (IOException e1) {
			}
		}
		return;
	}

	public void getPickListEnforcement(HS_JsonWriter jw) {
		/*
		 * var eiList = ["", "Conditional", "License Suspended", "Operational", "Other", "Out of Business", "Release", "Revoke", "Withhold"];
		 * 
		 * var vdhList = ["", "NOV's issued", "Informal Fact Finding Conferences Held", "Notices of Intent to Revoke Permit Issued", "Revocation Hearings Held", "Permits Revoked", "Permits Suspended",
		 * "Food Impoundment Actions Taken", "Food Impoundment Hearings", "Food Condemnation Actions Taken", "Food Condemnation Hearings"];
		 * 
		 * return ("VDH".equals(getDBDesign())) ? vdhList : eiList;
		 */
		String msgContext = RestUtil.showDBPath() + " ConfigServices.getPickListEho";
		try {
			jw.startProperty("values");

			jw.startArray();
			jw.addArrayItem("");

			jw.addArrayItem("Conditional");
			jw.addArrayItem("License Suspended");
			jw.addArrayItem("Operational");
			jw.addArrayItem("Other");
			jw.addArrayItem("Out of Business");
			jw.addArrayItem("Release");
			jw.addArrayItem("Revoke");
			jw.addArrayItem("Withhold");

			jw.endArray();
			jw.endProperty();
		} catch (IOException e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			// return "{\"error\":\"" + e.toString() + " in " + msgContext +
			// "\"}";
			try {
				jw.addProperty("error", e.toString());
			} catch (IOException e1) {
			}
		}
		return;
	}

	public String getProfileString(String pname, String fname) {
		String msgContext = RestUtil.showDBPath() + " ConfigServices.getProfileString";
		StringWriter sw = new StringWriter();
		HS_JsonWriter jw = new HS_JsonWriter(sw, false);

		try {
			jw.startObject();
			jw.addProperty("objectSource", msgContext);
			// if ("Yes".equals(HS_Util.getAppSettingString("debug_HSTConfigService") ) )
			// jw.addProperty("restDebugServer", "true");
			jw.addProperty("profileName", pname);
			jw.addProperty("profileField", fname);

			if ( "InspectionTypes".equals(fname) ) {
				jw.addProperty("value", "wip");
			} else if ( "GlobalSettings".equalsIgnoreCase(pname) ) {
				jw.addProperty("value", HS_Util.getGlobalSettingsString(fname));
			} else if ( "MasterSettings".equalsIgnoreCase(pname) ) {
				jw.addProperty("value", HS_Util.getMasterSettingsString(fname));
			} else {
				jw.addProperty("value", "Unknown profile: [" + pname + "]");
			}
			jw.endObject();
		} catch (IOException e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			return "{\"error\":\"" + e.toString() + " in " + msgContext + "\"}";
		}
		if ( "Yes".equals(HS_Util.getAppSettingString("debug_HSTConfigService")) ) RestUtil.writeToAgentLog(sw.toString(), msgContext);
		return sw.toString();
	}

	public String getProfileStringList() {
		return getProfileStringList(getPname(), getFname());
	}

	public String getProfileStringList(String pname, String fname) {
		String msgContext = RestUtil.showDBPath() + " ConfigServices.getProfileStringList";
		StringWriter sw = new StringWriter();
		HS_JsonWriter jw = new HS_JsonWriter(sw, false);

		try {
			jw.startObject();
			jw.addProperty("objectSource", msgContext);
			// if ("Yes".equals(HS_Util.getAppSettingString("debug_HSTConfigService") ) )
			// jw.addProperty("restDebugServer", "true");
			jw.addProperty("profileName", pname);
			jw.addProperty("profileField", fname);

			jw.startProperty("values");
			Vector<String> result = new Vector<String>();
			// if("InspectionTypes".equals(fname)) {

			// } else
			if ( "GlobalSettings".equalsIgnoreCase(pname) ) {
				result = HS_Util.getGlobalSettingsVector(fname);
			} else if ( "MasterSettings".equalsIgnoreCase(pname) ) {
				result = HS_Util.getMasterSettingsVector(fname);
			} else {
				result.add("Unknown profile: [" + pname + "]");
			}
			jw.startArray();
			Iterator<String> riter = result.iterator();
			while (riter.hasNext()) {
				jw.addArrayItem(riter.next());
			}
			jw.endArray();
			jw.endProperty();

			jw.endObject();
		} catch (IOException e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			return "{\"error\":\"" + e.toString() + " in " + msgContext + "\"}";
		}
		if ( "Yes".equals(HS_Util.getAppSettingString("debug_HSTConfigService")) ) RestUtil.writeToAgentLog(sw.toString(), msgContext);
		return sw.toString();
	}

	private String getModule() {
		String msgContext = RestUtil.showDBPath() + " ConfigServices.getModule";
		boolean localDebug = "Yes".equals(HS_Util.getAppSettingString("debug_HSTConfigService"));
		String modname = "";
		String urlModName = "";
		try {
			urlModName = JSFUtil.getParamValue("module");
			if ( localDebug ) System.out.print(msgContext + ": urlModName:[" + urlModName + "]");
			// modname = ("".equals(modname)) ? "Food Code 2009" : modname;
			// String modname = ("".equals(urlModName)) ? ViolationModule.getViolationModule("Food").get(0) : ViolationModule.getViolationModule(urlModName).get(0);
			modname = ("".equals(urlModName)) ? ViolationModule.getViolationModule("Food", localDebug).get(0) : urlModName;
			if ( "UNKNOWN!".equals(modname) ) modname = urlModName;
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if ( localDebug ) e.printStackTrace();
			modname = "ERROR";
		}
		if ( localDebug ) System.out.print(msgContext + ": returning modname:[" + modname + "]");
		return modname;
	}

	public String getSections() {
		return this.getSections(this.getModule());
	}

	public String getSections(String module) {
		String msgContext = RestUtil.showDBPath() + " ConfigServices.getSections";
		boolean localDebug = "Yes".equals(HS_Util.getAppSettingString("debug_HSTConfigService"));
		StringWriter sw = new StringWriter();
		HS_JsonWriter jw = new HS_JsonWriter(sw, false);

		InspectionPicklists bean = this.getInspectionPLBean();

		Date sdt = new Date();

		try {
			jw.startObject();
			jw.addProperty("xsource", msgContext);
			// if ("Yes".equals(HS_Util.getAppSettingString("debug_HSTConfigService") ) )
			// jw.addProperty("restDebugServer", "true");
			try {
				jw.addProperty("dbDesign", HS_Util.getMasterSettingsString("DBDesign"));
			} catch (Exception e2) {
				jw.addProperty("dbDesign", "ERROR: " + e2.toString());
			}
			jw.addProperty("module", module);

			Vector<ObjectObject> slist = bean.loadDataSectionsAsSigner(module);
			jw.addProperty("sectionDataSize", "" + slist.size() + "");

			if ( localDebug ) this.logMsgs = RestUtil.addDebugProperties(jw, sw, null);

			jw.startProperty("sectionData");
			jw.startArray();
			Iterator<ObjectObject> sit = slist.iterator();
			ObjectObject sentry = null;
			while (sit.hasNext()) {
				sentry = sit.next();
				jw.startArrayItem();
				jw.startObject();
				Iterator<JSProperty> sepit = sentry.getPropertyIterator();
				while (sepit.hasNext()) {
					JSProperty jsprop = sepit.next();
					// jw.addProperty(jsprop.getPropertyName(),
					// jsprop.getValue().toString());
					jw.addProperty(jsprop.getPropertyName(), jsprop.getValue());
				}
				jw.endObject();
				jw.endArrayItem();
			}
			jw.endArray();
			jw.endProperty();

		} catch (IOException e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			return "{\"error\":\"IOException: " + e.toString() + " in " + msgContext + "\"}";
		} catch (Exception e1) {
			HS_Util.debug(e1.toString(), "error", msgContext);
			return "{\"error\":\"Exception: " + e1.toString() + " in " + msgContext + "\"}";
		}

		this.logMsgs += "\n\n" + msgContext + " completed.\n\n" + HS_Util.emsecs(sdt);
		if ( localDebug ) RestUtil.writeToAgentLog(this.logMsgs, msgContext);

		try {
			jw.endObject();
		} catch (IOException e) {
		}
		return sw.toString();
	}

	public String getViolations() {
		return this.getViolations(this.getModule());
	}

	public String getViolations(String module) {
		String msgContext = RestUtil.showDBPath() + " ConfigServices.getViolations";
		boolean localDebug = "Yes".equals(HS_Util.getAppSettingString("debug_HSTConfigService"));

		if ( localDebug ) System.out.print("Starting " + msgContext + " with module:[" + module + "]");
		StringWriter sw = new StringWriter();
		HS_JsonWriter jw = new HS_JsonWriter(sw, false);

		InspectionPicklists bean = this.getInspectionPLBean();

		Date sdt = new Date();
		try {
			jw.startObject();
			jw.addProperty("xsource", msgContext);
			if ( localDebug ) jw.addProperty("restDebugServer", "true");

			try {
				jw.addProperty("dbDesign", HS_Util.getMasterSettingsString("DBDesign"));
			} catch (Exception e2) {
				jw.addProperty("dbDesign", "ERROR: " + e2.toString());
			}
			jw.addProperty("module", module);

			Vector<ObjectObject> vlist = bean.loadDataViolationsAsSigner(module);
			jw.addProperty("violationDataSize", "" + vlist.size() + "");

			if ( localDebug ) this.logMsgs = RestUtil.addDebugProperties(jw, sw, null);

			String secnum = "";
			jw.startProperty("violationData");
			jw.startArray();
			Iterator<ObjectObject> vit = vlist.iterator();
			ObjectObject ventry = null;
			while (vit.hasNext()) {
				ventry = vit.next();
				jw.startArrayItem();
				jw.startObject();
				Iterator<JSProperty> vepit = ventry.getPropertyIterator();
				while (vepit.hasNext()) {
					JSProperty jsprop = vepit.next();
					// jw.addProperty(jsprop.getPropertyName(),
					// jsprop.getValue().toString());
					if ( "number".equals(jsprop.getPropertyName()) ) {
						secnum = jsprop.getValue().stringValue();
						jw.addProperty("sectionNumber", secnum);
						jw.addProperty("sectionUnid", getSectionUnid(secnum, module));
					} else if ( "legalText".equals(jsprop.getPropertyName()) ) {
						// skip it
					} else if ( "module".equals(jsprop.getPropertyName()) ) {
						// skip it
					} else {
						jw.addProperty(jsprop.getPropertyName(), jsprop.getValue());
					}
				}
				jw.endObject();
				jw.endArrayItem();
			}
			jw.endArray();
			jw.endProperty();

			jw.endObject();
		} catch (IOException e) {
			String emsg = "IOException: " + e.toString() + " in " + msgContext + "";
			HS_Util.debug(emsg, "error", msgContext);
			this.logMsgs += "\n\nERROR:" + emsg + " \n\n" + HS_Util.emsecs(sdt);
			if ( localDebug ) {
				RestUtil.writeToAgentLog(this.logMsgs, msgContext);
				e.printStackTrace();
			}
			return "{\"error\":\"" + emsg + "\"}";
		} catch (Exception e1) {
			// HS_Util.debug(e1.toString(), "error", msgContext);
			// return "{\"error\":\"Exception: " + e1.toString() + " in " + msgContext + "\"}";
			String emsg = "" + e1.toString() + " in " + msgContext + "";
			HS_Util.debug(emsg, "error", msgContext);
			this.logMsgs += "\n\nERROR:" + emsg + " \n\n" + HS_Util.emsecs(sdt);
			if ( localDebug ) {
				RestUtil.writeToAgentLog(this.logMsgs, msgContext);
				e1.printStackTrace();
			}
			return "{\"error\":\"" + emsg + "\"}";
		}

		this.logMsgs += "\n\n" + msgContext + " completed.\n\n" + HS_Util.emsecs(sdt);
		if ( localDebug ) RestUtil.writeToAgentLog(this.logMsgs, msgContext);

		return sw.toString();
	}

	public String getObservations() {
		return this.getObservations(this.getModule());
	}

	public String getObservations(String module) {
		String msgContext = RestUtil.showDBPath() + " ConfigServices.getObservations";
		boolean localDebug = "Yes".equals(HS_Util.getAppSettingString("debug_HSTConfigService"));
		StringWriter sw = new StringWriter();
		HS_JsonWriter jw = new HS_JsonWriter(sw, false);

		// InspectionPicklists bean = this.getInspectionPLBean();

		Date sdt = new Date();
		try {
			jw.startObject();
			jw.addProperty("xsource", msgContext);
			if ( localDebug ) jw.addProperty("restDebugServer", "true");
			try {
				jw.addProperty("dbDesign", HS_Util.getMasterSettingsString("DBDesign"));
			} catch (Exception e2) {
				jw.addProperty("dbDesign", "ERROR: " + e2.toString());
			}
			jw.addProperty("module", module);

			Vector<ObjectObject> olist = this.getInspectionPLBean().loadDataObservationsAsSigner(module);
			jw.addProperty("observationDataSize", "" + olist.size() + "");

			if ( localDebug ) this.logMsgs = RestUtil.addDebugProperties(jw, sw, null);

			jw.startProperty("observationData");
			jw.startArray();
			Iterator<ObjectObject> oit = olist.iterator();
			ObjectObject oentry = null;
			while (oit.hasNext()) {
				oentry = oit.next();
				jw.startArrayItem();
				jw.startObject();
				Iterator<JSProperty> oepit = oentry.getPropertyIterator();
				while (oepit.hasNext()) {
					JSProperty jsprop = oepit.next();
					// jw.addProperty(jsprop.getPropertyName(),
					// jsprop.getValue().toString());
					// if ("number".equals(jsprop.getPropertyName())) {
					// secnum = jsprop.getValue().stringValue();
					// jw.addProperty("sectionNumber", secnum);
					// jw.addProperty("sectionUnid", getSectionUnid(secnum,
					// module));
					if ( "legalText".equals(jsprop.getPropertyName()) ) {
						// escape it??
						jw.addProperty(jsprop.getPropertyName(), StringEscapeUtils.escapeJavaScript(jsprop.getValue().stringValue()));
					} else if ( "module".equals(jsprop.getPropertyName()) ) {
						// skip it
					} else if ( "used".equals(jsprop.getPropertyName()) ) {
						// skip it
					} else if ( "code".equals(jsprop.getPropertyName()) ) {
						jw.addProperty("violCode", jsprop.getValue());
						jw.addProperty("violUnid", this.getViolationUnid(jsprop.getValue().stringValue(), module));
					} else {
						jw.addProperty(jsprop.getPropertyName(), StringEscapeUtils.escapeJavaScript(jsprop.getValue().stringValue()));
					}
				}
				jw.endObject();
				jw.endArrayItem();
			}
			jw.endArray();
			jw.endProperty();
			jw.endObject();
		} catch (IOException e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			return "{\"error\":\"IOException: " + e.toString() + " in " + msgContext + "\"}";
		} catch (Exception e1) {
			HS_Util.debug(e1.toString(), "error", msgContext);
			return "{\"error\":\"Exception: " + e1.toString() + " in " + msgContext + "\"}";
		}
		this.logMsgs += "\n\n" + msgContext + " completed.\n\n" + HS_Util.emsecs(sdt);
		if ( localDebug ) RestUtil.writeToAgentLog(this.logMsgs, msgContext);

		return sw.toString();
	}

	private Map<String, String>	sunidmap	= null;

	private String getSectionUnid(String secnum, String module) {
		if ( this.sunidmap == null ) {
			Vector<ObjectObject> slist = this.getInspectionPLBean().loadDataSections(module);
			this.sunidmap = new HashMap<String, String>();
			Iterator<ObjectObject> sit = slist.iterator();
			ObjectObject sentry = null;
			String snum = "";
			String sunid = "";
			while (sit.hasNext()) {
				sentry = sit.next();
				try {
					snum = sentry.get("number").stringValue();
					sunid = sentry.get("unid").stringValue();
					this.sunidmap.put(snum, sunid);
				} catch (InterpretException e) {
					// ignore errors for now...
				}
			}
		}
		if ( sunidmap.containsKey(secnum) ) return sunidmap.get(secnum);
		return secnum + "-unid-not-found";
	}

	private Map<String, String>	vunidmap	= null;

	private String getViolationUnid(String violCode, String module) {
		if ( this.sunidmap == null ) {
			Vector<ObjectObject> vlist = this.getInspectionPLBean().loadDataViolations(module);
			this.vunidmap = new HashMap<String, String>();
			Iterator<ObjectObject> vit = vlist.iterator();
			ObjectObject ventry = null;
			String vcode = "";
			String vunid = "";
			while (vit.hasNext()) {
				ventry = vit.next();
				try {
					vcode = ventry.get("code").stringValue();
					vunid = ventry.get("unid").stringValue();
					this.vunidmap.put(vcode, vunid);
				} catch (InterpretException e) {
					// ignore errors for now...
				}
			}
		}
		if ( vunidmap.containsKey(violCode) ) return vunidmap.get(violCode);
		return violCode + "-unid-not-found";
	}

	private InspectionPicklists getInspectionPLBean() {
		final String BEAN_NAME = "inspectionPicklists"; //$NON-NLS-1$
		FacesContext context = FacesContext.getCurrentInstance();
		InspectionPicklists bean = (InspectionPicklists) context.getApplication().getVariableResolver().resolveVariable(context, BEAN_NAME);
		return bean;
	}
}
