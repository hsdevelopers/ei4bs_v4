package com.healthspace.rest;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import com.healthspace.general.HS_Util;
import com.ibm.domino.services.util.JsonWriter;
import com.ibm.jscript.types.FBSValue;

public class HS_JsonWriter extends JsonWriter {

	public HS_JsonWriter(Writer writer, boolean compact) {
		super(writer, compact);
		// TODO Auto-generated constructor stub
	}

	public void addProperty(String propertyName, FBSValue value) throws IOException {
		super.startProperty(propertyName);
		super.outStringLiteral(value.stringValue());
		super.endProperty();
	}

	public void addProperty(String name, String value) throws IOException {
		super.startProperty(name);
		if ( value == null ) value = "-null-";
		super.outStringLiteral(value);
		super.endProperty();
	}

	public void addProperty(String name, Date value) throws IOException {
		// wbatch += HS_Util.formatDate(wdt, "yyyy-MM-dd HH:mm:ss:SSS");
		super.startProperty(name);
		super.outStringLiteral(HS_Util.formatDate(value, "yyyy-MM-dd HH:mm:ss"));
		super.endProperty();
	}

	public void addArrayItem(String value) throws IOException {
		super.startArrayItem();
		super.outStringLiteral(value);
		super.endArrayItem();
	}

	public void addProperty(String name, String[] values) throws IOException {
		super.startProperty(name);
		super.startArray();
		for (int i = 0; i < values.length; i++) {
			this.addArrayItem(values[i]);
		}
		super.endArray();
		super.endProperty();
	}

	@SuppressWarnings("unchecked")
	public void addProperty(String name, ArrayList values) throws IOException {
		super.startProperty(name);
		super.startArray();
		for (Object obj : values) {
			if ( obj instanceof String )
				this.addArrayItem((String) obj);
			else
				this.addArrayItem("1. UNKNOWN Object type: " + obj.getClass().getName());
		}
		super.endArray();
		super.endProperty();
	}

	public void addProperty(String name, Vector<String> values) throws IOException {
		super.startProperty(name);
		super.startArray();
		for (int i = 0; i < values.size(); i++) {
			this.addArrayItem(values.get(i));
		}
		super.endArray();
		super.endProperty();
	}

	public void addProperty(String name, Set<String> keySet) throws IOException {
		super.startProperty(name);
		super.startArray();
		for (String key : keySet) {
			this.addArrayItem(key);
		}
		super.endArray();
		super.endProperty();
	}

	public void addMap(Map<String, Object> map) throws IOException {

		for (String key : map.keySet()) {
			Object ob = map.get(key);
			this.addPropertyObject(key, ob);
		}

	}

	@SuppressWarnings("unchecked")
	private void addPropertyObject(String name, Object value) throws IOException {
		try {
			if ( value instanceof String ) {
				this.addProperty(name, (String) value);
			} else if ( value instanceof Date ) {
				this.addProperty(name, (Date) value);
			} else if ( value instanceof java.util.ArrayList ) {
				this.addPropertyArrayList(name, value);
			} else if ( value instanceof Vector ) {
				this.addProperty(name, (Vector<String>) value);
			} else {
				this.addProperty(name, "2. UNKNOWN OBJECT TYPE: " + value.getClass().getName());
			}
		} catch (Exception e) {
			this.addProperty(name, "Error adding propert of type " + value.getClass().getName() + " : " + e.toString());
		}
	}

	public void addProperty(String name, List<String> value) throws IOException {
		super.startProperty(name);
		super.startArray();

		if ( value.size() == 0 ) {
			// do nothing
		} else if ( value.get(0) instanceof String ) {
			for (int i = 0; i < value.size(); i++) {
				String x = value.get(i);
				this.addArrayItem(x);
			}
		} else {
			this.addArrayItem("3. UNKNOWN OBJECT TYPE: " + value.get(0).getClass().getName());
		}
		super.endArray();
		super.endProperty();
	}

	public void addProperty(String name, HashMap<String, Object> value) throws IOException {
		super.startProperty(name);
		super.startArray();

		for (String key : value.keySet()) {
			Object obj = value.get(key);
			super.startArrayItem();
			super.startObject();
			this.addProperty("key", key);
			this.addProperty("datatype", obj.getClass().getName());
			super.endObject();
			super.endArrayItem();

		}

		super.endArray();
		super.endProperty();
	}

	@SuppressWarnings("unchecked")
	private void addPropertyArrayList(String name, Object value) throws IOException {
		super.startProperty(name);
		super.startArray();
		ArrayList warray = (ArrayList) value;
		if ( warray.size() == 0 ) {
			// do nothing
		} else if ( warray.get(0) instanceof String ) {
			for (int i = 0; i < warray.size(); i++) {
				String x = (String) warray.get(i);
				this.addArrayItem(x);
			}
		} else if ( warray.get(0) instanceof Map ) {
			for (int i = 0; i < warray.size(); i++) {
				super.startArrayItem();
				super.startObject();
				this.addMap((Map<String, Object>) warray.get(i));
				super.endObject();
				super.endArrayItem();
			}
		} else {
			this.addArrayItem("4 UNKNOWN OBJECT TYPE: " + warray.get(0).getClass().getName());
		}
		super.endArray();
		super.endProperty();
	}
}
