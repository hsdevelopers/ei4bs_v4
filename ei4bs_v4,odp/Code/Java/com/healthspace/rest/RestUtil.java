package com.healthspace.rest;

import java.io.IOException;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.RichTextItem;
import org.openntf.domino.Session;

import com.healthspace.general.AgentLogger;
import com.healthspace.general.HS_Util;
import com.healthspace.general.HS_database;
import com.healthspace.tools.JSFUtil;
import com.ibm.xsp.designer.context.ServletXSPContext;
import com.ibm.xsp.designer.context.XSPUrl;

public class RestUtil {

	// static Database eidb = null;

	@SuppressWarnings("unchecked")
	public static String addDebugProperties(HS_JsonWriter jw, StringWriter sw, List<String> resultSections) throws IOException {

		try {
			if ( resultSections == null ) {
				resultSections = new ArrayList<String>();
				resultSections.add("headerValues");
			}
			Database eidb = new HS_database().getdb("eiRoot");
			if ( eidb == null ) {
				jw.addProperty("eidb", "-came back as null");
			} else if ( eidb.isOpen() ) {
				jw.addProperty("eidb", eidb.getServer() + "!!" + eidb.getFilePath() + " - Access level:[" + eidb.getCurrentAccessLevel() + "]");
			} else {
				jw.addProperty("eidb", eidb.getServer() + "!!" + eidb.getFilePath() + " - NOT OPENNED");
			}
		} catch (Exception eix) {
			jw.addProperty("eidb", "Got error: " + eix.toString());
		}

		if ( resultSections.contains("headerValues") ) {
			jw.startProperty("headerValues");
			jw.startObject();
			jw.addProperty("remoteUser", JSFUtil.getFacesContext().getExternalContext().getRemoteUser());

			XSPUrl thisurl = ((ServletXSPContext) JSFUtil.getVariableValue("context")).getUrl();
			jw.addProperty("urlpath", thisurl.getPath());
			jw.addProperty("urlhost", thisurl.getHost());
			jw.addProperty("url", thisurl.toString());

			try {
				HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
				jw.addProperty("remoteaddress", request.getRemoteAddr());
				jw.addProperty("reqesturi", request.getRequestURI());
				jw.addProperty("querystring", request.getQueryString());

				Enumeration headerNames = request.getHeaderNames();
				while (headerNames.hasMoreElements()) {
					String key = (String) headerNames.nextElement();
					String value = request.getHeader(key);
					jw.addProperty(key, value);
					// System.out.print("addDebugProperties: \"" + key + "\":\"" +
					// value + "\"");
				}
			} catch (Exception e) {
				jw.addProperty("error", "In addDebugProperties: " + e.toString());
			}
			jw.endObject();
			jw.endProperty();
		}
		return sw.toString();
	}

	public static Vector<String> addListItem(Vector<String> target, String value) {
		Vector<String> result = (target == null) ? new Vector<String>() : target;
		result.add(value);
		return result;
	}

	@SuppressWarnings("unchecked")
	public static String addUserAgent(HS_JsonWriter jw, StringWriter sw) throws IOException {
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			Enumeration headerNames = request.getHeaderNames();
			while (headerNames.hasMoreElements()) {
				String key = (String) headerNames.nextElement();
				String value = request.getHeader(key);
				if ( "User-Agent".equals(key) ) jw.addProperty(key, value);
				// System.out.print("addDebugProperties: \"" + key + "\":\"" +
				// value + "\"");
			}
		} catch (Exception e) {
			jw.addProperty("error", "In addUserAgent: " + e.toString());
		}

		return sw.toString();
	}

	public static Date cvrtJsonDate(String dtstr) {
		Date wdt = null;
		String[] dtparts = null;
		dtparts = dtstr.split("-");
		if ( !HS_Util.isStringNumeric(dtparts[1]) ) {
			// Assumes YYYY-MM-DD format.
			try {
				wdt = new SimpleDateFormat("yyyy-MM-dd", Locale.US).parse(dtstr);
			} catch (ParseException e) {
				wdt = null;
			}
		} else {
			try {
				wdt = new SimpleDateFormat("yyyy-MM-dd", Locale.US).parse(dtstr);
			} catch (ParseException e) {
				wdt = null;
			}
		}
		return wdt;
	}

	@SuppressWarnings("unchecked")
	public static String getDebugProperties() {
		String result = "remoteUser: [" + JSFUtil.getFacesContext().getExternalContext().getRemoteUser() + "]\n";

		HttpServletRequest request = null;

		try {
			request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			result += "Remote Address:[" + request.getRemoteAddr() + "]\n";
			result += "Request URI:[" + request.getRequestURI() + "]\n";
			result += "Request QueryString:[" + request.getQueryString() + "]\n";

			result += "HTTP HeaderValues";
			Enumeration headerNames = request.getHeaderNames();
			while (headerNames.hasMoreElements()) {
				String key = (String) headerNames.nextElement();
				String value = request.getHeader(key);
				result += "\n\t" + key + ": [" + value + "]";
			}

		} catch (Exception e) {
			result += "/n ERROR:[" + e.toString();
		}

		return result;
	}

	public static Database getEidb() {
		// if (eidb == null) {
		HS_database hsdb = new HS_database();
		Database eidb = hsdb.getdb("eiRoot");
		// }
		return eidb;
	}

	/*****
	 * private static Vector<String> serverRoles = null;
	 * 
	 * 
	 * public Vector<String> getServerRoles() { return RestUtil.serverRoles; }
	 * 
	 * public void setServerRoles(Vector<String> serverRoles) { RestUtil.serverRoles = serverRoles; }
	 * 
	 * public static boolean isRestDebugServer() { if ( RestUtil.serverRoles == null ) { RestUtil.serverRoles = new Vector<String>(); String server = HS_Util.getSession().getServerName(); ACL acl =
	 * HS_Util.getSession().getCurrentDatabase().getACL(); ACLEntry srvrEntry = acl.getEntry(server); if ( srvrEntry == null ) { // System.out.print("isRestDebugServer: srvrEntry is null for:[" + // server +
	 * "]"); } else { RestUtil.serverRoles = srvrEntry.getRoles(); } } // for (String role : srvrEntry.getRoles()) { for (String role : RestUtil.serverRoles) { //
	 * System.out.print("isRestDebugServer: testing role:[" + role + // "]"); if ( "[RestDebugServer]".equals(role) ) return true; } return false; }
	 ************/

	public static void sendAlert(String msg) {
		System.out.print("SENDING ALERT: " + msg);
		sendAlert(msg, "", "", null);
	}

	public static void sendAlert(String msg, String msg2, String msg3, Exception pe) {
		try {
			if ( pe != null ) System.out.print("SENDING ALERT WITH STACK TRACE: " + msg);

			Session sess = HS_Util.getSession();
			Document mdoc = sess.getCurrentDatabase().createDocument();
			mdoc.replaceItemValue("Form", "Memo");
			mdoc.replaceItemValue("Subject", "XPages RestUtil Alert!");
			RichTextItem body = mdoc.createRichTextItem("body");
			body.appendText(msg);
			if ( !"".equals(msg2) ) {
				body.addNewLine(1, true);
				body.appendText(msg2);
			}
			if ( !"".equals(msg3) ) {
				body.addNewLine(1, true);
				body.appendText(msg3);
			}
			body.addNewLine(2, true);
			body.appendText("server: " + sess.getCurrentDatabase().getServer());
			body.addNewLine(1, true);
			body.appendText("database: " + sess.getCurrentDatabase().getTitle() + " (" + sess.getCurrentDatabase().getFilePath() + ")");
			body.addNewLine(1, true);
			body.appendText("domino user: " + sess.getEffectiveUserName());
			body.addNewLine(1, true);
			body.appendText("remote user: " + FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
			body.addNewLine(1, true);
			body.appendText("req path info: " + FacesContext.getCurrentInstance().getExternalContext().getRequestPathInfo());
			body.addNewLine(2, true);

			if ( pe != null ) {
				body.appendText("EXCEPTION: " + pe.toString());
				body.addNewLine(2, true);
				body.appendText("STACK TRACE");
				body.addNewLine(1, true);
				StackTraceElement[] stes = pe.getStackTrace();
				for (StackTraceElement ste : stes) {
					body.addTab();
					body.appendText(ste.toString());
					body.addNewLine(1, true);
				}
			}
			String recip = HS_Util.getAppSettingString("HSTouchFailedUploadsNotice");
			if ( "".equals(recip) ) recip = "hnewberry@healthspace.com";
			mdoc.send(recip);

		} catch (Exception e) {
			System.out.print("FAILED TO SEND ALERT! " + e.toString());
		}
	}

	public static void writeToAgentLog(String message, String description) {
		RestUtil.writeToAgentLog(message, HS_Util.getSession().getEffectiveUserName(), description, "Resolved");
	}

	public static void writeToAgentLog(String message, String description, String user) {
		RestUtil.writeToAgentLog(message, user, description, "Resolved");
	}

	public static void writeToAgentLog(String message, String user, String description, String resolve) {
		try {
			AgentLogger alog = new AgentLogger();
			alog.init(HS_Util.getSessionAsSigner(), user, description, false, resolve);
			alog.log(message, true);
		} catch (Exception e) {
			System.out.print("RestUtil.writeToAgentLog caught error: " + e.toString());
		}
	}

	public static String showDBPath() {
		String dbpath = "";
		try {
			dbpath = HS_Util.getSession().getCurrentDatabase().getFilePath();
		} catch (Exception e) {
			dbpath = "path error";
		}
		return dbpath;
	}

	public static void writeWarningToAgentLog(String message, String user, String description, String resolve) {
		try {
			AgentLogger alog = new AgentLogger();
			alog.init(HS_Util.getSessionAsSigner(), user, description, false, resolve);
			alog.logWarning(message, true);
		} catch (Exception e) {
			System.out.print("RestUtil.writeToAgentLog caught error: " + e.toString());
		}
	}
}
