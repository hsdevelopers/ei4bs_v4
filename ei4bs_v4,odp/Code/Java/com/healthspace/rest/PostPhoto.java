package com.healthspace.rest;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.MIMEEntity;
import org.openntf.domino.MIMEHeader;
import org.openntf.domino.Session;
import org.openntf.domino.Stream;
import org.openntf.domino.utils.Factory;

import com.healthspace.general.HS_Util;
import com.healthspace.general.HS_database;
import com.healthspace.general.Inspection;

public class PostPhoto {

	public PostPhoto() {

	}

	public static boolean savePhotoDocument(Map<String, Object> req, HS_JsonWriter jw, PostServices caller) throws IOException {
		final String msgContext = "PostPhoto.savePhotoDocument";
		// final boolean localDebug = true;
		final boolean localDebug = "Yes".equals(HS_Util.getAppSettingString("Debug_HSTPhotos"));
		Vector<String> messages = new Vector<String>();

		Session sess = Factory.getSession();
		String imageKey = (String) req.get("record_photoid");

		sess.setConvertMime(false);
		sess.setConvertMIME(false);

		HS_database hsdb = new HS_database();
		Database eidb = hsdb.getdbAsSigner("eiRoot");
		// Document doc = sess.getCurrentDatabase().createDocument(); // This might be better in the eiRoot db...
		Document doc = eidb.createDocument();
		messages.add("Creating attachment doc in eiroot db:\"" + eidb.getTitle() + "\" [" + eidb.getServer() + "!!" + eidb.getFilePath() + "]");

		Document rdoc = sess.getCurrentDatabase().createDocument();

		try {
			// Create the MIME for an attachment.
			if ( localDebug ) jw.addProperty("photoDebug1", msgContext + ": creating the cameraImage element.");

			MIMEEntity rootMime = doc.createMIMEEntity("cameraImage");
			Stream stream2 = sess.createStream();
			stream2.writeText("This is a multipart message in MIME format");
			rootMime.setContentFromText(stream2, "multipart/mixed", MIMEEntity.ENC_NONE);
			MIMEEntity rootChild1 = rootMime.createChildEntity();
			rootChild1.setContentFromText(stream2, "multipart/alternative", MIMEEntity.ENC_NONE);
			MIMEEntity textPlain = rootChild1.createChildEntity();
			Stream stream3 = sess.createStream();
			stream3.writeText("Your client supports text only!");
			textPlain.setContentFromText(stream3, "text/plain", MIMEEntity.ENC_NONE);

			// Create the MIME child where the HTML goes after we add the image
			MIMEEntity textHtml = rootChild1.createChildEntity();

			// Create the MIME Child for the image
			MIMEEntity image1 = rootMime.createChildEntity();
			// MIMEHeader imageHeader = image1.createHeader("Content-ID");
			// imageHeader.setHeaderValAndParams("<" + imageKey + ">");

			MIMEHeader imageHeader = image1.createHeader("name");
			imageHeader.setHeaderVal("\"image.png\"");
			imageHeader = image1.createHeader("Content-Disposition");
			imageHeader.setHeaderVal("attachment;");
			imageHeader = image1.createHeader("filename");
			imageHeader.setHeaderVal("\"image.png\"");
			imageHeader = image1.createHeader("X-Attachment-Id");
			imageHeader.setHeaderVal(imageKey);

			Stream imgStream = sess.createStream();
			imgStream.writeText(caller.getPhotoData());
			image1.setContentFromText(imgStream, "image/png", MIMEEntity.ENC_BASE64);
			if ( localDebug ) messages.add("imgStream size:[" + imgStream.getPosition() + "]");
			if ( localDebug ) messages.add("image first 100 characters:[" + caller.getPhotoData().substring(0, 99) + "]");
			// Now image is attached, write HTML to the textHTML node

			Stream htmlStream = sess.createStream();
			// htmlStream.writeText("<div>Your image is attached</div>:<img src=\"cid:" + imageKey + "\" />");
			htmlStream.writeText("<div>Your image is attached:<br/></div>");
			textHtml.setContentFromText(htmlStream, "text/html; charset=UTF-8", MIMEEntity.ENC_NONE);

			// if (localDebug) jw.addProperty("photoDebug2",msgContext + ":calling doc.closeMIMEEntities(true, \"body\")...");
			doc.closeMIMEEntities(true, "cameraImage");
			// if (localDebug) jw.addProperty("photoDebug3",msgContext + ": body item with attachment created.");
			jw.addProperty("attachmentAdded", "using key: " + imageKey);
			messages.add("attachmentAdded using key: " + imageKey);
		} catch (Exception e) {
			jw.addProperty("attachmentAddError", "ERROR: " + e.toString());
			messages.add("attachmentAddError - ERROR: " + e.toString());
			if ( localDebug || "Yes".equals(HS_Util.getAppSettingString("debug_HSTDownload")) ) e.printStackTrace();
		}

		sess.setConvertMime(true);
		sess.setConvertMIME(true);
		try {
			doc.replaceItemValue("Form", "cameraImage");
			// doc.replaceItemValue("PostAuthor", sess.getEffectiveUserName());
			doc.replaceItemValue("RestBatch", caller.getBatch());
			doc.replaceItemValue("CameraImageKey", "??");
			doc.replaceItemValue("CameraImageKeySeq", "1");
			doc.replaceItemValue("CameraImageSeq", 1);

			doc.replaceItemValue("RestUser", req.get("RestUser"));
			doc.replaceItemValue("DocumentID", req.get("record_photoid"));
			doc.replaceItemValue("cameraImageKey", req.get("record_photoid"));
			doc.replaceItemValue("parentID", req.get("record_inspectionid"));

			doc.replaceItemValue("imageNotes", ((req.containsKey("record_notes") ? req.get("record_notes") : "")));

			if ( doc.save() ) {
				jw.addProperty("cameraImageUnid", doc.getUniversalID());
				jw.addProperty("cameraImageKey", doc.getItemValueString("cameraImageKey"));
				jw.addProperty("inspectionKey", doc.getItemValueString("parentID"));

				req.put("imageunid", doc.getUniversalID());

				caller.addPhotoUnid(doc.getUniversalID());
				doc.copyAllItems(rdoc, true);
				rdoc.replaceItemValue("Form", "RestBasicPost");
				if ( !localDebug ) rdoc.removeItem("cameraImage"); // get rid of the attachment uness we are debugging...

				Iterator<String> pitr = req.keySet().iterator();
				while (pitr.hasNext()) {
					String fname = pitr.next();
					Object fvalue = req.get(fname);
					if ( "record_photodata".equalsIgnoreCase(fname) ) {
						// skip this
						messages.add("Skipping the item:[" + fname + "] size:[" + fvalue.toString().length() + "]");
					} else if ( "restrecord".equalsIgnoreCase(fname) ) {
						// skip this for now...
						messages.add("Skipping the item:[" + fname + "] size:[" + fvalue.toString().length() + "]");
					} else if ( fname.toLowerCase().startsWith("auth_") && !"auth_user".equalsIgnoreCase(fname) ) {
						// skip this for now...
						messages.add("Skipping the item:[" + fname + "] size:[" + fvalue.toString().length() + "]");
					} else if ( fname.toLowerCase().startsWith("device_") ) {
						// skip this for now...
						messages.add("Skipping the item:[" + fname + "] size:[" + fvalue.toString().length() + "]");
					} else {
						rdoc.replaceItemValue(fname, fvalue);
					}
				}

				Date edt = new Date();
				rdoc.replaceItemValue("RunTime", (edt.getTime() - caller.getStartDateTime().getTime()));

				if ( localDebug ) rdoc.replaceItemValue("RestRecord", messages);

				if ( rdoc.save() ) {
					caller.setUnid(rdoc.getUniversalID());
					return true;
				}
			}
			// One or the other docs failed to save.
			sess.setConvertMime(true);
			sess.setConvertMIME(true);
			jw.addProperty("unid", "");
			if ( "".equals(doc.getUniversalID()) ) {
				jw.addProperty("error", "save of photo document failed to \"" + doc.getParentDatabase().getTitle() + "\"");
				jw.addProperty("dbCurrentAccessLevel", "[" + doc.getParentDatabase().getCurrentAccessLevel() + "]");
			} else {
				jw.addProperty("error", "save of failed to \"" + rdoc.getParentDatabase().getTitle() + "\"");
				jw.addProperty("dbCurrentAccessLevel", "[" + rdoc.getParentDatabase().getCurrentAccessLevel() + "]");
			}
		} catch (Exception e) {
			jw.addProperty("attachmentSaveError", "ERROR: " + e.toString());
			if ( "Yes".equals(HS_Util.getAppSettingString("debug_HSTDownload")) ) e.printStackTrace();
		}
		caller.setUnid("");
		return false;
	}

	public static String processPhoto(HS_JsonWriter jw, Map<String, Object> photoMap, Inspection inspection) throws IOException {
		final String msgContext = "PostPhoto.processPhoto";
		try {
			String iunid = (photoMap.containsKey("imageunid")) ? (String) photoMap.get("imageunid") : "";
			inspection.getImageUnids().add(iunid);
			jw.addProperty("imageunid", iunid);
			return "";
		} catch (Exception e) {
			jw.addProperty("error", e.toString());
			jw.addProperty("errorRoutine", msgContext);
			return e.toString();
		}
	}
}
