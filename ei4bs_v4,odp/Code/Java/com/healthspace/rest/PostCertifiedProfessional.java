package com.healthspace.rest;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

import com.healthspace.general.HS_Util;
import com.healthspace.general.Inspection;

public class PostCertifiedProfessional {

	public PostCertifiedProfessional() {

	}

	// { "inspectionID":"e70198d5-c5a5-4dda-ba06-890051a0e5d8"
	// "tabIDCertified_ProfessionalID":"7e03935d-f650-425d-ad52-d081c9259cfa"
	// "CertifiedManager":"Bevtest Hoffmanntest"
	// "CertNumber":"12345"
	// "CertExpiration":"2017-09-06"
	// "AllergenTraining":"Yes"
	// "synced":0
	// "datelastmodified":"2017-09-06 10:33:39.431"
	// "modifiedBy":""
	// "dateadd":"2017-09-06 10:33:39.432"}

	public static String processCertifiedProfessional(HS_JsonWriter jw, Map<String, Object> mgrmap, Inspection inspection) throws IOException {
		final String msgContext = "PostCertifiedProfessional.processCertifiedProfessional";
		// Vector <String> mgr = inspection.getManager();
		// Vector <String> mgraddr = inspection.getManagerAddress();
		// Vector <String> mgrExp = inspection.getManagerCertificateExpirationStr();
		// Vector <String> mgrCertId = inspection.getManagerCertificateId();

		try {
			String manager = "-not in data-";
			String address = "-not in data-";
			String expStr = "-not in data-";
			String certNumber = "-not in data-";
			String allergenTraining = "";
			// String certId = "-not in data-";

			if ( mgrmap.containsKey("record_certifiedmanager") ) manager = (String) mgrmap.get("record_certifiedmanager");
			if ( mgrmap.containsKey("record_manageraddress") ) address = (String) mgrmap.get("record_manageraddress");
			if ( mgrmap.containsKey("record_certnumber") ) certNumber = (String) mgrmap.get("record_certnumber");
			if ( mgrmap.containsKey("record_certexpiration") ) expStr = (String) mgrmap.get("record_certexpiration");
			// This element is not on all inspections, in fact is specific to Michigan NW
			if ( mgrmap.containsKey("record_allergentraining") ) allergenTraining = (String) mgrmap.get("record_allergentraining");

			inspection.setManager(RestUtil.addListItem(inspection.getManager(), manager));
			inspection.setManagerAddress(RestUtil.addListItem(inspection.getManagerAddress(), address));
			inspection.setManagerAllergenTraining(RestUtil.addListItem(inspection.getManagerAllergenTraining(), allergenTraining));

			inspection.setManagerCertificateId(RestUtil.addListItem(inspection.getManagerCertificateId(), certNumber));
			// inspection.setManagerCertificateExpirationStr(RestUtil.addListItem(inspection.getManagerCertificateExpirationStr(), expStr));
			if ( !expStr.startsWith("-") && !("".equals(expStr)) ) {
				String dtformat = HS_Util.getAppSettingString("inspDateFormat");
				Date expDate = RestUtil.cvrtJsonDate(expStr);
				inspection.setManagerCertificateExpirationStr(RestUtil.addListItem(inspection.getManagerCertificateExpirationStr(), HS_Util.formatDate(expDate, dtformat)));
			} else {
				inspection.setManagerCertificateExpirationStr(RestUtil.addListItem(inspection.getManagerCertificateExpirationStr(), ""));
			}

			jw.addProperty("certifiedManager", inspection.getManager().toString());
			jw.addProperty("certifiedMgrNumber", inspection.getManagerCertificateId().toString());
			jw.addProperty("certifiedMgrExpires", inspection.getManagerCertificateExpirationStr().toString());

			return "";
		} catch (Exception e) {
			jw.addProperty("error", e.toString());
			jw.addProperty("errorRoutine", msgContext);
			return e.toString();
		}
	}
}
