package com.healthspace.rest;

import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.Item;
import org.openntf.domino.Session;
import org.openntf.domino.View;
import org.openntf.domino.ViewEntry;
import org.openntf.domino.utils.Factory;

import com.google.gson.Gson;
import com.healthspace.general.AgentLogger;
import com.healthspace.general.HS_BusinessObjects;
import com.healthspace.general.HS_Util;
import com.healthspace.general.Inspection;
import com.healthspace.tools.JSFUtil;
import com.ibm.commons.util.io.json.JsonException;
import com.ibm.commons.util.io.json.JsonJavaFactory;
import com.ibm.commons.util.io.json.JsonParser;
import com.ibm.xsp.extlib.util.ExtLibUtil;

public class PostServices {

	private String				auth			= "";
	// private Database eidb = null;
	private String				batch			= "";
	private String				clientID		= "";
	private String				device			= "";
	private String				deviceUnid		= "";
	private String				dname			= "";
	private String				errormsg		= "";

	private String				logMsgs			= "";
	private String				photoData		= "";
	private String				platform		= "";

	private Map<String, Object>	recmap			= null;

	private int					recordCount		= -1;
	private int					recordNum		= -1;

	private boolean				saveDocs		= false;

	// public Database getEidb() {
	// if (eidb == null) {
	// HS_database hsdb = new HS_database();
	// eidb = hsdb.getdb("eiRoot");
	// }
	// return eidb;
	// }

	private String				schema			= "";

	private String				unid			= "";

	private String				user			= "";
	private String				userID			= "";
	private String				uuid			= "";
	private String				version			= "";
	private String				fullname		= "";
	private StringWriter		batchInspections;
	private Date				startDateTime;

	private boolean				debugOn			= false;

	private List<String>		photoUnids		= new ArrayList<String>();
	private List<String>		resultSections	= new ArrayList<String>();

	public PostServices() {

	}

	private String determineBatch(int recnum, int reccount, String buser, String bschema) {
		if ( recnum == -1 ) return "BADBATCH:No recordNum";
		if ( reccount == -1 ) return "BADBATCH:No recordCount";
		String wbatch = "";
		if ( recnum == 1 ) {
			Date wdt = new Date();
			wbatch = buser;
			wbatch += ":";
			wbatch += ("schLogData".equalsIgnoreCase(bschema)) ? "log:" : "";
			// wbatch += (wdt.getTime());
			wbatch += HS_Util.formatDate(wdt, "yyyy-MM-dd HH:mm:ss:SSS");

			if ( "schLogData".equalsIgnoreCase(bschema) ) {
				ExtLibUtil.getSessionScope().put("RestPostLogBatch", wbatch);
			} else {
				ExtLibUtil.getSessionScope().put("RestPostBatch", wbatch);
			}
		} else {
			wbatch = (String) ExtLibUtil.getSessionScope().get("RestPostBatch");
			if ( "".equals(wbatch) ) {
				wbatch = "No Batch in sessionScope for record " + recnum + " of " + reccount + " user: " + buser;
				System.out.print(this.getClass().getName() + ".determineBatch: " + wbatch);
			}
		}
		return wbatch;
	}

	@SuppressWarnings("unchecked")
	public String dopost(PostBean postbean) {
		String msgContext = this.getClass().getName() + ".dopost";
		StringWriter sw = new StringWriter();
		HS_JsonWriter jw = new HS_JsonWriter(sw, false);

		// int recordNum = -1;
		// int recordCount = -1;
		// String batch = "";
		startDateTime = new Date();
		this.logMsgs = "Processing started at " + new Date().toString() + "\n";
		try {

			jw.startObject();
			jw.addProperty("xsource", msgContext);
			jw.addProperty("version", "2018-01-10-004");
			jw.addProperty("debugOn", ((this.isDebugOn()) ? "true" : "false"));

			resultSections = HS_Util.getAppSettingsValues("HSTouchPostReponseSections");
			if ( this.isDebugOn() ) jw.addProperty("resultSections", resultSections);

			HttpServletRequest req = (HttpServletRequest) JSFUtil.getFacesContext().getExternalContext().getRequest();

			if ( resultSections.contains("parameterNames") ) {
				jw.startProperty("parameterNames");
				jw.startArray();
				for (Enumeration<String> e = req.getParameterNames(); e.hasMoreElements();) {
					// System.out.println(e.nextElement());
					String ename = e.nextElement();
					jw.addArrayItem(ename);
					if ( "RecordNum".equalsIgnoreCase(ename) ) recordNum = Integer.parseInt(req.getParameter(ename));
					if ( "RecordCount".equalsIgnoreCase(ename) ) recordCount = Integer.parseInt(req.getParameter(ename));
					if ( "schema".equalsIgnoreCase(ename) ) this.schema = req.getParameter(ename);
					if ( "user".equalsIgnoreCase(ename) ) this.user = req.getParameter(ename);
				}
				jw.endArray();
				jw.endProperty();
			}

			this.batch = this.determineBatch(recordNum, recordCount, user, schema);
			if ( "Yes".equals(HS_Util.getAppSettingString("Debug_HSTPostBean")) ) System.out.print(msgContext + ": Processing record " + recordNum + " of " + recordCount + " in batch: [" + this.batch + "] ");
			jw.addProperty("recordnum", "" + recordNum);
			jw.addProperty("recordcount", "" + recordCount);
			jw.addProperty("batchkey", this.batch);
			jw.addProperty("schema", schema);

			try {
				this.processMap(req, jw, recordNum, recordCount, postbean, "schPhotos".equals(schema));
			} catch (Exception e) {
				// HS_Util.debug(e.toString(), "error", msgContext);
				System.out.print(msgContext + ": ERROR batch map for [" + this.batch + "] - " + e.toString());
				if ( "Yes".equals(HS_Util.getAppSettingString("Debug_HSTPostBean")) ) e.printStackTrace();
				jw.addProperty("error", e.toString());
				jw.addProperty("errorRoutine", msgContext);
				// if (recordNum == 1) e.printStackTrace();
				// return "{\"error\":\"" + e.toString() + " in " + msgContext +
				// "\"}";
			}

			if ( "schLogData".equalsIgnoreCase(schema) ) {
				// this.saveDocs =
				// "Yes".equals(HS_Util.getAppSettingString("HSTouchLogEntries"));
				this.saveDocs = true;
			} else if ( "schPhotos".equalsIgnoreCase(schema) ) {
				this.saveDocs = true;
			} else {
				// this.saveDocs = ((this.isDebugOn() &&
				// !("schLogData".equalsIgnoreCase(schema))) ||
				// "schPhotos".equalsIgnoreCase(schema));
				// this.saveDocs = (this.isDebugOn());
				this.saveDocs = false;
			}

			jw.addProperty("savedocs", ((this.saveDocs) ? "true" : "false"));
			this.unid = "";
			// No longer saving at this point. All saves happen after the last
			// element of a batch is uploaded
			// boolean saveOk = (this.saveDocs) ?
			// (this.saveOutputDoc(this.recmap, jw)) : true;
			boolean saveOk = ("schPhotos".equalsIgnoreCase(this.schema)) ? savePhotoDocument(this.recmap, jw) : true;

			boolean logprocess = this.saveDocs;
			if ( saveOk ) {
				if ( "schLogData".equalsIgnoreCase(schema) ) {
					logprocess = false;
					this.logMsgs = RestUtil.addDebugProperties(jw, sw, resultSections);

					this.logMsgs += "\n}\n\n" + HS_Util.emsecs(startDateTime);
					// RestUtil.writeToAgentLog(this.logMsgs, "Silas Garrison",
					// "HSTouch.LogEntry", "Unknown");
					try {
						AgentLogger alog = new AgentLogger();
						alog.init(HS_Util.getSessionAsSigner(), "Silas Garrison", "HSTouch.LogEntry", false, "Unknown");
						alog.log(this.logMsgs, true);
						alog.setStatus("Failed");
						alog.failure("Error reported by HSTouch: ");
					} catch (Exception e) {
						System.out.print("RestUtil.writeToAgentLog caught error: " + e.toString());
					}
					jw.addProperty("status", "Log entry written");
					jw.endObject();
					return sw.toString();
				} else if ( recordCount == recordNum ) {
					logprocess = true; // Always log the last document in the
					// batch.
					if ( this.processBatch(jw, postbean) ) {
						jw.addProperty("status", "transaction complete");
						this.endDeviceLog(jw, "transaction complete");
					} else {
						jw.addProperty("status", "ERROR: transaction failed : " + this.errormsg);
						this.endDeviceLog(jw, "ERROR: transaction failed : " + this.errormsg);
					}
				} else {
					logprocess = this.isDebugOn();
					jw.addProperty("status", "transaction in process");
					if ( recordNum == 1 ) this.startDeviceLog(jw);
				}
			} else {
				jw.addProperty("status", "ERROR document save failed");
				logprocess = true;
			}

			if ( logprocess ) {
				this.logMsgs += RestUtil.addDebugProperties(jw, sw, resultSections);
				this.logMsgs += "\n}\n\n" + msgContext + " completed.\n\n" + HS_Util.emsecs(startDateTime);
				if ( (recordCount == recordNum) || "Yes".equals(HS_Util.getAppSettingString("Debug_HSTTransLog")) ) RestUtil.writeToAgentLog(this.logMsgs, msgContext);
				if ( !"".equals(this.unid) ) this.writeResults();
			}

			try {
				postbean.addResults(batch, recordNum, sw.toString() + "\n}");
				// always save on the last post in the batch...
				if ( (this.recordNum == this.recordCount) ) {
					if ( this.isDebugOn() ) System.out.print(this.getClass().getName() + ".dopost - Saving the batch documents.");
					postbean.saveBatch(this.batch);
				}
			} catch (Exception ex) {
				RestUtil.writeWarningToAgentLog("Failure during update of postbean", "Henry Newberry/Office/HealthSpace", this.getClass().getName() + ".dopost.ERROR", "Unknown");
				jw.addProperty("PostBeanSaveError", "Failure during update of postbean" + ex.toString());
			}
			jw.endObject();
			return sw.toString();
		} catch (IOException e) {
			// HS_Util.debug(e.toString(), "error", msgContext);
			return "{\"error\":\"" + e.toString() + " in " + msgContext + "\",\"status\":\"error\"}";
		} catch (Exception e1) {
			// HS_Util.debug(e1.toString(), "error", msgContext);
			if ( this.isDebugOn() ) e1.printStackTrace();
			return "{\"error\":\"" + e1.toString() + " in " + msgContext + "\",\"status\":\"error\"}";
		}
	}

	private void endDeviceLog(HS_JsonWriter jw, String status) throws IOException {
		final String msgContext = "restPostBasic.endDeviceLog";
		try {
			this.getDeviceInfo();
			if ( "".equals(uuid) ) {
				jw.addProperty("deviceLogDebug", "No uuid found so device log not created. in " + msgContext);
				return;
			}

			Document doc = getDeviceDoc(uuid);
			doc.replaceItemValue("devicePlatform", platform);
			doc.replaceItemValue("deviceVersion", version);
			doc.replaceItemValue("deviceName", dname);

			this.getAuthInfo();
			doc.replaceItemValue("authUserID", userID);
			doc.replaceItemValue("authClientID", clientID);
			doc.replaceItemValue("authFullname", fullname);

			doc.replaceItemValue("syncLastDate", new Date());
			doc.replaceItemValue("syncLastUser", user);
			doc.replaceItemValue("syncLastStatus", status);

			int permits = getPermitCount();
			if ( "transaction complete".equals(status) ) {
				doc.replaceItemValue("syncLastRecords", recordCount);
				doc.replaceItemValue("syncLastPermits", permits);
			} else {
				doc.replaceItemValue("syncLastRecords", 0);
				doc.replaceItemValue("syncLastPermits", permits);
			}
			// 2015-01-15 10:01:01 AM PST!Henry
			// Newberry~20~8~status~clientID~userID

			String history = HS_Util.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss z");
			history += "!" + this.user;
			history += "~" + recordCount;
			history += "~" + permits;
			history += "~" + status;
			history += "~" + clientID;
			history += "~" + userID;
			Vector<Object> newHist = new Vector<Object>();
			newHist.add(history);

			Vector<Object> oldHist = doc.getItemValue("syncHistory");
			if ( oldHist.size() == 0 || "".equals(oldHist.get(0)) ) {
				// nothing to do here
			} else {
				for (Iterator<Object> itr = oldHist.iterator(); itr.hasNext();) {
					newHist.add(itr.next());
				}
			}
			doc.replaceItemValue("syncHistory", newHist);

			boolean result = doc.save();
			this.deviceUnid = doc.getUniversalID();
			jw.addProperty("endDeviceLogDebug3", "Save result:[" + result + "] unid:[" + this.deviceUnid + "] user:[" + Factory.getSession().getEffectiveUserName() + "]");

		} catch (Exception e) {
			// HS_Util.debug(e.toString(), "error", msgContext);
			jw.addProperty("startDeviceLogError", e.toString());
		}

	}

	@SuppressWarnings("unchecked")
	private void getAuthInfo() throws JsonException {
		Map<String, String> authjs = new HashMap<String, String>();
		authjs = (Map<String, String>) JsonParser.fromJson(JsonJavaFactory.instance, this.auth);
		Iterator<String> authitr = authjs.keySet().iterator();
		while (authitr.hasNext()) {
			String fname = authitr.next();
			// String pname = rname + "_" + fname.toLowerCase();
			Object fvalue = authjs.get(fname);
			if ( "clientid".equalsIgnoreCase(fname) ) clientID = (String) fvalue;
			if ( "userid".equalsIgnoreCase(fname) || "userid".equalsIgnoreCase(fname) ) userID = (String) fvalue;
			if ( "fullname".equalsIgnoreCase(fname) ) fullname = (String) fvalue;
		}
	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> getChildrenMaps(Map<String, Object> batchmap, String targetKey, String targetKeyName) {
		Map<String, Object> result = new HashMap<String, Object>();
		for (Entry<String, Object> e : batchmap.entrySet()) {
			if ( e.getKey().startsWith("rec") && e.getValue() != null ) {

				Map<String, Object> rmap = ((Map<String, Object>) e.getValue());
				if ( rmap.containsKey(targetKeyName) && targetKey.equals(rmap.get(targetKeyName)) ) {
					result.put(e.getKey(), rmap);
				}
			}
		}
		return result;
	}

	private Document getDeviceDoc(String uuid) {
		Session sess = Factory.getSession();
		Database db = sess.getCurrentDatabase();
		View deviceView = db.getView("code.devices.by.uuid");
		Document doc = deviceView.getFirstDocumentByKey(uuid, true);
		if ( doc == null ) {
			doc = db.createDocument();
			doc.replaceItemValue("Form", "tablet.device");
			doc.replaceItemValue("documentId", sess.evaluate("@Unique"));
			doc.replaceItemValue("deviceUuid", uuid);
			doc.replaceItemValue("initializedDate", new Date());
			doc.replaceItemValue("syncHistory", "");
			// System.out.print(msgContext +
			// " - created tablet.device document.");
		}
		return doc;
	}

	@SuppressWarnings("unchecked")
	private void getDeviceInfo() throws JsonException {
		Map<String, String> devicejs = new HashMap<String, String>();
		devicejs = (Map<String, String>) JsonParser.fromJson(JsonJavaFactory.instance, this.device);
		Iterator<String> devitr = devicejs.keySet().iterator();
		while (devitr.hasNext()) {
			String fname = devitr.next();
			// String pname = rname + "_" + fname.toLowerCase();
			String fvalue = devicejs.get(fname);
			if ( "uuid".equalsIgnoreCase(fname) ) uuid = fvalue;
			if ( "platform".equalsIgnoreCase(fname) ) platform = fvalue;
			if ( "version".equalsIgnoreCase(fname) ) version = fvalue;
			if ( "name".equalsIgnoreCase(fname) ) dname = fvalue;
		}
	}

	private String getDocumentId(String schema) {
		String docid = "";

		if ( "schLogData".equals(schema) ) {
			docid = (String) HS_Util.getSession().evaluate("@Unique").get(0);
		} else {
			docid = ("schMisc_Permit".equals(schema)) ? (String) recmap.get("restpermitid") : ("schInspection".equals(schema)) ? (String) recmap.get("restinspectionid") : ("schInspection_Violation"
									.equals(schema)) ? (String) recmap.get("restviolationid") : "unknownDocSchema:" + schema;
		}
		if ( docid.startsWith("unknownDocSchema") ) {
			Iterator<String> ritr = recmap.keySet().iterator();
			while (ritr.hasNext()) {
				String fname = ritr.next();
				if ( fname.endsWith("id") ) {
					docid = (String) recmap.get(fname);
				}
			}
		}
		return docid;
	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> getInspectionRecord(Map<String, Object> batchmap, String ikey) {
		for (Entry<String, Object> e : batchmap.entrySet()) {
			if ( e.getKey().startsWith("rec") && e.getValue() != null ) {
				Map<String, Object> rmap = ((Map<String, Object>) e.getValue());
				String rschema = (String) rmap.get("restschema");
				String docid = (String) rmap.get("documentid");
				if ( "schInspection".equalsIgnoreCase(rschema) && ikey.equals(docid) ) {
					return rmap;
				}
			}
		}
		return null;
	}

	private int getPermitCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	private boolean isDuplicateInspection(String ikey, HS_JsonWriter jw, Database eidb) throws IOException {
		// Database eidb = RestUtil.getEidb();
		View docidview = eidb.getView("(DocumentIDLookup)");
		ViewEntry dupve = docidview.getFirstEntryByKey(ikey, true);
		if ( dupve == null ) return false;
		Document dupdoc = dupve.getDocument();

		jw.addProperty("duplicateInspectionFound", dupdoc.getItemValueString("FacilityName") + " - " + dupdoc.getItemValueString("ViewDescription"));
		return true;
	}

	public String noGet() {
		StringWriter sw = new StringWriter();
		HS_JsonWriter jw = new HS_JsonWriter(sw, false);

		try {
			jw.startObject();
			jw.addProperty("error", "There is no get method to this service.");
			jw.endObject();
		} catch (IOException e) {

		}
		return sw.toString();
	}

	@SuppressWarnings("unchecked")
	private boolean processBatch(HS_JsonWriter jw, PostBean postbean) {
		boolean result = true;
		String msgContext = "PostService.processBatch";
		Database eidb = null;
		try {
			jw.startProperty("batch");
			jw.startObject();

			jw.addProperty("key", this.batch);
			Map<String, Object> batchmap = (Map<String, Object>) postbean.getBatch(this.batch);

			int recctr = 0;
			int nullrecs = 0;
			int inspctr = 0;
			Vector<String> inspkeys = new Vector<String>();
			Vector<String> inspresults = new Vector<String>();
			for (Entry<String, Object> e : batchmap.entrySet()) {
				if ( e.getKey().startsWith("rec") ) {
					if ( e.getValue() != null ) {
						recctr += 1;
						Map<String, Object> rmap = ((Map<String, Object>) e.getValue());
						String rschema = (rmap.containsKey("restschema")) ? (String) rmap.get("restschema") : "null";
						if ( "schInspection".equalsIgnoreCase(rschema) ) {
							inspctr += 1;
							inspkeys.add((String) rmap.get("documentid"));
						}
						// try {
						// jw.addProperty("record_" + recctr + "_schema",
						// rschema);
						// } catch (Exception e1) {
						// jw.addProperty("record_" + recctr + "_schema",
						// "ERROR" + e1.toString());
						// if (HS_Util.isDebugServer()) e1.printStackTrace();
						// }
						if ( "null".equals(rschema) ) nullrecs += 1;
					} else {
						nullrecs += 1;
					}
				}
			}
			jw.addProperty("records", "" + recctr);
			jw.addProperty("nullRecords", "" + nullrecs);
			jw.addProperty("inspctr", "" + inspctr);
			jw.addProperty("inspkeys", inspkeys);

			eidb = RestUtil.getEidb();
			if ( nullrecs > 0 ) {
				this.errormsg = "not all records in batch were uploaded! Expected:[" + recctr + "] received:[" + (recctr - nullrecs) + "]";
				jw.addProperty("error", "ERROR: " + this.errormsg);
				System.out.print("ERROR: " + this.errormsg + " from: " + eidb.getFilePath());
				result = false;
			} else {
				jw.addProperty("userFullName", HS_Util.getSession().getEffectiveUserName());
				// eidb = RestUtil.getEidb();
				if ( eidb == null ) {
					jw.addProperty("dbaccess", "cannot access eiRoot");
				} else {
					jw.addProperty("dbaccess", "" + eidb.getCurrentAccessLevel());
				}

				this.batchInspections = new StringWriter();
				HS_JsonWriter batchjw = new HS_JsonWriter(this.batchInspections, false);
				batchjw.startProperty("inspections");
				batchjw.startArray();
				result = true;
				for (int i = 0; i < inspkeys.size() && result; i++) {
					String ikey = inspkeys.get(i);
					result = this.processInspection(batchmap, ikey, batchjw, eidb);
					inspresults.add((result) ? "saved" : "failed");
				}
				batchjw.endArray();
				batchjw.endProperty();
				batchmap.put("inspections", this.batchInspections.toString());
				jw.addProperty("inspresults", inspresults);
			}
		} catch (IOException e) {
			result = false;
			// HS_Util.debug(e.toString(), "error", msgContext);
			System.out.print(msgContext + " : IOException : " + e.toString());
			try {
				jw.addProperty("status", "error");
				jw.addProperty("error", e.toString() + " in " + msgContext);
				this.recmap.put("error", e.toString() + " in " + msgContext);

			} catch (IOException ignore1) {
			}
			if ( this.isDebugOn() ) e.printStackTrace();
		} catch (Exception e1) {
			result = false;
			// HS_Util.debug(e1.toString(), "error", msgContext);
			System.out.print(msgContext + " : Exception : " + e1.toString());
			try {
				jw.addProperty("status", "error");
				jw.addProperty("error", e1.toString() + " in " + msgContext);
				this.recmap.put("error", e1.toString() + " in " + msgContext);
			} catch (IOException ignore2) {
			}
			if ( this.isDebugOn() ) e1.printStackTrace();
		} finally {
			try {
				jw.endObject();
				jw.endProperty();
			} catch (IOException ignore3) {
			}
		}
		return result;
	}

	private String processChildren(HS_JsonWriter jw, Map<String, Object> childmap, String parentschema, Inspection inspection, Database eidb, int childctr) throws IOException {
		final String msgContext = "PostServices.processChildren";
		String childschema = (String) childmap.get("restschema");
		if ( childschema.equals(parentschema) ) return "";

		String errorMsg = "";
		jw.startArrayItem();
		jw.startObject();
		jw.addProperty("schema", childschema);
		// if ( this.isDebugOn() ) System.out.print(msgContext +
		// ": - processing child:[" + childctr + "] " + childschema);

		try {
			if ( "schInspection_Violation".equalsIgnoreCase(childschema) ) {
				errorMsg += PostViolation.processViolationSection(jw, childmap, inspection, eidb, this.isDebugOn());
			} else if ( "schTab_Certified_Managers".equalsIgnoreCase(childschema) ) {
				errorMsg += PostManager.processManager(jw, childmap, inspection);
			} else if ( "schTab_Equip_Temps".equalsIgnoreCase(childschema) ) {
				errorMsg += PostTemperatures.processEquipmentTemps(jw, childmap, inspection);
			} else if ( "schTab_Equipment_Temperatures".equalsIgnoreCase(childschema) ) {
				errorMsg += PostTemperatures.processEquipmentTemps(jw, childmap, inspection);
			} else if ( "schTab_Food_Temps".equalsIgnoreCase(childschema) || "schInspection_Temperature".equalsIgnoreCase(childschema) ) {
				errorMsg += PostTemperatures.processFoodTemps(jw, childmap, inspection);
			} else if ( "schTab_Food_Temperatures".equalsIgnoreCase(childschema) || "schInspection_Temperature".equalsIgnoreCase(childschema) ) {
				errorMsg += PostTemperatures.processFoodTemps(jw, childmap, inspection);
			} else if ( "schTab_Ware_Washing_Temps".equalsIgnoreCase(childschema) ) {
				errorMsg += PostTemperatures.processWareWashingTemps(jw, childmap, inspection);
			} else if ( "schTab_Warewashing_Info".equalsIgnoreCase(childschema) ) {
				errorMsg += PostTemperatures.processWareWashingTemps(jw, childmap, inspection);
			} else if ( "schTab_Time_Report".equalsIgnoreCase(childschema) || "schTab_Time_Reports".equalsIgnoreCase(childschema) ) {
				errorMsg += PostTimeReport.processTimeReport(jw, childmap, inspection);
			} else if ( "schTab_Tobacco_Information".equalsIgnoreCase(childschema) || "schTab_Tobacco_Report".equalsIgnoreCase(childschema) ) {
				errorMsg += PostTobaccoInfo.processTobaccoInfo(jw, childmap, inspection);
			} else if ( "schPhotos".equals(childschema) ) {
				// jw.addProperty("photosUploaded", this.photoUnids);
				errorMsg += PostPhoto.processPhoto(jw, childmap, inspection);
			} else if ( "schTab_Certified_Professional".equals(childschema) ) {
				// jw.addProperty("photosUploaded", this.photoUnids);
				errorMsg += PostCertifiedProfessional.processCertifiedProfessional(jw, childmap, inspection);
			} else {
				// jw.addProperty("error", "Unknown SCHEMA in inspection: " + childschema);
				errorMsg += "Unknown SCHEMA in inspection: " + childschema;
			}
			if ( !"".equals(errorMsg) ) {
				jw.addProperty("error", errorMsg);
				errorMsg += " on schema: " + childschema;
				System.out.print(msgContext + " ERROR: " + errorMsg);
			}
		} catch (Exception e) {
			errorMsg = msgContext + " : " + e.toString();
			errorMsg += " on schema: " + childschema;
			System.out.print(msgContext + " ERROR: " + errorMsg);
			jw.addProperty("error", e.toString());
			jw.addProperty("errorRoutine", msgContext);
		}
		jw.endObject();
		jw.endArrayItem();
		return errorMsg;
	}

	@SuppressWarnings("unchecked")
	private boolean processInspection(Map<String, Object> batchmap, String ikey, HS_JsonWriter jw, Database eidb) throws IOException {
		boolean result = false;
		boolean localDebug = this.isDebugOn();
		final String msgContext = "PostServices.processInspection";
		if ( localDebug ) System.out.print(msgContext + " - working on inspection key \"" + ikey + "\"");
		jw.startArrayItem();
		jw.startObject();
		jw.addProperty("inspectionId", ikey);
		Map<String, Object> imap = this.getInspectionRecord(batchmap, ikey);
		String signature1 = "";
		String signature2 = "";
		// String childErrors = "";
		String childError = "";
		List<String> childErrors = new ArrayList();
		if ( imap == null ) {
			jw.addProperty("error", "Inspection record not found");
			if ( localDebug ) System.out.print(msgContext + " - Inspection record not found for key: \"" + ikey + "\"");
		} else {
			try {
				// Set<String> imapkeys = imap.keySet();
				String facunid = this.setInspValue(imap, "permitid", "");
				if ( "".equals(facunid) ) facunid = this.setInspValue(imap, "locationid", "");
				jw.addProperty("facilityUnid", facunid);
				if ( localDebug ) System.out.print(msgContext + " - facunid:[" + facunid + "]");

				String dtformat = HS_Util.getAppSettingString("inspDateFormat");
				String idatestr = this.setInspValue(imap, "inspectiondate", "");
				// Date idate = HS_Util.cvrtStringToDate(idatestr);
				Date idate = RestUtil.cvrtJsonDate(idatestr);
				jw.addProperty("inspectionDate", idatestr);
				jw.addProperty("inspDateCvrted", HS_Util.formatDate(idate, dtformat));

				Inspection inspection = new Inspection();
				inspection.clearInspection();
				inspection.setTabletDebug(this.isDebugOn());
				inspection.setEnvironment("TabletApp");
				inspection.setHSTouch(true);
				inspection.setDocumentId(ikey);
				inspection.setParentUnid(facunid, localDebug);
				// inspection.setInspectionDate(new Date());
				inspection.setInspectionDateStr(HS_Util.formatDate(idate, dtformat));
				// String dbDesign =
				// HS_Util.getMasterSettingsString("DBDesign");
				String itype = this.setInspValue(imap, "inspectiontype", "");
				inspection.setInspType(itype);
				jw.addProperty("inspectionType", itype);

				Double lockednum = new Double(0);
				lockednum = this.setInspValue(imap, "finalized", lockednum);
				String locked = (lockednum == 1) ? "Yes" : "No";
				inspection.setLocked(locked);
				jw.addProperty("inspectionlocked", locked);

				inspection.setUseTobacco("TDA".equals(inspection.getDbDesign()));

				// inspection.setEho((imapkeys.contains("record_eho")) ?
				// (String) imap.get("record_eho") : "none");
				inspection.setEho(this.setInspValue(imap, "eho", ""));
				inspection.setCertifiedManager(this.setInspValue(imap, "certifiedmanager", ""));
				if ( imap.containsKey("record_comments") ) {
					inspection.setComments(this.setInspValue(imap, "record_comments", ""));
					jw.addProperty("commentsFrom", "comments:[" + imap.get("record_comments") + "]");
				} else if ( imap.containsKey("record_generalcomments") ) {
					inspection.setComments(this.setInspValue(imap, "record_generalcomments", ""));
					jw.addProperty("commentsFrom", "generalcomments:[" + imap.get("record_comments") + "]");
				} else {
					jw.addProperty("commentsFrom", "none found");
				}
				inspection.setEnforcement(this.setInspValue(imap, "enforcement", ""));
				inspection.setFollowupInspectionRequired(this.setInspValue(imap, "followupinspectionrequired", ""));
				jw.addProperty("followupInspectionRequired", inspection.getFollowupInspectionRequired());
				inspection.setFoodSafe(this.setInspValue(imap, "foodsafe", ""));

				if ( imap.containsKey("record_score") ) {
					inspection.setTotHazardRating(this.setInspValue(imap, "score", 100));
					jw.addProperty("totHazardRating", Integer.valueOf(inspection.getTotHazardRating()).toString());
					// this.logMsgs += "\ntotHazardRating set to:" +
					// Integer.valueOf(inspection.getTotHazardRating()).toString();
				}

				inspection.setNextInspectionDateStr("");
				idatestr = this.setInspValue(imap, "nextinspection", "");
				if ( !"".equals(idatestr) ) {
					idate = RestUtil.cvrtJsonDate(idatestr);
					inspection.setNextInspectionDateStr(HS_Util.formatDate(idate, dtformat));
					jw.addProperty("nextInspection", idate);
					jw.addProperty("nextInspectionStr", idatestr);
					jw.addProperty("nextInspectionFormatted", HS_Util.formatDate(idate, dtformat));
				} else {
					jw.addProperty("nextInspectionDate", "");
				}

				signature1 = setInspValue(imap, "signaturedata_1", "");
				signature2 = setInspValue(imap, "signaturedata_2", "");
				inspection.setSignatureData1((signature1.length() == 0) ? "" : "canvas codes not used.");
				inspection.setSignatureData2((signature2.length() == 0) ? "" : "canvas codes not used.");
				inspection.setSignatureData64_1(signature1);
				inspection.setSignatureData64_2(signature2);

				jw.addProperty("signatureLengths", "1:[" + signature1.length() + "] 2:[" + signature2.length() + "] total:[" + (signature1.length() + signature2.length()) + "]");
				if ( this.isDebugOn() )
					System.out.print((msgContext + ": - signatureLengths 1:[" + signature1.length() + "] 2:[" + signature2.length() + "] total:[" + (signature1.length() + signature2.length()) + "]"));

				String oseating = this.setInspValue(imap, "observedseating", "");
				if ( !"".equals(oseating) ) {
					inspection.setObservedSeating(Integer.parseInt(oseating));
					jw.addProperty("observedSeating", oseating);
				}

				jw.addProperty("facilityName", inspection.getFacilityName());

				inspection.setFullCloudMap(imap);
				this.updateInspectionFromMap(inspection, imap, jw);
				// jw.addProperty("fullCloudMapKeys", imap.keySet());

				if ( isDuplicateInspection(ikey, jw, eidb) ) {
					result = true;
					jw.addProperty("inspectionSaved", "false");
					jw.addProperty("validationErrors", "");
					this.errormsg = "Duplicate inspection unique ID received. NOT SAVING.";
					System.out.print(this.errormsg);
				} else {
					Map<String, Object> cmaps = this.getChildrenMaps(batchmap, ikey, "record_inspectionid");
					if ( cmaps != null ) {
						jw.addProperty("childcount", "" + cmaps.size());
						if ( this.isDebugOn() ) System.out.print(msgContext + ": - childCount:" + cmaps.size());
						int childctr = 0;
						jw.startProperty("children");
						jw.startArray();
						for (Entry<String, Object> e : cmaps.entrySet()) {
							try {
								childctr++;

								childError = this.processChildren(jw, (Map<String, Object>) e.getValue(), (String) imap.get("restschema"), inspection, eidb, childctr);
								// childErrors += childError;
								// childErrors[childErrors.length] = childError;
								if ( !"".equals(childError) ) childErrors.add(childError);
								if ( !("".equals(childError)) && localDebug ) System.out.print(msgContext + ",processChildren - ERROR: " + childError);
							} catch (Exception e2) {
								jw.addArrayItem(e2.toString());
								if ( localDebug ) System.out.print(msgContext + ",processChildren - ERROR: " + e2.toString());
							}
						}
						jw.endArray();
						jw.endProperty();
					}
					// jw.addProperty("inspViolationsOrder",
					// HS_Util.getAppSettingString("inspViolationsOrder"));
					inspection.sortSections();
					inspection.sortViolations();

					HS_BusinessObjects bo = HS_BusinessObjects.getInstance();
					bo.setDebugTablet(this.isDebugOn());

					try {
						// if ( "".equals(childErrors) ) {
						if ( childErrors.size() == 0 ) {
							result = inspection.save(bo);
						} else {
							jw.addProperty("error", childErrors);
							jw.addProperty("errorRoutine", msgContext + ".inspection.save(bo)");

							result = false;
						}
					} catch (Exception e4) {
						jw.addProperty("error", e4.toString());
						jw.addProperty("errorRoutine", msgContext + ".inspection.save(bo) - trapped.");
						System.out.print(msgContext + ".inspection.save(bo) - trapped: " + e4.toString());
						e4.printStackTrace();
						result = false;
					}
					// result = false;
					// inspection.getValidationMessages().add("Not saving for debug reasons");
					if ( result ) {
						jw.addProperty("inspectionSaved", "true");
						jw.addProperty("validationErrors", inspection.getValidationMessages());
						if ( this.isDebugOn() ) {
							jw.addProperty("inspectionUnid", inspection.getUnid());
							jw.addProperty("inspectionDocid", inspection.getDocumentId());
							System.out.print("Saved inspection for facility:[" + inspection.getFacilityName() + "] unid:[" + inspection.getUnid() + "] docid:[" + inspection.getDocumentId() + "]");
							jw.addProperty("repeatC", inspection.getRepeatC());
							jw.addProperty("repeatK", inspection.getRepeatK());
							jw.addProperty("repeatO", inspection.getRepeatO());

						}
					} else {
						jw.addProperty("inspectionSaved", "false");
						jw.addProperty("validationErrors", inspection.getValidationMessages());
						this.errormsg = "FAILED TO SAVE inspection for facility:[" + inspection.getFacilityName() + "] - ";
						// if (
						// !"".equals(inspection.getValidationMessages().get(0))
						// ) this.errormsg +=
						// inspection.getValidationMessages().toString();
						if ( inspection.getValidationMessages().size() > 0 ) this.errormsg += inspection.getValidationMessages().toString();
						if ( childErrors.size() > 0 ) this.errormsg += childErrors.toString();
						if ( this.isDebugOn() ) System.out.print("SAVE ERRORS: " + this.errormsg);
					}
				}
			} catch (Exception e3) {
				jw.addProperty("error", e3.toString());
				jw.addProperty("errorRoutine", msgContext);
				if ( localDebug ) System.out.print(msgContext + " - ERROR: " + e3.toString());
				if ( this.isDebugOn() ) e3.printStackTrace();
			}
		}
		jw.endObject();
		jw.endArrayItem();
		return result;
	}

	private void updateInspectionFromMap(Inspection insp, Map<String, Object> imap, HS_JsonWriter jw) throws IOException {
		// final String msgContext = this.getClass().getName() +
		// ".updateInspectionFromMap";
		String mname = "";
		Method bmeth = null;
		try {
			List<String> skipThese = new ArrayList<String>();
			List<String> savedThese = new ArrayList<String>();
			skipThese.add("followupinspectionrequired");
			skipThese.add("inspectiondate");
			skipThese.add("eho");

			Method[] methods = insp.getClass().getDeclaredMethods();
			List<String> notFound = new ArrayList<String>();

			boolean foundmname = false;
			for (String key : insp.getFullCloudMap().keySet()) {
				if ( key.startsWith("record_") ) {
					String fname = key.substring(7);
					if ( !skipThese.contains(fname.toLowerCase()) ) {
						mname = "set" + fname;
						foundmname = false;
						for (Method meth : methods) {
							if ( meth.getName().toLowerCase().equals(mname.toLowerCase()) ) {
								Object fcv = insp.getFullCloudMap().get(key);
								bmeth = meth;
								java.lang.reflect.Type[] txs = bmeth.getGenericParameterTypes();
								if ( txs[0].toString().contains("Integer") ) {
									Integer fci = Integer.parseInt(fcv.toString());
									meth.invoke(insp, fci);
									savedThese.add(fname);
								} else {
									// Now we think it is a String
									try {
										meth.invoke(insp, fcv.toString());
										savedThese.add(fname);
									} catch (Exception ie) {
										String emsg = "PostServices.updateInspFromMap Error: " + ie.toString() + " on method:[" + meth.getName() + "]";
										jw.addProperty("updateInspFromMap_Error", ie.toString() + " on method:[" + meth.getName() + "]");
										RestUtil.sendAlert("Error in PostServices.updateInspFromMap", emsg, "ParameterType: [" + txs[0].toString() + "]", ie);
									}
								}
								foundmname = true;
							}
						}
						if ( !foundmname ) notFound.add(fname);
					}
				}
			}
			jw.addProperty("updateInspFromMap", savedThese);
			if ( notFound.size() > 0 ) jw.addProperty("updateInspFromMapNotFound", notFound);
		} catch (Exception e) {
			jw.addProperty("updateInspFromMap_Error", e.toString() + " on method:[" + bmeth.getName() + "]");
		}
	}

	@SuppressWarnings("unchecked")
	private boolean processMap(HttpServletRequest req, HS_JsonWriter jw, int recnum, int reccount, PostBean postbean, boolean isPhoto) throws Exception {
		boolean result = false;
		String msgContext = "RestPostBasic.processMap";
		String schema = "not found";
		String record = "{\"status\":\"record parameter not found\"}";
		device = "{}";
		auth = "{}";
		Map<String, Object> batchmap = null;
		try {
			if ( recnum == 1 ) {
				batchmap = postbean.newBatch(this.batch, reccount);
			} else {
				batchmap = (Map<String, Object>) postbean.getBatch(this.batch);
			}
			if ( batchmap == null ) {
				HS_Util.debug("Could not get a map for [" + this.batch + "]", "error", msgContext);
			} else {
				String wnum = "0000" + recnum;
				String reckey = "rec" + (wnum).substring(wnum.length() - 4);
				this.recmap = (Map<String, Object>) batchmap.get(reckey);
				String pname = "";
				Vector<String> pnames = new Vector<String>();
				// String[] pnames = [];
				for (Enumeration<String> e = req.getParameterNames(); e.hasMoreElements();) {
					pname = e.nextElement();
					pnames.add(pname);
					this.recmap.put(("rest" + pname).toLowerCase(), req.getParameter(pname));
					if ( "schema".equalsIgnoreCase(pname) ) schema = req.getParameter(pname);
					if ( "record".equalsIgnoreCase(pname) ) record = req.getParameter(pname);
					if ( "auth".equalsIgnoreCase(pname) ) auth = req.getParameter(pname);
					if ( "device".equalsIgnoreCase(pname) ) device = req.getParameter(pname);
				}
				this.recmap.put("postitems", pnames);
				this.recmap.put("documentid", this.getDocumentId(schema));

				try {
					this.putJsonData("record", record, jw, recnum, isPhoto);
					if ( resultSections.contains("auth") ) this.putJsonData("auth", auth, jw, recnum);
					if ( resultSections.contains("device") ) this.putJsonData("device", device, jw, recnum);
					result = true;
				} catch (Exception e) {
					HS_Util.debug(e.toString(), "error", msgContext);
					System.out.println(msgContext + ": ERROR: " + e.toString());
					if ( this.isDebugOn() && recnum == 1 ) e.printStackTrace();
					try {
						jw.addProperty("status", "error");
						jw.addProperty("error", e.toString() + " in " + msgContext);
						this.recmap.put("error", e.toString() + " in " + msgContext);
					} catch (IOException e1) {
					}
				}
			}
		} catch (Exception e3) {
			if ( "Yes".equals(HS_Util.getAppSettingString("Debug_HSTPostBean")) ) e3.printStackTrace();
			System.out.println(msgContext + ": ERROR: " + e3.toString());
		}
		return result;
	}

	private void putJsonData(String rname, String record, HS_JsonWriter jw, int recnum) {
		this.putJsonData(rname, record, jw, recnum, false);
	}

	@SuppressWarnings( { "unchecked" })
	private void putJsonData(String rname, String record, HS_JsonWriter jw, int recnum, boolean isPhoto) {
		String msgContext = "PostServices.putJsonData." + rname;
		Object fvalue = null;
		try {
			Map<String, String> recjs = new HashMap<String, String>();
			recjs = (Map<String, String>) JsonParser.fromJson(JsonJavaFactory.instance, record);
			Iterator<String> recitr = recjs.keySet().iterator();
			jw.startProperty(rname);
			jw.startObject();
			while (recitr.hasNext()) {
				String fname = recitr.next();
				String pname = rname + "_" + fname.toLowerCase();
				if ( "record_[object object]".equals(pname) ) {
					pname = "record_object_object";
					fvalue = "";
				} else {
					fvalue = recjs.get(fname);
				}
				if ( pname.toLowerCase().startsWith("record_signaturedata_") ) {
					if ( this.isDebugOn() ) System.out.print(msgContext + ": " + pname + " size:[" + fvalue.toString().length() + "]");
					this.recmap.put(pname, fvalue);
				} else {
					this.recmap.put(pname, fvalue);
				}
				if ( isPhoto ) {
					if ( !"photodata".equalsIgnoreCase(fname) ) {
						jw.addProperty(pname, fvalue.toString());
					} else {
						System.out.println(msgContext + ": For photo item:[" + pname + "] is a:[" + fvalue.getClass().getName() + "]");
						jw.addProperty(pname, "Class:[" + fvalue.getClass().getName() + "] length:[" + ((String) fvalue).length() + "]");
						if ( fvalue.toString().startsWith("data:") ) {
							// trimmming off the "data:image/png;base64," from the start..
							int commaPos = fvalue.toString().indexOf(",");
							this.photoData = fvalue.toString().substring(commaPos + 1);
						} else {
							this.photoData = (String) fvalue;
						}
					}
				} else {
					jw.addProperty(pname, fvalue.toString());
				}
			}
			jw.endObject();
			jw.endProperty();
		} catch (IOException e2) {
			if ( this.isDebugOn() && recnum == 1 ) e2.printStackTrace();
			try {
				jw.addProperty("status", "error");
				jw.addProperty("error", e2.toString() + " in " + msgContext);
				this.recmap.put("error", e2.toString() + " in " + msgContext);
			} catch (IOException e1) {
			}
		} catch (Exception e) {
			if ( this.isDebugOn() && recnum == 1 ) e.printStackTrace();
			try {
				jw.addProperty("status", "error");
				jw.addProperty("error", e.toString() + " in " + msgContext);
				this.recmap.put("error", e.toString() + " in " + msgContext);
			} catch (IOException e1) {
			}
		}
	}

	@SuppressWarnings( { "unchecked", "unused" })
	private void putJsonDataGson(String rname, String record, HS_JsonWriter jw, int recnum) {
		/*
		 * This section ONLY works if the java.policy file on the domino server (<domino>\jvm\lib\security/java.policy) has the following entry:
		 * 
		 * grant { permission java.security.AllPermission; };
		 * 
		 * Otherwise you get the error: java.lang.SecurityException: not allowed to access members in class class java.util.HashMap in RestPostBasic.processMap
		 */
		String msgContext = "PostServices.putJsonDataGson." + rname;
		try {
			Gson gson = new Gson();
			Map<String, String> recjs = new HashMap<String, String>();
			recjs = gson.fromJson(record, recjs.getClass());
			Iterator<String> recitr = recjs.keySet().iterator();
			jw.startProperty(rname);
			jw.startObject();
			while (recitr.hasNext()) {
				String fname = recitr.next();
				String pname = rname + "_" + fname.toLowerCase();
				Object fvalue = recjs.get(fname);
				this.recmap.put(pname, fvalue);
				jw.addProperty(pname, fvalue.toString());
			}
			jw.endObject();
			jw.endProperty();
		} catch (IOException e2) {
			HS_Util.debug(e2.toString(), "error", msgContext);
			if ( this.isDebugOn() && recnum == 1 ) e2.printStackTrace();
			try {
				jw.addProperty("status", "error");
				jw.addProperty("error", e2.toString() + " in " + msgContext);
				this.recmap.put("error", e2.toString() + " in " + msgContext);
			} catch (IOException e1) {
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			if ( this.isDebugOn() && recnum == 1 ) e.printStackTrace();
			try {
				jw.addProperty("status", "error");
				jw.addProperty("error", e.toString() + " in " + msgContext);
				this.recmap.put("error", e.toString() + " in " + msgContext);
			} catch (IOException e1) {
			}
		}
	}

	@SuppressWarnings("unused")
	private boolean saveOutputDoc(Map<String, Object> req, HS_JsonWriter jw) {
		final String msgContext = "restPostBasic.saveOutputDoc.new";
		Session sess = null;
		Database db = null;
		Document doc = null;
		try {
			if ( "schPhotos".equalsIgnoreCase(this.schema) ) {
				return savePhotoDocument(req, jw);
			}
			sess = Factory.getSession();
			db = sess.getCurrentDatabase();
			doc = db.createDocument();
			doc.replaceItemValue("Form", "RestBasicPost");
			doc.replaceItemValue("PostAuthor", sess.getEffectiveUserName());
			doc.replaceItemValue("RestBatch", this.batch);

			if ( req == null ) {
				doc.replaceItemValue("Error", msgContext + " - request map is null!");
			} else if ( req.keySet() == null ) {
				doc.replaceItemValue("Error", msgContext + " - request map keyset is null!");
			} else if ( req.keySet().isEmpty() ) {
				doc.replaceItemValue("Error", msgContext + " - request map keyset is empty!");
			} else if ( req.keySet().iterator() == null ) {
				doc.replaceItemValue("Error", msgContext + " - request map keyset iterator is null!");
			} else {
				Iterator<String> pitr = req.keySet().iterator();
				while (pitr.hasNext()) {
					String fname = pitr.next();
					Object fvalue = req.get(fname);
					if ( "schPhotos".equalsIgnoreCase(this.schema) && "record_photodata".equalsIgnoreCase(fname) ) {
						// skip these - really never should get here
					} else if ( "schPhotos".equalsIgnoreCase(this.schema) && "restrecord".equalsIgnoreCase(fname) ) {
						// skip this for now... skip these - really never should
						// get here
					} else if ( "Auth_Password".equals(fname) ) {
						// security issue. No longer saving this...
					} else {
						doc.replaceItemValue(fname, fvalue);
					}
				}
			}

			Date edt = new Date();
			doc.replaceItemValue("RunTime", (edt.getTime() - startDateTime.getTime()));

			if ( doc.save() ) {
				// sess.setConvertMime(true);
				// sess.setConvertMIME(true);
				jw.addProperty("unid", doc.getUniversalID());
				this.unid = doc.getUniversalID();
				this.recmap.put("unid", this.unid);
				return true;
			} else {
				sess.setConvertMime(true);
				sess.setConvertMIME(true);
				jw.addProperty("unid", "");
				jw.addProperty("error", "save failed");
				jw.addProperty("dbCurrentAccessLevel", "" + db.getCurrentAccessLevel());
				this.unid = "";
				this.recmap.put("unid", "");
				return false;
			}

		} catch (Exception e) {
			// HS_Util.debug(e.toString(), "error", msgContext);
			if ( this.isDebugOn() ) e.printStackTrace();
			if ( doc != null ) {
				doc.replaceItemValue("Error", e.toString() + " in " + msgContext);
				doc.save();
			}
			try {
				jw.addProperty("status", "error");
				jw.addProperty("error", e.toString() + " in " + msgContext);
				if ( this.recmap != null ) this.recmap.put("error", e.toString() + " in " + msgContext);
			} catch (IOException e1) {
			}

		}
		return false;
	}

	private boolean savePhotoDocument(Map<String, Object> req, HS_JsonWriter jw) throws IOException {
		return PostPhoto.savePhotoDocument(req, jw, this);
	}

	private String setInspValue(Map<String, Object> imap, String fname, String dfltvalue) {
		fname = (fname.startsWith("record_")) ? fname : "record_" + fname;
		return (imap.keySet().contains(fname)) ? (String) imap.get(fname) : dfltvalue;
	}

	private Double setInspValue(Map<String, Object> imap, String fname, Double dfltvalue) {
		fname = (fname.startsWith("record_")) ? fname : "record_" + fname;
		if ( !(imap.keySet().contains(fname)) ) return dfltvalue;
		Object tmp = imap.get(fname);
		// if (this.isDebugOn()) System.out.print(this.getClass().getName() +
		// "-setInspValue: tmp is a [" +tmp.getClass().getName() + "] value:[");
		if ( !(tmp instanceof Double) ) return dfltvalue;
		return (imap.keySet().contains(fname)) ? (Double) tmp : dfltvalue;
	}

	private int setInspValue(Map<String, Object> imap, String fname, int i) {
		if ( !(imap.keySet().contains("record_" + fname)) ) return i;
		Double dbl = new Double(i);
		try {
			if ( imap.get("record_" + fname) instanceof Double ) {
				dbl = (Double) imap.get("record_" + fname);
				return dbl.intValue();
			}
			if ( imap.get("record_" + fname) instanceof String ) {
				String mvalue = (imap.keySet().contains("record_" + fname)) ? (String) imap.get("record_" + fname) : "" + i;
				return Integer.parseInt(mvalue);
			}
		} catch (Exception ex) {
			// do nothing
		}
		return i;
	}

	private void startDeviceLog(HS_JsonWriter jw) throws IOException {
		final String msgContext = "restPostBasic.startDeviceLog";
		Document doc = null;
		try {
			this.getDeviceInfo();
			if ( "".equals(uuid) ) {
				jw.addProperty("deviceLogDebug", "No uuid found so device log not created. in " + msgContext);
				return;
			}
			jw.addProperty("deviceLogDebug", "adding/updating device log for [" + uuid + "]");
			doc = getDeviceDoc(uuid);
			if ( doc == null ) return;

			doc.replaceItemValue("devicePlatform", platform);
			doc.replaceItemValue("deviceVersion", version);
			doc.replaceItemValue("deviceName", dname);

			this.getAuthInfo();
			doc.replaceItemValue("authUserID", userID);
			doc.replaceItemValue("authClientID", clientID);
			doc.replaceItemValue("authFullname", fullname);

			doc.replaceItemValue("syncLastDate", new Date());
			doc.replaceItemValue("syncLastUser", user);
			doc.replaceItemValue("syncLastRecords", 0);
			doc.replaceItemValue("syncLastPermits", 0);

			boolean result = doc.save();
			this.deviceUnid = doc.getUniversalID();
			jw.addProperty("startDeviceLogDebug3", "Save result:[" + result + "] unid:[" + this.deviceUnid + "] user:[" + Factory.getSession().getEffectiveUserName() + "]");

		} catch (Exception e) {
			// HS_Util.debug(e.toString(), "error", msgContext);
			jw.addProperty("startDeviceLogError", msgContext + ": ERROR: " + e.toString());
			if ( this.isDebugOn() ) e.printStackTrace();
		}
	}

	private void writeResults() {
		// final String msgContext = "restPostBasic.writeResults";
		try {
			Session sess = Factory.getSession();
			Database db = sess.getCurrentDatabase();
			Document doc = db.getDocumentByUNID(this.unid);
			final int flen = 3000;
			if ( doc != null ) {
				doc.replaceItemValue("batch_results", this.logMsgs);
				String binsp = this.batchInspections.toString();
				Item binspItem = null;
				if ( binsp.length() < flen ) {
					binspItem = doc.replaceItemValue("batch_inspections", binsp);
					binspItem.setSummary(false);
				} else {
					try {
						int ctr = 0;
						String fname;
						while (binsp != null) {
							fname = "batch_inspections";
							fname += (ctr == 0) ? "" : "_" + ctr;
							if ( binsp.length() < flen ) {
								binspItem = doc.replaceItemValue(fname, binsp);
								binspItem.setSummary(false);
								binsp = null;
							} else {
								binspItem = doc.replaceItemValue(fname, binsp.substring(0, flen));
								binspItem.setSummary(false);
								binsp = binsp.substring(flen);
							}
							ctr++;
						}
					} catch (Exception e1) {
						doc.replaceItemValue("batch_inspection_error", e1.toString());
					}
				}
				doc.save();
			}
		} catch (Exception e) {
			// HS_Util.debug(e.toString(), "error", msgContext);
		}

	}

	public String getUnid() {
		return unid;
	}

	public void setUnid(String unid) {
		this.unid = unid;
	}

	public String getBatch() {
		return batch;
	}

	public boolean isDebugOn() {
		if ( "Yes".equals(HS_Util.getAppSettingString("debug_HSTUpload")) ) debugOn = true;
		return debugOn;
	}

	public void setDebugOn(boolean debugOn) {
		this.debugOn = debugOn;
	}

	public String getPhotoData() {
		return photoData;
	}

	public Date getStartDateTime() {
		return startDateTime;
	}

	public void addPhotoUnid(String punid) {
		if ( photoUnids == null ) {
			photoUnids = new ArrayList<String>();
		}
		photoUnids.add(unid);
	}
}
