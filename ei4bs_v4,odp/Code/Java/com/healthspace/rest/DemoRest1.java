package com.healthspace.rest;

import java.io.IOException;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ibm.domino.services.ServiceException;
import com.ibm.domino.services.rest.RestServiceEngine;
import com.ibm.xsp.extlib.component.rest.CustomService;
import com.ibm.xsp.extlib.component.rest.CustomServiceBean;

public class DemoRest1 extends CustomServiceBean {

	StringWriter sw = null;
	HS_JsonWriter jw = null;

	public DemoRest1() {

	}

	private void get(RestServiceEngine engine) {
		String msgContext = this.getClass().getName() + ".get";
		try {
			jw.addProperty("method", msgContext);
		} catch (IOException e) {
			e.printStackTrace();
			try {
				jw.addProperty("error", this.getClass().getName() + ".renderService ERROR: " + e.toString());
			} catch (IOException e1) {
			}
		}
	}

	private void other(RestServiceEngine engine) {
		String msgContext = this.getClass().getName() + ".other";
		try {
			jw.addProperty("method", msgContext);
		} catch (IOException e) {
			e.printStackTrace();
			try {
				jw.addProperty("error", this.getClass().getName() + ".renderService ERROR: " + e.toString());
			} catch (IOException e1) {

			}
		}
	}

	private void post(RestServiceEngine engine, HttpServletRequest request) {
		String msgContext = this.getClass().getName() + ".post";
		try {
			jw.addProperty("method", msgContext);
		} catch (IOException e) {
			e.printStackTrace();
			try {
				jw.addProperty("error", this.getClass().getName() + ".renderService ERROR: " + e.toString());
			} catch (IOException e1) {

			}
		}
	}

	@Override
	public void renderService(CustomService service, RestServiceEngine engine) throws ServiceException {
		HttpServletRequest request = engine.getHttpRequest();
		HttpServletResponse response = engine.getHttpResponse();

		sw = new StringWriter();
		jw = new HS_JsonWriter(sw, false);
		try {
			String method = request.getMethod();
			response.setHeader("Content-Type", "text/plain; charset=UTF-8");

			if ( method.equals("GET") ) {
				jw.startObject(); // Change this if you wnat to rturn an array of something...
				this.get(engine);
				jw.endObject();
			} else if ( method.equals("POST") ) {
				jw.startObject(); // Change this if you wnat to rturn an array of something...
				this.post(engine, request);
				jw.endObject();
			} else {
				jw.startObject(); // Change this if you wnat to rturn an array of something...
				this.other(engine);
				jw.endObject();
			}

		} catch (Exception e) {
			try {
				jw.addProperty("ERROR", this.getClass().getName() + ".renderService ERROR: " + e.toString());
			} catch (IOException e1) {
			}
			// System.out.print(output);
		} finally {
			try {
				response.getWriter().write(sw.toString());
				response.getWriter().close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
