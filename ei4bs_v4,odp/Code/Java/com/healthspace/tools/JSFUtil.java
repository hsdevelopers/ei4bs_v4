package com.healthspace.tools;

import java.util.Enumeration;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.VariableResolver;

import org.openntf.domino.Database;
import org.openntf.domino.Session;
import org.openntf.domino.utils.Factory;
import org.openntf.domino.utils.Factory.SessionType;

import com.ibm.xsp.application.DesignerApplicationEx;
import com.ibm.xsp.component.UIViewRootEx;
import com.ibm.xsp.designer.context.ServletXSPContext;
import com.ibm.xsp.designer.context.XSPContext;
import com.ibm.xsp.designer.context.XSPUrl;

/*
 * Based on JSFUtils from Tim Tripcony and Keith Strickland
 * 
 * portions base on work Provided by Mindoo Blog at: 
 * http://www.mindoo.com/web/blog.nsf/dx/18.07.2009191738KLENAL.htm?opendocument&comments#anc1
 */
public class JSFUtil {
	public JSFUtil() {
	}

	private static Session _signerSess;

	public static DesignerApplicationEx getApplication() {
		return (DesignerApplicationEx) getFacesContext().getApplication();
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> getApplicationScope() {
		return getServletContext().getApplicationMap();
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> getCompositeData() {
		return (Map<String, Object>) getVariableResolver().resolveVariable(getFacesContext(), "compositeData");
	}

	public static Database getCurrentDatabase() {
		return (Database) getVariableResolver().resolveVariable(getFacesContext(), "database");
	}

	public static Session getCurrentSession() {
		return (Session) getVariableResolver().resolveVariable(getFacesContext(), "session");
	}

	public static FacesContext getFacesContext() {
		return FacesContext.getCurrentInstance();
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> getRequestScope() {
		return getServletContext().getRequestMap();
	}

	@SuppressWarnings("unchecked")
	public static String getReferer() {
		String referer = "dunno yet";
		ExternalContext extContext = FacesContext.getCurrentInstance().getExternalContext();
		Map rhvm = extContext.getRequestHeaderValuesMap();
		Enumeration ref = (Enumeration) rhvm.get("Referer");
		if (ref instanceof Enumeration) {
			Object val = (ref).nextElement();
			if (val instanceof String) {
				referer = (String) val;
			} else if (val == null) {
				referer = "";
			} else {
				referer = "is not a string: " + val.getClass().getName();
			}
		} else {
			// report what it is instead of an Enumeration
			referer = "";
		}
		// if (localDebug) HS_Util.debug("referer:[" + referer + "]", "debug", msgContext);
		return referer;
	}

	public static ExternalContext getServletContext() {
		return getFacesContext().getExternalContext();
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> getSessionScope() {
		return getServletContext().getSessionMap();
	}

	private static VariableResolver getVariableResolver() {
		return getApplication().getVariableResolver();
	}

	public static UIViewRootEx getViewRoot() {
		return (UIViewRootEx) getFacesContext().getViewRoot();
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> getViewScope() {
		return getViewRoot().getViewMap();
	}

	public static XSPContext getXSPContext() {
		return XSPContext.getXSPContext(getFacesContext());
	}

	public static Object resolveVariable(String variable) {
		return FacesContext.getCurrentInstance().getApplication().getVariableResolver().resolveVariable(FacesContext.getCurrentInstance(), variable);
	}

	/**
	 * @since 3.0.0 Added by Paul Withers to get current session as signer
	 */
	public static Session getSessionAsSigner() {
		if (_signerSess == null) {
			// _signerSess = NotesContext.getCurrent().getSessionAsSigner();
			// _signerSess = Factory.fromLotus(NotesContext.getCurrent().getSessionAsSigner(), org.openntf.domino.Session.class, null);
			// _signerSess = Factory.fromLotus(NotesContext.getCurrent().getSessionAsSigner(), org.openntf.domino.Session.SCHEMA, null);
			/* // was: _signerSess = XSP Util . xgetCurrentSessionAsSigner(); */

			_signerSess = Factory.getSession(SessionType.SIGNER);
		}
		return _signerSess;
	}

	/**
	 * Get the current db Url
	 * 
	 * @return java.lang.String
	 */
	public static String getDbUrl() {
		ServletXSPContext thisContext = (ServletXSPContext) JSFUtil.getVariableValue("context");
		XSPUrl thisUrl = thisContext.getUrl();
		String permaLink = "http://" + thisUrl.getHost() + thisUrl.getPath().substring(0, thisUrl.getPath().lastIndexOf("/"));
		return permaLink;
	}

	/**
	 * The method returns the value of a global JavaScript variable.
	 * 
	 * @param varName
	 *            variable name
	 * @return value
	 * @throws javax.faces.el.EvaluationException
	 *             if an exception is thrown while resolving the variable name
	 */
	public static Object getVariableValue(String varName) {
		FacesContext context = FacesContext.getCurrentInstance();
		return context.getApplication().getVariableResolver().resolveVariable(context, varName);
	}

	/**
	 * Get a url parameter value
	 * 
	 * @param paramName
	 *            java.lang.String - The parameter name you want the value from
	 * @return java.lang.String
	 */
	public static String getParamValue(String paramName) {
		ServletXSPContext thisContext = (ServletXSPContext) JSFUtil.getVariableValue("context");
		return thisContext.getUrlParameter(paramName).toString();
	}

	/**
	 * Get the url as a string
	 * 
	 * @return java.lang.String
	 */
	public static String getUrl() {
		ServletXSPContext thisContext = (ServletXSPContext) JSFUtil.getVariableValue("context");
		return thisContext.getUrl().toString();
	}
}
