package com.healthspace.tools;

import java.io.Serializable;
import java.util.Date;

import org.openntf.EMailBean;
import org.openntf.domino.Database;
import org.openntf.domino.DateTime;
import org.openntf.domino.Document;
import org.openntf.domino.Name;
import org.openntf.domino.Session;
import org.openntf.domino.View;

import com.healthspace.general.HS_Util;
import com.ibm.xsp.extlib.util.ExtLibUtil;

public class Authentication implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Authentication() {

	}

	public static String requestPasswordReset(String email) {

		boolean localDebug = true;
		String dbarLoc = "Authentication.requestPasswordReset";
		// String msg = "";
		// var email = pwdreset.getItemValueString("emailAddress")

		boolean createDoc = true;
		try {
			if ( localDebug ) HS_Util.debug("testing email:[" + email + "]", "debug", dbarLoc);
			Session sess = HS_Util.getSessionAsSigner();

			// Check to see if the email is in the NAB
			Database nab = sess.getDatabase(sess.getServerName(), "names.nsf");
			View nabview = nab.getView("$Users");
			Document nabdoc = nabview.getFirstDocumentByKey(email, true);
			if ( nabdoc == null ) {
				ExtLibUtil.getSessionScope().put("pwdResetEmail", email);
			} else {
				// Check to see if the email is in a request
				View localview = sess.getCurrentDatabase().getView("CODE.pwdResetRequests.by.email");
				Document doc = localview.getFirstDocumentByKey(email, true);
				if ( doc != null ) {
					DateTime created = doc.getFirstItem("Created").getDateTimeValue();
					DateTime wdt = sess.createDateTime(new Date());

					Long age = (wdt.toJavaDate().getTime() - created.toJavaDate().getTime()) / 1000;
					if ( localDebug ) HS_Util.debug("Request found that was created:[" + created.getLocalTime() + "] age:[" + age + "]", "debug", dbarLoc);

					if ( age <= 60 * 60 * 2 ) {
						if ( localDebug ) HS_Util.debug("age:[" + age + "] is <= 60*60*2:[" + (60 * 60 * 2) + "]", "debug", dbarLoc);
						createDoc = false;
						// msg = "A request for an id for " + email + " is already in the system at HealthSpace. ";
						// msg += "Please contact support if this request has been outstanding more than one business day.";
						// // @ErrorMessage(msg);
						// if ( localDebug ) HS_Util.debug(msg, "debug", dbarLoc);
						// return msg;
					} else if ( "1. Submitted".equals(doc.getItemValueString("Status")) ) {
						if ( localDebug ) HS_Util.debug("age:[" + age + "] is > 60*60*2:[" + (60 * 60 * 2) + "] - flagging as expired.", "debug", dbarLoc);
						doc.replaceItemValue("Deleted", new Date());
						doc.replaceItemValue("Status", "Expired"); // remove the expired request
						doc.save(true, false);
					}
				}

				Document outDoc = null;
				if ( createDoc ) {
					outDoc = sess.getCurrentDatabase().createDocument();
					outDoc.replaceItemValue("Form", "pwdResetRequest");
					outDoc.replaceItemValue("EMailAddress", email);
					outDoc.replaceItemValue("FullName", nabdoc.getItemValue("FullName"));
					outDoc.replaceItemValue("Status", "1. Submitted");
					outDoc.replaceItemValue("Created", new Date());
					outDoc.replaceItemValue("DocumentID", sess.evaluate("@Unique"));

					outDoc.save(true, false);
					if ( localDebug ) HS_Util.debug("new request doc created: id:[" + outDoc.getItemValueString("DocumentID") + "]", "debug", dbarLoc);
				} else {
					outDoc = doc;
					if ( localDebug ) HS_Util.debug("resending document for request id:[" + outDoc.getItemValueString("DocumentID") + "]", "debug", dbarLoc);
				}

				// SEND THE EMAIL>>>>>
				String body = Authentication.buildBody((String) nabdoc.getItemValue("FullName").get(0), outDoc.getItemValueString("DocumentID"));
				Authentication.sendEMail(email, "HAgent@healthspace.com", "DoNotReply@healthspace.com", "HealthSpace Password Reset Request", "", body);

				ExtLibUtil.getSessionScope().put("pwdResetRequestUNID", outDoc.getUniversalID());
				ExtLibUtil.getSessionScope().put("pwdResetEmail", email);
			}
			JSFUtil.getXSPContext().redirectToPage("idResetRequest_thankyou.xsp");
			return "";
		} catch (Exception e) {
			// @ErrorMessage(e.toString);
			HS_Util.debug(e.toString(), "error", dbarLoc);
			return e.toString();
		}

	}

	private static String buildBody(String fullname, String docid) {
		Name fname = HS_Util.getSession().createName(fullname);

		String url = JSFUtil.getXSPContext().getUrl().toString();
		if ( !url.endsWith(".xsp") ) {

		}
		url += "?docid=" + docid;

		String body = "";
		body += "<style>p {padding-bottom:10px;}</style>";
		body += "<span style=\"font-family:Arial sans-serif\">";
		body += "<h3>Password reset email</h3>";
		body += "<p>Dear " + fname.getCommon() + "</p>";
		body += "<p>This message allows you to reset the web access password for your account at healthspace.com.";
		// body += "<p></p>";
		body += "<p>If you did not make the request to change your password, please delete and ignore this message.</p>";
		// body += "<p></p>";
		body += "<p>To reset your password click this link: <a href=\"" + url + "\">" + url + "</a></p>";
		body += "<p>HealthSpace Support</p>";
		body += "</span>";

		return body;
	}

	private static String sendEMail(String recipient, String sender, String principal, String subject, String banner, String body) {
		try {
			EMailBean emailBean = new EMailBean();
			emailBean.setSendTo(recipient);
			emailBean.setSubject(subject);
			emailBean.setSenderEmail(sender);
			// emailBean.setSenderName(principal);

			emailBean.setFieldName("Body");
			emailBean.setBannerHTML(banner);
			emailBean.addHTML(body);
			emailBean.send();

			return "message sent to " + emailBean.getSendTo();
		} catch (Exception e) {
			return "Error: " + e.toString();
		}
	}
}
