/**
 * This class is used to generate PDFs of web pages
 * It requires PD4ML to be installed in the database in which it resides. 
 * See documentation in the demo XPage for instructions on installation and use
 */
package com.healthspace.tools;

import java.awt.Dimension;
import java.awt.Insets;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;

import org.openntf.domino.Document;
import org.openntf.domino.EmbeddedObject;
import org.openntf.domino.RichTextItem;
import org.openntf.domino.Stream;
import org.zefer.pd4ml.PD4Constants;
import org.zefer.pd4ml.PD4ML;

import com.healthspace.general.HS_Util;

public class PDFGenerator {
	/**
	 * 
	 */
	// private static final long serialVersionUID = 1L;
	protected Dimension format = PD4Constants.LETTER;
	protected boolean landscapeValue = false;
	protected int topValue = 10;
	protected int leftValue = 10;
	protected int rightValue = 10;
	protected int bottomValue = 10;
	protected String unitsValue = "mm";
	protected String proxyHost = "";
	protected int proxyPort = 0;
	protected int userSpaceWidth = 780;
	private String versionInfo;

	/**
	 * Given a URL will generate a PDF version of the page and attach it to a new Document in the Database passed in
	 * 
	 * @param urlstring
	 * @param output
	 * @throws IOException
	 */
	public Document runConverter(String urlstring, Document document, String fieldname) throws IOException {
		return runConverter(urlstring, document, fieldname, false);
	}

	public Document runConverter(String urlstring, Document document, String fieldname, boolean skipPd4ml) throws IOException {
		return runConverter(urlstring, document, fieldname, false, false);
	}

	public Document runConverter(String urlstring, Document document, String fieldname, boolean skipPd4ml, boolean localDebug) throws IOException {
		String msgContext = "PDFGenerator.runConverter";
		try {
			if (urlstring.length() > 0) {
				// Make sure the URL begins with http
				if (!urlstring.startsWith("http://") && !urlstring.startsWith("https://") && !urlstring.startsWith("file:")) {
					urlstring = "http://" + urlstring;
				}
				if (!skipPd4ml) {
					// Create a temporary file to store the PDF
					Date date = new Date();
					File file = new File("webpage_" + date.getTime() + ".pdf");
					java.io.FileOutputStream fos = new java.io.FileOutputStream(file);
					if (proxyHost != null && proxyHost.length() != 0 && proxyPort != 0) {
						System.getProperties().setProperty("proxySet", "true");
						System.getProperties().setProperty("proxyHost", proxyHost);
						System.getProperties().setProperty("proxyPort", "" + proxyPort);
					}

					this.doRenderer(urlstring, fos);

					document.replaceItemValue("PD4MLVersion", PD4ML.getVersion());
					document.replaceItemValue("PD4MLDemo", ((PD4ML.isDemoMode()) ? "demo version" : "prod version"));

					if (localDebug) HS_Util.debug("PD4MLVersion:[" + PD4ML.getVersion() + "] - demo:[" + PD4ML.isDemoMode() + "]", "debug", msgContext);
					// Attach the PDF into the NotesDocument
					RichTextItem rtitem = null;
					if (document.hasItem(fieldname)) {
						rtitem = (RichTextItem) document.getFirstItem(fieldname);
					} else {
						rtitem = document.createRichTextItem(fieldname);
					}
					HS_Util.debug("file.getAbsolutePath():[" + file.getAbsolutePath() + "]", "debug", "PDFGenerator.runConverter");
					HS_Util.debug("file.getName():[" + file.getName() + "]", "debug", "PDFGenerator.runConverter");
					rtitem.embedObject(EmbeddedObject.EMBED_ATTACHMENT, "", file.getAbsolutePath(), file.getName());
					// document.save();

					// Remove the temporary file
					file.delete();
				} else {
					RichTextItem rtitem = null;
					if (document.hasItem(fieldname)) {
						rtitem = (RichTextItem) document.getFirstItem(fieldname);
					} else {
						rtitem = document.createRichTextItem(fieldname);
					}
					HS_Util.debug("sending fake file.", "debug", "PDFGenerator.runConverter");
					rtitem.embedObject(EmbeddedObject.EMBED_ATTACHMENT, "", "E:\\Domino\\DataHS\\testpdf.pdf", "testpdf.pdf");

				}
				// Return the Document to the caller
				return document;

			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", "PDFGenerator.runConverter");
			e.printStackTrace();
		}
		return null;
	}

	public Stream runConverterToStream(String urlstring, boolean skipPd4ml) throws IOException {
		return runConverterToStream(urlstring, skipPd4ml, false);
	}

	public ByteArrayInputStream runConverterToInputStream(String urlstring, boolean skipPd4ml, boolean localDebug) throws IOException {
		String msgContext = "PDFGenerator.runConverterToInputStream";
		try {
			if (urlstring.length() > 0) {
				// Make sure the URL begins with http
				if (!urlstring.startsWith("http://") && !urlstring.startsWith("https://") && !urlstring.startsWith("file:")) {
					urlstring = "http://" + urlstring;
				}

				this.versionInfo = "PD4MLVersion:[" + PD4ML.getVersion() + "] - demo:[" + PD4ML.isDemoMode() + "]  skipPd4ml[" + skipPd4ml + "]";
				if (localDebug) HS_Util.debug(this.versionInfo, "debug", msgContext);
				if (localDebug) System.out.println(msgContext + "- " + this.versionInfo);
				if (!skipPd4ml) {
					if (proxyHost != null && proxyHost.length() != 0 && proxyPort != 0) {
						System.getProperties().setProperty("proxySet", "true");
						System.getProperties().setProperty("proxyHost", proxyHost);
						System.getProperties().setProperty("proxyPort", "" + proxyPort);
					}

					ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
					if (this.doRenderer(urlstring, outputStream, localDebug)) {
						ByteArrayInputStream in = new ByteArrayInputStream(outputStream.toByteArray());
						return in;
					}
					return null;
				}
				// TODO Need to create a fake PDF stream here...
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			e.printStackTrace();
		}
		return null;
	}

	public Stream runConverterToStream(String urlstring, boolean skipPd4ml, boolean localDebug) throws IOException {
		String msgContext = "PDFGenerator.runConverterToStream";

		try {
			ByteArrayInputStream in = this.runConverterToInputStream(urlstring, skipPd4ml, localDebug);
			Stream dominoStream = HS_Util.getSession().createStream();
			dominoStream.setContents(in);
			if (localDebug) HS_Util.debug("finished doRenderer() dominoStreamSize:[" + dominoStream.getBytes() + "]", "debug", msgContext);
			return dominoStream;
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * Do the pd4ml stuff here
	 */
	private boolean doRenderer(String urlstring, OutputStream fos) {
		return doRenderer(urlstring, fos, false);
	}

	private boolean doRenderer(String urlstring, OutputStream fos, boolean localDebug) {
		String msgContext = "PDFGenerator.doRenderer";
		// Initialize the PD4ML object
		try {
			PD4ML pd4ml = new PD4ML();
			if (localDebug) pd4ml.enableDebugInfo();

			try {
				pd4ml.setPageSize(landscapeValue ? pd4ml.changePageOrientation(format) : format);
			} catch (Exception e) {
				// Ignore error here
			}
			if (unitsValue.equals("mm")) {
				pd4ml.setPageInsetsMM(new Insets(topValue, leftValue, bottomValue, rightValue));
			} else {
				pd4ml.setPageInsets(new Insets(topValue, leftValue, bottomValue, rightValue));
			}
			pd4ml.setHtmlWidth(userSpaceWidth);
			// Get the current user authentication
			setCookies(pd4ml);

			long sdt = new Date().getTime();
			if (localDebug) HS_Util.debug("calling pd4ml.render()", "debug", msgContext);
			if (localDebug) System.out.println(msgContext + " - calling pd4ml.render()");
			// Create the PDF
			pd4ml.render(urlstring, fos);
			if (localDebug) {
				long edt = new Date().getTime();
				System.out.println(msgContext + " - returned from pd4ml.render() msecs:[" + (edt - sdt) + "]");
			}

			return true;
		} catch (Exception e) {
			// HS_Util.debug(e.toString(), "error", "PDFGenerator.doRenderer");
			HS_Util.debug(e.toString(), "error", msgContext);
			return false;
		}
	}

	/**
	 * Takes the current user's cookies and passes them into PD4ML so that it is authenticated as current user NB - You MUST be running
	 * Session Authentication on the Domino server for this to work
	 * 
	 * @param pd4ml
	 */
	@SuppressWarnings("unchecked")
	private void setCookies(PD4ML pd4ml) {
		FacesContext ctx = FacesContext.getCurrentInstance();
		ExternalContext exCon = ctx.getExternalContext();
		Map<String, Object> cookies = exCon.getRequestCookieMap();
		Iterator<String> it = cookies.keySet().iterator();
		while (it.hasNext()) {
			Cookie cookie = (Cookie) cookies.get(it.next());
			pd4ml.setCookie(cookie.getName(), cookie.getValue());
		}
	}

	public String getVersionInfo() {
		return versionInfo;
	}

	/*****************
	 * public Dimension getFormat() { return format; }
	 * 
	 * public boolean isLandscapeValue() { return landscapeValue; }
	 * 
	 * public int getTopValue() { return topValue; }
	 * 
	 * public int getLeftValue() { return leftValue; }
	 * 
	 * public int getRightValue() { return rightValue; }
	 * 
	 * public int getBottomValue() { return bottomValue; }
	 * 
	 * public String getUnitsValue() { return unitsValue; }
	 * 
	 * public String getProxyHost() { return proxyHost; }
	 * 
	 * public int getProxyPort() { return proxyPort; }
	 * 
	 * public int getUserSpaceWidth() { return userSpaceWidth; }
	 * 
	 * public void setFormat(Dimension format) { this.format = format; }
	 * 
	 * public void setLandscapeValue(boolean landscapeValue) { this.landscapeValue = landscapeValue; }
	 * 
	 * public void setTopValue(int topValue) { this.topValue = topValue; }
	 * 
	 * public void setLeftValue(int leftValue) { this.leftValue = leftValue; }
	 * 
	 * public void setRightValue(int rightValue) { this.rightValue = rightValue; }
	 * 
	 * public void setBottomValue(int bottomValue) { this.bottomValue = bottomValue; }
	 * 
	 * public void setUnitsValue(String unitsValue) { this.unitsValue = unitsValue; }
	 * 
	 * public void setProxyHost(String proxyHost) { this.proxyHost = proxyHost; }
	 * 
	 * public void setProxyPort(int proxyPort) { this.proxyPort = proxyPort; }
	 * 
	 * public void setUserSpaceWidth(int userSpaceWidth) { this.userSpaceWidth = userSpaceWidth; }
	 ************/
}
