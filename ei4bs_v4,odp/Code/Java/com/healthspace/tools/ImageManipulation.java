package com.healthspace.tools;

import java.io.FileInputStream;
import java.util.Arrays;

import org.apache.commons.codec.binary.Base64;
import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.EmbeddedObject;
import org.openntf.domino.RichTextItem;
import org.openntf.domino.View;
import org.openntf.domino.ViewEntry;

import com.healthspace.general.HS_Util;

public class ImageManipulation {

	public ImageManipulation() {
	}

	public static Object getEO(String purpose) {
		return getEO(purpose, 0);
	}

	public static Object getEO(String purpose, int pos) {
		Object rslt = null;
		final String dbarLoc = "ImageManipulation.getEO";
		final boolean localDebug = true;
		try {
			Database db = HS_Util.getSession().getCurrentDatabase();
			View vw = db.getView("HS_Files");
			for (ViewEntry ve : vw.getAllEntries()) {
				// if (localDebug) HS_Util.debug("doc purpose:[" + ve.getColumnValue("filePurpose") + "]", "debug", dbarLoc);
				if (purpose.equalsIgnoreCase((String) ve.getColumnValue("filePurpose"))) {
					String attname = (String) ve.getColumnValue("attname");
					if (localDebug)
						HS_Util.debug("FOUND the doc with purpose:[" + ve.getColumnValue("filePurpose") + "] attname:[" + attname + "]", "debug", dbarLoc);
					Document doc = ve.getDocument();
					if (doc.getEmbeddedObjects().size() > 0) {
						rslt = (doc.getEmbeddedObjects()).get(pos);
					} else {
						RichTextItem rtitem = (RichTextItem) doc.getFirstItem("file");
						rslt = rtitem.getEmbeddedObjects().get(pos);
					}
					return rslt;
				}
			}
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
		}
		return rslt;
	}

	public static FileInputStream convertEOToStream(EmbeddedObject eo) {
		FileInputStream rslt = null;
		final String dbarLoc = "ImageManipulation.convertEOToStream";
		try {
			rslt = (FileInputStream) eo.getInputStream();
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
		}
		return rslt;
	}

	public static byte[] convertImgStreamToByteArray(FileInputStream imgStream) {
		byte[] rslt = null;
		final String dbarLoc = "ImageManipulation.convertImgStreamToByteArray";
		final boolean localDebug = true;
		try {
			rslt = new byte[imgStream.available()];
			Arrays.fill(rslt, (byte) 0);
			int rlen = imgStream.read(rslt);
			if (localDebug) HS_Util.debug("stream read:[" + rlen + "]", "debug", dbarLoc);
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", dbarLoc);
		}

		return rslt;
	}

	/**
	 * Encodes the byte array into base64 string
	 * 
	 * @param imageByteArray
	 *            - byte array
	 * @return String a {@link java.lang.String}
	 */
	public static String encodeImage(byte[] imageByteArray) {
		return Base64.encodeBase64URLSafeString(imageByteArray);
	}

	/**
	 * Decodes the base64 string into byte array
	 * 
	 * @param imageDataString
	 *            - a {@link java.lang.String}
	 * @return byte array
	 */
	public static byte[] decodeImage(String imageDataString) {
		return Base64.decodeBase64(imageDataString);
	}
}
