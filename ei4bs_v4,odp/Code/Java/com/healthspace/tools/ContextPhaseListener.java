package com.healthspace.tools;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpServletRequest;

public class ContextPhaseListener implements PhaseListener {

	private static final long serialVersionUID = 1L;
	private static final String testXpage = "test.xsp";

	public void afterPhase(PhaseEvent arg0) {
		try {
			if ( arg0.getPhaseId() == PhaseId.RESTORE_VIEW ) return;

			FacesContext ctx = arg0.getFacesContext();
			ExternalContext exctx = ctx.getExternalContext();
			if ( null != exctx ) {
				HttpServletRequest request = (HttpServletRequest) exctx.getRequest();
				if ( null != request ) {
					String path = request.getPathTranslated();
					if ( null != path && path.contains(testXpage) ) {
						System.out.println("************************************ END PHASE LISTENER FOR " + arg0.getPhaseId().toString()
								+ " *******************************");
						System.out.println("--");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("AFTER PHASE LISTENER FOR " + arg0.getPhaseId().toString() + " THREW ERROR.");
		}
	}

	public void beforePhase(PhaseEvent arg0) {
		try {
			if ( arg0.getPhaseId() == PhaseId.RESTORE_VIEW ) return;

			FacesContext ctx = arg0.getFacesContext();
			ExternalContext exctx = ctx.getExternalContext();
			if ( null != exctx ) {
				HttpServletRequest request = (HttpServletRequest) exctx.getRequest();
				if ( null != request ) {
					String path = request.getPathTranslated();
					if ( null != path && path.contains(testXpage) ) {
						System.out.println("************************************ " + "BEGIN PHASE LISTENER FOR " + arg0.getPhaseId().toString()
								+ " *******************************");
						System.out.println("path: " + path);
					}
				}
			}

			// FacesContext ctx = arg0.getFacesContext();
			// UIViewRoot vr = ctx.getViewRoot();
			// if (vr != null) {
			// // final String cid = "inspectionCommentsTA1";
			// final String cid = "view:_id1:_id310:_id334:inspectionCommentsTA1";
			//
			// UIComponent comp = vr.findComponent(cid);
			// if (comp == null) {
			// System.out.println("ALERT: Unable to location the component, [" + cid + "]");
			// } else if (comp instanceof UIInput) {
			// UIInput input = (UIInput) comp;
			// System.out.println("Current value: " + String.valueOf(input.getValue()));
			// System.out.println("Submitted value: " + String.valueOf(input.getValue()));
			// } else {
			// System.out.println("ALERT: Comment component was not an input. It's a " + comp.getClass().getName());
			// }
			// }
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("BEFORE PHASE LISTENER FOR " + arg0.getPhaseId().toString() + " THREW ERROR.");
		}

	}

	public PhaseId getPhaseId() {
		return PhaseId.ANY_PHASE;
	}

}
