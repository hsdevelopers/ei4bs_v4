package com.healthspace.tools;

import java.util.UUID;

import org.openntf.domino.Document;
import org.openntf.domino.MIMEEntity;
import org.openntf.domino.MIMEHeader;
import org.openntf.domino.Session;
import org.openntf.domino.Stream;

import com.healthspace.general.HS_Util;

public class CanvasImageSaver {

	public CanvasImageSaver() {

	}

	public Document convertStream(Document doc, String imgBase64, String fldname) {
		final boolean localDebug = true;
		final String msgContext = "CanvasImageSaver.convertStream";
		if (localDebug) HS_Util.debug("Starting with source of [" + imgBase64.length() + "] characters for field:[" + fldname + "]", "debug", msgContext);

		Session ns = HS_Util.getSession();
		// Stream stream = null;
		MIMEEntity sigME = null;
		// MIMEEntity imgMe = null;
		try {
			String imgid = UUID.randomUUID().toString();
			if (localDebug) HS_Util.debug("imgid:[" + imgid + "]", "debug", msgContext);
			// String buffer = imgBase64.replace("data:image/png;base64,", "");
			ns.setConvertMime(false);
			// Create MIME Entity to store the image and HTML to display it
			sigME = doc.createMIMEEntity(fldname);
			MIMEHeader header = sigME.createHeader("Content-Type");
			header.setHeaderVal("multipart/mixed");
			header = sigME.createHeader("MIME-Version");
			header.setHeaderVal("1.0");

			// Create a text MIME part to show if the rest cannot be shown.
			// MIMEEntity txtME = sigME.createChildEntity();
			// header = txtME.createHeader("Content-Type");
			// header.setHeaderVal("text/plain");
			// stream = ns.createStream();
			// stream.writeText("The signature requires HTML rendering");
			// txtME.setContentFromText(stream, "text/plain", MIMEEntity.ENC_NONE);

			// Create the image mime part
			// imgMe = sigME.createChildEntity();
			// header = imgMe.createHeader("Content-Disposition");
			// header.setHeaderValAndParams("attachment; filename=signature1.png");
			// header = imgMe.createHeader("Content-Type");
			// header.setHeaderVal("multipart/related");
			// header = imgMe.createHeader("Content-ID");
			// header.setHeaderVal("<" + imgid + ">");

			// Add the image to MIME via stream
			// stream = ns.createStream();
			// Convert the Base64 string to binary
			// stream.writeText(buffer);
			// imgMe.setContentFromText(stream, "image/png", MIMEEntity.ENC_BASE64);
			// stream.close();

			// create the HTML mime part
			MIMEEntity htmlMe = sigME.createChildEntity();
			Stream textStream = ns.createStream();
			textStream.writeText("<html><body>");
			// textStream.writeText("<img src=\"cid:" + imgid + "\" alt=\"signature\"/>");
			textStream.writeText("<img src=\"" + imgBase64 + "\" alt=\"signature 64\"/>");
			textStream.writeText("</body></html>");
			htmlMe.setContentFromText(textStream, "text/html", MIMEEntity.ENC_NONE);

			// if (doc.hasItem(fldname)) doc.removeItem(fldname);
			// rtitem = doc.createRichTextItem(fldname);
			// tmpRtItem = (RichTextItem) doc.getFirstItem(tmpMimeFld);
			// rtitem.appendRTItem(tmpRtItem);
			// doc.removeItem(tmpMimeFld);

			doc.closeMIMEEntities();
			return doc;
		} catch (Exception e) {
			HS_Util.debug(e.toString(), "error", msgContext);
			return null;
		}
	}
}
