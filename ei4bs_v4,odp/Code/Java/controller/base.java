package controller;

import java.io.Serializable;
import java.util.Map;

import javax.faces.context.FacesContext;

import com.ibm.xsp.extlib.util.ExtLibUtil;

@SuppressWarnings("serial")
public class base implements Serializable {

	public void print(String msg) {

		System.out.println(msg);

	}

	@SuppressWarnings("unchecked")
	public static String getParam(String key) {

		// Using the Extention Library Latest Version
		// Map<String, String> myMap = (Map<String, String>) ExtLibUtil.resolveVariable("param");
		Map<String, String> myMap = (Map<String, String>) ExtLibUtil.resolveVariable(FacesContext.getCurrentInstance(), "param");

		if ( myMap.containsKey(key) ) {
			return myMap.get(key);
		} else {
			return "";
		}

	}

}
